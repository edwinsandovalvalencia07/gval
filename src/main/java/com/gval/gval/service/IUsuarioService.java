package com.gval.gval.service;


import com.gval.gval.dto.request.UsuarioRequestDto;
import com.gval.gval.dto.response.UsuarioResponseDto;

import java.util.List;

public interface IUsuarioService {
    List<UsuarioResponseDto> listAll();

    UsuarioResponseDto save(UsuarioRequestDto request);

    UsuarioResponseDto update(UsuarioRequestDto request,Integer id);

    String eliminar(Integer id);

    UsuarioResponseDto getUsuarioById (Integer id);
    List<UsuarioResponseDto> listarUsuarioActivo(Boolean active);

}
