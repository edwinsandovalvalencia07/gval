package com.gval.gval;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GvalApplication {

	public static void main(String[] args) {
		SpringApplication.run(GvalApplication.class, args);
	}

}
