package com.gval.gval.interfaceJar;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.lang.NonNull;

import com.querydsl.core.types.FactoryExpression;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.JPQLQuery;
public interface ExtendedQuerydslPredicateExecutor<T> extends QuerydslPredicateExecutor<T>{


    /**
     * Find one.
     *
     * @param <P> the generic type
     * @param query the query
     * @return the optional
     */
    <P> Optional<P> findOne(@NonNull JPQLQuery<P> query);

    /**
     * Find one.
     *
     * @param <P> the generic type
     * @param factoryExpression the factory expression
     * @param predicate the predicate
     * @return the optional
     */
    <P> Optional<P> findOne(@NonNull FactoryExpression<P> factoryExpression,
                            @NonNull Predicate predicate);

    /**
     * Find all.
     *
     * @param <P> the generic type
     * @param query the query
     * @return the list
     */
    <P> List<P> findAll(@NonNull JPQLQuery<P> query);

    /**
     * Find all.
     *
     * @param <P> the generic type
     * @param query the query
     * @param pageable the pageable
     * @return the page
     */
    <P> Page<P> findAll(@NonNull JPQLQuery<P> query, @NonNull Pageable pageable);

    /**
     * Find all.
     *
     * @param <P> the generic type
     * @param factoryExpression the factory expression
     * @param predicate the predicate
     * @return the list
     */
    <P> List<P> findAll(@NonNull FactoryExpression<P> factoryExpression,
                        @NonNull Predicate predicate);

    /**
     * Find all.
     *
     * @param <P> the generic type
     * @param factoryExpression the factory expression
     * @param predicate the predicate
     * @param pageable the pageable
     * @return the page
     */
    <P> Page<P> findAll(@NonNull FactoryExpression<P> factoryExpression,
                        @NonNull Predicate predicate, @NonNull Pageable pageable);

}
