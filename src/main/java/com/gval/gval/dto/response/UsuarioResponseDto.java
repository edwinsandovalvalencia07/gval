package com.gval.gval.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioResponseDto {
    private Integer id;
    private String username;
    private String password;
    private Boolean active;
    private Integer idEmpleado;
//    private EmpleadoResponseDto empleado;


}
