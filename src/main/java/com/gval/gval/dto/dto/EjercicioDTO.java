package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;
import es.gva.dependencia.ada.util.Utilidades;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * The Class EjercicioDTO.
 */
public class EjercicioDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -4065262178113774486L;

  /** The clave. */
  private Long clave;

  /** The anyo. */
  @NotNull
  private Integer anyo;

  /** The cerrado. */
  @NotNull
  private Boolean cerrado;

  /** The descripcion. */
  @Size(max = 50)
  private String descripcion;


  /**
   *
   */
  public EjercicioDTO() {
    super();
    this.anyo = Integer.valueOf(
        Utilidades.dateToString(new Date(), UtilidadesCommons.FORMATO_ANIO));
    this.cerrado = Boolean.FALSE;
  }



  /**
   * Gets the clave.
   *
   * @return the clave
   */
  public Long getClave() {
    return clave;
  }



  /**
   * Sets the clave.
   *
   * @param clave the clave to set
   */
  public void setClave(final Long clave) {
    this.clave = clave;
  }



  /**
   * Gets the anyo.
   *
   * @return the anyo
   */
  public Integer getAnyo() {
    return anyo;
  }



  /**
   * Sets the anyo.
   *
   * @param anyo the anyo to set
   */
  public void setAnyo(final Integer anyo) {
    this.anyo = anyo;
  }



  /**
   * Gets the cerrado.
   *
   * @return the cerrado
   */
  public Boolean getCerrado() {
    return cerrado;
  }



  /**
   * Sets the cerrado.
   *
   * @param cerrado the cerrado to set
   */
  public void setCerrado(final Boolean cerrado) {
    this.cerrado = cerrado;
  }



  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }



  /**
   * Sets the descripcion.
   *
   * @param descripcion the descripcion to set
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
