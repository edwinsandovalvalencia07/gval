package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;
import com.gval.gval.entity.model.Constantes;

import org.apache.commons.lang3.StringUtils;


/**
 * The Class UsuarioSimpleDTO.
 */
public class UsuarioSimpleDTO extends BaseDTO
    implements Comparable<UsuarioSimpleDTO> {

  /** The Constant COMA. */
  private static final String COMA = ",";

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -439863075644244136L;

  /** The pk persona. */
  private Long pkPersona;

  /** The identificador. */
  private String identificador;

  /** The nombre. */
  private String nombre;

  /** The primer apellido. */
  private String primerApellido;

  /** The segundo apellido. */
  private String segundoApellido;

  /**
   * Instantiates a new usuario simple DTO.
   */
  public UsuarioSimpleDTO() {
    super();
  }

  /**
   * Instantiates a new usuario simple DTO.
   *
   * @param pkPersona the pk persona
   * @param identificador the identificador
   * @param nombre the nombre
   * @param primerApellido the primer apellido
   * @param segundoApellido the segundo apellido
   */
  public UsuarioSimpleDTO(final Long pkPersona, final String identificador,
      final String nombre, final String primerApellido,
      final String segundoApellido) {
    super();
    this.pkPersona = pkPersona;
    this.identificador = identificador;
    this.nombre = nombre;
    this.primerApellido = primerApellido;
    this.segundoApellido = segundoApellido;
  }

  /**
   * Gets the pk persona.
   *
   * @return the pk persona
   */
  public Long getPkPersona() {
    return pkPersona;
  }

  /**
   * Sets the pk persona.
   *
   * @param pkPersona the new pk persona
   */
  public void setPkPersona(final Long pkPersona) {
    this.pkPersona = pkPersona;
  }

  /**
   * Gets the identificador.
   *
   * @return the identificador
   */
  public String getIdentificador() {
    return identificador;
  }

  /**
   * Sets the identificador.
   *
   * @param identificador the new identificador
   */
  public void setIdentificador(final String identificador) {
    this.identificador = identificador;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the primer apellido.
   *
   * @return the primer apellido
   */
  public String getPrimerApellido() {
    return primerApellido;
  }

  /**
   * Sets the primer apellido.
   *
   * @param primerApellido the new primer apellido
   */
  public void setPrimerApellido(final String primerApellido) {
    this.primerApellido = primerApellido;
  }

  /**
   * Gets the segundo apellido.
   *
   * @return the segundo apellido
   */
  public String getSegundoApellido() {
    return segundoApellido;
  }

  /**
   * Sets the segundo apellido.
   *
   * @param segundoApellido the new segundo apellido
   */
  public void setSegundoApellido(final String segundoApellido) {
    this.segundoApellido = segundoApellido;
  }

  /**
   * Gets the nombre completo.
   *
   * @return the nombre completo
   */
  public String getNombreCompleto() {
    final String nombrePersona =
        (StringUtils.isEmpty(getNombre())) ? Constantes.VACIO : getNombre();
    final String apellido1 =
        (StringUtils.isEmpty(getPrimerApellido())) ? Constantes.VACIO
            : getPrimerApellido();
    final String apellido2 =
        (StringUtils.isEmpty(getSegundoApellido())) ? Constantes.VACIO
            : getSegundoApellido();

    return nombrePersona.concat(Constantes.ESPACIO).concat(apellido1)
        .concat(Constantes.ESPACIO).concat(apellido2);
  }

  /**
   * Gets the apellidos nombre.
   *
   * @return the apellidos nombre
   */
  public String getApellidosNombre() {
    final String apellido1 =
        (StringUtils.isEmpty(getPrimerApellido())) ? Constantes.VACIO
            : getPrimerApellido();

    return UtilidadesCommons.stripAccents(apellido1
        .concat(getSegundoApellido() != null
            ? Constantes.ESPACIO.concat(getSegundoApellido())
            : Constantes.VACIO)
        .concat(COMA).concat(Constantes.ESPACIO).concat(getNombre()));
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final UsuarioSimpleDTO o) {
    return this.getApellidosNombre()
        .compareToIgnoreCase(o.getApellidosNombre());
  }

}
