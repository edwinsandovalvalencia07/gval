package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;

/**
 * The Class TipoPreferenciaDTO.
 */
public class TipoPreferenciaDTO extends BaseDTO
    implements Comparable<TipoPreferenciaDTO> {


  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -1097018804098203605L;

  /** The pk tipo prefsol. */
  private Long pkTipoPrefsol;

  /** The descripcion. */
  private String descripcion;

  /**
   * Instantiates a new tipo preferencia DTO.
   */
  public TipoPreferenciaDTO() {
    super();
  }

  /**
   * Instantiates a new tipo preferencia DTO.
   *
   * @param pkTipoPrefsol the pk tipo prefsol
   * @param descripcion the descripcion
   */
  public TipoPreferenciaDTO(final Long pkTipoPrefsol,
      final String descripcion) {
    super();
    this.pkTipoPrefsol = pkTipoPrefsol;
    this.descripcion = descripcion;
  }

  /**
   * Gets the pk tipo prefsol.
   *
   * @return the pk tipo prefsol
   */
  public Long getpkTipoPrefsol() {
    return pkTipoPrefsol;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the pk tipo prefsol.
   *
   * @param pkTipoPrefsol the new pk tipo prefsol
   */
  public void setpkTipoPrefsol(final Long pkTipoPrefsol) {
    this.pkTipoPrefsol = pkTipoPrefsol;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final TipoPreferenciaDTO o) {
    return 0;
  }

}
