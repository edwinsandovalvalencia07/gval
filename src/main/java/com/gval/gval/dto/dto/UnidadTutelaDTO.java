/**
 * Copyright (c) 2020 Generalitat Valenciana - Todos los derechos reservados.
 */
package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * Clase que tiene la información de Unidad Tutela.
 *
 * @author Indra
 */
public class UnidadTutelaDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 8042757587506026311L;

  /**  Identificador único de la unidad tutela *. */
  private Long pkUnidadTutela;

  /**  Nombre de la tulela (Provincia) *. */
  @Length(max = 100)
  private String descripcion;

  /**  Provincia *. */
  @NotNull
  private ProvinciaDTO provincia;

  /**
   * Gets the pk unidad tutela.
   *
   * @return the pkUnidadTutela
   */
  public Long getPkUnidadTutela() {
    return pkUnidadTutela;
  }

  /**
   * Sets the pk unidad tutela.
   *
   * @param pkUnidadTutela the pkUnidadTutela to set
   */
  public void setPkUnidadTutela(Long pkUnidadTutela) {
    this.pkUnidadTutela = pkUnidadTutela;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the descripcion to set
   */
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the provincia.
   *
   * @return the provincia
   */
  public ProvinciaDTO getProvincia() {
    return provincia;
  }

  /**
   * Sets the provincia.
   *
   * @param provincia the provincia to set
   */
  public void setProvincia(ProvinciaDTO provincia) {
    this.provincia = provincia;
  }



}
