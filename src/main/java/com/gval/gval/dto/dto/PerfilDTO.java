package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;

/**
 * The Class PerfilDTO.
 */
public class PerfilDTO extends BaseDTO implements Comparable<PerfilDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 6072355694531917675L;

  /** The pk perfil. */
  private Long pkPerfil;

  /** The activo. */
  private Boolean activo;

  /** The fecha hasta. */
  private Date fechaHasta;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The fecha desde. */
  private Date fechaDesde;

  /** The tipo perfil. */
  private TipoPerfilDTO tipoPerfil;

  /** The usuario. */
  private UsuarioDTO usuario;

  /**
   * Instantiates a new perfil DTO.
   */
  public PerfilDTO() {
    super();
  }

  /**
   * Instantiates a new perfil DTO.
   *
   * @param pkPerfil the pk perfil
   * @param activo the activo
   * @param fechaHasta the fecha hasta
   * @param fechaCreacion the fecha creacion
   * @param fechaDesde the fecha desde
   * @param tipoPerfil the tipo perfil
   * @param usuario the usuario
   */
  public PerfilDTO(final Long pkPerfil, final Boolean activo,
      final Date fechaHasta, final Date fechaCreacion, final Date fechaDesde,
      final TipoPerfilDTO tipoPerfil, final UsuarioDTO usuario) {
    super();
    this.pkPerfil = pkPerfil;
    this.activo = activo;
    this.fechaHasta = UtilidadesCommons.cloneDate(fechaHasta);
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
    this.fechaDesde = UtilidadesCommons.cloneDate(fechaDesde);
    this.tipoPerfil = tipoPerfil;
    this.usuario = usuario;
  }

  /**
   * Gets the pk perfil.
   *
   * @return the pk perfil
   */
  public Long getPkPerfil() {
    return pkPerfil;
  }

  /**
   * Sets the pk perfil.
   *
   * @param pkPerfil the new pk perfil
   */
  public void setPkPerfil(final Long pkPerfil) {
    this.pkPerfil = pkPerfil;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha hasta.
   *
   * @return the fecha hasta
   */
  public Date getFechaHasta() {
    return (fechaHasta == null) ? null : (Date) fechaHasta.clone();
  }

  /**
   * Sets the fecha hasta.
   *
   * @param fechaHasta the new fecha hasta
   */
  public void setFechaHasta(final Date fechaHasta) {
    this.fechaHasta = (fechaHasta == null) ? null : (Date) fechaHasta.clone();
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return (fechaCreacion == null) ? null : (Date) fechaCreacion.clone();
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion =
        (fechaCreacion == null) ? null : (Date) fechaCreacion.clone();
  }

  /**
   * Gets the fecha desde.
   *
   * @return the fecha desde
   */
  public Date getFechaDesde() {
    return (fechaDesde == null) ? null : (Date) fechaDesde.clone();
  }

  /**
   * Sets the fecha desde.
   *
   * @param fechaDesde the new fecha desde
   */
  public void setFechaDesde(final Date fechaDesde) {
    this.fechaDesde = (fechaDesde == null) ? null : (Date) fechaDesde.clone();
  }

  /**
   * Gets the tipo perfil.
   *
   * @return the tipo perfil
   */
  public TipoPerfilDTO getTipoPerfil() {
    return tipoPerfil;
  }

  /**
   * Sets the tipo perfil.
   *
   * @param tipoPerfil the new tipo perfil
   */
  public void setTipoPerfil(final TipoPerfilDTO tipoPerfil) {
    this.tipoPerfil = tipoPerfil;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final PerfilDTO o) {
    return 0;
  }

}
