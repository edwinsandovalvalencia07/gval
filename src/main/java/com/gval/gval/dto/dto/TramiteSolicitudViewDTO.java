package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;

import java.util.Date;

/**
 * The Class EstadoTramiteSolicitudDTO.
 */
public class TramiteSolicitudViewDTO {

  /** The pk est tramite solicitud. */
  private Long pkTramiteSolicitud;

  /** The nombre. */
  private String nombre;

  /** The apellidos. */
  private String apellidos;

  /** The nif. */
  private String nif;

  /** The telefono. */
  private String telefono;

  /** The cod expediente. */
  private String codExpediente;

  /** The estado. */
  private String estado;

  /** The fecha registro. */
  private Date fechaRegistro;

  /** The informacion general. */
  private int informacionGeneral;

  /** The pagos pendientes. */
  private int pagosPendientes;

  /** The respon patrimonial. */
  private int responPatrimonial;

  /** The pagos. */
  private int pagos;

  /** The otros. */
  private int otros;

  /** The otros desc. */
  private String otrosDesc;

  /** The fecha llamada. */
  private Date fechaLlamada;

  /** The observaciones. */
  private String observaciones;

  /**
   * Instantiates a new estado tramite solicitud DTO.
   */
  public TramiteSolicitudViewDTO() {
    super();
  }

  /**
   * Gets the otros desc.
   *
   * @return the otros desc
   */
  public String getOtrosDesc() {
    return otrosDesc;
  }

  /**
   * Sets the otros desc.
   *
   * @param otrosDesc the new otros desc
   */
  public void setOtrosDesc(final String otrosDesc) {
    this.otrosDesc = otrosDesc;
  }

  /**
   * Gets the nif.
   *
   * @return the nif
   */
  public String getNif() {
    return nif;
  }

  /**
   * Sets the nif.
   *
   * @param nif the new nif
   */
  public void setNif(final String nif) {
    this.nif = nif;
  }

  /**
   * Gets the telefono.
   *
   * @return the telefono
   */
  public String getTelefono() {
    return telefono;
  }

  /**
   * Sets the telefono.
   *
   * @param telefono the new telefono
   */
  public void setTelefono(final String telefono) {
    this.telefono = telefono;
  }

  /**
   * Gets the pk est tramite solicitud.
   *
   * @return the pk est tramite solicitud
   */
  public Long getPkTramiteSolicitud() {
    return pkTramiteSolicitud;
  }

  /**
   * Sets the pk est tramite solicitud.
   *
   * @param pkTramiteSolicitud the new pk est tramite solicitud
   */
  public void setPkTramiteSolicitud(final Long pkTramiteSolicitud) {
    this.pkTramiteSolicitud = pkTramiteSolicitud;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the apellidos.
   *
   * @return the apellidos
   */
  public String getApellidos() {
    return apellidos;
  }

  /**
   * Sets the apellidos.
   *
   * @param apellidos the new apellidos
   */
  public void setApellidos(final String apellidos) {
    this.apellidos = apellidos;
  }

  /**
   * Gets the cod expediente.
   *
   * @return the cod expediente
   */
  public String getCodExpediente() {
    return codExpediente;
  }

  /**
   * Sets the cod expediente.
   *
   * @param codExpediente the new cod expediente
   */
  public void setCodExpediente(final String codExpediente) {
    this.codExpediente = codExpediente;
  }

  /**
   * Gets the estado.
   *
   * @return the estado
   */
  public String getEstado() {
    return estado;
  }

  /**
   * Sets the estado.
   *
   * @param estado the new estado
   */
  public void setEstado(final String estado) {
    this.estado = estado;
  }

  /**
   * Gets the fecha registro.
   *
   * @return the fecha registro
   */
  public Date getFechaRegistro() {
    return UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Sets the fecha registro.
   *
   * @param fechaRegistro the new fecha registro
   */
  public void setFechaRegistro(final Date fechaRegistro) {
    this.fechaRegistro = UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Gets the informacion general.
   *
   * @return the informacion general
   */
  public int getInformacionGeneral() {
    return informacionGeneral;
  }

  /**
   * Sets the informacion general.
   *
   * @param informacionGeneral the new informacion general
   */
  public void setInformacionGeneral(final int informacionGeneral) {
    this.informacionGeneral = informacionGeneral;
  }

  /**
   * Gets the pagos pendientes.
   *
   * @return the pagos pendientes
   */
  public int getPagosPendientes() {
    return pagosPendientes;
  }

  /**
   * Sets the pagos pendientes.
   *
   * @param pagosPendientes the new pagos pendientes
   */
  public void setPagosPendientes(final int pagosPendientes) {
    this.pagosPendientes = pagosPendientes;
  }

  /**
   * Gets the respon patrimonial.
   *
   * @return the respon patrimonial
   */
  public int getResponPatrimonial() {
    return responPatrimonial;
  }

  /**
   * Sets the respon patrimonial.
   *
   * @param responPatrimonial the new respon patrimonial
   */
  public void setResponPatrimonial(final int responPatrimonial) {
    this.responPatrimonial = responPatrimonial;
  }

  /**
   * Gets the pagos.
   *
   * @return the pagos
   */
  public int getPagos() {
    return pagos;
  }

  /**
   * Sets the pagos.
   *
   * @param pagos the new pagos
   */
  public void setPagos(final int pagos) {
    this.pagos = pagos;
  }

  /**
   * Gets the otros.
   *
   * @return the otros
   */
  public int getOtros() {
    return otros;
  }

  /**
   * Sets the otros.
   *
   * @param otros the new otros
   */
  public void setOtros(final int otros) {
    this.otros = otros;
  }

  /**
   * Gets the fecha llamada.
   *
   * @return the fecha llamada
   */
  public Date getFechaLlamada() {
    return UtilidadesCommons.cloneDate(fechaLlamada);
  }

  /**
   * Sets the fecha llamada.
   *
   * @param fechaLlamada the new fecha llamada
   */
  public void setFechaLlamada(final Date fechaLlamada) {
    this.fechaLlamada = UtilidadesCommons.cloneDate(fechaLlamada);
  }

  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return observaciones;
  }

  /**
   * Sets the observaciones.
   *
   * @param observaciones the new observaciones
   */
  public void setObservaciones(final String observaciones) {
    this.observaciones = observaciones;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return "TramiteSolicitudViewDTO [pkTramiteSolicitud=" + pkTramiteSolicitud
        + ", nombre=" + nombre + ", apellidos=" + apellidos + ", nif=" + nif
        + ", telefono=" + telefono + ", codExpediente=" + codExpediente
        + ", estado=" + estado + ", fechaRegistro=" + fechaRegistro
        + ", informacionGeneral=" + informacionGeneral + ", pagosPendientes="
        + pagosPendientes + ", responPatrimonial=" + responPatrimonial
        + ", pagos=" + pagos + ", otros=" + otros + ", otrosDesc=" + otrosDesc
        + ", fechaLlamada=" + fechaLlamada + ", observaciones=" + observaciones
        + "]";
  }


}
