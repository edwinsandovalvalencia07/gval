package com.gval.gval.dto.dto.util;

import com.gval.gval.common.UtilidadesCommons;


/**
 * The Class ByteArrayDTO.
 */

public class ByteArrayDTO extends BaseDTO {


  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 5340196778913477566L;


  /** Array de bytes del fichero. */
  private byte[] value;

  /** The name. */
  private String name;

  /**
   * Instantiates a new ByteArrayDTO.
   */
  public ByteArrayDTO() {
    super();
  }


  /**
   * Instantiates a new ByteArrayDTO.
   *
   * @param valor the valor
   */
  public ByteArrayDTO(final byte[] valor) {
    super();
    this.setValue(valor);
  }



  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public byte[] getValue() {
    return UtilidadesCommons.cloneBytes(value);
  }


  /**
   * Sets the valor.
   *
   * @param valor the new valor
   */
  public void setValue(final byte[] valor) {
    this.value = UtilidadesCommons.cloneBytes(valor);
  }


  /**
   * Gets the name.
   *
   * @return the name
   */
  public String getName() {
    return name;
  }


  /**
   * Sets the name.
   *
   * @param name the new name
   */
  public void setName(final String name) {
    this.name = name;
  }

}
