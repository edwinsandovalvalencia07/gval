package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;
import java.util.List;


public class LoteTercerosDTO extends BaseDTO {

  /**
   *
   */
  private static final long serialVersionUID = 2193994676439499303L;

  private long pkLote;
  
  private String codigo;
 
  private Date fechaEnvio;
 
  private Date fechaRespuesta;

  private List<TerceroDTO> terceros;
    

  /**
   * Instantiates a new lote terceros DTO.
   */
  public LoteTercerosDTO() {
    super();
    this.setCodigo(codigo);
    this.setFechaEnvio(fechaEnvio);
    this.setFechaRespuesta(fechaRespuesta);
    this.setTerceros(terceros);
   
  }

  /**
   * @return the pkLote
   */
  public long getPkLote() {
    return pkLote;
  }

  /**
   * @param pkLote the pkLote to set
   */
  public void setPkLote(long pkLote) {
    this.pkLote = pkLote;
  }

  /**
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * @param codigo the codigo to set
   */
  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

  /**
   * @return the fechaEnvio
   */
  public Date getFechaEnvio() {
    return UtilidadesCommons.cloneDate(fechaEnvio);
  }

  /**
   * @param fechaEnvio the fechaEnvio to set
   */
  public void setFechaEnvio(Date fechaEnvio) {
    this.fechaEnvio = UtilidadesCommons.cloneDate(fechaEnvio);
  }

  /**
   * @return the fechaRespuesta
   */
  public Date getFechaRespuesta() {
    return UtilidadesCommons.cloneDate(fechaRespuesta);
  }

  /**
   * @param fechaRespuesta the fechaRespuesta to set
   */
  public void setFechaRespuesta(Date fechaRespuesta) {
    this.fechaRespuesta = UtilidadesCommons.cloneDate(fechaRespuesta);
  }

  /**
   * @return the terceros
   */
  public List<TerceroDTO> getTerceros() {
    return UtilidadesCommons.collectorsToList(terceros);
  }

  /**
   * @param terceros the terceros to set
   */
  public void setTerceros(List<TerceroDTO> terceros) {
    this.terceros = UtilidadesCommons.collectorsToList(terceros);
  }
  
}
