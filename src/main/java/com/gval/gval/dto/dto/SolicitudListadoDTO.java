package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;
import java.util.Objects;


/**
 * The Class SolicitudListadoDTO.
 */
public class SolicitudListadoDTO extends BaseDTO
    implements Comparable<SolicitudListadoDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -1230766153282658196L;

  /** The pk solicitud. */
  private Long pkSolicitud;

  /** The codigo. */
  private String codigo;

  /** The tipo solicitud. */
  private String tipoSolicitud;

  /** The fecha registro. */
  private Date fechaRegistro;

  /** The bloqueada. */
  private Boolean bloqueada;

  /** The estado solicitud. */
  private String estadoSolicitud;

  /** The grado. */
  private String grado;

  /** The nivel. */
  private String nivelPantalla;

  /** The traslado entrante. */
  private Boolean trasladoEntrante;

  /** The traslado saliente. */
  private Boolean trasladoSaliente;

  /** The fecha Revisable. */
  private Date fechaRevisable;


  /** The motivo finalizada. */
  private String motivoFinalizada;

  /**
   * Instantiates a new solicitud listado DTO.
   */
  public SolicitudListadoDTO() {
    super();
  }

  /**
   * Instantiates a new solicitud listado DTO.
   *
   * @param pkSolicitud the pk solicitud
   * @param codigo the codigo
   * @param tipoSolicitud the tipo solicitud
   * @param fechaRegistro the fecha registro
   * @param estadoSolicitud the estado solicitud
   * @param bloqueada the bloqueada
   * @param grado the grado
   * @param nivelPantalla the nivel pantalla
   * @param fechaRevisable the fecha revisable
   * @param trasladoEntrante the traslado entrante
   * @param trasladoSaliente the traslado saliente
   * @param revision the revision
   * @param motivoFinalizada the motivo finalizada
   */
  public SolicitudListadoDTO(final Long pkSolicitud, final String codigo,
      final String tipoSolicitud, final Date fechaRegistro,
      final String estadoSolicitud, final Boolean bloqueada, final String grado,
      final String nivelPantalla, final Date fechaRevisable,
      final Boolean trasladoEntrante, final Boolean trasladoSaliente,
      final Boolean revision, final String motivoFinalizada) {
    super();
    this.pkSolicitud = pkSolicitud;
    this.codigo = codigo;
    this.tipoSolicitud = tipoSolicitud;
    this.fechaRegistro = UtilidadesCommons.cloneDate(fechaRegistro);
    this.estadoSolicitud = estadoSolicitud;
    this.bloqueada = bloqueada;
    this.grado = grado;
    this.nivelPantalla = nivelPantalla;
    this.fechaRevisable = UtilidadesCommons.cloneDate(fechaRevisable);
    this.trasladoEntrante = trasladoEntrante;
    this.trasladoSaliente = trasladoSaliente;
    this.motivoFinalizada = motivoFinalizada;
  }

  /**
   * Gets the pk solicitud.
   *
   * @return the pkSolicitud
   */
  public Long getPkSolicitud() {
    return pkSolicitud;
  }

  /**
   * Sets the pk solicitud.
   *
   * @param pkSolicitud the pkSolicitud to set
   */
  public void setPkSolicitud(final Long pkSolicitud) {
    this.pkSolicitud = pkSolicitud;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the codigo to set
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the tipo solicitud.
   *
   * @return the tipoSolicitud
   */
  public String getTipoSolicitud() {
    return tipoSolicitud;
  }

  /**
   * Sets the tipo solicitud.
   *
   * @param tipoSolicitud the tipoSolicitud to set
   */
  public void setTipoSolicitud(final String tipoSolicitud) {
    this.tipoSolicitud = tipoSolicitud;
  }

  /**
   * Gets the fecha registro.
   *
   * @return the fechaRegistro
   */
  public Date getFechaRegistro() {
    return UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Sets the fecha registro.
   *
   * @param fechaRegistro the fechaRegistro to set
   */
  public void setFechaRegistro(final Date fechaRegistro) {
    this.fechaRegistro = UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Gets the bloqueada.
   *
   * @return the bloqueada
   */
  public Boolean getBloqueada() {
    return bloqueada;
  }


  /**
   * Sets the bloqueada.
   *
   * @param bloqueada the bloqueada to set
   */
  public void setBloqueada(final Boolean bloqueada) {
    this.bloqueada = bloqueada;
  }

  /**
   * Gets the estado solicitud.
   *
   * @return the estadoSolicitud
   */
  public String getEstadoSolicitud() {
    return estadoSolicitud;
  }

  /**
   * Sets the estado solicitud.
   *
   * @param estadoSolicitud the estadoSolicitud to set
   */
  public void setEstadoSolicitud(final String estadoSolicitud) {
    this.estadoSolicitud = estadoSolicitud;
  }

  /**
   * Gets the grado.
   *
   * @return the grado
   */
  public String getGrado() {
    return grado;
  }

  /**
   * Sets the grado.
   *
   * @param grado the grado to set
   */
  public void setGrado(final String grado) {
    this.grado = grado;
  }

  /**
   * Gets the traslado entrante.
   *
   * @return the trasladoEntrante
   */
  public Boolean getTrasladoEntrante() {
    return trasladoEntrante;
  }


  /**
   * Sets the traslado entrante.
   *
   * @param trasladoEntrante the trasladoEntrante to set
   */
  public void setTrasladoEntrante(final Boolean trasladoEntrante) {
    this.trasladoEntrante = trasladoEntrante;
  }

  /**
   * Gets the traslado saliente.
   *
   * @return the trasladoSaliente
   */
  public Boolean getTrasladoSaliente() {
    return trasladoSaliente;
  }


  /**
   * Sets the traslado saliente.
   *
   * @param trasladoSaliente the trasladoSaliente to set
   */
  public void setTrasladoSaliente(final Boolean trasladoSaliente) {
    this.trasladoSaliente = trasladoSaliente;
  }



  /**
   * Gets the nivel pantalla.
   *
   * @return the nivel pantalla
   */
  public String getNivelPantalla() {
    return nivelPantalla;
  }

  /**
   * Sets the nivel pantalla.
   *
   * @param nivelPantalla the new nivel pantalla
   */
  public void setNivelPantalla(final String nivelPantalla) {
    this.nivelPantalla = nivelPantalla;
  }

  /**
   * Gets the fecha revisable.
   *
   * @return the fecha revisable
   */
  public Date getFechaRevisable() {
    return UtilidadesCommons.cloneDate(fechaRevisable);
  }

  /**
   * Sets the fecha revisable.
   *
   * @param fechaRevisable the new fecha revisable
   */
  public void setFechaRevisable(final Date fechaRevisable) {
    this.fechaRevisable = UtilidadesCommons.cloneDate(fechaRevisable);
  }

  /**
   * Gets the motivo finalizada.
   *
   * @return the motivo finalizada
   */
  public String getMotivoFinalizada() {
    return motivoFinalizada;
  }

  /**
   * Sets the motivo finalizada.
   *
   * @param motivoFinalizada the new motivo finalizada
   */
  public void setMotivoFinalizada(final String motivoFinalizada) {
    this.motivoFinalizada = motivoFinalizada;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final SolicitudListadoDTO o) {
    return 0;
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result =
        prime * result + ((pkSolicitud == null) ? 0 : pkSolicitud.hashCode());
    return result;
  }

  /**
   * Equals.
   *
   * @param obj the obj
   * @return true, if successful
   */
  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    // null check
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final SolicitudListadoDTO other = (SolicitudListadoDTO) obj;
    return other.getPkSolicitud() != null && this.pkSolicitud != null
        && Objects.equals(this.pkSolicitud, other.getPkSolicitud());
  }
}
