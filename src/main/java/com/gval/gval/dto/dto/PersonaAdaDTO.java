//package com.gval.gval.dto.dto;
//
//import com.gval.gval.common.UtilidadesCommons;
//
//
//import com.fasterxml.jackson.annotation.JsonBackReference;
//
//import com.gval.gval.common.enums.RolPersonaEnum;
//import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
//import org.hibernate.validator.constraints.Length;
//import org.hibernate.validator.constraints.NotBlank;
//
//import java.io.Serializable;
//import java.text.SimpleDateFormat;
//import java.time.ZoneId;
//import java.util.Collections;
//import java.util.Date;
//
//import javax.validation.constraints.AssertTrue;
//
//
///**
// * The Class PersonaAdaDTO.
// */
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.SOLICITANTE, dependFieldName = "sexoAda",
////    message = "error.solicitante.sexo")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.SOLICITANTE,
////    dependFieldName = "fechaNacimiento",
////    message = "error.solicitante.fecha_nacimiento")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.SOLICITANTE,
////    dependFieldName = "nacionalidadAda",
////    message = "error.solicitante.nacionalidad")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.SOLICITANTE,
////    dependFieldName = "autorizaAccesoIdentidad",
////    message = "error.solicitante.autorizacion.identidad")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.SOLICITANTE,
////    dependFieldName = "autorizaAccesoResidencia",
////    message = "error.solicitante.autorizacion.residencia")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.SOLICITANTE,
////    dependFieldName = "autorizaAccesoEconomicos",
////    message = "error.solicitante.autorizacion.economicos")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.SOLICITANTE,
////    dependFieldName = "autorizaAccesoSanitarios",
////    message = "error.solicitante.autorizacion.sanitarios")
////@NotNullIfAnotherFieldHasValueWithCondition(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.SOLICITANTE,
////    dependFieldName = "autorizaAccesoNacimiento",
////    condition = "conditionToCheck",
////    message = "error.solicitante.autorizacion.nacimiento")
////@NotNullIfAnotherFieldHasValueWithCondition(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.SOLICITANTE,
////    dependFieldName = "autorizaAccesoPensiones",
////    condition = "conditionToCheck",
////    message = "error.solicitante.autorizacion.pensiones")
////@NotNullIfAnotherFieldHasValueWithCondition(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.SOLICITANTE,
////    dependFieldName = "autorizaAccesoRepresentantes",
////    condition = "conditionToCheck",
////    message = "error.solicitante.autorizacion.representantes")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.FAMILIAR,
////    dependFieldName = "autorizaAccesoIdentidad",
////    message = "error.unidadfamiliar.autorizacion.identidad")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.FAMILIAR,
////    dependFieldName = "autorizaAccesoResidencia",
////    message = "error.unidadfamiliar.autorizacion.residencia")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.FAMILIAR,
////    dependFieldName = "autorizaAccesoEconomicos",
////    message = "error.unidadfamiliar.autorizacion.economicos")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.FAMILIAR,
////    dependFieldName = "fechaNacimiento",
////    message = "error.solicitante.fecha_nacimiento")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.REPRESENTANTE,
////    dependFieldName = "autorizaAccesoIdentidad",
////    message = "error.representante.autorizacion.identidad")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.CUIDADOR,
////    dependFieldName = "autorizaAccesoIdentidad",
////    message = "error.cuidador.autorizacion.identidad")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.CUIDADOR,
////    dependFieldName = "autorizaAccesoResidencia",
////    message = "error.cuidador.autorizacion.residencia")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.CUIDADOR,
////    dependFieldName = "autorizaAccesoEconomicos",
////    message = "error.cuidador.autorizacion.economicos")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.CUIDADOR,
////    dependFieldName = "fechaNacimiento",
////    message = "error.solicitante.fecha_nacimiento")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.CUIDADOR,
////    dependFieldName = "nacionalidadAda",
////    message = "error.solicitante.nacionalidad")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.CUIDADOR, dependFieldName = "sexoAda",
////    message = "error.solicitante.sexo")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.ASISTENTE,
////    dependFieldName = "autorizaAccesoIdentidad",
////    message = "error.asistente.autorizacion.identidad")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.ASISTENTE,
////    dependFieldName = "autorizaAccesoResidencia",
////    message = "error.asistente.autorizacion.residencia")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.ASISTENTE,
////    dependFieldName = "autorizaAccesoEconomicos",
////    message = "error.asistente.autorizacion.economicos")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.ASISTENTE,
////    dependFieldName = "fechaNacimiento",
////    message = "error.solicitante.fecha_nacimiento")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.ASISTENTE,
////    dependFieldName = "nacionalidadAda",
////    message = "error.solicitante.nacionalidad")
////@NotNullIfAnotherFieldHasValue(fieldName = "rolPersona",
////    fieldValue = RolPersonaEnum.Roles.ASISTENTE, dependFieldName = "sexoAda",
////    message = "error.solicitante.sexo")
//public class PersonaAdaDTO extends PersonaDTO
//    implements Comparable<PersonaAdaDTO>, Serializable {
//
//  /** serialVersionUID. */
//  private static final long serialVersionUID = -6382432220377903218L;
//
//  /** The pk persona. */
//  private Long pkPersona;
//
//  /** The id intregracion imserso. */
//  private String idIntregracionImserso;
//
//  /** The pendiente envio imserso. */
//  private Boolean pendienteEnvioImserso;
//
//  /** The activo. */
//  private Boolean activo;
//
//  /** The edad. */
//  private Long edad;
//
//  /** The centro salud. */
//  private String centroSalud;
//
//  /** The grupo. */
//  private Long grupo;
//
//  /** The sector. */
//  private String sector;
//
//  /** The seguridad social. */
//  private Boolean seguridadSocial;
//
//  /** The servicio privado. */
//  private Boolean servicioPrivado;
//
//  /** The servicio publico. */
//  private Boolean servicioPublico;
//
//  /** The titular. */
//  private Boolean titular;
//
//  /** The numero SIP. */
//  @NotBlank(groups = Alerta.class, message = "alerta.personas.sip.vacio")
//  private String numeroSIP;
//
//  /** The nuss. */
//  @Length(groups = ValidacionesEstadoGrabada.class, max = 15,
//      message = "validacion.personas.nuss.longitud")
//  private String nuss;
//
//  /** The autoriza acceso identidad. */
//  @AssertTrue(groups = AutorizaAcceso.class,
//      message = "error.solicitante.no.autorizacion.identidad")
//  private Boolean autorizaAccesoIdentidad;
//
//  /** The autoriza acceso residencia. */
//  @AssertTrue(groups = AutorizaAcceso.class,
//      message = "error.solicitante.no.autorizacion.residencia")
//  private Boolean autorizaAccesoResidencia;
//
//  /** The autoriza acceso otras. */
//  private Boolean autorizaAccesoOtras;
//
//  /** The autoriza acceso economicos. */
//  @AssertTrue(groups = AutorizaAcceso.class,
//      message = "error.solicitante.no.autorizacion.economicos")
//  private Boolean autorizaAccesoEconomicos;
//
//  /** The autoriza acceso sanitarios. */
//  private Boolean autorizaAccesoSanitarios;
//
//  /** The autoriza acceso nacimiento y filiacion. */
//  private Boolean autorizaAccesoNacimiento;
//
//  /** The autoriza acceso pensiones INSS. */
//  private Boolean autorizaAccesoPensiones;
//
//  /** The autoriza acceso registro de representantes. */
//  private Boolean autorizaAccesoRepresentantes;
//
//  /** The sexo ada. */
//  private Long sexoAda;
//
//  /** The nacionalidad ada. */
//  private Long nacionalidadAda;
//
//  /** The tipo identificador ada. */
//  private Long tipoIdentificadorAda;
//
//  /** The tipo identificador DTO. */
//  private RefCodeDTO tipoIdentificadorRefCode;
//
//  /** The tipo estado civil ada. */
//  private Long tipoEstadoCivilAda;
//
//  /** The fecha creacion. */
//  private Date fechaCreacion;
//
//  /** The direccion habitual. */
//  @JsonBackReference
//  private DireccionAdaDTO direccionHabitual;
//
//  /** The tipo persona. */
//  private String rolPersona;
//
//  /** The condition to check. */
//  private Boolean conditionToCheck;
//
//  /**
//   * Instantiates a new persona ada DTO.
//   *
//   * @param builder the builder
//   */
//  /*
//   * Instantiates a new persona ada DTO.
//   *
//   * @param builder the builder
//   */
//  private PersonaAdaDTO(final Builder builder) {
//    super();
//    this.tipoIdentificador = builder.tipoIdentificador;
//    this.createdBy = builder.createdBy;
//    this.creationDate = builder.creationDate;
//    this.lastModifiedBy = builder.lastModifiedBy;
//    this.lastModifiedDate = builder.lastModifiedDate;
//    this.pkPersona = builder.pkPersona;
//    this.idIntregracionImserso = builder.idIntregracionImserso;
//    this.pendienteEnvioImserso = builder.pendienteEnvioImserso;
//    this.activo = builder.activo;
//    this.edad = builder.edad;
//    this.centroSalud = builder.centroSalud;
//    this.grupo = builder.grupo;
//    this.sector = builder.sector;
//    this.seguridadSocial = builder.seguridadSocial;
//    this.servicioPrivado = builder.servicioPrivado;
//    this.servicioPublico = builder.servicioPublico;
//    this.titular = builder.titular;
//    this.fechaCreacion = builder.fechaCreacion;
//  }
//
//  /**
//   * Instantiates a new persona ada DTO.
//   */
//  public PersonaAdaDTO() {
//    super();
//  }
//
//  /**
//   * Instantiates a new persona ada DTO.
//   *
//   * @param defaultData the default data
//   */
//  public PersonaAdaDTO(final boolean defaultData) {
//    super();
//    if (defaultData) {
//      super.setId(new PersonaIdDTOBuilder().build());
//      super.setAutorizacionesAccesoDatos(Collections.emptyList());
//      super.setCuentasBancarias(Collections.emptyList());
//      super.setDirecciones(Collections.emptyList());
//      super.setDocumentosPersona(Collections.emptyList());
//      setDefaultDataPersonaAda();
//    }
//  }
//
//  /**
//   * Sets the dafult data persona ada.
//   */
//  public void setDefaultDataPersonaAda() {
//    this.setActivo(Boolean.TRUE);
//    this.setFechaCreacion(new Date());
//  }
//
//  /**
//   * Gets the pk persona.
//   *
//   * @return the pk persona
//   */
//  public Long getPkPersona() {
//    return pkPersona;
//  }
//
//  /**
//   * Sets the pk persona.
//   *
//   * @param pkPersona the new pk persona
//   */
//  public void setPkPersona(final Long pkPersona) {
//    this.pkPersona = pkPersona;
//  }
//
//
//  /**
//   * Gets the id intregracion imserso.
//   *
//   * @return the id intregracion imserso
//   */
//  public String getIdIntregracionImserso() {
//    return idIntregracionImserso;
//  }
//
//
//  /**
//   * Sets the id intregracion imserso.
//   *
//   * @param idIntregracionImserso the new id intregracion imserso
//   */
//  public void setIdIntregracionImserso(final String idIntregracionImserso) {
//    this.idIntregracionImserso = idIntregracionImserso;
//  }
//
//
//  /**
//   * Gets the pendiente envio imserso.
//   *
//   * @return the pendiente envio imserso
//   */
//  public Boolean getPendienteEnvioImserso() {
//    return pendienteEnvioImserso;
//  }
//
//
//  /**
//   * Sets the pendiente envio imserso.
//   *
//   * @param pendienteEnvioImserso the new pendiente envio imserso
//   */
//  public void setPendienteEnvioImserso(final Boolean pendienteEnvioImserso) {
//    this.pendienteEnvioImserso = pendienteEnvioImserso;
//  }
//
//
//  /**
//   * Gets the activo.
//   *
//   * @return the activo
//   */
//  public Boolean getActivo() {
//    return activo;
//  }
//
//
//  /**
//   * Sets the activo.
//   *
//   * @param activo the new activo
//   */
//  public void setActivo(final Boolean activo) {
//    this.activo = activo;
//  }
//
//
//  /**
//   * Gets the edad.
//   *
//   * @return the edad
//   */
//  public Long getEdad() {
//    return edad;
//  }
//
//
//  /**
//   * Sets the edad.
//   *
//   * @param edad the new edad
//   */
//  public void setEdad(final Long edad) {
//    this.edad = edad;
//  }
//
//
//  /**
//   * Gets the centro salud.
//   *
//   * @return the centro salud
//   */
//  public String getCentroSalud() {
//    return centroSalud;
//  }
//
//
//  /**
//   * Sets the centro salud.
//   *
//   * @param centroSalud the new centro salud
//   */
//  public void setCentroSalud(final String centroSalud) {
//    this.centroSalud = centroSalud;
//  }
//
//
//  /**
//   * Gets the grupo.
//   *
//   * @return the grupo
//   */
//  public Long getGrupo() {
//    return grupo;
//  }
//
//
//  /**
//   * Sets the grupo.
//   *
//   * @param grupo the new grupo
//   */
//  public void setGrupo(final Long grupo) {
//    this.grupo = grupo;
//  }
//
//
//  /**
//   * Gets the sector.
//   *
//   * @return the sector
//   */
//  public String getSector() {
//    return sector;
//  }
//
//
//  /**
//   * Sets the sector.
//   *
//   * @param sector the new sector
//   */
//  public void setSector(final String sector) {
//    this.sector = sector;
//  }
//
//
//  /**
//   * Gets the seguridad social.
//   *
//   * @return the seguridad social
//   */
//  public Boolean getSeguridadSocial() {
//    return seguridadSocial;
//  }
//
//
//  /**
//   * Sets the seguridad social.
//   *
//   * @param seguridadSocial the new seguridad social
//   */
//  public void setSeguridadSocial(final Boolean seguridadSocial) {
//    this.seguridadSocial = seguridadSocial;
//  }
//
//
//  /**
//   * Gets the servicio privado.
//   *
//   * @return the servicio privado
//   */
//  public Boolean getServicioPrivado() {
//    return servicioPrivado;
//  }
//
//
//  /**
//   * Sets the servicio privado.
//   *
//   * @param servicioPrivado the new servicio privado
//   */
//  public void setServicioPrivado(final Boolean servicioPrivado) {
//    this.servicioPrivado = servicioPrivado;
//  }
//
//
//  /**
//   * Gets the servicio publico.
//   *
//   * @return the servicio publico
//   */
//  public Boolean getServicioPublico() {
//    return servicioPublico;
//  }
//
//
//  /**
//   * Sets the servicio publico.
//   *
//   * @param servicioPublico the new servicio publico
//   */
//  public void setServicioPublico(final Boolean servicioPublico) {
//    this.servicioPublico = servicioPublico;
//  }
//
//
//  /**
//   * Gets the titular.
//   *
//   * @return the titular
//   */
//  public Boolean getTitular() {
//    return titular;
//  }
//
//
//  /**
//   * Sets the titular.
//   *
//   * @param titular the new titular
//   */
//  public void setTitular(final Boolean titular) {
//    this.titular = titular;
//  }
//
//  /**
//   * Gets the fecha creacion.
//   *
//   * @return the fecha creacion
//   */
//  public Date getFechaCreacion() {
//    return UtilidadesCommons.cloneDate(fechaCreacion);
//  }
//
//  /**
//   * Sets the fecha creacion.
//   *
//   * @param fechaCreacion the new fecha creacion
//   */
//  public void setFechaCreacion(final Date fechaCreacion) {
//    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
//  }
//
//
//  /**
//   * Gets the numero SIP.
//   *
//   * @return the numero SIP
//   */
//  public String getNumeroSIP() {
//    return numeroSIP;
//  }
//
//  /**
//   * Sets the numero SIP.
//   *
//   * @param numeroSIP the new numero SIP
//   */
//  public void setNumeroSIP(final String numeroSIP) {
//    this.numeroSIP = numeroSIP;
//  }
//
//  /**
//   * Gets the nuss.
//   *
//   * @return the nuss
//   */
//  public String getNuss() {
//    return nuss;
//  }
//
//  /**
//   * Sets the nuss.
//   *
//   * @param nuss the new nuss
//   */
//  public void setNuss(final String nuss) {
//    this.nuss = nuss;
//  }
//
//  /**
//   * Gets the autoriza acceso identidad.
//   *
//   * @return the autoriza acceso identidad
//   */
//  public Boolean getAutorizaAccesoIdentidad() {
//    return autorizaAccesoIdentidad;
//  }
//
//  /**
//   * Sets the autoriza acceso identidad.
//   *
//   * @param autorizaAccesoIdentidad the new autoriza acceso identidad
//   */
//  public void setAutorizaAccesoIdentidad(
//      final Boolean autorizaAccesoIdentidad) {
//    this.autorizaAccesoIdentidad = autorizaAccesoIdentidad;
//  }
//
//  /**
//   * Gets the autoriza acceso residencia.
//   *
//   * @return the autoriza acceso residencia
//   */
//  public Boolean getAutorizaAccesoResidencia() {
//    return autorizaAccesoResidencia;
//  }
//
//  /**
//   * Sets the autoriza acceso residencia.
//   *
//   * @param autorizaAccesoResidencia the new autoriza acceso residencia
//   */
//  public void setAutorizaAccesoResidencia(
//      final Boolean autorizaAccesoResidencia) {
//    this.autorizaAccesoResidencia = autorizaAccesoResidencia;
//  }
//
//  /**
//   * Gets the autoriza acceso otras.
//   *
//   * @return the autoriza acceso otras
//   */
//  public Boolean getAutorizaAccesoOtras() {
//    return autorizaAccesoOtras;
//  }
//
//  /**
//   * Sets the autoriza acceso otras.
//   *
//   * @param autorizaAccesoOtras the new autoriza acceso otras
//   */
//  public void setAutorizaAccesoOtras(final Boolean autorizaAccesoOtras) {
//    this.autorizaAccesoOtras = autorizaAccesoOtras;
//  }
//
//  /**
//   * Gets the autoriza acceso economicos.
//   *
//   * @return the autoriza acceso economicos
//   */
//  public Boolean getAutorizaAccesoEconomicos() {
//    return autorizaAccesoEconomicos;
//  }
//
//  /**
//   * Sets the autoriza acceso economicos.
//   *
//   * @param autorizaAccesoEconomicos the new autoriza acceso economicos
//   */
//  public void setAutorizaAccesoEconomicos(
//      final Boolean autorizaAccesoEconomicos) {
//    this.autorizaAccesoEconomicos = autorizaAccesoEconomicos;
//  }
//
//  /**
//   * Gets the autoriza acceso nacimiento y filiacion.
//   *
//   * @return the autoriza acceso nacimiento y filiacion
//   */
//  public Boolean getAutorizaAccesoNacimiento() {
//    return autorizaAccesoNacimiento;
//  }
//
//  /**
//   * Sets the autoriza acceso nacimiento y filiacion.
//   *
//   * @param autorizaAccesoNacimiento the new autoriza acceso nacimiento y
//   *        filiacion
//   */
//  public void setAutorizaAccesoNacimiento(
//      final Boolean autorizaAccesoNacimiento) {
//    this.autorizaAccesoNacimiento = autorizaAccesoNacimiento;
//  }
//
//  /**
//   * Gets the autoriza acceso pensiones INSS.
//   *
//   * @return the autoriza acceso pensiones INSS
//   */
//  public Boolean getAutorizaAccesoPensiones() {
//    return autorizaAccesoPensiones;
//  }
//
//  /**
//   * Sets the autoriza acceso pensiones INSS.
//   *
//   * @param autorizaAccesoPensiones the new autoriza acceso pensiones INSS
//   */
//  public void setAutorizaAccesoPensiones(
//      final Boolean autorizaAccesoPensiones) {
//    this.autorizaAccesoPensiones = autorizaAccesoPensiones;
//  }
//
//
//  /**
//   * Gets the autoriza acceso registro de representantes.
//   *
//   * @return the autoriza acceso registro de representantes
//   */
//  public Boolean getAutorizaAccesoRepresentantes() {
//    return autorizaAccesoRepresentantes;
//  }
//
//  /**
//   * Sets the autoriza acceso registro de representantes.
//   *
//   * @param autorizaAccesoRepresentantes the new autoriza acceso representantes
//   */
//  public void setAutorizaAccesoRepresentantes(
//      final Boolean autorizaAccesoRepresentantes) {
//    this.autorizaAccesoRepresentantes = autorizaAccesoRepresentantes;
//  }
//
//
//  /**
//   * Gets the autoriza acceso sanitarios.
//   *
//   * @return the autoriza acceso sanitarios
//   */
//  public Boolean getAutorizaAccesoSanitarios() {
//    return autorizaAccesoSanitarios;
//  }
//
//  /**
//   * Sets the autoriza acceso sanitarios.
//   *
//   * @param autorizaAccesoSanitarios the new autoriza acceso sanitarios
//   */
//  public void setAutorizaAccesoSanitarios(
//      final Boolean autorizaAccesoSanitarios) {
//    this.autorizaAccesoSanitarios = autorizaAccesoSanitarios;
//  }
//
//  /**
//   * Gets the sexo ada.
//   *
//   * @return the sexo ada
//   */
//  public Long getSexoAda() {
//    return sexoAda;
//  }
//
//  /**
//   * Sets the sexo ada.
//   *
//   * @param sexoAda the new sexo ada
//   */
//  public void setSexoAda(final Long sexoAda) {
//    this.sexoAda = sexoAda;
//  }
//
//  /**
//   * Gets the nacionalidad ada.
//   *
//   * @return the nacionalidad ada
//   */
//  public Long getNacionalidadAda() {
//    return nacionalidadAda;
//  }
//
//  /**
//   * Sets the nacionalidad ada.
//   *
//   * @param nacionalidadAda the new nacionalidad ada
//   */
//  public void setNacionalidadAda(final Long nacionalidadAda) {
//    this.nacionalidadAda = nacionalidadAda;
//  }
//
//  /**
//   * Gets the tipo identificador ada.
//   *
//   * @return the tipo identificador ada
//   */
//  public Long getTipoIdentificadorAda() {
//    return tipoIdentificadorAda;
//  }
//
//  /**
//   * Sets the tipo identificador ada.
//   *
//   * @param tipoIdentificadorAda the new tipo identificador ada
//   */
//  public void setTipoIdentificadorAda(final Long tipoIdentificadorAda) {
//    this.tipoIdentificadorAda = tipoIdentificadorAda;
//  }
//
//  /**
//   * Gets the tipo identificador ref code.
//   *
//   * @return the tipo identificador ref code
//   */
//  public RefCodeDTO getTipoIdentificadorRefCode() {
//    return tipoIdentificadorRefCode;
//  }
//
//  /**
//   * Sets the tipo identificador ref code.
//   *
//   * @param tipoIdentificadorRefCode the new tipo identificador ref code
//   */
//  public void setTipoIdentificadorRefCode(
//      final RefCodeDTO tipoIdentificadorRefCode) {
//    this.tipoIdentificadorRefCode = tipoIdentificadorRefCode;
//  }
//
//  /**
//   * Gets the tipo identificador label.
//   *
//   * @return the tipo identificador label
//   */
//  @Nullable
//  public String getTipoIdentificadorLabel() {
//
//    return tipoIdentificadorRefCode != null
//        ? Labels.getLabel(tipoIdentificadorRefCode.getDescripcionLabel(), "")
//        : null;
//  }
//
//  /**
//   * Gets the nombre completo.
//   *
//   * @return the nombre completo
//   */
//  public String getNombreCompleto() {
//    final String nombre =
//        (StringUtils.isEmpty(getNombre())) ? Constantes.VACIO : getNombre();
//    final String primerApellido =
//        (StringUtils.isEmpty(getApellido1())) ? Constantes.VACIO
//            : getApellido1();
//    final String segundoApellido =
//        (StringUtils.isEmpty(getApellido2())) ? Constantes.VACIO
//            : getApellido2();
//
//    return nombre.concat(Constantes.ESPACIO).concat(primerApellido)
//        .concat(Constantes.ESPACIO).concat(segundoApellido);
//  }
//
//  /**
//   * Gets the direccion habitual.
//   *
//   * @return the direccion habitual
//   */
//  public DireccionAdaDTO getDireccionHabitual() {
//    return direccionHabitual;
//  }
//
//  /**
//   * Sets the direccion habitual.
//   *
//   * @param direccionHabitual the new direccion habitual
//   */
//  public void setDireccionHabitual(final DireccionAdaDTO direccionHabitual) {
//    this.direccionHabitual = direccionHabitual;
//  }
//
//  /**
//   * Compare to.
//   *
//   * @param o the o
//   * @return the int
//   */
//  @Override
//  public int compareTo(final PersonaAdaDTO o) {
//    return 0;
//  }
//
//  /**
//   * Creates builder to build {@link PersonaAdaDTO}.
//   *
//   * @return created builder
//   */
//  public static Builder builder() {
//    return new Builder();
//  }
//
//
//
//  /**
//   * Gets the rol persona.
//   *
//   * @return the rol persona
//   */
//  public String getRolPersona() {
//    return rolPersona;
//  }
//
//  /**
//   * Sets the rol persona.
//   *
//   * @param rolPersona the new rol persona
//   */
//  public void setRolPersona(final String rolPersona) {
//    this.rolPersona = rolPersona;
//  }
//
//  /**
//   * Gets the condition to check.
//   *
//   * @return the condition to check
//   */
//  public Boolean getConditionToCheck() {
//    return conditionToCheck;
//  }
//
//  /**
//   * Sets the condition to check.
//   *
//   * @param conditionToCheck the new condition to check
//   */
//  public void setConditionToCheck(final Boolean conditionToCheck) {
//    this.conditionToCheck = conditionToCheck;
//  }
//
//
//  /**
//   * Gets the edad meses.
//   *
//   * @return the edad meses
//   */
//  public int getEdadMeses() {
//    if (this.getFechaNacimiento() == null) {
//      return -1;// Error Fecha de nacimiento no establecida.
//    }
//    return FechasCalculator
//        .calcularEdadMeses(new Date(this.getFechaNacimiento().getTime())
//            .toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
//
//  }
//
//  /**
//   * Gets the tipo estado civil ada.
//   *
//   * @return the tipo estado civil ada
//   */
//  public Long getTipoEstadoCivilAda() {
//    return tipoEstadoCivilAda;
//  }
//
//  /**
//   * Sets the tipo estado civil ada.
//   *
//   * @param tipoEstadoCivilAda the new tipo estado civil ada
//   */
//  public void setTipoEstadoCivilAda(final Long tipoEstadoCivilAda) {
//    this.tipoEstadoCivilAda = tipoEstadoCivilAda;
//  }
//
//  /**
//   * Gets the fecha nacimiento formateada.
//   *
//   * @return the fecha nacimiento formateada
//   */
//  public String getFechaNacimientoFormateada() {
//    return this.getFechaNacimiento() == null ? ""
//        : new SimpleDateFormat(Utilidades.FORMAT_DATE_FECHA)
//            .format(this.getFechaNacimiento());
//  }
//
//  /**
//   * To string.
//   *
//   * @return the string
//   */
//  @Override
//  public String toString() {
//    return ReflectionToStringBuilder.toString(this);
//  }
//
//  /**
//   * Builder to build {@link PersonaAdaDTO}.
//   */
//  public static final class Builder {
//
//    /** The tipo identificador. */
//    private String tipoIdentificador;
//
//    /** The created by. */
//    private String createdBy;
//
//    /** The creation date. */
//    private Date creationDate;
//
//    /** The last modified by. */
//    private String lastModifiedBy;
//
//    /** The last modified date. */
//    private Date lastModifiedDate;
//
//    /** The pk persona. */
//    private Long pkPersona;
//
//    /** The id intregracion imserso. */
//    private String idIntregracionImserso;
//
//    /** The pendiente envio imserso. */
//    private Boolean pendienteEnvioImserso;
//
//    /** The activo. */
//    private Boolean activo;
//
//    /** The edad. */
//    private Long edad;
//
//    /** The centro salud. */
//    private String centroSalud;
//
//    /** The grupo. */
//    private Long grupo;
//
//    /** The sector. */
//    private String sector;
//
//    /** The seguridad social. */
//    private Boolean seguridadSocial;
//
//    /** The servicio privado. */
//    private Boolean servicioPrivado;
//
//    /** The servicio publico. */
//    private Boolean servicioPublico;
//
//    /** The titular. */
//    private Boolean titular;
//
//    /** The fecha creacion. */
//    private Date fechaCreacion;
//
//    /**
//     * Instantiates a new builder.
//     */
//    private Builder() {}
//
//    /**
//     * With tipo identificador.
//     *
//     * @param tipoIdentificador the tipo identificador
//     * @return the builder
//     */
//    public Builder withTipoIdentificador(final String tipoIdentificador) {
//      this.tipoIdentificador = tipoIdentificador;
//      return this;
//    }
//
//    /**
//     * With created by.
//     *
//     * @param createdBy the created by
//     * @return the builder
//     */
//    public Builder withCreatedBy(final String createdBy) {
//      this.createdBy = createdBy;
//      return this;
//    }
//
//    /**
//     * With creation date.
//     *
//     * @param creationDate the creation date
//     * @return the builder
//     */
//    public Builder withCreationDate(final Date creationDate) {
//      this.creationDate = UtilidadesCommons.cloneDate(creationDate);
//      return this;
//    }
//
//    /**
//     * With last modified by.
//     *
//     * @param lastModifiedBy the last modified by
//     * @return the builder
//     */
//    public Builder withLastModifiedBy(final String lastModifiedBy) {
//      this.lastModifiedBy = lastModifiedBy;
//      return this;
//    }
//
//    /**
//     * With last modified date.
//     *
//     * @param lastModifiedDate the last modified date
//     * @return the builder
//     */
//    public Builder withLastModifiedDate(final Date lastModifiedDate) {
//      this.lastModifiedDate = UtilidadesCommons.cloneDate(lastModifiedDate);
//      return this;
//    }
//
//    /**
//     * With pk persona.
//     *
//     * @param pkPersona the pk persona
//     * @return the builder
//     */
//    public Builder withPkPersona(final Long pkPersona) {
//      this.pkPersona = pkPersona;
//      return this;
//    }
//
//    /**
//     * With id intregracion imserso.
//     *
//     * @param idIntregracionImserso the id intregracion imserso
//     * @return the builder
//     */
//    public Builder withIdIntregracionImserso(
//        final String idIntregracionImserso) {
//      this.idIntregracionImserso = idIntregracionImserso;
//      return this;
//    }
//
//    /**
//     * With pendiente envio imserso.
//     *
//     * @param pendienteEnvioImserso the pendiente envio imserso
//     * @return the builder
//     */
//    public Builder withPendienteEnvioImserso(
//        final Boolean pendienteEnvioImserso) {
//      this.pendienteEnvioImserso = pendienteEnvioImserso;
//      return this;
//    }
//
//    /**
//     * With activo.
//     *
//     * @param activo the activo
//     * @return the builder
//     */
//    public Builder withActivo(final Boolean activo) {
//      this.activo = activo;
//      return this;
//    }
//
//    /**
//     * With edad.
//     *
//     * @param edad the edad
//     * @return the builder
//     */
//    public Builder withEdad(final Long edad) {
//      this.edad = edad;
//      return this;
//    }
//
//    /**
//     * With centro salud.
//     *
//     * @param centroSalud the centro salud
//     * @return the builder
//     */
//    public Builder withCentroSalud(final String centroSalud) {
//      this.centroSalud = centroSalud;
//      return this;
//    }
//
//    /**
//     * With grupo.
//     *
//     * @param grupo the grupo
//     * @return the builder
//     */
//    public Builder withGrupo(final Long grupo) {
//      this.grupo = grupo;
//      return this;
//    }
//
//    /**
//     * With sector.
//     *
//     * @param sector the sector
//     * @return the builder
//     */
//    public Builder withSector(final String sector) {
//      this.sector = sector;
//      return this;
//    }
//
//    /**
//     * With seguridad social.
//     *
//     * @param seguridadSocial the seguridad social
//     * @return the builder
//     */
//    public Builder withSeguridadSocial(final Boolean seguridadSocial) {
//      this.seguridadSocial = seguridadSocial;
//      return this;
//    }
//
//    /**
//     * With servicio privado.
//     *
//     * @param servicioPrivado the servicio privado
//     * @return the builder
//     */
//    public Builder withServicioPrivado(final Boolean servicioPrivado) {
//      this.servicioPrivado = servicioPrivado;
//      return this;
//    }
//
//    /**
//     * With servicio publico.
//     *
//     * @param servicioPublico the servicio publico
//     * @return the builder
//     */
//    public Builder withServicioPublico(final Boolean servicioPublico) {
//      this.servicioPublico = servicioPublico;
//      return this;
//    }
//
//    /**
//     * With titular.
//     *
//     * @param titular the titular
//     * @return the builder
//     */
//    public Builder withTitular(final Boolean titular) {
//      this.titular = titular;
//      return this;
//    }
//
//    /**
//     * With fecha creacion.
//     *
//     * @param fechaCreacion the fecha creacion
//     * @return the builder
//     */
//    public Builder withFechaCreacion(final Date fechaCreacion) {
//      this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
//      return this;
//    }
//
//    /**
//     * Builds the.
//     *
//     * @return the persona ada DTO
//     */
//    public PersonaAdaDTO build() {
//      return new PersonaAdaDTO(this);
//    }
//  }
//
//}
