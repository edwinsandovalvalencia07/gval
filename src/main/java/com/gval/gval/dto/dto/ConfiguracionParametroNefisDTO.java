package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * The Class ConfiguracionParametrosNefisDTO.
 */
public class ConfiguracionParametroNefisDTO extends BaseDTO
    implements Comparable<ConfiguracionParametroNefisDTO> {


  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -996252934487129135L;

  /** The id. */
  private Long idParam;

  /** The codigo Padre. */
  private String parametro;

  /** The codigo. */
  private String valor;

  /** The descripcion. */
  private String descripcion;

  /** The valor caracter. */
  private String aplicacion;

  /** The valor caracter. */
  private String editable;

  /**
   * Instantiates a new configuracion parametros nefis DTO.
   */
  public ConfiguracionParametroNefisDTO() {
    super();
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final ConfiguracionParametroNefisDTO o) {
    return 0;
  }

  /**
   * Gets the id.
   *
   * @return the id
   */
  public Long getIdParam() {
    return idParam;
  }

  /**
   * Sets the id.
   *
   * @param idParam the new id param
   */
  public void setIdParam(final Long idParam) {
    this.idParam = idParam;
  }

  /**
   * Gets the parametro.
   *
   * @return the parametro
   */
  public String getParametro() {
    return parametro;
  }

  /**
   * Sets the parametro.
   *
   * @param parametro the parametro to set
   */
  public void setParametro(final String parametro) {
    this.parametro = parametro;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

  /**
   * Sets the valor.
   *
   * @param valor the valor to set
   */
  public void setValor(final String valor) {
    this.valor = valor;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the descripcion to set
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the aplicacion.
   *
   * @return the aplicacion
   */
  public String getAplicacion() {
    return aplicacion;
  }

  /**
   * Sets the aplicacion.
   *
   * @param aplicacion the aplicacion to set
   */
  public void setAplicacion(final String aplicacion) {
    this.aplicacion = aplicacion;
  }

  /**
   * Gets the editable.
   *
   * @return the editable
   */
  public String getEditable() {
    return editable;
  }

  /**
   * Sets the editable.
   *
   * @param editable the editable to set
   */
  public void setEditable(final String editable) {
    this.editable = editable;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
