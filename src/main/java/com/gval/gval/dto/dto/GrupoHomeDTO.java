package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import es.gva.dependencia.ada.common.enums.BotonesHomeEnum;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.zkoss.util.resource.Labels;

import java.util.List;
import java.util.stream.Collectors;


/**
 * The Class GrupoHomeDTO.
 */
public class GrupoHomeDTO extends BaseDTO implements Comparable<ContadorDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The nombre grupo. */
  private String labelGrupo;

  /** The botones. */
  private List<BotonesHomeEnum> botones;



  /**
   * Instantiates a new grupo home DTO.
   *
   * @param nombreGrupo the nombre grupo
   * @param botones the botones
   */
  public GrupoHomeDTO(final String nombreGrupo,
      final List<BotonesHomeEnum> botones) {
    super();
    this.labelGrupo = nombreGrupo;
    this.botones = UtilidadesCommons.collectorsToList(botones);
  }

  /**
   * Gets the label grupo.
   *
   * @return the label grupo
   */
  public String getLabelGrupo() {
    return labelGrupo;
  }


  /**
   * Sets the label grupo.
   *
   * @param labelGrupo the new label grupo
   */
  public void setLabelGrupo(final String labelGrupo) {
    this.labelGrupo = labelGrupo;
  }


  /**
   * Gets the botones.
   *
   * @return the botones
   */
  public List<BotonesHomeEnum> getBotones() {
    return UtilidadesCommons.collectorsToList(botones);
  }


  /**
   * Sets the botones.
   *
   * @param botones the new botones
   */
  public void setBotones(final List<BotonesHomeEnum> botones) {
    this.botones = UtilidadesCommons.collectorsToList(botones);
  }

  /**
   * Gets the botones filtrados.
   *
   * @param filtroBuscar the filtro buscar
   * @return the botones filtrados
   */
  public List<BotonesHomeEnum> getBotonesFiltrados(final String filtroBuscar) {
    return getBotones().stream()
        .filter(btn -> UtilidadesCommons.containsIgnoreCaseAndAccents(
            Labels.getLabel(btn.getLabel()), StringUtils.trim(filtroBuscar)))
        .collect(Collectors.toList());
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }


  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final ContadorDTO o) {
    return 0;
  }
}
