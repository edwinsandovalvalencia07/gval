package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;


/**
 * The Class ConfiguracionDTO.
 */
public class ConfiguracionDTO extends BaseDTO
    implements Comparable<ConfiguracionDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 5922945813575900882L;

  /** The pk configuracion. */
  private Long pkConfiguracion;

  /** The codigo Padre. */
  private String codigoPadre;

  /** The codigo. */
  private String codigo;

  /** The descripcion. */
  private String descripcion;

  /** The valor caracter. */
  private String valorCaracter;

  /** The valor numerico. */
  private Long valorNumerico;

  /** The Label */
  private String label;

  /**
   * Instantiates a new configuracion DTO.
   */
  public ConfiguracionDTO() {
    super();
  }

  /**
   * Gets the pk configuracion.
   *
   * @return the pk configuracion
   */
  public Long getPkConfiguracion() {
    return pkConfiguracion;
  }

  /**
   * Sets the pk configuracion.
   *
   * @param pkConfiguracion the new pk configuracion
   */
  public void setPkConfiguracion(final Long pkConfiguracion) {
    this.pkConfiguracion = pkConfiguracion;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the valor caracter.
   *
   * @return the valor caracter
   */
  public String getValorCaracter() {
    return valorCaracter;
  }

  /**
   * Sets the valor caracter.
   *
   * @param valorCaracter the new valor caracter
   */
  public void setValorCaracter(final String valorCaracter) {
    this.valorCaracter = valorCaracter;
  }

  /**
   * Gets the valor numerico.
   *
   * @return the valor numerico
   */
  public Long getValorNumerico() {
    return valorNumerico;
  }

  /**
   * Sets the valor numerico.
   *
   * @param valorNumerico the new valor numerico
   */
  public void setValorNumerico(final Long valorNumerico) {
    this.valorNumerico = valorNumerico;
  }

  /**
   * Compare to.
   *
   * @param arg0 the arg 0
   * @return the int
   */
  @Override
  public int compareTo(final ConfiguracionDTO arg0) {
    return 0;
  }


  /**
   * @return the codigoPadre
   */
  public String getCodigoPadre() {
    return codigoPadre;
  }

  /**
   * @param codigoPadre the codigoPadre to set
   */
  public void setCodigoPadre(final String codigoPadre) {
    this.codigoPadre = codigoPadre;
  }

  /**
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * @param label the label to set
   */
  public void setLabel(final String label) {
    this.label = label;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
