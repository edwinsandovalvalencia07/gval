package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;


/**
 * The Class ZonaCoberturaDTO.
 */
public class ZonaCoberturaDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 2654023991322120069L;

  /** The pk zona cobertura. */
  private Long pkZonaCobertura;

  /** The activo. */
  private Boolean activo;

  /** The ayuntamiento. */
  private Boolean ayuntamiento;

  /** The codigo. */
  private String codigo;

  /** The nombre. */
  private String nombre;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /**
   * Instantiates a new zona cobertura DTO.
   */
  public ZonaCoberturaDTO() {
    super();
  }

  /**
   * Instantiates a new zona cobertura DTO.
   *
   * @param pkZonaCobertura the pk zona cobertura
   * @param activo the activo
   * @param ayuntamiento the ayuntamiento
   * @param codigo the codigo
   * @param nombre the nombre
   * @param fechaCreacion the fecha creacion
   */
  public ZonaCoberturaDTO(final Long pkZonaCobertura, final Boolean activo,
      final Boolean ayuntamiento, final String codigo, final String nombre,
      final Date fechaCreacion) {
    super();
    this.pkZonaCobertura = pkZonaCobertura;
    this.activo = activo;
    this.ayuntamiento = ayuntamiento;
    this.codigo = codigo;
    this.nombre = nombre;
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the pk zona cobertura.
   *
   * @return the pk zona cobertura
   */
  public Long getPkZonaCobertura() {
    return pkZonaCobertura;
  }

  /**
   * Sets the pk zona cobertura.
   *
   * @param pkZonaCobertura the new pk zona cobertura
   */
  public void setPkZonaCobertura(final Long pkZonaCobertura) {
    this.pkZonaCobertura = pkZonaCobertura;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the ayuntamiento.
   *
   * @return the ayuntamiento
   */
  public Boolean getAyuntamiento() {
    return ayuntamiento;
  }

  /**
   * Sets the ayuntamiento.
   *
   * @param ayuntamiento the new ayuntamiento
   */
  public void setAyuntamiento(final Boolean ayuntamiento) {
    this.ayuntamiento = ayuntamiento;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

}
