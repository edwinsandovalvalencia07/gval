package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;

/**
 * The Class RegistroFallecidoDTO.
 */
public class RegistroFallecidoDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -4753084801630031070L;

  /** The pk registro fallecido. */
  private Long pkRegistroFallecido;

  /** The expediente. */
  private ExpedienteDTO expediente;

  /** The usuario. */
  private UsuarioDTO usuario;

  /** The fecha registro. */
  private Date fechaRegistro;

  /** The fecha fallecimiento. */
  private Date fechaFallecimiento;

  /** The indica fallecimiento. */
  private Boolean indicaFallecimiento;

  /**
   * Instantiates a new registro fallecido.
   */
  public RegistroFallecidoDTO() {
    super();
  }

  /**
   * Gets the pk registro fallecido.
   *
   * @return the pk registro fallecido
   */
  public Long getPkRegistroFallecido() {
    return pkRegistroFallecido;
  }

  /**
   * Sets the pk registro fallecido.
   *
   * @param pkRegistroFallecido the new pk registro fallecido
   */
  public void setPkRegistroFallecido(final Long pkRegistroFallecido) {
    this.pkRegistroFallecido = pkRegistroFallecido;
  }

  /**
   * Gets the expediente.
   *
   * @return the expediente
   */
  public ExpedienteDTO getExpediente() {
    return expediente;
  }

  /**
   * Sets the expediente.
   *
   * @param expediente the new expediente
   */
  public void setExpediente(final ExpedienteDTO expediente) {
    this.expediente = expediente;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the fecha registro.
   *
   * @return the fecha registro
   */
  public Date getFechaRegistro() {
    return UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Sets the fecha registro.
   *
   * @param fechaRegistro the new fecha registro
   */
  public void setFechaRegistro(final Date fechaRegistro) {
    this.fechaRegistro = UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Gets the fecha fallecimiento.
   *
   * @return the fecha fallecimiento
   */
  public Date getFechaFallecimiento() {
    return UtilidadesCommons.cloneDate(fechaFallecimiento);
  }

  /**
   * Sets the fecha fallecimiento.
   *
   * @param fechaFallecimiento the new fecha fallecimiento
   */
  public void setFechaFallecimiento(final Date fechaFallecimiento) {
    this.fechaFallecimiento = UtilidadesCommons.cloneDate(fechaFallecimiento);
  }

  /**
   * Gets the indica fallecimiento.
   *
   * @return the indica fallecimiento
   */
  public Boolean getIndicaFallecimiento() {
    return indicaFallecimiento;
  }

  /**
   * Sets the indica fallecimiento.
   *
   * @param indicaFallecimiento the new indica fallecimiento
   */
  public void setIndicaFallecimiento(final Boolean indicaFallecimiento) {
    this.indicaFallecimiento = indicaFallecimiento;
  }

}
