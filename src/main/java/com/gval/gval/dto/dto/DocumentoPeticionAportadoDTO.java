package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;


/**
 * The Class DocumentoPeticionAportadoDTO.
 */
public class DocumentoPeticionAportadoDTO extends BaseDTO
    implements Comparable<DocumentoPeticionAportadoDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -2280839374914897896L;

  /** The pk documento peticion aportado. */
  private Long pkDocumentoPeticionAportado;

  /** The activo. */
  private Boolean activo;

  /** The doentregad. */
  private Date fechaEntrega;

  /** The documento. */
  private DocumentoDTO documento;

  /** The documento peticion. */
  private DocumentoPeticionDTO documentoPeticion;

  /** The usuario. */
  private UsuarioDTO usuario;

  /** The tipo documento. */
  private TipoDocumentoDTO tipoDocumento;

  /**
   * Instantiates a new documento peticion aportado DTO.
   */
  public DocumentoPeticionAportadoDTO() {
    super();
  }

  /**
   * Gets the pk documento peticion aportado.
   *
   * @return the pk documento peticion aportado
   */
  public Long getPkDocumentoPeticionAportado() {
    return pkDocumentoPeticionAportado;
  }

  /**
   * Sets the pk documento peticion aportado.
   *
   * @param pkDocumentoPeticionAportado the new pk documento peticion aportado
   */
  public void setPkDocumentoPeticionAportado(
      final Long pkDocumentoPeticionAportado) {
    this.pkDocumentoPeticionAportado = pkDocumentoPeticionAportado;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the doentregad.
   *
   * @return the doentregad
   */
  public Date getFechaEntrega() {
    return UtilidadesCommons.cloneDate(fechaEntrega);
  }

  /**
   * Sets the doentregad.
   *
   * @param fechaEntrega the new doentregad
   */
  public void setFechaEntrega(final Date fechaEntrega) {
    this.fechaEntrega = UtilidadesCommons.cloneDate(fechaEntrega);
  }

  /**
   * Gets the documento.
   *
   * @return the documento
   */
  public DocumentoDTO getDocumento() {
    return documento;
  }

  /**
   * Sets the documento.
   *
   * @param documento the new documento
   */
  public void setDocumento(final DocumentoDTO documento) {
    this.documento = documento;
  }

  /**
   * Gets the documento peticion.
   *
   * @return the documento peticion
   */
  public DocumentoPeticionDTO getDocumentoPeticion() {
    return documentoPeticion;
  }

  /**
   * Sets the documento peticion.
   *
   * @param documentoPeticion the new documento peticion
   */
  public void setDocumentoPeticion(
      final DocumentoPeticionDTO documentoPeticion) {
    this.documentoPeticion = documentoPeticion;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the tipo documento.
   *
   * @return the tipo documento
   */
  public TipoDocumentoDTO getTipoDocumento() {
    return tipoDocumento;
  }

  /**
   * Sets the tipo documento.
   *
   * @param tipoDocumento the new tipo documento
   */
  public void setTipoDocumento(final TipoDocumentoDTO tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final DocumentoPeticionAportadoDTO o) {
    return 0;
  }
}
