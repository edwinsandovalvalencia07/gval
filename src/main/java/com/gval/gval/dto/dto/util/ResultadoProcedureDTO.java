package com.gval.gval.dto.dto.util;


import com.gval.gval.entity.model.Constantes;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.util.ObjectUtils;

import java.io.Serializable;

/**
 * The Class ResultadoProcedureDTO.
 */

public class ResultadoProcedureDTO implements Serializable {

  /** The Constant RESULTADO_PL_CORRECTO. */
  public static final String RESULTADO_PL_CORRECTO = "0";

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The codigo error. */
  private String codigoError;

  /** The error detalle. */
  private String errorDetalle;

  /** The error detalle. */
  private String errorDetalleProperties;

  /** The resultado adicional. */
  private String resultadoAdicional;

  /**
   * Instantiates a new resultado procedure DTO.
   */
  public ResultadoProcedureDTO() {
    // ResultadoProcedure constructor
  }


  /**
   * Instantiates a new resultado procedure DTO.
   *
   * @param codigoError the codigo error
   * @param errorDetalle the error detalle
   * @param errorDetalleProperties the error detalle properties
   * @param resultadoAdicional the resultado adicional
   */
  public ResultadoProcedureDTO(final String codigoError,
      final String errorDetalle, final String errorDetalleProperties,
      final String resultadoAdicional) {
    super();
    this.codigoError = codigoError;
    this.errorDetalle = errorDetalle;
    this.errorDetalleProperties = errorDetalleProperties;
    this.resultadoAdicional = resultadoAdicional;
  }

  /**
   * Instantiates a new resultado procedure DTO.
   *
   * @param codigoError the codigo error
   * @param errorDetalle the error detalle
   * @param errorDetalleProperties the error detalle properties
   */
  public ResultadoProcedureDTO(final String codigoError,
      final String errorDetalle, final String errorDetalleProperties) {
    this(codigoError, errorDetalle, errorDetalleProperties, StringUtils.EMPTY);
  }


  /**
   * Instantiates a new resultado procedure DTO.
   *
   * @param codigoError the codigo error
   * @param errorDetalle the error detalle
   * @param errorDetalleProperties the error detalle properties
   */
  public ResultadoProcedureDTO(final Object codigoError,
      final Object errorDetalle, final Object errorDetalleProperties) {
    this(ObjectUtils.nullSafeToString(codigoError),
        ObjectUtils.nullSafeToString(errorDetalle),
        ObjectUtils.nullSafeToString(errorDetalleProperties),
        StringUtils.EMPTY);
  }

  /**
   * Gets the codigo error.
   *
   * @return the codigo error
   */
  public String getCodigoError() {
    return codigoError;
  }

  /**
   * Sets the codigo error.
   *
   * @param codigoError the new codigo error
   */
  public void setCodigoError(final String codigoError) {
    this.codigoError = codigoError;
  }

  /**
   * Gets the error detalle.
   *
   * @return the error detalle
   */
  public String getErrorDetalle() {
    return errorDetalle;
  }

  /**
   * Sets the error detalle.
   *
   * @param errorDetalle the new error detalle
   */
  public void setErrorDetalle(final String errorDetalle) {
    this.errorDetalle = errorDetalle;
  }

  /**
   * Gets the resultado adicional.
   *
   * @return the resultado adicional
   */
  public String getResultadoAdicional() {
    return resultadoAdicional;
  }

  /**
   * Sets the resultado adicional.
   *
   * @param resultadoAdicional the new resultado adicional
   */
  public void setResultadoAdicional(final String resultadoAdicional) {
    this.resultadoAdicional = resultadoAdicional;
  }

  /**
   * Gets the error detalle properties.
   *
   * @return the error detalle properties
   */
  public String getErrorDetalleProperties() {
    return StringUtils.defaultString(errorDetalleProperties, StringUtils.EMPTY);
  }

  /**
   * Sets the error detalle properties.
   *
   * @param errorDetalleProperties the new error detalle properties
   */
  public void setErrorDetalleProperties(final String errorDetalleProperties) {
    this.errorDetalleProperties = errorDetalleProperties;
  }

  /**
   * Checkea si la respuesta es correcta, .
   *
   * @return true, if is ok
   */
  public boolean isOk() {
    return StringUtils.equals(this.getCodigoError(), RESULTADO_PL_CORRECTO);
  }

  /**
   * Gets the i18, traduccion de las llamadas PL a properties de zk.
   *
   * @return the i18
   */
  public String getI18() {
    final String mensajeMasParamsI18[] = this.getErrorDetalleProperties()
        .split(PlSqlExceptionDTO.SEPARADOR_PARAMETROS);
    if (mensajeMasParamsI18.length > PlSqlExceptionDTO.POSICICION_MENSAJE_MULTIIDIOMA) {
      return new StringBuilder().append(PlSqlExceptionDTO.DETALLE_I18).append(
          mensajeMasParamsI18[PlSqlExceptionDTO.POSICICION_MENSAJE_MULTIIDIOMA])
          .toString();
    }
    return PlSqlExceptionDTO.DEFAULT_ERROR_EXCEPTION;
  }


  /**
   * Gets the i 18 sin prefijo.
   *
   * @return the i 18 sin prefijo
   */
  public String getI18SinPrefijo() {
    final String mensajeMasParamsI18[] = this.getErrorDetalleProperties()
        .split(PlSqlExceptionDTO.SEPARADOR_PARAMETROS);
    if (mensajeMasParamsI18.length > PlSqlExceptionDTO.POSICICION_MENSAJE_MULTIIDIOMA) {
      return new StringBuilder().append(
          mensajeMasParamsI18[PlSqlExceptionDTO.POSICICION_MENSAJE_MULTIIDIOMA])
          .toString();
    }
    return PlSqlExceptionDTO.DEFAULT_ERROR_EXCEPTION;
  }

  /**
   * Gets the args, trae los argumentos de las llamadas plsql.
   *
   * @return the args
   */
  public String[] getArgs() {
    String mensajeMasParams[] = this.getErrorDetalleProperties()
        .split(PlSqlExceptionDTO.SEPARADOR_PARAMETROS);

    if (mensajeMasParams.length > PlSqlExceptionDTO.POSICICION_MENSAJE_MULTIIDIOMA_PARAMETROS_A_PARTIR) {
      mensajeMasParams = ArrayUtils.remove(mensajeMasParams,
          PlSqlExceptionDTO.POSICICION_MENSAJE_MULTIIDIOMA);
    }
    return mensajeMasParams;
  }

  /**
   * Gets the log message.
   *
   * @return the log message
   */
  public String getLogMessage() {
    return new StringBuilder().append(codigoError).append(Constantes.ESPACIO)
        .append(errorDetalle).toString();
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }



}
