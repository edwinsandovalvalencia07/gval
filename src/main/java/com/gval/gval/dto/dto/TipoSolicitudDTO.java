package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.validator.constraints.Length;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * The Class TipoSolicitudDTO.
 */
public class TipoSolicitudDTO extends BaseDTO
    implements Comparable<TipoSolicitudDTO> {


  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1397400429301532090L;

  /** The pk tipo solicitud. */
  @NotNull
  private Long pkTipoSolicitud;

  /** The codigo. */
  @NotNull
  @Length(max = 2)
  private String codigo;

  /** The fecha creacion. */
  @NotNull
  private Date fechaCreacion;

  /** The nombre. */
  @NotNull
  @Length(max = 50)
  private String nombre;

  /** The activo. */
  @NotNull
  private Boolean activo;

  /** The solicitudes. */
  @JsonBackReference
  @JsonIgnore
  private List<SolicitudDTO> solicitudes;


  /**
   * Gets the pk tipo solicitud.
   *
   * @return the pk tipo solicitud
   */
  public Long getPkTipoSolicitud() {
    return pkTipoSolicitud;
  }

  /**
   * Sets the pk tipo solicitud.
   *
   * @param pkTipoSolicitud the new pk tipo solicitud
   */
  public void setPkTipoSolicitud(final Long pkTipoSolicitud) {
    this.pkTipoSolicitud = pkTipoSolicitud;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public List<SolicitudDTO> getSolicitud() {
    return UtilidadesCommons.collectorsToList(solicitudes);
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitudes the new solicitud
   */
  public void setSolicitud(final List<SolicitudDTO> solicitudes) {
    this.solicitudes = UtilidadesCommons.collectorsToList(solicitudes);
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final TipoSolicitudDTO o) {
    return 0;
  }

}
