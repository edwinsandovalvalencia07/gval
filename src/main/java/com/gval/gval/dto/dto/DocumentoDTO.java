package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;
import java.util.List;

import jakarta.annotation.Nullable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * Tiene la información de un documento.
 *
 */
public final class DocumentoDTO extends BaseDTO
implements Comparable<DocumentoDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 8618890747066892952L;

  /** The pk documento. */
  private Long pkDocumento;

  /** The csv. */
  @Size(max = 1000)
  private String csv;

  /** The activo. */
  @NotNull
  private Boolean activo;

  /** The correos acuse 1. */
  @Size(max = 50)
  private String correosAcuse1;

  /** The correos acuse 2. */
  @Size(max = 50)
  private String correosAcuse2;

  /** The descripcion registro. */
  @Size(max = 100, message = "error.documento.descripcionregistro.longitud")
  private String descripcionRegistro;

  /** The enviado acuse 1. */
  @Size(max = 1)
  private String enviadoAcuse1;

  /** The enviado acuse 2. */
  @Size(max = 1000)
  private String enviadoAcuse2;

  /** The fecha acuse 1. */
  private Date fechaAcuse1;

  /** The fecha acuse 2. */
  private Date fechaAcuse2;

  /** The fecha creacion. */
  @NotNull
  private Date fechaCreacion;

  /** The fecha registro. */
  private Date fechaRegistro;

  /** The fichero. */
  private byte[] fichero;

  /** The fichero. */
  private byte[] ficheroGde;

  /** The nombre. */
  @Size(max = 250, message = "error.documento.nombre.longitud")
  private String nombre;

  /** The nuacre acuse 1. */
  @Size(max = 50)
  private String nuacreAcuse1;

  /** The nuacre acuse 2. */
  @Size(max = 50)
  private String nuacreAcuse2;

  /** The numero registro. */
  @Size(max = 20, message = "error.documento.numeroregistro.longitud")
  private String numeroRegistro;

  /** The tipo acuse 1. */
  @Size(max = 2)
  private String tipoAcuse1;

  /** The tipo acuse 2. */
  @Size(max = 2)
  private String tipoAcuse2;

  /** The uuid. */
  @Size(max = 100)
  private String uuid;

  /** The clave consulta. */
  @Size(max = 4000)
  private String claveConsulta;

  /** The error sincronizacion. */
  @NotNull
  private Boolean errorSincronizacion;

  /** The expediente. */
  // bi-directional many-to-one association to SdmExpedien
  private ExpedienteDTO expediente;

  /** The solicitud. */
  // bi-directional many-to-one association to SdmSolicit
  private SolicitudDTO solicitud;

  /** The tipo documento. */
  // bi-directional many-to-one association to SdxTipodocu
  @NotNull(message = "error.documento.tipo_documento")
  private TipoDocumentoDTO tipoDocumento;

  /** The unidad. */
  // bi-directional many-to-one association to SdxUnidad
  private UnidadDTO unidad;

  /** The estados. */
  private List<HistoricoEstadoDocumentoDTO> estados;

  /** The documento generado. */
  private DocumentoGeneradoDTO documentoGenerado;

  /** The fecha publicacion BOE. */
  private Date fechaPublicacionBOE;

  /** The numero BOE. */
  private String numeroBOE;

  /** The dofechimp. */
  private Date fechaImpresion;

  /** The usuario. */
  private UsuarioDTO usuarioImpresion;

  /**
   * Constructor. Inicializa las variables marcadas como no nulas
   */
  public DocumentoDTO() {
    super();
    this.activo = Boolean.TRUE;
    this.errorSincronizacion = Boolean.FALSE;
    this.fechaCreacion = new Date();
    this.tipoDocumento = new TipoDocumentoDTO();
  }

  /**
   * Instantiates a new documento DTO.
   *
   * @param solicitudDTO the solicitud DTO
   */
  public DocumentoDTO(final SolicitudDTO solicitudDTO) {
    this();
    this.solicitud = solicitudDTO;
    this.expediente = solicitudDTO.getExpediente();
  }


  /**
   * Gets the pk documento.
   *
   * @return the pkDocumento
   */
  public Long getPkDocumento() {
    return pkDocumento;
  }

  /**
   * Sets the pk documento.
   *
   * @param pkDocumento the pkDocumento to set
   */
  public void setPkDocumento(final Long pkDocumento) {
    this.pkDocumento = pkDocumento;
  }

  /**
   * Gets the csv.
   *
   * @return the csv
   */
  public String getCsv() {
    return csv;
  }

  /**
   * Sets the csv.
   *
   * @param csv the csv to set
   */
  public void setCsv(final String csv) {
    this.csv = csv;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the correos acuse 1.
   *
   * @return the correosAcuse1
   */
  public String getCorreosAcuse1() {
    return correosAcuse1;
  }

  /**
   * Sets the correos acuse 1.
   *
   * @param correosAcuse1 the correosAcuse1 to set
   */
  public void setCorreosAcuse1(final String correosAcuse1) {
    this.correosAcuse1 = correosAcuse1;
  }

  /**
   * Gets the correos acuse 2.
   *
   * @return the correosAcuse2
   */
  public String getCorreosAcuse2() {
    return correosAcuse2;
  }

  /**
   * Sets the correos acuse 2.
   *
   * @param correosAcuse2 the correosAcuse2 to set
   */
  public void setCorreosAcuse2(final String correosAcuse2) {
    this.correosAcuse2 = correosAcuse2;
  }

  /**
   * Gets the descripcion registro.
   *
   * @return the descripcionRegistro
   */
  public String getDescripcionRegistro() {
    return descripcionRegistro;
  }

  /**
   * Sets the descripcion registro.
   *
   * @param descripcionRegistro the descripcionRegistro to set
   */
  public void setDescripcionRegistro(final String descripcionRegistro) {
    this.descripcionRegistro = descripcionRegistro;
  }

  /**
   * Gets the enviado acuse 1.
   *
   * @return the enviadoAcuse1
   */
  public String getEnviadoAcuse1() {
    return enviadoAcuse1;
  }

  /**
   * Sets the enviado acuse 1.
   *
   * @param enviadoAcuse1 the enviadoAcuse1 to set
   */
  public void setEnviadoAcuse1(final String enviadoAcuse1) {
    this.enviadoAcuse1 = enviadoAcuse1;
  }

  /**
   * Gets the enviado acuse 2.
   *
   * @return the enviadoAcuse2
   */
  public String getEnviadoAcuse2() {
    return enviadoAcuse2;
  }

  /**
   * Sets the enviado acuse 2.
   *
   * @param enviadoAcuse2 the enviadoAcuse2 to set
   */
  public void setEnviadoAcuse2(final String enviadoAcuse2) {
    this.enviadoAcuse2 = enviadoAcuse2;
  }

  /**
   * Gets the fecha acuse 1.
   *
   * @return the fechaAcuse1
   */
  public Date getFechaAcuse1() {
    return UtilidadesCommons.cloneDate(fechaAcuse1);
  }

  /**
   * Sets the fecha acuse 1.
   *
   * @param fechaAcuse1 the fechaAcuse1 to set
   */
  public void setFechaAcuse1(final Date fechaAcuse1) {
    this.fechaAcuse1 = UtilidadesCommons.cloneDate(fechaAcuse1);
  }

  /**
   * Gets the fecha acuse 2.
   *
   * @return the fechaAcuse2
   */
  public Date getFechaAcuse2() {
    return UtilidadesCommons.cloneDate(fechaAcuse2);
  }

  /**
   * Sets the fecha acuse 2.
   *
   * @param fechaAcuse2 the fechaAcuse2 to set
   */
  public void setFechaAcuse2(final Date fechaAcuse2) {
    this.fechaAcuse2 = UtilidadesCommons.cloneDate(fechaAcuse2);
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha registro.
   *
   * @return the fechaRegistro
   */
  public Date getFechaRegistro() {
    return UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Sets the fecha registro.
   *
   * @param fechaRegistro the fechaRegistro to set
   */
  public void setFechaRegistro(final Date fechaRegistro) {
    this.fechaRegistro = UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Gets the fichero.
   *
   * @return the fichero
   */
  public byte[] getFichero() {
    return UtilidadesCommons.cloneBytes(fichero);
  }

  /**
   * Sets the fichero.
   *
   * @param fichero the fichero to set
   */
  public void setFichero(final byte[] fichero) {
    this.fichero = UtilidadesCommons.cloneBytes(fichero);
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the nombre to set
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the nuacre acuse 1.
   *
   * @return the nuacreAcuse1
   */
  public String getNuacreAcuse1() {
    return nuacreAcuse1;
  }

  /**
   * Sets the nuacre acuse 1.
   *
   * @param nuacreAcuse1 the nuacreAcuse1 to set
   */
  public void setNuacreAcuse1(final String nuacreAcuse1) {
    this.nuacreAcuse1 = nuacreAcuse1;
  }

  /**
   * Gets the nuacre acuse 2.
   *
   * @return the nuacreAcuse2
   */
  public String getNuacreAcuse2() {
    return nuacreAcuse2;
  }

  /**
   * Sets the nuacre acuse 2.
   *
   * @param nuacreAcuse2 the nuacreAcuse2 to set
   */
  public void setNuacreAcuse2(final String nuacreAcuse2) {
    this.nuacreAcuse2 = nuacreAcuse2;
  }

  /**
   * Gets the numero registro.
   *
   * @return the numeroRegistro
   */
  public String getNumeroRegistro() {
    return numeroRegistro;
  }

  /**
   * Sets the numero registro.
   *
   * @param numeroRegistro the numeroRegistro to set
   */
  public void setNumeroRegistro(final String numeroRegistro) {
    this.numeroRegistro = numeroRegistro;
  }

  /**
   * Gets the tipo acuse 1.
   *
   * @return the tipoAcuse1
   */
  public String getTipoAcuse1() {
    return tipoAcuse1;
  }

  /**
   * Sets the tipo acuse 1.
   *
   * @param tipoAcuse1 the tipoAcuse1 to set
   */
  public void setTipoAcuse1(final String tipoAcuse1) {
    this.tipoAcuse1 = tipoAcuse1;
  }

  /**
   * Gets the tipo acuse 2.
   *
   * @return the tipoAcuse2
   */
  public String getTipoAcuse2() {
    return tipoAcuse2;
  }

  /**
   * Sets the tipo acuse 2.
   *
   * @param tipoAcuse2 the tipoAcuse2 to set
   */
  public void setTipoAcuse2(final String tipoAcuse2) {
    this.tipoAcuse2 = tipoAcuse2;
  }

  /**
   * Gets the uuid.
   *
   * @return the uuid
   */
  public String getUuid() {
    return uuid;
  }


  /**
   * Sets the uuid.
   *
   * @param uuid the new uuid
   */
  public void setUuid(final String uuid) {
    this.uuid = uuid;
  }

  /**
   * Gets the clave consulta.
   *
   * @return the clave consulta
   */
  public String getClaveConsulta() {
    return claveConsulta;
  }

  /**
   * Sets the clave consulta.
   *
   * @param claveConsulta the new clave consulta
   */
  public void setClaveConsulta(final String claveConsulta) {
    this.claveConsulta = claveConsulta;
  }

  /**
   * Gets the error sincronizacion.
   *
   * @return the error sincronizacion
   */
  public Boolean getErrorSincronizacion() {
    return errorSincronizacion;
  }

  /**
   * Sets the error sincronizacion.
   *
   * @param errorSincronizacion the new error sincronizacion
   */
  public void setErrorSincronizacion(final Boolean errorSincronizacion) {
    this.errorSincronizacion = errorSincronizacion;
  }

  /**
   * Gets the expediente.
   *
   * @return the expediente
   */
  public ExpedienteDTO getExpediente() {
    return expediente;
  }

  /**
   * Sets the expediente.
   *
   * @param expediente the expediente to set
   */
  public void setExpediente(final ExpedienteDTO expediente) {
    this.expediente = expediente;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public SolicitudDTO getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the solicitud to set
   */
  public void setSolicitud(final SolicitudDTO solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the tipo documento.
   *
   * @return the tipoDocumento
   */
  public TipoDocumentoDTO getTipoDocumento() {
    return tipoDocumento;
  }

  /**
   * Sets the tipo documento.
   *
   * @param tipoDocumento the tipoDocumento to set
   */
  public void setTipoDocumento(final TipoDocumentoDTO tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }

  /**
   * Gets the unidad.
   *
   * @return the unidad
   */
  public UnidadDTO getUnidad() {
    return unidad;
  }

  /**
   * Sets the unidad.
   *
   * @param unidad the unidad to set
   */
  public void setUnidad(final UnidadDTO unidad) {
    this.unidad = unidad;
  }


  /**
   * Gets the estados.
   *
   * @return the estados
   */
  public List<HistoricoEstadoDocumentoDTO> getEstados() {
    return UtilidadesCommons.collectorsToList(estados);
  }

  /**
   * Sets the estados.
   *
   * @param estados the new estados
   */
  public void setEstados(final List<HistoricoEstadoDocumentoDTO> estados) {
    this.estados = UtilidadesCommons.collectorsToList(estados);
  }



  /**
   * Gets the estado activo.
   *
   * @return the estado activo
   */
  @Nullable
  public HistoricoEstadoDocumentoDTO getEstadoActivo() {
    return estados != null
        ? estados.stream().filter(HistoricoEstadoDocumentoDTO::getActivo)
            .findFirst().orElse(null)
            : null;
  }


  /**
   * Gets the documento generado.
   *
   * @return the documento generado
   */
  public DocumentoGeneradoDTO getDocumentoGenerado() {
    return documentoGenerado;
  }

  /**
   * Sets the documento generado.
   *
   * @param documentoGenerado the new documento generado
   */
  public void setDocumentoGenerado(
      final DocumentoGeneradoDTO documentoGenerado) {
    this.documentoGenerado = documentoGenerado;
  }


  /**
   * Gets the fichero gde.
   *
   * @return the fichero gde
   */
  public byte[] getFicheroGde() {
    return UtilidadesCommons.cloneBytes(ficheroGde);
  }

  /**
   * Sets the fichero gde.
   *
   * @param ficheroGde the new fichero gde
   */
  public void setFicheroGde(final byte[] ficheroGde) {
    this.ficheroGde = UtilidadesCommons.cloneBytes(ficheroGde);
  }


  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */

  @Override
  public int compareTo(final DocumentoDTO o) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

  /**
   * Gets the fecha publicacion BOE.
   *
   * @return the fecha publicacion BOE
   */
  public Date getFechaPublicacionBOE() {
    return UtilidadesCommons.cloneDate(fechaPublicacionBOE);
  }

  /**
   * Sets the fecha publicacion BOE.
   *
   * @param fechaPublicacionBOE the new fecha publicacion BOE
   */
  public void setFechaPublicacionBOE(final Date fechaPublicacionBOE) {
    this.fechaPublicacionBOE = UtilidadesCommons.cloneDate(fechaPublicacionBOE);
  }

  /**
   * Gets the numero BOE.
   *
   * @return the numero BOE
   */
  public String getNumeroBOE() {
    return numeroBOE;
  }

  /**
   * Sets the numero BOE.
   *
   * @param numeroBOE the new numero BOE
   */
  public void setNumeroBOE(final String numeroBOE) {
    this.numeroBOE = numeroBOE;
  }

  /**
   * Gets the fecha impresion.
   *
   * @return the fecha impresion
   */
  public Date getFechaImpresion() {
    return UtilidadesCommons.cloneDate(fechaImpresion);
  }

  /**
   * Sets the fecha impresion.
   *
   * @param fechaImpresion the new fecha impresion
   */
  public void setFechaImpresion(final Date fechaImpresion) {
    this.fechaImpresion = UtilidadesCommons.cloneDate(fechaImpresion);
  }

  /**
   * Gets the usuario impresion.
   *
   * @return the usuario impresion
   */
  public UsuarioDTO getUsuarioImpresion() {
    return usuarioImpresion;
  }

  /**
   * Sets the usuario impresion.
   *
   * @param usuarioImpresion the new usuario impresion
   */
  public void setUsuarioImpresion(
      final UsuarioDTO usuarioImpresion) {
    this.usuarioImpresion = usuarioImpresion;
  }
}
