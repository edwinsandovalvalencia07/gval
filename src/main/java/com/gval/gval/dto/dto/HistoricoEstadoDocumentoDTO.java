package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;


/**
 * The Class HistoricoEstadoDocumentoDTO.
 */
public class HistoricoEstadoDocumentoDTO extends BaseDTO
    implements Comparable<HistoricoEstadoDocumentoDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1758720581146448342L;

  /** The pk historico estado documento. */
  private Long pkHistoricoEstadoDocumento;

  /** The activo. */
  private Boolean activo;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The fecha desde. */
  private Date fechaDesde;

  /** The fecha hasta. */
  private Date fechaHasta;

  /** The documento. */
  private DocumentoDTO documento;

  /** The usuario. */
  private UsuarioDTO usuario;

  /** The estado documento. */
  private EstadoDocumentoDTO estadoDocumento;

  /** The observaciones. */
  private String observaciones;

  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return observaciones;
  }

  /**
   * Sets the observaciones.
   *
   * @param observaciones the new observaciones
   */
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }

  /**
   * Instantiates a new historico estado documento DTO.
   */
  public HistoricoEstadoDocumentoDTO() {
    super();
  }

  /**
   * Creates the instance.
   *
   * @return the historico estado documento DTO
   */
  public static HistoricoEstadoDocumentoDTO createInstance() {
    final HistoricoEstadoDocumentoDTO historicoEstadoDocumentoDTO =
        new HistoricoEstadoDocumentoDTO();

    historicoEstadoDocumentoDTO.setFechaDesde(new Date());
    historicoEstadoDocumentoDTO.setActivo(Boolean.TRUE);
    historicoEstadoDocumentoDTO.setFechaHasta(null);
    historicoEstadoDocumentoDTO.setFechaCreacion(new Date());

    return historicoEstadoDocumentoDTO;
  }

  /**
   * Gets the pk historico estado documento.
   *
   * @return the pk historico estado documento
   */
  public Long getPkHistoricoEstadoDocumento() {
    return pkHistoricoEstadoDocumento;
  }

  /**
   * Sets the pk historico estado documento.
   *
   * @param pkHistoricoEstadoDocumento the new pk historico estado documento
   */
  public void setPkHistoricoEstadoDocumento(
      final Long pkHistoricoEstadoDocumento) {
    this.pkHistoricoEstadoDocumento = pkHistoricoEstadoDocumento;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha desde.
   *
   * @return the fecha desde
   */
  public Date getFechaDesde() {
    return UtilidadesCommons.cloneDate(fechaDesde);
  }

  /**
   * Sets the fecha desde.
   *
   * @param fechaDesde the new fecha desde
   */
  public void setFechaDesde(final Date fechaDesde) {
    this.fechaDesde = UtilidadesCommons.cloneDate(fechaDesde);
  }

  /**
   * Gets the fecha hasta.
   *
   * @return the fecha hasta
   */
  public Date getFechaHasta() {
    return UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Sets the fecha hasta.
   *
   * @param fechaHasta the new fecha hasta
   */
  public void setFechaHasta(final Date fechaHasta) {
    this.fechaHasta = UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Gets the documento.
   *
   * @return the documento
   */
  public DocumentoDTO getDocumento() {
    return documento;
  }

  /**
   * Sets the documento.
   *
   * @param documento the new documento
   */
  public void setDocumento(final DocumentoDTO documento) {
    this.documento = documento;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the estado documento.
   *
   * @return the estado documento
   */
  public EstadoDocumentoDTO getEstadoDocumento() {
    return estadoDocumento;
  }

  /**
   * Sets the estado documento.
   *
   * @param estadoDocumento the new estado documento
   */
  public void setEstadoDocumento(final EstadoDocumentoDTO estadoDocumento) {
    this.estadoDocumento = estadoDocumento;
  }


  /**
   * Compare to.
   *
   * @param arg0 the arg 0
   * @return the int
   */
  @Override
  public int compareTo(final HistoricoEstadoDocumentoDTO arg0) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
