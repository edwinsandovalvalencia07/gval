package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;
import com.gval.gval.entity.model.Constantes;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;

/**
 * The Class HistoricoEstadosDTO.
 */
public class HistoricoEstadosDTO extends BaseDTO
    implements Comparable<HistoricoEstadosDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 2094733357315213438L;

  /** The nombre. */
  private String nombre;

  /** The descripcion. */
  private String descripcion;

  /** The fecha desde. */
  private Date fechaDesde;

  /** The fecha hasta. */
  private Date fechaHasta;


  /**
   * Instantiates a new historico estados DTO.
   */
  public HistoricoEstadosDTO() {
    super();
  }

  /**
   * Instantiates a new historico estados DTO.
   *
   * @param nombre the nombre
   * @param descripcion the descripcion
   * @param fechaDesde the fecha desde
   * @param fechaHasta the fecha hasta
   */
  public HistoricoEstadosDTO(final String nombre, final String descripcion,
      final Date fechaDesde, final Date fechaHasta) {
    super();
    this.nombre = nombre;
    this.descripcion = descripcion;
    this.fechaDesde = UtilidadesCommons.cloneDate(fechaDesde);
    this.fechaHasta = UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the fecha desde.
   *
   * @return the fecha desde
   */
  public Date getFechaDesde() {
    return UtilidadesCommons.cloneDate(fechaDesde);
  }

  /**
   * Sets the fecha desde.
   *
   * @param fechaDesde the new fecha desde
   */
  public void setFechaDesde(final Date fechaDesde) {
    this.fechaDesde = UtilidadesCommons.cloneDate(fechaDesde);
  }

  /**
   * Gets the fecha hasta.
   *
   * @return the fecha hasta
   */
  public Date getFechaHasta() {
    return UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Sets the fecha hasta.
   *
   * @param fechaHasta the new fecha hasta
   */
  public void setFechaHasta(final Date fechaHasta) {
    this.fechaHasta = UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Gets the estado.
   *
   * @return the estado
   */
  public String getEstado() {
    final StringBuilder estado = new StringBuilder();

    estado.append(this.getNombre() == null || this.getNombre().isEmpty()
        ? this.getDescripcion()
        : this.getNombre());
    estado
        .append(this.getDescripcion() == null || this.getDescripcion().isEmpty()
            ? Constantes.VACIO
            : Constantes.ESPACIO.concat(this.getDescripcion()));

    return estado.toString();
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final HistoricoEstadosDTO o) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
