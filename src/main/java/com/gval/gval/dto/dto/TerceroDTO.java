//package com.gval.gval.dto.dto;
//
//import com.gval.gval.common.ConstantesCommons;
//import com.gval.gval.common.UtilidadesCommons;
//
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//
//import org.apache.commons.lang3.StringUtils;
//import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
//import org.hibernate.validator.constraints.NotBlank;
//
//import java.io.Serializable;
//import java.util.Date;
//
//import jakarta.annotation.Nullable;
//import javax.validation.constraints.AssertTrue;
//
///**
// * The Class TerceroDTO.
// */
//public class TerceroDTO extends CuentaBancariaDTO
//    implements Comparable<TerceroDTO>, Serializable {
//
//
//
//  /** The Constant serialVersionUID. */
//  private static final long serialVersionUID = 3263381313703531264L;
//
//  /** The Constant CODIGO_IBAN_ESPANYA. */
//  private static final String CODIGO_IBAN_ESPANYA = "ES";
//
//  /** The pk terceros. */
//  private Long pkTerceros;
//
//  /** The loteTerceros. */
//  private LoteTercerosDTO loteTerceros;
//
//  /** The tipo identificador. */
//  private Long tipoIdentificador;
//
//  /** The cuenta iban. */
//  private String cuentaIban;
//
//  /** The banco original. */
//  @AssertTrue(message = "error.terceros.no-selecciona-declaracion")
//  private Boolean declaraResponsabilidad;
//
//  /** The fecha alta. */
//  private Date fechaAlta;
//
//  /** The fecha validacion. */
//  private Date fechaValidacion;
//
//  /** The fecha baja. */
//  private Date fechaBaja;
//
//  /** The fecha creacion. */
//  private Date fechaCreacion;
//
//  /** The heredero. */
//  private Boolean heredero;
//
//
//  /** The nombre ada. */
//  private String nombreAda;
//
//  /** The nif. */
//  @NotBlank(message = "error.terceros.no-nif")
//  private String nif;
//
//  /** The origen. */
//  private String origen;
//
//  /** The primer apellido. */
//  private String primerApellido;
//
//  /** The activo. */
//  private Boolean activo;
//
//  /** The segundo apellido. */
//  private String segundoApellido;
//
//  /** The tipo. */
//  private Long tipoPersona;
//
//  /** The tipo de cuenta bancaria . */
//  private String tipoTercero;
//
//  /** The persona ada. */
//  private PersonaAdaDTO personaAda;
//
//  /** The expediente. */
//  private ExpedienteDTO expediente;
//
//  /** The pendiente activar. */
//  private Boolean pendienteActivar;
//
//  /** The app origen. */
//  private String appOrigen;
//
//  /** The fecha modificacion. */
//  private Date fechaModificacion;
//
//  /**
//   * Instantiates a new tercero DTO.
//   */
//  public TerceroDTO() {
//    super();
//    this.setValidado(Boolean.FALSE);
//    this.setDeclaraResponsabilidad(Boolean.FALSE);
//    this.setVersion("01");
//    this.setActivo(Boolean.FALSE);
//    this.setPendienteActivar(Boolean.FALSE);
//    this.setHeredero(Boolean.FALSE);
//    this.setTipoTercero(TipoTercero.EXTRANJERO.getValor());
//  }
//
//  /**
//   * Instantiates a new tercero DTO.
//   *
//   * @param expedienteDTO the expediente DTO
//   * @param pendienteActivar es true si va a ser un nuevo tercero
//   */
//  public TerceroDTO(final ExpedienteDTO expedienteDTO,
//      final boolean pendienteActivar) {
//    this();
//    this.setExpediente(expedienteDTO);
//    this.setPersonaAda(expedienteDTO.getSolicitante());
//    this.setPendienteActivar(pendienteActivar);
//    this.setLoteTerceros(loteTerceros);
//  }
//
//
//  /**
//   * Gets the pk terceros.
//   *
//   * @return the pk terceros
//   */
//  public Long getPkTerceros() {
//    return pkTerceros;
//  }
//
//
//  /**
//   * Sets the pk terceros.
//   *
//   * @param pkTerceros the new pk terceros
//   */
//  public void setPkTerceros(final Long pkTerceros) {
//    this.pkTerceros = pkTerceros;
//  }
//
//
//
//  /**
//   * Gets the tipo identificador.
//   *
//   * @return the tipo identificador
//   */
//  public Long getTipoIdentificador() {
//    return tipoIdentificador;
//  }
//
//
//  /**
//   * Sets the tipo identificador.
//   *
//   * @param tipoIdentificador the new tipo identificador
//   */
//  public void setTipoIdentificador(final Long tipoIdentificador) {
//    this.tipoIdentificador = tipoIdentificador;
//  }
//
//
//  /**
//   * Gets the cuenta iban.
//   *
//   * @return the cuenta iban
//   */
//  public String getCuentaIban() {
//    return cuentaIban;
//  }
//
//
//  /**
//   * Sets the cuenta iban.
//   *
//   * @param cuentaIban the new cuenta iban
//   */
//  public void setCuentaIban(final String cuentaIban) {
//    this.cuentaIban = cuentaIban;
//  }
//
//
//  /**
//   * Gets the banco original.
//   *
//   * @return the banco original
//   */
//  public Boolean getDeclaraResponsabilidad() {
//    return declaraResponsabilidad;
//  }
//
//
//  /**
//   * Sets the banco original.
//   *
//   * @param declaraResponsabilidad the new banco original
//   */
//  public void setDeclaraResponsabilidad(final Boolean declaraResponsabilidad) {
//    this.declaraResponsabilidad = declaraResponsabilidad;
//  }
//
//
//  /**
//   * Gets the fecha alta.
//   *
//   * @return the fecha alta
//   */
//  public Date getFechaAlta() {
//    return UtilidadesCommons.cloneDate(fechaAlta);
//  }
//
//
//  /**
//   * Sets the fecha alta.
//   *
//   * @param fechaAlta the new fecha alta
//   */
//  public void setFechaAlta(final Date fechaAlta) {
//    this.fechaAlta = UtilidadesCommons.cloneDate(fechaAlta);
//  }
//
//
//  /**
//   * Gets the fecha validacion.
//   *
//   * @return the fecha validacion
//   */
//  public Date getFechaValidacion() {
//    return UtilidadesCommons.cloneDate(fechaValidacion);
//  }
//
//
//  /**
//   * Sets the fecha validacion.
//   *
//   * @param fechaValidacion the new fecha validacion
//   */
//  public void setFechaValidacion(final Date fechaValidacion) {
//    this.fechaValidacion = UtilidadesCommons.cloneDate(fechaValidacion);
//  }
//
//
//  /**
//   * Gets the fecha baja.
//   *
//   * @return the fecha baja
//   */
//  public Date getFechaBaja() {
//    return UtilidadesCommons.cloneDate(fechaBaja);
//  }
//
//
//  /**
//   * Sets the fecha baja.
//   *
//   * @param fechaBaja the new fecha baja
//   */
//  public void setFechaBaja(final Date fechaBaja) {
//    this.fechaBaja = UtilidadesCommons.cloneDate(fechaBaja);
//  }
//
//
//  /**
//   * Gets the fecha creacion.
//   *
//   * @return the fecha creacion
//   */
//  public Date getFechaCreacion() {
//    return UtilidadesCommons.cloneDate(fechaCreacion);
//  }
//
//
//  /**
//   * Sets the fecha creacion.
//   *
//   * @param fechaCreacion the new fecha creacion
//   */
//  public void setFechaCreacion(final Date fechaCreacion) {
//    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
//  }
//
//
//  /**
//   * Gets the heredero.
//   *
//   * @return the heredero
//   */
//  public Boolean getHeredero() {
//    return heredero;
//  }
//
//
//  /**
//   * Sets the heredero.
//   *
//   * @param heredero the new heredero
//   */
//  public void setHeredero(final Boolean heredero) {
//    this.heredero = heredero;
//  }
//
//
//  /**
//   * Gets the nombre.
//   *
//   * @return the nombre
//   */
//  public String getNombreAda() {
//    return nombreAda;
//  }
//
//
//  /**
//   * Sets the nombre.
//   *
//   * @param nombreAda the new nombre ada
//   */
//  public void setNombreAda(final String nombreAda) {
//    this.nombreAda = nombreAda;
//  }
//
//
//  /**
//   * Gets the nif.
//   *
//   * @return the nif
//   */
//  public String getNif() {
//    return nif;
//  }
//
//
//  /**
//   * Sets the nif.
//   *
//   * @param nif the new nif
//   */
//  public void setNif(final String nif) {
//    this.nif = nif;
//  }
//
//
//  /**
//   * Gets the origen.
//   *
//   * @return the origen
//   */
//  public String getOrigen() {
//    return origen;
//  }
//
//
//  /**
//   * Sets the origen.
//   *
//   * @param origen the new origen
//   */
//  public void setOrigen(final String origen) {
//    this.origen = origen;
//  }
//
//
//  /**
//   * Gets the primer apellido.
//   *
//   * @return the primer apellido
//   */
//  public String getPrimerApellido() {
//    return primerApellido;
//  }
//
//
//  /**
//   * Sets the primer apellido.
//   *
//   * @param primerApellido the new primer apellido
//   */
//  public void setPrimerApellido(final String primerApellido) {
//    this.primerApellido = primerApellido;
//  }
//
//
//  /**
//   * Gets the activo.
//   *
//   * @return the activo
//   */
//  public Boolean getActivo() {
//    return activo;
//  }
//
//
//  /**
//   * Sets the activo.
//   *
//   * @param activo the new activo
//   */
//  public void setActivo(final Boolean activo) {
//    this.activo = activo;
//  }
//
//
//  /**
//   * Gets the segundo apellido.
//   *
//   * @return the segundo apellido
//   */
//  public String getSegundoApellido() {
//    return segundoApellido;
//  }
//
//
//  /**
//   * Sets the segundo apellido.
//   *
//   * @param segundoApellido the new segundo apellido
//   */
//  public void setSegundoApellido(final String segundoApellido) {
//    this.segundoApellido = segundoApellido;
//  }
//
//
//  /**
//   * Gets the tipo tercero.
//   *
//   * @return the tipo tercero
//   */
//  public String getTipoTercero() {
//    return tipoTercero;
//  }
//
//
//  /**
//   * Sets the tipo tercero.
//   *
//   * @param tipoTercero the new tipo tercero
//   */
//  public void setTipoTercero(final String tipoTercero) {
//    this.tipoTercero = tipoTercero;
//  }
//
//
//  /**
//   * Gets the persona ada.
//   *
//   * @return the persona ada
//   */
//  public PersonaAdaDTO getPersonaAda() {
//    return personaAda;
//  }
//
//
//  /**
//   * Sets the persona ada.
//   *
//   * @param personaAda the new persona ada
//   */
//  public void setPersonaAda(final PersonaAdaDTO personaAda) {
//    this.personaAda = personaAda;
//  }
//
//
//  /**
//   * Gets the expediente.
//   *
//   * @return the expediente
//   */
//  public ExpedienteDTO getExpediente() {
//    return expediente;
//  }
//
//
//  /**
//   * Sets the expediente.
//   *
//   * @param expediente the new expediente
//   */
//  public void setExpediente(final ExpedienteDTO expediente) {
//    this.expediente = expediente;
//  }
//
//
//  /**
//   * Gets the lote terceros.
//   *
//   * @return the lote
//   */
//  public LoteTercerosDTO getLoteTerceros() {
//    return loteTerceros;
//  }
//
//  /**
//   * Sets the lote terceros.
//   *
//   * @param loteTerceros the new lote terceros
//   */
//  public void setLoteTerceros(final LoteTercerosDTO loteTerceros) {
//    this.loteTerceros = loteTerceros;
//  }
//
//  /**
//   * Gets the pendiente activar.
//   *
//   * @return the pendiente activar
//   */
//  public Boolean getPendienteActivar() {
//    return pendienteActivar;
//  }
//
//
//  /**
//   * Sets the pendiente activar.
//   *
//   * @param pendienteActivar the new pendiente activar
//   */
//  public void setPendienteActivar(final Boolean pendienteActivar) {
//    this.pendienteActivar = pendienteActivar;
//  }
//
//  /**
//   * Gets the nombre completo.
//   *
//   * @return the nombre completo
//   */
//  public String getNombreCompleto() {
//    final String nombreAdaTercero =
//        (StringUtils.isEmpty(getNombreAda())) ? Constantes.VACIO
//            : getNombreAda();
//    final String primerApellidoTercero =
//        (StringUtils.isEmpty(getPrimerApellido())) ? Constantes.VACIO
//            : getPrimerApellido();
//    final String segundoApellidoTercero =
//        (StringUtils.isEmpty(getSegundoApellido())) ? Constantes.VACIO
//            : getSegundoApellido();
//
//    return nombreAdaTercero.concat(Constantes.ESPACIO)
//        .concat(primerApellidoTercero).concat(Constantes.ESPACIO)
//        .concat(segundoApellidoTercero);
//  }
//
//  /**
//   * Recupera de ada el iban en el formato correspondiente.
//   *
//   * @return the cuenta iban completa ada
//   */
//  @JsonIgnore
//  @Nullable
//  public String getCuentaIbanCompletaAda() {
//    if (cuentaIban != null) {
//      final StringBuilder sb = new StringBuilder();
//      // cada 4 caracteres insertamos un espacio, ya que es el formato del iban
//      sb.append(getIbanCodigoPais()).append(getIbanDigitoControl())
//          .append(ConstantesCommons.ESPACIO_CHAR)
//          .append(cuentaIban.replaceAll("(.{4})", "$1 ").trim());
//      return sb.toString();
//    } else {
//      return null;
//    }
//  }
//
//  /**
//   * Recupera la cuenta bancaria nacional en caso de que el iban sea espanyol.
//   *
//   * @return the cuenta nacional completa ada
//   */
//  @JsonIgnore
//  @Nullable
//  public String getCuentaNacionalCompletaAda() {
//    if ((esTipoTerceroNacional() && !esIban()) || esIbanNacional()) {
//      final StringBuilder sb = new StringBuilder();
//      sb.append(getCodigoBanco()).append(ConstantesCommons.ESPACIO_CHAR)
//          .append(getCodigoSucursal()).append(ConstantesCommons.ESPACIO_CHAR)
//          .append(getDigitoControl()).append(ConstantesCommons.ESPACIO_CHAR)
//          .append(getCuenta());
//      return sb.toString();
//    } else {
//      return null;
//    }
//  }
//
//
//  /**
//   * Compare to.
//   *
//   * @param o the o
//   * @return the int
//   */
//  @Override
//  public int compareTo(final TerceroDTO o) {
//    return 0;
//  }
//
//
//  /**
//   * Gets the tipo persona.
//   *
//   * @return the tipo persona
//   */
//  public Long getTipoPersona() {
//    return tipoPersona;
//  }
//
//
//  /**
//   * Sets the tipo persona.
//   *
//   * @param tipoPersona the new tipo persona
//   */
//  public void setTipoPersona(final Long tipoPersona) {
//    this.tipoPersona = tipoPersona;
//  }
//
//  /**
//   * Es tipo tercero nacional.
//   *
//   * @return true, if successful
//   */
//  public boolean esTipoTerceroNacional() {
//    return TipoTercero.NACIONAL.getValor().equals(tipoTercero);
//  }
//
//
//  /**
//   * Es cuenta nacional.
//   *
//   * @return true, if successful
//   */
//  public boolean esIbanCodigoPaisNacional() {
//    return CODIGO_IBAN_ESPANYA.equals(getIbanCodigoPais());
//  }
//
//  /**
//   * Es iban nacional.
//   *
//   * @return true, if successful
//   */
//  public boolean esIbanNacional() {
//    return esIban() && esIbanCodigoPaisNacional();
//
//  }
//
//  /**
//   * Es iban extranjero.
//   *
//   * @return true, if successful
//   */
//  public boolean esIbanExtranjero() {
//    return esIban() && !esIbanCodigoPaisNacional();
//
//  }
//
//  /**
//   * Es iban.
//   *
//   * @return true, if successful
//   */
//  public boolean esIban() {
//    return StringUtils.isNotBlank(this.getIbanDigitoControl());
//  }
//
//  /**
//   * Gets the app origen.
//   *
//   * @return the app origen
//   */
//  public String getAppOrigen() {
//    return appOrigen;
//  }
//
//  /**
//   * Sets the app origen.
//   *
//   * @param appOrigen the new app origen
//   */
//  public void setAppOrigen(final String appOrigen) {
//    this.appOrigen = appOrigen;
//  }
//
//  /**
//   * Gets the fecha modificacion.
//   *
//   * @return the fecha modificacion
//   */
//  public Date getFechaModificacion() {
//    return UtilidadesCommons.cloneDate(fechaModificacion);
//  }
//
//  /**
//   * Sets the fecha modificacion.
//   *
//   * @param fechaModificacion the new fecha modificacion
//   */
//  public void setFechaModificacion(final Date fechaModificacion) {
//    this.fechaModificacion = UtilidadesCommons.cloneDate(fechaModificacion);
//  }
//
//
//
//  /**
//   * To string.
//   *
//   * @return the string
//   */
//  @Override
//  public String toString() {
//    return ReflectionToStringBuilder.toString(this);
//  }
//}
//
//
