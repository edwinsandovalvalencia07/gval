package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;


/**
 * The Class EstudioDTO.
 */
public class EstudioRecursoRepetDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk estudio. */
  private Long pkEstudio;

  /** The esactivo. */
  private Boolean activo;

  /** The estudio anterior. */
  private EstudioDTO estudioAnterior;

  /** The estudio inicial. */
  private EstudioDTO estudioInicial;


  /**
   * Instantiates a new estudioDTO.
   */
  public EstudioRecursoRepetDTO() {
    super();
  }


  /**
   * Gets the pk estudio.
   *
   * @return the pk estudio
   */
  public Long getPkEstudio() {
    return this.pkEstudio;
  }


  /**
   * Sets the pk estudio.
   *
   * @param pkEstudio the new pk estudio
   */
  public void setPkEstudio(final Long pkEstudio) {
    this.pkEstudio = pkEstudio;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return this.activo;
  }


  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }


  /**
   * Gets the estudio anterior.
   *
   * @return the estudio anterior
   */
  public EstudioDTO getEstudioAnterior() {
    return this.estudioAnterior;
  }


  /**
   * Sets the estudio anterior.
   *
   * @param estudioAnterior the new estudio anterior
   */
  public void setEstudioAnterior(final EstudioDTO estudioAnterior) {
    this.estudioAnterior = estudioAnterior;
  }


  /**
   * Gets the estudio inicial.
   *
   * @return the estudio inicial
   */
  public EstudioDTO getEstudioInicial() {
    return this.estudioInicial;
  }


  /**
   * Sets the estudio inicial.
   *
   * @param estudioInicial the new estudio inicial
   */
  public void setEstudioInicial(final EstudioDTO estudioInicial) {
    this.estudioInicial = estudioInicial;
  }


}
