/**
 * Copyright (c) 2020 Generalitat Valenciana - Todos los derechos reservados.
 */
package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;
import es.gva.dependencia.ada.util.Utilidades;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;

import javax.validation.constraints.NotNull;


/**
 * The Class DocumentoInformeSocialDTO.
 */
public class DocumentoInformeSocialDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -1884147329501567891L;

  /** The pk docu infosoci. */
  private Long pkDocuInfosoci;

  /** The fecha creacion. */
  @NotNull
  private Date fechaCreacion;

  /** The documento. */
  @NotNull
  private DocumentoDTO documento;

  /** The informe social. */
  @NotNull
  private InformeSocialDTO informeSocial;


  /**
   * Crea un nuevo Documento Informe Social DTO.
   */
  public DocumentoInformeSocialDTO() {
    super();
    this.fechaCreacion = Utilidades.getFechaCorrectamente(new Date());
    this.documento = new DocumentoDTO();
    this.informeSocial = new InformeSocialDTO();
  }

  /**
   * Gets the pk docu infosoci.
   *
   * @return the pkDocuInfosoci
   */
  public Long getPkDocuInfosoci() {
    return pkDocuInfosoci;
  }

  /**
   * Sets the pk docu infosoci.
   *
   * @param pkDocuInfosoci the pkDocuInfosoci to set
   */
  public void setPkDocuInfosoci(final Long pkDocuInfosoci) {
    this.pkDocuInfosoci = pkDocuInfosoci;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the documento.
   *
   * @return the documento
   */
  public DocumentoDTO getDocumento() {
    return documento;
  }

  /**
   * Sets the documento.
   *
   * @param documento the documento to set
   */
  public void setDocumento(final DocumentoDTO documento) {
    this.documento = documento;
  }

  /**
   * Gets the informe social.
   *
   * @return the informeSocial
   */
  public InformeSocialDTO getInformeSocial() {
    return informeSocial;
  }

  /**
   * Sets the informe social.
   *
   * @param informeSocial the informeSocial to set
   */
  public void setInformeSocial(final InformeSocialDTO informeSocial) {
    this.informeSocial = informeSocial;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
