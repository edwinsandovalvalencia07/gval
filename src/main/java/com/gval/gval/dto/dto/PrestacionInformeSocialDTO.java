package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * The Class PrestacionInformeSocialDTO.
 */
public final class PrestacionInformeSocialDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -7229990062930862817L;

  /** The pk prescomp. */
  private Long pkPrescomp;

  /** The aportacion. */
  private BigDecimal aportacion;

  /** The descripcion detallada. */
  @Size(max = 4000)
  private String descripcionDetallada;

  /** The descripcion. */
  @Size(max = 250)
  private String descripcion;

  /** The horas semanales. */
  @Size(max = 250)
  private String horasSemanales;

  /** The titularidad. */
  @Size(max = 250)
  private String titularidad;

  /** The informe social. */
  @NotNull
  private InformeSocialDTO informeSocial;


  /**
   * Crea una instancia de Prestacion Informe Social.
   */
  public PrestacionInformeSocialDTO() {
    super();
  }

  /**
   * Gets the pk prescomp.
   *
   * @return the pk prescomp
   */
  public Long getPkPrescomp() {
    return pkPrescomp;
  }

  /**
   * Sets the pk prescomp.
   *
   * @param pkPrescomp the new pk prescomp
   */
  public void setPkPrescomp(final Long pkPrescomp) {
    this.pkPrescomp = pkPrescomp;
  }

  /**
   * Gets the aportacion.
   *
   * @return the aportacion
   */
  public BigDecimal getAportacion() {
    return aportacion;
  }

  /**
   * Sets the aportacion.
   *
   * @param aportacion the new aportacion
   */
  public void setAportacion(final BigDecimal aportacion) {
    this.aportacion = aportacion;
  }

  /**
   * Gets the descripcion detallada.
   *
   * @return the descripcion detallada
   */
  public String getDescripcionDetallada() {
    return descripcionDetallada;
  }

  /**
   * Sets the descripcion detallada.
   *
   * @param descripcionDetallada the new descripcion detallada
   */
  public void setDescripcionDetallada(final String descripcionDetallada) {
    this.descripcionDetallada = descripcionDetallada;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the horas semanales.
   *
   * @return the horas semanales
   */
  public String getHorasSemanales() {
    return horasSemanales;
  }

  /**
   * Sets the horas semanales.
   *
   * @param horasSemanales the new horas semanales
   */
  public void setHorasSemanales(final String horasSemanales) {
    this.horasSemanales = horasSemanales;
  }

  /**
   * Gets the titularidad.
   *
   * @return the titularidad
   */
  public String getTitularidad() {
    return titularidad;
  }

  /**
   * Sets the titularidad.
   *
   * @param titularidad the new titularidad
   */
  public void setTitularidad(final String titularidad) {
    this.titularidad = titularidad;
  }

  /**
   * Gets the informe social.
   *
   * @return the informe social
   */
  public InformeSocialDTO getInformeSocial() {
    return informeSocial;
  }

  /**
   * Sets the informe social.
   *
   * @param informeSocial the new informe social
   */
  public void setInformeSocial(final InformeSocialDTO informeSocial) {
    this.informeSocial = informeSocial;
  }



}
