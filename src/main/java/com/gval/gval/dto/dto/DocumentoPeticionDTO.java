package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;
import java.util.List;

/**
 * The Class DocumentoPeticionDTO.
 */
public class DocumentoPeticionDTO extends BaseDTO
    implements Comparable<DocumentoPeticionDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 6644336761819066141L;

  /** The pk docupeticion. */
  private long pkDocupeticion;

  /** The activo. */
  private Boolean activo;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The fecha completo. */
  private Date fechaCompleto;

  /** The documento peticion aportados. */
  private List<DocumentoPeticionAportadoDTO> documentoPeticionAportados;

  /** The documento. */
  private DocumentoDTO documento;

  /** The usuario. */
  private UsuarioDTO usuario;

  /**
   * Instantiates a new documento peticion.
   */
  public DocumentoPeticionDTO() {
    super();
  }

  /**
   * Gets the pk docupeticion.
   *
   * @return the pk docupeticion
   */
  public long getPkDocupeticion() {
    return pkDocupeticion;
  }

  /**
   * Sets the pk docupeticion.
   *
   * @param pkDocupeticion the new pk docupeticion
   */
  public void setPkDocupeticion(final long pkDocupeticion) {
    this.pkDocupeticion = pkDocupeticion;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha completo.
   *
   * @return the fecha completo
   */
  public Date getFechaCompleto() {
    return UtilidadesCommons.cloneDate(fechaCompleto);
  }

  /**
   * Sets the fecha completo.
   *
   * @param fechaCompleto the new fecha completo
   */
  public void setFechaCompleto(final Date fechaCompleto) {
    this.fechaCompleto = UtilidadesCommons.cloneDate(fechaCompleto);
  }

  /**
   * Gets the documento.
   *
   * @return the documento
   */
  public DocumentoDTO getDocumento() {
    return documento;
  }

  /**
   * Sets the documento.
   *
   * @param documento the new documento
   */
  public void setDocumento(final DocumentoDTO documento) {
    this.documento = documento;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the documento peticion aportados.
   *
   * @return the documento peticion aportados
   */
  public List<DocumentoPeticionAportadoDTO> getDocumentoPeticionAportados() {
    return UtilidadesCommons.collectorsToList(documentoPeticionAportados);
  }

  /**
   * Sets the documento peticion aportados.
   *
   * @param documentoPeticionAportados the new documento peticion aportados
   */
  public void setDocumentoPeticionAportados(
      final List<DocumentoPeticionAportadoDTO> documentoPeticionAportados) {
    this.documentoPeticionAportados =
        UtilidadesCommons.collectorsToList(documentoPeticionAportados);
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final DocumentoPeticionDTO o) {
    return 0;
  }
}
