package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;

/**
 * The type Contador dto.
 */
public class ContadorDTO extends BaseDTO implements Comparable<ContadorDTO> {


  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -4369470584842489209L;

  /** The id contador. */
  private Long idContador;

  /** The fk zona cobertura. */
  private Long fkZonaCobertura;

  /** The clave. */
  private Long clave;

  /** The estado. */
  private String estado;

  /** The activo. */
  private Boolean activo;

  /** The fecha inicio. */
  private Date fechaInicio;

  /** The fecha fin. */
  private Date fechaFin;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The presupuesto anual. */
  private Double presupuestoAnual;

  /** The presupuesto disponible. */
  private Double presupuestoDisponible;

  /** The precio hora. */
  private Double precioHora;

  /** The horas asignadas. */
  private Double horasAsignadas;

  /** The horas disponibles. */
  private Double horasDisponibles;

  /** The zona cobertura. */
  private ZonaCoberturaDTO zonaCobertura;

  /** The ejercicio. */
  private EjercicioDTO ejercicio;


  /**
   * Constructor de contador.
   */
  public ContadorDTO() {
    super();
  }

  /**
   * Gets id contador.
   *
   * @return the id contador
   */

  public Long getIdContador() {
    return idContador;
  }

  /**
   * Sets id contador.
   *
   * @param pkContadorSad the pk contador sad
   */
  public void setIdContador(final Long pkContadorSad) {
    this.idContador = pkContadorSad;
  }

  /**
   * Gets fk zona cobertura.
   *
   * @return the fk zona cobertura
   */

  public Long getFkZonaCobertura() {
    return fkZonaCobertura;
  }

  /**
   * Sets fk zona cobertura.
   *
   * @param pkZonacobe the pk zonacobe
   */
  public void setFkZonaCobertura(final Long pkZonacobe) {
    this.fkZonaCobertura = pkZonacobe;
  }

  /**
   * Gets clave.
   *
   * @return the clave
   */

  public Long getClave() {
    return clave;
  }

  /**
   * Sets clave.
   *
   * @param clave the clave
   */
  public void setClave(final Long clave) {
    this.clave = clave;
  }

  /**
   * Gets estado.
   *
   * @return the estado
   */

  public String getEstado() {
    return estado;
  }

  /**
   * Sets estado.
   *
   * @param estado the estado
   */
  public void setEstado(final String estado) {
    this.estado = estado;
  }

  /**
   * Gets activo.
   *
   * @return the activo
   */

  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets activo.
   *
   * @param activo the activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets fecha inicio.
   *
   * @return the fecha inicio
   */
  public Date getFechaInicio() {
    return UtilidadesCommons.cloneDate(fechaInicio);
  }

  /**
   * Sets fecha inicio.
   *
   * @param fechaInicio the fecha inicio
   */
  public void setFechaInicio(final Date fechaInicio) {
    this.fechaInicio = UtilidadesCommons.cloneDate(fechaInicio);
  }

  /**
   * Gets fecha fin.
   *
   * @return the fecha fin
   */

  public Date getFechaFin() {
    return UtilidadesCommons.cloneDate(fechaFin);
  }

  /**
   * Sets fecha fin.
   *
   * @param fechaFin the fecha fin
   */
  public void setFechaFin(final Date fechaFin) {
    this.fechaFin = UtilidadesCommons.cloneDate(fechaFin);
  }

  /**
   * Gets fecha creacion.
   *
   * @return the fecha creacion
   */

  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets fecha creacion.
   *
   * @param fechaCreacion the fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets presupuesto anual.
   *
   * @return the presupuesto anual
   */
  public Double getPresupuestoAnual() {
    return presupuestoAnual;
  }

  /**
   * Sets presupuesto anual.
   *
   * @param presupuestoAnual the presupuesto anual
   */
  public void setPresupuestoAnual(final Double presupuestoAnual) {
    this.presupuestoAnual = presupuestoAnual;
  }

  /**
   * Gets presupuesto disponible.
   *
   * @return the presupuesto disponible
   */

  public Double getPresupuestoDisponible() {
    return presupuestoDisponible;
  }

  /**
   * Sets presupuesto disponible.
   *
   * @param presupuestoDisponible the presupuesto disponible
   */
  public void setPresupuestoDisponible(final Double presupuestoDisponible) {
    this.presupuestoDisponible = presupuestoDisponible;
  }

  /**
   * Gets precio hora.
   *
   * @return the precio hora
   */
  public Double getPrecioHora() {
    return precioHora;
  }

  /**
   * Sets precio hora.
   *
   * @param precioHora the precio hora
   */
  public void setPrecioHora(final Double precioHora) {
    this.precioHora = precioHora;
  }

  /**
   * Gets horas asignadas.
   *
   * @return the horas asignadas
   */
  public Double getHorasAsignadas() {
    return horasAsignadas;
  }

  /**
   * Sets horas asignadas.
   *
   * @param horasAsignadas the horas asignadas
   */
  public void setHorasAsignadas(final Double horasAsignadas) {
    this.horasAsignadas = horasAsignadas;
  }

  /**
   * Gets horas disponibles.
   *
   * @return the horas disponibles
   */
  public Double getHorasDisponibles() {
    return horasDisponibles;
  }

  /**
   * Sets horas disponibles.
   *
   * @param horasDisponibles the horas disponibles
   */
  public void setHorasDisponibles(final Double horasDisponibles) {
    this.horasDisponibles = horasDisponibles;
  }


  /**
   * Gets the zona cobertura.
   *
   * @return the zona cobertura
   */
  public final ZonaCoberturaDTO getZonaCobertura() {
    return zonaCobertura;
  }

  /**
   * Sets the zona cobertura.
   *
   * @param zonaCobertura the new zona cobertura
   */
  public final void setZonaCobertura(final ZonaCoberturaDTO zonaCobertura) {
    this.zonaCobertura = zonaCobertura;
  }

  /**
   * Gets the ejercicio.
   *
   * @return the ejercicio
   */
  public final EjercicioDTO getEjercicio() {
    return ejercicio;
  }

  /**
   * Sets the ejercicio.
   *
   * @param ejercicio the new ejercicio
   */
  public final void setEjercicio(final EjercicioDTO ejercicio) {
    this.ejercicio = ejercicio;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final ContadorDTO o) {
    return 0;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
