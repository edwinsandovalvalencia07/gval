package com.gval.gval.dto.dto;



import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * The Class AsignacionSolicitudDTO.
 */
public class AsignarSolicitudDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -1702107637815647404L;

  /** The pk asigsoli. */
  private Long pkAsigsoli;

  /** The activo. */
  private Boolean activo;

  /** The fecha aplazamiento. */
  private Date fechaAplazamiento;

  /** The fecha asignacion. */
  private Date fechaAsignacion;

  /** The fecha cita. */
  private Date fechaCita;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The hora desde. */
  private Date horaDesde;

  /** The hora hasta. */
  private Date horaHasta;

  /** The motivo. */
  private String motivo;

  /** The direccion. */
  private DireccionAdaDTO direccion;

  /** The solicitud. */
  private SolicitudDTO solicitud;

  /** The usuario. */
  private UsuarioDTO usuario;

  /** The valoracion. */
  private ValoracionDTO valoracion;

  /**
   * Instantiates a new asignacion solicitud DTO.
   */
  public AsignarSolicitudDTO() {
    super();
  }


  /**
   * Gets the pk asigsoli.
   *
   * @return the pk asigsoli
   */
  public Long getPkAsigsoli() {
    return pkAsigsoli;
  }

  /**
   * Sets the pk asigsoli.
   *
   * @param pkAsigsoli the new pk asigsoli
   */
  public void setPkAsigsoli(final Long pkAsigsoli) {
    this.pkAsigsoli = pkAsigsoli;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha aplazamiento.
   *
   * @return the fecha aplazamiento
   */
  public Date getFechaAplazamiento() {
    return UtilidadesCommons.cloneDate(fechaAplazamiento);
  }

  /**
   * Sets the fecha aplazamiento.
   *
   * @param fechaAplazamiento the new fecha aplazamiento
   */
  public void setFechaAplazamiento(final Date fechaAplazamiento) {
    this.fechaAplazamiento = UtilidadesCommons.cloneDate(fechaAplazamiento);
  }

  /**
   * Gets the fecha asignacion.
   *
   * @return the fecha asignacion
   */
  public Date getFechaAsignacion() {
    return UtilidadesCommons.cloneDate(fechaAsignacion);
  }

  /**
   * Sets the fecha asignacion.
   *
   * @param fechaAsignacion the new fecha asignacion
   */
  public void setFechaAsignacion(final Date fechaAsignacion) {
    this.fechaAsignacion = UtilidadesCommons.cloneDate(fechaAsignacion);
  }

  /**
   * Gets the fecha cita.
   *
   * @return the fecha cita
   */
  public Date getFechaCita() {
    return UtilidadesCommons.cloneDate(fechaCita);
  }

  /**
   * Sets the fecha cita.
   *
   * @param fechaCita the new fecha cita
   */
  public void setFechaCita(final Date fechaCita) {
    this.fechaCita = UtilidadesCommons.cloneDate(fechaCita);
  }

  /**
   * Gets the hora desde.
   *
   * @return the hora desde
   */
  public Date getHoraDesde() {
    return UtilidadesCommons.cloneDate(horaDesde);
  }

  /**
   * Sets the hora desde.
   *
   * @param horaDesde the new hora desde
   */
  public void setHoraDesde(final Date horaDesde) {
    this.horaDesde = UtilidadesCommons.cloneDate(horaDesde);
  }

  /**
   * Gets the hora hasta.
   *
   * @return the hora hasta
   */
  public Date getHoraHasta() {
    return UtilidadesCommons.cloneDate(horaHasta);
  }

  /**
   * Sets the hora hasta.
   *
   * @param horaHasta the new hora hasta
   */
  public void setHoraHasta(final Date horaHasta) {
    this.horaHasta = UtilidadesCommons.cloneDate(horaHasta);
  }

  /**
   * Gets the motivo.
   *
   * @return the motivo
   */
  public String getMotivo() {
    return motivo;
  }

  /**
   * Sets the motivo.
   *
   * @param motivo the new motivo
   */
  public void setMotivo(final String motivo) {
    this.motivo = motivo;
  }

  /**
   * Gets the direccion.
   *
   * @return the direccion
   */
  public DireccionAdaDTO getDireccion() {
    return direccion;
  }

  /**
   * Sets the direccion.
   *
   * @param direccion the new direccion
   */
  public void setDireccion(final DireccionAdaDTO direccion) {
    this.direccion = direccion;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public SolicitudDTO getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the new solicitud
   */
  public void setSolicitud(final SolicitudDTO solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }


  /**
   * Gets the valoracion.
   *
   * @return the valoracion
   */
  public ValoracionDTO getValoracion() {
    return valoracion;
  }


  /**
   * Sets the valoracion.
   *
   * @param valoracion the new valoracion
   */
  public void setValoracion(final ValoracionDTO valoracion) {
    this.valoracion = valoracion;
  }

  /**
   * Gets the fecha cita formateada.
   *
   * @return the fecha cita formateada
   */
//  public String getFechaCitaFormateada() {
//    return fechaCita == null ? ""
//        : new SimpleDateFormat(Utilidades.FORMAT_DATE_FECHA).format(fechaCita);
//  }

}
