package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;

/**
 * The Class UnidadDTO.
 */
public class UnidadDTO extends BaseDTO implements Comparable<UnidadDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 3901211856922256835L;

  /** The pk unidad. */
  private Long pkUnidad;

  /** The activo. */
  private Boolean activo;

  /** The descripcion. */
  private String descripcion;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The smad. */
  private Boolean smad;

  /** The direccion. */
  private DireccionAdaDTO direccion;

  /** The zona cobertura. */
  private ZonaCoberturaDTO zonaCobertura;

  /**
   * Instantiates a new unidad DTO.
   */
  public UnidadDTO() {
    super();
  }

  /**
   * Instantiates a new unidad DTO.
   *
   * @param pkUnidad the pk unidad
   * @param activo the activo
   * @param descripcion the descripcion
   * @param fechaCreacion the fecha creacion
   * @param smad the smad
   * @param direccion the direccion
   * @param zonaCobertura the zona cobertura
   */
  public UnidadDTO(final Long pkUnidad, final Boolean activo,
      final String descripcion, final Date fechaCreacion, final Boolean smad,
      final DireccionAdaDTO direccion, final ZonaCoberturaDTO zonaCobertura) {
    super();
    this.pkUnidad = pkUnidad;
    this.activo = activo;
    this.descripcion = descripcion;
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
    this.smad = smad;
    this.direccion = direccion;
    this.zonaCobertura = zonaCobertura;
  }

  /**
   * Gets the pk unidad.
   *
   * @return the pk unidad
   */
  public Long getPkUnidad() {
    return pkUnidad;
  }

  /**
   * Sets the pk unidad.
   *
   * @param pkUnidad the new pk unidad
   */
  public void setPkUnidad(final Long pkUnidad) {
    this.pkUnidad = pkUnidad;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the smad.
   *
   * @return the smad
   */
  public Boolean getSmad() {
    return smad;
  }

  /**
   * Sets the smad.
   *
   * @param smad the new smad
   */
  public void setSmad(final Boolean smad) {
    this.smad = smad;
  }

  /**
   * Gets the direccion.
   *
   * @return the direccion
   */
  public DireccionAdaDTO getDireccion() {
    return direccion;
  }

  /**
   * Sets the direccion.
   *
   * @param direccion the new direccion
   */
  public void setDireccion(final DireccionAdaDTO direccion) {
    this.direccion = direccion;
  }

  /**
   * Gets the zona cobertura.
   *
   * @return the zona cobertura
   */
  public ZonaCoberturaDTO getZonaCobertura() {
    return zonaCobertura;
  }

  /**
   * Sets the zona cobertura.
   *
   * @param zonaCobertura the new zona cobertura
   */
  public void setZonaCobertura(final ZonaCoberturaDTO zonaCobertura) {
    this.zonaCobertura = zonaCobertura;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final UnidadDTO o) {
    return 0;
  }

}
