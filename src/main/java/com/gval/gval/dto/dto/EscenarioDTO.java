package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * The Class EscenarioDTO.
 */
public class EscenarioDTO extends BaseDTO implements Comparable<EscenarioDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 6379140644839273479L;


  /** The persona. */
  private PersonaAdaDTO persona;

  /** The tipo documento. */
  private TipoDocumentoDTO tipoDocumento;

  /** The bloqueante. */
  private Boolean bloqueante;

  /** The obligatorio. */
  private Boolean obligatorio;

  /** The numero. */
  private Long numero;

  /** The aportado. */
  private Boolean aportado;


  /**
   * Instantiates a new escenario DTO.
   */
  public EscenarioDTO() {
    super();
    this.persona = null;
    this.tipoDocumento = null;
    this.bloqueante = null;
    this.obligatorio = null;
    this.numero = null;
    this.aportado = null;
  }


  /**
   * Gets the persona.
   *
   * @return the persona
   */
  public PersonaAdaDTO getPersona() {
    return persona;
  }



  /**
   * Sets the persona.
   *
   * @param persona the new persona
   */
  public void setPersona(final PersonaAdaDTO persona) {
    this.persona = persona;
  }



  /**
   * Gets the tipo documento.
   *
   * @return the tipo documento
   */
  public TipoDocumentoDTO getTipoDocumento() {
    return tipoDocumento;
  }



  /**
   * Sets the tipo documento.
   *
   * @param tipoDocumento the new tipo documento
   */
  public void setTipoDocumento(final TipoDocumentoDTO tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }



  /**
   * Gets the bloqueante.
   *
   * @return the bloqueante
   */
  public Boolean getBloqueante() {
    return bloqueante;
  }

  /**
   * Sets the bloqueante.
   *
   * @param bloqueante the new bloqueante
   */
  public void setBloqueante(final Boolean bloqueante) {
    this.bloqueante = bloqueante;
  }

  /**
   * Gets the obligatorio.
   *
   * @return the obligatorio
   */
  public Boolean getObligatorio() {
    return obligatorio;
  }

  /**
   * Sets the obligatorio.
   *
   * @param obligatorio the new obligatorio
   */
  public void setObligatorio(final Boolean obligatorio) {
    this.obligatorio = obligatorio;
  }

  /**
   * Gets the numero.
   *
   * @return the numero
   */
  public Long getNumero() {
    return numero;
  }

  /**
   * Sets the numero.
   *
   * @param numero the new numero
   */
  public void setNumero(final Long numero) {
    this.numero = numero;
  }

  /**
   * Gets the aportado.
   *
   * @return the aportado
   */
  public Boolean getAportado() {
    return aportado;
  }

  /**
   * Sets the aportado.
   *
   * @param aportado the new aportado
   */
  public void setAportado(final Boolean aportado) {
    this.aportado = aportado;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final EscenarioDTO o) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }


  /**
   * Es nulo.
   *
   * @return true, if successful
   */
  public boolean esNulo() {
    return this.persona == null && this.tipoDocumento == null
        && this.numero == null;
  }
}
