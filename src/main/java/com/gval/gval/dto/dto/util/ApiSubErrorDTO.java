package com.gval.gval.dto.dto.util;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * The Class ApiSubErrorDto.
 *
 * Recoge los suberrores del Api
 */
public class ApiSubErrorDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -7326910889670709364L;

  /** The object. */
  private String object;

  /** The field. */
  private String field;

  /** The rejected value. */
  private String rejectedValue;

  /** The message. */
  private String message;

  /**
   * Instantiates a new api sub error dto.
   */
  public ApiSubErrorDTO() {
    super();
  }

  /**
   * Gets the object.
   *
   * @return the object
   */
  public String getObject() {
    return object;
  }

  /**
   * Sets the object.
   *
   * @param object the new object
   */
  public void setObject(final String object) {
    this.object = object;
  }

  /**
   * Gets the field.
   *
   * @return the field
   */
  public String getField() {
    return field;
  }

  /**
   * Sets the field.
   *
   * @param field the new field
   */
  public void setField(final String field) {
    this.field = field;
  }

  /**
   * Gets the rejected value.
   *
   * @return the rejected value
   */
  public String getRejectedValue() {
    return rejectedValue;
  }

  /**
   * Sets the rejected value.
   *
   * @param rejectedValue the new rejected value
   */
  public void setRejectedValue(final String rejectedValue) {
    this.rejectedValue = rejectedValue;
  }

  /**
   * Gets the message.
   *
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  /**
   * Sets the message.
   *
   * @param message the new message
   */
  public void setMessage(final String message) {
    this.message = message;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }


}
