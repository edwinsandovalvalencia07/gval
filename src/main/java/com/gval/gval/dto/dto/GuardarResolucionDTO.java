package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;

/**
 * The Class ValidacionResolucionDTO.
 */
public class GuardarResolucionDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -8298295050057798590L;


  /** The resolucion DTO. */
  private ResolucionDTO resolucionDTO;

  /** The codigo solicitud. */
  private String codigoSolicitud;

  /** The pk usuario. */
  private Long pkUsuario;

  /** The pk resolucion. */
  private Long pkResolucion;

  /**
   * Gets the resolucion DTO.
   *
   * @return the resolucion DTO
   */
  public ResolucionDTO getResolucionDTO() {
    return resolucionDTO;
  }

  /**
   * Sets the resolucion DTO.
   *
   * @param resolucionDTO the new resolucion DTO
   */
  public void setResolucionDTO(final ResolucionDTO resolucionDTO) {
    this.resolucionDTO = resolucionDTO;
  }

  /**
   * Gets the codigo solicitud.
   *
   * @return the codigo solicitud
   */
  public String getCodigoSolicitud() {
    return codigoSolicitud;
  }

  /**
   * Sets the codigo solicitud.
   *
   * @param codigoSolicitud the new codigo solicitud
   */
  public void setCodigoSolicitud(final String codigoSolicitud) {
    this.codigoSolicitud = codigoSolicitud;
  }

  /**
   * Gets the pk usuario.
   *
   * @return the pk usuario
   */
  public Long getPkUsuario() {
    return pkUsuario;
  }

  /**
   * Sets the pk usuario.
   *
   * @param pkUsuario the new pk usuario
   */
  public void setPkUsuario(final Long pkUsuario) {
    this.pkUsuario = pkUsuario;
  }

  /**
   * Gets the pk resolucion.
   *
   * @return the pk resolucion
   */
  public Long getPkResolucion() {
    return pkResolucion;
  }

  /**
   * Sets the pk resolucion.
   *
   * @param pkResolucion the new pk resolucion
   */
  public void setPkResolucion(final Long pkResolucion) {
    this.pkResolucion = pkResolucion;
  }

}
