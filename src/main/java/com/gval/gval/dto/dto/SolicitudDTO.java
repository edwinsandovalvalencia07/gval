package com.gval.gval.dto.dto;

import com.gval.gval.common.ConstantesCommons;
import com.gval.gval.common.UtilidadesCommons;
import es.gva.dependencia.ada.common.enums.EstadoSolicitudEnum;
import es.gva.dependencia.ada.common.enums.SubestadoEnum;
import es.gva.dependencia.ada.common.enums.TipoRevisionEnum;
import es.gva.dependencia.ada.common.enums.TipoSolicitudEnum;
import com.gval.gval.dto.dto.util.BaseDTO;
import es.gva.dependencia.ada.util.Utilidades;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * The Class SolicitudDTO.
 */
public class SolicitudDTO extends BaseDTO implements Comparable<SolicitudDTO> {

  /** The Constant ESTADO_PROCESO_RESUELTO_GRADO. */
  private static final long ESTADO_PROCESO_RESUELTO_GRADO = 5L;

  /** The Constant ERROR_SOLICITUD_GUARDAR. */
  private static final String ERROR_SOLICITUD_GUARDAR =
      "error.solicitud.guardar";

  /** The Constant SIGUIENTE_SOLICITUD. */
  private static final int NUEVA_SOLICITUD = 1;

  /** The Constant EDAD_MAXIMA_CAMBIO_NIF_FICTICIO. */
  private static final int EDAD_MAXIMA_CAMBIO_NIF_FICTICIO = 3;

  /** The Constant TIPO_IDENTIFICADOR. */
  private static final long TIPO_IDENTIFICADOR = 3L;

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 4871708181923129341L;

  /** The pk solicitud. */
  private Long pkSolicitud;

  /** The codigo solicitud sidep. */
  @Length(max = 100)
  private String codigoSolicitudSidep;

  /** The estado traslado. */
  private Long estadoTraslado;

  /** The fecha efectos traslado saliente. */
  private Date fechaEfectosTrasladoSaliente;

  /** The fecha revisable. */
  private Date fechaRevisable;

  /** The fecha solicitud traslado entrante. */
  private Date fechaSolicitudTrasladoEntrante;

  /** The fecha solicitud traslado saliente. */
  private Date fechaSolicitudTrasladoSaliente;

  /** The grado. */
  @Length(max = 1)
  private String grado;

  /** The migrado documento sidep. */
  @Length(max = 1)
  private String migradoDocumentoSidep;

  /** The nivel. */
  @Length(max = 1)
  private String nivel;

  /** The comunidad autonoma entrante. */
  private ComunidadAutonomaDTO comunidadAutonomaEntrante;

  /** The comunidad autonoma saliente. */
  private ComunidadAutonomaDTO comunidadAutonomaSaliente;

  /** The entidad dependencia entrante. */
  private EntidadDependenciaDTO entidadDependenciaEntrante;

  /** The entidad dependencia saliente. */
  private EntidadDependenciaDTO entidadDependenciaSaliente;

  /** The pk estado civil. */
  private Long pkEstadoCivil;

  /** The pk expediente. */
  private Long pkExpediente;

  /** The pk motivo archivo. */
  private Long pkMotivoArchivo;

  /** The pk unidad. */
  private Long pkUnidad;

  /** The activo. */
  private Boolean activo;

  /** The ambito solicitud. */
  @Length(max = 1)
  private String ambitoSolicitud;

  /** The bloqueada. */
  private Boolean bloqueada;

  /** The bloqueo discrepancia. */
  private Boolean bloqueoDiscrepancia;

  /** The capacidad economica. */
  private BigDecimal capacidadEconomica;

  /** The codigo. */
  @Length(max = 15)
  private String codigo;

  /** The fecha efectos traslado entrante. */
  private Date fechaEfectosTrasladoEntrante;

  /** The fecha anterior reactivacion. */
  private Date fechaAnteriorReactivacion;

  /** The fecha traslado saliente. */
  private Date fechaTrasladoSaliente;

  /** The fecha creacion. */
  @NotNull
  private Date fechaCreacion;

  /** The fecha entrega. */
  private Date fechaEntrega;

  /** The fecha registro. */
  @NotNull(message = "error.solicitud.fechaRegistro")
  private Date fechaRegistro;

  /** The fecha entrada comunidad. */
  private Date fechaEntradaComunidad;

  /** The finalizada. */
  private Boolean finalizada;

  /** The fecha notificacion GYN. */
  private Date fechaNotificacionGYN;

  /** The fecha notificacion PIA. */
  private Date fechaNotificacionPIA;

  /** The fecha resolucion GYN. */
  private Date fechaResolucionGYN;

  /** The fecha resolucion PIA. */
  private Date fechaResolucionPIA;

  /** The fecha revision GYN. */
  private Date fechaRevisionGYN;

  /** The grado traslado. */
  @Length(max = 1)
  private String gradoTraslado;

  /** The pendiente informe social. */
  private Boolean pendienteInformeSocial;

  /** The pendiente informe salud. */
  private Boolean pendienteInformeSalud;

  /** The pendiente informe extendido fisico. */
  private Boolean pendienteInformeExtendidoFisico;

  /** The pendiente informe extendido mental. */
  private Boolean pendienteInformeExtendidoMental;

  /** The motivo borrado. */
  @Length(max = 250)
  private String motivoBorrado;

  /** The motivo revision. */
  @Length(max = 500)
  private String motivoRevision;

  /** The nivel traslado. */
  @Length(max = 1)
  private String nivelTraslado;

  /** The numero entidad local. */
  @Length(max = 25)
  private String numeroEntidadLocal;

  /** The numero registro. */
  @NotBlank(message = "error.solicitud.numeroRegistro")
  @Length(max = 20)
  private String numeroRegistro;

  /** The revision oficio. */
  private Boolean revisionOficio;

  /** The revision oficio es menor. */
  private Boolean revisionOficioEsMenor;

  /** The pendiente documentacion. */
  private Boolean pendienteDocumentacion;

  /** The tipo perfil apertura. */
  private Long tipoPerfilApertura;

  /** The traslado permanente. */
  private Boolean trasladoPermanente;

  /** The reactivacion. */
  private Boolean reactivacion;

  /** The revision. */
  private Boolean revision;

  /** The servicio. */
  private Boolean servicio;

  /** The tiene nivel. */
  private Boolean tieneNivel;

  /** The traslado. */
  private Boolean traslado;

  /** The traslado saliente. */
  private Boolean trasladoSaliente;

  /** The proceso urgencia validacion. */
  private Boolean procesoUrgenciaValidacion;

  /** The pendiente valoracion. */
  private Boolean pendienteValoracion;

  /** The vive solo. */
  private Boolean viveSolo;

  /** The pendiente envio. */
  private Boolean pendienteEnvio;

  /** The provincia entrante. */
  @JsonManagedReference
  private ProvinciaDTO provinciaEntrante;

  /** The provincia saliente. */
  @JsonManagedReference
  private ProvinciaDTO provinciaSaliente;

  /** The info solicitud. */
  @JsonBackReference
  private InfoSolicitudDTO infoSolicitud;

  /** The tipo solicitud. */
  @NotNull(message = "error.solicitud.tipoSolicitud")
  @JsonManagedReference
  private TipoSolicitudDTO tipoSolicitud;

  /** The expediente. */
  @JsonManagedReference
  private ExpedienteDTO expediente;

  /** Relaciona el Usuario asociado a la solicitud *. */
  private UsuarioDTO usuario;

  /** The solicitudes. */
  private List<InfoSolicitudDTO> solicitudes;


  /** The estado activo. */
  private List<EstadoSolicitudDTO> estados;

  /** The solicitud revisada. */
  private SolicitudDTO solicitudRevisada;

  /** The app origen. */
  private String appOrigen;

  /**
   * Instantiates a new solicitud DTO.
   */
  public SolicitudDTO() {
    super();
  }

  /**
   * Gets the pk solicitud.
   *
   * @return the pk solicitud
   */
  public Long getPkSolicitud() {
    return pkSolicitud;
  }

  /**
   * Sets the pk solicitud.
   *
   * @param pkSolicitud the new pk solicitud
   */
  public void setPkSolicitud(final Long pkSolicitud) {
    this.pkSolicitud = pkSolicitud;
  }

  /**
   * Gets the codigo solicitud sidep.
   *
   * @return the codigo solicitud sidep
   */
  public String getCodigoSolicitudSidep() {
    return codigoSolicitudSidep;
  }

  /**
   * Sets the codigo solicitud sidep.
   *
   * @param codigoSolicitudSidep the new codigo solicitud sidep
   */
  public void setCodigoSolicitudSidep(final String codigoSolicitudSidep) {
    this.codigoSolicitudSidep = codigoSolicitudSidep;
  }

  /**
   * Gets the estado traslado.
   *
   * @return the estado traslado
   */
  public Long getEstadoTraslado() {
    return estadoTraslado;
  }

  /**
   * Sets the estado traslado.
   *
   * @param estadoTraslado the new estado traslado
   */
  public void setEstadoTraslado(final Long estadoTraslado) {
    this.estadoTraslado = estadoTraslado;
  }

  /**
   * Gets the fecha efectos traslado saliente.
   *
   * @return the fecha efectos traslado saliente
   */
  public Date getFechaEfectosTrasladoSaliente() {
    return UtilidadesCommons.cloneDate(fechaEfectosTrasladoSaliente);
  }

  /**
   * Sets the fecha efectos traslado saliente.
   *
   * @param fechaEfectosTrasladoSaliente the new fecha efectos traslado saliente
   */
  public void setFechaEfectosTrasladoSaliente(
      final Date fechaEfectosTrasladoSaliente) {
    this.fechaEfectosTrasladoSaliente =
        UtilidadesCommons.cloneDate(fechaEfectosTrasladoSaliente);
  }

  /**
   * Gets the fecha revisable.
   *
   * @return the fecha revisable
   */
  public Date getFechaRevisable() {
    return UtilidadesCommons.cloneDate(fechaRevisable);
  }

  /**
   * Sets the fecha revisable.
   *
   * @param fechaRevisable the new fecha revisable
   */
  public void setFechaRevisable(final Date fechaRevisable) {
    this.fechaRevisable = UtilidadesCommons.cloneDate(fechaRevisable);
  }

  /**
   * Gets the fecha solicitud traslado entrante.
   *
   * @return the fecha solicitud traslado entrante
   */
  public Date getFechaSolicitudTrasladoEntrante() {
    return UtilidadesCommons.cloneDate(fechaSolicitudTrasladoEntrante);
  }

  /**
   * Sets the fecha solicitud traslado entrante.
   *
   * @param fechaSolicitudTrasladoEntrante the new fecha solicitud traslado
   *        entrante
   */
  public void setFechaSolicitudTrasladoEntrante(
      final Date fechaSolicitudTrasladoEntrante) {
    this.fechaSolicitudTrasladoEntrante =
        UtilidadesCommons.cloneDate(fechaSolicitudTrasladoEntrante);
  }

  /**
   * Gets the fecha solicitud traslado saliente.
   *
   * @return the fecha solicitud traslado saliente
   */
  public Date getFechaSolicitudTrasladoSaliente() {
    return UtilidadesCommons.cloneDate(fechaSolicitudTrasladoSaliente);
  }

  /**
   * Sets the fecha solicitud traslado saliente.
   *
   * @param fechaSolicitudTrasladoSaliente the new fecha solicitud traslado
   *        saliente
   */
  public void setFechaSolicitudTrasladoSaliente(
      final Date fechaSolicitudTrasladoSaliente) {
    this.fechaSolicitudTrasladoSaliente =
        UtilidadesCommons.cloneDate(fechaSolicitudTrasladoSaliente);
  }

  /**
   * Gets the grado.
   *
   * @return the grado
   */
  public String getGrado() {
    return grado;
  }

  /**
   * Sets the grado.
   *
   * @param grado the new grado
   */
  public void setGrado(final String grado) {
    this.grado = grado;
  }

  /**
   * Gets the nivel.
   *
   * @return the nivel
   */
  public String getNivel() {
    return nivel;
  }

  /**
   * Sets the nivel.
   *
   * @param nivel the new nivel
   */
  public void setNivel(final String nivel) {
    this.nivel = nivel;
  }

  /**
   * Gets the comunidad autonoma entrante.
   *
   * @return the comunidad autonoma entrante
   */
  public ComunidadAutonomaDTO getComunidadAutonomaEntrante() {
    return comunidadAutonomaEntrante;
  }


  /**
   * Sets the comunidad autonoma entrante.
   *
   * @param comunidadAutonomaEntrante the new comunidad autonoma entrante
   */
  public void setComunidadAutonomaEntrante(
      final ComunidadAutonomaDTO comunidadAutonomaEntrante) {
    this.comunidadAutonomaEntrante = comunidadAutonomaEntrante;
  }


  /**
   * Gets the comunidad autonoma saliente.
   *
   * @return the comunidad autonoma saliente
   */
  public ComunidadAutonomaDTO getComunidadAutonomaSaliente() {
    return comunidadAutonomaSaliente;
  }

  /**
   * Sets the comunidad autonoma saliente.
   *
   * @param comunidadAutonomaSaliente the new comunidad autonoma saliente
   */
  public void setComunidadAutonomaSaliente(
      final ComunidadAutonomaDTO comunidadAutonomaSaliente) {
    this.comunidadAutonomaSaliente = comunidadAutonomaSaliente;
  }

  /**
   * Gets the entidad dependencia entrante.
   *
   * @return the entidad dependencia entrante
   */
  public EntidadDependenciaDTO getEntidadDependenciaEntrante() {
    return entidadDependenciaEntrante;
  }

  /**
   * Sets the entidad dependencia entrante.
   *
   * @param entidadDependenciaEntrante the new entidad dependencia entrante
   */
  public void setEntidadDependenciaEntrante(
      final EntidadDependenciaDTO entidadDependenciaEntrante) {
    this.entidadDependenciaEntrante = entidadDependenciaEntrante;
  }

  /**
   * Gets the entidad dependencia saliente.
   *
   * @return the entidad dependencia saliente
   */
  public EntidadDependenciaDTO getEntidadDependenciaSaliente() {
    return entidadDependenciaSaliente;
  }

  /**
   * Sets the entidad dependencia saliente.
   *
   * @param entidadDependenciaSaliente the new entidad dependencia saliente
   */
  public void setEntidadDependenciaSaliente(
      final EntidadDependenciaDTO entidadDependenciaSaliente) {
    this.entidadDependenciaSaliente = entidadDependenciaSaliente;
  }

  /**
   * Gets the pk estado civil.
   *
   * @return the pk estado civil
   */
  public Long getPkEstadoCivil() {
    return pkEstadoCivil;
  }

  /**
   * Sets the pk estado civil.
   *
   * @param pkEstadoCivil the new pk estado civil
   */
  public void setPkEstadoCivil(final Long pkEstadoCivil) {
    this.pkEstadoCivil = pkEstadoCivil;
  }

  /**
   * Gets the pk expediente.
   *
   * @return the pk expediente
   */
  public Long getPkExpediente() {
    return pkExpediente;
  }

  /**
   * Sets the pk expediente.
   *
   * @param pkExpediente the new pk expediente
   */
  public void setPkExpediente(final Long pkExpediente) {
    this.pkExpediente = pkExpediente;
  }

  /**
   * Gets the pk motivo archivo.
   *
   * @return the pk motivo archivo
   */
  public Long getPkMotivoArchivo() {
    return pkMotivoArchivo;
  }

  /**
   * Sets the pk motivo archivo.
   *
   * @param pkMotivoArchivo the new pk motivo archivo
   */
  public void setPkMotivoArchivo(final Long pkMotivoArchivo) {
    this.pkMotivoArchivo = pkMotivoArchivo;
  }

  /**
   * Gets the provincia entrante.
   *
   * @return the provincia entrante
   */
  public ProvinciaDTO getProvinciaEntrante() {
    return provinciaEntrante;
  }

  /**
   * Sets the provincia entrante.
   *
   * @param provinciaEntrante the new provincia entrante
   */
  public void setProvinciaEntrante(final ProvinciaDTO provinciaEntrante) {
    this.provinciaEntrante = provinciaEntrante;
  }

  /**
   * Gets the provincia saliente.
   *
   * @return the provincia saliente
   */
  public ProvinciaDTO getProvinciaSaliente() {
    return provinciaSaliente;
  }

  /**
   * Sets the provincia saliente.
   *
   * @param provinciaSaliente the new provincia saliente
   */
  public void setProvinciaSaliente(final ProvinciaDTO provinciaSaliente) {
    this.provinciaSaliente = provinciaSaliente;
  }

  /**
   * Gets the info solicitud.
   *
   * @return the info solicitud
   */
  public InfoSolicitudDTO getInfoSolicitud() {
    return infoSolicitud;
  }

  /**
   * Sets the info solicitud.
   *
   * @param infoSolicitud the new info solicitud
   */
  public void setInfoSolicitud(final InfoSolicitudDTO infoSolicitud) {
    this.infoSolicitud = infoSolicitud;
  }

  /**
   * Gets the pk unidad.
   *
   * @return the pk unidad
   */
  public Long getPkUnidad() {
    return pkUnidad;
  }

  /**
   * Sets the pk unidad.
   *
   * @param pkUnidad the new pk unidad
   */
  public void setPkUnidad(final Long pkUnidad) {
    this.pkUnidad = pkUnidad;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the ambito solicitud.
   *
   * @return the ambito solicitud
   */
  public String getAmbitoSolicitud() {
    return ambitoSolicitud;
  }

  /**
   * Sets the ambito solicitud.
   *
   * @param ambitoSolicitud the new ambito solicitud
   */
  public void setAmbitoSolicitud(final String ambitoSolicitud) {
    this.ambitoSolicitud = ambitoSolicitud;
  }

  /**
   * Gets the bloqueada.
   *
   * @return the bloqueada
   */
  public Boolean getBloqueada() {
    return bloqueada;
  }

  /**
   * Sets the bloqueada.
   *
   * @param bloqueada the new bloqueada
   */
  public void setBloqueada(final Boolean bloqueada) {
    this.bloqueada = bloqueada;
  }

  /**
   * Gets the bloqueo discrepancia.
   *
   * @return the bloqueo discrepancia
   */
  public Boolean getBloqueoDiscrepancia() {
    return bloqueoDiscrepancia;
  }

  /**
   * Sets the bloqueo discrepancia.
   *
   * @param bloqueoDiscrepancia the new bloqueo discrepancia
   */
  public void setBloqueoDiscrepancia(final Boolean bloqueoDiscrepancia) {
    this.bloqueoDiscrepancia = bloqueoDiscrepancia;
  }

  /**
   * Gets the capacidad economica.
   *
   * @return the capacidad economica
   */
  public BigDecimal getCapacidadEconomica() {
    return capacidadEconomica;
  }

  /**
   * Sets the capacidad economica.
   *
   * @param capacidadEconomica the new capacidad economica
   */
  public void setCapacidadEconomica(final BigDecimal capacidadEconomica) {
    this.capacidadEconomica = capacidadEconomica;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the fecha efectos traslado entrante.
   *
   * @return the fecha efectos traslado entrante
   */
  public Date getFechaEfectosTrasladoEntrante() {
    return UtilidadesCommons.cloneDate(fechaEfectosTrasladoEntrante);
  }

  /**
   * Sets the fecha efectos traslado entrante.
   *
   * @param fechaEfectosTrasladoEntrante the new fecha efectos traslado entrante
   */
  public void setFechaEfectosTrasladoEntrante(
      final Date fechaEfectosTrasladoEntrante) {
    this.fechaEfectosTrasladoEntrante =
        UtilidadesCommons.cloneDate(fechaEfectosTrasladoEntrante);
  }


  /**
   * Gets the fecha anterior reactivacion.
   *
   * @return the fecha anterior reactivacion
   */
  public Date getFechaAnteriorReactivacion() {
    return UtilidadesCommons.cloneDate(fechaAnteriorReactivacion);
  }

  /**
   * Sets the fecha anterior reactivacion.
   *
   * @param fechaAnteriorReactivacion the new fecha anterior reactivacion
   */
  public void setFechaAnteriorReactivacion(
      final Date fechaAnteriorReactivacion) {
    this.fechaAnteriorReactivacion =
        UtilidadesCommons.cloneDate(fechaAnteriorReactivacion);
  }

  /**
   * Gets the fecha traslado saliente.
   *
   * @return the fecha traslado saliente
   */
  public Date getFechaTrasladoSaliente() {
    return UtilidadesCommons.cloneDate(fechaTrasladoSaliente);
  }

  /**
   * Sets the fecha traslado saliente.
   *
   * @param fechaTrasladoSaliente the new fecha traslado saliente
   */
  public void setFechaTrasladoSaliente(final Date fechaTrasladoSaliente) {
    this.fechaTrasladoSaliente =
        UtilidadesCommons.cloneDate(fechaTrasladoSaliente);
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha entrega.
   *
   * @return the fecha entrega
   */
  public Date getFechaEntrega() {
    return UtilidadesCommons.cloneDate(fechaEntrega);
  }

  /**
   * Sets the fecha entrega.
   *
   * @param fechaEntrega the new fecha entrega
   */
  public void setFechaEntrega(final Date fechaEntrega) {
    this.fechaEntrega = UtilidadesCommons.cloneDate(fechaEntrega);
  }

  /**
   * Gets the fecha registro.
   *
   * @return the fecha registro
   */
  public Date getFechaRegistro() {
    return UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Gets the fecha registro formateada.
   *
   * @return the fecha registro formateada
   */
  public String getFechaRegistroFormateada() {
    return fechaRegistro == null ? ""
        : new SimpleDateFormat(Utilidades.FORMAT_DATE_FECHA)
            .format(fechaRegistro);
  }


  /**
   * Sets the fecha registro.
   *
   * @param fechaRegistro the new fecha registro
   */
  public void setFechaRegistro(final Date fechaRegistro) {
    this.fechaRegistro = UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Gets the fecha entrada comunidad.
   *
   * @return the fecha entrada comunidad
   */
  public Date getFechaEntradaComunidad() {
    return UtilidadesCommons.cloneDate(fechaEntradaComunidad);
  }

  /**
   * Sets the fecha entrada comunidad.
   *
   * @param fechaEntradaComunidad the new fecha entrada comunidad
   */
  public void setFechaEntradaComunidad(final Date fechaEntradaComunidad) {
    this.fechaEntradaComunidad =
        UtilidadesCommons.cloneDate(fechaEntradaComunidad);
  }

  /**
   * Gets the finalizada.
   *
   * @return the finalizada
   */
  public Boolean getFinalizada() {
    return finalizada;
  }

  /**
   * Sets the finalizada.
   *
   * @param finalizada the new finalizada
   */
  public void setFinalizada(final Boolean finalizada) {
    this.finalizada = finalizada;
  }

  /**
   * Gets the fecha notificacion GYN.
   *
   * @return the fecha notificacion GYN
   */
  public Date getFechaNotificacionGYN() {
    return UtilidadesCommons.cloneDate(fechaNotificacionGYN);
  }

  /**
   * Sets the fecha notificacion GYN.
   *
   * @param fechaNotificacionGYN the new fecha notificacion GYN
   */
  public void setFechaNotificacionGYN(final Date fechaNotificacionGYN) {
    this.fechaNotificacionGYN =
        UtilidadesCommons.cloneDate(fechaNotificacionGYN);
  }

  /**
   * Gets the fecha notificacion PIA.
   *
   * @return the fecha notificacion PIA
   */
  public Date getFechaNotificacionPIA() {
    return UtilidadesCommons.cloneDate(fechaNotificacionPIA);
  }

  /**
   * Sets the fecha notificacion PIA.
   *
   * @param fechaNotificacionPIA the new fecha notificacion PIA
   */
  public void setFechaNotificacionPIA(final Date fechaNotificacionPIA) {
    this.fechaNotificacionPIA =
        UtilidadesCommons.cloneDate(fechaNotificacionPIA);
  }

  /**
   * Gets the fecha resolucion GYN.
   *
   * @return the fecha resolucion GYN
   */
  public Date getFechaResolucionGYN() {
    return UtilidadesCommons.cloneDate(fechaResolucionGYN);
  }

  /**
   * Sets the fecha resolucion GYN.
   *
   * @param fechaResolucionGYN the new fecha resolucion GYN
   */
  public void setFechaResolucionGYN(final Date fechaResolucionGYN) {
    this.fechaResolucionGYN = UtilidadesCommons.cloneDate(fechaResolucionGYN);
  }

  /**
   * Gets the fecha resolucion PIA.
   *
   * @return the fecha resolucion PIA
   */
  public Date getFechaResolucionPIA() {
    return UtilidadesCommons.cloneDate(fechaResolucionPIA);
  }

  /**
   * Sets the fecha resolucion PIA.
   *
   * @param fechaResolucionPIA the new fecha resolucion PIA
   */
  public void setFechaResolucionPIA(final Date fechaResolucionPIA) {
    this.fechaResolucionPIA = UtilidadesCommons.cloneDate(fechaResolucionPIA);
  }

  /**
   * Gets the fecha revision GYN.
   *
   * @return the fecha revision GYN
   */
  public Date getFechaRevisionGYN() {
    return UtilidadesCommons.cloneDate(fechaRevisionGYN);
  }

  /**
   * Sets the fecha revision GYN.
   *
   * @param fechaRevisionGYN the new fecha revision GYN
   */
  public void setFechaRevisionGYN(final Date fechaRevisionGYN) {
    this.fechaRevisionGYN = UtilidadesCommons.cloneDate(fechaRevisionGYN);
  }

  /**
   * Gets the grado traslado.
   *
   * @return the grado traslado
   */
  public String getGradoTraslado() {
    return gradoTraslado;
  }

  /**
   * Sets the grado traslado.
   *
   * @param gradoTraslado the new grado traslado
   */
  public void setGradoTraslado(final String gradoTraslado) {
    this.gradoTraslado = gradoTraslado;
  }

  /**
   * Gets the pendiente informe social.
   *
   * @return the pendienteInformeSocial
   */
  public Boolean getPendienteInformeSocial() {
    return pendienteInformeSocial;
  }

  /**
   * Sets the pendiente informe social.
   *
   * @param pendienteInformeSocial the pendienteInformeSocial to set
   */
  public void setPendienteInformeSocial(final Boolean pendienteInformeSocial) {
    this.pendienteInformeSocial = pendienteInformeSocial;
  }

  /**
   * Gets the pendiente informe salud.
   *
   * @return the pendiente informe salud
   */
  public Boolean getPendienteInformeSalud() {
    return pendienteInformeSalud;
  }

  /**
   * Sets the pendiente informe salud.
   *
   * @param pendienteInformeSalud the new pendiente informe salud
   */
  public void setPendienteInformeSalud(final Boolean pendienteInformeSalud) {
    this.pendienteInformeSalud = pendienteInformeSalud;
  }

  /**
   * Gets the pendiente informe extendido fisico.
   *
   * @return the pendiente informe extendido fisico
   */
  public Boolean getPendienteInformeExtendidoFisico() {
    return pendienteInformeExtendidoFisico;
  }

  /**
   * Sets the pendiente informe extendido fisico.
   *
   * @param pendienteInformeExtendidoFisico the new pendiente informe extendido
   *        fisico
   */
  public void setPendienteInformeExtendidoFisico(
      final Boolean pendienteInformeExtendidoFisico) {
    this.pendienteInformeExtendidoFisico = pendienteInformeExtendidoFisico;
  }

  /**
   * Gets the pendiente informe extendido mental.
   *
   * @return the pendiente informe extendido mental
   */
  public Boolean getPendienteInformeExtendidoMental() {
    return pendienteInformeExtendidoMental;
  }

  /**
   * Sets the pendiente informe extendido mental.
   *
   * @param pendienteInformeExtendidoMental the new pendiente informe extendido
   *        mental
   */
  public void setPendienteInformeExtendidoMental(
      final Boolean pendienteInformeExtendidoMental) {
    this.pendienteInformeExtendidoMental = pendienteInformeExtendidoMental;
  }

  /**
   * Gets the motivo revision.
   *
   * @return the motivo revision
   */
  public String getMotivoRevision() {
    return motivoRevision;
  }

  /**
   * Sets the motivo revision.
   *
   * @param motivoRevision the new motivo revision
   */
  public void setMotivoRevision(final String motivoRevision) {
    this.motivoRevision = motivoRevision;
  }

  /**
   * Gets the nivel traslado.
   *
   * @return the nivel traslado
   */
  public String getNivelTraslado() {
    return nivelTraslado;
  }

  /**
   * Sets the nivel traslado.
   *
   * @param nivelTraslado the new nivel traslado
   */
  public void setNivelTraslado(final String nivelTraslado) {
    this.nivelTraslado = nivelTraslado;
  }

  /**
   * Gets the numero entidad local.
   *
   * @return the numero entidad local
   */
  public String getNumeroEntidadLocal() {
    return numeroEntidadLocal;
  }

  /**
   * Sets the numero entidad local.
   *
   * @param numeroEntidadLocal the new numero entidad local
   */
  public void setNumeroEntidadLocal(final String numeroEntidadLocal) {
    this.numeroEntidadLocal = numeroEntidadLocal;
  }

  /**
   * Gets the numero registro.
   *
   * @return the numero registro
   */
  public String getNumeroRegistro() {
    return numeroRegistro;
  }

  /**
   * Sets the numero registro.
   *
   * @param numeroRegistro the new numero registro
   */
  public void setNumeroRegistro(final String numeroRegistro) {
    this.numeroRegistro = numeroRegistro;
  }

  /**
   * Gets the revision oficio.
   *
   * @return the revision oficio
   */
  public Boolean getRevisionOficio() {
    return revisionOficio;
  }

  /**
   * Sets the revision oficio.
   *
   * @param revisionOficio the new revision oficio
   */
  public void setRevisionOficio(final Boolean revisionOficio) {
    this.revisionOficio = revisionOficio;
  }

  /**
   * Gets the revision oficio es menor.
   *
   * @return the revision oficio es menor
   */
  public Boolean getRevisionOficioEsMenor() {
    return revisionOficioEsMenor;
  }

  /**
   * Sets the revision oficio es menor.
   *
   * @param revisionOficioEsMenor the new revision oficio es menor
   */
  public void setRevisionOficioEsMenor(final Boolean revisionOficioEsMenor) {
    this.revisionOficioEsMenor = revisionOficioEsMenor;
  }

  /**
   * Gets the pendiente documentacion.
   *
   * @return the pendiente documentacion
   */
  public Boolean getPendienteDocumentacion() {
    return pendienteDocumentacion;
  }

  /**
   * Sets the pendiente documentacion.
   *
   * @param pendienteDocumentacion the new pendiente documentacion
   */
  public void setPendienteDocumentacion(final Boolean pendienteDocumentacion) {
    this.pendienteDocumentacion = pendienteDocumentacion;
  }

  /**
   * Gets the tipo perfil apertura.
   *
   * @return the tipo perfil apertura
   */
  public Long getTipoPerfilApertura() {
    return tipoPerfilApertura;
  }

  /**
   * Sets the tipo perfil apertura.
   *
   * @param tipoPerfilApertura the new tipo perfil apertura
   */
  public void setTipoPerfilApertura(final Long tipoPerfilApertura) {
    this.tipoPerfilApertura = tipoPerfilApertura;
  }

  /**
   * Gets the traslado permanente.
   *
   * @return the traslado permanente
   */
  public Boolean getTrasladoPermanente() {
    return trasladoPermanente;
  }

  /**
   * Sets the traslado permanente.
   *
   * @param trasladoPermanente the new traslado permanente
   */
  public void setTrasladoPermanente(final Boolean trasladoPermanente) {
    this.trasladoPermanente = trasladoPermanente;
  }

  /**
   * Gets the reactivacion.
   *
   * @return the reactivacion
   */
  public Boolean getReactivacion() {
    return reactivacion;
  }

  /**
   * Sets the reactivacion.
   *
   * @param reactivacion the new reactivacion
   */
  public void setReactivacion(final Boolean reactivacion) {
    this.reactivacion = reactivacion;
  }

  /**
   * Gets the revision.
   *
   * @return the revision
   */
  public Boolean getRevision() {
    return revision;
  }

  /**
   * Sets the revision.
   *
   * @param revision the new revision
   */
  public void setRevision(final Boolean revision) {
    this.revision = revision;
  }

  /**
   * Gets the servicio.
   *
   * @return the servicio
   */
  public Boolean getServicio() {
    return servicio;
  }

  /**
   * Sets the servicio.
   *
   * @param servicio the new servicio
   */
  public void setServicio(final Boolean servicio) {
    this.servicio = servicio;
  }

  /**
   * Gets the tiene nivel.
   *
   * @return the tiene nivel
   */
  public Boolean getTieneNivel() {
    return tieneNivel;
  }

  /**
   * Sets the tiene nivel.
   *
   * @param tieneNivel the new tiene nivel
   */
  public void setTieneNivel(final Boolean tieneNivel) {
    this.tieneNivel = tieneNivel;
  }

  /**
   * Gets the traslado.
   *
   * @return the traslado
   */
  public Boolean getTraslado() {
    return traslado;
  }

  /**
   * Sets the traslado.
   *
   * @param traslado the new traslado
   */
  public void setTraslado(final Boolean traslado) {
    this.traslado = traslado;
  }

  /**
   * Gets the traslado saliente.
   *
   * @return the traslado saliente
   */
  public Boolean getTrasladoSaliente() {
    return trasladoSaliente;
  }

  /**
   * Sets the traslado saliente.
   *
   * @param trasladoSaliente the new traslado saliente
   */
  public void setTrasladoSaliente(final Boolean trasladoSaliente) {
    this.trasladoSaliente = trasladoSaliente;
  }

  /**
   * Gets the proceso urgencia validacion.
   *
   * @return the proceso urgencia validacion
   */
  public Boolean getProcesoUrgenciaValidacion() {
    return procesoUrgenciaValidacion;
  }

  /**
   * Sets the proceso urgencia validacion.
   *
   * @param procesoUrgenciaValidacion the new proceso urgencia validacion
   */
  public void setProcesoUrgenciaValidacion(
      final Boolean procesoUrgenciaValidacion) {
    this.procesoUrgenciaValidacion = procesoUrgenciaValidacion;
  }

  /**
   * Gets the pendiente valoracion.
   *
   * @return the pendienteValoracion
   */
  public Boolean getPendienteValoracion() {
    return pendienteValoracion;
  }

  /**
   * Sets the pendiente valoracion.
   *
   * @param pendienteValoracion the pendienteValoracion to set
   */
  public void setPendienteValoracion(final Boolean pendienteValoracion) {
    this.pendienteValoracion = pendienteValoracion;
  }

  /**
   * Gets the vive solo.
   *
   * @return the vive solo
   */
  public Boolean getViveSolo() {
    return viveSolo;
  }

  /**
   * Sets the vive solo.
   *
   * @param viveSolo the new vive solo
   */
  public void setViveSolo(final Boolean viveSolo) {
    this.viveSolo = viveSolo;
  }

  /**
   * Gets the pendiente envio.
   *
   * @return the pendiente envio
   */
  public Boolean getPendienteEnvio() {
    return pendienteEnvio;
  }

  /**
   * Sets the pendiente envio.
   *
   * @param pendienteEnvio the new pendiente envio
   */
  public void setPendienteEnvio(final Boolean pendienteEnvio) {
    this.pendienteEnvio = pendienteEnvio;
  }

  /**
   * Gets the tipo solicitud.
   *
   * @return the tipo solicitud
   */
  public TipoSolicitudDTO getTipoSolicitud() {
    return tipoSolicitud;
  }

  /**
   * Sets the tipo solicitud.
   *
   * @param tipoSolicitud the new tipo solicitud
   */
  public void setTipoSolicitud(final TipoSolicitudDTO tipoSolicitud) {
    this.tipoSolicitud = tipoSolicitud;
  }

  /**
   * Gets the motivo borrado.
   *
   * @return the motivo borrado
   */
  public String getMotivoBorrado() {
    return motivoBorrado;
  }

  /**
   * Sets the motivo borrado.
   *
   * @param motivoBorrado the new motivo borrado
   */
  public void setMotivoBorrado(final String motivoBorrado) {
    this.motivoBorrado = motivoBorrado;
  }

  /**
   * Gets the migrado documento sidep.
   *
   * @return the migrado documento sidep
   */
  public String getMigradoDocumentoSidep() {
    return migradoDocumentoSidep;
  }

  /**
   * Sets the migrado documento sidep.
   *
   * @param migradoDocumentoSidep the new migrado documento sidep
   */
  public void setMigradoDocumentoSidep(final String migradoDocumentoSidep) {
    this.migradoDocumentoSidep = migradoDocumentoSidep;
  }

  /**
   * Gets the expediente.
   *
   * @return the expediente
   */
  public ExpedienteDTO getExpediente() {
    return expediente;
  }

  /**
   * Sets the expediente.
   *
   * @param expediente the new expediente
   */
  public void setExpediente(final ExpedienteDTO expediente) {
    this.expediente = expediente;
  }

  /**
   * Gets the usuario DTO.
   *
   * @return the usuarioDTO
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario DTO.
   *
   * @param usuarioDTO the personaDTO to set
   */
  public void setUsuario(final UsuarioDTO usuarioDTO) {
    this.usuario = usuarioDTO;
  }



  /**
   * Gets the solicitudes.
   *
   * @return the solicitudes
   */
  public List<InfoSolicitudDTO> getSolicitudes() {
    return UtilidadesCommons.collectorsToList(solicitudes);
  }

  /**
   * Sets the solicitudes.
   *
   * @param solicitudes the new solicitudes
   */
  public void setSolicitudes(final List<InfoSolicitudDTO> solicitudes) {
    this.solicitudes = UtilidadesCommons.collectorsToList(solicitudes);
  }



  /**
   * Gets the estado activo.
   *
   * @return the estado activo
   */
  public List<EstadoSolicitudDTO> getEstados() {
    return UtilidadesCommons.collectorsToList(estados);
  }

  /**
   * Sets the estado activo.
   *
   * @param estados the new estado activo
   */
  public void setEstados(final List<EstadoSolicitudDTO> estados) {
    this.estados = UtilidadesCommons.collectorsToList(estados);
  }

  /**
   * Gets the estado activo.
   *
   * @return the estado activo
   */
  public EstadoSolicitudDTO getEstadoActivo() {
    return (this.estados == null) ? null
        : this.estados.stream()
            .filter(estado -> Boolean.TRUE.equals(estado.getActivo()))
            .findFirst().orElse(null);
  }

  /**
   * Gets the estado anterior.
   *
   * @return the estado anterior
   */
  public EstadoSolicitudDTO getEstadoAnterior() {
    return (this.estados == null) ? null
        : this.estados.stream()
            .filter(estado -> Boolean.FALSE.equals(estado.getActivo()))
            .max(Comparator.comparing(EstadoSolicitudDTO::getPkEstsol))
            .orElse(null);
  }

  /**
   * Gets the revision oficio enum.
   *
   * @return the revision oficio enum
   */
  public String getRevisionOficioEnum() {
    final TipoRevisionEnum tipoRevision =
        TipoRevisionEnum.fromBoolean(this.revisionOficio);
    return tipoRevision != null ? tipoRevision.getValori18() : null;
  }

  /**
   * Gets the revision oficio str.
   *
   * @return the revision oficio str
   */
  public String getRevisionOficioStr() {
    final TipoRevisionEnum tipoRevision =
        TipoRevisionEnum.fromBoolean(this.revisionOficio);
    return tipoRevision != null ? tipoRevision.getValorString() : null;
  }

  /**
   * Sets the revision oficio str.
   *
   * @param valor the new revision oficio str
   */
  public void setRevisionOficioStr(final String valor) {
    final TipoRevisionEnum tipoRevision = TipoRevisionEnum.fromString(valor);
    this.revisionOficio = tipoRevision != null ? tipoRevision.getValor() : null;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final SolicitudDTO o) {
    return 0;
  }


  /**
   * Crea una instancia de Solicitud.
   *
   * @return solicitud
   */
  public static SolicitudDTO createInstance() {
    final SolicitudDTO solicitud = new SolicitudDTO();

    solicitud.setPendienteInformeSalud(Boolean.FALSE);
    solicitud.setTraslado(Boolean.FALSE);
    solicitud.setPendienteDocumentacion(Boolean.FALSE);
    solicitud.setBloqueada(Boolean.FALSE);
    solicitud.setActivo(Boolean.TRUE);
    solicitud.setFechaCreacion(new Date());
    solicitud.setPendienteInformeExtendidoFisico(Boolean.FALSE);
    solicitud.setPendienteInformeExtendidoMental(Boolean.FALSE);
    solicitud.setPendienteValoracion(Boolean.FALSE);
    solicitud.setPendienteInformeSocial(Boolean.FALSE);
    solicitud.setTieneNivel(Boolean.FALSE);
    solicitud.setRevision(Boolean.FALSE);
    solicitud.setReactivacion(Boolean.FALSE);
    solicitud.setFinalizada(Boolean.FALSE);
    solicitud.setBloqueoDiscrepancia(Boolean.FALSE);
    solicitud.setProcesoUrgenciaValidacion(Boolean.FALSE);

    return solicitud;
  }

  /**
   * Crea una instancia de Solicitud.
   *
   * @param tipoSolicitud the tipo solicitud
   * @param expediente the expediente
   * @param ultimaSolicitud the ultima solicitud
   * @param solicitudesTotales the solicitudes totales
   * @return solicitud
   */
  public static SolicitudDTO createInstance(final String tipoSolicitud,
      final ExpedienteDTO expediente, final SolicitudDTO ultimaSolicitud,
      final int solicitudesTotales) {
    final SolicitudDTO nuevaSolicitud = SolicitudDTO.createInstance();
    nuevaSolicitud.setExpediente(expediente);

    final TipoSolicitudEnum tipoSolicitudEnum =
        TipoSolicitudEnum.fromValor(tipoSolicitud);

    if (tipoSolicitudEnum != null) {
      switch (tipoSolicitudEnum) {
        case REVISION:
          nuevaSolicitud.setPkEstadoCivil(ultimaSolicitud.getPkEstadoCivil());
          nuevaSolicitud.setRevision(Boolean.TRUE);
          nuevaSolicitud
              .setRevisionOficio(TipoRevisionEnum.DE_PARTE.getValor());
          nuevaSolicitud.setSolicitudRevisada(ultimaSolicitud);
          break;
        case VALORACION:
        case HOMOLOGACION:
          nuevaSolicitud.setRevision(Boolean.FALSE);
          break;
        default:
          break;
      }
    }

    // Anadimos una solicitud extra al total, que corresponde a la que se está
    // creando
    nuevaSolicitud.setCodigo(generarCodigoSolicitud(expediente, tipoSolicitud,
        solicitudesTotales + NUEVA_SOLICITUD));

    return nuevaSolicitud;
  }

  /**
   * Generar codigo solicitud.
   *
   * @param expediente the expediente
   * @param tipoSolicitud the tipo solicitud
   * @param solicitudesTotales the solicitudes totales
   * @return the string
   */
  public static String generarCodigoSolicitud(final ExpedienteDTO expediente,
      final String tipoSolicitud, final int solicitudesTotales) {
    // A este punto tiene que llegar siempre el expediente con numero de
    // expediente
    if (expediente.getNumeroExpediente() == null) {
      throw new IllegalArgumentException(ERROR_SOLICITUD_GUARDAR);
    }
    final int numeroSolicitud = solicitudesTotales;
    return new StringBuilder().append(expediente.getNumeroExpediente())
        .append(tipoSolicitud).append(numeroSolicitud).toString();

  }

  /**
   * Gets the solicitud revisada.
   *
   * @return the solicitud revisada
   */
  public SolicitudDTO getSolicitudRevisada() {
    return solicitudRevisada;
  }

  /**
   * Sets the solicitud revisada.
   *
   * @param solicitudRevisada the new solicitud revisada
   */
  public void setSolicitudRevisada(final SolicitudDTO solicitudRevisada) {
    this.solicitudRevisada = solicitudRevisada;
  }

  /**
   * Gets the app origen.
   *
   * @return the app origen
   */
  public String getAppOrigen() {
    return appOrigen;
  }

  /**
   * Sets the app origen.
   *
   * @param appOrigen the new app origen
   */
  public void setAppOrigen(final String appOrigen) {
    this.appOrigen = appOrigen;
  }

  /**
   * Gets the en proceso reconocimiento.
   *
   * @return the en proceso reconocimiento
   */
  public Boolean getEnProcesoReconocimiento() {
    if (getEstadoActivo() == null) {
      return Boolean.TRUE;
    }
    final EstadoDTO estado = getEstadoActivo().getEstado();
    // esproceso <= 5 AND escodigo != 'GNN'
    return NumberUtils.compare(estado.getProceso(),
        ESTADO_PROCESO_RESUELTO_GRADO) <= 0
        && UtilidadesCommons.notEquals(
            EstadoSolicitudEnum.RESOLUCION_GYN_NOTIFICADA.getCodigo(),
            estado.getCodigo());

  }

  /**
   * Gets the en proceso reconocimiento.
   *
   * @return the en proceso reconocimiento
   */
  public boolean getEnProcesoGrabacion() {
    if (getEstadoActivo() == null || getEstadoActivo().getEstado().getCodigo()
        .equals(EstadoSolicitudEnum.BORRADOR.getCodigo())) {
      return true;
    }
    final EstadoDTO estado = getEstadoActivo().getEstado();
    // estado Borrador o estado Grabada En proceso
    if (UtilidadesCommons.esNulo(() -> getEstadoActivo().getSubestado())) {
      return false;
    }
    return EstadoSolicitudEnum.GRABADA.getCodigo().equals(estado.getCodigo())
        && SubestadoEnum.EN_PROCESO.getCodigo()
            .equals(getEstadoActivo().getSubestado().getCodigo());
  }

  /**
   * The get nif ficticio menores de 3.
   *
   * @return the nif ficticio menores de 3
   */
  public Boolean getNifFicticioMenoresDe3() {
    // Se comprueba que el estado no sea null y que la edad y la fecha de
    // nacimiento no esten vacias.
    if (ObjectUtils.allNotNull(getEstadoActivo(),
        getExpediente().getSolicitante().getEdad(),
        getExpediente().getSolicitante().getFechaNacimiento())) {
      // Si es menor que 3 y tiene nif ficticio en este caso seria 3. retornara
      // true en este caso sera
      // "S"
      /*
       * TODO: Se intento realizar de la forma que indica Miguel Angel con
       * TipoIdentificadorEnum, pero no estaba retornando datos si se realiza
       * eso. Deberia añadirse los tipo de identificadores en el
       * TipoIdentificadorEnum para poder realizar la comparación ya que da NULL
       * si comparamos con el getIdentificador.
       */
      return EDAD_MAXIMA_CAMBIO_NIF_FICTICIO >= getExpediente().getSolicitante()
          .getEdad()
          && TIPO_IDENTIFICADOR == getExpediente().getSolicitante()
              .getTipoIdentificadorAda();
    }
    return Boolean.FALSE;


  }

  /**
   * Gets the en archivo no final.
   *
   * @return the en archivo no final
   */
  public Boolean getEnArchivoNoFinal() {
    if (getEstadoActivo() == null) {
      return Boolean.FALSE;
    }
    final EstadoDTO estado = getEstadoActivo().getEstado();
    // esproceso = 10 AND esfinal = 1
    return NumberUtils.compare(estado.getProceso(),
        ConstantesCommons.ESTADO_PROCESO_FINAL) == 0
        && !estado.getEsEstadoFinal();
  }


  /**
   * Gets the es no comprobada.
   *
   * @return the es no comprobada
   */
  public boolean getEsNoComprobada() {
    // si esta en estado borrador, grabada o comprobada/bloqueada devolvemos
    // true
    if (getEstadoActivo() == null) {
      return true;
    }
    return EstadoSolicitudEnum
        .esAnteriorComprobada(getEstadoActivo().getEstado().getCodigo())
        || (getEstadoActivo().getEstado().getCodigo()
            .equals(EstadoSolicitudEnum.COMPROBADA.getCodigo())
            && Boolean.TRUE.equals(getBloqueada()));
  }

  /**
   * Gets the es de tipo.
   *
   * @param tipoSolicitud the tipo solicitud
   * @return the es de tipo
   */
  public boolean getEsDeTipo(final String tipoSolicitud) {
    return tipoSolicitud.equals(getTipoSolicitud().getCodigo());
  }



  /**
   * Es traslado con grado revisable.
   *
   * @return true, if successful
   */
  public boolean esTrasladoConGradoRevisable() {
    // El campo en bbdd esta al reves, es revisable cuando esta marcada como
    // permantente
    return Boolean.TRUE.equals(trasladoPermanente);
  }

}

