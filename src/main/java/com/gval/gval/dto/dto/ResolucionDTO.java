package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;

import javax.validation.constraints.Size;

/**
 * The Class ResolucionDTO.
 */
public final class ResolucionDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 7397395703447461013L;

  /** The pk resoluci. */
  private Long pkResoluci;

  /** The solicitud. */
  private SolicitudDTO solicitud;

  /** The usuario. */
  private UsuarioDTO usuario;

  /** The reestado. */
  @Size(max = 1)
  private String estado;

  /** The retipo. */
  @Size(max = 3)
  private String tipo;

  /** The remotivo. */
  @Size(max = 250)
  private String motivo;

  /** The refechreso. */
  private Date fechaResolucion;

  /** The refechvigo. */
  private Date fechaEfecto;

  /** The rerecurrid. */
  private Boolean recurrida;

  /** The refechrecu. */
  private Date fechaRecuperacion;

  /** The rerevocada. */
  private Boolean revocada;

  /** The regrado. */
  @Size(max = 1)
  private String grado;

  /** The renivel. */
  @Size(max = 1)
  private String nivel;

  /** The reactivo. */
  private boolean activo;

  /** The refechcrea. */
  private Date fechaCreacion;

  /** The refechnoti. */
  private Date fechaNotificacion;

  /** The resolucion recorrida. */
  private ResolucionDTO resolucionRecorrida;

  /** The resolucion correccion errores GYN. */
  private ResolucionDTO resolucionCorreccionErroresGYN;

  /** The reacuerdoi. */
  private Long acuerdoi;

  /** The remodificagrado. */
  @Size(max = 25)
  private String modificagrado;

  /** The refechfirmaresol. */
  private Date fechaFirmaResolucion;

  /** The resolucion revocada. */
  private ResolucionDTO resolucionRevocada;

  /** The recentro. */
  @Size(max = 250)
  private String centro;

  /** The renotasmigracion. */
  @Size(max = 100)
  private String notasMigracion;

  /** The resolucion pia extingue. */
  private ResolucionDTO resolucionPiaExtingue;

  /** The rensisaad migracion. */
  @Size(max = 20)
  private String nsisaadMigracion;

  /** The a enviar nsisaad. */
  private boolean aEnviarNsisaad;

  /** The representa escrito. */
  @Size(max = 1)
  private String representaEscrito;

  /** The rejustificaciondoc. */
  @Size(max = 1)
  private String justificacionDocu;

  /** The reestado nsisaad. */
  @Size(max = 2)
  private String estadoNsisaad;

  /** The recaducada. */
  private Boolean caducada;

  /** The revalido. */
  private Boolean valido;

  /**
   * Gets the pk resoluci.
   *
   * @return the pk resoluci
   */
  public Long getPkResoluci() {
    return pkResoluci;
  }

  /**
   * Sets the pk resoluci.
   *
   * @param pkResoluci the new pk resoluci
   */
  public void setPkResoluci(final Long pkResoluci) {
    this.pkResoluci = pkResoluci;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public SolicitudDTO getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the new solicitud
   */
  public void setSolicitud(final SolicitudDTO solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the resolucion correccion errores GYN.
   *
   * @return the resolucion correccion errores GYN
   */
  public ResolucionDTO getResolucionCorreccionErroresGYN() {
    return resolucionCorreccionErroresGYN;
  }

  /**
   * Sets the resolucion correccion errores GYN.
   *
   * @param resolucionCorreccionErroresGYN the new resolucion correccion errores
   *        GYN
   */
  public void setResolucionCorreccionErroresGYN(
      final ResolucionDTO resolucionCorreccionErroresGYN) {
    this.resolucionCorreccionErroresGYN = resolucionCorreccionErroresGYN;
  }

  /**
   * Gets the resolucion revocada.
   *
   * @return the resolucion revocada
   */
  public ResolucionDTO getResolucionRevocada() {
    return resolucionRevocada;
  }

  /**
   * Sets the resolucion revocada.
   *
   * @param resolucionRevocada the new resolucion revocada
   */
  public void setResolucionRevocada(final ResolucionDTO resolucionRevocada) {
    this.resolucionRevocada = resolucionRevocada;
  }

  /**
   * Gets the resolucion pia extingue.
   *
   * @return the resolucion pia extingue
   */
  public ResolucionDTO getResolucionPiaExtingue() {
    return resolucionPiaExtingue;
  }

  /**
   * Sets the resolucion pia extingue.
   *
   * @param resolucionPiaExtingue the new resolucion pia extingue
   */
  public void setResolucionPiaExtingue(
      final ResolucionDTO resolucionPiaExtingue) {
    this.resolucionPiaExtingue = resolucionPiaExtingue;
  }

  /**
   * Gets the estado.
   *
   * @return the estado
   */
  public String getEstado() {
    return estado;
  }

  /**
   * Sets the estado.
   *
   * @param estado the new estado
   */
  public void setEstado(final String estado) {
    this.estado = estado;
  }

  /**
   * Gets the tipo.
   *
   * @return the tipo
   */
  public String getTipo() {
    return tipo;
  }

  /**
   * Sets the tipo.
   *
   * @param tipo the new tipo
   */
  public void setTipo(final String tipo) {
    this.tipo = tipo;
  }

  /**
   * Gets the motivo.
   *
   * @return the motivo
   */
  public String getMotivo() {
    return motivo;
  }

  /**
   * Sets the motivo.
   *
   * @param motivo the new motivo
   */
  public void setMotivo(final String motivo) {
    this.motivo = motivo;
  }

  /**
   * Gets the fecha resolucion.
   *
   * @return the fecha resolucion
   */
  public Date getFechaResolucion() {
    return UtilidadesCommons.cloneDate(fechaResolucion);
  }

  /**
   * Sets the fecha resolucion.
   *
   * @param fechaResolucion the new fecha resolucion
   */
  public void setFechaResolucion(final Date fechaResolucion) {
    this.fechaResolucion = UtilidadesCommons.cloneDate(fechaResolucion);
  }

  /**
   * Gets the fecha efecto.
   *
   * @return the fecha efecto
   */
  public Date getFechaEfecto() {
    return UtilidadesCommons.cloneDate(fechaEfecto);
  }

  /**
   * Sets the fecha efecto.
   *
   * @param fechaEfecto the new fecha efecto
   */
  public void setFechaEfecto(final Date fechaEfecto) {
    this.fechaEfecto = UtilidadesCommons.cloneDate(fechaEfecto);
  }

  /**
   * Gets the recurrida.
   *
   * @return the recurrida
   */
  public Boolean getRecurrida() {
    return recurrida;
  }

  /**
   * Sets the recurrida.
   *
   * @param recurrida the new recurrida
   */
  public void setRecurrida(final Boolean recurrida) {
    this.recurrida = recurrida;
  }

  /**
   * Gets the fecha recuperacion.
   *
   * @return the fecha recuperacion
   */
  public Date getFechaRecuperacion() {
    return UtilidadesCommons.cloneDate(fechaRecuperacion);
  }

  /**
   * Sets the fecha recuperacion.
   *
   * @param fechaRecuperacion the new fecha recuperacion
   */
  public void setFechaRecuperacion(final Date fechaRecuperacion) {
    this.fechaRecuperacion = UtilidadesCommons.cloneDate(fechaRecuperacion);
  }

  /**
   * Gets the revocada.
   *
   * @return the revocada
   */
  public Boolean getRevocada() {
    return revocada;
  }

  /**
   * Sets the revocada.
   *
   * @param revocada the new revocada
   */
  public void setRevocada(final Boolean revocada) {
    this.revocada = revocada;
  }

  /**
   * Gets the grado.
   *
   * @return the grado
   */
  public String getGrado() {
    return grado;
  }

  /**
   * Sets the grado.
   *
   * @param grado the new grado
   */
  public void setGrado(final String grado) {
    this.grado = grado;
  }

  /**
   * Gets the nivel.
   *
   * @return the nivel
   */
  public String getNivel() {
    return nivel;
  }

  /**
   * Sets the nivel.
   *
   * @param nivel the new nivel
   */
  public void setNivel(final String nivel) {
    this.nivel = nivel;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha notificacion.
   *
   * @return the fecha notificacion
   */
  public Date getFechaNotificacion() {
    return UtilidadesCommons.cloneDate(fechaNotificacion);
  }

  /**
   * Sets the fecha notificacion.
   *
   * @param fechaNotificacion the new fecha notificacion
   */
  public void setFechaNotificacion(final Date fechaNotificacion) {
    this.fechaNotificacion = UtilidadesCommons.cloneDate(fechaNotificacion);
  }

  /**
   * Gets the resolucion recorrida.
   *
   * @return the resolucion recorrida
   */
  public ResolucionDTO getResolucionRecorrida() {
    return resolucionRecorrida;
  }

  /**
   * Sets the resolucion recorrida.
   *
   * @param resolucionRecorrida the new resolucion recorrida
   */
  public void setResolucionRecorrida(ResolucionDTO resolucionRecorrida) {
    this.resolucionRecorrida = resolucionRecorrida;
  }

  /**
   * Gets the acuerdoi.
   *
   * @return the acuerdoi
   */
  public Long getAcuerdoi() {
    return acuerdoi;
  }

  /**
   * Sets the acuerdoi.
   *
   * @param acuerdoi the new acuerdoi
   */
  public void setAcuerdoi(final Long acuerdoi) {
    this.acuerdoi = acuerdoi;
  }

  /**
   * Gets the modificagrado.
   *
   * @return the modificagrado
   */
  public String getModificagrado() {
    return modificagrado;
  }

  /**
   * Sets the modificagrado.
   *
   * @param modificagrado the new modificagrado
   */
  public void setModificagrado(final String modificagrado) {
    this.modificagrado = modificagrado;
  }

  /**
   * Gets the fecha firma resolucion.
   *
   * @return the fecha firma resolucion
   */
  public Date getFechaFirmaResolucion() {
    return UtilidadesCommons.cloneDate(fechaFirmaResolucion);
  }

  /**
   * Sets the fecha firma resolucion.
   *
   * @param fechaFirmaResolucion the new fecha firma resolucion
   */
  public void setFechaFirmaResolucion(final Date fechaFirmaResolucion) {
    this.fechaFirmaResolucion =
        UtilidadesCommons.cloneDate(fechaFirmaResolucion);
  }

  /**
   * Gets the centro.
   *
   * @return the centro
   */
  public String getCentro() {
    return centro;
  }

  /**
   * Sets the centro.
   *
   * @param centro the new centro
   */
  public void setCentro(final String centro) {
    this.centro = centro;
  }

  /**
   * Gets the notas migracion.
   *
   * @return the notas migracion
   */
  public String getNotasMigracion() {
    return notasMigracion;
  }

  /**
   * Sets the notas migracion.
   *
   * @param notasMigracion the new notas migracion
   */
  public void setNotasMigracion(final String notasMigracion) {
    this.notasMigracion = notasMigracion;
  }

  /**
   * Gets the nsisaad migracion.
   *
   * @return the nsisaad migracion
   */
  public String getNsisaadMigracion() {
    return nsisaadMigracion;
  }

  /**
   * Sets the nsisaad migracion.
   *
   * @param nsisaadMigracion the new nsisaad migracion
   */
  public void setNsisaadMigracion(final String nsisaadMigracion) {
    this.nsisaadMigracion = nsisaadMigracion;
  }

  /**
   * Gets the a enviar nsisaad.
   *
   * @return the a enviar nsisaad
   */
  public boolean getaEnviarNsisaad() {
    return aEnviarNsisaad;
  }

  /**
   * Sets the a enviar nsisaad.
   *
   * @param aEnviarNsisaad the new a enviar nsisaad
   */
  public void setaEnviarNsisaad(final boolean aEnviarNsisaad) {
    this.aEnviarNsisaad = aEnviarNsisaad;
  }

  /**
   * Gets the representa escrito.
   *
   * @return the representa escrito
   */
  public String getRepresentaEscrito() {
    return representaEscrito;
  }

  /**
   * Sets the representa escrito.
   *
   * @param representaEscrito the new representa escrito
   */
  public void setRepresentaEscrito(final String representaEscrito) {
    this.representaEscrito = representaEscrito;
  }

  /**
   * Gets the justificacion docu.
   *
   * @return the justificacion docu
   */
  public String getJustificacionDocu() {
    return justificacionDocu;
  }

  /**
   * Sets the justificacion docu.
   *
   * @param justificacionDocu the new justificacion docu
   */
  public void setJustificacionDocu(final String justificacionDocu) {
    this.justificacionDocu = justificacionDocu;
  }

  /**
   * Gets the estado nsisaad.
   *
   * @return the estado nsisaad
   */
  public String getEstadoNsisaad() {
    return estadoNsisaad;
  }

  /**
   * Sets the estado nsisaad.
   *
   * @param estadoNsisaad the new estado nsisaad
   */
  public void setEstadoNsisaad(final String estadoNsisaad) {
    this.estadoNsisaad = estadoNsisaad;
  }

  /**
   * Gets the caducada.
   *
   * @return the caducada
   */
  public Boolean getCaducada() {
    return caducada;
  }

  /**
   * Sets the caducada.
   *
   * @param caducada the new caducada
   */
  public void setCaducada(final Boolean caducada) {
    this.caducada = caducada;
  }

  /**
   * Gets the valido.
   *
   * @return the valido
   */
  public Boolean getValido() {
    return valido;
  }

  /**
   * Sets the valido.
   *
   * @param valido the new valido
   */
  public void setValido(final Boolean valido) {
    this.valido = valido;
  }

}
