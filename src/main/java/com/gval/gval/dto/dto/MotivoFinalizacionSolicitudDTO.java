package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;


/**
 * The Class MotivoFinalizacionSolicitudDTO.
 */
public class MotivoFinalizacionSolicitudDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -3183914989633825542L;

  /** The pk motivo finalizacion solicitud. */
  private Long pkMotivoFinalizacionSolicitud;

  /** The activo. */
  private Boolean activo;

  /** The fecha finalizacion. */
  private Date fechaFinalizacion;

  /** The observaciones. */
  private String observaciones;

  /** The solicitud. */
  private SolicitudDTO solicitud;

  /** The motivo finalizacion. */
  private MotivoFinalizacionDTO motivoFinalizacion;

  /**
   * Instantiates a new motivo finalizacion solicitud DTO.
   */
  public MotivoFinalizacionSolicitudDTO() {
    super();
  }

  /**
   * Instantiates a new motivo finalizacion solicitud DTO.
   *
   * @param solicitud the solicitud
   * @param motivoFinalizacion the motivo finalizacion
   */
  public MotivoFinalizacionSolicitudDTO(final SolicitudDTO solicitud,
      final MotivoFinalizacionDTO motivoFinalizacion) {
    super();
    this.activo = Boolean.TRUE;
    this.fechaFinalizacion = new Date();
    this.solicitud = solicitud;
    this.motivoFinalizacion = motivoFinalizacion;
  }

  /**
   * Gets the pk motivo finalizacion solicitud.
   *
   * @return the pk motivo finalizacion solicitud
   */
  public Long getPkMotivoFinalizacionSolicitud() {
    return pkMotivoFinalizacionSolicitud;
  }

  /**
   * Sets the pk motivo finalizacion solicitud.
   *
   * @param pkMotivoFinalizacionSolicitud the new pk motivo finalizacion
   *        solicitud
   */
  public void setPkMotivoFinalizacionSolicitud(
      final Long pkMotivoFinalizacionSolicitud) {
    this.pkMotivoFinalizacionSolicitud = pkMotivoFinalizacionSolicitud;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha finalizacion.
   *
   * @return the fecha finalizacion
   */
  public Date getFechaFinalizacion() {
    return UtilidadesCommons.cloneDate(fechaFinalizacion);
  }

  /**
   * Sets the fecha finalizacion.
   *
   * @param fechaFinalizacion the new fecha finalizacion
   */
  public void setFechaFinalizacion(final Date fechaFinalizacion) {
    this.fechaFinalizacion = UtilidadesCommons.cloneDate(fechaFinalizacion);
  }

  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return observaciones;
  }

  /**
   * Sets the observaciones.
   *
   * @param observaciones the new observaciones
   */
  public void setObservaciones(final String observaciones) {
    this.observaciones = observaciones;
  }


  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public SolicitudDTO getSolicitud() {
    return solicitud;
  }


  /**
   * Sets the solicitud.
   *
   * @param solicitud the new solicitud
   */
  public void setSolicitud(final SolicitudDTO solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the motivo finalizacion.
   *
   * @return the motivo finalizacion
   */
  public MotivoFinalizacionDTO getMotivoFinalizacion() {
    return motivoFinalizacion;
  }

  /**
   * Sets the motivo finalizacion.
   *
   * @param motivoFinalizacion the new motivo finalizacion
   */
  public void setMotivoFinalizacion(
      final MotivoFinalizacionDTO motivoFinalizacion) {
    this.motivoFinalizacion = motivoFinalizacion;
  }

}
