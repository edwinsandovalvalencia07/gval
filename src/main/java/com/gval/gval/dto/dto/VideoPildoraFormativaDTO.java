package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.validator.constraints.NotBlank;
import org.zkoss.util.resource.Labels;

import java.util.Date;

import javax.validation.constraints.NotNull;

/**
 * The Class VideoPildoraFormativa.
 */
public class VideoPildoraFormativaDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 7554280302889965264L;

  /** The id pildora formativa. */
  private Long idPildoraFormativa;

  /** The nombre. */
  @NotBlank
  private String tituloLabel;

  /** The capitulo. */
  @NotNull
  private Long capitulo;

  /** The url. */
  @NotBlank
  private String url;

  /** The activo. */
  @NotNull
  private boolean activo;

  /** The fecha creacion. */
  @NotNull
  private Date fechaCreacion;

  /** The aplicacion. */
  private String aplicacion;

  /** The acces role. */
  private String accessRole;


  /**
   * Instantiates a new video pildora formativa DTO.
   */
  public VideoPildoraFormativaDTO() {
    super();
    activo = true;
    fechaCreacion = new Date();
    capitulo = 0L;
  }


  /**
   * Gets the id pildora formativa.
   *
   * @return the id pildora formativa
   */
  public Long getIdPildoraFormativa() {
    return idPildoraFormativa;
  }


  /**
   * Sets the id pildora formativa.
   *
   * @param idPildoraFormativa the new id pildora formativa
   */
  public void setIdPildoraFormativa(final Long idPildoraFormativa) {
    this.idPildoraFormativa = idPildoraFormativa;
  }


  /**
   * Gets the titulo label.
   *
   * @return the titulo label
   */
  public String getTituloLabel() {
    return tituloLabel;
  }


  /**
   * Sets the titulo label.
   *
   * @param tituloLabel the new titulo label
   */
  public void setTituloLabel(final String tituloLabel) {
    this.tituloLabel = tituloLabel;
  }


  /**
   * Gets the url.
   *
   * @return the url
   */
  public String getUrl() {
    return url;
  }


  /**
   * Sets the url.
   *
   * @param url the new url
   */
  public void setUrl(final String url) {
    this.url = url;
  }


  /**
   * Checks if is activo.
   *
   * @return true, if is activo
   */
  public boolean isActivo() {
    return activo;
  }


  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final boolean activo) {
    this.activo = activo;
  }


  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }


  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }


  /**
   * Gets the capitulo.
   *
   * @return the capitulo
   */
  public Long getCapitulo() {
    return capitulo;
  }


  /**
   * Sets the capitulo.
   *
   * @param capitulo the new capitulo
   */
  public void setCapitulo(final Long capitulo) {
    this.capitulo = capitulo;
  }

  /**
   * Gets the titulo.
   *
   * @return the titulo
   */
  public String getTitulo() {
    return Labels.getLabel(tituloLabel);
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return Labels.getLabel(tituloLabel.concat(".descripcion"), "");
  }

  /**
   * Gets the aplicacion.
   *
   * @return the aplicacion
   */
  public String getAplicacion() {
    return aplicacion;
  }


  /**
   * Sets the aplicacion.
   *
   * @param aplicacion the new aplicacion
   */
  public void setAplicacion(final String aplicacion) {
    this.aplicacion = aplicacion;
  }


  /**
   * Gets the access role.
   *
   * @return the access role
   */
  public String getAccessRole() {
    return accessRole;
  }


  /**
   * Sets the access role.
   *
   * @param accessRole the new access role
   */
  public void setAccessRole(final String accessRole) {
    this.accessRole = accessRole;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
