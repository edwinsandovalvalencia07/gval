package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;

/**
 * The Class ValidacionResolucionDTO.
 */
public class ValidacionResolucionDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 8515835657308040430L;

  /** The expediente DTO. */
  private ExpedienteDTO expedienteDTO;

  /** The solicitud DTO. */
  private SolicitudDTO solicitudDTO;

  /** The resolucion DTO. */
  private ResolucionDTO resolucionDTO;

  /** The fecha resolucion. */
  private Date fechaResolucion;

  /**
   * Instantiates a new validacion resolucion DTO.
   */
  public ValidacionResolucionDTO() {
    super();
  }

  /**
   * Gets the expediente DTO.
   *
   * @return the expedienteDTO
   */
  public ExpedienteDTO getExpedienteDTO() {
    return expedienteDTO;
  }

  /**
   * Sets the expediente DTO.
   *
   * @param expedienteDTO the expedienteDTO to set
   */
  public void setExpedienteDTO(final ExpedienteDTO expedienteDTO) {
    this.expedienteDTO = expedienteDTO;
  }

  /**
   * Gets the solicitud DTO.
   *
   * @return the solicitudDTO
   */
  public SolicitudDTO getSolicitudDTO() {
    return solicitudDTO;
  }

  /**
   * Sets the solicitud DTO.
   *
   * @param solicitudDTO the solicitudDTO to set
   */
  public void setSolicitudDTO(final SolicitudDTO solicitudDTO) {
    this.solicitudDTO = solicitudDTO;
  }

  /**
   * Gets the fecha resolucion.
   *
   * @return the fechaResolucion
   */
  public Date getFechaResolucion() {
    return UtilidadesCommons.cloneDate(fechaResolucion);
  }

  /**
   * Sets the fecha resolucion.
   *
   * @param fechaResolucion the fechaResolucion to set
   */
  public void setFechaResolucion(final Date fechaResolucion) {
    this.fechaResolucion = UtilidadesCommons.cloneDate(fechaResolucion);
  }

  /**
   * Gets the resolucion DTO.
   *
   * @return the resolucion DTO
   */
  public ResolucionDTO getResolucionDTO() {
    return resolucionDTO;
  }

  /**
   * Sets the resolucion DTO.
   *
   * @param resolucionDTO the new resolucion DTO
   */
  public void setResolucionDTO(final ResolucionDTO resolucionDTO) {
    this.resolucionDTO = resolucionDTO;
  }
}
