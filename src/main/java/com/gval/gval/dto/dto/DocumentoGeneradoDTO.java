package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;



/**
 * The Class DocumentoGeneradoDTO.
 */
public final class DocumentoGeneradoDTO extends BaseDTO
    implements Comparable<DocumentoGeneradoDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 6665366312618727459L;

  /** The pk documento. */
  private Long pkDocumento;

  /** The observaciones. */
  @Length(max = 250)
  private String observaciones;

  /** The documento aportado. */
  private Long documentoAportado;

  /** The documento. */
  @NotNull
  private DocumentoDTO documento;

  /** The usuario. */
  @NotNull
  private UsuarioDTO usuario;

  /** The direccion. */
  private DireccionAdaDTO direccion;

  /** The firma documento. */
  private FirmaDocumentoDTO firmaDocumento;

  /** The orden. */
  private String orden;

  /**
   * Constructor. Inicializa las variables no nulas.
   */
  public DocumentoGeneradoDTO() {
    super();
  }

  /**
   * Gets the pk documento.
   *
   * @return the pkDocumento
   */
  public Long getPkDocumento() {
    return pkDocumento;
  }

  /**
   * Sets the pk documento.
   *
   * @param pkDocumento the pkDocumento to set
   */
  public void setPkDocumento(final Long pkDocumento) {
    this.pkDocumento = pkDocumento;
  }

  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return observaciones;
  }

  /**
   * Sets the observaciones.
   *
   * @param observaciones the observaciones to set
   */
  public void setObservaciones(final String observaciones) {
    this.observaciones = observaciones;
  }

  /**
   * Gets the documento aportado.
   *
   * @return the documentoAportado
   */
  public Long getDocumentoAportado() {
    return documentoAportado;
  }

  /**
   * Sets the documento aportado.
   *
   * @param documentoAportado the documentoAportado to set
   */
  public void setDocumentoAportado(final Long documentoAportado) {
    this.documentoAportado = documentoAportado;
  }

  /**
   * Gets the documento.
   *
   * @return the documento
   */
  public DocumentoDTO getDocumento() {
    return documento;
  }

  /**
   * Sets the documento.
   *
   * @param documento the documento to set
   */
  public void setDocumento(final DocumentoDTO documento) {
    this.documento = documento;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the usuario to set
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the direccion.
   *
   * @return the direccion
   */
  public DireccionAdaDTO getDireccion() {
    return direccion;
  }

  /**
   * Sets the direccion.
   *
   * @param direccion the new direccion
   */
  public void setDireccion(final DireccionAdaDTO direccion) {
    this.direccion = direccion;
  }

  /**
   * Gets the firma documento.
   *
   * @return the firma documento
   */
  public FirmaDocumentoDTO getFirmaDocumento() {
    return firmaDocumento;
  }

  /**
   * Sets the firma documento.
   *
   * @param firmaDocumento the new firma documento
   */
  public void setFirmaDocumento(final FirmaDocumentoDTO firmaDocumento) {
    this.firmaDocumento = firmaDocumento;
  }

  /**
   * Gets the orden.
   *
   * @return the orden
   */
  public String getOrden() {
    return orden;
  }

  /**
   * Sets the orden.
   *
   * @param orden the new orden
   */
  public void setOrden(final String orden) {
    this.orden = orden;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final DocumentoGeneradoDTO o) {
    return 0;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
