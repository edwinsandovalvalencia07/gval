package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * The Class ComarcaDTO.
 */
public class ComarcaDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 3393966079178286208L;

  /** The pk comarca. */
  private Long pkComarca;

  /** The codigo area. */
  private Long codigoArea;

  /** The nombre. */
  private String nombre;

  /** The provincia. */
  private ProvinciaDTO provincia;

  /**
   * Instantiates a new comarca DTO.
   */
  public ComarcaDTO() {
    super();
  }

  /**
   * Instantiates a new comarca DTO.
   *
   * @param pkComarca the pk comarca
   * @param codigoArea the codigo area
   * @param nombre the nombre
   * @param provincia the provincia
   */
  public ComarcaDTO(final Long pkComarca, final Long codigoArea,
      final String nombre, final ProvinciaDTO provincia) {
    super();
    this.pkComarca = pkComarca;
    this.codigoArea = codigoArea;
    this.nombre = nombre;
    this.provincia = provincia;
  }

  /**
   * Gets the pk comarca.
   *
   * @return the pk comarca
   */
  public Long getPkComarca() {
    return pkComarca;
  }

  /**
   * Sets the pk comarca.
   *
   * @param pkComarca the new pk comarca
   */
  public void setPkComarca(final Long pkComarca) {
    this.pkComarca = pkComarca;
  }

  /**
   * Gets the codigo area.
   *
   * @return the codigo area
   */
  public Long getCodigoArea() {
    return codigoArea;
  }

  /**
   * Sets the codigo area.
   *
   * @param codigoArea the new codigo area
   */
  public void setCodigoArea(final Long codigoArea) {
    this.codigoArea = codigoArea;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the provincia.
   *
   * @return the provincia
   */
  public ProvinciaDTO getProvincia() {
    return provincia;
  }

  /**
   * Sets the provincia.
   *
   * @param provincia the new provincia
   */
  public void setProvincia(final ProvinciaDTO provincia) {
    this.provincia = provincia;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
