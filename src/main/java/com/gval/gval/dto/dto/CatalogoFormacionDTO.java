package com.gval.gval.dto.dto;


import com.gval.gval.dto.dto.util.BaseDTO;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;


/**
 * The Class CatalogoFormacionDTO.
 */
public class CatalogoFormacionDTO extends BaseDTO implements Serializable, Comparable<CatalogoFormacionDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk cat formacion. */
  private Long pkCatFormacion;

  /** The activo. */
  private Boolean activo;

  /** The especifico. */
  private Boolean especifico;

  /** The nombre. */
  private String nombre;

  /** The impartidopor. */
  private String impartidopor;

  /** The codigo. */
  private String codigo;

  /** The duracion. */
  private Long duracion;


  /**
   * Instantiates a new catalogo formacion DTO.
   */
  public CatalogoFormacionDTO() {
    super();
  }

  /**
   * Instantiates a new catalogo formacion DTO.
   *
   * @param pkCatFormacion the pk cat formacion
   * @param activo the activo
   * @param especifico the especifico
   * @param nombre the nombre
   * @param impartidopor the impartidopor
   * @param codigo the codigo
   * @param duracion the duracion
   */
  public CatalogoFormacionDTO(final Long pkCatFormacion, final Boolean activo,
      final Boolean especifico, final String nombre, final String impartidopor, final String codigo,
      final Long duracion) {
    super();
    this.pkCatFormacion = pkCatFormacion;
    this.activo = activo;
    this.especifico = especifico;
    this.nombre = nombre;
    this.impartidopor = impartidopor;
    this.codigo = codigo;
    this.duracion = duracion;
  }

  /**
   * Gets the pk cat formacion.
   *
   * @return the pk cat formacion
   */
  public Long getPkCatFormacion() {
    return pkCatFormacion;
  }

  /**
   * Sets the pk cat formacion.
   *
   * @param pkCatFormacion the new pk cat formacion
   */
  public void setPkCatFormacion(final Long pkCatFormacion) {
    this.pkCatFormacion = pkCatFormacion;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the especifico.
   *
   * @return the especifico
   */
  public Boolean getEspecifico() {
    return especifico;
  }

  /**
   * Sets the especifico.
   *
   * @param especifico the new especifico
   */
  public void setEspecifico(final Boolean especifico) {
    this.especifico = especifico;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the impartidopor.
   *
   * @return the impartidopor
   */
  public String getImpartidopor() {
    return impartidopor;
  }

  /**
   * Sets the impartidopor.
   *
   * @param impartidopor the new impartidopor
   */
  public void setImpartidopor(final String impartidopor) {
    this.impartidopor = impartidopor;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the duracion.
   *
   * @return the duracion
   */
  public Long getDuracion() {
    return duracion;
  }

  /**
   * Sets the duracion.
   *
   * @param duracion the new duracion
   */
  public void setDuracion(final Long duracion) {
    this.duracion = duracion;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final CatalogoFormacionDTO o) {
    return this.nombre.compareTo(o.nombre);
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
