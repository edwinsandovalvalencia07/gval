package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * The Class DocumentoSubsanacionDTO.
 */
public final class DocumentoSubsanacionDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -2719467268773609406L;

  /** The pk docusubs. */
  private Long pkDocusubs;

  /** The fecha entregado. */
  private Date fechaEntregado;

  /** The observaciones. */
  @Size(max = 4000)
  private String observaciones;

  /** The activo. */
  @NotNull
  private Boolean activo;

  /** The persona. */
  private PersonaAdaDTO persona;

  /** The subsanacion. */
  @NotNull
  @JsonManagedReference
  private SubsanacionDTO subsanacion;

  /** The tipo documento. */
  @NotNull
  private TipoDocumentoDTO tipoDocumento;

  /** The usuario. */
  private UsuarioDTO usuario;


  /** The bloqueante. */
  private boolean bloqueante;

  /**
   * Instantiates a new documento subsanacion DTO.
   */
  public DocumentoSubsanacionDTO() {
    super();
    this.activo = Boolean.TRUE;
    this.subsanacion = new SubsanacionDTO();
    this.tipoDocumento = new TipoDocumentoDTO();
  }

  /**
   * Instantiates a new documento subsanacion DTO.
   *
   * @param escenarioDTO the escenario DTO
   */
  public DocumentoSubsanacionDTO(final EscenarioDTO escenarioDTO) {
    this();
    tipoDocumento = escenarioDTO.getTipoDocumento();
    persona = escenarioDTO.getPersona();
    bloqueante = escenarioDTO.getBloqueante();
  }

  /**
   * Instantiates a new documento subsanacion DTO.
   *
   * @param escenarioDTO the escenario DTO
   * @param subsanacion the subsanacion
   * @param usuario the usuario
   */
  public DocumentoSubsanacionDTO(final EscenarioDTO escenarioDTO,
      final SubsanacionDTO subsanacion, final UsuarioDTO usuario) {
    this(escenarioDTO);
    this.subsanacion = subsanacion;
    this.usuario = usuario;
  }

  /**
   * Instantiates a new documento subsanacion DTO.
   *
   * @param escenarioDTO the escenario DTO
   * @param subsanacion the subsanacion
   */
  public DocumentoSubsanacionDTO(final EscenarioDTO escenarioDTO,
      final SubsanacionDTO subsanacion) {
    this(escenarioDTO, subsanacion, subsanacion.getUsuario());
  }

  /**
   * Gets the pk docusubs.
   *
   * @return the pkDocusubs
   */
  public Long getPkDocusubs() {
    return pkDocusubs;
  }

  /**
   * Sets the pk docusubs.
   *
   * @param pkDocusubs the pkDocusubs to set
   */
  public void setPkDocusubs(final Long pkDocusubs) {
    this.pkDocusubs = pkDocusubs;
  }

  /**
   * Gets the fecha entregado.
   *
   * @return the fechaEntregado
   */
  public Date getFechaEntregado() {
    return UtilidadesCommons.cloneDate(fechaEntregado);
  }

  /**
   * Sets the fecha entregado.
   *
   * @param fechaEntregado the fechaEntregado to set
   */
  public void setFechaEntregado(final Date fechaEntregado) {
    this.fechaEntregado = UtilidadesCommons.cloneDate(fechaEntregado);
  }

  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return observaciones;
  }

  /**
   * Sets the observaciones.
   *
   * @param observaciones the observaciones to set
   */
  public void setObservaciones(final String observaciones) {
    this.observaciones = observaciones;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }


  /**
   * Gets the persona.
   *
   * @return the persona
   */
  public PersonaAdaDTO getPersona() {
    return persona;
  }


  /**
   * Sets the persona.
   *
   * @param persona the new persona
   */
  public void setPersona(final PersonaAdaDTO persona) {
    this.persona = persona;
  }

  /**
   * Gets the subsanacion.
   *
   * @return the subsanacion
   */
  public SubsanacionDTO getSubsanacion() {
    return subsanacion;
  }

  /**
   * Sets the subsanacion.
   *
   * @param subsanacion the subsanacion to set
   */
  public void setSubsanacion(final SubsanacionDTO subsanacion) {
    this.subsanacion = subsanacion;
  }

  /**
   * Gets the tipo documento.
   *
   * @return the tipoDocumento
   */
  public TipoDocumentoDTO getTipoDocumento() {
    return tipoDocumento;
  }

  /**
   * Sets the tipo documento.
   *
   * @param tipoDocumento the tipoDocumento to set
   */
  public void setTipoDocumento(final TipoDocumentoDTO tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the usuario to set
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }

  /**
   * Checks if is bloqueante.
   *
   * @return true, if is bloqueante
   */
  public boolean isBloqueante() {
    return bloqueante;
  }

  /**
   * Sets the bloqueante.
   *
   * @param bloqueante the new bloqueante
   */
  public void setBloqueante(final boolean bloqueante) {
    this.bloqueante = bloqueante;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
