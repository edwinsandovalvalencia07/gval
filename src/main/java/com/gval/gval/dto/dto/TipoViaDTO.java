package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;


/**
 * The Class TipoViaDTO.
 */
public class TipoViaDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -1364588086159996019L;

  /** The pk tipo via. */
  private Long pkTipoVia;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The nombre corto. */
  private String nombreCorto;

  /** The nombre. */
  private String nombre;

  /** The activo. */
  private Boolean activo;

  /** The imserso. */
  private String imserso;

  /**
   * Instantiates a new tipo via DTO.
   */
  public TipoViaDTO() {
    super();
  }

  /**
   * Instantiates a new tipo via DTO.
   *
   * @param pkTipoVia the pk tipo via
   * @param fechaCreacion the fecha creacion
   * @param nombreCorto the nombre corto
   * @param nombre the nombre
   * @param activo the activo
   * @param imserso the imserso
   */
  public TipoViaDTO(final Long pkTipoVia, final Date fechaCreacion,
      final String nombreCorto, final String nombre, final Boolean activo,
      final String imserso) {
    super();
    this.pkTipoVia = pkTipoVia;
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
    this.nombreCorto = nombreCorto;
    this.nombre = nombre;
    this.activo = activo;
    this.imserso = imserso;
  }

  /**
   * Gets the pk tipo via.
   *
   * @return the pk tipo via
   */
  public Long getPkTipoVia() {
    return pkTipoVia;
  }

  /**
   * Sets the pk tipo via.
   *
   * @param pkTipoVia the new pk tipo via
   */
  public void setPkTipoVia(final Long pkTipoVia) {
    this.pkTipoVia = pkTipoVia;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the nombre corto.
   *
   * @return the nombre corto
   */
  public String getNombreCorto() {
    return nombreCorto;
  }

  /**
   * Sets the nombre corto.
   *
   * @param nombreCorto the new nombre corto
   */
  public void setNombreCorto(final String nombreCorto) {
    this.nombreCorto = nombreCorto;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the imserso.
   *
   * @return the imserso
   */
  public String getImserso() {
    return imserso;
  }

  /**
   * Sets the imserso.
   *
   * @param imserso the new imserso
   */
  public void setImserso(final String imserso) {
    this.imserso = imserso;
  }


}
