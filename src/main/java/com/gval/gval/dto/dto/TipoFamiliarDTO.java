package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;

/**
 * The Class TipoFamiliarDTO.
 */
public class TipoFamiliarDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -2615513337053288188L;

  /** The pk tipofami. */
  private Long pkTipofami;

  /** The activo. */
  private Boolean activo;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The imserso. */
  private Long imserso;

  /** The parentesco. */
  private String parentesco;

  /** The relacion familiar. */
  private Long relacionFamiliar;

  /**
   * Instantiates a new tipo familiar DTO.
   */
  public TipoFamiliarDTO() {
    super();
  }

  /**
   * Instantiates a new tipo familiar DTO.
   *
   * @param pkTipofami the pk tipofami
   * @param activo the activo
   * @param fechaCreacion the fecha creacion
   * @param imserso the imserso
   * @param parentesco the parentesco
   * @param relacionFamiliar the relacion familiar
   */
  public TipoFamiliarDTO(final Long pkTipofami, final Boolean activo,
      final Date fechaCreacion, final Long imserso, final String parentesco,
      final Long relacionFamiliar) {
    super();
    this.activo = activo;
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
    this.imserso = imserso;
    this.parentesco = parentesco;
    this.pkTipofami = pkTipofami;
    this.relacionFamiliar = relacionFamiliar;
  }

  /**
   * Gets the pk tipofami.
   *
   * @return the pk tipofami
   */
  public Long getPkTipofami() {
    return pkTipofami;
  }

  /**
   * Sets the pk tipofami.
   *
   * @param pkTipofami the new pk tipofami
   */
  public void setPkTipofami(final Long pkTipofami) {
    this.pkTipofami = pkTipofami;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the imserso.
   *
   * @return the imserso
   */
  public Long getImserso() {
    return imserso;
  }

  /**
   * Sets the imserso.
   *
   * @param imserso the new imserso
   */
  public void setImserso(final Long imserso) {
    this.imserso = imserso;
  }

  /**
   * Gets the parentesco.
   *
   * @return the parentesco
   */
  public String getParentesco() {
    return parentesco;
  }

  /**
   * Sets the parentesco.
   *
   * @param parentesco the new parentesco
   */
  public void setParentesco(final String parentesco) {
    this.parentesco = parentesco;
  }

  /**
   * Gets the relacion familiar.
   *
   * @return the relacion familiar
   */
  public Long getRelacionFamiliar() {
    return relacionFamiliar;
  }

  /**
   * Sets the relacion familiar.
   *
   * @param relacionFamiliar the new relacion familiar
   */
  public void setRelacionFamiliar(final Long relacionFamiliar) {
    this.relacionFamiliar = relacionFamiliar;
  }

}
