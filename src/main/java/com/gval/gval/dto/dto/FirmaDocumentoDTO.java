package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class FirmaDocumentoDTO.
 */
public class FirmaDocumentoDTO extends BaseDTO
    implements Serializable, Comparable<FirmaDocumentoDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk firma documento. */
  private Long pkFirmaDocumento;

  /** The activo. */
  private Boolean activo;

  /** The fecha hasta. */
  private Date fechaHasta;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The fecha desde. */
  private Date fechaDesde;

  /** The suplente. */
  private Boolean suplente;

  /** The cargo. */
  private CargoDTO cargo;

  /** The tipo documento. */
  private TipoDocumentoDTO tipoDocumento;



  /**
   * Gets the pk firma documento.
   *
   * @return the pk firma documento
   */
  public Long getPkFirmaDocumento() {
    return pkFirmaDocumento;
  }

  /**
   * Sets the pk firma documento.
   *
   * @param pkFirmaDocumento the new pk firma documento
   */
  public void setPkFirmaDocumento(final Long pkFirmaDocumento) {
    this.pkFirmaDocumento = pkFirmaDocumento;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha hasta.
   *
   * @return the fecha hasta
   */
  public Date getFechaHasta() {
    return UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Sets the fecha hasta.
   *
   * @param fechaHasta the new fecha hasta
   */
  public void setFechaHasta(final Date fechaHasta) {
    this.fechaHasta = UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha desde.
   *
   * @return the fecha desde
   */
  public Date getFechaDesde() {
    return UtilidadesCommons.cloneDate(fechaDesde);
  }

  /**
   * Sets the fecha desde.
   *
   * @param fechaDesde the new fecha desde
   */
  public void setFechaDesde(final Date fechaDesde) {
    this.fechaDesde = UtilidadesCommons.cloneDate(fechaDesde);
  }

  /**
   * Gets the suplente.
   *
   * @return the suplente
   */
  public Boolean getSuplente() {
    return suplente;
  }

  /**
   * Sets the suplente.
   *
   * @param suplente the new suplente
   */
  public void setSuplente(final Boolean suplente) {
    this.suplente = suplente;
  }

  /**
   * Gets the cargo.
   *
   * @return the cargo
   */
  public CargoDTO getCargo() {
    return cargo;
  }

  /**
   * Sets the cargo.
   *
   * @param cargo the new cargo
   */
  public void setCargo(final CargoDTO cargo) {
    this.cargo = cargo;
  }

  /**
   * Gets the tipo documento.
   *
   * @return the tipo documento
   */
  public TipoDocumentoDTO getTipoDocumento() {
    return tipoDocumento;
  }

  /**
   * Sets the tipo documento.
   *
   * @param tipoDocumento the new tipo documento
   */
  public void setTipoDocumento(final TipoDocumentoDTO tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final FirmaDocumentoDTO o) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
