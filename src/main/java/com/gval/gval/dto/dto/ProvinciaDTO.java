package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;

/**
 * The Class ProvinciaDTO.
 */
public class ProvinciaDTO extends BaseDTO implements Comparable<ProvinciaDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -8808219968135936387L;

  /** The pk provincia. */
  @NotNull
  private Long pkProvincia;

  /** The pk comunidad autonoma. */
  @NotNull
  private BigDecimal pkComunidadAutonoma;

  /** The activo. */
  @NotNull
  private Boolean activo;

  /** The fecha creacion. */
  @NotNull
  private Date fechaCreacion;

  /** The imserso. */
  @Length(max = 2)
  private String imserso;

  /** The nombre. */
  @NotNull
  @Length(max = 50)
  private String nombre;

  /** The comunidad autonoma. */
  @JsonManagedReference
  @NotNull
  private ComunidadAutonomaDTO comunidadAutonoma;

  /**
   * Instantiates a new provincia DTO.
   */
  public ProvinciaDTO() {
    super();
  }

  /**
   * Instantiates a new provincia DTO.
   *
   * @param pkProvincia the pk provincia
   * @param activo the activo
   * @param fechaCreacion the fecha creacion
   * @param imserso the imserso
   * @param nombre the nombre
   * @param comunidadAutonoma the comunidad autonoma
   */
  public ProvinciaDTO(final Long pkProvincia, final Boolean activo,
      final Date fechaCreacion, final String imserso, final String nombre,
      final ComunidadAutonomaDTO comunidadAutonoma) {
    super();
    this.pkProvincia = pkProvincia;
    this.activo = activo;
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
    this.imserso = imserso;
    this.nombre = nombre;
    this.comunidadAutonoma = comunidadAutonoma;
  }

  /**
   * Instantiates a new provincia DTO.
   *
   * @param imserso the imserso
   * @param nombre the nombre
   */
  public ProvinciaDTO(final String imserso, final String nombre) {
    super();
    this.imserso = imserso;
    this.nombre = nombre;
  }

  /**
   * Gets the pk provincia.
   *
   * @return the pk provincia
   */
  public Long getPkProvincia() {
    return pkProvincia;
  }

  /**
   * Sets the pk provincia.
   *
   * @param pkProvincia the new pk provincia
   */
  public void setPkProvincia(final Long pkProvincia) {
    this.pkProvincia = pkProvincia;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the imserso.
   *
   * @return the imserso
   */
  public String getImserso() {
    return imserso;
  }

  /**
   * Sets the imserso.
   *
   * @param imserso the new imserso
   */
  public void setImserso(final String imserso) {
    this.imserso = imserso;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the comunidad autonoma.
   *
   * @return the comunidad autonoma
   */
  public ComunidadAutonomaDTO getComunidadAutonoma() {
    return comunidadAutonoma;
  }

  /**
   * Sets the comunidad autonoma.
   *
   * @param comunidadAutonoma the new comunidad autonoma
   */
  public void setComunidadAutonoma(
      final ComunidadAutonomaDTO comunidadAutonoma) {
    this.comunidadAutonoma = comunidadAutonoma;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final ProvinciaDTO o) {
    return 0;
  }
}
