package com.gval.gval.dto.dto.util;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Map;

/**
 * The Class ButtonDTO.
 */
public class AccionAlertaDTO extends BaseDTO {
  
  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -4877243151964798987L;

  /** The label. */
  private String accion;

  /** The args. */
  //TODO revisar Map serializable
  @SuppressWarnings("squid:S1948")
  private final Map<String, Object> args;

  /** The command. */
  private AlertaDTO alerta;

  /**
   * Instantiates a new accion alerta DTO.
   *
   * @param accion the accion
   * @param alerta the alerta
   */
  public AccionAlertaDTO(final String accion, final AlertaDTO alerta) {
    super();
    this.accion = accion;
    this.args = null;
    this.alerta = alerta;
  }


  /**
   * Instantiates a new accion alerta DTO.
   *
   * @param accion the accion
   * @param args the args
   * @param alerta the alerta
   */
  public AccionAlertaDTO(final String accion, final Map<String, Object> args,
      final AlertaDTO alerta) {
    super();
    this.accion = accion;
    this.args = args;
    this.alerta = alerta;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getAccion() {
    return accion;
  }

  /**
   * Sets the label.
   *
   * @param accion the new label
   */
  public void setAccion(final String accion) {
    this.accion = accion;
  }

  /**
   * Gets the command.
   *
   * @return the command
   */
  public AlertaDTO getAlerta() {
    return alerta;
  }

  /**
   * Sets the command.
   *
   * @param alerta the new command
   */
  public void setAlerta(final AlertaDTO alerta) {
    this.alerta = alerta;
  }


  /**
   * Gets the args.
   *
   * @return the args
   */
  public Map<String, Object> getArgs() {
    return args;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
