package com.gval.gval.dto.dto;

import java.util.Date;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class EstudioDTO.
 */
public class RecursosDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1537422863836791464L;

  /** The pk recursos. */
  private Long pkRecursos;

  /** The estimado. */
  private Boolean estimado;

  /** The estimado jur. */
  private Boolean estimadoJur;

  /** The activo. */
  private Boolean activo;

  /** The cargado. */
  private Boolean cargado;

  /** The causa desestimacion. */
  private String causaDesestimacion;

  /** The causa inad. */
  private String causaInad;

  /** The descripcion. */
  private String descripcion;

  /** The fecha fin retro. */
  private Date fechaFinRetro;

  /** The fecha ini retro. */
  private Date fechaIniRetro;

  /** The motivo desestima. */
  private String motivoDesestima;

  /** The Motivo juridico. */
  private String motivoJuridico;

  /** The motivo. */
  private String motivo;

  /** The nombre. */
  private String nombre;

  /** The ident persona recu. */
  private String identPersonaRecu;

  /** The primer ape. */
  private String primerApe;

  /** The acepta recurso. */
  private Boolean aceptaRecurso;

  /** The segundo ape. */
  private String segundoApe;

  /** The tipo recurso pia. */
  private String tipoRecursoPia;

  /** The documento. */
  private DocumentoAportadoDTO documento;

  /** The estudio. */
  private EstudioDTO estudio;

  /** The resolucion. */
  private ResolucionDTO resolucion;

  /** The resolucion recurso. */
  private Long resolucionRecurso;

  /** The identificador. */
  private TipoIdentificadorDTO identificador;

  /** The direccion. */
  private DireccionAdaDTO direccion;


  /**
   * Instantiates a new estudioDTO.
   */
  public RecursosDTO() {
    super();
  }


  /**
   * Gets the pk recursos.
   *
   * @return the pk recursos
   */
  public Long getPkRecursos() {
    return this.pkRecursos;
  }


  /**
   * Sets the pk recursos.
   *
   * @param pkRecursos the new pk recursos
   */
  public void setPkRecursos(final Long pkRecursos) {
    this.pkRecursos = pkRecursos;
  }


  /**
   * Gets the estimado.
   *
   * @return the estimado
   */
  public Boolean getEstimado() {
    return this.estimado;
  }


  /**
   * Sets the estimado.
   *
   * @param estimado the new estimado
   */
  public void setEstimado(final Boolean estimado) {
    this.estimado = estimado;
  }


  /**
   * Gets the estimado jur.
   *
   * @return the estimado jur
   */
  public Boolean getEstimadoJur() {
    return this.estimadoJur;
  }


  /**
   * Sets the estimado jur.
   *
   * @param estimadoJur the new estimado jur
   */
  public void setEstimadoJur(final Boolean estimadoJur) {
    this.estimadoJur = estimadoJur;
  }


  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return this.activo;
  }


  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }


  /**
   * Gets the cargado.
   *
   * @return the cargado
   */
  public Boolean getCargado() {
    return this.cargado;
  }


  /**
   * Sets the cargado.
   *
   * @param cargado the new cargado
   */
  public void setCargado(final Boolean cargado) {
    this.cargado = cargado;
  }


  /**
   * Gets the causa desestimacion.
   *
   * @return the causa desestimacion
   */
  public String getCausaDesestimacion() {
    return this.causaDesestimacion;
  }


  /**
   * Sets the causa desestimacion.
   *
   * @param causaDesestimacion the new causa desestimacion
   */
  public void setCausaDesestimacion(final String causaDesestimacion) {
    this.causaDesestimacion = causaDesestimacion;
  }


  /**
   * Gets the causa inad.
   *
   * @return the causa inad
   */
  public String getCausaInad() {
    return this.causaInad;
  }


  /**
   * Sets the causa inad.
   *
   * @param causaInad the new causa inad
   */
  public void setCausaInad(final String causaInad) {
    this.causaInad = causaInad;
  }


  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return this.descripcion;
  }


  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }


  /**
   * Gets the fecha fin retro.
   *
   * @return the fecha fin retro
   */
  public Date getFechaFinRetro() {
    return UtilidadesCommons.cloneDate(this.fechaFinRetro);
  }


  /**
   * Sets the fecha fin retro.
   *
   * @param fechaFinRetro the new fecha fin retro
   */
  public void setFechaFinRetro(final Date fechaFinRetro) {
    this.fechaFinRetro = UtilidadesCommons.cloneDate(fechaFinRetro);
  }


  /**
   * Gets the fecha ini retro.
   *
   * @return the fecha ini retro
   */
  public Date getFechaIniRetro() {
    return UtilidadesCommons.cloneDate(this.fechaIniRetro);
  }


  /**
   * Sets the fecha ini retro.
   *
   * @param fechaIniRetro the new fecha ini retro
   */
  public void setFechaIniRetro(final Date fechaIniRetro) {
    this.fechaIniRetro = UtilidadesCommons.cloneDate(fechaIniRetro);
  }


  /**
   * Gets the motivo desestima.
   *
   * @return the motivo desestima
   */
  public String getMotivoDesestima() {
    return this.motivoDesestima;
  }


  /**
   * Sets the motivo desestima.
   *
   * @param motivoDesestima the new motivo desestima
   */
  public void setMotivoDesestima(final String motivoDesestima) {
    this.motivoDesestima = motivoDesestima;
  }


  /**
   * Gets the motivo juridico.
   *
   * @return the motivo juridico
   */
  public String getMotivoJuridico() {
    return this.motivoJuridico;
  }


  /**
   * Sets the motivo juridico.
   *
   * @param motivoJuridico the new motivo juridico
   */
  public void setMotivoJuridico(final String motivoJuridico) {
    this.motivoJuridico = motivoJuridico;
  }


  /**
   * Gets the motivo.
   *
   * @return the motivo
   */
  public String getMotivo() {
    return this.motivo;
  }


  /**
   * Sets the motivo.
   *
   * @param motivo the new motivo
   */
  public void setMotivo(final String motivo) {
    this.motivo = motivo;
  }


  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return this.nombre;
  }


  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }


  /**
   * Gets the ident persona recu.
   *
   * @return the ident persona recu
   */
  public String getIdentPersonaRecu() {
    return this.identPersonaRecu;
  }


  /**
   * Sets the ident persona recu.
   *
   * @param identPersonaRecu the new ident persona recu
   */
  public void setIdentPersonaRecu(final String identPersonaRecu) {
    this.identPersonaRecu = identPersonaRecu;
  }


  /**
   * Gets the primer ape.
   *
   * @return the primer ape
   */
  public String getPrimerApe() {
    return this.primerApe;
  }


  /**
   * Sets the primer ape.
   *
   * @param primerApe the new primer ape
   */
  public void setPrimerApe(final String primerApe) {
    this.primerApe = primerApe;
  }


  /**
   * Gets the acepta recurso.
   *
   * @return the acepta recurso
   */
  public Boolean getAceptaRecurso() {
    return this.aceptaRecurso;
  }


  /**
   * Sets the acepta recurso.
   *
   * @param aceptaRecurso the new acepta recurso
   */
  public void setAceptaRecurso(final Boolean aceptaRecurso) {
    this.aceptaRecurso = aceptaRecurso;
  }


  /**
   * Gets the segundo ape.
   *
   * @return the segundo ape
   */
  public String getSegundoApe() {
    return this.segundoApe;
  }


  /**
   * Sets the segundo ape.
   *
   * @param segundoApe the new segundo ape
   */
  public void setSegundoApe(final String segundoApe) {
    this.segundoApe = segundoApe;
  }


  /**
   * Gets the tipo recurso pia.
   *
   * @return the tipo recurso pia
   */
  public String getTipoRecursoPia() {
    return this.tipoRecursoPia;
  }


  /**
   * Sets the tipo recurso pia.
   *
   * @param tipoRecursoPia the new tipo recurso pia
   */
  public void setTipoRecursoPia(final String tipoRecursoPia) {
    this.tipoRecursoPia = tipoRecursoPia;
  }


  /**
   * Gets the documento.
   *
   * @return the documento
   */
  public DocumentoAportadoDTO getDocumento() {
    return this.documento;
  }


  /**
   * Sets the documento.
   *
   * @param documento the new documento
   */
  public void setDocumento(final DocumentoAportadoDTO documento) {
    this.documento = documento;
  }


  /**
   * Gets the estudio.
   *
   * @return the estudio
   */
  public EstudioDTO getEstudio() {
    return this.estudio;
  }


  /**
   * Sets the estudio.
   *
   * @param estudio the new estudio
   */
  public void setEstudio(final EstudioDTO estudio) {
    this.estudio = estudio;
  }



  /**
   * Gets the resolucion.
   *
   * @return the resolucion
   */
  public ResolucionDTO getResolucion() {
    return resolucion;
  }


  /**
   * Sets the resolucion.
   *
   * @param resolucion the new resolucion
   */
  public void setResolucion(final ResolucionDTO resolucion) {
    this.resolucion = resolucion;
  }


  /**
   * Gets the resolucion recurso.
   *
   * @return the resolucion recurso
   */
  public Long getResolucionRecurso() {
    return this.resolucionRecurso;
  }


  /**
   * Sets the resolucion recurso.
   *
   * @param resolucionRecurso the new resolucion recurso
   */
  public void setResolucionRecurso(final Long resolucionRecurso) {
    this.resolucionRecurso = resolucionRecurso;
  }


  /**
   * Gets the identificador.
   *
   * @return the identificador
   */
  public TipoIdentificadorDTO getIdentificador() {
    return this.identificador;
  }


  /**
   * Sets the identificador.
   *
   * @param identificador the new identificador
   */
  public void setIdentificador(final TipoIdentificadorDTO identificador) {
    this.identificador = identificador;
  }


  /**
   * Gets the direccion.
   *
   * @return the direccion
   */
  public DireccionAdaDTO getDireccion() {
    return this.direccion;
  }


  /**
   * Sets the direccion.
   *
   * @param direccion the new direccion
   */
  public void setDireccion(final DireccionAdaDTO direccion) {
    this.direccion = direccion;
  }
}
