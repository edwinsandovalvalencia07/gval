package com.gval.gval.dto.dto;



import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.validator.constraints.Length;

import java.util.Date;

/**
 * The Class CentroServiciosSocialesDTO.
 */
public class CentroServiciosSocialesDTO extends BaseDTO
    implements Comparable<CentroServiciosSocialesDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -661707932230093093L;

  /** The pk centro social. */
  private Long pkCentroSocial;

  /** The codigo. */
  @Length(max = 3)
  private String codigo;

  /** The nombre. */
  @Length(max = 100)
  private String nombre;

  /** The zona cobertura. */
  private UnidadDTO unidad;

  /** The activo. */
  private Boolean activo;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /**
   * Instantiates a new centro servicios sociales DTO.
   */
  public CentroServiciosSocialesDTO() {
    super();
  }

  /**
   * Gets the pk centro social.
   *
   * @return the pk centro social
   */
  public Long getPkCentroSocial() {
    return pkCentroSocial;
  }

  /**
   * Sets the pk centro social.
   *
   * @param pkCentroSocial the new pk centro social
   */
  public void setPkCentroSocial(final Long pkCentroSocial) {
    this.pkCentroSocial = pkCentroSocial;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return this.codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return this.nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the unidad.
   *
   * @return the unidad
   */
  public UnidadDTO getUnidad() {
    return unidad;
  }

  /**
   * Sets the unidad.
   *
   * @param unidad the new unidad
   */
  public void setUnidad(final UnidadDTO unidad) {
    this.unidad = unidad;
  }


  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return this.activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(this.fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final CentroServiciosSocialesDTO o) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
