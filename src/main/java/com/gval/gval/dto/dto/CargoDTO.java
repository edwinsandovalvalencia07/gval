package com.gval.gval.dto.dto;


import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;


/**
 * The Class CargoDTO.
 */
public class CargoDTO extends BaseDTO
    implements Serializable, Comparable<CargoDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk cargo. */
  private Long pkCargo;

  /** The activo. */
  private Boolean activo;

  /** The descripcion femenino. */
  private String descripcionFemenino;

  /** The descripcion masculino. */
  private String descripcionMasculino;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The firma. */
  // bi-directional many-to-one association to SdmFirma
  private FirmaDTO firma;


  /**
   * Gets the pk cargo.
   *
   * @return the pk cargo
   */
  public Long getPkCargo() {
    return pkCargo;
  }

  /**
   * Sets the pk cargo.
   *
   * @param pkCargo the new pk cargo
   */
  public void setPkCargo(final Long pkCargo) {
    this.pkCargo = pkCargo;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the descripcion femenino.
   *
   * @return the descripcion femenino
   */
  public String getDescripcionFemenino() {
    return descripcionFemenino;
  }

  /**
   * Sets the descripcion femenino.
   *
   * @param descripcionFemenino the new descripcion femenino
   */
  public void setDescripcionFemenino(final String descripcionFemenino) {
    this.descripcionFemenino = descripcionFemenino;
  }

  /**
   * Gets the descripcion masculino.
   *
   * @return the descripcion masculino
   */
  public String getDescripcionMasculino() {
    return descripcionMasculino;
  }

  /**
   * Sets the descripcion masculino.
   *
   * @param descripcionMasculino the new descripcion masculino
   */
  public void setDescripcionMasculino(final String descripcionMasculino) {
    this.descripcionMasculino = descripcionMasculino;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the firma.
   *
   * @return the firma
   */
  public FirmaDTO getFirma() {
    return firma;
  }

  /**
   * Sets the firma.
   *
   * @param firma the new firma
   */
  public void setFirma(final FirmaDTO firma) {
    this.firma = firma;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final CargoDTO o) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
