package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import org.hibernate.validator.constraints.Length;

import java.util.Date;

import javax.validation.constraints.NotNull;

/**
 * The Class InfoSolicitudDTO.
 */
public class InfoSolicitudDTO extends BaseDTO
    implements Comparable<InfoSolicitudDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 5742516199996248464L;

  /** The pk infosoli. */
  private Long pkInfosoli;

  /** The activo. */
  private Boolean activo;

  /** The anyo gran invalidez. */
  private Long anyoGranInvalidez;

  /** The anyo grado minusvalia. */
  private Long anyoGradoMinusvalia;

  /** The anyo reconocimiento ATP. */
  private Long anyoReconocimientoATP;

  /** The anyo solicitud anterior. */
  private Long anyoSolicitudAnterior;

  /** The fecha caducidad minusvalia. */
  private Date fechaCaducidadMinusvalia;

  /** The fecha caducidad ATP. */
  private Date fechaCaducidadATP;

  /** The caracter plaza. */
  @Length(max = 1)
  private String caracterPlaza;

  /** The indica gran dependencia. */
  private Long indicaGranDependencia;

  /** The cuantia anual. */
  private Long cuantiaAnual;

  /** The declaracion impuesto. */
  private Long declaracionImpuesto;

  /** The nombre perceptor. */
  @Length(max = 50)
  private String nombrePerceptor;

  /** The documento identificador. */
  @Length(max = 15)
  private String documentoIdentificador;

  /** The primer apelido perceptor. */
  @Length(max = 50)
  private String primerApelidoPerceptor;

  /** The desea permanecer centro. */
  private Boolean deseaPermanecerCentro;

  /** The segundo apelido perceptor. */
  @Length(max = 50)
  private String segundoApelidoPerceptor;

  /** The emigrante retornado. */
  private Boolean emigranteRetornado;

  /** The fecha retorno emigrante. */
  private Date fechaRetornoEmigrante;

  /** The residente legal. */
  private Boolean residenteLegal;

  /** The residente legal 2 anyos. */
  private Boolean residenteLegal2anyos;

  /** The residente legal 5 anyos. */
  private Boolean residenteLegal5anyos;

  /** The entidad publica. */
  private Long entidadPublica;

  /** The fecha creacion. */
  @NotNull
  private Date fechaCreacion;

  /** The tiene gran invalidez. */
  private Boolean tieneGranInvalidez;

  /** The tiene minusvalia. */
  private Boolean tieneMinusvalia;

  /** The puntuacion adicional. */
  private Long puntuacionAdicional;

  /** The movilidad reducida. */
  private Long movilidadReducida;

  /** The reconocimiento ATP. */
  private Boolean reconocimientoATP;

  /** The admun. */
  private Long admun;

  /** The porcentaje minusvalia. */
  private Long porcentajeMinusvalia;

  /** The puntuacion ATP. */
  private Long puntuacionATP;

  /** The solicitado anteriormente. */
  private Boolean solicitadoAnteriormente;

  /** The tele publico. */
  private Long telePublico;

  /** The prestacion recibida. */
  @Length(max = 250)
  private String prestacionRecibida;

  /** The tipo residencia. */
  @Length(max = 1)
  private String tipoResidencia;

  /** The cata serv. */
  private Long cataServ;

  /** The solicitud. */
  @NotNull
  @JsonManagedReference
  @JsonIgnore
  private SolicitudDTO solicitud;

  /** The provincia situacion dependencia. */
  private ProvinciaDTO provinciaSituacionDependencia;

  /** The provincia minusvalia. */
  private ProvinciaDTO provinciaMinusvalia;

  /** The provincia asistencia tercera persona. */
  private ProvinciaDTO provinciaAsistenciaTerceraPersona;

  /** The provincia gran invalidez. */
  private ProvinciaDTO provinciaGranInvalidez;

  /** The provincia emigrante retornado. */
  private ProvinciaDTO provinciaEmigranteRetornado;

  /** The cuantia. */
  private Double cuantiaPension;

  /** The paises pension. */
  private String paisesPension;

  /**
   * Instantiates a new info solicitud DTO.
   */
  public InfoSolicitudDTO() {
    super();
    activo = Boolean.TRUE;
    fechaCreacion = new Date();
    this.solicitud = new SolicitudDTO();
  }


  /**
   * Instantiates a new info solicitud DTO.
   *
   * @param solicitud the solicitud
   */
  public InfoSolicitudDTO(final SolicitudDTO solicitud) {
    this();
    this.solicitud = solicitud;
  }


  /**
   * Gets the pk infosoli.
   *
   * @return the pk infosoli
   */
  public Long getPkInfosoli() {
    return pkInfosoli;
  }

  /**
   * Sets the pk infosoli.
   *
   * @param pkInfosoli the new pk infosoli
   */
  public void setPkInfosoli(final Long pkInfosoli) {
    this.pkInfosoli = pkInfosoli;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the anyo gran invalidez.
   *
   * @return the anyo gran invalidez
   */
  public Long getAnyoGranInvalidez() {
    return anyoGranInvalidez;
  }

  /**
   * Sets the anyo gran invalidez.
   *
   * @param anyoGranInvalidez the new anyo gran invalidez
   */
  public void setAnyoGranInvalidez(final Long anyoGranInvalidez) {
    this.anyoGranInvalidez = anyoGranInvalidez;
  }

  /**
   * Gets the anyo grado minusvalia.
   *
   * @return the anyo grado minusvalia
   */
  public Long getAnyoGradoMinusvalia() {
    return anyoGradoMinusvalia;
  }

  /**
   * Sets the anyo grado minusvalia.
   *
   * @param anyoGradoMinusvalia the new anyo grado minusvalia
   */
  public void setAnyoGradoMinusvalia(final Long anyoGradoMinusvalia) {
    this.anyoGradoMinusvalia = anyoGradoMinusvalia;
  }

  /**
   * Gets the anyo reconocimiento ATP.
   *
   * @return the anyo reconocimiento ATP
   */
  public Long getAnyoReconocimientoATP() {
    return anyoReconocimientoATP;
  }

  /**
   * Sets the anyo reconocimiento ATP.
   *
   * @param anyoReconocimientoATP the new anyo reconocimiento ATP
   */
  public void setAnyoReconocimientoATP(final Long anyoReconocimientoATP) {
    this.anyoReconocimientoATP = anyoReconocimientoATP;
  }

  /**
   * Gets the anyo solicitud anterior.
   *
   * @return the anyo solicitud anterior
   */
  public Long getAnyoSolicitudAnterior() {
    return anyoSolicitudAnterior;
  }

  /**
   * Sets the anyo solicitud anterior.
   *
   * @param anyoSolicitudAnterior the new anyo solicitud anterior
   */
  public void setAnyoSolicitudAnterior(final Long anyoSolicitudAnterior) {
    this.anyoSolicitudAnterior = anyoSolicitudAnterior;
  }

  /**
   * Gets the fecha caducidad minusvalia.
   *
   * @return the fecha caducidad minusvalia
   */
  public Date getFechaCaducidadMinusvalia() {
    return UtilidadesCommons.cloneDate(fechaCaducidadMinusvalia);
  }

  /**
   * Sets the fecha caducidad minusvalia.
   *
   * @param fechaCaducidadMinusvalia the new fecha caducidad minusvalia
   */
  public void setFechaCaducidadMinusvalia(final Date fechaCaducidadMinusvalia) {
    this.fechaCaducidadMinusvalia =
        UtilidadesCommons.cloneDate(fechaCaducidadMinusvalia);
  }

  /**
   * Gets the fecha caducidad ATP.
   *
   * @return the fecha caducidad ATP
   */
  public Date getFechaCaducidadATP() {
    return UtilidadesCommons.cloneDate(fechaCaducidadATP);
  }

  /**
   * Sets the fecha caducidad ATP.
   *
   * @param fechaCaducidadATP the new fecha caducidad ATP
   */
  public void setFechaCaducidadATP(final Date fechaCaducidadATP) {
    this.fechaCaducidadATP = UtilidadesCommons.cloneDate(fechaCaducidadATP);
  }

  /**
   * Gets the caracter plaza.
   *
   * @return the caracter plaza
   */
  public String getCaracterPlaza() {
    return caracterPlaza;
  }

  /**
   * Sets the caracter plaza.
   *
   * @param caracterPlaza the new caracter plaza
   */
  public void setCaracterPlaza(final String caracterPlaza) {
    this.caracterPlaza = caracterPlaza;
  }

  /**
   * Gets the indica gran dependencia.
   *
   * @return the indica gran dependencia
   */
  public Long getIndicaGranDependencia() {
    return indicaGranDependencia;
  }

  /**
   * Sets the indica gran dependencia.
   *
   * @param indicaGranDependencia the new indica gran dependencia
   */
  public void setIndicaGranDependencia(final Long indicaGranDependencia) {
    this.indicaGranDependencia = indicaGranDependencia;
  }

  /**
   * Gets the cuantia anual.
   *
   * @return the cuantia anual
   */
  public Long getCuantiaAnual() {
    return cuantiaAnual;
  }

  /**
   * Sets the cuantia anual.
   *
   * @param cuantiaAnual the new cuantia anual
   */
  public void setCuantiaAnual(final Long cuantiaAnual) {
    this.cuantiaAnual = cuantiaAnual;
  }

  /**
   * Gets the declaracion impuesto.
   *
   * @return the declaracion impuesto
   */
  public Long getDeclaracionImpuesto() {
    return declaracionImpuesto;
  }

  /**
   * Sets the declaracion impuesto.
   *
   * @param declaracionImpuesto the new declaracion impuesto
   */
  public void setDeclaracionImpuesto(final Long declaracionImpuesto) {
    this.declaracionImpuesto = declaracionImpuesto;
  }

  /**
   * Gets the nombre perceptor.
   *
   * @return the nombre perceptor
   */
  public String getNombrePerceptor() {
    return nombrePerceptor;
  }

  /**
   * Sets the nombre perceptor.
   *
   * @param nombrePerceptor the new nombre perceptor
   */
  public void setNombrePerceptor(final String nombrePerceptor) {
    this.nombrePerceptor = nombrePerceptor;
  }

  /**
   * Gets the documento identificador.
   *
   * @return the documento identificador
   */
  public String getDocumentoIdentificador() {
    return documentoIdentificador;
  }

  /**
   * Sets the documento identificador.
   *
   * @param documentoIdentificador the new documento identificador
   */
  public void setDocumentoIdentificador(final String documentoIdentificador) {
    this.documentoIdentificador = documentoIdentificador;
  }

  /**
   * Gets the primer apelido perceptor.
   *
   * @return the primer apelido perceptor
   */
  public String getPrimerApelidoPerceptor() {
    return primerApelidoPerceptor;
  }

  /**
   * Sets the primer apelido perceptor.
   *
   * @param primerApelidoPerceptor the new primer apelido perceptor
   */
  public void setPrimerApelidoPerceptor(final String primerApelidoPerceptor) {
    this.primerApelidoPerceptor = primerApelidoPerceptor;
  }

  /**
   * Gets the desea permanecer centro.
   *
   * @return the desea permanecer centro
   */
  public Boolean getDeseaPermanecerCentro() {
    return deseaPermanecerCentro;
  }

  /**
   * Sets the desea permanecer centro.
   *
   * @param deseaPermanecerCentro the new desea permanecer centro
   */
  public void setDeseaPermanecerCentro(final Boolean deseaPermanecerCentro) {
    this.deseaPermanecerCentro = deseaPermanecerCentro;
  }

  /**
   * Gets the segundo apelido perceptor.
   *
   * @return the segundo apelido perceptor
   */
  public String getSegundoApelidoPerceptor() {
    return segundoApelidoPerceptor;
  }

  /**
   * Sets the segundo apelido perceptor.
   *
   * @param segundoApelidoPerceptor the new segundo apelido perceptor
   */
  public void setSegundoApelidoPerceptor(final String segundoApelidoPerceptor) {
    this.segundoApelidoPerceptor = segundoApelidoPerceptor;
  }

  /**
   * Gets the emigrante retornado.
   *
   * @return the emigrante retornado
   */
  public Boolean getEmigranteRetornado() {
    return emigranteRetornado;
  }

  /**
   * Sets the emigrante retornado.
   *
   * @param emigranteRetornado the new emigrante retornado
   */
  public void setEmigranteRetornado(final Boolean emigranteRetornado) {
    this.emigranteRetornado = emigranteRetornado;
  }

  /**
   * Gets the fecha retorno emigrante.
   *
   * @return the fecha retorno emigrante
   */
  public Date getFechaRetornoEmigrante() {
    return UtilidadesCommons.cloneDate(fechaRetornoEmigrante);
  }

  /**
   * Sets the fecha retorno emigrante.
   *
   * @param fechaRetornoEmigrante the new fecha retorno emigrante
   */
  public void setFechaRetornoEmigrante(final Date fechaRetornoEmigrante) {
    this.fechaRetornoEmigrante =
        UtilidadesCommons.cloneDate(fechaRetornoEmigrante);
  }

  /**
   * Gets the residente legal.
   *
   * @return the residente legal
   */
  public Boolean getResidenteLegal() {
    return residenteLegal;
  }

  /**
   * Sets the residente legal.
   *
   * @param residenteLegal the new residente legal
   */
  public void setResidenteLegal(final Boolean residenteLegal) {
    this.residenteLegal = residenteLegal;
  }

  /**
   * Gets the residente legal 2 anyos.
   *
   * @return the residente legal 2 anyos
   */
  public Boolean getResidenteLegal2anyos() {
    return residenteLegal2anyos;
  }

  /**
   * Sets the residente legal 2 anyos.
   *
   * @param residenteLegal2anyos the new residente legal 2 anyos
   */
  public void setResidenteLegal2anyos(final Boolean residenteLegal2anyos) {
    this.residenteLegal2anyos = residenteLegal2anyos;
  }

  /**
   * Gets the residente legal 5 anyos.
   *
   * @return the residente legal 5 anyos
   */
  public Boolean getResidenteLegal5anyos() {
    return residenteLegal5anyos;
  }

  /**
   * Sets the residente legal 5 anyos.
   *
   * @param residenteLegal5anyos the new residente legal 5 anyos
   */
  public void setResidenteLegal5anyos(final Boolean residenteLegal5anyos) {
    this.residenteLegal5anyos = residenteLegal5anyos;
  }

  /**
   * Gets the entidad publica.
   *
   * @return the entidad publica
   */
  public Long getEntidadPublica() {
    return entidadPublica;
  }

  /**
   * Sets the entidad publica.
   *
   * @param entidadPublica the new entidad publica
   */
  public void setEntidadPublica(final Long entidadPublica) {
    this.entidadPublica = entidadPublica;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the tiene gran invalidez.
   *
   * @return the tiene gran invalidez
   */
  public Boolean getTieneGranInvalidez() {
    return tieneGranInvalidez;
  }

  /**
   * Sets the tiene gran invalidez.
   *
   * @param tieneGranInvalidez the new tiene gran invalidez
   */
  public void setTieneGranInvalidez(final Boolean tieneGranInvalidez) {
    this.tieneGranInvalidez = tieneGranInvalidez;
  }

  /**
   * Gets the tiene minusvalia.
   *
   * @return the tiene minusvalia
   */
  public Boolean getTieneMinusvalia() {
    return tieneMinusvalia;
  }

  /**
   * Sets the tiene minusvalia.
   *
   * @param tieneMinusvalia the new tiene minusvalia
   */
  public void setTieneMinusvalia(final Boolean tieneMinusvalia) {
    this.tieneMinusvalia = tieneMinusvalia;
  }

  /**
   * Gets the puntuacion adicional.
   *
   * @return the puntuacion adicional
   */
  public Long getPuntuacionAdicional() {
    return puntuacionAdicional;
  }

  /**
   * Sets the puntuacion adicional.
   *
   * @param puntuacionAdicional the new puntuacion adicional
   */
  public void setPuntuacionAdicional(final Long puntuacionAdicional) {
    this.puntuacionAdicional = puntuacionAdicional;
  }

  /**
   * Gets the movilidad reducida.
   *
   * @return the movilidad reducida
   */
  public Long getMovilidadReducida() {
    return movilidadReducida;
  }

  /**
   * Sets the movilidad reducida.
   *
   * @param movilidadReducida the new movilidad reducida
   */
  public void setMovilidadReducida(final Long movilidadReducida) {
    this.movilidadReducida = movilidadReducida;
  }

  /**
   * Gets the reconocimiento ATP.
   *
   * @return the reconocimiento ATP
   */
  public Boolean getReconocimientoATP() {
    return reconocimientoATP;
  }

  /**
   * Sets the reconocimiento ATP.
   *
   * @param reconocimientoATP the new reconocimiento ATP
   */
  public void setReconocimientoATP(final Boolean reconocimientoATP) {
    this.reconocimientoATP = reconocimientoATP;
  }

  /**
   * Gets the admun.
   *
   * @return the admun
   */
  public Long getAdmun() {
    return admun;
  }

  /**
   * Sets the admun.
   *
   * @param admun the new admun
   */
  public void setAdmun(final Long admun) {
    this.admun = admun;
  }

  /**
   * Gets the porcentaje minusvalia.
   *
   * @return the porcentaje minusvalia
   */
  public Long getPorcentajeMinusvalia() {
    return porcentajeMinusvalia;
  }

  /**
   * Sets the porcentaje minusvalia.
   *
   * @param porcentajeMinusvalia the new porcentaje minusvalia
   */
  public void setPorcentajeMinusvalia(final Long porcentajeMinusvalia) {
    this.porcentajeMinusvalia = porcentajeMinusvalia;
  }

  /**
   * Gets the puntuacion ATP.
   *
   * @return the puntuacion ATP
   */
  public Long getPuntuacionATP() {
    return puntuacionATP;
  }

  /**
   * Sets the puntuacion ATP.
   *
   * @param puntuacionATP the new puntuacion ATP
   */
  public void setPuntuacionATP(final Long puntuacionATP) {
    this.puntuacionATP = puntuacionATP;
  }

  /**
   * Gets the solicitado anteriormente.
   *
   * @return the solicitado anteriormente
   */
  public Boolean getSolicitadoAnteriormente() {
    return solicitadoAnteriormente;
  }

  /**
   * Sets the solicitado anteriormente.
   *
   * @param solicitadoAnteriormente the new solicitado anteriormente
   */
  public void setSolicitadoAnteriormente(
      final Boolean solicitadoAnteriormente) {
    this.solicitadoAnteriormente = solicitadoAnteriormente;
  }

  /**
   * Gets the tele publico.
   *
   * @return the tele publico
   */
  public Long getTelePublico() {
    return telePublico;
  }

  /**
   * Sets the tele publico.
   *
   * @param telePublico the new tele publico
   */
  public void setTelePublico(final Long telePublico) {
    this.telePublico = telePublico;
  }

  /**
   * Gets the prestacion recibida.
   *
   * @return the prestacion recibida
   */
  public String getPrestacionRecibida() {
    return prestacionRecibida;
  }

  /**
   * Sets the prestacion recibida.
   *
   * @param prestacionRecibida the new prestacion recibida
   */
  public void setPrestacionRecibida(final String prestacionRecibida) {
    this.prestacionRecibida = prestacionRecibida;
  }

  /**
   * Gets the tipo residencia.
   *
   * @return the tipo residencia
   */
  public String getTipoResidencia() {
    return tipoResidencia;
  }

  /**
   * Sets the tipo residencia.
   *
   * @param tipoResidencia the new tipo residencia
   */
  public void setTipoResidencia(final String tipoResidencia) {
    this.tipoResidencia = tipoResidencia;
  }

  /**
   * Gets the cata serv.
   *
   * @return the cata serv
   */
  public Long getCataServ() {
    return cataServ;
  }

  /**
   * Sets the cata serv.
   *
   * @param cataServ the new cata serv
   */
  public void setCataServ(final Long cataServ) {
    this.cataServ = cataServ;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public SolicitudDTO getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the new solicitud
   */
  public void setSolicitud(final SolicitudDTO solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the provincia situacion dependencia.
   *
   * @return the provincia situacion dependencia
   */
  public ProvinciaDTO getProvinciaSituacionDependencia() {
    return provinciaSituacionDependencia;
  }

  /**
   * Sets the provincia situacion dependencia.
   *
   * @param provincia the new provincia situacion dependencia
   */
  public void setProvinciaSituacionDependencia(final ProvinciaDTO provincia) {
    this.provinciaSituacionDependencia = provincia;
  }

  /**
   * Gets the provincia minusvalia.
   *
   * @return the provincia minusvalia
   */
  public ProvinciaDTO getProvinciaMinusvalia() {
    return provinciaMinusvalia;
  }

  /**
   * Sets the provincia minusvalia.
   *
   * @param provincia2 the new provincia minusvalia
   */
  public void setProvinciaMinusvalia(final ProvinciaDTO provincia2) {
    this.provinciaMinusvalia = provincia2;
  }

  /**
   * Gets the provincia asistencia tercera persona.
   *
   * @return the provincia asistencia tercera persona
   */
  public ProvinciaDTO getProvinciaAsistenciaTerceraPersona() {
    return provinciaAsistenciaTerceraPersona;
  }

  /**
   * Sets the provincia asistencia tercera persona.
   *
   * @param provincia3 the new provincia asistencia tercera persona
   */
  public void setProvinciaAsistenciaTerceraPersona(
      final ProvinciaDTO provincia3) {
    this.provinciaAsistenciaTerceraPersona = provincia3;
  }

  /**
   * Gets the provincia gran invalidez.
   *
   * @return the provincia gran invalidez
   */
  public ProvinciaDTO getProvinciaGranInvalidez() {
    return provinciaGranInvalidez;
  }

  /**
   * Sets the provincia gran invalidez.
   *
   * @param provincia4 the new provincia gran invalidez
   */
  public void setProvinciaGranInvalidez(final ProvinciaDTO provincia4) {
    this.provinciaGranInvalidez = provincia4;
  }

  /**
   * Gets the provincia emigrante retornado.
   *
   * @return the provincia emigrante retornado
   */
  public ProvinciaDTO getProvinciaEmigranteRetornado() {
    return provinciaEmigranteRetornado;
  }

  /**
   * Sets the provincia emigrante retornado.
   *
   * @param provincia5 the new provincia emigrante retornado
   */
  public void setProvinciaEmigranteRetornado(final ProvinciaDTO provincia5) {
    this.provinciaEmigranteRetornado = provincia5;
  }

  /**
   * Gets the cuantia pension.
   *
   * @return the cuantia pension
   */
  public Double getCuantiaPension() {
    return cuantiaPension;
  }


  /**
   * Sets the cuantia pension.
   *
   * @param cuantiaPension the new cuantia pension
   */
  public void setCuantiaPension(final Double cuantiaPension) {
    this.cuantiaPension = cuantiaPension;
  }


  /**
   * Gets the paises pension.
   *
   * @return the paises pension
   */
  public String getPaisesPension() {
    return paisesPension;
  }


  /**
   * Sets the paises pension.
   *
   * @param paisesPension the new paises pension
   */
  public void setPaisesPension(final String paisesPension) {
    this.paisesPension = paisesPension;
  }


  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final InfoSolicitudDTO o) {
    return 0;
  }
}
