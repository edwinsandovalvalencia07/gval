package com.gval.gval.dto.dto;

import com.gval.gval.common.ConstantesCommons;
import com.gval.gval.common.UtilidadesCommons;

import com.gval.gval.common.enums.listados.IntensidadEnum;
import com.gval.gval.common.enums.listados.TipoCuidadorEnum;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;

import javax.validation.constraints.NotNull;

/**
 * The Class CuidadorDTO.
 */
//@NotNullIfAnotherFieldHasValue(fieldName = "tipoPeriodoCuidados",
//    fieldValue = "1", dependFieldName = "mesInformado",
//    message = "error.cuidador.meses-no-seleccionados")
//@NotNullIfAnotherFieldHasValue(fieldName = "datosMinimos",
//    fieldValue = "false", dependFieldName = "conviveConSolicitante",
//    message = "error.cuidador.notnull.situacionconvivencia")
//@NotNullIfAnotherFieldHasValue(fieldName = "datosMinimos", fieldValue = "false",
//    dependFieldName = "tienenParentesco",
//    message = "error.cuidador.notnull.parentesco")
//@NotNullIfAnotherFieldHasValue(fieldName = "datosMinimos", fieldValue = "false",
//    dependFieldName = "tipoFamiliar",
//    message = "error.cuidador.notnull.con-sin-parentesco")
public class CuidadorDTO extends BaseDTO implements Comparable<CuidadorDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -4310724377017055915L;

  /** The pk cuidador. */
  private Long pkCuidador;

  /** The activo. */
  @NotNull(message = "error.cuidador.notnull.activo")
  private Boolean activo;

  /** The comprom perma. */
  private Boolean compromisoPermanencia;

  /** The situ convivencia sol. */
  private Boolean conviveConSolicitante;

  /** The tipo denegacion. */
  private Long tipoDenegacion;

  /** The fecha denegacion. */
  private Date fechaDenegacion;

  /** The fecha alta conv SS. */
  private Date fechaAltaConvenioSS;

  /** The fecha baja cuidador. */
  private Date fechaBajaPia;

  /** The fecha baja SS. */
  private Date fechaBajaConvenioSS;

  /** The fecha crea registro. */
  private Date fechaCreacion;

  /** The fecha efecto cuidador. */
  private Date fechaEfecto;

  /** The fecha fin. */
  private Date fechaFinCuidados;

  /** The fecha inicio. */
  private Date fechaInicioCuidados;

  /** The comprom formacion. */
  private Boolean compromisoFormacion;

  /** The num horas mes. */
  private Long horasMesDedicacion;

  /** The intensidad. */
  private String intensidad;

  /**
   * 1 si jornada completa 0 si es jornada media null si situacion laboral no es
   * cuenta ajena.
   */
  private Boolean tipoJornada;

  /** The enero. */
  private Boolean enero;

  /** The octubre. */
  private Boolean octubre;

  /** The noviembre. */
  private Boolean noviembre;

  /** The diciembre. */
  private Boolean diciembre;

  /** The febrero. */
  private Boolean febrero;

  /** The marzo. */
  private Boolean marzo;

  /** The abril. */
  private Boolean abril;

  /** The mayo. */
  private Boolean mayo;

  /** The junio. */
  private Boolean junio;

  /** The julio. */
  private Boolean julio;

  /** The agosto. */
  private Boolean agosto;

  /** The septiembre. */
  private Boolean septiembre;

  /** The Desc motivo denegacion. */
  private String motivoDenegacion;

  /** The nuss cuidador. */
  private String nussCuidador;

  /** The suscripcion oblig conv. */
  private Boolean suscripcionObligatoriaConvenio;

  /** The parientes. */
  private Boolean tienenParentesco;

  /** The periodo cuidados. */
  /* Indica si el periodo de cuidados es por años (0) o por meses (1) */
  private Long tipoPeriodoCuidados;

  /** The prestacion desempleo. */
  private Boolean prestacionDesempleo;

  /** The otros laboral. */
  private String descripcionOtraSituacionLaboral;

  /** The subsidio desempleo. */
  private Long tipoSubsidioDesempleo;

  /** The tipo cuidador. */
  private Long tipoCuidador;


  /** The direccion. */
  // bi-directional many-to-one association to DireccionAdaDTO
  @NotNull(message = "error.cuidador.notnull.direccion")
  private DireccionAdaDTO direccion;

  /** The persona. */
  // bi-directional many-to-one association to PersonaAdaDTO
  @NotNull(message = "error.cuidador.notnull.persona")
  private PersonaAdaDTO persona;

  /** The pk pia. */
  private Long pkPia;

  /** The Pref catalogo servicio. */
  // bi-directional many-to-one association to PrefCatalogoServicio
  private PreferenciaCatalogoServicioDTO preferenciaCatalogoServicio;

  /** The situacion laboral. */
  // bi-directional many-to-one association to SdxSitlabora
  private SituacionLaboralDTO situacionLaboral;

  /** The tipo familiar. */
  // bi-directional many-to-one association to SdxTipofami
  private TipoFamiliarDTO tipoFamiliar;

  /** The vigente. */
  private boolean vigente;

  /** The fecha modificacion. */
  private Date fechaModificacion;

  /** The datos minimos. */
  private boolean datosMinimos;

  /**
   * Instantiates a new cuidador DTO.
   */
  @SuppressWarnings("squid:S2637")
  public CuidadorDTO() {
    super();
    // Creacion del registro
    this.activo = Boolean.TRUE;

    // Relativo a la persona y direccion
    final PersonaAdaDTO personaDTO =
        new PersonaAdaDTO(ConstantesCommons.VALORES_POR_DEFECTO);
    final DireccionAdaDTO direccionDTO =
        new DireccionAdaDTO(ConstantesCommons.VALORES_POR_DEFECTO);
    direccionDTO.setPersonaAdaDTO(personaDTO);
    this.direccion = direccionDTO;
    this.persona = personaDTO;

    // Propio de los cuidados
    this.intensidad = IntensidadEnum.COMPLETO.getValor();
    this.tipoCuidador = TipoCuidadorEnum.PRIMERO.getValor();

    // Meses cuidados a 0
    this.enero = Boolean.FALSE;
    this.febrero = Boolean.FALSE;
    this.marzo = Boolean.FALSE;
    this.abril = Boolean.FALSE;
    this.mayo = Boolean.FALSE;
    this.junio = Boolean.FALSE;
    this.julio = Boolean.FALSE;
    this.agosto = Boolean.FALSE;
    this.septiembre = Boolean.FALSE;
    this.octubre = Boolean.FALSE;
    this.noviembre = Boolean.FALSE;
    this.diciembre = Boolean.FALSE;
  }



  /**
   * Gets the pk cuidador.
   *
   * @return the pk cuidador
   */
  public Long getPkCuidador() {
    return pkCuidador;
  }



  /**
   * Sets the pk cuidador.
   *
   * @param pkCuidador the new pk cuidador
   */
  public void setPkCuidador(final Long pkCuidador) {
    this.pkCuidador = pkCuidador;
  }



  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }



  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }



  /**
   * Gets the compromiso permanencia.
   *
   * @return the compromiso permanencia
   */
  public Boolean getCompromisoPermanencia() {
    return compromisoPermanencia;
  }



  /**
   * Sets the compromiso permanencia.
   *
   * @param compromisoPermanencia the new compromiso permanencia
   */
  public void setCompromisoPermanencia(final Boolean compromisoPermanencia) {
    this.compromisoPermanencia = compromisoPermanencia;
  }



  /**
   * Gets the convive con solicitante.
   *
   * @return the convive con solicitante
   */
  public Boolean getConviveConSolicitante() {
    return conviveConSolicitante;
  }



  /**
   * Sets the convive con solicitante.
   *
   * @param conviveConSolicitante the new convive con solicitante
   */
  public void setConviveConSolicitante(final Boolean conviveConSolicitante) {
    this.conviveConSolicitante = conviveConSolicitante;
  }



  /**
   * Gets the tipo denegacion.
   *
   * @return the tipo denegacion
   */
  public Long getTipoDenegacion() {
    return tipoDenegacion;
  }



  /**
   * Sets the tipo denegacion.
   *
   * @param tipoDenegacion the new tipo denegacion
   */
  public void setTipoDenegacion(final Long tipoDenegacion) {
    this.tipoDenegacion = tipoDenegacion;
  }



  /**
   * Gets the fecha denegacion.
   *
   * @return the fecha denegacion
   */
  public Date getFechaDenegacion() {
    return UtilidadesCommons.cloneDate(fechaDenegacion);
  }



  /**
   * Sets the fecha denegacion.
   *
   * @param fechaDenegacion the new fecha denegacion
   */
  public void setFechaDenegacion(final Date fechaDenegacion) {
    this.fechaDenegacion = UtilidadesCommons.cloneDate(fechaDenegacion);
  }



  /**
   * Gets the fecha alta convenio SS.
   *
   * @return the fecha alta convenio SS
   */
  public Date getFechaAltaConvenioSS() {
    return UtilidadesCommons.cloneDate(fechaAltaConvenioSS);
  }



  /**
   * Sets the fecha alta convenio SS.
   *
   * @param fechaAltaConvenioSS the new fecha alta convenio SS
   */
  public void setFechaAltaConvenioSS(final Date fechaAltaConvenioSS) {
    this.fechaAltaConvenioSS = UtilidadesCommons.cloneDate(fechaAltaConvenioSS);
  }



  /**
   * Gets the fecha baja pia.
   *
   * @return the fecha baja pia
   */
  public Date getFechaBajaPia() {
    return UtilidadesCommons.cloneDate(fechaBajaPia);
  }



  /**
   * Sets the fecha baja pia.
   *
   * @param fechaBajaPia the new fecha baja pia
   */
  public void setFechaBajaPia(final Date fechaBajaPia) {
    this.fechaBajaPia = UtilidadesCommons.cloneDate(fechaBajaPia);
  }



  /**
   * Gets the fecha baja convenio SS.
   *
   * @return the fecha baja convenio SS
   */
  public Date getFechaBajaConvenioSS() {
    return UtilidadesCommons.cloneDate(fechaBajaConvenioSS);
  }



  /**
   * Sets the fecha baja convenio SS.
   *
   * @param fechaBajaConvenioSS the new fecha baja convenio SS
   */
  public void setFechaBajaConvenioSS(final Date fechaBajaConvenioSS) {
    this.fechaBajaConvenioSS = UtilidadesCommons.cloneDate(fechaBajaConvenioSS);
  }



  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }



  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }



  /**
   * Gets the fecha efecto.
   *
   * @return the fecha efecto
   */
  public Date getFechaEfecto() {
    return UtilidadesCommons.cloneDate(fechaEfecto);
  }



  /**
   * Sets the fecha efecto.
   *
   * @param fechaEfecto the new fecha efecto
   */
  public void setFechaEfecto(final Date fechaEfecto) {
    this.fechaEfecto = UtilidadesCommons.cloneDate(fechaEfecto);
  }



  /**
   * Gets the fecha fin cuidados.
   *
   * @return the fecha fin cuidados
   */
  public Date getFechaFinCuidados() {
    return UtilidadesCommons.cloneDate(fechaFinCuidados);
  }



  /**
   * Sets the fecha fin cuidados.
   *
   * @param fechaFinCuidados the new fecha fin cuidados
   */
  public void setFechaFinCuidados(final Date fechaFinCuidados) {
    this.fechaFinCuidados = UtilidadesCommons.cloneDate(fechaFinCuidados);
  }



  /**
   * Gets the fecha inicio cuidados.
   *
   * @return the fecha inicio cuidados
   */
  public Date getFechaInicioCuidados() {
    return UtilidadesCommons.cloneDate(fechaInicioCuidados);
  }



  /**
   * Sets the fecha inicio cuidados.
   *
   * @param fechaInicioCuidados the new fecha inicio cuidados
   */
  public void setFechaInicioCuidados(final Date fechaInicioCuidados) {
    this.fechaInicioCuidados = UtilidadesCommons.cloneDate(fechaInicioCuidados);
  }



  /**
   * Gets the compromiso formacion.
   *
   * @return the compromiso formacion
   */
  public Boolean getCompromisoFormacion() {
    return compromisoFormacion;
  }



  /**
   * Sets the compromiso formacion.
   *
   * @param compromisoFormacion the new compromiso formacion
   */
  public void setCompromisoFormacion(final Boolean compromisoFormacion) {
    this.compromisoFormacion = compromisoFormacion;
  }



  /**
   * Gets the horas mes dedicacion.
   *
   * @return the horas mes dedicacion
   */
  public Long getHorasMesDedicacion() {
    return horasMesDedicacion;
  }



  /**
   * Sets the horas mes dedicacion.
   *
   * @param horasMesDedicacion the new horas mes dedicacion
   */
  public void setHorasMesDedicacion(final Long horasMesDedicacion) {
    this.horasMesDedicacion = horasMesDedicacion;
  }



  /**
   * Gets the intensidad.
   *
   * @return the intensidad
   */
  public String getIntensidad() {
    return intensidad;
  }



  /**
   * Sets the intensidad.
   *
   * @param intensidad the new intensidad
   */
  public void setIntensidad(final String intensidad) {
    this.intensidad = intensidad;
  }



  /**
   * Gets the tipo jornada.
   *
   * @return the tipo jornada
   */
  public Boolean getTipoJornada() {
    return tipoJornada;
  }



  /**
   * Sets the tipo jornada.
   *
   * @param tipoJornada the new tipo jornada
   */
  public void setTipoJornada(final Boolean tipoJornada) {
    this.tipoJornada = tipoJornada;
  }



  /**
   * Gets the enero.
   *
   * @return the enero
   */
  public Boolean getEnero() {
    return enero;
  }



  /**
   * Sets the enero.
   *
   * @param enero the new enero
   */
  public void setEnero(final Boolean enero) {
    this.enero = enero;
  }



  /**
   * Gets the octubre.
   *
   * @return the octubre
   */
  public Boolean getOctubre() {
    return octubre;
  }



  /**
   * Sets the octubre.
   *
   * @param octubre the new octubre
   */
  public void setOctubre(final Boolean octubre) {
    this.octubre = octubre;
  }



  /**
   * Gets the noviembre.
   *
   * @return the noviembre
   */
  public Boolean getNoviembre() {
    return noviembre;
  }



  /**
   * Sets the noviembre.
   *
   * @param noviembre the new noviembre
   */
  public void setNoviembre(final Boolean noviembre) {
    this.noviembre = noviembre;
  }



  /**
   * Gets the diciembre.
   *
   * @return the diciembre
   */
  public Boolean getDiciembre() {
    return diciembre;
  }



  /**
   * Sets the diciembre.
   *
   * @param diciembre the new diciembre
   */
  public void setDiciembre(final Boolean diciembre) {
    this.diciembre = diciembre;
  }



  /**
   * Gets the febrero.
   *
   * @return the febrero
   */
  public Boolean getFebrero() {
    return febrero;
  }



  /**
   * Sets the febrero.
   *
   * @param febrero the new febrero
   */
  public void setFebrero(final Boolean febrero) {
    this.febrero = febrero;
  }



  /**
   * Gets the marzo.
   *
   * @return the marzo
   */
  public Boolean getMarzo() {
    return marzo;
  }



  /**
   * Sets the marzo.
   *
   * @param marzo the new marzo
   */
  public void setMarzo(final Boolean marzo) {
    this.marzo = marzo;
  }



  /**
   * Gets the abril.
   *
   * @return the abril
   */
  public Boolean getAbril() {
    return abril;
  }



  /**
   * Sets the abril.
   *
   * @param abril the new abril
   */
  public void setAbril(final Boolean abril) {
    this.abril = abril;
  }



  /**
   * Gets the mayo.
   *
   * @return the mayo
   */
  public Boolean getMayo() {
    return mayo;
  }



  /**
   * Sets the mayo.
   *
   * @param mayo the new mayo
   */
  public void setMayo(final Boolean mayo) {
    this.mayo = mayo;
  }



  /**
   * Gets the junio.
   *
   * @return the junio
   */
  public Boolean getJunio() {
    return junio;
  }



  /**
   * Sets the junio.
   *
   * @param junio the new junio
   */
  public void setJunio(final Boolean junio) {
    this.junio = junio;
  }



  /**
   * Gets the julio.
   *
   * @return the julio
   */
  public Boolean getJulio() {
    return julio;
  }



  /**
   * Sets the julio.
   *
   * @param julio the new julio
   */
  public void setJulio(final Boolean julio) {
    this.julio = julio;
  }



  /**
   * Gets the agosto.
   *
   * @return the agosto
   */
  public Boolean getAgosto() {
    return agosto;
  }



  /**
   * Sets the agosto.
   *
   * @param agosto the new agosto
   */
  public void setAgosto(final Boolean agosto) {
    this.agosto = agosto;
  }



  /**
   * Gets the septiembre.
   *
   * @return the septiembre
   */
  public Boolean getSeptiembre() {
    return septiembre;
  }



  /**
   * Sets the septiembre.
   *
   * @param septiembre the new septiembre
   */
  public void setSeptiembre(final Boolean septiembre) {
    this.septiembre = septiembre;
  }



  /**
   * Gets the motivo denegacion.
   *
   * @return the motivo denegacion
   */
  public String getMotivoDenegacion() {
    return motivoDenegacion;
  }



  /**
   * Sets the motivo denegacion.
   *
   * @param motivoDenegacion the new motivo denegacion
   */
  public void setMotivoDenegacion(final String motivoDenegacion) {
    this.motivoDenegacion = motivoDenegacion;
  }



  /**
   * Gets the nuss cuidador.
   *
   * @return the nuss cuidador
   */
  public String getNussCuidador() {
    return nussCuidador;
  }



  /**
   * Sets the nuss cuidador.
   *
   * @param nussCuidador the new nuss cuidador
   */
  public void setNussCuidador(final String nussCuidador) {
    this.nussCuidador = nussCuidador;
  }



  /**
   * Gets the suscripcion obligatoria convenio.
   *
   * @return the suscripcion obligatoria convenio
   */
  public Boolean getSuscripcionObligatoriaConvenio() {
    return suscripcionObligatoriaConvenio;
  }



  /**
   * Sets the suscripcion obligatoria convenio.
   *
   * @param suscripcionObligatoriaConvenio the new suscripcion obligatoria
   *        convenio
   */
  public void setSuscripcionObligatoriaConvenio(
      final Boolean suscripcionObligatoriaConvenio) {
    this.suscripcionObligatoriaConvenio = suscripcionObligatoriaConvenio;
  }



  /**
   * Gets the tienen parentesco.
   *
   * @return the tienen parentesco
   */
  public Boolean getTienenParentesco() {
    return tienenParentesco;
  }



  /**
   * Sets the tienen parentesco.
   *
   * @param tienenParentesco the new tienen parentesco
   */
  public void setTienenParentesco(final Boolean tienenParentesco) {
    this.tienenParentesco = tienenParentesco;
  }



  /**
   * Gets the tipo periodo cuidados.
   *
   * @return the tipo periodo cuidados
   */
  public Long getTipoPeriodoCuidados() {
    return tipoPeriodoCuidados;
  }



  /**
   * Sets the tipo periodo cuidados.
   *
   * @param tipoPeriodoCuidados the new tipo periodo cuidados
   */
  public void setTipoPeriodoCuidados(final Long tipoPeriodoCuidados) {
    this.tipoPeriodoCuidados = tipoPeriodoCuidados;
  }



  /**
   * Gets the prestacion desempleo.
   *
   * @return the prestacion desempleo
   */
  public Boolean getPrestacionDesempleo() {
    return prestacionDesempleo;
  }



  /**
   * Sets the prestacion desempleo.
   *
   * @param prestacionDesempleo the new prestacion desempleo
   */
  public void setPrestacionDesempleo(final Boolean prestacionDesempleo) {
    this.prestacionDesempleo = prestacionDesempleo;
  }



  /**
   * Gets the descripcion otra situacion laboral.
   *
   * @return the descripcion otra situacion laboral
   */
  public String getDescripcionOtraSituacionLaboral() {
    return descripcionOtraSituacionLaboral;
  }



  /**
   * Sets the descripcion otra situacion laboral.
   *
   * @param descripcionOtraSituacionLaboral the new descripcion otra situacion
   *        laboral
   */
  public void setDescripcionOtraSituacionLaboral(
      final String descripcionOtraSituacionLaboral) {
    this.descripcionOtraSituacionLaboral =
        StringUtils.trim(descripcionOtraSituacionLaboral);
  }



  /**
   * Gets the tipo subsidio desempleo.
   *
   * @return the tipo subsidio desempleo
   */
  public Long getTipoSubsidioDesempleo() {
    return tipoSubsidioDesempleo;
  }



  /**
   * Sets the tipo subsidio desempleo.
   *
   * @param tipoSubsidioDesempleo the new tipo subsidio desempleo
   */
  public void setTipoSubsidioDesempleo(final Long tipoSubsidioDesempleo) {
    this.tipoSubsidioDesempleo = tipoSubsidioDesempleo;
  }



  /**
   * Gets the tipo cuidador.
   *
   * @return the tipo cuidador
   */
  public Long getTipoCuidador() {
    return tipoCuidador;
  }



  /**
   * Gets the tipo cuidador label.
   *
   * @return the tipo cuidador label
   */
//  public String getTipoCuidadorLabel() {
//    final String label = TipoCuidadorEnum.labelFromValor(tipoCuidador);
//    return label == null ? null : Labels.getLabel(label);
//  }

  /**
   * Sets the tipo cuidador.
   *
   * @param tipoCuidador the new tipo cuidador
   */
  public void setTipoCuidador(final Long tipoCuidador) {
    this.tipoCuidador = tipoCuidador;
  }



  /**
   * Gets the direccion.
   *
   * @return the direccion
   */
  public DireccionAdaDTO getDireccion() {
    return direccion;
  }



  /**
   * Sets the direccion.
   *
   * @param direccion the new direccion
   */
  public void setDireccion(final DireccionAdaDTO direccion) {
    this.direccion = direccion;
  }



  /**
   * Gets the persona.
   *
   * @return the persona
   */
  public PersonaAdaDTO getPersona() {
    return persona;
  }



  /**
   * Sets the persona.
   *
   * @param persona the new persona
   */
  public void setPersona(final PersonaAdaDTO persona) {
    this.persona = persona;
  }



  /**
   * Gets the preferencia catalogo servicio.
   *
   * @return the preferencia catalogo servicio
   */
  public PreferenciaCatalogoServicioDTO getPreferenciaCatalogoServicio() {
    return preferenciaCatalogoServicio;
  }



  /**
   * Sets the preferencia catalogo servicio.
   *
   * @param preferenciaCatalogoServicio the new preferencia catalogo servicio
   */
  public void setPreferenciaCatalogoServicio(
      final PreferenciaCatalogoServicioDTO preferenciaCatalogoServicio) {
    this.preferenciaCatalogoServicio = preferenciaCatalogoServicio;
  }



  /**
   * Gets the situacion laboral.
   *
   * @return the situacion laboral
   */
  public SituacionLaboralDTO getSituacionLaboral() {
    return situacionLaboral;
  }



  /**
   * Sets the situacion laboral.
   *
   * @param situacionLaboral the new situacion laboral
   */
  public void setSituacionLaboral(final SituacionLaboralDTO situacionLaboral) {
    this.situacionLaboral = situacionLaboral;
  }



  /**
   * Gets the tipo familiar.
   *
   * @return the tipo familiar
   */
  public TipoFamiliarDTO getTipoFamiliar() {
    return tipoFamiliar;
  }



  /**
   * Sets the tipo familiar.
   *
   * @param tipoFamiliar the new tipo familiar
   */
  public void setTipoFamiliar(final TipoFamiliarDTO tipoFamiliar) {
    this.tipoFamiliar = tipoFamiliar;
  }


  /**
   * Gets the mes informado. Devuelve nulo si no hay ningun mes marcado, para
   * usarlo en la validacion.
   *
   * @return the mes informado
   */
  public Boolean getMesInformado() {
    Boolean existeMes = null;
    if (this.enero || this.febrero || this.marzo || this.abril || this.mayo
        || this.junio || this.julio || this.agosto || this.septiembre
        || this.octubre || this.noviembre || this.diciembre) {
      existeMes = Boolean.TRUE;
    }
    return existeMes;
  }


  /**
   * Checks if is vigente.
   *
   * @return true, if is vigente
   */
  public boolean isVigente() {
    return vigente;
  }

  /**
   * Sets the vigente. No se utiliza ya que es un campo calculado.
   *
   * @param vigente the new vigente
   */
  public void setVigente(final boolean vigente) {
    this.vigente = vigente;
  }

  /**
   * Gets the pk pia.
   *
   * @return the pk pia
   */
  public Long getPkPia() {
    return pkPia;
  }

  /**
   * Sets the pk pia.
   *
   * @param pkPia the new pk pia
   */
  public void setPkPia(final Long pkPia) {
    this.pkPia = pkPia;
  }


  /**
   * Gets the fecha modificacion.
   *
   * @return the fecha modificacion
   */
  public Date getFechaModificacion() {
    return UtilidadesCommons.cloneDate(fechaModificacion);
  }


  /**
   * Sets the fecha modificacion.
   *
   * @param fechaModificacion the new fecha modificacion
   */
  public void setFechaModificacion(final Date fechaModificacion) {
    this.fechaModificacion = UtilidadesCommons.cloneDate(fechaModificacion);
  }


  /**
   * Gets the fecha grabacion.
   *
   * @return the fecha grabacion
   */
  public Date getFechaGrabacion() {
    return fechaModificacion == null
        ? UtilidadesCommons.cloneDate(fechaCreacion)
        : UtilidadesCommons.cloneDate(fechaModificacion);
  }

  /**
   * Checks if is datos minimos.
   *
   * @return true, if is datos minimos
   */
  public boolean isDatosMinimos() {
    return datosMinimos;
  }

  /**
   * Sets the datos minimos.
   *
   * @param datosMinimos the new datos minimos
   */
  public void setDatosMinimos(final boolean datosMinimos) {
    this.datosMinimos = datosMinimos;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final CuidadorDTO o) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
