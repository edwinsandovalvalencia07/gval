package com.gval.gval.dto.dto;



import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The Class ExpedienteDTO.
 */
public class CabeceraExpedienteDTO extends BaseDTO
    implements Comparable<CabeceraExpedienteDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1175012299101541094L;

  /** The pk expediente. */
  private Long pkExpediente;

  /** The codigo expediente. */
  private String codigoExpediente;

  /** The identificador solicitante. */
  private String identificadorSolicitante;

  /** The nombre solicitante. */
  private String nombreCompletoSolicitante;

  /** The fecha registro. */
  private Date fechaRegistro;

  /** The ultimo grado. */
  private String ultimoGrado;

  /** The fallecido. */
  private String fallecido;

  /** The activo. */
  private String activo;

  /** The pia 1. */
  private String pia1;

  /** The pia 2. */
  private String pia2;

  /** The tiene teleasistencia. */
  private String tieneTeleasistencia;

  /** The traslado. */
  private String traslado;

  /** The ingresado. */
  private String ingresado;

  /** The urgencia. */
  private String urgencia;

  /** The responsabilidad patrimonial. */
  private String responsabilidadPatrimonial;

  /** The archivado. */
  private String archivado;

  /** The codigo solicitante sidep. */
  private String codigoSolicitanteSidep;

  /** The atendido. */
  private String atendido;

  /** The exclusion imserso. */
  private String exclusionImserso;

  /**
   * Instantiates a new cabecera expediente DTO.
   */
  public CabeceraExpedienteDTO() {
    super();
  }


  /**
   * Gets the pk expediente.
   *
   * @return the pk expediente
   */
  public Long getPkExpediente() {
    return pkExpediente;
  }

  /**
   * Sets the pk expediente.
   *
   * @param pkExpediente the new pk expediente
   */
  public void setPkExpediente(final Long pkExpediente) {
    this.pkExpediente = pkExpediente;
  }

  /**
   * Gets the codigo expediente.
   *
   * @return the codigo expediente
   */
  public String getCodigoExpediente() {
    return codigoExpediente;
  }

  /**
   * Sets the codigo expediente.
   *
   * @param codigoExpediente the new codigo expediente
   */
  public void setCodigoExpediente(final String codigoExpediente) {
    this.codigoExpediente = codigoExpediente;
  }

  /**
   * Gets the identificador solicitante.
   *
   * @return the identificador solicitante
   */
  public String getIdentificadorSolicitante() {
    return identificadorSolicitante;
  }

  /**
   * Sets the identificador solicitante.
   *
   * @param identificadorSolicitante the new identificador solicitante
   */
  public void setIdentificadorSolicitante(
      final String identificadorSolicitante) {
    this.identificadorSolicitante = identificadorSolicitante;
  }

  /**
   * Gets the nombre solicitante.
   *
   * @return the nombre solicitante
   */
  public String getNombreCompletoSolicitante() {
    return nombreCompletoSolicitante;
  }

  /**
   * Sets the nombre solicitante.
   *
   * @param nombreCompletoSolicitante the new nombre solicitante
   */
  public void setNombreCompletoSolicitante(
      final String nombreCompletoSolicitante) {
    this.nombreCompletoSolicitante = nombreCompletoSolicitante;
  }

  /**
   * Gets the fecha registro.
   *
   * @return the fecha registro
   */
  public Date getFechaRegistro() {
    return UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Gets the fecha registro formateada.
   *
   * @return the fecha registro formateada
   */
//  public String getFechaRegistroFormateada() {
//    return fechaRegistro == null ? ""
//        : new SimpleDateFormat(Utilidades.FORMAT_DATE_FECHA)
//            .format(fechaRegistro);
//  }

  /**
   * Sets the fecha registro.
   *
   * @param fechaRegistro the new fecha registro
   */
  public void setFechaRegistro(final Date fechaRegistro) {
    this.fechaRegistro = UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Gets the ultimo grado.
   *
   * @return the ultimo grado
   */
  public String getUltimoGrado() {
    return ultimoGrado;
  }

  /**
   * Sets the ultimo grado.
   *
   * @param ultimoGrado the new ultimo grado
   */
  public void setUltimoGrado(final String ultimoGrado) {
    this.ultimoGrado = ultimoGrado;
  }

  /**
   * Gets the fallecido.
   *
   * @return the fallecido
   */
  public String getFallecido() {
    return fallecido;
  }

  /**
   * Sets the fallecido.
   *
   * @param fallecido the new fallecido
   */
  public void setFallecido(final String fallecido) {
    this.fallecido = fallecido;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public String getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final String activo) {
    this.activo = activo;
  }

  /**
   * Gets the pia 1.
   *
   * @return the pia 1
   */
  public String getPia1() {
    return pia1;
  }

  /**
   * Sets the pia 1.
   *
   * @param pia1 the new pia 1
   */
  public void setPia1(final String pia1) {
    this.pia1 = pia1;
  }

  /**
   * Gets the pia 2.
   *
   * @return the pia 2
   */
  public String getPia2() {
    return pia2;
  }

  /**
   * Sets the pia 2.
   *
   * @param pia2 the new pia 2
   */
  public void setPia2(final String pia2) {
    this.pia2 = pia2;
  }

  /**
   * Gets the tiene teleasistencia.
   *
   * @return the tiene teleasistencia
   */
  public String getTieneTeleasistencia() {
    return tieneTeleasistencia;
  }

  /**
   * Sets the tiene teleasistencia.
   *
   * @param tieneTeleasistencia the new tiene teleasistencia
   */
  public void setTieneTeleasistencia(final String tieneTeleasistencia) {
    this.tieneTeleasistencia = tieneTeleasistencia;
  }

  /**
   * Gets the traslado.
   *
   * @return the traslado
   */
  public String getTraslado() {
    return traslado;
  }

  /**
   * Sets the traslado.
   *
   * @param traslado the new traslado
   */
  public void setTraslado(final String traslado) {
    this.traslado = traslado;
  }

  /**
   * Gets the ingresado.
   *
   * @return the ingresado
   */
  public String getIngresado() {
    return ingresado;
  }

  /**
   * Sets the ingresado.
   *
   * @param ingresado the new ingresado
   */
  public void setIngresado(final String ingresado) {
    this.ingresado = ingresado;
  }

  /**
   * Gets the urgencia.
   *
   * @return the urgencia
   */
  public String getUrgencia() {
    return urgencia;
  }

  /**
   * Sets the urgencia.
   *
   * @param urgencia the new urgencia
   */
  public void setUrgencia(final String urgencia) {
    this.urgencia = urgencia;
  }

  /**
   * Gets the responsabilidad patrimonial.
   *
   * @return the responsabilidad patrimonial
   */
  public String getResponsabilidadPatrimonial() {
    return responsabilidadPatrimonial;
  }

  /**
   * Sets the responsabilidad patrimonial.
   *
   * @param responsabilidadPatrimonial the new responsabilidad patrimonial
   */
  public void setResponsabilidadPatrimonial(
      final String responsabilidadPatrimonial) {
    this.responsabilidadPatrimonial = responsabilidadPatrimonial;
  }

  /**
   * Gets the archivado.
   *
   * @return the archivado
   */
  public String getArchivado() {
    return archivado;
  }

  /**
   * Sets the archivado.
   *
   * @param archivado the new archivado
   */
  public void setArchivado(final String archivado) {
    this.archivado = archivado;
  }

  /**
   * Gets the codigo solicitante sidep.
   *
   * @return the codigo solicitante sidep
   */
  public String getCodigoSolicitanteSidep() {
    return codigoSolicitanteSidep;
  }

  /**
   * Sets the codigo solicitante sidep.
   *
   * @param codigoSolicitanteSidep the new codigo solicitante sidep
   */
  public void setCodigoSolicitanteSidep(final String codigoSolicitanteSidep) {
    this.codigoSolicitanteSidep = codigoSolicitanteSidep;
  }

  /**
   * Gets the atendido.
   *
   * @return the atendido
   */
  public String getAtendido() {
    return atendido;
  }

  /**
   * Sets the atendido.
   *
   * @param atendido the new atendido
   */
  public void setAtendido(final String atendido) {
    this.atendido = atendido;
  }

  /**
   * Gets the exclusion imserso.
   *
   * @return the exclusion imserso
   */
  public String getExclusionImserso() {
    return exclusionImserso;
  }

  /**
   * Sets the exclusion imserso.
   *
   * @param exclusionImserso the new exclusion imserso
   */
  public void setExclusionImserso(String exclusionImserso) {
    this.exclusionImserso = exclusionImserso;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final CabeceraExpedienteDTO o) {
    return 0;
  }

}
