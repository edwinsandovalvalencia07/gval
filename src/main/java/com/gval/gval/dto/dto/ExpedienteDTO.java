package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.math.BigDecimal;
import java.util.Date;

/**
 * The Class ExpedienteDTO.
 */
public class ExpedienteDTO extends BaseDTO
    implements Comparable<ExpedienteDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1736206628951905345L;

  /** The pk expedien. */
  private Long pkExpedien;

  /** The activo. */
  private Boolean activo;

  /** The ambito. */
  private String ambito;

  /** The caja. */
  private String caja;

  /** The fallecimiento persona. */
  private Boolean fallecimientoPersona;

  /** The indica fallecimiento. */
  private Boolean indicaFallecimiento;

  /** The fecha sale. */
  private Date fechaSale;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The fecha fallecimiento. */
  private Date fechaFallecimiento;

  /** The grado nivel vigente. */
  private BigDecimal gradoNivelVigente;

  /** The motivo cancelacion archivo. */
  private Long motivoCancelacionArchivo;

  /** The numero expediente. */
  private String numeroExpediente;

  /** The resolucion PIA. */
  private BigDecimal resolucionPIA;

  /** The traslado entrantre. */
  private Boolean trasladoEntrante;

  /** The traslado saliente. */
  private Boolean trasladoSaliente;

  /** The fecha traslado saliente. */
  private Date fechaTrasladoSaliente;

  /** The fecha solicitud traslado saliente. */
  private Date fechaSolicitudTrasladoSaliente;

  /** The solicitante sidep. */
  private BigDecimal solicitanteSidep;

  /** The padron verificado. */
  private Boolean padronVerificado;

  /** The entidad dependencia saliente. */
  private EntidadDependenciaDTO entidadDependenciaSaliente;

  /** The comunidad autonoma entrante. */
  private ComunidadAutonomaDTO comunidadAutonomaEntrante;

  /** The comunidad autonoma saliente. */
  private ComunidadAutonomaDTO comunidadAutonomaSaliente;

  /** The usuario. */
  private UsuarioDTO usuario;

  /** The ultima solictud inicial. */
  private SolicitudDTO ultimaSolicitudInicialDTO;

  /** The solicitante. */
  private PersonaAdaDTO solicitante;

  /** The grado. */
  private Integer grado;

  /** The unidad. */
  private Long unidad;

  /** The app origen. */
  private String appOrigen;

  /** The cambio datos fallecido. */
  private Boolean cambioDatosFallecido;

  /** The servicio identidad pai consultado. */
  private Boolean servicioIdentidadPaiConsultado;

  /** The servicio identidad imserso consultado. */
  private Boolean servicioIdentidadImsersoConsultado;

  /** The servicio residencia pai consultado. */
  private Boolean servicioResidenciaPaiConsultado;

  /** The servicio prestacion pai consultado. */
  private Boolean servicioPrestacionPaiConsultado;

  /**
   * Gets the pk expedien.
   *
   * @return the pk expedien
   */
  public Long getPkExpedien() {
    return pkExpedien;
  }

  /**
   * Sets the pk expedien.
   *
   * @param pkExpedien the new pk expedien
   */
  public void setPkExpedien(final Long pkExpedien) {
    this.pkExpedien = pkExpedien;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the ambito.
   *
   * @return the ambito
   */
  public String getAmbito() {
    return ambito;
  }

  /**
   * Sets the ambito.
   *
   * @param ambito the new ambito
   */
  public void setAmbito(final String ambito) {
    this.ambito = ambito;
  }

  /**
   * Gets the caja.
   *
   * @return the caja
   */
  public String getCaja() {
    return caja;
  }

  /**
   * Sets the caja.
   *
   * @param caja the new caja
   */
  public void setCaja(final String caja) {
    this.caja = caja;
  }

  /**
   * Gets the fallecimiento persona.
   *
   * @return the fallecimiento persona
   */
  public Boolean getFallecimientoPersona() {
    return fallecimientoPersona;
  }

  /**
   * Sets the fallecimiento persona.
   *
   * @param fallecimientoPersona the new fallecimiento persona
   */
  public void setFallecimientoPersona(final Boolean fallecimientoPersona) {
    this.fallecimientoPersona = fallecimientoPersona;
  }

  /**
   * Gets the indica fallecimiento.
   *
   * @return the indica fallecimiento
   */
  public Boolean getIndicaFallecimiento() {
    return indicaFallecimiento;
  }

  /**
   * Sets the indica fallecimiento.
   *
   * @param indicaFallecimiento the new indica fallecimiento
   */
  public void setIndicaFallecimiento(final Boolean indicaFallecimiento) {
    this.indicaFallecimiento = indicaFallecimiento;
  }

  /**
   * Gets the fecha sale.
   *
   * @return the fecha sale
   */
  public Date getFechaSale() {
    return UtilidadesCommons.cloneDate(fechaSale);
  }

  /**
   * Sets the fecha sale.
   *
   * @param fechaSale the new fecha sale
   */
  public void setFechaSale(final Date fechaSale) {
    this.fechaSale = UtilidadesCommons.cloneDate(fechaSale);
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha fallecimiento.
   *
   * @return the fecha fallecimiento
   */
  public Date getFechaFallecimiento() {
    return UtilidadesCommons.cloneDate(fechaFallecimiento);
  }

  /**
   * Sets the fecha fallecimiento.
   *
   * @param fechaFallecimiento the new fecha fallecimiento
   */
  public void setFechaFallecimiento(final Date fechaFallecimiento) {
    this.fechaFallecimiento = UtilidadesCommons.cloneDate(fechaFallecimiento);
  }

  /**
   * Gets the grado nivel vigente.
   *
   * @return the grado nivel vigente
   */
  public BigDecimal getGradoNivelVigente() {
    return gradoNivelVigente;
  }

  /**
   * Sets the grado nivel vigente.
   *
   * @param gradoNivelVigente the new grado nivel vigente
   */
  public void setGradoNivelVigente(final BigDecimal gradoNivelVigente) {
    this.gradoNivelVigente = gradoNivelVigente;
  }

  /**
   * Gets the numero expediente.
   *
   * @return the numero expediente
   */
  public String getNumeroExpediente() {
    return numeroExpediente;
  }

  /**
   * Sets the numero expediente.
   *
   * @param numeroExpediente the new numero expediente
   */
  public void setNumeroExpediente(final String numeroExpediente) {
    this.numeroExpediente = numeroExpediente;
  }

  /**
   * Gets the resolucion PIA.
   *
   * @return the resolucion PIA
   */
  public BigDecimal getResolucionPIA() {
    return resolucionPIA;
  }

  /**
   * Sets the resolucion PIA.
   *
   * @param resolucionPIA the new resolucion PIA
   */
  public void setResolucionPIA(final BigDecimal resolucionPIA) {
    this.resolucionPIA = resolucionPIA;
  }

  /**
   * Gets the traslado entrantre.
   *
   * @return the traslado entrantre
   */
  public Boolean getTrasladoEntrante() {
    return trasladoEntrante;
  }

  /**
   * Sets the traslado entrantre.
   *
   * @param trasladoEntrante the new traslado entrantre
   */
  public void setTrasladoEntrante(final Boolean trasladoEntrante) {
    this.trasladoEntrante = trasladoEntrante;
  }

  /**
   * Gets the traslado saliente.
   *
   * @return the traslado saliente
   */
  public Boolean getTrasladoSaliente() {
    return trasladoSaliente;
  }

  /**
   * Sets the traslado saliente.
   *
   * @param trasladoSaliente the new traslado saliente
   */
  public void setTrasladoSaliente(final Boolean trasladoSaliente) {
    this.trasladoSaliente = trasladoSaliente;
  }

  /**
   * Gets the fecha traslado saliente.
   *
   * @return the fecha traslado saliente
   */
  public Date getFechaTrasladoSaliente() {
    return UtilidadesCommons.cloneDate(fechaTrasladoSaliente);
  }

  /**
   * Sets the fecha traslado saliente.
   *
   * @param fechaTrasladoSaliente the new fecha traslado saliente
   */
  public void setFechaTrasladoSaliente(final Date fechaTrasladoSaliente) {
    this.fechaTrasladoSaliente =
        UtilidadesCommons.cloneDate(fechaTrasladoSaliente);
  }

  /**
   * Gets the fecha solicitud traslado saliente.
   *
   * @return the fecha solicitud traslado saliente
   */
  public Date getFechaSolicitudTrasladoSaliente() {
    return UtilidadesCommons.cloneDate(fechaSolicitudTrasladoSaliente);
  }

  /**
   * Sets the fecha solicitud traslado saliente.
   *
   * @param fechaSolicitudTrasladoSaliente the new fecha solicitud traslado
   *        saliente
   */
  public void setFechaSolicitudTrasladoSaliente(
      final Date fechaSolicitudTrasladoSaliente) {
    this.fechaSolicitudTrasladoSaliente =
        UtilidadesCommons.cloneDate(fechaSolicitudTrasladoSaliente);
  }

  /**
   * Gets the solicitante sidep.
   *
   * @return the solicitante sidep
   */
  public BigDecimal getSolicitanteSidep() {
    return solicitanteSidep;
  }

  /**
   * Sets the solicitante sidep.
   *
   * @param solicitanteSidep the new solicitante sidep
   */
  public void setSolicitanteSidep(final BigDecimal solicitanteSidep) {
    this.solicitanteSidep = solicitanteSidep;
  }

  /**
   * Gets the padron verificado.
   *
   * @return the padron verificado
   */
  public Boolean getPadronVerificado() {
    return padronVerificado;
  }

  /**
   * Sets the padron verificado.
   *
   * @param padronVerificado the new padron verificado
   */
  public void setPadronVerificado(final Boolean padronVerificado) {
    this.padronVerificado = padronVerificado;
  }

  /**
   * Gets the entidad dependencia saliente.
   *
   * @return the entidadDependenciaSaliente
   */
  public EntidadDependenciaDTO getEntidadDependenciaSaliente() {
    return entidadDependenciaSaliente;
  }

  /**
   * Sets the entidad dependencia saliente.
   *
   * @param entidadDependenciaSaliente the entidadDependenciaSaliente to set
   */
  public void setEntidadDependenciaSaliente(
      final EntidadDependenciaDTO entidadDependenciaSaliente) {
    this.entidadDependenciaSaliente = entidadDependenciaSaliente;
  }

  /**
   * Gets the comunidad autonoma entrante.
   *
   * @return the comunidadAutonomaEntrante
   */
  public ComunidadAutonomaDTO getComunidadAutonomaEntrante() {
    return comunidadAutonomaEntrante;
  }

  /**
   * Sets the comunidad autonoma entrante.
   *
   * @param comunidadAutonomaEntrante the comunidadAutonomaEntrante to set
   */
  public void setComunidadAutonomaEntrante(
      final ComunidadAutonomaDTO comunidadAutonomaEntrante) {
    this.comunidadAutonomaEntrante = comunidadAutonomaEntrante;
  }

  /**
   * Gets the comunidad autonoma saliente.
   *
   * @return the comunidadAutonomaSaliente
   */
  public ComunidadAutonomaDTO getComunidadAutonomaSaliente() {
    return comunidadAutonomaSaliente;
  }

  /**
   * Sets the comunidad autonoma saliente.
   *
   * @param comunidadAutonomaSaliente the comunidadAutonomaSaliente to set
   */
  public void setComunidadAutonomaSaliente(
      final ComunidadAutonomaDTO comunidadAutonomaSaliente) {
    this.comunidadAutonomaSaliente = comunidadAutonomaSaliente;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the ultima solicitud inicial DTO.
   *
   * @return the ultima solicitud inicial DTO
   */
  public SolicitudDTO getUltimaSolicitudInicialDTO() {
    return ultimaSolicitudInicialDTO;
  }

  /**
   * Sets the ultima solicitud inicial DTO.
   *
   * @param ultimaSolicitudInicialDTO the new ultima solicitud inicial DTO
   */
  public void setUltimaSolicitudInicialDTO(
      final SolicitudDTO ultimaSolicitudInicialDTO) {
    this.ultimaSolicitudInicialDTO = ultimaSolicitudInicialDTO;
  }

  /**
   * Gets the solicitante.
   *
   * @return the solicitante
   */
  public PersonaAdaDTO getSolicitante() {
    return solicitante;
  }

  /**
   * Sets the solicitante.
   *
   * @param solicitante the new solicitante
   */
  public void setSolicitante(final PersonaAdaDTO solicitante) {
    this.solicitante = solicitante;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final ExpedienteDTO o) {
    return 0;
  }

  /**
   * Crea una nueva instancia de Expediente.
   *
   * @return the expediente DTO
   */
  public static ExpedienteDTO createInstance() {
    final ExpedienteDTO expediente = new ExpedienteDTO();


    expediente.setTrasladoEntrante(Boolean.FALSE);
    expediente.setActivo(Boolean.TRUE);
    expediente.setFechaCreacion(new Date());
    expediente.setTrasladoSaliente(Boolean.FALSE);
    expediente.setFallecimientoPersona(Boolean.FALSE);
    expediente.setIndicaFallecimiento(Boolean.FALSE);

    return expediente;
  }

  /**
   * Inicializar valoraes por defecto.
   */
  public void inicializarValoresPorDefecto() {

    this.trasladoEntrante = Boolean.FALSE;
    this.activo = Boolean.TRUE;
    this.fechaCreacion = new Date();
    this.trasladoSaliente = Boolean.FALSE;
    this.fallecimientoPersona = Boolean.FALSE;
    this.indicaFallecimiento = Boolean.FALSE;
  }

  /**
   * Gets the grado.
   *
   * @return the grado
   */
  public Integer getGrado() {
    return grado;
  }

  /**
   * Sets the grado.
   *
   * @param grado the new grado
   */
  public void setGrado(final Integer grado) {
    this.grado = grado;
  }

  /**
   * Gets the unidad.
   *
   * @return the unidad
   */
  public Long getUnidad() {
    return unidad;
  }

  /**
   * Sets the unidad.
   *
   * @param unidad the unidad to set
   */
  public void setUnidad(final Long unidad) {
    this.unidad = unidad;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

  /**
   * Gets the app origen.
   *
   * @return the app origen
   */
  public String getAppOrigen() {
    return appOrigen;
  }

  /**
   * Sets the app origen.
   *
   * @param appOrigen the new app origen
   */
  public void setAppOrigen(final String appOrigen) {
    this.appOrigen = appOrigen;
  }

  /**
   * Gets the cambio datos fallecido.
   *
   * @return the cambio datos fallecido
   */
  public Boolean getCambioDatosFallecido() {
    return cambioDatosFallecido;
  }

  /**
   * Sets the cambio datos fallecido.
   *
   * @param cambioDatosFallecido the new cambio datos fallecido
   */
  public void setCambioDatosFallecido(final Boolean cambioDatosFallecido) {
    this.cambioDatosFallecido = cambioDatosFallecido;
  }

  /**
   * Gets the motivo cancelacion archivo.
   *
   * @return the motivo cancelacion archivo
   */
  public Long getMotivoCancelacionArchivo() {
    return this.motivoCancelacionArchivo;
  }

  /**
   * Sets the motivo cancelacion archivo.
   *
   * @param motivoCancelacionArchivo the new motivo cancelacion archivo
   */
  public void setMotivoCancelacionArchivo(final Long motivoCancelacionArchivo) {
    this.motivoCancelacionArchivo = motivoCancelacionArchivo;
  }


  /**
   * Gets the servicio identidad pai consultado.
   *
   * @return the servicio identidad pai consultado
   */
  public Boolean getServicioIdentidadPaiConsultado() {
    return servicioIdentidadPaiConsultado;
  }


  /**
   * Sets the servicio identidad pai consultado.
   *
   * @param servicioIdentidadPaiConsultado the new servicio identidad pai consultado
   */
  public void setServicioIdentidadPaiConsultado(
      final Boolean servicioIdentidadPaiConsultado) {
    this.servicioIdentidadPaiConsultado = servicioIdentidadPaiConsultado;
  }



  /**
   * Gets the servicio identidad imserso consultado.
   *
   * @return the servicio identidad imserso consultado
   */
  public Boolean getServicioIdentidadImsersoConsultado() {
    return servicioIdentidadImsersoConsultado;
  }


  /**
   * Sets the servicio identidad imserso consultado.
   *
   * @param servicioIdentidadImsersoConsultado the new servicio identidad imserso consultado
   */
  public void setServicioIdentidadImsersoConsultado(
      final Boolean servicioIdentidadImsersoConsultado) {
    this.servicioIdentidadImsersoConsultado =
        servicioIdentidadImsersoConsultado;
  }


  /**
   * Gets the servicio residencia pai consultado.
   *
   * @return the servicio residencia pai consultado
   */
  public Boolean getServicioResidenciaPaiConsultado() {
    return servicioResidenciaPaiConsultado;
  }


  /**
   * Sets the servicio residencia pai consultado.
   *
   * @param servicioResidenciaPaiConsultado the new servicio residencia pai consultado
   */
  public void setServicioResidenciaPaiConsultado(
      final Boolean servicioResidenciaPaiConsultado) {
    this.servicioResidenciaPaiConsultado = servicioResidenciaPaiConsultado;
  }


  /**
   * Gets the servicio prestacion pai consultado.
   *
   * @return the servicio prestacion pai consultado
   */
  public Boolean getServicioPrestacionPaiConsultado() {
    return servicioPrestacionPaiConsultado;
  }


  /**
   * Sets the servicio prestacion pai consultado.
   *
   * @param servicioPrestacionPaiConsultado the new servicio prestacion pai consultado
   */
  public void setServicioPrestacionPaiConsultado(
      final Boolean servicioPrestacionPaiConsultado) {
    this.servicioPrestacionPaiConsultado = servicioPrestacionPaiConsultado;
  }


}
