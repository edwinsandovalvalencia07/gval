package com.gval.gval.dto.dto;

import java.io.Serializable;

/**
 * The Class GrupoValoracionUsuarioDTO.
 */
public class GrupoValoracionUsuarioDTO implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk grupoval usu. */
  private long pkGrupovalUsu;

  /** The activo. */
  private Boolean activo;

  /** The es valorador. */
  private Boolean esValorador;

  /** The es trabajador social. */
  private Boolean esTrabajadorSocial;

  /** The grupo valoracion. */
  private GrupoValoracionDTO grupoValoracion;

  /** The usuario. */
  private UsuarioDTO usuario;

  /**
   * Instantiates a new grupo valoracion usuario.
   */
  public GrupoValoracionUsuarioDTO() {
    super();
  }

  /**
   * Gets the pk grupoval usu.
   *
   * @return the pkGrupovalUsu
   */
  public long getPkGrupovalUsu() {
    return pkGrupovalUsu;
  }

  /**
   * Sets the pk grupoval usu.
   *
   * @param pkGrupovalUsu the pkGrupovalUsu to set
   */
  public void setPkGrupovalUsu(final long pkGrupovalUsu) {
    this.pkGrupovalUsu = pkGrupovalUsu;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the es valorador.
   *
   * @return the esValorador
   */
  public Boolean getEsValorador() {
    return esValorador;
  }

  /**
   * Sets the es valorador.
   *
   * @param esValorador the esValorador to set
   */
  public void setEsValorador(final Boolean esValorador) {
    this.esValorador = esValorador;
  }

  /**
   * Gets the es trabajador social.
   *
   * @return the esTrabajadorSocial
   */
  public Boolean getEsTrabajadorSocial() {
    return esTrabajadorSocial;
  }

  /**
   * Sets the es trabajador social.
   *
   * @param esTrabajadorSocial the esTrabajadorSocial to set
   */
  public void setEsTrabajadorSocial(final Boolean esTrabajadorSocial) {
    this.esTrabajadorSocial = esTrabajadorSocial;
  }

  /**
   * Gets the grupo valoracion.
   *
   * @return the grupoValoracion
   */
  public GrupoValoracionDTO getGrupoValoracion() {
    return grupoValoracion;
  }

  /**
   * Sets the grupo valoracion.
   *
   * @param grupoValoracion the grupoValoracion to set
   */
  public void setGrupoValoracion(final GrupoValoracionDTO grupoValoracion) {
    this.grupoValoracion = grupoValoracion;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the usuario to set
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }

}
