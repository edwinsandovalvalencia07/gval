package com.gval.gval.dto.dto;

import es.gva.dependencia.ada.common.enums.CatalogoServicioEnum;
import com.gval.gval.dto.dto.util.BaseDTO;
import es.gva.dependencia.ada.vistas.dto.CentroDTO;

import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * The Class PreferenciaCatalogoServicioDTO.
 */
public class PreferenciaCatalogoServicioDTO extends BaseDTO
    implements Comparable<PreferenciaCatalogoServicioDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -311269631958656788L;

  /** The Constant ORDEN_CERO. */
  private static final long ORDEN_CERO = 0L;

  /** The Constant ORDEN_UNO. */
  private static final long ORDEN_UNO = 1L;

  /** The pk pref cataserv. */
  @NotNull
  private Long pkPrefCataserv;

  /** The preferencia. */
  @NotNull
  private PreferenciaDTO preferencia;

  /** The catalogo servicio. */
  @NotNull
  private CatalogoServicioDTO catalogoServicio;

  /** The orden. */
  private Long orden;

  /** The centro primero. */
  private CentroDTO centroPrimero;

  /** The centro segundo. */
  private CentroDTO centroSegundo;

  /** The centro tercero. */
  private CentroDTO centroTercero;
  
  /** The provincia DV. */
  private ProvinciaDTO provincia;

  /** The satisfecho. */
  private Boolean satisfecho;

  /** The denegado. */
  private Boolean denegado;

  /** The observaciones. */
  private String observaciones;

  /**
   * Instantiates a new preferencia catalogo servicio DTO.
   */
  public PreferenciaCatalogoServicioDTO() {
    super();
  }

  /**
   * Instantiates a new preferencia catalogo servicio DTO.
   *
   * @param defaultData the default data
   */
  public PreferenciaCatalogoServicioDTO(final boolean defaultData) {
    super();
    if (defaultData) {
      catalogoServicio = new CatalogoServicioDTO();
      this.satisfecho = Boolean.FALSE;
      this.denegado = Boolean.FALSE;
    }
  }

  /**
   * Instantiates a new preferencia catalogo servicio DTO.
   *
   * @param valoresPorDefecto the valores por defecto
   * @param ordenPreferencia the orden preferencia
   */
  public PreferenciaCatalogoServicioDTO(final boolean valoresPorDefecto,
      final Long ordenPreferencia) {
    this(valoresPorDefecto);
    this.setOrden(ordenPreferencia);
  }

  /**
   * Gets the pk pref cataserv.
   *
   * @return the pk pref cataserv
   */
  public Long getPkPrefCataserv() {
    return pkPrefCataserv;
  }

  /**
   * Gets the preferencia.
   *
   * @return the preferencia
   */
  public PreferenciaDTO getPreferencia() {
    return preferencia;
  }

  /**
   * Sets the preferencia.
   *
   * @param preferencia the new preferencia
   */
  public void setPreferencia(final PreferenciaDTO preferencia) {
    this.preferencia = preferencia;
  }

  /**
   * Sets the pk pref cataserv.
   *
   * @param pkPrefCataserv the new pk pref cataserv
   */
  public void setPkPrefCataserv(final Long pkPrefCataserv) {
    this.pkPrefCataserv = pkPrefCataserv;
  }

  /**
   * Gets the catalogo servicio.
   *
   * @return the catalogo servicio
   */
  public CatalogoServicioDTO getCatalogoServicio() {
    return catalogoServicio;
  }

  /**
   * Sets the catalogo servicio.
   *
   * @param catalogoServicio the new catalogo servicio
   */
  public void setCatalogoServicio(final CatalogoServicioDTO catalogoServicio) {
    this.catalogoServicio = catalogoServicio;
  }

  /**
   * Gets the orden.
   *
   * @return the orden
   */
  public Long getOrden() {
    return orden;
  }

  /**
   * Sets the orden.
   *
   * @param orden the new orden
   */
  public void setOrden(final Long orden) {
    this.orden = orden;
  }

  /**
   * Gets the centro primero.
   *
   * @return the centro primero
   */
  public CentroDTO getCentroPrimero() {
    return centroPrimero;
  }

  /**
   * Sets the centro primero.
   *
   * @param centroPrimero the new centro primero
   */
  public void setCentroPrimero(final CentroDTO centroPrimero) {
    this.centroPrimero = centroPrimero;
  }

  /**
   * Gets the centro segundo.
   *
   * @return the centro segundo
   */
  public CentroDTO getCentroSegundo() {
    return centroSegundo;
  }

  /**
   * Sets the centro segundo.
   *
   * @param centroSegundo the new centro segundo
   */
  public void setCentroSegundo(final CentroDTO centroSegundo) {
    this.centroSegundo = centroSegundo;
  }

  /**
   * Gets the centro tercero.
   *
   * @return the centro tercero
   */
  public CentroDTO getCentroTercero() {
    return centroTercero;
  }

  /**
   * Sets the centro tercero.
   *
   * @param centroTercero the new centro tercero
   */
  public void setCentroTercero(final CentroDTO centroTercero) {
    this.centroTercero = centroTercero;
  }

  /**
   * Gets the provincia CV.
   *
   * @return the provincia CV
   */
  public ProvinciaDTO getProvincia() {
    return provincia;
  }

  /**
   * Sets the provincia CV.
   *
   * @param provincia the new provincia CV
   */
  public void setProvincia(final ProvinciaDTO provincia) {
    this.provincia = provincia;
  }
  
  /**
   * Gets the satisfecho.
   *
   * @return the satisfecho
   */
  public Boolean getSatisfecho() {
    return satisfecho;
  }

  /**
   * Sets the satisfecho.
   *
   * @param satisfecho the new satisfecho
   */
  public void setSatisfecho(final Boolean satisfecho) {
    this.satisfecho = satisfecho;
  }

  /**
   * Gets the denegado.
   *
   * @return the denegado
   */
  public Boolean getDenegado() {
    return denegado;
  }

  /**
   * Sets the denegado.
   *
   * @param denegado the new denegado
   */
  public void setDenegado(final Boolean denegado) {
    this.denegado = denegado;
  }

  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return observaciones;
  }

  /**
   * Sets the observaciones.
   *
   * @param observaciones the new observaciones
   */
  public void setObservaciones(final String observaciones) {
    this.observaciones = observaciones;
  }

  /**
   * Es primera pref.
   *
   * @return true, if successful
   */
  public boolean esPrimeraPref() {
    return orden == ORDEN_UNO;
  }

  /**
   * Es segunda pref.
   *
   * @return true, if successful
   */
  public boolean esSegundaPref() {
    return orden == ORDEN_CERO && !CatalogoServicioEnum.TELEASISTENCIA
        .getCodigo().equals(catalogoServicio.getCodigo());
  }

  /**
   * Es teleasis pref.
   *
   * @return true, if successful
   */
  public boolean esTeleasisPref() {
    return orden == ORDEN_CERO && CatalogoServicioEnum.TELEASISTENCIA
        .getCodigo().equals(catalogoServicio.getCodigo());
  }

  /**
   * Es pref de tipo.
   *
   * @param pref the pref
   * @return true, if successful
   */
  public boolean esPrefDeTipo(final CatalogoServicioEnum pref) {
    return pref != null
        && pref.getCodigo().equals(catalogoServicio.getCodigo());
  }



  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final PreferenciaCatalogoServicioDTO o) {
    return 0;
  }

  /**
   * Equals.
   *
   * @param obj the obj
   * @return true, if successful
   */
  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    // null check
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final PreferenciaCatalogoServicioDTO other =
        (PreferenciaCatalogoServicioDTO) obj;
    return other.getPkPrefCataserv() != null && this.pkPrefCataserv != null
        && Objects.equals(this.pkPrefCataserv, other.getPkPrefCataserv());
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return super.hashCode();
  }

}
