package com.gval.gval.dto.dto.util;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * The Class ApiErrorResponseDto.
 */
public class ApiErrorResponseDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1542520138660690930L;

  /** The apierror. */
  private ApiErrorDTO apierror;

  /**
   * Instantiates a new api error response dto.
   */
  public ApiErrorResponseDTO() {
    super();
  }

  /**
   * Gets the apierror.
   *
   * @return the apierror
   */
  public ApiErrorDTO getApierror() {
    return apierror;
  }

  /**
   * Sets the apierror.
   *
   * @param apierror the new apierror
   */
  public void setApierror(final ApiErrorDTO apierror) {
    this.apierror = apierror;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
