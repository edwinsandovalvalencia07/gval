package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;

/**
 * The Class ResolTipoDocuDTO.
 */
public class ResolTipoDocuDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 5895427494959524074L;

  /** The pk resoluci. */
  private Long pkResoluci;

  /** The pk tipodocu. */
  private Long pkTipodocu;

  /** The pk solicit. */
  private Boolean activo;

  /**
   * Gets the pk resoluci.
   *
   * @return the pk resoluci
   */
  public Long getPkResoluci() {
    return pkResoluci;
  }

  /**
   * Sets the pk resoluci.
   *
   * @param pkResoluci the new pk resoluci
   */
  public void setPkResoluci(final Long pkResoluci) {
    this.pkResoluci = pkResoluci;
  }

  /**
   * Gets the pk tipodocu.
   *
   * @return the pk tipodocu
   */
  public Long getPkTipodocu() {
    return pkTipodocu;
  }

  /**
   * Sets the pk tipodocu.
   *
   * @param pkTipodocu the new pk tipodocu
   */
  public void setPkTipodocu(final Long pkTipodocu) {
    this.pkTipodocu = pkTipodocu;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

}
