package com.gval.gval.dto.dto;



import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;

/**
 * The Class CodigoPostalDTO.
 */
public class CodigoPostalDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -1957048577922719589L;

  /** The pk codigo postal. */
  private Long pkCodigoPostal;

  /** The activo. */
  private Boolean activo;

  /** The codigo postal. */
  private String codigoPostal;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /**
   * Instantiates a new codigo postal DTO.
   */
  public CodigoPostalDTO() {
    super();
  }


  /**
   * Instantiates a new codigo postal DTO.
   *
   * @param pkCodigoPostal the pk codigo postal
   * @param activo the activo
   * @param codigoPostal the codigo postal
   * @param fechaCreacion the fecha creacion
   */
  public CodigoPostalDTO(final Long pkCodigoPostal, final Boolean activo,
      final String codigoPostal, final Date fechaCreacion) {
    super();
    this.pkCodigoPostal = pkCodigoPostal;
    this.activo = activo;
    this.codigoPostal = codigoPostal;
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }


  /**
   * Gets the pk codigo postal.
   *
   * @return the pk codigo postal
   */
  public long getPkCodigoPostal() {
    return pkCodigoPostal;
  }

  /**
   * Sets the pk codigo postal.
   *
   * @param pkCodigoPostal the new pk codigo postal
   */
  public void setPkCodigoPostal(final Long pkCodigoPostal) {
    this.pkCodigoPostal = pkCodigoPostal;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the codigo postal.
   *
   * @return the codigo postal
   */
  public String getCodigoPostal() {
    return codigoPostal;
  }

  /**
   * Sets the codigo postal.
   *
   * @param codigoPostal the new codigo postal
   */
  public void setCodigoPostal(final String codigoPostal) {
    this.codigoPostal = codigoPostal;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
