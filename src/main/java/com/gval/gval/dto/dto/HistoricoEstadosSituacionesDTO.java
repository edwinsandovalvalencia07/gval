package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;
import com.gval.gval.entity.model.Constantes;

import java.util.Date;


/**
 * The Class HistoricoEstadosSituacionesDTO.
 */
public class HistoricoEstadosSituacionesDTO extends BaseDTO
    implements Comparable<HistoricoEstadosSituacionesDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -4557578434512976593L;

  /** The fecha. */
  private Date fecha;

  /** The nombre. */
  private String nombre;

  /** The descripcion. */
  private String descripcion;

  /**
   * Instantiates a new historico estados situaciones DTO.
   */
  public HistoricoEstadosSituacionesDTO() {
    super();
  }

  /**
   * Instantiates a new historico estados situaciones DTO.
   *
   * @param fecha the fecha
   * @param nombre the nombre
   * @param descripcion the descripcion
   */
  public HistoricoEstadosSituacionesDTO(final Date fecha, final String nombre,
      final String descripcion) {
    super();
    this.fecha = UtilidadesCommons.cloneDate(fecha);
    this.nombre = nombre;
    this.descripcion = descripcion;
  }

  /**
   * Gets the fecha.
   *
   * @return the fecha
   */
  public Date getFecha() {
    return UtilidadesCommons.cloneDate(fecha);
  }

  /**
   * Sets the fecha.
   *
   * @param fecha the new fecha
   */
  public void setFecha(final Date fecha) {
    this.fecha = UtilidadesCommons.cloneDate(fecha);
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the accion.
   *
   * @return the accion
   */
  public String getAccion() {
    final StringBuilder accion = new StringBuilder();

    accion.append(this.getNombre() == null || this.getNombre().isEmpty()
        ? this.getDescripcion()
        : this.getNombre());
    accion
        .append(this.getDescripcion() == null || this.getDescripcion().isEmpty()
            ? Constantes.VACIO
            : Constantes.ESPACIO.concat(Constantes.DOS_PUNTOS)
                .concat(this.getDescripcion()));

    return accion.toString();
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final HistoricoEstadosSituacionesDTO o) {
    return 0;
  }

}
