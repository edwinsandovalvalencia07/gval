package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.validator.constraints.Length;

import java.util.Date;

import javax.validation.constraints.NotNull;

/**
 * The Class ComentarioIncidenciaDTO.
 */
public class ComentarioIncidenciaDTO extends BaseDTO
    implements Comparable<ComentarioIncidenciaDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -5035594330199163530L;

  /** The pk comninci. */
  @NotNull
  private Long pkComninci;

  /** The activo. */
  @NotNull
  private Boolean activo;

  /** The comentario. */
  @NotNull
  @Length(max = 4000)
  private String comentario;

  /** The fecha creacion. */
  @NotNull
  private Date fechaCreacion;

  /** The incidencia. */
  @NotNull
  private IncidenciaDTO incidencia;

  /** The usuario. */
  @NotNull
  private UsuarioDTO usuario;

  /**
   * Instantiates a new comentario incidencia DTO.
   */
  public ComentarioIncidenciaDTO() {
    super();
  }

  /**
   * Instantiates a new comentario incidencia DTO.
   *
   * @param pkComninci the pk comninci
   * @param activo the activo
   * @param comentario the comentario
   * @param fechaCreacion the fecha creacion
   * @param incidencia the incidencia
   * @param usuario the usuario
   */
  public ComentarioIncidenciaDTO(final Long pkComninci, final Boolean activo,
      final String comentario, final Date fechaCreacion,
      final IncidenciaDTO incidencia, final UsuarioDTO usuario) {
    super();
    this.pkComninci = pkComninci;
    this.activo = activo;
    this.comentario = comentario;
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
    this.incidencia = incidencia;
    this.usuario = usuario;
  }

  /**
   * Gets the pk comninci.
   *
   * @return the pk comninci
   */
  public Long getPkComninci() {
    return pkComninci;
  }

  /**
   * Sets the pk comninci.
   *
   * @param pkComninci the new pk comninci
   */
  public void setPkComninci(final Long pkComninci) {
    this.pkComninci = pkComninci;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the comentario.
   *
   * @return the comentario
   */
  public String getComentario() {
    return comentario;
  }

  /**
   * Sets the comentario.
   *
   * @param comentario the new comentario
   */
  public void setComentario(final String comentario) {
    this.comentario = comentario;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the incidencia.
   *
   * @return the incidencia
   */
  public IncidenciaDTO getIncidencia() {
    return incidencia;
  }

  /**
   * Sets the incidencia.
   *
   * @param incidencia the new incidencia
   */
  public void setIncidencia(final IncidenciaDTO incidencia) {
    this.incidencia = incidencia;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final ComentarioIncidenciaDTO o) {
    return 0;
  }

  /**
   * Creates the instance.
   *
   * @return the comentario incidencia DTO
   */
  public static ComentarioIncidenciaDTO createInstance() {
    final ComentarioIncidenciaDTO comentarioNuevo =
        new ComentarioIncidenciaDTO();

    comentarioNuevo.setActivo(Boolean.TRUE);
    comentarioNuevo.setFechaCreacion(new Date());

    return comentarioNuevo;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
