package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;

/**
 * The Class ComunidadAutonomaDTO.
 */
public class ComunidadAutonomaDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 3156437960088426494L;

  /** The pk comauto. */
  private Long pkComauto;

  /** The activo. */
  private Boolean activo;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The imserso. */
  private String imserso;

  /** The nombre. */
  private String nombre;

  /**
   * Instantiates a new comunidad autonoma DTO.
   */
  public ComunidadAutonomaDTO() {
    super();
  }

  /**
   * Instantiates a new comunidad autonoma DTO.
   *
   * @param pkComauto the pk comauto
   * @param activo the activo
   * @param fechaCreacion the fecha creacion
   * @param imserso the imserso
   * @param nombre the nombre
   */
  public ComunidadAutonomaDTO(final Long pkComauto, final Boolean activo,
      final Date fechaCreacion, final String imserso, final String nombre) {
    super();
    this.pkComauto = pkComauto;
    this.activo = activo;
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
    this.imserso = imserso;
    this.nombre = nombre;
  }

  /**
   * Gets the pk comauto.
   *
   * @return the pk comauto
   */
  public Long getPkComauto() {
    return pkComauto;
  }

  /**
   * Sets the pk comauto.
   *
   * @param pkComauto the new pk comauto
   */
  public void setPkComauto(final Long pkComauto) {
    this.pkComauto = pkComauto;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the imserso.
   *
   * @return the imserso
   */
  public String getImserso() {
    return imserso;
  }

  /**
   * Sets the imserso.
   *
   * @param imserso the new imserso
   */
  public void setImserso(final String imserso) {
    this.imserso = imserso;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
