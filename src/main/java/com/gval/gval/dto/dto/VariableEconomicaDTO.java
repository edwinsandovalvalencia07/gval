package com.gval.gval.dto.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;


/**
 * The Class VariableEconomicaDTO.
 */
public class VariableEconomicaDTO implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 8974478883924597137L;

  /** The pk variecon. */
  private Long pkVariecon;

  /** The codigo. */
  @Size(max = 15)
  private String codigo;

  /** The descripcion. */
  @Size(max = 100)
  private String descripcion;

  /** The valor. */
  private BigDecimal valor;

  /** The ejercicio. */
  @ManyToOne
  @JoinColumn(name = "CLAVE")
  private EjercicioDTO ejercicio;



  /**
   * Instantiates a new variable economica DTO.
   */
  public VariableEconomicaDTO() {
    // Constructor
  }



  /**
   * @return the pkVariecon
   */
  public Long getPkVariecon() {
    return pkVariecon;
  }



  /**
   * @param pkVariecon the pkVariecon to set
   */
  public void setPkVariecon(Long pkVariecon) {
    this.pkVariecon = pkVariecon;
  }



  /**
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }



  /**
   * @param codigo the codigo to set
   */
  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }



  /**
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }



  /**
   * @param descripcion the descripcion to set
   */
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }



  /**
   * @return the valor
   */
  public BigDecimal getValor() {
    return valor;
  }



  /**
   * @param valor the valor to set
   */
  public void setValor(BigDecimal valor) {
    this.valor = valor;
  }



  /**
   * @return the ejercicio
   */
  public EjercicioDTO getEjercicio() {
    return ejercicio;
  }



  /**
   * @param ejercicio the ejercicio to set
   */
  public void setEjercicio(EjercicioDTO ejercicio) {
    this.ejercicio = ejercicio;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
