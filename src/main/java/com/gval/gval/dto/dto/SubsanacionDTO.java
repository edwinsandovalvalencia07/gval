package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import es.gva.dependencia.ada.common.enums.listados.TipoSubsanacionEnum;
import com.gval.gval.dto.dto.util.BaseDTO;

import com.fasterxml.jackson.annotation.JsonBackReference;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.zkoss.util.resource.Labels;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * The Class SubsanacionDTO.
 */
public final class SubsanacionDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -6517420183622809933L;

  /** The pk subsanacion. */
  private Long pkSubsanacion;

  /** The activo. */
  @NotNull
  private Boolean activo;

  /** The derecho pendiente. */
  @NotNull
  private Boolean derechoPendiente;

  /** The fase. */
  @Size(max = 1)
  private String fase;

  /** The fecha completado. */
  private Date fechaCompletado;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The observaciones. */
  @Size(max = 4000)
  private String observaciones;

  /** The perfil subsanador. */
  @NotNull
  private Long perfilSubsanador;

  /** The documentos subsanacion. */
  @JsonBackReference
  private List<DocumentoSubsanacionDTO> documentosSubsanacion;

  /** The documento generado. */
  private DocumentoGeneradoDTO documentoGenerado;

  /** The solicitud. */
  private SolicitudDTO solicitud;

  /** The usuario. */
  private UsuarioDTO usuario;

  /** The bloquea tramite. */
  private boolean bloqueaTramite;

  /** The app origen. */
  private String appOrigen;

  /** The documento A subsanar. */
  private DocumentoAportadoDTO documentoASubsanar;


  /**
   * Instantiates a new subsanacion DTO.
   */
  public SubsanacionDTO() {
    super();
    this.bloqueaTramite = true;
    this.activo = Boolean.TRUE;
    this.derechoPendiente = Boolean.FALSE;
    this.perfilSubsanador = Long.valueOf(0L);
    this.fechaCreacion = new Date();
    documentosSubsanacion = new ArrayList<>();
  }


  /**
   * Instantiates a new subsanacion DTO.
   *
   * @param solicitud the solicitud
   */
  public SubsanacionDTO(final SolicitudDTO solicitud) {
    this();
    this.solicitud = solicitud;
  }


  /**
   * Gets the pk subsanacion.
   *
   * @return the pkSubsanacion
   */
  public Long getPkSubsanacion() {
    return pkSubsanacion;
  }

  /**
   * Sets the pk subsanacion.
   *
   * @param pkSubsanacion the pkSubsanacion to set
   */
  public void setPkSubsanacion(final Long pkSubsanacion) {
    this.pkSubsanacion = pkSubsanacion;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the derecho pendiente.
   *
   * @return the derechoPendiente
   */
  public Boolean getDerechoPendiente() {
    return derechoPendiente;
  }

  /**
   * Sets the derecho pendiente.
   *
   * @param derechoPendiente the derechoPendiente to set
   */
  public void setDerechoPendiente(final Boolean derechoPendiente) {
    this.derechoPendiente = derechoPendiente;
  }

  /**
   * Gets the fase.
   *
   * @return the fase
   */
  public String getFase() {
    return fase;
  }

  /**
   * Sets the fase.
   *
   * @param fase the fase to set
   */
  public void setFase(final String fase) {
    this.fase = fase;
  }

  /**
   * Gets the fecha completado.
   *
   * @return the fechaCompletado
   */
  public Date getFechaCompletado() {
    return UtilidadesCommons.cloneDate(fechaCompletado);
  }

  /**
   * Sets the fecha completado.
   *
   * @param fechaCompletado the fechaCompletado to set
   */
  public void setFechaCompletado(final Date fechaCompletado) {
    this.fechaCompletado = UtilidadesCommons.cloneDate(fechaCompletado);
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return StringUtils.trim(observaciones);
  }

  /**
   * Sets the observaciones.
   *
   * @param observaciones the observaciones to set
   */
  public void setObservaciones(final String observaciones) {
    this.observaciones = StringUtils.trim(observaciones);
  }

  /**
   * Gets the perfil subsanador.
   *
   * @return the perfilSubsanador
   */
  public Long getPerfilSubsanador() {
    return perfilSubsanador;
  }

  /**
   * Sets the perfil subsanador.
   *
   * @param perfilSubsanador the perfilSubsanador to set
   */
  public void setPerfilSubsanador(final Long perfilSubsanador) {
    this.perfilSubsanador = perfilSubsanador;
  }

  /**
   * Gets the documento generado.
   *
   * @return the documentoGenerado
   */
  public DocumentoGeneradoDTO getDocumentoGenerado() {
    return documentoGenerado;
  }

  /**
   * Sets the documento generado.
   *
   * @param documentoGenerado the documentoGenerado to set
   */
  public void setDocumentoGenerado(
      final DocumentoGeneradoDTO documentoGenerado) {
    this.documentoGenerado = documentoGenerado;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public SolicitudDTO getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the solicitud to set
   */
  public void setSolicitud(final SolicitudDTO solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the usuario to set
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the documentos subsanacion.
   *
   * @return the documentos subsanacion
   */
  public List<DocumentoSubsanacionDTO> getDocumentosSubsanacion() {
    return UtilidadesCommons.collectorsToList(documentosSubsanacion);
  }

  /**
   * Sets the documentos subsanacion.
   *
   * @param documentosSubsanacion the new documentos subsanacion
   */
  public void setDocumentosSubsanacion(
      final List<DocumentoSubsanacionDTO> documentosSubsanacion) {
    this.documentosSubsanacion =
        UtilidadesCommons.collectorsToList(documentosSubsanacion);
  }

  /**
   * Gets the tipo subsanacion label.
   *
   * @return the tipo subsanacion label
   */
  public String getTipoSubsanacionLabel() {
    final String label = TipoSubsanacionEnum.labelFromCodigo(fase);
    return label == null ? null : Labels.getLabel(label);
  }

  /**
   * Checks if is bloquea tramite.
   *
   * @return true, if is bloquea tramite
   */
  public boolean isBloqueaTramite() {
    return bloqueaTramite;
  }


  /**
   * Sets the bloquea tramite.
   *
   * @param bloqueaTramite the new bloquea tramite
   */
  public void setBloqueaTramite(final boolean bloqueaTramite) {
    this.bloqueaTramite = bloqueaTramite;
  }

  /**
   * Adds the documentos subsanacion.
   *
   * @param documentosSubsanacionAnyadidos the documentos requerido to
   *        documentos subsanacion
   */
  public void addDocumentosSubsanacion(
      final List<DocumentoSubsanacionDTO> documentosSubsanacionAnyadidos) {
    documentosSubsanacion.addAll(documentosSubsanacionAnyadidos);
  }


  /**
   * Gets the app origen.
   *
   * @return the app origen
   */
  public String getAppOrigen() {
    return appOrigen;
  }


  /**
   * Sets the app origen.
   *
   * @param appOrigen the new app origen
   */
  public void setAppOrigen(final String appOrigen) {
    this.appOrigen = appOrigen;
  }

  /**
   * Gets the documento A subsanar.
   *
   * @return the documento A subsanar
   */
  public DocumentoAportadoDTO getDocumentoASubsanar() {
    return documentoASubsanar;
  }


  /**
   * Sets the documento A subsanar.
   *
   * @param documentoASubsanar the new documento A subsanar
   */
  public void setDocumentoASubsanar(
      final DocumentoAportadoDTO documentoASubsanar) {
    this.documentoASubsanar = documentoASubsanar;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
