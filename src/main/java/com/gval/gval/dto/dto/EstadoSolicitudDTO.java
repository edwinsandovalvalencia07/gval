package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;

/**
 * The Class EstadoSolicitudDTO.
 */
public class EstadoSolicitudDTO extends BaseDTO
    implements Comparable<EstadoSolicitudDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -1294343287761234L;

  /** The pk estsol. */
  private Long pkEstsol;

  /** The activo. */
  private Boolean activo;

  /** The fecha hasta. */
  private Date fechaHasta;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The fecha desde. */
  private Date fechaDesde;

  /** The observacion. */
  private String observacion;

  /** The estado. */
  private EstadoDTO estado;

  /** The subestado. */
  private SubestadoDTO subestado;

  /** The usuario. */
  private UsuarioDTO usuario;

  /** The solicitud. */
  @JsonManagedReference
  private SolicitudDTO solicitud;


  /**
   * Instantiates a new estado solicitud DTO.
   */
  public EstadoSolicitudDTO() {
    super();
  }


  /**
   * Gets the pk estsol.
   *
   * @return the pkEstsol
   */
  public Long getPkEstsol() {
    return pkEstsol;
  }

  /**
   * Sets the pk estsol.
   *
   * @param pkEstsol the pkEstsol to set
   */
  public void setPkEstsol(final Long pkEstsol) {
    this.pkEstsol = pkEstsol;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha hasta.
   *
   * @return the fechaHasta
   */
  public Date getFechaHasta() {
    return UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Sets the fecha hasta.
   *
   * @param fechaHasta the fechaHasta to set
   */
  public void setFechaHasta(final Date fechaHasta) {
    this.fechaHasta = UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha desde.
   *
   * @return the fechaDesde
   */
  public Date getFechaDesde() {
    return UtilidadesCommons.cloneDate(fechaDesde);
  }

  /**
   * Sets the fecha desde.
   *
   * @param fechaDesde the fechaDesde to set
   */
  public void setFechaDesde(final Date fechaDesde) {
    this.fechaDesde = UtilidadesCommons.cloneDate(fechaDesde);
  }

  /**
   * Gets the observacion.
   *
   * @return the observacion
   */
  public String getObservacion() {
    return observacion;
  }

  /**
   * Sets the observacion.
   *
   * @param observacion the observacion to set
   */
  public void setObservacion(final String observacion) {
    this.observacion = observacion;
  }

  /**
   * Gets the estado.
   *
   * @return the estado
   */
  public EstadoDTO getEstado() {
    return estado;
  }

  /**
   * Sets the estado.
   *
   * @param estado the estado to set
   */
  public void setEstado(final EstadoDTO estado) {
    this.estado = estado;
  }

  /**
   * Gets the subestado.
   *
   * @return the subestado
   */
  public SubestadoDTO getSubestado() {
    return subestado;
  }

  /**
   * Gets the nombre completo.
   *
   * @return the nombre completo
   */
  public String getNombreCompleto() {
    if (estado == null) {
      return "";
    } else {
      return subestado == null ? estado.getNombre()
          : (estado.getNombre().concat(StringUtils.SPACE)
              .concat(subestado.getDescripcion()));
    }
  }

  /**
   * Sets the subestado.
   *
   * @param subestado the subestado to set
   */
  public void setSubestado(final SubestadoDTO subestado) {
    this.subestado = subestado;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }


  /**
   * Sets the usuario.
   *
   * @param usuario the usuario to set
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public SolicitudDTO getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the solicitud to set
   */
  public void setSolicitud(final SolicitudDTO solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final EstadoSolicitudDTO o) {
    return 0;
  }

  /**
   * Inicialiación Estado Solicitud.
   *
   * @return estadoSolicitud
   */
  public static EstadoSolicitudDTO createInstance() {
    final EstadoSolicitudDTO estadoSolicitud = new EstadoSolicitudDTO();

    estadoSolicitud.setFechaDesde(new Date());
    estadoSolicitud.setActivo(Boolean.TRUE);
    estadoSolicitud.setFechaHasta(null);
    estadoSolicitud.setFechaCreacion(new Date());

    return estadoSolicitud;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
