/**
 * Copyright (c) 2020 Generalitat Valenciana - Todos los derechos reservados.
 */
package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.validator.constraints.Length;

import java.util.Date;

import javax.validation.constraints.NotNull;

/**
 * The Class EstadoCivilDTO.
 */
public class EstadoCivilDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -5035856688930555741L;

  /** The pk estado civil. */
  private Long pkEstadoCivil;

  /** The activo. */
  @NotNull
  private Boolean activo;

  /** The fecha creacion. */
  @NotNull
  private Date fechaCreacion;

  /** The imserso. */
  @Length(max = 1)
  private String imserso;

  /** The nombre. */
  @NotNull
  @Length(max = 15)
  private String nombre;

  /**
   * Gets the pk estado civil.
   *
   * @return the pk estado civil
   */
  public Long getPkEstadoCivil() {
    return pkEstadoCivil;
  }

  /**
   * Sets the pk estado civil.
   *
   * @param pkEstadoCivil the new pk estado civil
   */
  public void setPkEstadoCivil(final Long pkEstadoCivil) {
    this.pkEstadoCivil = pkEstadoCivil;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the imserso.
   *
   * @return the imserso
   */
  public String getImserso() {
    return imserso;
  }

  /**
   * Sets the imserso.
   *
   * @param imserso the new imserso
   */
  public void setImserso(final String imserso) {
    this.imserso = imserso;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
