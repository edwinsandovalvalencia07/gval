package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * The persistent class for the SDM_COMPATIBILIDADES_PIA database table.
 *
 */
public class CompatibilidadesPiaDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk com pia. */
  private long pkComPia;

  /** The activo. */
  private Boolean activo;

  /** The check grado 32. */
  private Boolean checkGrado32;

  /** The pk cataserv 1. */
  private CatalogoServicioDTO servicio1;

  /** The pk cataserv 2. */
  private CatalogoServicioDTO servicio2;

  /**
   * Instantiates a new compatibilidades pia DTO.
   */
  public CompatibilidadesPiaDTO() {
    super();
  }

  /**
   * Instantiates a new compatibilidades pia DTO.
   *
   * @param pkComPia the pk com pia
   * @param activo the activo
   * @param checkGrado32 the check grado 32
   * @param servicio1 the servicio 1
   * @param servicio2 the servicio 2
   */
  public CompatibilidadesPiaDTO(final long pkComPia, final Boolean activo,
      final Boolean checkGrado32, final CatalogoServicioDTO servicio1,
      final CatalogoServicioDTO servicio2) {
    super();
    this.pkComPia = pkComPia;
    this.activo = activo;
    this.checkGrado32 = checkGrado32;
    this.servicio1 = servicio1;
    this.servicio2 = servicio2;
  }

  /**
   * Gets the pk com pia.
   *
   * @return the pk com pia
   */
  public long getPkComPia() {
    return pkComPia;
  }

  /**
   * Sets the pk com pia.
   *
   * @param pkComPia the new pk com pia
   */
  public void setPkComPia(final long pkComPia) {
    this.pkComPia = pkComPia;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the check grado 32.
   *
   * @return the check grado 32
   */
  public Boolean getCheckGrado32() {
    return checkGrado32;
  }

  /**
   * Sets the check grado 32.
   *
   * @param checkGrado32 the new check grado 32
   */
  public void setCheckGrado32(final Boolean checkGrado32) {
    this.checkGrado32 = checkGrado32;
  }


  /**
   * @return the servicio1
   */
  public CatalogoServicioDTO getServicio1() {
    return servicio1;
  }

  /**
   * @param servicio1 the servicio1 to set
   */
  public void setServicio1(final CatalogoServicioDTO servicio1) {
    this.servicio1 = servicio1;
  }

  /**
   * @return the servicio2
   */
  public CatalogoServicioDTO getServicio2() {
    return servicio2;
  }

  /**
   * @param servicio2 the servicio2 to set
   */
  public void setServicio2(final CatalogoServicioDTO servicio2) {
    this.servicio2 = servicio2;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
