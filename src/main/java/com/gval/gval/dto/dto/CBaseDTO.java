package com.gval.gval.dto.dto;


import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;

/**
 * The Class CBaseDTO.
 */
public class CBaseDTO extends BaseDTO implements Comparable<CBaseDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -8639380464752152478L;

  /** The identificador. */
  private String identificador;

  /** The nombre. */
  private String nombre;

  /** The primer apellido. */
  private String primerApellido;

  /** The segundo apellido. */
  private String segundoApellido;

  /** The fecha nacimiento. */
  private Date fechaNacimiento;

  /** The existe PDF. */
  private String existePDF;

  /** The puntos ATP. */
  private Long puntosATP;

  /** The grado discapacidad. */
  private Long gradoDiscapacidad;

  /** The fecha junta. */
  private Date fechaJunta;

  /** The fecha proxima revision. */
  private Date fechaProximaRevision;

  /** The resultado baremacion. */
  private String resultadoBaremacion;

  /** The fecha valoracion. */
  private Date fechaValoracion;

  /** The provincia. */
  private String provincia;

  /** The provincia DTO. */
  private ProvinciaDTO provinciaDTO;

  /** The estado. */
  private String estado;


  /**
   * Instantiates a new c base DTO.
   */
  public CBaseDTO() {
    super();
  }

  /**
   * Instantiates a new c base DTO.
   *
   * @param identificador the identificador
   * @param nombre the nombre
   * @param primerApellido the primer apellido
   * @param segundoApellido the segundo apellido
   * @param fechaNacimiento the fecha nacimiento
   * @param existePDF the existe PDF
   * @param puntosATP the puntos ATP
   * @param gradoDiscapacidad the grado discapacidad
   * @param fechaJunta the fecha junta
   * @param fechaProximaRevision the fecha proxima revision
   * @param resultadoBaremacion the resultado baremacion
   * @param fechaValoracion the fecha valoracion
   * @param provincia the provincia
   * @param estado the estado
   */
  public CBaseDTO(final String identificador, final String nombre,
      final String primerApellido, final String segundoApellido,
      final Date fechaNacimiento, final String existePDF, final Long puntosATP,
      final Long gradoDiscapacidad, final Date fechaJunta,
      final Date fechaProximaRevision, final String resultadoBaremacion,
      final Date fechaValoracion, final String provincia, final String estado) {
    super();
    this.identificador = identificador;
    this.nombre = nombre;
    this.primerApellido = primerApellido;
    this.segundoApellido = segundoApellido;
    this.fechaNacimiento = UtilidadesCommons.cloneDate(fechaNacimiento);
    this.existePDF = existePDF;
    this.puntosATP = puntosATP;
    this.gradoDiscapacidad = gradoDiscapacidad;
    this.fechaJunta = UtilidadesCommons.cloneDate(fechaJunta);
    this.fechaProximaRevision =
        UtilidadesCommons.cloneDate(fechaProximaRevision);
    this.resultadoBaremacion = resultadoBaremacion;
    this.fechaValoracion = UtilidadesCommons.cloneDate(fechaValoracion);
    this.provincia = provincia;
    this.estado = estado;
  }

  /**
   * Gets the identificador.
   *
   * @return the identificador
   */
  public String getIdentificador() {
    return identificador;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Gets the primer apellido.
   *
   * @return the primer apellido
   */
  public String getPrimerApellido() {
    return primerApellido;
  }

  /**
   * Gets the segundo apellido.
   *
   * @return the segundo apellido
   */
  public String getSegundoApellido() {
    return segundoApellido;
  }

  /**
   * Gets the fecha nacimiento.
   *
   * @return the fecha nacimiento
   */
  public Date getFechaNacimiento() {
    return UtilidadesCommons.cloneDate(fechaNacimiento);
  }

  /**
   * Gets the existe PDF.
   *
   * @return the existe PDF
   */
  public String getExistePDF() {
    return existePDF;
  }

  /**
   * Gets the puntos ATP.
   *
   * @return the puntos ATP
   */
  public Long getPuntosATP() {
    return puntosATP;
  }

  /**
   * Gets the grado discapacidad.
   *
   * @return the grado discapacidad
   */
  public Long getGradoDiscapacidad() {
    return gradoDiscapacidad;
  }

  /**
   * Gets the fecha junta.
   *
   * @return the fecha junta
   */
  public Date getFechaJunta() {
    return UtilidadesCommons.cloneDate(fechaJunta);
  }

  /**
   * Gets the fecha proxima revision.
   *
   * @return the fecha proxima revision
   */
  public Date getFechaProximaRevision() {
    return UtilidadesCommons.cloneDate(fechaProximaRevision);
  }

  /**
   * Gets the fecha valoracion.
   *
   * @return the fecha valoracion
   */
  public Date getFechaValoracion() {
    return UtilidadesCommons.cloneDate(fechaValoracion);
  }

  /**
   * Gets the provincia.
   *
   * @return the provincia
   */
  public String getProvincia() {
    return provincia;
  }

  /**
   * Sets the identificador.
   *
   * @param identificador the new identificador
   */
  public void setIdentificador(final String identificador) {
    this.identificador = identificador;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Sets the primer apellido.
   *
   * @param primerApellido the new primer apellido
   */
  public void setPrimerApellido(final String primerApellido) {
    this.primerApellido = primerApellido;
  }

  /**
   * Sets the segundo apellido.
   *
   * @param segundoApellido the new segundo apellido
   */
  public void setSegundoApellido(final String segundoApellido) {
    this.segundoApellido = segundoApellido;
  }

  /**
   * Sets the fecha nacimiento.
   *
   * @param fechaNacimiento the new fecha nacimiento
   */
  public void setFechaNacimiento(final Date fechaNacimiento) {
    this.fechaNacimiento = UtilidadesCommons.cloneDate(fechaNacimiento);
  }

  /**
   * Sets the existe PDF.
   *
   * @param existePDF the new existe PDF
   */
  public void setExistePDF(final String existePDF) {
    this.existePDF = existePDF;
  }

  /**
   * Sets the puntos ATP.
   *
   * @param puntosATP the new puntos ATP
   */
  public void setPuntosATP(final Long puntosATP) {
    this.puntosATP = puntosATP;
  }

  /**
   * Sets the grado discapacidad.
   *
   * @param gradoDiscapacidad the new grado discapacidad
   */
  public void setGradoDiscapacidad(final Long gradoDiscapacidad) {
    this.gradoDiscapacidad = gradoDiscapacidad;
  }

  /**
   * Sets the fecha junta.
   *
   * @param fechaJunta the new fecha junta
   */
  public void setFechaJunta(final Date fechaJunta) {
    this.fechaJunta = UtilidadesCommons.cloneDate(fechaJunta);
  }

  /**
   * Sets the fecha proxima revision.
   *
   * @param fechaProximaRevision the new fecha proxima revision
   */
  public void setFechaProximaRevision(final Date fechaProximaRevision) {
    this.fechaProximaRevision =
        UtilidadesCommons.cloneDate(fechaProximaRevision);
  }

  /**
   * Sets the fecha valoracion.
   *
   * @param fechaValoracion the new fecha valoracion
   */
  public void setFechaValoracion(final Date fechaValoracion) {
    this.fechaValoracion = UtilidadesCommons.cloneDate(fechaValoracion);
  }

  /**
   * Sets the provincia.
   *
   * @param provincia the new provincia
   */
  public void setProvincia(final String provincia) {
    this.provincia = provincia;
  }

  /**
   * Gets the resultado baremacion.
   *
   * @return the resultado baremacion
   */
  public String getResultadoBaremacion() {
    return resultadoBaremacion;
  }

  /**
   * Sets the resultado baremacion.
   *
   * @param resultadoBaremacion the new resultado baremacion
   */
  public void setResultadoBaremacion(final String resultadoBaremacion) {
    this.resultadoBaremacion = resultadoBaremacion;
  }

  /**
   * Gets the provincia DTO.
   *
   * @return the provincia DTO
   */
  public ProvinciaDTO getProvinciaDTO() {
    return provinciaDTO;
  }

  /**
   * Sets the provincia DTO.
   *
   * @param provinciaDTO the new provincia DTO
   */
  public void setProvinciaDTO(final ProvinciaDTO provinciaDTO) {
    this.provinciaDTO = provinciaDTO;
  }

  /**
   * Gets the estado.
   *
   * @return the estado
   */
  public String getEstado() {
    return estado;
  }

  /**
   * Sets the estado.
   *
   * @param estado the new estado
   */
  public void setEstado(final String estado) {
    this.estado = estado;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final CBaseDTO o) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
