package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;


/**
 * The Class UnidadUsuarioDTO.
 */
public class UnidadUsuarioDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -7113037052349247419L;

  /** The pk unidad usuario. */
  private Long pkUnidadUsuario;

  /** The activo. */
  private boolean activo;

  /** The fecha hasta. */
  private Date fechaHasta;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The fecha desde. */
  private Date fechaDesde;

  /** The unidad. */
  private UnidadDTO unidad;

  /** The usuario. */
  private UsuarioDTO usuario;

  /**
   * Instantiates a new unidad usuario DTO.
   */
  public UnidadUsuarioDTO() {
    super();
  }

  /**
   * Gets the pk unidad usuario.
   *
   * @return the pk unidad usuario
   */
  public Long getPkUnidadUsuario() {
    return pkUnidadUsuario;
  }

  /**
   * Sets the pk unidad usuario.
   *
   * @param pkUnidadUsuario the new pk unidad usuario
   */
  public void setPkUnidadUsuario(final Long pkUnidadUsuario) {
    this.pkUnidadUsuario = pkUnidadUsuario;
  }

  /**
   * Checks if is activo.
   *
   * @return true, if is activo
   */
  public boolean isActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha hasta.
   *
   * @return the fecha hasta
   */
  public Date getFechaHasta() {
    return UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Sets the fecha hasta.
   *
   * @param fechaHasta the new fecha hasta
   */
  public void setFechaHasta(final Date fechaHasta) {
    this.fechaHasta = UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha desde.
   *
   * @return the fecha desde
   */
  public Date getFechaDesde() {
    return UtilidadesCommons.cloneDate(fechaDesde);
  }

  /**
   * Sets the fecha desde.
   *
   * @param fechaDesde the new fecha desde
   */
  public void setFechaDesde(final Date fechaDesde) {
    this.fechaDesde = UtilidadesCommons.cloneDate(fechaDesde);
  }

  /**
   * Gets the unidad.
   *
   * @return the unidad
   */
  public UnidadDTO getUnidad() {
    return unidad;
  }

  /**
   * Sets the unidad.
   *
   * @param unidad the new unidad
   */
  public void setUnidad(final UnidadDTO unidad) {
    this.unidad = unidad;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }


}
