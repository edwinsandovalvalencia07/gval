/**
 * Copyright (c) 2020 Generalitat Valenciana - Todos los derechos reservados.
 */
package com.gval.gval.dto.dto;

import es.gva.dependencia.ada.common.ConstantesWeb;
import com.gval.gval.common.UtilidadesCommons;
import es.gva.dependencia.ada.common.enums.MotivoGeneracionEnum;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * The Class MotivoGeneracionInformeSocialDTO.
 */
public final class MotivoGeneracionInformeSocialDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 7415775720074480014L;

  /** The pk motgeneracion is. */
  private Long pkMotgeneracionIs;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The observaciones. */
  @Size(max = 300)
  private String observaciones;

  /** The informe social. */
  @NotNull
  private InformeSocialDTO informeSocial;

  /** The motivo generacion. */
  @NotNull
  private MotivoGeneracionDTO motivoGeneracion;

  /** The usuario. */
  @NotNull
  private UsuarioDTO usuario;



  /**
   * Instantiates a new motivo generacion informe social DTO.
   */
  public MotivoGeneracionInformeSocialDTO() {
    super();
    this.informeSocial = new InformeSocialDTO();
    this.motivoGeneracion = new MotivoGeneracionDTO();
    this.usuario = new UsuarioDTO();
  }

  /**
   * Gets the pk motgeneracion is.
   *
   * @return the pkMotgeneracionIs
   */
  public Long getPkMotgeneracionIs() {
    return pkMotgeneracionIs;
  }

  /**
   * Sets the pk motgeneracion is.
   *
   * @param pkMotgeneracionIs the pkMotgeneracionIs to set
   */
  public void setPkMotgeneracionIs(final Long pkMotgeneracionIs) {
    this.pkMotgeneracionIs = pkMotgeneracionIs;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return observaciones;
  }

  /**
   * Sets the observaciones.
   *
   * @param observaciones the observaciones to set
   */
  public void setObservaciones(final String observaciones) {
    this.observaciones = observaciones;
  }

  /**
   * Gets the informe social.
   *
   * @return the informeSocial
   */
  public InformeSocialDTO getInformeSocial() {
    return informeSocial;
  }

  /**
   * Sets the informe social.
   *
   * @param informeSocial the informeSocial to set
   */
  public void setInformeSocial(final InformeSocialDTO informeSocial) {
    this.informeSocial = informeSocial;
  }

  /**
   * Gets the motivo generacion.
   *
   * @return the motivoGeneracion
   */
  public MotivoGeneracionDTO getMotivoGeneracion() {
    return motivoGeneracion;
  }

  /**
   * Gets the texto motivo generacion.
   *
   * @return the texto motivo generacion
   */
  public String getTextoMotivoGeneracion() {
    final String motivo = motivoGeneracion.getNombre();
    final StringBuilder textoMotivo = new StringBuilder();
    textoMotivo.append(motivo);
    if (MotivoGeneracionEnum.OTROS.getCodigo()
        .equals(motivoGeneracion.getCodigo())) {
      textoMotivo.append(ConstantesWeb.ESPACIO)
          .append(UtilidadesCommons.textoEntreParentesis(observaciones));
    }
    return textoMotivo.toString();
  }

  /**
   * Sets the motivo generacion.
   *
   * @param motivoGeneracion the motivoGeneracion to set
   */
  public void setMotivoGeneracion(final MotivoGeneracionDTO motivoGeneracion) {
    this.motivoGeneracion = motivoGeneracion;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the usuario to set
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }



}
