package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;

/**
 * The Class EstadoDTO.
 */
public class EstadoDTO extends BaseDTO
    implements Comparable<EstadoSolicitudDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 6533866344085027599L;

  /** The pk estado. */
  private long pkEstado;

  /** The codigo. */
  private String codigo;

  /** The descripcion. */
  private String descripcion;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The es estado final. */
  private Boolean esEstadoFinal;

  /** The nombre. */
  private String nombre;

  /** The proceso. */
  private Long proceso;

  /** The activo. */
  private Boolean activo;

  /** The nsisaad terminado. */
  private Boolean nsisaadTerminado;

  /**
   * Instantiates a new estado DTO.
   */
  public EstadoDTO() {
    super();
  }

  /**
   * Gets the pk estado.
   *
   * @return the pkEstado
   */
  public long getPkEstado() {
    return pkEstado;
  }

  /**
   * Sets the pk estado.
   *
   * @param pkEstado the pkEstado to set
   */
  public void setPkEstado(final long pkEstado) {
    this.pkEstado = pkEstado;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the codigo to set
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the descripcion to set
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the es estado final.
   *
   * @return the esEstadoFinal
   */
  public Boolean getEsEstadoFinal() {
    return esEstadoFinal;
  }

  /**
   * Sets the es estado final.
   *
   * @param esEstadoFinal the esEstadoFinal to set
   */
  public void setEsEstadoFinal(final Boolean esEstadoFinal) {
    this.esEstadoFinal = esEstadoFinal;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the nombre to set
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the proceso.
   *
   * @return the proceso
   */
  public Long getProceso() {
    return proceso;
  }

  /**
   * Sets the proceso.
   *
   * @param proceso the proceso to set
   */
  public void setProceso(final Long proceso) {
    this.proceso = proceso;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the nsisaad terminado.
   *
   * @return the nsisaadTerminado
   */
  public Boolean getNsisaadTerminado() {
    return nsisaadTerminado;
  }

  /**
   * Sets the nsisaad terminado.
   *
   * @param nsisaadTerminado the nsisaadTerminado to set
   */
  public void setNsisaadTerminado(final Boolean nsisaadTerminado) {
    this.nsisaadTerminado = nsisaadTerminado;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final EstadoSolicitudDTO o) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
