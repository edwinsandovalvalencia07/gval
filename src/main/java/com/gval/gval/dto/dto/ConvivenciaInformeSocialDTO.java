/**
 * Copyright (c) 2020 Generalitat Valenciana - Todos los derechos reservados.
 */
package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * The Class ConvivenciaInformeSocialDTO.
 */
public class ConvivenciaInformeSocialDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1907057655285665877L;

  /** The pk conviven. */
  private Long pkConviven;

  /** The ayuda administracion. */
  private Boolean ayudaAdministracion;

  /** The ayuda comunicacion. */
  private Boolean ayudaComunicacion;

  /** The ayuda decisiones. */
  private Boolean ayudaDecisiones;

  /** The ayuda desplazamiento. */
  private Boolean ayudaDesplazamiento;

  /** The ayuda higiene. */
  private Boolean ayudaHigiene;

  /** The ayuda conflictos. */
  private Boolean ayudaConflictos;

  /** The tipo cuidador. */
  @NotBlank
  private Integer tipoCuidador;

  /** The cuidados hogar. */
  private Boolean cuidadosHogar;

  /** The cuidados otros. */
  private Boolean cuidadosOtros;

  /** The edad. */
  private Integer edad;

  /** The intensidad. */
  @Size(max = 1)
  private String intensidad;

  /** The nombre. */
  @Size(max = 50)
  private String nombre;

  /** The nombre otros dependientes. */
  @Size(max = 200)
  private String nombreOtrosDependientes;

  /** The cuida otros dependientes. */
  private Boolean cuidaOtrosDependientes;

  /** The principal. */
  private Boolean principal;

  /** The tipo jornada. */
  private Integer tipoJornada;

  /** The tipo trabajo. */
  private Integer tipoTrabajo;

  /** The trabajo fuera hogar. */
  private Boolean trabajoFueraHogar;

  /** The informe social. */
  @NotNull
  private InformeSocialDTO informeSocial;

  /** The cuidador relacion otros dependientes. */
  private TipoFamiliarDTO cuidadorRelacionOtrosDependientes;

  /** The cuidador relacion principal. */
  private TipoFamiliarDTO cuidadorRelacionPrincipal;

  /**
   * Crea una instancia de Convivencia para Informe Social DTO.
   */
  public ConvivenciaInformeSocialDTO() {
    super();
  }

  /**
   * Instantiates a new convivencia informe social DTO.
   *
   * @param informeSocialDTO the informe social DTO
   */
  public ConvivenciaInformeSocialDTO(final InformeSocialDTO informeSocialDTO) {
    this();
    this.informeSocial = informeSocialDTO;
  }

  /**
   * @return the pkConviven
   */
  public Long getPkConviven() {
    return pkConviven;
  }

  /**
   * @param pkConviven the pkConviven to set
   */
  public void setPkConviven(final Long pkConviven) {
    this.pkConviven = pkConviven;
  }

  /**
   * @return the ayudaAdministracion
   */
  public Boolean getAyudaAdministracion() {
    return ayudaAdministracion;
  }

  /**
   * @param ayudaAdministracion the ayudaAdministracion to set
   */
  public void setAyudaAdministracion(final Boolean ayudaAdministracion) {
    this.ayudaAdministracion = ayudaAdministracion;
  }

  /**
   * @return the ayudaComunicacion
   */
  public Boolean getAyudaComunicacion() {
    return ayudaComunicacion;
  }

  /**
   * @param ayudaComunicacion the ayudaComunicacion to set
   */
  public void setAyudaComunicacion(final Boolean ayudaComunicacion) {
    this.ayudaComunicacion = ayudaComunicacion;
  }

  /**
   * @return the ayudaDecisiones
   */
  public Boolean getAyudaDecisiones() {
    return ayudaDecisiones;
  }

  /**
   * @param ayudaDecisiones the ayudaDecisiones to set
   */
  public void setAyudaDecisiones(final Boolean ayudaDecisiones) {
    this.ayudaDecisiones = ayudaDecisiones;
  }

  /**
   * @return the ayudaDesplazamiento
   */
  public Boolean getAyudaDesplazamiento() {
    return ayudaDesplazamiento;
  }

  /**
   * @param ayudaDesplazamiento the ayudaDesplazamiento to set
   */
  public void setAyudaDesplazamiento(final Boolean ayudaDesplazamiento) {
    this.ayudaDesplazamiento = ayudaDesplazamiento;
  }

  /**
   * @return the ayudaHigiene
   */
  public Boolean getAyudaHigiene() {
    return ayudaHigiene;
  }

  /**
   * @param ayudaHigiene the ayudaHigiene to set
   */
  public void setAyudaHigiene(final Boolean ayudaHigiene) {
    this.ayudaHigiene = ayudaHigiene;
  }

  /**
   * @return the ayudaConflictos
   */
  public Boolean getAyudaConflictos() {
    return ayudaConflictos;
  }

  /**
   * @param ayudaConflictos the ayudaConflictos to set
   */
  public void setAyudaConflictos(final Boolean ayudaConflictos) {
    this.ayudaConflictos = ayudaConflictos;
  }

  /**
   * @return the tipoCuidador
   */
  public Integer getTipoCuidador() {
    return tipoCuidador;
  }

  /**
   * @param tipoCuidador the tipoCuidador to set
   */
  public void setTipoCuidador(final Integer tipoCuidador) {
    this.tipoCuidador = tipoCuidador;
  }

  /**
   * @return the cuidadosHogar
   */
  public Boolean getCuidadosHogar() {
    return cuidadosHogar;
  }

  /**
   * @param cuidadosHogar the cuidadosHogar to set
   */
  public void setCuidadosHogar(final Boolean cuidadosHogar) {
    this.cuidadosHogar = cuidadosHogar;
  }

  /**
   * @return the cuidadosOtros
   */
  public Boolean getCuidadosOtros() {
    return cuidadosOtros;
  }

  /**
   * @param cuidadosOtros the cuidadosOtros to set
   */
  public void setCuidadosOtros(final Boolean cuidadosOtros) {
    this.cuidadosOtros = cuidadosOtros;
  }

  /**
   * @return the edad
   */
  public Integer getEdad() {
    return edad;
  }

  /**
   * @param edad the edad to set
   */
  public void setEdad(final Integer edad) {
    this.edad = (edad == null) ? 0 : edad;
  }

  /**
   * @return the intensidad
   */
  public String getIntensidad() {
    return intensidad;
  }

  /**
   * @param intensidad the intensidad to set
   */
  public void setIntensidad(final String intensidad) {
    this.intensidad = intensidad;
  }

  /**
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * @param nombre the nombre to set
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * @return the nombreOtrosDependientes
   */
  public String getNombreOtrosDependientes() {
    return nombreOtrosDependientes;
  }

  /**
   * @param nombreOtrosDependientes the nombreOtrosDependientes to set
   */
  public void setNombreOtrosDependientes(final String nombreOtrosDependientes) {
    this.nombreOtrosDependientes = nombreOtrosDependientes;
  }

  /**
   * @return the cuidaOtrosDependientes
   */
  public Boolean getCuidaOtrosDependientes() {
    return cuidaOtrosDependientes;
  }

  /**
   * @param cuidaOtrosDependientes the cuidaOtrosDependientes to set
   */
  public void setCuidaOtrosDependientes(final Boolean cuidaOtrosDependientes) {
    this.cuidaOtrosDependientes = cuidaOtrosDependientes;
  }

  /**
   * @return the principal
   */
  public Boolean getPrincipal() {
    return principal;
  }

  /**
   * @param principal the principal to set
   */
  public void setPrincipal(final Boolean principal) {
    this.principal = principal;
  }

  /**
   * @return the tipoJornada
   */
  public Integer getTipoJornada() {
    return tipoJornada;
  }

  /**
   * @param tipoJornada the tipoJornada to set
   */
  public void setTipoJornada(final Integer tipoJornada) {
    this.tipoJornada = tipoJornada;
  }

  /**
   * @return the tipoTrabajo
   */
  public Integer getTipoTrabajo() {
    return tipoTrabajo;
  }

  /**
   * @param tipoTrabajo the tipoTrabajo to set
   */
  public void setTipoTrabajo(final Integer tipoTrabajo) {
    this.tipoTrabajo = tipoTrabajo;
  }

  /**
   * @return the trabajoFueraHogar
   */
  public Boolean getTrabajoFueraHogar() {
    return trabajoFueraHogar;
  }

  /**
   * @param trabajoFueraHogar the trabajoFueraHogar to set
   */
  public void setTrabajoFueraHogar(final Boolean trabajoFueraHogar) {
    this.trabajoFueraHogar = trabajoFueraHogar;
  }

  /**
   * @return the informeSocial
   */
  public InformeSocialDTO getInformeSocial() {
    return informeSocial;
  }

  /**
   * @param informeSocial the informeSocial to set
   */
  public void setInformeSocial(final InformeSocialDTO informeSocial) {
    this.informeSocial = informeSocial;
  }

  /**
   * @return the cuidadorRelacionOtrosDependientes
   */
  public TipoFamiliarDTO getCuidadorRelacionOtrosDependientes() {
    return cuidadorRelacionOtrosDependientes;
  }

  /**
   * @param cuidadorRelacionOtrosDependientes the
   *        cuidadorRelacionOtrosDependientes to set
   */
  public void setCuidadorRelacionOtrosDependientes(
      final TipoFamiliarDTO cuidadorRelacionOtrosDependientes) {
    this.cuidadorRelacionOtrosDependientes = cuidadorRelacionOtrosDependientes;
  }

  /**
   * @return the cuidadorRelacionPrincipal
   */
  public TipoFamiliarDTO getCuidadorRelacionPrincipal() {
    return cuidadorRelacionPrincipal;
  }

  /**
   * @param cuidadorRelacionPrincipal the cuidadorRelacionPrincipal to set
   */
  public void setCuidadorRelacionPrincipal(
      final TipoFamiliarDTO cuidadorRelacionPrincipal) {
    this.cuidadorRelacionPrincipal = cuidadorRelacionPrincipal;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
