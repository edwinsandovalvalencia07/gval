package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * The Class TipoDocumentoDTO.
 */
public final class TipoResolucionDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 6286739913171854311L;

  /** The pk tipo resolución. */
  private String pkTipoResolucion;

  /** The descripción. */
  @Size(max = 400)
  private String descripcion;

  /** The tipo manual. */
  @NotNull
  @Size(max = 1)
  private Boolean manual;

  /** The tipo activo. */
  @Size(max = 1)
  private Boolean activo;

  /** The tipo aporta pdf. */
  @Size(max = 1)
  private Boolean aportaPdf;

  /** The tipo codigo. */
  @Size(max = 15)
  private String codigo;

  /** The tipo tiene PIA. */
  @Size(max = 1)
  private Boolean tienePIA;

  /** The tipo manual. */
  @Size(max = 7)
  private Date fechaCreacion;

  /** The tipo grado y nivel. */
  @Size(max = 1)
  private Boolean esdeGradoyNivel;

  /** The tipo es de PIA. */
  @Size(max = 1)
  private Boolean esdePIA;

  /** The tipo envio BOE. */
  @Size(max = 1)
  private Boolean envioBOE;

  /** The tipo eliminar. */
  @Size(max = 1)
  private Boolean eliminar;

  /** The tipo nsisaad grado. */
  @Size(max = 1)
  private Boolean nsisaadGrado;

  /** The nsisaad archivado. */
  @Size(max = 1)
  private Boolean nsisaadArchivado;

  /** The tipo nsisaad cierreg. */
  @Size(max = 1)
  private Boolean nsisaadCierreg;

  /** The tipo nsisaad pia. */
  @Size(max = 1)
  private Boolean nsisaadPIA;

  /** The tipo nsisaad pendiente arc. */
  @Size(max = 1)
  private Boolean nsisaadPendiente;

  /** The tipo nsisaad comher. */
  @Size(max = 1)
  private Boolean nsisaadComher;

  /** The tipo resoexitus. */
  @Size(max = 1)
  private Boolean resoExitus;

  /** The tipo retro. */
  @Size(max = 1)
  private Boolean retro;

  /** The tipo sec sdv bi tipo tramite. */
  @Size(max = 1)
  private Long tramite;

  /** The tipo reso minorada. */
  @Size(max = 1)
  private Boolean resoMinorada;

  /** The tipo es ADA3. */
  @Size(max = 1)
  private Boolean esADA;

  /**
   * Gets the pk tipo resolucion.
   *
   * @return the pk tipo resolucion
   */
  public String getPkTipoResolucion() {
    return pkTipoResolucion;
  }

  /**
   * Sets the pk tipo resolucion.
   *
   * @param pkTipoResolucion the new pk tipo resolucion
   */
  public void setPkTipoResolucion(final String pkTipoResolucion) {
    this.pkTipoResolucion = pkTipoResolucion;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the manual.
   *
   * @return the manual
   */
  public Boolean getManual() {
    return manual;
  }

  /**
   * Sets the manual.
   *
   * @param manual the new manual
   */
  public void setManual(final Boolean manual) {
    this.manual = manual;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the aporta pdf.
   *
   * @return the aporta pdf
   */
  public Boolean getAportaPdf() {
    return aportaPdf;
  }

  /**
   * Sets the aporta pdf.
   *
   * @param aportaPdf the new aporta pdf
   */
  public void setAportaPdf(final Boolean aportaPdf) {
    this.aportaPdf = aportaPdf;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the tiene PIA.
   *
   * @return the tiene PIA
   */
  public Boolean getTienePIA() {
    return tienePIA;
  }

  /**
   * Sets the tiene PIA.
   *
   * @param tienePIA the new tiene PIA
   */
  public void setTienePIA(final Boolean tienePIA) {
    this.tienePIA = tienePIA;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the esde gradoy nivel.
   *
   * @return the esde gradoy nivel
   */
  public Boolean getEsdeGradoyNivel() {
    return esdeGradoyNivel;
  }

  /**
   * Sets the esde gradoy nivel.
   *
   * @param esdeGradoyNivel the new esde gradoy nivel
   */
  public void setEsdeGradoyNivel(final Boolean esdeGradoyNivel) {
    this.esdeGradoyNivel = esdeGradoyNivel;
  }

  /**
   * Gets the esde PIA.
   *
   * @return the esde PIA
   */
  public Boolean getEsdePIA() {
    return esdePIA;
  }

  /**
   * Sets the esde PIA.
   *
   * @param esdePIA the new esde PIA
   */
  public void setEsdePIA(final Boolean esdePIA) {
    this.esdePIA = esdePIA;
  }

  /**
   * Gets the envio BOE.
   *
   * @return the envio BOE
   */
  public Boolean getEnvioBOE() {
    return envioBOE;
  }

  /**
   * Sets the envio BOE.
   *
   * @param envioBOE the new envio BOE
   */
  public void setEnvioBOE(final Boolean envioBOE) {
    this.envioBOE = envioBOE;
  }

  /**
   * Gets the eliminar.
   *
   * @return the eliminar
   */
  public Boolean getEliminar() {
    return eliminar;
  }

  /**
   * Sets the eliminar.
   *
   * @param eliminar the new eliminar
   */
  public void setEliminar(final Boolean eliminar) {
    this.eliminar = eliminar;
  }

  /**
   * Gets the nsisaad grado.
   *
   * @return the nsisaad grado
   */
  public Boolean getNsisaadGrado() {
    return nsisaadGrado;
  }

  /**
   * Sets the nsisaad grado.
   *
   * @param nsisaadGrado the new nsisaad grado
   */
  public void setNsisaadGrado(final Boolean nsisaadGrado) {
    this.nsisaadGrado = nsisaadGrado;
  }

  /**
   * Gets the nsisaad archivado.
   *
   * @return the nsisaad archivado
   */
  public Boolean getNsisaadArchivado() {
    return nsisaadArchivado;
  }

  /**
   * Sets the nsisaad archivado.
   *
   * @param nsisaadArchivado the new nsisaad archivado
   */
  public void setNsisaadArchivado(final Boolean nsisaadArchivado) {
    this.nsisaadArchivado = nsisaadArchivado;
  }

  /**
   * Gets the nsisaad cierreg.
   *
   * @return the nsisaad cierreg
   */
  public Boolean getNsisaadCierreg() {
    return nsisaadCierreg;
  }

  /**
   * Sets the nsisaad cierreg.
   *
   * @param nsisaadCierreg the new nsisaad cierreg
   */
  public void setNsisaadCierreg(final Boolean nsisaadCierreg) {
    this.nsisaadCierreg = nsisaadCierreg;
  }

  /**
   * Gets the nsisaad PIA.
   *
   * @return the nsisaad PIA
   */
  public Boolean getNsisaadPIA() {
    return nsisaadPIA;
  }

  /**
   * Sets the nsisaad PIA.
   *
   * @param nsisaadPIA the new nsisaad PIA
   */
  public void setNsisaadPIA(final Boolean nsisaadPIA) {
    this.nsisaadPIA = nsisaadPIA;
  }

  /**
   * Gets the nsisaad pendiente.
   *
   * @return the nsisaad pendiente
   */
  public Boolean getNsisaadPendiente() {
    return nsisaadPendiente;
  }

  /**
   * Sets the nsisaad pendiente.
   *
   * @param nsisaadPendiente the new nsisaad pendiente
   */
  public void setNsisaadPendiente(final Boolean nsisaadPendiente) {
    this.nsisaadPendiente = nsisaadPendiente;
  }

  /**
   * Gets the nsisaad comher.
   *
   * @return the nsisaad comher
   */
  public Boolean getNsisaadComher() {
    return nsisaadComher;
  }

  /**
   * Sets the nsisaad comher.
   *
   * @param nsisaadComher the new nsisaad comher
   */
  public void setNsisaadComher(final Boolean nsisaadComher) {
    this.nsisaadComher = nsisaadComher;
  }

  /**
   * Gets the reso exitus.
   *
   * @return the reso exitus
   */
  public Boolean getResoExitus() {
    return resoExitus;
  }

  /**
   * Sets the reso exitus.
   *
   * @param resoExitus the new reso exitus
   */
  public void setResoExitus(final Boolean resoExitus) {
    this.resoExitus = resoExitus;
  }

  /**
   * Gets the retro.
   *
   * @return the retro
   */
  public Boolean getRetro() {
    return retro;
  }

  /**
   * Sets the retro.
   *
   * @param retro the new retro
   */
  public void setRetro(final Boolean retro) {
    this.retro = retro;
  }

  /**
   * Gets the tramite.
   *
   * @return the tramite
   */
  public Long getTramite() {
    return tramite;
  }

  /**
   * Sets the tramite.
   *
   * @param tramite the new tramite
   */
  public void setTramite(final Long tramite) {
    this.tramite = tramite;
  }

  /**
   * Gets the reso minorada.
   *
   * @return the reso minorada
   */
  public Boolean getResoMinorada() {
    return resoMinorada;
  }

  /**
   * Sets the reso minorada.
   *
   * @param resoMinorada the new reso minorada
   */
  public void setResoMinorada(final Boolean resoMinorada) {
    this.resoMinorada = resoMinorada;
  }

  /**
   * Gets the es ADA.
   *
   * @return the es ADA
   */
  public Boolean getEsADA() {
    return esADA;
  }

  /**
   * Sets the es ADA.
   *
   * @param esADA the new es ADA
   */
  public void setEsADA(final Boolean esADA) {
    this.esADA = esADA;
  }

}
