package com.gval.gval.dto.dto.util;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;


/**
 * The Class PlSqlExceptionDTO.
 */
public class PlSqlExceptionDTO implements Serializable {



  /** The Constant RESULTADO_PL_CORRECTO. */
  public static final String SEPARADOR_PARAMETROS = "#";

  /** The Constant POSICICION_INICIAL_ORA. */
  public static final int POSICICION_INICIAL_ORA = 0;

  /** The Constant POSICICION_FINAL_ORA. */
  public static final int POSICICION_INICIAL_ORA_CODIGO_ERROR = 4;

  /** The Constant POSICICION_FINAL_ORA_CODIGO_ERROR. */
  public static final int POSICICION_FINAL_ORA_CODIGO_ERROR = 9;

  /** The Constant POSICICION_INICIO_DETALLE_CODIGO_ERROR. */
  public static final int POSICICION_INICIO_DETALLE_CODIGO_ERROR = 11;

  /** The Constant POSICICION_INICIAL_MENSAJES_PROPIOS. */
  public static final int POSICICION_INICIAL_MENSAJES_PROPIOS = 20000;

  /** The Constant POSICICION_FINAL_MENSAJES_PROPIOS. */
  public static final int POSICICION_FINAL_MENSAJES_PROPIOS = 20999;

  /** The Constant POSICICION_MENSAJE_MULTIIDIOMA. */
  public static final int POSICICION_MENSAJE_MULTIIDIOMA = 0;

  /** The Constant POSICICION_MENSAJE_MULTIIDIOMA_PARAMETROS_A_PARTIR. */
  public static final int POSICICION_MENSAJE_MULTIIDIOMA_PARAMETROS_A_PARTIR =
      1;
  /** The Constant DETALLE_I18. */
  public static final String DETALLE_I18 = "pl.procedure.";


  /** The Constant DEFAULT_ERROR_EXCEPTION. */
  public static final String DEFAULT_ERROR_EXCEPTION = "error";

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The i 18. */
  private String i18;

  /** The params. */
  private String[] params;

  /**
   * Instantiates a new resultado procedure DTO.
   */
  public PlSqlExceptionDTO() {
    // PlSqlExceptionDTO constructor
  }

  /**
   * Instantiates a new pl sql exception DTO.
   *
   * @param i18 the i 18
   * @param params the params
   */
  @SuppressWarnings("squid:S2384")
  public PlSqlExceptionDTO(final String i18, final String[] params) {
    this.i18 = i18;
    this.params = params;
  }

  /**
   * Gets the i18.
   *
   * @return the i18
   */
  public final String getI18() {
    return i18;
  }



  /**
   * Sets the i18.
   *
   * @param i18 the new i18
   */
  public final void setI18(final String i18) {
    this.i18 = i18;
  }



  /**
   * Gets the params.
   *
   * @return the params
   */
  @SuppressWarnings("squid:S2384")
  public final String[] getParams() {
    return params;
  }



  /**
   * Sets the params.
   *
   * @param params the new params
   */
  @SuppressWarnings("squid:S2384")
  public final void setParams(final String[] params) {
    this.params = params;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
