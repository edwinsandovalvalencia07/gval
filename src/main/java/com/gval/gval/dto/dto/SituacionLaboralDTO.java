package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;


/**
 * The Class SituacionLaboralDTO.
 */
public class SituacionLaboralDTO extends BaseDTO
    implements Comparable<SituacionLaboralDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -7430242216048102334L;

  /** The pk situacionlaboral. */
  private long pkSituacionlaboral;

  /** The descripcion. */
  private String descripcion;

  /**
   * Instantiates a new situacion laboral DTO.
   */
  public SituacionLaboralDTO() {
    super();
  }


  /**
   * Gets the pk situacionlaboral.
   *
   * @return the pk situacionlaboral
   */
  public long getPkSituacionlaboral() {
    return pkSituacionlaboral;
  }


  /**
   * Sets the pk situacionlaboral.
   *
   * @param pkSituacionlaboral the new pk situacionlaboral
   */
  public void setPkSituacionlaboral(final long pkSituacionlaboral) {
    this.pkSituacionlaboral = pkSituacionlaboral;
  }



  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }


  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }


  /**
   * Compare to.
   *
   * @param arg0 the arg 0
   * @return the int
   */
  @Override
  public int compareTo(final SituacionLaboralDTO arg0) {
    return 0;
  }

}
