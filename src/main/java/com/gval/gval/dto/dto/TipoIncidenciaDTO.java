package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.hibernate.validator.constraints.Length;

import java.util.Date;

import javax.validation.constraints.NotNull;

/**
 * The Class TipoIncidenciaDTO.
 */
public class TipoIncidenciaDTO extends BaseDTO
    implements Comparable<TipoIncidenciaDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 7735360026979489533L;

  /** The pk tipoinci. */
  @NotNull
  private Long pkTipoinci;

  /** The pk tipodocu. */
  private Long pkTipodocu;

  /** The autogenerado. */
  @NotNull
  private Boolean autogenerado;

  /** The bloquea. */
  private Boolean bloquea;

  /** The borrable. */
  private Boolean borrable;

  /** The fecha creacion. */
  @NotNull
  private Date fechaCreacion;

  /** The activo. */
  @NotNull
  private Boolean activo;

  /** The nombre. */
  @NotNull
  @Length(max = 150)
  private String nombre;

  /**
   * Instantiates a new tipo incidencia DTO.
   */
  public TipoIncidenciaDTO() {
    super();
  }

  /**
   * Instantiates a new tipo incidencia DTO.
   *
   * @param pkTipoinci the pk tipoinci
   * @param pkTipodocu the pk tipodocu
   * @param autogenerado the autogenerado
   * @param bloquea the bloquea
   * @param borrable the borrable
   * @param fechaCreacion the fecha creacion
   * @param activo the activo
   * @param nombre the nombre
   */
  public TipoIncidenciaDTO(final Long pkTipoinci, final Long pkTipodocu,
      final Boolean autogenerado, final Boolean bloquea, final Boolean borrable,
      final Date fechaCreacion, final Boolean activo, final String nombre) {
    super();
    this.pkTipoinci = pkTipoinci;
    this.pkTipodocu = pkTipodocu;
    this.autogenerado = autogenerado;
    this.bloquea = bloquea;
    this.borrable = borrable;
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
    this.activo = activo;
    this.nombre = nombre;
  }

  /**
   * Gets the pk tipoinci.
   *
   * @return the pk tipoinci
   */
  public Long getPkTipoinci() {
    return pkTipoinci;
  }

  /**
   * Gets the pk tipodocu.
   *
   * @return the pk tipodocu
   */
  public Long getPkTipodocu() {
    return pkTipodocu;
  }

  /**
   * Gets the autogenerado.
   *
   * @return the autogenerado
   */
  public Boolean getAutogenerado() {
    return autogenerado;
  }

  /**
   * Gets the bloquea.
   *
   * @return the bloquea
   */
  public Boolean getBloquea() {
    return bloquea;
  }

  /**
   * Gets the borrable.
   *
   * @return the borrable
   */
  public Boolean getBorrable() {
    return borrable;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the pk tipoinci.
   *
   * @param pkTipoinci the new pk tipoinci
   */
  public void setPkTipoinci(final Long pkTipoinci) {
    this.pkTipoinci = pkTipoinci;
  }

  /**
   * Sets the pk tipodocu.
   *
   * @param pkTipodocu the new pk tipodocu
   */
  public void setPkTipodocu(final Long pkTipodocu) {
    this.pkTipodocu = pkTipodocu;
  }

  /**
   * Sets the autogenerado.
   *
   * @param autogenerado the new autogenerado
   */
  public void setAutogenerado(final Boolean autogenerado) {
    this.autogenerado = autogenerado;
  }

  /**
   * Sets the bloquea.
   *
   * @param bloquea the new bloquea
   */
  public void setBloquea(final Boolean bloquea) {
    this.bloquea = bloquea;
  }

  /**
   * Sets the borrable.
   *
   * @param borrable the new borrable
   */
  public void setBorrable(final Boolean borrable) {
    this.borrable = borrable;
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final TipoIncidenciaDTO o) {
    return 0;
  }

}
