/**
 * Copyright (c) 2020 Generalitat Valenciana - Todos los derechos reservados.
 */
package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The Class PropuestaPiaDTO.
 */
public class PropuestaPiaViewDTO implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -6311477745168765194L;

  /** The pk solicitud. */
  private Long pkSolicitud;

  /** The pk persona solicitante. */
  private Long pkPersonaSolicitante;

  /** The pk usuario conectado. */
  private Long pkUsuarioConectado;

  /** The pk prop pia. */
  private Long pkPropPia;

  /** The pk pia. */
  private Long pkPia;

  /** The documentacion circuito verificacion. */
  private Boolean documentacionCircuitoVerificacion;

  /** The capacidad economica no confirmada. */
  private Boolean capacidadEconomicaNoConfirmada;

  /** The coincide preferencias. */
  private Boolean coincidePreferencias;

  /** The coincide valoracion TS. */
  private Boolean coincideValoracionTS;

  /** The horas cuidades personales. */
  private Integer horasCuidadosPersonales;

  /** The horas asistencia hogar. */
  private Integer horasAsistenciaHogar;

  /** The resolver pia. */
  private Boolean resolverPia;

  /** The fecha resolucion. */
  private Date fechaResolucion;

  /** The fecha efecto. */
  private Date fechaEfecto;

  /** The confirmar capa eco. */
  private Boolean confirmarCapaEco;

  /** The es excepcion. */
  private Boolean esExcepcion;
  /**
   * Instantiates a new propuesta pia DTO.
   */
  public PropuestaPiaViewDTO() {
    this.coincidePreferencias = Boolean.TRUE;
    this.coincideValoracionTS = Boolean.TRUE;
  }

  /**
   * Gets the pk solicitud.
   *
   * @return the pkSolicitud
   */
  public Long getPkSolicitud() {
    return pkSolicitud;
  }

  /**
   * Sets the pk solicitud.
   *
   * @param pkSolicitud the pkSolicitud to set
   */
  public void setPkSolicitud(final Long pkSolicitud) {
    this.pkSolicitud = pkSolicitud;
  }

  /**
   * Gets the pk persona solicitante.
   *
   * @return the pkPersonaSolicitante
   */
  public Long getPkPersonaSolicitante() {
    return pkPersonaSolicitante;
  }

  /**
   * Sets the pk persona solicitante.
   *
   * @param pkPersona the new pk persona solicitante
   */
  public void setPkPersonaSolicitante(final Long pkPersona) {
    this.pkPersonaSolicitante = pkPersona;
  }

  /**
   * Gets the pk usuario conectado.
   *
   * @return the pkUsuarioConectado
   */
  public Long getPkUsuarioConectado() {
    return pkUsuarioConectado;
  }

  /**
   * Sets the pk usuario conectado.
   *
   * @param pkUsuarioConectado the pkUsuarioConectado to set
   */
  public void setPkUsuarioConectado(final Long pkUsuarioConectado) {
    this.pkUsuarioConectado = pkUsuarioConectado;
  }

  /**
   * Gets the pk prop pia.
   *
   * @return the pkPropPia
   */
  public Long getPkPropPia() {
    return pkPropPia;
  }

  /**
   * Sets the pk prop pia.
   *
   * @param pkPropPia the pkPropPia to set
   */
  public void setPkPropPia(final Long pkPropPia) {
    this.pkPropPia = pkPropPia;
  }

  /**
   * Gets the documentacion circuito verificacion.
   *
   * @return the documentacionCircuitoVerificacion
   */
  public Boolean getDocumentacionCircuitoVerificacion() {
    return documentacionCircuitoVerificacion;
  }

  /**
   * Sets the documentacion circuito verificacion.
   *
   * @param documentacionCircuitoVerificacion the
   *        documentacionCircuitoVerificacion to set
   */
  public void setDocumentacionCircuitoVerificacion(
      final Boolean documentacionCircuitoVerificacion) {
    this.documentacionCircuitoVerificacion = documentacionCircuitoVerificacion;
  }

  /**
   * Gets the capacidad economica no confirmada.
   *
   * @return the capacidadEconomicaNoConfirmada
   */
  public Boolean getCapacidadEconomicaNoConfirmada() {
    return capacidadEconomicaNoConfirmada;
  }

  /**
   * Sets the capacidad economica no confirmada.
   *
   * @param capacidadEconomicaNoConfirmada the capacidadEconomicaNoConfirmada to
   *        set
   */
  public void setCapacidadEconomicaNoConfirmada(
      final Boolean capacidadEconomicaNoConfirmada) {
    this.capacidadEconomicaNoConfirmada = capacidadEconomicaNoConfirmada;
  }

  /**
   * Gets the coincide preferencias.
   *
   * @return the coincidePreferencias
   */
  public Boolean getCoincidePreferencias() {
    return coincidePreferencias;
  }

  /**
   * Sets the coincide preferencias.
   *
   * @param coincidePreferencias the coincidePreferencias to set
   */
  public void setCoincidePreferencias(final Boolean coincidePreferencias) {
    this.coincidePreferencias = coincidePreferencias;
  }

  /**
   * Gets the coincide valoracion TS.
   *
   * @return the coincideValoracionTS
   */
  public Boolean getCoincideValoracionTS() {
    return coincideValoracionTS;
  }

  /**
   * Sets the coincide valoracion TS.
   *
   * @param coincideValoracionTS the coincideValoracionTS to set
   */
  public void setCoincideValoracionTS(final Boolean coincideValoracionTS) {
    this.coincideValoracionTS = coincideValoracionTS;
  }

  /**
   * Gets the horas cuidades personales.
   *
   * @return the horasCuidadosPersonales
   */
  public Integer getHorasCuidadosPersonales() {
    return horasCuidadosPersonales;
  }

  /**
   * Sets the horas cuidades personales.
   *
   * @param horasCuidadesPersonales the new horas cuidados personales
   */
  public void setHorasCuidadosPersonales(
      final Integer horasCuidadesPersonales) {
    this.horasCuidadosPersonales = horasCuidadesPersonales;
  }

  /**
   * Gets the horas asistencia hogar.
   *
   * @return the horasAsistenciaHogar
   */
  public Integer getHorasAsistenciaHogar() {
    return horasAsistenciaHogar;
  }

  /**
   * Sets the horas asistencia hogar.
   *
   * @param horasAsistenciaHogar the horasAsistenciaHogar to set
   */
  public void setHorasAsistenciaHogar(final Integer horasAsistenciaHogar) {
    this.horasAsistenciaHogar = horasAsistenciaHogar;
  }

  /**
   * Gets the resolver pia.
   *
   * @return the resolverPia
   */
  public Boolean getResolverPia() {
    return resolverPia;
  }

  /**
   * Sets the resolver pia.
   *
   * @param resolverPia the resolverPia to set
   */
  public void setResolverPia(final Boolean resolverPia) {
    this.resolverPia = resolverPia;
  }


  /**
   * Gets the fecha resolucion.
   *
   * @return the fechaResolucion
   */
  public Date getFechaResolucion() {
    return UtilidadesCommons.cloneDate(fechaResolucion);
  }

  /**
   * Sets the fecha resolucion.
   *
   * @param fechaResolucion the fechaResolucion to set
   */
  public void setFechaResolucion(final Date fechaResolucion) {
    this.fechaResolucion = UtilidadesCommons.cloneDate(fechaResolucion);
  }

  /**
   * Gets the fecha efecto.
   *
   * @return the fechaEfecto
   */
  public Date getFechaEfecto() {
    return UtilidadesCommons.cloneDate(fechaEfecto);
  }

  /**
   * Sets the fecha efecto.
   *
   * @param fechaEfecto the fechaEfecto to set
   */
  public void setFechaEfecto(final Date fechaEfecto) {
    this.fechaEfecto = UtilidadesCommons.cloneDate(fechaEfecto);
  }

  /**
   * Sets the horas calculadas.
   *
   * @param horasCalculadas the new horas calculadas
   */
  public void setHorasCalculadas(final BigDecimal[] horasCalculadas) {
    this.setHorasCuidadosPersonales(horasCalculadas[0].intValue());
    this.setHorasAsistenciaHogar(horasCalculadas[1].intValue());
  }

  /**
   * Gets the pk pia.
   *
   * @return the pk pia
   */
  public Long getPkPia() {
    return pkPia;
  }

  /**
   * Sets the pk pia.
   *
   * @param pkPia the new pk pia
   */
  public void setPkPia(final Long pkPia) {
    this.pkPia = pkPia;
  }

  /**
   * Gets the confirmar capa eco.
   *
   * @return the confirmar capa eco
   */
  public Boolean getConfirmarCapaEco() {
    return confirmarCapaEco;
  }

  /**
   * Sets the confirmar capa eco.
   *
   * @param confirmarCapaEco the new confirmar capa eco
   */
  public void setConfirmarCapaEco(final Boolean confirmarCapaEco) {
    this.confirmarCapaEco = confirmarCapaEco;
  }


  /**
   * Gets the es excepcion.
   *
   * @return the es excepcion
   */
  public Boolean getEsExcepcion() {
    return esExcepcion;
  }

  /**
   * Sets the es excepcion.
   *
   * @param esExcepcion the new es excepcion
   */
  public void setEsExcepcion(Boolean esExcepcion) {
    this.esExcepcion = esExcepcion;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
