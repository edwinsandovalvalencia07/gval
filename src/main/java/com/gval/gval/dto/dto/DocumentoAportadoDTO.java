package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;
import es.gva.dependencia.ada.validators.NotNullIfAnotherFieldHasValue;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * The Class DocumentoAportadoDTO.
 */
@NotNullIfAnotherFieldHasValue(fieldName = "existePersona", fieldValue = "true",
    dependFieldName = "personaRelativa",
    message = "error.documentoaportado.persona_relativa")
public final class DocumentoAportadoDTO extends BaseDTO
    implements Comparable<DocumentoAportadoDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 4644349616510089017L;

  /** The pk documento. */
  private Long pkDocumento;

  /** The observaciones. */
  @Size(max = 250, message = "error.documentoAportado.observaciones.longitud")
  private String observaciones;

  /** The visto administrador. */
  @NotNull
  private Boolean vistoAdministrador;

  /** The documento. */
  @Valid
  private DocumentoDTO documento;

  /** The usuario. */
  // bi-directional many-to-one association to SdxUsuario
  private UsuarioDTO usuario;

  /** The existe persona. */
  private Boolean existePersona;

  /** The persona relativa. */
  private PersonaAdaDTO personaRelativa;


  /**
   * Instantiates a new documento aportado DTO.
   *
   * @param pkDocumento the pk documento
   * @param observaciones the observaciones
   * @param vistoAdministrador the visto administrador
   * @param documento the documento
   * @param usuario the usuario
   */
  public DocumentoAportadoDTO(final Long pkDocumento,
      final String observaciones, final Boolean vistoAdministrador,
      final DocumentoDTO documento, final UsuarioDTO usuario) {
    super();
    this.pkDocumento = pkDocumento;
    this.observaciones = observaciones;
    this.vistoAdministrador = vistoAdministrador;
    this.documento = documento;
    this.usuario = usuario;
  }



  /**
   * Instantiates a new documento aportado DTO.
   */
  public DocumentoAportadoDTO() {
    super();
    this.vistoAdministrador = Boolean.FALSE;
    this.documento = new DocumentoDTO();
  }

  /**
   * Gets the pk documento.
   *
   * @return the pkDocumento
   */
  public Long getPkDocumento() {
    return pkDocumento;
  }


  /**
   * Sets the pk documento.
   *
   * @param pkDocumento the pkDocumento to set
   */
  public void setPkDocumento(final Long pkDocumento) {
    this.pkDocumento = pkDocumento;
  }


  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return observaciones;
  }


  /**
   * Sets the observaciones.
   *
   * @param observaciones the observaciones to set
   */
  public void setObservaciones(final String observaciones) {
    this.observaciones = observaciones;
  }


  /**
   * Gets the visto administrador.
   *
   * @return the vistoAdministrador
   */
  public Boolean getVistoAdministrador() {
    return vistoAdministrador;
  }


  /**
   * Sets the visto administrador.
   *
   * @param vistoAdministrador the vistoAdministrador to set
   */
  public void setVistoAdministrador(final Boolean vistoAdministrador) {
    this.vistoAdministrador = vistoAdministrador;
  }


  /**
   * Gets the documento.
   *
   * @return the documento
   */
  public DocumentoDTO getDocumento() {
    return documento;
  }


  /**
   * Sets the documento.
   *
   * @param documento the documento to set
   */
  public void setDocumento(final DocumentoDTO documento) {
    this.documento = documento;
  }


  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }


  /**
   * Sets the usuario.
   *
   * @param usuario the usuario to set
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the existe persona.
   *
   * @return the existe persona
   */
  public Boolean getExistePersona() {
    return existePersona;
  }

  /**
   * Sets the existe persona.
   *
   * @param existePersona the new existe persona
   */
  public void setExistePersona(final Boolean existePersona) {
    this.existePersona = existePersona;
  }


  /**
   * Gets the persona relativa.
   *
   * @return the persona relativa
   */
  public PersonaAdaDTO getPersonaRelativa() {
    return personaRelativa;
  }


  /**
   * Sets the persona relativa.
   *
   * @param personaRelativa the new persona relativa
   */
  public void setPersonaRelativa(final PersonaAdaDTO personaRelativa) {
    this.personaRelativa = personaRelativa;
  }


  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final DocumentoAportadoDTO o) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
