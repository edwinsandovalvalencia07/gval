package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;

/**
 * The Class UsuarioDTO.
 */
public class UsuarioConectadoDTO extends BaseDTO
    implements Comparable<UsuarioConectadoDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -381253172596080932L;

  /** The pk persona. */
  private Long pkPersona;

  /** The estiloapp. */
  private Long estiloapp;

  /** The provincia. */
  private ProvinciaDTO provincia;

  /** The activo. */
  private Boolean activo;

  /** The administrador. */
  private Boolean administrador;

  /** The uscbs 1. */
  private Boolean uscbs1;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The firma. */
  private Boolean firma;

  /** The nombre. */
  private String nombre;

  /** The provincias. */
  private String provincias;

  /** The sector. */
  private String sector;

  /** The ultima conexion. */
  private Date ultimaConexion;

  /** The persona. */
  // bi-directional one-to-one association to SdmPersona
  private PersonaAdaDTO persona;

  /**
   * Instantiates a new usuario DTO.
   */
  public UsuarioConectadoDTO() {
    super();
  }


  /**
   * Instantiates a new usuario DTO.
   *
   * @param pkPersona the pk persona
   * @param estiloapp the estiloapp
   * @param provincia the provincia
   * @param activo the activo
   * @param administrador the administrador
   * @param uscbs1 the uscbs 1
   * @param fechaCreacion the fecha creacion
   * @param firma the firma
   * @param nombre the nombre
   * @param provincias the provincias
   * @param sector the sector
   * @param persona the persona
   */
  public UsuarioConectadoDTO(final Long pkPersona, final Long estiloapp,
      final ProvinciaDTO provincia, final Boolean activo,
      final Boolean administrador, final Boolean uscbs1,
      final Date fechaCreacion, final Boolean firma, final String nombre,
      final String provincias, final String sector,
      final PersonaAdaDTO persona) {
    super();
    this.pkPersona = pkPersona;
    this.estiloapp = estiloapp;
    this.provincia = provincia;
    this.activo = activo;
    this.administrador = administrador;
    this.uscbs1 = uscbs1;
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
    this.firma = firma;
    this.nombre = nombre;
    this.provincias = provincias;
    this.sector = sector;
    this.persona = persona;
  }

  /**
   * Gets the pk persona.
   *
   * @return the pkPersona
   */
  public Long getPkPersona() {
    return pkPersona;
  }

  /**
   * Sets the pk persona.
   *
   * @param pkPersona the pkPersona to set
   */
  public void setPkPersona(final Long pkPersona) {
    this.pkPersona = pkPersona;
  }

  /**
   * Gets the estiloapp.
   *
   * @return the estiloapp
   */
  public Long getEstiloapp() {
    return estiloapp;
  }

  /**
   * Sets the estiloapp.
   *
   * @param estiloapp the new estiloapp
   */
  public void setEstiloapp(final Long estiloapp) {
    this.estiloapp = estiloapp;
  }

  /**
   * Gets the provincia.
   *
   * @return the provincia
   */
  public ProvinciaDTO getProvincia() {
    return provincia;
  }

  /**
   * Sets the provincia.
   *
   * @param provincia the new provincia
   */
  public void setProvincia(final ProvinciaDTO provincia) {
    this.provincia = provincia;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the administrador.
   *
   * @return the administrador
   */
  public Boolean getAdministrador() {
    return administrador;
  }

  /**
   * Sets the administrador.
   *
   * @param administrador the new administrador
   */
  public void setAdministrador(final Boolean administrador) {
    this.administrador = administrador;
  }


  /**
   * Gets the uscbs 1.
   *
   * @return the uscbs1
   */
  public Boolean getUscbs1() {
    return uscbs1;
  }


  /**
   * Sets the uscbs 1.
   *
   * @param uscbs1 the uscbs1 to set
   */
  public void setUscbs1(final Boolean uscbs1) {
    this.uscbs1 = uscbs1;
  }


  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the firma.
   *
   * @return the firma
   */
  public Boolean getFirma() {
    return firma;
  }

  /**
   * Sets the firma.
   *
   * @param firma the new firma
   */
  public void setFirma(final Boolean firma) {
    this.firma = firma;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the provincias.
   *
   * @return the provincias
   */
  public String getProvincias() {
    return provincias;
  }

  /**
   * Sets the provincias.
   *
   * @param provincias the new provincias
   */
  public void setProvincias(final String provincias) {
    this.provincias = provincias;
  }

  /**
   * Gets the sector.
   *
   * @return the sector
   */
  public String getSector() {
    return sector;
  }

  /**
   * Sets the sector.
   *
   * @param sector the new sector
   */
  public void setSector(final String sector) {
    this.sector = sector;
  }

  /**
   * Gets the persona.
   *
   * @return the persona
   */
  public PersonaAdaDTO getPersona() {
    return persona;
  }

  /**
   * Sets the persona.
   *
   * @param persona the new persona
   */
  public void setPersona(final PersonaAdaDTO persona) {
    this.persona = persona;
  }


  /**
   * Gets the ultima conexion.
   *
   * @return the ultima conexion
   */
  public Date getUltimaConexion() {
    return UtilidadesCommons.cloneDate(ultimaConexion);
  }

  /**
   * Sets the ultima conexion.
   *
   * @param ultimaConexion the new ultima conexion
   */
  public void setUltimaConexion(final Date ultimaConexion) {
    this.ultimaConexion = UtilidadesCommons.cloneDate(ultimaConexion);
  }


  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final UsuarioConectadoDTO o) {
    final long thisPkPersona =
        this.getPkPersona() != null ? this.getPkPersona().longValue() : 0L;
    final long comparePkPersona = (o != null && o.getPkPersona() != null)
        ? this.getPkPersona().longValue()
        : 0L;
    if (thisPkPersona < comparePkPersona) {
      return -1;
    } else if (thisPkPersona == comparePkPersona) {
      return 0;
    } else {
      return 1;
    }
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
