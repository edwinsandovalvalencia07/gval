package com.gval.gval.dto.dto.util;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * The Class ButtonDTO.
 */
public class ButtonDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -5821466417162058879L;

  /** The label. */
  private String label;

  /** The command. */
  private String command;

  /** The visible. */
  private boolean visible;

  /** The disabled. */
  private boolean disabled;

  /** The auditable. */
  private boolean auditable;


  /**
   * Instantiates a new button DTO.
   */
  public ButtonDTO() {
    super();
  }


  /**
   * Instantiates a new button DTO.
   *
   * @param label the label
   * @param command the command
   * @param visible the visible
   * @param disabled the disabled
   */
  public ButtonDTO(final String label, final String command,
      final boolean visible, final boolean disabled) {
    super();
    this.label = label;
    this.command = command;
    this.visible = visible;
    this.disabled = disabled;
    this.auditable = false;
  }


  /**
   * Instantiates a new button DTO.
   *
   * @param label the label
   * @param command the command
   * @param visible the visible
   * @param disabled the disabled
   * @param auditable the auditable
   */
  public ButtonDTO(final String label, final String command,
      final boolean visible, final boolean disabled, final boolean auditable) {
    super();
    this.label = label;
    this.command = command;
    this.visible = visible;
    this.disabled = disabled;
    this.auditable = auditable;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * Sets the label.
   *
   * @param label the label to set
   */
  public void setLabel(final String label) {
    this.label = label;
  }

  /**
   * Gets the command.
   *
   * @return the command
   */
  public String getCommand() {
    return command;
  }

  /**
   * Sets the command.
   *
   * @param command the command to set
   */
  public void setCommand(final String command) {
    this.command = command;
  }

  /**
   * Checks if is visible.
   *
   * @return the visible
   */
  public boolean isVisible() {
    return visible;
  }

  /**
   * Sets the visible.
   *
   * @param visible the visible to set
   */
  public void setVisible(final boolean visible) {
    this.visible = visible;
  }

  /**
   * Checks if is disabled.
   *
   * @return the disabled
   */
  public boolean isDisabled() {
    return disabled;
  }

  /**
   * Sets the disabled.
   *
   * @param disabled the disabled to set
   */
  public void setDisabled(final boolean disabled) {
    this.disabled = disabled;
  }


  /**
   * Checks if is auditable.
   *
   * @return true, if is auditable
   */
  public boolean isAuditable() {
    return auditable;
  }


  /**
   * Sets the auditable.
   *
   * @param auditable the new auditable
   */
  public void setAuditable(final boolean auditable) {
    this.auditable = auditable;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
