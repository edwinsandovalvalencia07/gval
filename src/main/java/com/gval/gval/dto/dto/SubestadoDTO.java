package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;
import es.gva.dependencia.ada.vistas.dto.CentroDTO;

/**
 * The Class SubestadoDTO.
 */
public class SubestadoDTO extends BaseDTO implements Comparable<CentroDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 64132987432623432L;

  /** The pk subestado. */
  private Long pkSubestado;

  /** The activo. */
  private Boolean activo;

  /** The codigo. */
  private String codigo;

  /** The descripcion. */
  private String descripcion;

  /**
   * Instantiates a new subestado DTO.
   */
  public SubestadoDTO() {
    super();
  }

  /**
   * Instantiates a new subestado DTO.
   *
   * @param pkSubestado the pk subestado
   * @param activo the activo
   * @param codigo the codigo
   * @param descripcion the descripcion
   */
  public SubestadoDTO(final Long pkSubestado, final Boolean activo,
      final String codigo, final String descripcion) {
    super();
    this.pkSubestado = pkSubestado;
    this.activo = activo;
    this.codigo = codigo;
    this.descripcion = descripcion;
  }

  /**
   * Gets the pk subestado.
   *
   * @return the pk subestado
   */
  public Long getPkSubestado() {
    return pkSubestado;
  }

  /**
   * Sets the pk subestado.
   *
   * @param pkSubestado the new pk subestado
   */
  public void setPkSubestado(final Long pkSubestado) {
    this.pkSubestado = pkSubestado;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Compare to.
   *
   * @param arg0 the arg 0
   * @return the int
   */
  @Override
  public int compareTo(final CentroDTO arg0) {
    return 0;
  }
}
