package com.gval.gval.dto.dto;

import java.io.Serializable;

/**
 * The Class GrupoValoracionDTO.
 */
public class GrupoValoracionDTO implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 3016350926349197263L;

  /** The pk grupo valoracion. */
  private Long pkGrupoValoracion;

  /** The codigo. */
  private String codigo;

  /** The descripcion. */
  private String descripcion;

  /** The activo. */
  private Boolean activo;

  /**
   * Instantiates a new grupo valoracion DTO.
   */
  public GrupoValoracionDTO() {
    super();
  }

  /**
   * Gets the pk grupo valoracion.
   *
   * @return the pkGrupoValoracion
   */
  public Long getPkGrupoValoracion() {
    return pkGrupoValoracion;
  }

  /**
   * Sets the pk grupo valoracion.
   *
   * @param pkGrupoValoracion the pkGrupoValoracion to set
   */
  public void setPkGrupoValoracion(final Long pkGrupoValoracion) {
    this.pkGrupoValoracion = pkGrupoValoracion;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the codigo to set
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the descripcion to set
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }


}
