package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.sql.Timestamp;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * The Class TrazabilidadDto.
 */
public final class TrazabilidadDto extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -8767346275915879906L;

  /** The timestamp ejecucion. */
  @NotNull
  private Timestamp timestampEjecucion;

  /** The duracion ejecucion. */
  private Long duracionEjecucion;

  /** The id usuario. */
  @Size(max = 20)
  private String idUsuario;

  /** The objeto. */
  @Size(max = 1000)
  private String objeto;

  /** The accion. */
  @Size(max = 255)
  private String accion;

  /** The parametros. */
  @Size(max = 4000)
  private String parametros;

  /** The texto auxiliar. */
  @Size(max = 4000)
  private String textoAuxiliar;

  /**
   *
   */
  public TrazabilidadDto() {
    super();
  }

  /**
   * @param timestampEjecucion
   */
  public TrazabilidadDto(final Timestamp timestampEjecucion) {
    super();
    this.timestampEjecucion =
        UtilidadesCommons.cloneTimeStamp(timestampEjecucion);
  }

  /**
   * @return
   */
  public Timestamp getTimestampEjecucion() {
    return UtilidadesCommons.cloneTimeStamp(timestampEjecucion);
  }

  /**
   *
   * @param timestampEjecucion
   */
  public void setTimestampEjecucion(final Timestamp timestampEjecucion) {
    this.timestampEjecucion =
        UtilidadesCommons.cloneTimeStamp(timestampEjecucion);
  }

  /**
   *
   * @return
   */
  public Long getDuracionEjecucion() {
    return duracionEjecucion;
  }

  /**
   *
   * @param duracionEjecucion
   */
  public void setDuracionEjecucion(final Long duracionEjecucion) {
    this.duracionEjecucion = duracionEjecucion;
  }

  /**
   *
   * @return
   */
  public String getIdUsuario() {
    return idUsuario;
  }

  /**
   *
   * @param idUsuario
   */
  public void setIdUsuario(final String idUsuario) {
    this.idUsuario = idUsuario;
  }

  /**
   *
   * @return
   */
  public String getObjeto() {
    return objeto;
  }

  /**
   *
   * @param objeto
   */
  public void setObjeto(final String objeto) {
    this.objeto = objeto;
  }

  /**
   *
   * @return
   */
  public String getAccion() {
    return accion;
  }

  /**
   *
   * @param accion
   */
  public void setAccion(final String accion) {
    this.accion = accion;
  }

  /**
   *
   * @return
   */
  public String getParametros() {
    return parametros;
  }

  /**
   *
   * @param parametros
   */
  public void setParametros(final String parametros) {
    this.parametros = parametros;
  }

  /**
   *
   * @return
   */
  public String getTextoAuxiliar() {
    return textoAuxiliar;
  }

  /**
   *
   * @param textoAuxiliar
   */
  public void setTextoAuxiliar(final String textoAuxiliar) {
    this.textoAuxiliar = textoAuxiliar;
  }
}
