package com.gval.gval.dto.dto;


import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.common.enums.TipoOrigenEnum;
import com.gval.gval.dto.dto.util.BaseDTO;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;

import javax.validation.constraints.NotNull;


/**
 * The Class AsistenteInformeSocialDTO.
 */
public class AsistenteInformeSocialDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk asispers infosoci. */
  private Long pkAsispersInfosoci;

  /** The activo. */
  @NotNull
  private Boolean activo;

  /** The fecha creacion. */
  @NotNull
  private Date fechaCreacion;

  /** The asistente. */
  @NotNull
  private AsistentePersonalDTO asistente;

  /** The informe social. */
  @NotNull
  private InformeSocialDTO informeSocial;

  /** The origen. */
  private String origen;

  /** The cumple requisitos. */
  private Boolean cumpleRequisitos;

  /** The cumple PVI. */
  private Boolean cumplePVI;

  /**
   * Instantiates a new asistente informe social DTO.
   */
  public AsistenteInformeSocialDTO() {
    super();
    this.activo = Boolean.TRUE;
   // this.fechaCreacion = Utilidades.getFechaCorrectamente(new Date());
    this.asistente = new AsistentePersonalDTO();
    this.informeSocial = new InformeSocialDTO();
  }

  /**
   * Instantiates a new asistente informe social DTO.
   *
   * @param asistentePersonalDTO the asistente personal DTO
   * @param informeSocialDTO the informe social DTO
   * @param origen the origen
   */
  public AsistenteInformeSocialDTO(
      final AsistentePersonalDTO asistentePersonalDTO,
      final InformeSocialDTO informeSocialDTO, final TipoOrigenEnum origen) {
    this();
    this.asistente = asistentePersonalDTO;
    this.informeSocial = informeSocialDTO;
    this.origen = origen.getCodigo();
  }



  /**
   * Gets the pk asispers infosoci.
   *
   * @return the pk asispers infosoci
   */
  public Long getPkAsispersInfosoci() {
    return pkAsispersInfosoci;
  }

  /**
   * Sets the pk asispers infosoci.
   *
   * @param pkAsispersInfosoci the new pk asistente infosoci
   */
  public void setPkAsispersInfosoci(final Long pkAsispersInfosoci) {
    this.pkAsispersInfosoci = pkAsispersInfosoci;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the asistente.
   *
   * @return the asistente
   */
  public AsistentePersonalDTO getAsistente() {
    return asistente;
  }

  /**
   * Sets the asistente.
   *
   * @param asistente the new asistente
   */
  public void setAsistente(final AsistentePersonalDTO asistente) {
    this.asistente = asistente;
  }

  /**
   * Gets the informe social.
   *
   * @return the informe social
   */
  public InformeSocialDTO getInformeSocial() {
    return informeSocial;
  }

  /**
   * Sets the informe social.
   *
   * @param informeSocial the new informe social
   */
  public void setInformeSocial(final InformeSocialDTO informeSocial) {
    this.informeSocial = informeSocial;
  }

  /**
   * Gets the origen.
   *
   * @return the origen
   */
  public String getOrigen() {
    return origen;
  }

  /**
   * Sets the origen.
   *
   * @param origen the new origen
   */
  public void setOrigen(final String origen) {
    this.origen = origen;
  }

  /**
   * Tiene origen tipo.
   *
   * @param tipo the tipo
   * @return true, if successful
   */
  public boolean tieneOrigenTipo(final TipoOrigenEnum tipo) {
    return tipo.getCodigo().equals(origen);
  }

  /**
   * Gets the cumple requisitos.
   *
   * @return the cumple requisitos
   */
  public Boolean getCumpleRequisitos() {
    return cumpleRequisitos;
  }

  /**
   * Sets the cumple requisitos.
   *
   * @param cumpleRequisitos the new cumple requisitos
   */
  public void setCumpleRequisitos(final Boolean cumpleRequisitos) {
    this.cumpleRequisitos = cumpleRequisitos;
  }

  /**
   * Gets the cumple PVI.
   *
   * @return the cumple PVI
   */
  public Boolean getCumplePVI() {
    return cumplePVI;
  }

  /**
   * Sets the cumple PVI.
   *
   * @param cumplePVI the new cumple PVI
   */
  public void setCumplePVI(final Boolean cumplePVI) {
    this.cumplePVI = cumplePVI;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
