package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;

/**
 * The Class MunicipioDTO.
 */
public class MunicipioDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -7700670371812031492L;


  /** The pk municipio. */
  private Long pkMunicipio;

  /** The activo. */
  private Boolean activo;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The nombre. */
  private String nombre;

  /** The codigo municipio. */
  private Long codigoMunicipio;

  /** The codigo provincia. */
  private Long codigoProvincia;

  /** The provincia. */
  private ProvinciaDTO provincia;

  /** The zona cobertura. */
  private ZonaCoberturaDTO zonaCobertura;

  /** The comarca. */
  private ComarcaDTO comarca;

  /**
   * Instantiates a new municipio DTO.
   */
  public MunicipioDTO() {
    super();
  }

  /**
   * Instantiates a new municipio DTO.
   *
   * @param pkMunicipio the pk municipio
   * @param activo the activo
   * @param fechaCreacion the fecha creacion
   * @param nombre the nombre
   * @param codigoMunicipio the codigo municipio
   * @param codigoProvincia the codigo provincia
   * @param provincia the provincia
   * @param zonaCobertura the zona cobertura
   * @param comarca the comarca
   */
  public MunicipioDTO(final Long pkMunicipio, final Boolean activo,
      final Date fechaCreacion, final String nombre, final Long codigoMunicipio,
      final Long codigoProvincia, final ProvinciaDTO provincia,
      final ZonaCoberturaDTO zonaCobertura, final ComarcaDTO comarca) {
    super();
    this.pkMunicipio = pkMunicipio;
    this.activo = activo;
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
    this.nombre = nombre;
    this.codigoMunicipio = codigoMunicipio;
    this.codigoProvincia = codigoProvincia;
    this.provincia = provincia;
    this.zonaCobertura = zonaCobertura;
    this.comarca = comarca;
  }

  /**
   * Gets the pk municipio.
   *
   * @return the pk municipio
   */
  public Long getPkMunicipio() {
    return pkMunicipio;
  }

  /**
   * Sets the pk municipio.
   *
   * @param pkMunicipio the new pk municipio
   */
  public void setPkMunicipio(final Long pkMunicipio) {
    this.pkMunicipio = pkMunicipio;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the provincia.
   *
   * @return the provincia
   */
  public ProvinciaDTO getProvincia() {
    return provincia;
  }

  /**
   * Sets the provincia.
   *
   * @param provincia the new provincia
   */
  public void setProvincia(final ProvinciaDTO provincia) {
    this.provincia = provincia;
  }

  /**
   * Gets the zona cobertura.
   *
   * @return the zona cobertura
   */
  public ZonaCoberturaDTO getZonaCobertura() {
    return zonaCobertura;
  }

  /**
   * Sets the zona cobertura.
   *
   * @param zonaCobertura the new zona cobertura
   */
  public void setZonaCobertura(final ZonaCoberturaDTO zonaCobertura) {
    this.zonaCobertura = zonaCobertura;
  }

  /**
   * Gets the comarca.
   *
   * @return the comarca
   */
  public ComarcaDTO getComarca() {
    return comarca;
  }

  /**
   * Sets the comarca.
   *
   * @param comarca the new comarca
   */
  public void setComarca(final ComarcaDTO comarca) {
    this.comarca = comarca;
  }

  /**
   * Gets the codigo municipio.
   *
   * @return the codigo municipio
   */
  public Long getCodigoMunicipio() {
    return codigoMunicipio;
  }

  /**
   * Sets the codigo municipio.
   *
   * @param codigoMunicipio the new codigo municipio
   */
  public void setCodigoMunicipio(final Long codigoMunicipio) {
    this.codigoMunicipio = codigoMunicipio;
  }

  /**
   * Gets the codigo provincia.
   *
   * @return the codigo provincia
   */
  public Long getCodigoProvincia() {
    return codigoProvincia;
  }

  /**
   * Sets the codigo provincia.
   *
   * @param codigoProvincia the new codigo provincia
   */
  public void setCodigoProvincia(final Long codigoProvincia) {
    this.codigoProvincia = codigoProvincia;
  }


}
