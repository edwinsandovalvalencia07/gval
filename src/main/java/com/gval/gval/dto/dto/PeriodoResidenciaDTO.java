package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;
import es.gva.dependencia.ada.util.Utilidades;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.constraints.NotNull;



/**
 * The Class PeriodoResidenciaDTO.
 */
public class PeriodoResidenciaDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk periodo residencia. */
  private Long pkPeriodoResidencia;

  /** The fecha inicio. */
  @NotNull(message = "error.periodoResidencia.fechainicio.nulo")
  private Date fechaInicio;

  /** The fecha fin. */
  private Date fechaFin;

  /** The solicitud. */
  @NotNull(message = "error.periodoResidencia.solicitud.nulo")
  private SolicitudDTO solicitud;

  /** The poblacion. */
  @NotNull(message = "error.periodoResidencia.poblacion.nulo")
  private PoblacionDTO poblacion;

  /** The provincia. */
  @NotNull(message = "error.periodoResidencia.provincia.nulo")
  private ProvinciaDTO provincia;

  /**
   * Instantiates a new periodo residencia DTO.
   */
  public PeriodoResidenciaDTO() {
    super();
  }

  /**
   * Gets the pk periodo residencia.
   *
   * @return the pk periodo residencia
   */
  public Long getPkPeriodoResidencia() {
    return pkPeriodoResidencia;
  }

  /**
   * Sets the pk periodo residencia.
   *
   * @param pkPeriodoResidencia the new pk periodo residencia
   */
  public void setPkPeriodoResidencia(final Long pkPeriodoResidencia) {
    this.pkPeriodoResidencia = pkPeriodoResidencia;
  }

  /**
   * Gets the fecha fin.
   *
   * @return the fecha fin
   */
  public Date getFechaFin() {
    return UtilidadesCommons.cloneDate(fechaFin);
  }

  /**
   * Sets the fecha fin.
   *
   * @param fechaFin the new fecha fin
   */
  public void setFechaFin(final Date fechaFin) {
    this.fechaFin = UtilidadesCommons.cloneDate(fechaFin);
  }

  /**
   * Gets the fecha inicio.
   *
   * @return the fecha inicio
   */
  public Date getFechaInicio() {
    return UtilidadesCommons.cloneDate(fechaInicio);
  }

  /**
   * Sets the fecha inicio.
   *
   * @param fechaInicio the new fecha inicio
   */
  public void setFechaInicio(final Date fechaInicio) {
    this.fechaInicio = UtilidadesCommons.cloneDate(fechaInicio);
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public SolicitudDTO getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the new solicitud
   */
  public void setSolicitud(final SolicitudDTO solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the poblacion.
   *
   * @return the poblacion
   */
  public PoblacionDTO getPoblacion() {
    return poblacion;
  }

  /**
   * Sets the poblacion.
   *
   * @param poblacion the new poblacion
   */
  public void setPoblacion(final PoblacionDTO poblacion) {
    this.poblacion = poblacion;
  }

  /**
   * Gets the provincia.
   *
   * @return the provincia
   */
  public ProvinciaDTO getProvincia() {
    return provincia;
  }

  /**
   * Sets the provincia.
   *
   * @param provincia the new provincia
   */
  public void setProvincia(final ProvinciaDTO provincia) {
    this.provincia = provincia;
  }

  /**
   * Gets the periodo fechas.
   *
   * @return the periodo fechas
   */
  public String getPeriodoFechas() {

    final SimpleDateFormat simpleDateFormat =
        new SimpleDateFormat(Utilidades.FORMAT_DATE_FECHA);
    String date = simpleDateFormat.format(fechaInicio);


    final StringBuilder sb = new StringBuilder();
    sb.append(date);

    if (fechaFin != null) {
      sb.append(" - ");
      date = simpleDateFormat.format(fechaFin);
      sb.append(date);
    }
    return sb.toString();
  }

}
