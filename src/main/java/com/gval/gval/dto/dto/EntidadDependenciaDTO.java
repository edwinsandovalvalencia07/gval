package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * The Class EntidadDependenciaDTO.
 */
public class EntidadDependenciaDTO extends BaseDTO
    implements Comparable<EntidadDependenciaDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -744031548821147789L;

  /** The pk entidepe. */
  @NotNull
  private Long pkEntidepe;

  /** The contacto. */
  @Length(max = 250)
  private String contacto;

  /** The departamento. */
  @Length(max = 250)
  private String departamento;

  /** The nombre entidad. */
  @Length(max = 250)
  private String nombreEntidad;

  /** The servicio. */
  @Length(max = 250)
  private String servicio;

  /** The direccion. */
  @NotNull
  private DireccionAdaDTO direccion;

  /** The comunidad autonoma. */
  @NotNull
  private ComunidadAutonomaDTO comunidadAutonoma;

  /**
   * Instantiates a new entidad dependencia DTO.
   */
  public EntidadDependenciaDTO() {
    super();
  }


  /**
   * Instantiates a new entidad dependencia DTO.
   *
   * @param pkEntidepe the pk entidepe
   * @param contacto the contacto
   * @param departamento the departamento
   * @param nombreEntidad the nombre entidad
   * @param servicio the servicio
   * @param direccion the direccion
   * @param comunidadAutonoma the comunidad autonoma
   */
  public EntidadDependenciaDTO(final Long pkEntidepe, final String contacto,
      final String departamento, final String nombreEntidad,
      final String servicio, final DireccionAdaDTO direccion,
      final ComunidadAutonomaDTO comunidadAutonoma) {
    super();
    this.pkEntidepe = pkEntidepe;
    this.contacto = contacto;
    this.departamento = departamento;
    this.nombreEntidad = nombreEntidad;
    this.servicio = servicio;
    this.direccion = direccion;
    this.comunidadAutonoma = comunidadAutonoma;
  }

  /**
   * Gets the pk entidepe.
   *
   * @return the pk entidepe
   */
  public Long getPkEntidepe() {
    return pkEntidepe;
  }

  /**
   * Sets the pk entidepe.
   *
   * @param pkEntidepe the new pk entidepe
   */
  public void setPkEntidepe(final Long pkEntidepe) {
    this.pkEntidepe = pkEntidepe;
  }

  /**
   * Gets the direccion.
   *
   * @return the direccion
   */
  public DireccionAdaDTO getDireccion() {
    return direccion;
  }

  /**
   * Sets the direccion.
   *
   * @param direccion the new direccion
   */
  public void setDireccion(final DireccionAdaDTO direccion) {
    this.direccion = direccion;
  }

  /**
   * Gets the comunidad autonoma.
   *
   * @return the comunidad autonoma
   */
  public ComunidadAutonomaDTO getComunidadAutonoma() {
    return comunidadAutonoma;
  }

  /**
   * Sets the comunidad autonoma.
   *
   * @param comunidadAutonoma the new comunidad autonoma
   */
  public void setComunidadAutonoma(
      final ComunidadAutonomaDTO comunidadAutonoma) {
    this.comunidadAutonoma = comunidadAutonoma;
  }

  /**
   * Gets the contacto.
   *
   * @return the contacto
   */
  public String getContacto() {
    return contacto;
  }

  /**
   * Sets the contacto.
   *
   * @param contacto the new contacto
   */
  public void setContacto(final String contacto) {
    this.contacto = contacto;
  }

  /**
   * Gets the departamento.
   *
   * @return the departamento
   */
  public String getDepartamento() {
    return departamento;
  }

  /**
   * Sets the departamento.
   *
   * @param departamento the new departamento
   */
  public void setDepartamento(final String departamento) {
    this.departamento = departamento;
  }

  /**
   * Gets the nombre entidad.
   *
   * @return the nombre entidad
   */
  public String getNombreEntidad() {
    return nombreEntidad;
  }

  /**
   * Sets the nombre entidad.
   *
   * @param nombreEntidad the new nombre entidad
   */
  public void setNombreEntidad(final String nombreEntidad) {
    this.nombreEntidad = nombreEntidad;
  }

  /**
   * Gets the servicio.
   *
   * @return the servicio
   */
  public String getServicio() {
    return servicio;
  }

  /**
   * Sets the servicio.
   *
   * @param servicio the new servicio
   */
  public void setServicio(final String servicio) {
    this.servicio = servicio;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final EntidadDependenciaDTO o) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
