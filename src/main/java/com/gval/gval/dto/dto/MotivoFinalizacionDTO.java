package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;


/**
 * The Class MotivoFinalizacionDTO.
 */
public class MotivoFinalizacionDTO extends BaseDTO {


  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -3712351089984654695L;

  /** The pk motivo finalizacion. */
  private Long pkMotivoFinalizacion;

  /** The codigo. */
  private String codigo;

  /** The motivo. */
  private String motivo;


  /**
   * Instantiates a new motivo finalizacion DTO.
   *
   * @param pkMotivoFinalizacion the pk motivo finalizacion
   * @param codigo the codigo
   * @param motivo the motivo
   */
  public MotivoFinalizacionDTO() {
    super();
  }

  /**
   * Gets the pk motivo finalizacion.
   *
   * @return the pk motivo finalizacion
   */
  public Long getPkMotivoFinalizacion() {
    return pkMotivoFinalizacion;
  }

  /**
   * Sets the pk motivo finalizacion.
   *
   * @param pkMotivoFinalizacion the new pk motivo finalizacion
   */
  public void setPkMotivoFinalizacion(final Long pkMotivoFinalizacion) {
    this.pkMotivoFinalizacion = pkMotivoFinalizacion;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the motivo.
   *
   * @return the motivo
   */
  public String getMotivo() {
    return motivo;
  }

  /**
   * Sets the motivo.
   *
   * @param motivo the new motivo
   */
  public void setMotivo(final String motivo) {
    this.motivo = motivo;
  }

}
