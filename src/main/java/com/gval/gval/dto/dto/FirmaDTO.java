package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;


/**
 * The Class FirmaDTO.
 */
public class FirmaDTO extends BaseDTO
    implements Serializable, Comparable<FirmaDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk firma. */
  private Long pkFirma;

  /** The activo. */
  private Boolean activo;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The fecha desde. */
  private Date fechaDesde;

  /** The fecha hasta. */
  private Date fechaHasta;

  /** The nombre. */
  private String nombre;

  /** The firmante. */
  private UsuarioDTO firmante;



  /**
   * Gets the pk firma.
   *
   * @return the pk firma
   */
  public Long getPkFirma() {
    return pkFirma;
  }


  /**
   * Sets the pk firma.
   *
   * @param pkFirma the new pk firma
   */
  public void setPkFirma(final Long pkFirma) {
    this.pkFirma = pkFirma;
  }


  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }


  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }


  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }


  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }


  /**
   * Gets the fecha desde.
   *
   * @return the fecha desde
   */
  public Date getFechaDesde() {
    return UtilidadesCommons.cloneDate(fechaDesde);
  }


  /**
   * Sets the fecha desde.
   *
   * @param fechaDesde the new fecha desde
   */
  public void setFechaDesde(final Date fechaDesde) {
    this.fechaDesde = UtilidadesCommons.cloneDate(fechaDesde);
  }


  /**
   * Gets the fecha hasta.
   *
   * @return the fecha hasta
   */
  public Date getFechaHasta() {
    return UtilidadesCommons.cloneDate(fechaHasta);
  }


  /**
   * Sets the fecha hasta.
   *
   * @param fechaHasta the new fecha hasta
   */
  public void setFechaHasta(final Date fechaHasta) {
    this.fechaHasta = UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }


  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }



  /**
   * Gets the firmante.
   *
   * @return the firmante
   */
  public UsuarioDTO getFirmante() {
    return firmante;
  }


  /**
   * Sets the firmante.
   *
   * @param firmante the new firmante
   */
  public void setFirmante(final UsuarioDTO firmante) {
    this.firmante = firmante;
  }


  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final FirmaDTO o) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
