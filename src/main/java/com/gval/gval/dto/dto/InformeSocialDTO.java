package com.gval.gval.dto.dto;

import es.gva.dependencia.ada.common.ConstantesWeb;
import com.gval.gval.common.UtilidadesCommons;
import es.gva.dependencia.ada.common.enums.CatalogoServicioEnum;
import es.gva.dependencia.ada.common.enums.EstadoInformeSocialEnum;
import es.gva.dependencia.ada.common.enums.MotivoGeneracionEnum;
import es.gva.dependencia.ada.common.enums.TipoInformeSocialEnum;
import es.gva.dependencia.ada.common.enums.TipoOrigenEnum;
import es.gva.dependencia.ada.common.enums.TipoPreferenciaInformeSocialEnum;
import com.gval.gval.dto.dto.util.BaseDTO;
import es.gva.dependencia.ada.vistas.dto.CentroDTO;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.zkoss.util.resource.Labels;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;



/**
 * The Class InformeSocialDTO.
 */
public class InformeSocialDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -2925084329754478452L;

  /** The pk informe social. */
  private Long pkInformeSocial;

  /** The direccion ada. */
  private DireccionAdaDTO direccionAda;

  /** The solicitud. */
  private SolicitudDTO solicitud;

  /** The usuario empieza. */
  @NotNull
  private UsuarioDTO usuarioCreador;

  /** The usuario asignado. */
  private UsuarioDTO usuarioAsignado;

  /** The documento generado. */
  private DocumentoGeneradoDTO documentoGenerado;

  /** The estado peticion. */
  @NotNull
  @Size(max = 1)
  private String estadoPeticion;

  /** The fecha peticion. */
  @NotNull
  private Date fechaPeticion;

  /** The motivo peticion. */
  @Size(max = 4000)
  private String motivoPeticion;

  /** The fecha inicio. */
  private Date fechaGrabacion;

  /** The fecha inicio. */
  private Date fechaInicio;

  /** The fecha fin. */
  private Date fechaFin;

  /** The es ultimo informe. */
  @NotNull
  private Boolean esUltimoInforme;

  /** The persona. */
  private PersonaAdaDTO solicitante;

  /** The tiempo residencia. */
  private Long tiempoResidencia;

  /** The situacion capacidad. */
  private String situacionCapacidad;

  /** The tiene edad escolar. */
  private Boolean tieneEdadEscolar;

  /** The centro estudios. */
  @Size(max = 250)
  private String centroEstudios;

  /** The tipo plaza residencia. */
  @Size(max = 250)
  private String tipoPlazaResidencia;

  /** The centro aportacion usuario. */
  private BigDecimal centroAportacionUsuario;

  /** The tipo centro especializado. */
  @Size(max = 250)
  private String tipoCentroEspecializado;

  /** The aportacion usuario en centro especializado. */
  private BigDecimal aportacionUsuarioCentroEspecializado;

  /** The apoyo domicilio horas semana. */
  @NotNull
  private Long apoyoDomicilioHorasSemana;

  /** The opinion persona situacion centro. */
  @Size(max = 1)
  private String opinionPersonaSituacionCentro;

  /** The opinion familia cren. */
  @Size(max = 1)
  private String opinionFamiliaCren;

  /** The valoracion tecnica. */
  @Size(max = 4000)
  private String valoracionTecnica;

  /** The tipo convivencia. */
  @Size(max = 1)
  private String tipoConvivencia;

  /** The apoyo familiar diario. */
  private Boolean apoyoFamiliarDiario;

  /** The apoyo familiar esporadico. */
  private Boolean apoyoFamiliarEsporadico;

  /** The apoyo social-comunitario diario. */
  private Boolean apoyoSocialDiario;

  /** The apoyo social-comunitario esporadico. */
  private Boolean apoyoSocialEsporadico;

  /** The sin apoyo. */
  private Boolean sinApoyo;

  /** The sin apoyo social-comunitario. */
  private Boolean sinApoyoSocial;

  /** The nivel relacion. */
  @Size(max = 1)
  private String nivelRelacion;

  /** The mayor edad. */
  private Boolean mayorEdad;

  /** The cuidador predependiente O dependiencia acusada. */
  private Boolean cuidadorPredependiente;

  /** The concurrencia enfermedad. */
  private Boolean concurrenciaEnfermedad;

  /** The seguridad economica. */
  private Boolean seguridadEconomica;

  /** The conocimientos suficientes. */
  private Boolean conocimientosSuficientes;

  /** The conocimientos escasos. */
  private Boolean conocimientosEscasos;

  /** The dificultad comprension enfermedad. */
  private Boolean dificultadComprensionEnfermedad;

  /** The disponibilidad apoyos profesionales. */
  private Boolean disponibilidadApoyosProfesionales;

  /** The disponibilidad tiempo. */
  private Boolean disponibilidadTiempo;

  /** The periodos descanso. */
  private Boolean periodosDescanso;

  /** The signos agotamiento. */
  private Boolean signosAgotamiento;

  /** The sin signos fragilidad. */
  private Boolean sinSignosFragilidad;

  /** The ausencia compromiso estable. */
  private Boolean ausenciaCompromiso;

  /** The dificultados conexion. */
  private Boolean dificultadosConexion;

  /** The riesgo claudicacion. */
  @Size(max = 1)
  private String riesgoClaudicacion;

  /** The propiedad vivienda habitual. */
  @Size(max = 1)
  private String propiedadViviendaHabitual;

  /** The condiciones vivienda. */
  @Size(max = 1)
  private String condicionesVivienda;

  /** The adaptacion hogar. */
  @Size(max = 1000)
  private String adaptacionHogar;

  /** The facilita acceso. */
  private Long facilitaAcceso;

  /** The valoracion social. */
  @Size(max = 1000)
  private String valoracionSocial;

  /** The coinciden valores preferencias. */
  private Boolean coincidenValoresPreferencias;

  /** The motivos priorizacion profesional. */
  @Size(max = 1000)
  private String motivosPriorizacionProfesional;

  /** The identificador trabajador social. */
  @Size(max = 250)
  private String identificadorTrabajadorSocial;

  /** The numero colegiado. */
  @Size(max = 250)
  private String numeroColegiado;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The nivel cultural. */
  private Long nivelCultural;

  /** The activo. */
  @NotNull
  private Boolean activo;

  /** The tipo centro. */
  private Long tipoCentro;

  /** The tiene edad laboral. */
  private Boolean tieneEdadLaboral;

  /** The activo laboralmente. */
  private Boolean activoLaboralmente;

  /** The situacion inactividad. */
  private Long situacionInactividad;

  /** The tipo jornada. */
  private Long tipoJornada;

  /** The profesion solicitante. */
  @Size(max = 250)
  private String profesionSolicitante;

  /** The tiene centro residencial. */
  private Boolean tieneCentroResidencial;

  /** The centro atencion residencial. */
  private CentroDTO centroAtencionResidencial;

  /** The tiene centro atencion diurna. */
  private Boolean tieneCentroAtencionDiurna;

  /** The centro atencion diurna. */
  private CentroDTO centroAtencionDiurna;

  /** The soporte social Y familiar. */
  private Long soporteSocialYFamiliar;

  /** The ayuda movilidad baston muleta. */
  private Boolean ayudaMovilidadBastonMuleta;

  /** The ayuda movilidad silla ruedas. */
  private Boolean ayudaMovilidadSillaRuedas;

  /** The ayuda movilidad grua transferencia. */
  private Boolean ayudaMovilidadGruaTransferencia;

  /** The ayuda movilidad otros. */
  private Boolean ayudaMovilidadOtros;

  /** The otras ayudas movilidad. */
  @Size(max = 1000)
  private String otrasAyudasMovilidad;

  /** The ayuda aseo personal. */
  private Boolean ayudaAseoPersonal;

  /** The ayuda aseo banio Y ducha. */
  private Boolean ayudaAseoBanioYDucha;

  /** The ayuda aseo otros. */
  private Boolean ayudaAseoOtros;

  /** The otras ayudas aseo. */
  @Size(max = 1000)
  private String otrasAyudasAseo;

  /** The ayuda alimentacion para preparar comida. */
  private Boolean ayudaAlimentacionParaPrepararComida;

  /** The ayuda alimentacion para comer Y beber. */
  private Boolean ayudaAlimentacionParaComerYBeber;

  /** The ayuda alimentacion otros. */
  private Boolean ayudaAlimentacionOtros;

  /** The otras ayudas alimentacion. */
  @Size(max = 1000)
  private String otrasAyudasAlimentacion;

  /** The ayudas tecnicas tic. */
  @Size(max = 1000)
  private String ayudasTecnicasTic;

  /** The ancho puertas. */
  private Boolean anchoPuertas;

  /** The escaleras interiores. */
  private Boolean escalerasInteriores;

  /** The adecuacion sanitarios. */
  private Boolean adecuacionSanitarios;

  /** The pasamanos. */
  private Boolean pasamanos;

  /** The adecuacion cocina. */
  private Boolean adecuacionCocina;

  /** The instalacion gas luz. */
  private Boolean instalacionGasLuz;

  /** The suelo antideslizante. */
  private Boolean sueloAntideslizante;

  /** The otros dentro vivienda. */
  private Boolean otrosDentroVivienda;

  /** The rampas. */
  private Boolean rampas;

  /** The ascensor. */
  private Boolean ascensor;

  /** The silla salvaescalera. */
  private Boolean sillaSalvaescalera;

  /** The otrosAccesoHogar. */
  private Boolean otrosAccesoHogar;

  /** The otros entorno. */
  @Size(max = 1000)
  private String otrosEntorno;

  /** The dificultades servicio. */
  @Size(max = 4000)
  private String dificultadesServicio;

  /** The info cuidador. */
  @Size(max = 4000)
  private String infoCuidador;

  /** The info asistente. */
  @Size(max = 4000)
  private String infoAsistente;

  /** The pedido. */
  private Long pedido;

  /** The tipo informe social. */
  private Long tipoInformeSocial;

  /** The situacion capacidad laboral. */
  private Long situacionCapacidadLaboral;

  /** The modalidad. */
  @Size(max = 1)
  private String modalidad;

  /** The intensidad asistencia. */
  @Size(max = 1)
  private String intensidadAsistencia;

  /** The fecha cuidador. */
  private Date fechaCuidador;

  /** The motivoPersonal. */
  private Boolean motivoPersonal;

  /** The preferencias revisadas. */
  @NotNull
  private Boolean preferenciasRevisadas;

  /** The cambio revisado. */
  @NotNull
  private Boolean cambioRevisado;

  /** The coincide cuidador. */
  private Boolean coincideCuidador;

  /** The documento nuevas preferencias. */
  private Long pkDocumentoPreferencias;

  /** The pk pia. */
  private Long pkPia;

  /** The app origen. */
  private String appOrigen;

  /** The recurso idoneo. */
  private Boolean recursoIdoneo;

  /** The motivo no idoneidad. */
  private String motivoNoIdoneidad;

  /** The nombre centro antecedente social. */
  // Este campo no se mapea con la entidad solo va al dto
  private String nombreCentroAntecedenteSocial;

  /** The motivos generacion informe social. */
  private List<MotivoGeneracionInformeSocialDTO> motivosGeneracionInformeSocial;

  /** The preferencia informe social. */
  private List<PreferenciaInformeSocialDTO> preferenciasInformeSocial;

  /** The cuidadores informe social. */
  private List<CuidadorInformeSocialDTO> cuidadoresInformeSocial;

  /** The asistentes informe social. */
  private List<AsistenteInformeSocialDTO> asistentesInformeSocial;

  /** The documentos informe social. */
  private List<DocumentoInformeSocialDTO> documentosInformeSocial;

  /**
   * Puede que los cuidadores de preferencias hayan sido guardados desde la
   * pantalla de cambio cuidador. Si han guardado el informe con el coincide
   * valoración 'No', estos cuidadores deberían borrarse.
   */
  private boolean borrarCuidadoresPreferenciasAlGuardar = false;


  /**
   * The nueva version informes sociales.
   *
   * 3 si el informe social tiene el nuevo formato.
   */
  @NotNull
  private Long version;

  /** The Anamnesis. */
  @Size(max = 1000)
  private String anamnesis;

  /** The riesgo claudica. */
  private Boolean riesgoClaudica;

  /** The abandono. */
  private Boolean abandono;

  /** The tiene vivienda. */
  private Boolean tieneVivienda;

  /** The conservacion. */
  private Boolean conservacion;

  /** The habitabilidad. */
  private Boolean habitabilidad;

  /** The salubridad. */
  private Boolean salubridad;

  /** The adaptaciones. */
  @Size(max = 1000)
  private String adaptacionHogarActual;

  /** The situacion socio familiar. */
  @Size(max = 4000)
  private String situacionSocioFamiliar;

  /** The centro trabajador social. */
  @Size(max = 250)
  @NotNull(message = "error.informesocial.notnull.centro-trabajador-social")
  private String centroTrabajadorSocial;

  // IS nuevas preferencias AP:

  /** The ayuda comunicacion. */
  private Boolean ayudaComunicacion;

  /** The ayuda desplazamiento. */
  private Boolean ayudaDesplazamiento;

  /** The ayuda acceso recursos. */
  private Boolean ayudaAccesoRecursos;

  /** The realiza formacion. */
  private Boolean realizaFormacion;

  /** The nombre formacion. */
  @Size(max = 250)
  private String nombreFormacion;

  /** The formacion ayuda AP. */
  private Boolean formacionAyudaAP;

  /** The trabajo ayuda AP. */
  private Boolean trabajoAyudaAP;

  /** The realiza actividad. */
  private Boolean realizaActividad;

  /** The tipo actividad. */
  @Size(max = 250)
  private String tipoActividad;

  /** The nombre entidad actividades. */
  @Size(max = 250)
  private String nombreEntidadActividad;

  /** The actividad ayuda AP. */
  private Boolean actividadAyudaAP;

  /** The proyecto adecuado. */
  private Boolean proyectoAdecuado;

  /** The otro recurso. */
  private Boolean otroRecurso;

  /** The otro recurso seleccionado. */
  private CatalogoServicioDTO otroRecursoSeleccionado;

  /** The otro recurso motivos. */
  @Size(max = 1000)
  private String otroRecursoMotivos;

  /** The tipo persona AP fisica. */
  private Boolean tipoPersonaAPFisica;

  /** The tipo persona AP juridica. */
  private Boolean tipoPersonaAPJuridica;

  /** The horario lectivo AP. */
  private Boolean horarioLectivoAP;

  /** The mantener primera pref. */
  private Boolean mantenerPreferencia1;

  /** The mantener segunda pref. */
  private Boolean mantenerPreferencia2;

  /** The mantener teleasistencia. */
  private Boolean mantenerTeleasistencia;

  /**
   * The tipo asispers. Tipo de persona de asistencia personal: PATI - menores
   * edad (0) / PAP - mayores edad (1)
   */
  private Boolean tipoAsispers;

  /** The aumento horas. */
  private Boolean aumentoHoras;

  /**
   * Instantiates a new informe social DTO.
   */
  public InformeSocialDTO() {
    super();
    this.usuarioCreador = new UsuarioDTO();
    this.estadoPeticion = EstadoInformeSocialEnum.PENDIENTE.getValor();
    this.fechaPeticion = new Date();
    this.fechaInicio = new Date();
    this.esUltimoInforme = Boolean.TRUE;
    this.apoyoDomicilioHorasSemana = 0L;
    this.activo = Boolean.TRUE;
    this.preferenciasRevisadas = Boolean.TRUE;
    this.cambioRevisado = Boolean.TRUE;
    this.version = ConstantesWeb.DEFAULT_VERSION_INFSOCIAL;
  }

  /**
   * Gets the pk informe social.
   *
   * @return the pkInformeSocial
   */
  public Long getPkInformeSocial() {
    return pkInformeSocial;
  }

  /**
   * Sets the pk informe social.
   *
   * @param pkInformeSocial the pkInformeSocial to set
   */
  public void setPkInformeSocial(final Long pkInformeSocial) {
    this.pkInformeSocial = pkInformeSocial;
  }


  /**
   * Gets the direccion ada.
   *
   * @return the direccionAda
   */
  public DireccionAdaDTO getDireccionAda() {
    return direccionAda;
  }

  /**
   * Sets the direccion ada.
   *
   * @param direccionAda the direccionAda to set
   */
  public void setDireccionAda(final DireccionAdaDTO direccionAda) {
    this.direccionAda = direccionAda;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public SolicitudDTO getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the solicitud to set
   */
  public void setSolicitud(final SolicitudDTO solicitud) {
    this.solicitud = solicitud;
  }



  /**
   * Gets the usuario creador.
   *
   * @return the usuario creador
   */
  public UsuarioDTO getUsuarioCreador() {
    return usuarioCreador;
  }

  /**
   * Sets the usuario creador.
   *
   * @param usuarioCreador the new usuario creador
   */
  public void setUsuarioCreador(final UsuarioDTO usuarioCreador) {
    this.usuarioCreador = usuarioCreador;
  }

  /**
   * Gets the usuario asignado.
   *
   * @return the usuario asignado
   */
  public UsuarioDTO getUsuarioAsignado() {
    return usuarioAsignado;
  }

  /**
   * Sets the usuario asignado.
   *
   * @param usuarioAsignado the new usuario asignado
   */
  public void setUsuarioAsignado(final UsuarioDTO usuarioAsignado) {
    this.usuarioAsignado = usuarioAsignado;
  }

  /**
   * Gets the documento generado.
   *
   * @return the documentoGenerado
   */
  public DocumentoGeneradoDTO getDocumentoGenerado() {
    return documentoGenerado;
  }

  /**
   * Sets the documento generado.
   *
   * @param documentoGenerado the documentoGenerado to set
   */
  public void setDocumentoGenerado(
      final DocumentoGeneradoDTO documentoGenerado) {
    this.documentoGenerado = documentoGenerado;
  }

  /**
   * Gets the estado peticion.
   *
   * @return the estadoPeticion
   */
  public String getEstadoPeticion() {
    return estadoPeticion;
  }

  /**
   * Sets the estado peticion.
   *
   * @param estadoPeticion the estadoPeticion to set
   */
  public void setEstadoPeticion(final String estadoPeticion) {
    this.estadoPeticion = estadoPeticion;
  }

  /**
   * Gets the estado peticion label.
   *
   * @return the estado peticion label
   */
  public String getEstadoPeticionLabel() {
    return Labels
        .getLabel(EstadoInformeSocialEnum.labelFromValor(estadoPeticion));
  }


  /**
   * Gets the fecha peticion.
   *
   * @return the fechaPeticion
   */
  public Date getFechaPeticion() {
    return UtilidadesCommons.cloneDate(fechaPeticion);
  }

  /**
   * Sets the fecha peticion.
   *
   * @param fechaPeticion the fechaPeticion to set
   */
  public void setFechaPeticion(final Date fechaPeticion) {
    this.fechaPeticion = UtilidadesCommons.cloneDate(fechaPeticion);
  }

  /**
   * Gets the motivo peticion.
   *
   * @return the motivoPeticion
   */
  public String getMotivoPeticion() {
    return motivoPeticion;
  }

  /**
   * Sets the motivo peticion.
   *
   * @param motivoPeticion the motivoPeticion to set
   */
  public void setMotivoPeticion(final String motivoPeticion) {
    this.motivoPeticion = motivoPeticion;
  }

  /**
   * Gets the fecha inicio.
   *
   * @return the fechaInicio
   */
  public Date getFechaInicio() {
    return UtilidadesCommons.cloneDate(fechaInicio);
  }

  /**
   * Sets the fecha inicio.
   *
   * @param fechaInicio the fechaInicio to set
   */
  public void setFechaInicio(final Date fechaInicio) {
    this.fechaInicio = UtilidadesCommons.cloneDate(fechaInicio);
  }

  /**
   * Gets the fecha fin.
   *
   * @return the fechaFin
   */
  public Date getFechaFin() {
    return UtilidadesCommons.cloneDate(fechaFin);
  }

  /**
   * Sets the fecha fin.
   *
   * @param fechaFin the fechaFin to set
   */
  public void setFechaFin(final Date fechaFin) {
    this.fechaFin = UtilidadesCommons.cloneDate(fechaFin);
  }

  /**
   * Gets the ultimo informe realizado.
   *
   * @return the esUltimoInforme
   */
  public Boolean getEsUltimoInforme() {
    return esUltimoInforme;
  }

  /**
   * Sets the ultimo informe realizado.
   *
   * @param ultimoInformeRealizado the new es ultimo informe
   */
  public void setEsUltimoInforme(final Boolean ultimoInformeRealizado) {
    this.esUltimoInforme = ultimoInformeRealizado;
  }

  /**
   * Gets the tiempo residencia.
   *
   * @return the tiempoResidencia
   */
  public Long getTiempoResidencia() {
    return tiempoResidencia;
  }

  /**
   * Sets the tiempo residencia.
   *
   * @param tiempoResidencia the tiempoResidencia to set
   */
  public void setTiempoResidencia(final Long tiempoResidencia) {
    this.tiempoResidencia = tiempoResidencia;
  }

  /**
   * Gets the situacion capacidad.
   *
   * @return the situacionCapacidad
   */
  public String getSituacionCapacidad() {
    return situacionCapacidad;
  }

  /**
   * Sets the situacion capacidad.
   *
   * @param situacionCapacidad the situacionCapacidad to set
   */
  public void setSituacionCapacidad(final String situacionCapacidad) {
    this.situacionCapacidad = situacionCapacidad;
  }

  /**
   * Gets the tiene edad escolar.
   *
   * @return the tieneEdadEscolar
   */
  public Boolean getTieneEdadEscolar() {
    return tieneEdadEscolar;
  }

  /**
   * Sets the tiene edad escolar.
   *
   * @param tieneEdadEscolar the tieneEdadEscolar to set
   */
  public void setTieneEdadEscolar(final Boolean tieneEdadEscolar) {
    this.tieneEdadEscolar = tieneEdadEscolar;
  }

  /**
   * Gets the centro estudios.
   *
   * @return the centroEstudios
   */
  public String getCentroEstudios() {
    return centroEstudios;
  }

  /**
   * Sets the centro estudios.
   *
   * @param centroEstudios the centroEstudios to set
   */
  public void setCentroEstudios(final String centroEstudios) {
    this.centroEstudios = centroEstudios;
  }

  /**
   * Gets the tipo plaza residencia.
   *
   * @return the tipoPlazaResidencia
   */
  public String getTipoPlazaResidencia() {
    return tipoPlazaResidencia;
  }

  /**
   * Sets the tipo plaza residencia.
   *
   * @param tipoPlazaResidencia the tipoPlazaResidencia to set
   */
  public void setTipoPlazaResidencia(final String tipoPlazaResidencia) {
    this.tipoPlazaResidencia = tipoPlazaResidencia;
  }

  /**
   * Gets the centro aportacion usuario.
   *
   * @return the centroAportacionUsuario
   */
  public BigDecimal getCentroAportacionUsuario() {
    return centroAportacionUsuario;
  }

  /**
   * Sets the centro aportacion usuario.
   *
   * @param centroAportacionUsuario the centroAportacionUsuario to set
   */
  public void setCentroAportacionUsuario(
      final BigDecimal centroAportacionUsuario) {
    this.centroAportacionUsuario = centroAportacionUsuario;
  }

  /**
   * Gets the tipo centro especializado.
   *
   * @return the tipoCentroEspecializado
   */
  public String getTipoCentroEspecializado() {
    return tipoCentroEspecializado;
  }

  /**
   * Sets the tipo centro especializado.
   *
   * @param tipoCentroEspecializado the tipoCentroEspecializado to set
   */
  public void setTipoCentroEspecializado(final String tipoCentroEspecializado) {
    this.tipoCentroEspecializado = tipoCentroEspecializado;
  }

  /**
   * Gets the aportacion usuario centro especializado.
   *
   * @return the aportacionUsuarioCentroEspecializado
   */
  public BigDecimal getAportacionUsuarioCentroEspecializado() {
    return aportacionUsuarioCentroEspecializado;
  }

  /**
   * Sets the aportacion usuario centro especializado.
   *
   * @param aportacionUsuarioCentroEspecializado the
   *        aportacionUsuarioCentroEspecializado to set
   */
  public void setAportacionUsuarioCentroEspecializado(
      final BigDecimal aportacionUsuarioCentroEspecializado) {
    this.aportacionUsuarioCentroEspecializado =
        aportacionUsuarioCentroEspecializado;
  }

  /**
   * Gets the apoyo domicilio horas semana.
   *
   * @return the apoyoDomicilioHorasSemana
   */
  public Long getApoyoDomicilioHorasSemana() {
    return apoyoDomicilioHorasSemana;
  }

  /**
   * Sets the apoyo domicilio horas semana.
   *
   * @param apoyoDomicilioHorasSemana the apoyoDomicilioHorasSemana to set
   */
  public void setApoyoDomicilioHorasSemana(
      final Long apoyoDomicilioHorasSemana) {
    this.apoyoDomicilioHorasSemana = apoyoDomicilioHorasSemana;
  }

  /**
   * Gets the opinion persona situacion centro.
   *
   * @return the opinionPersonaSituacionCentro
   */
  public String getOpinionPersonaSituacionCentro() {
    return opinionPersonaSituacionCentro;
  }

  /**
   * Sets the opinion persona situacion centro.
   *
   * @param opinionPersonaSituacionCentro the opinionPersonaSituacionCentro to
   *        set
   */
  public void setOpinionPersonaSituacionCentro(
      final String opinionPersonaSituacionCentro) {
    this.opinionPersonaSituacionCentro = opinionPersonaSituacionCentro;
  }

  /**
   * Gets the opinion familia cren.
   *
   * @return the opinionFamiliaCren
   */
  public String getOpinionFamiliaCren() {
    return opinionFamiliaCren;
  }

  /**
   * Sets the opinion familia cren.
   *
   * @param opinionFamiliaCren the opinionFamiliaCren to set
   */
  public void setOpinionFamiliaCren(final String opinionFamiliaCren) {
    this.opinionFamiliaCren = opinionFamiliaCren;
  }

  /**
   * Gets the valoracion tecnica.
   *
   * @return the valoracionTecnica
   */
  public String getValoracionTecnica() {
    return valoracionTecnica;
  }

  /**
   * Sets the valoracion tecnica.
   *
   * @param valoracionTecnica the valoracionTecnica to set
   */
  public void setValoracionTecnica(final String valoracionTecnica) {
    this.valoracionTecnica = valoracionTecnica;
  }

  /**
   * Gets the tipo convivencia.
   *
   * @return the tipo convivencia
   */
  public String getTipoConvivencia() {
    return tipoConvivencia;
  }

  /**
   * Sets the tipo convivencia.
   *
   * @param tipoConvivencia the new tipo convivencia
   */
  public void setTipoConvivencia(final String tipoConvivencia) {
    this.tipoConvivencia = tipoConvivencia;
  }

  /**
   * Gets the apoyo familiar diario.
   *
   * @return the apoyoFamiliarDiario
   */
  public Boolean getApoyoFamiliarDiario() {
    return apoyoFamiliarDiario;
  }

  /**
   * Sets the apoyo familiar diario.
   *
   * @param apoyoFamiliarDiario the apoyoFamiliarDiario to set
   */
  public void setApoyoFamiliarDiario(final Boolean apoyoFamiliarDiario) {
    this.apoyoFamiliarDiario = apoyoFamiliarDiario;
  }

  /**
   * Gets the apoyo familiar esporadico.
   *
   * @return the apoyoFamiliarEsporadico
   */
  public Boolean getApoyoFamiliarEsporadico() {
    return apoyoFamiliarEsporadico;
  }

  /**
   * Sets the apoyo familiar esporadico.
   *
   * @param apoyoFamiliarEsporadico the apoyoFamiliarEsporadico to set
   */
  public void setApoyoFamiliarEsporadico(
      final Boolean apoyoFamiliarEsporadico) {
    this.apoyoFamiliarEsporadico = apoyoFamiliarEsporadico;
  }

  /**
   * Gets the apoyo social diario.
   *
   * @return the apoyoSocialDiario
   */
  public Boolean getApoyoSocialDiario() {
    return apoyoSocialDiario;
  }

  /**
   * Sets the apoyo social diario.
   *
   * @param apoyoSocialDiario the apoyoSocialDiario to set
   */
  public void setApoyoSocialDiario(final Boolean apoyoSocialDiario) {
    this.apoyoSocialDiario = apoyoSocialDiario;
  }

  /**
   * Gets the apoyo social esporadico.
   *
   * @return the apoyoSocialEsporadico
   */
  public Boolean getApoyoSocialEsporadico() {
    return apoyoSocialEsporadico;
  }

  /**
   * Sets the apoyo social esporadico.
   *
   * @param apoyoSocialEsporadico the apoyoSocialEsporadico to set
   */
  public void setApoyoSocialEsporadico(final Boolean apoyoSocialEsporadico) {
    this.apoyoSocialEsporadico = apoyoSocialEsporadico;
  }

  /**
   * Gets the sin apoyo.
   *
   * @return the sinApoyo
   */
  public Boolean getSinApoyo() {
    return sinApoyo;
  }

  /**
   * Sets the sin apoyo.
   *
   * @param sinApoyo the sinApoyo to set
   */
  public void setSinApoyo(final Boolean sinApoyo) {
    this.sinApoyo = sinApoyo;
  }

  /**
   * Gets the sin apoyo social.
   *
   * @return the sin apoyo social
   */
  public Boolean getSinApoyoSocial() {
    return sinApoyoSocial;
  }

  /**
   * Sets the sin apoyo social.
   *
   * @param sinApoyoSocial the new sin apoyo social
   */
  public void setSinApoyoSocial(final Boolean sinApoyoSocial) {
    this.sinApoyoSocial = sinApoyoSocial;
  }

  /**
   * Gets the nivel relacion.
   *
   * @return the nivelRelacion
   */
  public String getNivelRelacion() {
    return nivelRelacion;
  }

  /**
   * Sets the nivel relacion.
   *
   * @param nivelRelacion the nivelRelacion to set
   */
  public void setNivelRelacion(final String nivelRelacion) {
    this.nivelRelacion = nivelRelacion;
  }

  /**
   * Gets the mayor edad.
   *
   * @return the mayorEdad
   */
  public Boolean getMayorEdad() {
    return mayorEdad;
  }

  /**
   * Sets the mayor edad.
   *
   * @param mayorEdad the mayorEdad to set
   */
  public void setMayorEdad(final Boolean mayorEdad) {
    this.mayorEdad = mayorEdad;
  }

  /**
   * Gets the cuidador predependiente.
   *
   * @return the cuidadorPredependiente
   */
  public Boolean getCuidadorPredependiente() {
    return cuidadorPredependiente;
  }

  /**
   * Sets the cuidador predependiente.
   *
   * @param cuidadorPredependiente the cuidadorPredependiente to set
   */
  public void setCuidadorPredependiente(final Boolean cuidadorPredependiente) {
    this.cuidadorPredependiente = cuidadorPredependiente;
  }

  /**
   * Gets the concurrencia enfermedad.
   *
   * @return the concurrenciaEnfermedad
   */
  public Boolean getConcurrenciaEnfermedad() {
    return concurrenciaEnfermedad;
  }

  /**
   * Sets the concurrencia enfermedad.
   *
   * @param concurrenciaEnfermedad the concurrenciaEnfermedad to set
   */
  public void setConcurrenciaEnfermedad(final Boolean concurrenciaEnfermedad) {
    this.concurrenciaEnfermedad = concurrenciaEnfermedad;
  }

  /**
   * Gets the seguridad economica.
   *
   * @return the seguridadEconomica
   */
  public Boolean getSeguridadEconomica() {
    return seguridadEconomica;
  }

  /**
   * Sets the seguridad economica.
   *
   * @param seguridadEconomica the seguridadEconomica to set
   */
  public void setSeguridadEconomica(final Boolean seguridadEconomica) {
    this.seguridadEconomica = seguridadEconomica;
  }

  /**
   * Gets the conocimientos suficientes.
   *
   * @return the conocimientosSuficientes
   */
  public Boolean getConocimientosSuficientes() {
    return conocimientosSuficientes;
  }

  /**
   * Sets the conocimientos suficientes.
   *
   * @param conocimientosSuficientes the conocimientosSuficientes to set
   */
  public void setConocimientosSuficientes(
      final Boolean conocimientosSuficientes) {
    this.conocimientosSuficientes = conocimientosSuficientes;
  }

  /**
   * Gets the conocimientos escasos.
   *
   * @return the conocimientosEscasos
   */
  public Boolean getConocimientosEscasos() {
    return conocimientosEscasos;
  }

  /**
   * Sets the conocimientos escasos.
   *
   * @param conocimientosEscasos the conocimientosEscasos to set
   */
  public void setConocimientosEscasos(final Boolean conocimientosEscasos) {
    this.conocimientosEscasos = conocimientosEscasos;
  }

  /**
   * Gets the dificultad comprension enfermedad.
   *
   * @return the dificultadComprensionEnfermedad
   */
  public Boolean getDificultadComprensionEnfermedad() {
    return dificultadComprensionEnfermedad;
  }

  /**
   * Sets the dificultad comprension enfermedad.
   *
   * @param dificultadComprensionEnfermedad the dificultadComprensionEnfermedad
   *        to set
   */
  public void setDificultadComprensionEnfermedad(
      final Boolean dificultadComprensionEnfermedad) {
    this.dificultadComprensionEnfermedad = dificultadComprensionEnfermedad;
  }

  /**
   * Gets the disponibilidad apoyos profesionales.
   *
   * @return the disponibilidadApoyosProfesionales
   */
  public Boolean getDisponibilidadApoyosProfesionales() {
    return disponibilidadApoyosProfesionales;
  }

  /**
   * Sets the disponibilidad apoyos profesionales.
   *
   * @param disponibilidadApoyosProfesionales the
   *        disponibilidadApoyosProfesionales to set
   */
  public void setDisponibilidadApoyosProfesionales(
      final Boolean disponibilidadApoyosProfesionales) {
    this.disponibilidadApoyosProfesionales = disponibilidadApoyosProfesionales;
  }

  /**
   * Gets the disponibilidad tiempo.
   *
   * @return the disponibilidadTiempo
   */
  public Boolean getDisponibilidadTiempo() {
    return disponibilidadTiempo;
  }

  /**
   * Sets the disponibilidad tiempo.
   *
   * @param disponibilidadTiempo the disponibilidadTiempo to set
   */
  public void setDisponibilidadTiempo(final Boolean disponibilidadTiempo) {
    this.disponibilidadTiempo = disponibilidadTiempo;
  }

  /**
   * Gets the periodos descanso.
   *
   * @return the periodosDescanso
   */
  public Boolean getPeriodosDescanso() {
    return periodosDescanso;
  }

  /**
   * Sets the periodos descanso.
   *
   * @param periodosDescanso the periodosDescanso to set
   */
  public void setPeriodosDescanso(final Boolean periodosDescanso) {
    this.periodosDescanso = periodosDescanso;
  }

  /**
   * Gets the signos agotamiento.
   *
   * @return the signosAgotamiento
   */
  public Boolean getSignosAgotamiento() {
    return signosAgotamiento;
  }

  /**
   * Sets the signos agotamiento.
   *
   * @param signosAgotamiento the signosAgotamiento to set
   */
  public void setSignosAgotamiento(final Boolean signosAgotamiento) {
    this.signosAgotamiento = signosAgotamiento;
  }

  /**
   * Gets the sin signos fragilidad.
   *
   * @return the sinSignosFragilidad
   */
  public Boolean getSinSignosFragilidad() {
    return sinSignosFragilidad;
  }

  /**
   * Sets the sin signos fragilidad.
   *
   * @param sinSignosFragilidad the sinSignosFragilidad to set
   */
  public void setSinSignosFragilidad(final Boolean sinSignosFragilidad) {
    this.sinSignosFragilidad = sinSignosFragilidad;
  }

  /**
   * Gets the ausencia compromiso.
   *
   * @return the ausencia compromiso
   */
  public Boolean getAusenciaCompromiso() {
    return ausenciaCompromiso;
  }

  /**
   * Sets the ausencia compromiso.
   *
   * @param ausenciaCompromiso the new ausencia compromiso
   */
  public void setAusenciaCompromiso(final Boolean ausenciaCompromiso) {
    this.ausenciaCompromiso = ausenciaCompromiso;
  }

  /**
   * Gets the dificultados conexion.
   *
   * @return the dificultadosConexion
   */
  public Boolean getDificultadosConexion() {
    return dificultadosConexion;
  }

  /**
   * Sets the dificultados conexion.
   *
   * @param dificultadosConexion the dificultadosConexion to set
   */
  public void setDificultadosConexion(final Boolean dificultadosConexion) {
    this.dificultadosConexion = dificultadosConexion;
  }

  /**
   * Gets the riesgo claudicacion.
   *
   * @return the riesgoClaudicacion
   */
  public String getRiesgoClaudicacion() {
    return riesgoClaudicacion;
  }

  /**
   * Sets the riesgo claudicacion.
   *
   * @param riesgoClaudicacion the riesgoClaudicacion to set
   */
  public void setRiesgoClaudicacion(final String riesgoClaudicacion) {
    this.riesgoClaudicacion = riesgoClaudicacion;
  }

  /**
   * Gets the propiedad vivienda habitual.
   *
   * @return the propiedadViviendaHabitual
   */
  public String getPropiedadViviendaHabitual() {
    return propiedadViviendaHabitual;
  }

  /**
   * Sets the propiedad vivienda habitual.
   *
   * @param propiedadViviendaHabitual the propiedadViviendaHabitual to set
   */
  public void setPropiedadViviendaHabitual(
      final String propiedadViviendaHabitual) {
    this.propiedadViviendaHabitual = propiedadViviendaHabitual;
  }

  /**
   * Gets the condiciones vivienda.
   *
   * @return the condicionesVivienda
   */
  public String getCondicionesVivienda() {
    return condicionesVivienda;
  }

  /**
   * Sets the condiciones vivienda.
   *
   * @param condicionesVivienda the condicionesVivienda to set
   */
  public void setCondicionesVivienda(final String condicionesVivienda) {
    this.condicionesVivienda = condicionesVivienda;
  }

  /**
   * Gets the adaptacion hogar.
   *
   * @return the adaptacionHogar
   */
  public String getAdaptacionHogar() {
    return adaptacionHogar;
  }

  /**
   * Sets the adaptacion hogar.
   *
   * @param adaptacionHogar the adaptacionHogar to set
   */
  public void setAdaptacionHogar(final String adaptacionHogar) {
    this.adaptacionHogar = adaptacionHogar;
  }

  /**
   * Gets the facilita acceso.
   *
   * @return the facilitaAcceso
   */
  public Long getFacilitaAcceso() {
    return facilitaAcceso;
  }

  /**
   * Sets the facilita acceso.
   *
   * @param facilitaAcceso the facilitaAcceso to set
   */
  public void setFacilitaAcceso(final Long facilitaAcceso) {
    this.facilitaAcceso = facilitaAcceso;
  }

  /**
   * Gets the valoracion social.
   *
   * @return the valoracionSocial
   */
  public String getValoracionSocial() {
    return StringUtils.trim(valoracionSocial);
  }

  /**
   * Sets the valoracion social.
   *
   * @param valoracionSocial the valoracionSocial to set
   */
  public void setValoracionSocial(final String valoracionSocial) {
    this.valoracionSocial = StringUtils.trim(valoracionSocial);
  }

  /**
   * Gets the coinciden valores preferencias.
   *
   * @return the coincidenValoresPreferencias
   */
  public Boolean getCoincidenValoresPreferencias() {
    return coincidenValoresPreferencias;
  }

  /**
   * Sets the coinciden valores preferencias.
   *
   * @param coincidenValoresPreferencias the coincidenValoresPreferencias to set
   */
  public void setCoincidenValoresPreferencias(
      final Boolean coincidenValoresPreferencias) {
    this.coincidenValoresPreferencias = coincidenValoresPreferencias;
  }

  /**
   * Gets the motivos priorizacion profesional.
   *
   * @return the motivosPriorizacionProfesional
   */
  public String getMotivosPriorizacionProfesional() {
    return motivosPriorizacionProfesional;
  }

  /**
   * Sets the motivos priorizacion profesional.
   *
   * @param motivosPriorizacionProfesional the motivosPriorizacionProfesional to
   *        set
   */
  public void setMotivosPriorizacionProfesional(
      final String motivosPriorizacionProfesional) {
    this.motivosPriorizacionProfesional = motivosPriorizacionProfesional;
  }

  /**
   * Gets the identificador trabajador social.
   *
   * @return the identificadorTrabajadorSocial
   */
  public String getIdentificadorTrabajadorSocial() {
    return identificadorTrabajadorSocial;
  }

  /**
   * Sets the identificador trabajador social.
   *
   * @param identificadorTrabajadorSocial the identificadorTrabajadorSocial to
   *        set
   */
  public void setIdentificadorTrabajadorSocial(
      final String identificadorTrabajadorSocial) {
    this.identificadorTrabajadorSocial = identificadorTrabajadorSocial;
  }

  /**
   * Gets the numero colegiado.
   *
   * @return the numeroColegiado
   */
  public String getNumeroColegiado() {
    return numeroColegiado;
  }

  /**
   * Sets the numero colegiado.
   *
   * @param numeroColegiado the numeroColegiado to set
   */
  public void setNumeroColegiado(final String numeroColegiado) {
    this.numeroColegiado = numeroColegiado;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the nivel cultural.
   *
   * @return the nivelCultural
   */
  public Long getNivelCultural() {
    return nivelCultural;
  }

  /**
   * Sets the nivel cultural.
   *
   * @param nivelCultural the nivelCultural to set
   */
  public void setNivelCultural(final Long nivelCultural) {
    this.nivelCultural = nivelCultural;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the tipo centro.
   *
   * @return the tipoCentro
   */
  public Long getTipoCentro() {
    return tipoCentro;
  }

  /**
   * Sets the tipo centro.
   *
   * @param tipoCentro the tipoCentro to set
   */
  public void setTipoCentro(final Long tipoCentro) {
    this.tipoCentro = tipoCentro;
  }

  /**
   * Gets the tiene edad laboral.
   *
   * @return the tieneEdadLaboral
   */
  public Boolean getTieneEdadLaboral() {
    return tieneEdadLaboral;
  }

  /**
   * Sets the tiene edad laboral.
   *
   * @param tieneEdadLaboral the tieneEdadLaboral to set
   */
  public void setTieneEdadLaboral(final Boolean tieneEdadLaboral) {
    this.tieneEdadLaboral = tieneEdadLaboral;
  }

  /**
   * Gets the activo laboralmente.
   *
   * @return the activoLaboralmente
   */
  public Boolean getActivoLaboralmente() {
    return activoLaboralmente;
  }

  /**
   * Sets the activo laboralmente.
   *
   * @param activoLaboralmente the activoLaboralmente to set
   */
  public void setActivoLaboralmente(final Boolean activoLaboralmente) {
    this.activoLaboralmente = activoLaboralmente;
  }

  /**
   * Gets the situacion inactividad.
   *
   * @return the situacionInactividad
   */
  public Long getSituacionInactividad() {
    return situacionInactividad;
  }

  /**
   * Sets the situacion inactividad.
   *
   * @param situacionInactividad the situacionInactividad to set
   */
  public void setSituacionInactividad(final Long situacionInactividad) {
    this.situacionInactividad = situacionInactividad;
  }

  /**
   * Gets the tipo jornada.
   *
   * @return the tipoJornada
   */
  public Long getTipoJornada() {
    return tipoJornada;
  }

  /**
   * Sets the tipo jornada.
   *
   * @param tipoJornada the tipoJornada to set
   */
  public void setTipoJornada(final Long tipoJornada) {
    this.tipoJornada = tipoJornada;
  }

  /**
   * Gets the profesion solicitante.
   *
   * @return the profesionSolicitante
   */
  public String getProfesionSolicitante() {
    return profesionSolicitante;
  }

  /**
   * Sets the profesion solicitante.
   *
   * @param profesionSolicitante the profesionSolicitante to set
   */
  public void setProfesionSolicitante(final String profesionSolicitante) {
    this.profesionSolicitante = profesionSolicitante;
  }


  /**
   * Gets the soporte social Y familiar.
   *
   * @return the soporteSocialYFamiliar
   */
  public Long getSoporteSocialYFamiliar() {
    return soporteSocialYFamiliar;
  }

  /**
   * Sets the soporte social Y familiar.
   *
   * @param soporteSocialYFamiliar the soporteSocialYFamiliar to set
   */
  public void setSoporteSocialYFamiliar(final Long soporteSocialYFamiliar) {
    this.soporteSocialYFamiliar = soporteSocialYFamiliar;
  }

  /**
   * Gets the ayuda movilidad baston muleta.
   *
   * @return the ayudaMovilidadBastonMuleta
   */
  public Boolean getAyudaMovilidadBastonMuleta() {
    return ayudaMovilidadBastonMuleta;
  }

  /**
   * Sets the ayuda movilidad baston muleta.
   *
   * @param ayudaMovilidadBastonMuleta the ayudaMovilidadBastonMuleta to set
   */
  public void setAyudaMovilidadBastonMuleta(
      final Boolean ayudaMovilidadBastonMuleta) {
    this.ayudaMovilidadBastonMuleta = ayudaMovilidadBastonMuleta;
  }

  /**
   * Gets the ayuda movilidad silla ruedas.
   *
   * @return the ayudaMovilidadSillaRuedas
   */
  public Boolean getAyudaMovilidadSillaRuedas() {
    return ayudaMovilidadSillaRuedas;
  }

  /**
   * Sets the ayuda movilidad silla ruedas.
   *
   * @param ayudaMovilidadSillaRuedas the ayudaMovilidadSillaRuedas to set
   */
  public void setAyudaMovilidadSillaRuedas(
      final Boolean ayudaMovilidadSillaRuedas) {
    this.ayudaMovilidadSillaRuedas = ayudaMovilidadSillaRuedas;
  }

  /**
   * Gets the ayuda movilidad grua transferencia.
   *
   * @return the ayudaMovilidadGruaTransferencia
   */
  public Boolean getAyudaMovilidadGruaTransferencia() {
    return ayudaMovilidadGruaTransferencia;
  }

  /**
   * Sets the ayuda movilidad grua transferencia.
   *
   * @param ayudaMovilidadGruaTransferencia the ayudaMovilidadGruaTransferencia
   *        to set
   */
  public void setAyudaMovilidadGruaTransferencia(
      final Boolean ayudaMovilidadGruaTransferencia) {
    this.ayudaMovilidadGruaTransferencia = ayudaMovilidadGruaTransferencia;
  }

  /**
   * Gets the ayuda movilidad otros.
   *
   * @return the ayudaMovilidadOtros
   */
  public Boolean getAyudaMovilidadOtros() {
    return ayudaMovilidadOtros;
  }

  /**
   * Sets the ayuda movilidad otros.
   *
   * @param ayudaMovilidadOtros the ayudaMovilidadOtros to set
   */
  public void setAyudaMovilidadOtros(final Boolean ayudaMovilidadOtros) {
    this.ayudaMovilidadOtros = ayudaMovilidadOtros;
  }

  /**
   * Gets the otras ayudas movilidad.
   *
   * @return the otrasAyudasMovilidad
   */
  public String getOtrasAyudasMovilidad() {
    return otrasAyudasMovilidad;
  }

  /**
   * Sets the otras ayudas movilidad.
   *
   * @param otrasAyudasMovilidad the otrasAyudasMovilidad to set
   */
  public void setOtrasAyudasMovilidad(final String otrasAyudasMovilidad) {
    this.otrasAyudasMovilidad = otrasAyudasMovilidad;
  }

  /**
   * Gets the ayuda aseo personal.
   *
   * @return the ayudaAseoPersonal
   */
  public Boolean getAyudaAseoPersonal() {
    return ayudaAseoPersonal;
  }

  /**
   * Sets the ayuda aseo personal.
   *
   * @param ayudaAseoPersonal the ayudaAseoPersonal to set
   */
  public void setAyudaAseoPersonal(final Boolean ayudaAseoPersonal) {
    this.ayudaAseoPersonal = ayudaAseoPersonal;
  }

  /**
   * Gets the ayuda aseo banio Y ducha.
   *
   * @return the ayudaAseoBanioYDucha
   */
  public Boolean getAyudaAseoBanioYDucha() {
    return ayudaAseoBanioYDucha;
  }

  /**
   * Sets the ayuda aseo banio Y ducha.
   *
   * @param ayudaAseoBanioYDucha the ayudaAseoBanioYDucha to set
   */
  public void setAyudaAseoBanioYDucha(final Boolean ayudaAseoBanioYDucha) {
    this.ayudaAseoBanioYDucha = ayudaAseoBanioYDucha;
  }

  /**
   * Gets the ayuda aseo otros.
   *
   * @return the ayudaAseoOtros
   */
  public Boolean getAyudaAseoOtros() {
    return ayudaAseoOtros;
  }

  /**
   * Sets the ayuda aseo otros.
   *
   * @param ayudaAseoOtros the ayudaAseoOtros to set
   */
  public void setAyudaAseoOtros(final Boolean ayudaAseoOtros) {
    this.ayudaAseoOtros = ayudaAseoOtros;
  }

  /**
   * Gets the otras ayudas aseo.
   *
   * @return the otrasAyudasAseo
   */
  public String getOtrasAyudasAseo() {
    return otrasAyudasAseo;
  }

  /**
   * Sets the otras ayudas aseo.
   *
   * @param otrasAyudasAseo the otrasAyudasAseo to set
   */
  public void setOtrasAyudasAseo(final String otrasAyudasAseo) {
    this.otrasAyudasAseo = otrasAyudasAseo;
  }

  /**
   * Gets the ayuda alimentacion para preparar comida.
   *
   * @return the ayudaAlimentacionParaPrepararComida
   */
  public Boolean getAyudaAlimentacionParaPrepararComida() {
    return ayudaAlimentacionParaPrepararComida;
  }

  /**
   * Sets the ayuda alimentacion para preparar comida.
   *
   * @param ayudaAlimentacionParaPrepararComida the
   *        ayudaAlimentacionParaPrepararComida to set
   */
  public void setAyudaAlimentacionParaPrepararComida(
      final Boolean ayudaAlimentacionParaPrepararComida) {
    this.ayudaAlimentacionParaPrepararComida =
        ayudaAlimentacionParaPrepararComida;
  }

  /**
   * Gets the ayuda alimentacion para comer Y beber.
   *
   * @return the ayudaAlimentacionParaComerYBeber
   */
  public Boolean getAyudaAlimentacionParaComerYBeber() {
    return ayudaAlimentacionParaComerYBeber;
  }

  /**
   * Sets the ayuda alimentacion para comer Y beber.
   *
   * @param ayudaAlimentacionParaComerYBeber the
   *        ayudaAlimentacionParaComerYBeber to set
   */
  public void setAyudaAlimentacionParaComerYBeber(
      final Boolean ayudaAlimentacionParaComerYBeber) {
    this.ayudaAlimentacionParaComerYBeber = ayudaAlimentacionParaComerYBeber;
  }

  /**
   * Gets the ayuda alimentacion otros.
   *
   * @return the ayudaAlimentacionOtros
   */
  public Boolean getAyudaAlimentacionOtros() {
    return ayudaAlimentacionOtros;
  }

  /**
   * Sets the ayuda alimentacion otros.
   *
   * @param ayudaAlimentacionOtros the ayudaAlimentacionOtros to set
   */
  public void setAyudaAlimentacionOtros(final Boolean ayudaAlimentacionOtros) {
    this.ayudaAlimentacionOtros = ayudaAlimentacionOtros;
  }

  /**
   * Gets the otras ayudas alimentacion.
   *
   * @return the otrasAyudasAlimentacion
   */
  public String getOtrasAyudasAlimentacion() {
    return otrasAyudasAlimentacion;
  }

  /**
   * Sets the otras ayudas alimentacion.
   *
   * @param otrasAyudasAlimentacion the otrasAyudasAlimentacion to set
   */
  public void setOtrasAyudasAlimentacion(final String otrasAyudasAlimentacion) {
    this.otrasAyudasAlimentacion = otrasAyudasAlimentacion;
  }

  /**
   * Gets the ayudas tecnicas tic.
   *
   * @return the ayudasTecnicasTic
   */
  public String getAyudasTecnicasTic() {
    return ayudasTecnicasTic;
  }

  /**
   * Sets the ayudas tecnicas tic.
   *
   * @param ayudasTecnicasTic the ayudasTecnicasTic to set
   */
  public void setAyudasTecnicasTic(final String ayudasTecnicasTic) {
    this.ayudasTecnicasTic = ayudasTecnicasTic;
  }

  /**
   * Gets the ancho puertas.
   *
   * @return the anchoPuertas
   */
  public Boolean getAnchoPuertas() {
    return anchoPuertas;
  }

  /**
   * Sets the ancho puertas.
   *
   * @param anchoPuertas the anchoPuertas to set
   */
  public void setAnchoPuertas(final Boolean anchoPuertas) {
    this.anchoPuertas = anchoPuertas;
  }

  /**
   * Gets the escaleras interiores.
   *
   * @return the escalerasInteriores
   */
  public Boolean getEscalerasInteriores() {
    return escalerasInteriores;
  }

  /**
   * Sets the escaleras interiores.
   *
   * @param escalerasInteriores the escalerasInteriores to set
   */
  public void setEscalerasInteriores(final Boolean escalerasInteriores) {
    this.escalerasInteriores = escalerasInteriores;
  }

  /**
   * Gets the adecuacion sanitarios.
   *
   * @return the adecuacionSanitarios
   */
  public Boolean getAdecuacionSanitarios() {
    return adecuacionSanitarios;
  }

  /**
   * Sets the adecuacion sanitarios.
   *
   * @param adecuacionSanitarios the adecuacionSanitarios to set
   */
  public void setAdecuacionSanitarios(final Boolean adecuacionSanitarios) {
    this.adecuacionSanitarios = adecuacionSanitarios;
  }

  /**
   * Gets the pasamanos.
   *
   * @return the pasamanos
   */
  public Boolean getPasamanos() {
    return pasamanos;
  }

  /**
   * Sets the pasamanos.
   *
   * @param pasamanos the pasamanos to set
   */
  public void setPasamanos(final Boolean pasamanos) {
    this.pasamanos = pasamanos;
  }

  /**
   * Gets the adecuacion cocina.
   *
   * @return the adecuacionCocina
   */
  public Boolean getAdecuacionCocina() {
    return adecuacionCocina;
  }

  /**
   * Sets the adecuacion cocina.
   *
   * @param adecuacionCocina the adecuacionCocina to set
   */
  public void setAdecuacionCocina(final Boolean adecuacionCocina) {
    this.adecuacionCocina = adecuacionCocina;
  }

  /**
   * Gets the instalacion gas luz.
   *
   * @return the instalacionGasLuz
   */
  public Boolean getInstalacionGasLuz() {
    return instalacionGasLuz;
  }

  /**
   * Sets the instalacion gas luz.
   *
   * @param instalacionGasLuz the instalacionGasLuz to set
   */
  public void setInstalacionGasLuz(final Boolean instalacionGasLuz) {
    this.instalacionGasLuz = instalacionGasLuz;
  }

  /**
   * Gets the suelo antideslizante.
   *
   * @return the sueloAntideslizante
   */
  public Boolean getSueloAntideslizante() {
    return sueloAntideslizante;
  }

  /**
   * Sets the suelo antideslizante.
   *
   * @param sueloAntideslizante the sueloAntideslizante to set
   */
  public void setSueloAntideslizante(final Boolean sueloAntideslizante) {
    this.sueloAntideslizante = sueloAntideslizante;
  }

  /**
   * Gets the otros dentro vivienda.
   *
   * @return the otrosDentroVivienda
   */
  public Boolean getOtrosDentroVivienda() {
    return otrosDentroVivienda;
  }

  /**
   * Sets the otros dentro vivienda.
   *
   * @param otrosDentroVivienda the otrosDentroVivienda to set
   */
  public void setOtrosDentroVivienda(final Boolean otrosDentroVivienda) {
    this.otrosDentroVivienda = otrosDentroVivienda;
  }

  /**
   * Gets the rampas.
   *
   * @return the rampas
   */
  public Boolean getRampas() {
    return rampas;
  }

  /**
   * Sets the rampas.
   *
   * @param rampas the rampas to set
   */
  public void setRampas(final Boolean rampas) {
    this.rampas = rampas;
  }

  /**
   * Gets the ascensor.
   *
   * @return the ascensor
   */
  public Boolean getAscensor() {
    return ascensor;
  }

  /**
   * Sets the ascensor.
   *
   * @param ascensor the ascensor to set
   */
  public void setAscensor(final Boolean ascensor) {
    this.ascensor = ascensor;
  }

  /**
   * Gets the silla salvaescalera.
   *
   * @return the sillaSalvaescalera
   */
  public Boolean getSillaSalvaescalera() {
    return sillaSalvaescalera;
  }

  /**
   * Sets the silla salvaescalera.
   *
   * @param sillaSalvaescalera the sillaSalvaescalera to set
   */
  public void setSillaSalvaescalera(final Boolean sillaSalvaescalera) {
    this.sillaSalvaescalera = sillaSalvaescalera;
  }

  /**
   * Gets the otros acceso hogar.
   *
   * @return the otrosAccesoHogar
   */
  public Boolean getOtrosAccesoHogar() {
    return otrosAccesoHogar;
  }

  /**
   * Sets the otros acceso hogar.
   *
   * @param otrosAccesoHogar the otrosAccesoHogar to set
   */
  public void setOtrosAccesoHogar(final Boolean otrosAccesoHogar) {
    this.otrosAccesoHogar = otrosAccesoHogar;
  }

  /**
   * Gets the otros entorno.
   *
   * @return the otrosEntorno
   */
  public String getOtrosEntorno() {
    return otrosEntorno;
  }

  /**
   * Sets the otros entorno.
   *
   * @param otrosEntorno the otrosEntorno to set
   */
  public void setOtrosEntorno(final String otrosEntorno) {
    this.otrosEntorno = otrosEntorno;
  }

  /**
   * Gets the dificultades servicio.
   *
   * @return the dificultadesServicio
   */
  public String getDificultadesServicio() {
    return dificultadesServicio;
  }

  /**
   * Sets the dificultades servicio.
   *
   * @param dificultadesServicio the dificultadesServicio to set
   */
  public void setDificultadesServicio(final String dificultadesServicio) {
    this.dificultadesServicio = dificultadesServicio;
  }

  /**
   * Gets the info cuidador.
   *
   * @return the infoCuidador
   */
  public String getInfoCuidador() {
    return infoCuidador;
  }

  /**
   * Sets the info cuidador.
   *
   * @param infoCuidador the infoCuidador to set
   */
  public void setInfoCuidador(final String infoCuidador) {
    this.infoCuidador = infoCuidador;
  }

  /**
   * Gets the info asistente.
   *
   * @return the infoAsistente
   */
  public String getInfoAsistente() {
    return infoAsistente;
  }

  /**
   * Sets the info asistente.
   *
   * @param infoAsistente the infoAsistente to set
   */
  public void setInfoAsistente(final String infoAsistente) {
    this.infoAsistente = infoAsistente;
  }

  /**
   * Gets the pedido.
   *
   * @return the pedido
   */
  public Long getPedido() {
    return pedido;
  }

  /**
   * Sets the pedido.
   *
   * @param pedido the pedido to set
   */
  public void setPedido(final Long pedido) {
    this.pedido = pedido;
  }

  /**
   * Gets the tipo informe social.
   *
   * @return the tipoInformeSocial
   */
  public Long getTipoInformeSocial() {
    return tipoInformeSocial;
  }

  /**
   * Sets the tipo informe social.
   *
   * @param tipoInformeSocial the tipoInformeSocial to set
   */
  public void setTipoInformeSocial(final Long tipoInformeSocial) {
    this.tipoInformeSocial = tipoInformeSocial;
  }

  /**
   * Gets the situacion capacidad laboral.
   *
   * @return the situacionCapacidadLaboral
   */
  public Long getSituacionCapacidadLaboral() {
    return situacionCapacidadLaboral;
  }

  /**
   * Sets the situacion capacidad laboral.
   *
   * @param situacionCapacidadLaboral the situacionCapacidadLaboral to set
   */
  public void setSituacionCapacidadLaboral(
      final Long situacionCapacidadLaboral) {
    this.situacionCapacidadLaboral = situacionCapacidadLaboral;
  }

  /**
   * Gets the modalidad.
   *
   * @return the modalidad
   */
  public String getModalidad() {
    return modalidad;
  }

  /**
   * Sets the modalidad.
   *
   * @param modalidad the modalidad to set
   */
  public void setModalidad(final String modalidad) {
    this.modalidad = modalidad;
  }

  /**
   * Gets the intensidad asistencia.
   *
   * @return the intensidadAsistencia
   */
  public String getIntensidadAsistencia() {
    return intensidadAsistencia;
  }

  /**
   * Sets the intensidad asistencia.
   *
   * @param intensidadAsistencia the intensidadAsistencia to set
   */
  public void setIntensidadAsistencia(final String intensidadAsistencia) {
    this.intensidadAsistencia = intensidadAsistencia;
  }

  /**
   * Gets the fecha cuidador.
   *
   * @return the fechaCuidador
   */
  public Date getFechaCuidador() {
    return UtilidadesCommons.cloneDate(fechaCuidador);
  }

  /**
   * Sets the fecha cuidador.
   *
   * @param fechaCuidador the fechaCuidador to set
   */
  public void setFechaCuidador(final Date fechaCuidador) {
    this.fechaCuidador = UtilidadesCommons.cloneDate(fechaCuidador);
  }

  /**
   * Gets the motivo personal.
   *
   * @return the motivoPersonal
   */
  public Boolean getMotivoPersonal() {
    return motivoPersonal;
  }

  /**
   * Sets the motivo personal.
   *
   * @param motivoPersonal the motivoPersonal to set
   */
  public void setMotivoPersonal(final Boolean motivoPersonal) {
    this.motivoPersonal = motivoPersonal;
  }

  /**
   * Gets the preferencias revisadas.
   *
   * @return the preferenciasRevisadas
   */
  public Boolean getPreferenciasRevisadas() {
    return preferenciasRevisadas;
  }

  /**
   * Sets the preferencias revisadas.
   *
   * @param preferenciasRevisadas the preferenciasRevisadas to set
   */
  public void setPreferenciasRevisadas(final Boolean preferenciasRevisadas) {
    this.preferenciasRevisadas = preferenciasRevisadas;
  }

  /**
   * Gets the cambio revisado.
   *
   * @return the cambioRevisado
   */
  public Boolean getCambioRevisado() {
    return cambioRevisado;
  }

  /**
   * Sets the cambio revisado.
   *
   * @param cambioRevisado the cambioRevisado to set
   */
  public void setCambioRevisado(final Boolean cambioRevisado) {
    this.cambioRevisado = cambioRevisado;
  }

  /**
   * Gets the pk documento preferencias.
   *
   * @return the pkDocumentoPreferencias
   */
  public Long getPkDocumentoPreferencias() {
    return pkDocumentoPreferencias;
  }

  /**
   * Sets the pk documento preferencias.
   *
   * @param pkDocumentoPreferencias the pkDocumentoPreferencias to set
   */
  public void setPkDocumentoPreferencias(final Long pkDocumentoPreferencias) {
    this.pkDocumentoPreferencias = pkDocumentoPreferencias;
  }

  /**
   * Gets the pk pia.
   *
   * @return the pk pia
   */
  public Long getPkPia() {
    return pkPia;
  }

  /**
   * Sets the pk pia.
   *
   * @param pkPia the new pk pia
   */
  public void setPkPia(final Long pkPia) {
    this.pkPia = pkPia;
  }

  /**
   * Gets the tiene centro residencial.
   *
   * @return the tiene centro residencial
   */
  public Boolean getTieneCentroResidencial() {
    return tieneCentroResidencial;
  }

  /**
   * Sets the tiene centro residencial.
   *
   * @param tieneCentroResidencial the new tiene centro residencial
   */
  public void setTieneCentroResidencial(final Boolean tieneCentroResidencial) {
    this.tieneCentroResidencial = tieneCentroResidencial;
  }

  /**
   * Gets the centro atencion residencial.
   *
   * @return the centro atencion residencial
   */
  public CentroDTO getCentroAtencionResidencial() {
    return centroAtencionResidencial;
  }

  /**
   * Gets the nombre centro antecedente social.
   *
   * @return the nombre centro antecedente social
   */
  public String getNombreCentroAntecedenteSocial() {
    return nombreCentroAntecedenteSocial;
  }

  /**
   * Sets the nombre centro antecedente social.
   *
   * @param nombreCentroAntecedenteSocial the new nombre centro antecedente
   *        social
   */
  public void setNombreCentroAntecedenteSocial(
      final String nombreCentroAntecedenteSocial) {
    this.nombreCentroAntecedenteSocial = nombreCentroAntecedenteSocial;
  }

  /**
   * Sets the centro atencion residencial.
   *
   * @param centroAtencionResidencial the new centro atencion residencial
   */
  public void setCentroAtencionResidencial(
      final CentroDTO centroAtencionResidencial) {
    this.centroAtencionResidencial = centroAtencionResidencial;
  }

  /**
   * Gets the tiene centro atencion diurna.
   *
   * @return the tiene centro atencion diurna
   */
  public Boolean getTieneCentroAtencionDiurna() {
    return tieneCentroAtencionDiurna;
  }

  /**
   * Sets the tiene centro atencion diurna.
   *
   * @param tieneCentroAtencionDiurna the new tiene centro atencion diurna
   */
  public void setTieneCentroAtencionDiurna(
      final Boolean tieneCentroAtencionDiurna) {
    this.tieneCentroAtencionDiurna = tieneCentroAtencionDiurna;
  }

  /**
   * Gets the centro atencion diurna.
   *
   * @return the centro atencion diurna
   */
  public CentroDTO getCentroAtencionDiurna() {
    return centroAtencionDiurna;
  }

  /**
   * Sets the centro atencion diurna.
   *
   * @param centroAtencionDiurna the new centro atencion diurna
   */
  public void setCentroAtencionDiurna(final CentroDTO centroAtencionDiurna) {
    this.centroAtencionDiurna = centroAtencionDiurna;
  }

  /**
   * Gets the persona.
   *
   * @return the persona
   */
  public PersonaAdaDTO getSolicitante() {
    return solicitante;
  }

  /**
   * Sets the persona.
   *
   * @param solicitante the new solicitante
   */
  public void setSolicitante(final PersonaAdaDTO solicitante) {
    this.solicitante = solicitante;
  }

  /**
   * En estado.
   *
   * @param estado the estado
   * @return true, if successful
   */
  public boolean enEstado(final EstadoInformeSocialEnum estado) {
    return estado.getValor().equals(this.estadoPeticion);
  }

  /**
   * En estado cerrado.
   *
   * @return true, if successful
   */
  public boolean enEstadoCerrado() {
    return this.enEstado(EstadoInformeSocialEnum.CERRADO);
  }

  /**
   * Gets the motivos generacion informe social.
   *
   * @return the motivos generacion informe social
   */

  public List<MotivoGeneracionInformeSocialDTO> getMotivosGeneracionInformeSocial() {
    return UtilidadesCommons.collectorsToList(motivosGeneracionInformeSocial);
  }

  /**
   * Sets the motivos generacion informe social.
   *
   * @param motivosGeneracionInformeSocial the new motivos generacion informe
   *        social
   */

  public void setMotivosGeneracionInformeSocial(
      final List<MotivoGeneracionInformeSocialDTO> motivosGeneracionInformeSocial) {
    this.motivosGeneracionInformeSocial =
        UtilidadesCommons.collectorsToList(motivosGeneracionInformeSocial);
  }

  /**
   * Gets the motivos generacion to string.
   *
   * @return the motivos generacion to string
   */
  public String getMotivosGeneracionToString() {
    final StringBuilder sb = new StringBuilder();

    this.motivosGeneracionInformeSocial.stream()
        .map(MotivoGeneracionInformeSocialDTO::getTextoMotivoGeneracion)
        .collect(Collectors.toList())
        .forEach(motivo -> sb.append(motivo).append(ConstantesWeb.ESPACIO));

    return sb.toString();
  }

  /**
   * Gets the tipo informe social label.
   *
   * @return the tipo informe social label
   */
  public String getTipoInformeSocialLabel() {
    final String label =
        TipoInformeSocialEnum.labelFromValor(tipoInformeSocial);
    return label == null ? null : Labels.getLabel(label);
  }

  /**
   * Gets the estado informe social label.
   *
   * @return the estado informe social label
   */
  public String getEstadoInformeSocialLabel() {
    final String label = EstadoInformeSocialEnum.labelFromValor(estadoPeticion);
    return label == null ? null : Labels.getLabel(label);
  }


  /**
   * Generado por motivo.
   *
   * @param motivos the motivos
   * @return true, if successful
   */
  public boolean generadoPorMotivo(final MotivoGeneracionEnum... motivos) {
    final List<MotivoGeneracionEnum> motivosList = Arrays.asList(motivos);
    if (CollectionUtils.isEmpty(motivosList)) {
      return false; // No se proporcionaron motivos, no se cumplirá la condición
    }

    for (final MotivoGeneracionEnum motivo : motivosList) {
      final boolean anyMatch = motivosGeneracionInformeSocial.stream().anyMatch(
          m -> motivo.getCodigo().equals(m.getMotivoGeneracion().getCodigo()));

      if (anyMatch) {
        return true; // Si al menos un motivo coincide, devolvemos true
      }
    }

    return false; // Si no hay coincidencias, devolvemos false
  }

  /**
   * Generado por motivo manual.
   *
   * @param e the e
   * @return true, if successful
   */
  public boolean generadoPorMotivoManual(final MotivoGeneracionEnum e) {
    return generadoPorMotivo(e) && e.isManual();
  }

  /**
   * Es informe tipo.
   *
   * @param tipoEnum the tipo enum
   * @return true, if successful
   */
  public boolean esInformeDeTipo(final TipoInformeSocialEnum tipoEnum) {
    return tipoEnum.getValor().equals(this.tipoInformeSocial);
  }


  /**
   * Gets the preferencia informe social.
   *
   * @return the preferencia informe social
   */

  public List<PreferenciaInformeSocialDTO> getPreferenciasInformeSocial() {
    return UtilidadesCommons.collectorsToList(preferenciasInformeSocial);
  }

  /**
   * Sets the preferencia informe social.
   *
   * @param preferenciasInformeSocial the new preferencia informe social
   */

  public void setPreferenciasInformeSocial(
      final List<PreferenciaInformeSocialDTO> preferenciasInformeSocial) {
    this.preferenciasInformeSocial =
        UtilidadesCommons.collectorsToList(preferenciasInformeSocial);
  }

  /**
   * Gets the foto preferencias solicitud.
   *
   * @return the foto preferencias solicitud
   */
  public List<PreferenciaInformeSocialDTO> getFotoPreferenciasSolicitud() {
    return getPreferenciasTipo(
        TipoPreferenciaInformeSocialEnum.PREFERENCIA_SOLICITANTE);
  }

  /**
   * Gets the preferencias valoracion.
   *
   * @return the preferencias valoracion
   */
  public List<PreferenciaInformeSocialDTO> getPreferenciasValoracion() {
    return getPreferenciasTipo(
        TipoPreferenciaInformeSocialEnum.VALORACION_PROFESIONAL);
  }


  /**
   * Gets the preferencias tipo.
   *
   * @param tipo the tipo
   * @return the preferencias tipo
   */
  private List<PreferenciaInformeSocialDTO> getPreferenciasTipo(
      final TipoPreferenciaInformeSocialEnum tipo) {
    return CollectionUtils.isEmpty(preferenciasInformeSocial)
        ? new ArrayList<>()
        : preferenciasInformeSocial.stream()
            .filter(pref -> tipo.getValor().equals(pref.getTipoPreferencia()))
            .collect(Collectors.toList());
  }

  /**
   * Gets the documentos informe social.
   *
   * @return the documentos informe social
   */

  public List<DocumentoInformeSocialDTO> getDocumentosInformeSocial() {
    return UtilidadesCommons.collectorsToList(documentosInformeSocial);
  }

  /**
   * Sets the documentos informe social.
   *
   * @param documentosInformeSocial the new documentos informe social
   */

  public void setDocumentosInformeSocial(
      final List<DocumentoInformeSocialDTO> documentosInformeSocial) {
    this.documentosInformeSocial =
        UtilidadesCommons.collectorsToList(documentosInformeSocial);
  }

  /**
   * Gets the nombre completo usuario asignado.
   *
   * @return the nombre completo usuario asignado
   */
  public String getNombreCompletoUsuarioCreador() {
    return (this.usuarioCreador == null) ? ""
        : this.usuarioCreador.getPersona().getNombreCompleto();
  }

  /**
   * Gets the nombre completo usuario asignado.
   *
   * @return the nombre completo usuario asignado
   */
  public String getNombreCompletoUsuarioAsignado() {
    return (this.usuarioAsignado == null) ? ""
        : this.usuarioAsignado.getPersona().getNombreCompleto();
  }


  /**
   * Gets the tiene cuidador idoneo.
   *
   * @return the tiene cuidador idoneo
   */
  public boolean getTieneCuidadorIdoneo() {
    return !StringUtils.isEmpty(infoCuidador) || fechaCuidador != null;
  }

  /**
   * Gets the tiene asistente idoneo.
   *
   * @return the tiene asistente idoneo
   */
  public boolean getTieneAsistenteIdoneo() {
    return !StringUtils.isEmpty(infoAsistente);
  }

  /**
   * Gets the coincide cuidador.
   *
   * @return the coincide cuidador
   */
  public Boolean getCoincideCuidador() {
    return coincideCuidador;
  }

  /**
   * Sets the coincide cuidador.
   *
   * @param coincideCuidador the new coincide cuidador
   */
  public void setCoincideCuidador(final Boolean coincideCuidador) {
    this.coincideCuidador = coincideCuidador;
  }

  /**
   * Gets the cuidadores informe social.
   *
   * @return the cuidadores informe social
   */
  public List<CuidadorInformeSocialDTO> getCuidadoresInformeSocial() {
    return UtilidadesCommons.collectorsToList(cuidadoresInformeSocial);
  }

  /**
   * Sets the cuidadores informe social.
   *
   * @param cuidadoresInformeSocial the new cuidadores informe social
   */
  public void setCuidadoresInformeSocial(
      final List<CuidadorInformeSocialDTO> cuidadoresInformeSocial) {
    this.cuidadoresInformeSocial =
        UtilidadesCommons.collectorsToList(cuidadoresInformeSocial);
  }

  /**
   * Gets the cuidadores valoracion.
   *
   * @return the cuidadores valoracion
   */
  public List<CuidadorInformeSocialDTO> getCuidadoresInformeSocialValoracion() {
    return CollectionUtils.isEmpty(cuidadoresInformeSocial) ? new ArrayList<>()
        : cuidadoresInformeSocial.stream()
            .filter(cnp -> cnp.tieneOrigenTipo(TipoOrigenEnum.VALORACION))
            .collect(Collectors.toList());
  }

  /**
   * Gets the cuidadores valoracion.
   *
   * @return the cuidadores valoracion
   */
  public List<CuidadorInformeSocialDTO> getCuidadoresInformeSocialPreferencia() {
    return CollectionUtils.isEmpty(cuidadoresInformeSocial) ? new ArrayList<>()
        : cuidadoresInformeSocial.stream()
            .filter(cnp -> cnp.tieneOrigenTipo(TipoOrigenEnum.PREFERENCIA))
            .collect(Collectors.toList());
  }

  /**
   * Gets the asistentes informe social valoracion.
   *
   * @return the asistentes informe social valoracion
   */
  public List<AsistenteInformeSocialDTO> getAsistentesInformeSocialValoracion() {
    return CollectionUtils.isEmpty(asistentesInformeSocial) ? new ArrayList<>()
        : asistentesInformeSocial.stream()
            .filter(ap -> ap.tieneOrigenTipo(TipoOrigenEnum.VALORACION))
            .collect(Collectors.toList());
  }

  /**
   * Gets the cuidadores con origen.
   *
   * @param origen the origen
   * @return the cuidadores con origen
   */
  public List<CuidadorDTO> getCuidadoresConOrigen(final TipoOrigenEnum origen) {
    return CollectionUtils.isEmpty(cuidadoresInformeSocial) ? new ArrayList<>()
        : cuidadoresInformeSocial.stream()
            .filter(cnp -> cnp.tieneOrigenTipo(origen))
            .map(CuidadorInformeSocialDTO::getCuidador)
            .collect(Collectors.toList());
  }

  /**
   * Gets the asistentes con origen.
   *
   * @param origen the origen
   * @return the asistentes con origen
   */
  public List<AsistentePersonalDTO> getAsistentesConOrigen(
      final TipoOrigenEnum origen) {
    return CollectionUtils.isEmpty(asistentesInformeSocial) ? new ArrayList<>()
        : asistentesInformeSocial.stream()
            .filter(ap -> ap.tieneOrigenTipo(origen))
            .map(AsistenteInformeSocialDTO::getAsistente)
            .collect(Collectors.toList());
  }

  /**
   * Adds the preferencia.
   *
   * @param pref the pref
   * @param tipoPreferencia the valoracion profesional
   */
  public void addPreferencia(final PreferenciaInformeSocialDTO pref,
      final TipoPreferenciaInformeSocialEnum tipoPreferencia) {
    pref.setTipoPreferencia(tipoPreferencia.getValor());
    addPreferencia(pref);
  }

  /**
   * Adds the preferencia.
   *
   * @param pref the pref
   */
  public void addPreferencia(final PreferenciaInformeSocialDTO pref) {
    pref.setInformeSocial(this);
    preferenciasInformeSocial.add(pref);
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

  /**
   * Contiene preferencias tipo.
   *
   * @param codigo the codigo
   * @return true, if successful
   */
  public PreferenciaInformeSocialDTO contienePreferenciasTipo(
      final String codigo) {
    Optional<PreferenciaInformeSocialDTO> findFirst = Optional.empty();
    if (CollectionUtils.isNotEmpty(this.preferenciasInformeSocial)) {
      findFirst = preferenciasInformeSocial.stream()
          .filter(pref -> pref.getCatalogoServicio() != null
              && codigo.equals(pref.getCatalogoServicio().getCodigo()))
          .findFirst();
    }
    return findFirst.isPresent() ? findFirst.get() : null;
  }

  /**
   * Contiene preferencias cuidador.
   *
   * @return true, if successful
   */
  public boolean contienePreferenciasCuidador() {
    return CollectionUtils.isNotEmpty(this.preferenciasInformeSocial)
        && preferenciasInformeSocial.stream()
            .anyMatch(pref -> pref.getCatalogoServicio() != null
                && CatalogoServicioEnum.CNP.getCodigo()
                    .equals(pref.getCatalogoServicio().getCodigo()));
  }

  /**
   * Gets the app origen.
   *
   * @return the app origen
   */
  public String getAppOrigen() {
    return appOrigen;
  }

  /**
   * Sets the app origen.
   *
   * @param appOrigen the new app origen
   */
  public void setAppOrigen(final String appOrigen) {
    this.appOrigen = appOrigen;
  }

  /**
   * Resetear relaciones centro dia.
   */
  public void resetearRelacionesCentroDia() {
    tieneCentroAtencionDiurna = Boolean.FALSE;
    tipoCentroEspecializado = null;
    aportacionUsuarioCentroEspecializado = null;
    apoyoDomicilioHorasSemana = 0L;
    if (centroAtencionDiurna != null) {
      UtilidadesCommons.copyProperties(new CentroDTO(), centroAtencionDiurna);
    }
  }

  /**
   * Resetear relaciones centro residencial.
   */
  public void resetearRelacionesCentroResidencial() {
    tieneCentroResidencial = Boolean.FALSE;
    tipoPlazaResidencia = null;
    centroAportacionUsuario = null;
    if (centroAtencionResidencial != null) {
      UtilidadesCommons.copyProperties(new CentroDTO(),
          centroAtencionResidencial);
    }
  }

  /**
   * Checks if is borrar cuidadores preferencias al guardar.
   *
   * @return true, if is borrar cuidadores preferencias al guardar
   */
  public boolean isBorrarCuidadoresPreferenciasAlGuardar() {
    return borrarCuidadoresPreferenciasAlGuardar;
  }

  /**
   * Sets the borrar cuidadores preferencias al guardar.
   *
   * @param borrarCuidadoresPreferenciasAlGuardar the new borrar cuidadores
   *        preferencias al guardar
   */
  public void setBorrarCuidadoresPreferenciasAlGuardar(
      final boolean borrarCuidadoresPreferenciasAlGuardar) {
    this.borrarCuidadoresPreferenciasAlGuardar =
        borrarCuidadoresPreferenciasAlGuardar;
  }

  /**
   * Gets the recurso idoneo.
   *
   * @return the recurso idoneo
   */
  public Boolean getRecursoIdoneo() {
    return recursoIdoneo;
  }

  /**
   * Sets the recurso idoneo.
   *
   * @param recursoIdoneo the new recurso idoneo
   */
  public void setRecursoIdoneo(final Boolean recursoIdoneo) {
    this.recursoIdoneo = recursoIdoneo;
  }


  /**
   * Gets the anamnesis.
   *
   * @return the anamnesis
   */
  public String getAnamnesis() {
    return StringUtils.trim(anamnesis);
  }

  /**
   * Sets the anamnesis.
   *
   * @param anamnesis the new anamnesis
   */
  public void setAnamnesis(final String anamnesis) {
    this.anamnesis = StringUtils.trim(anamnesis);
  }

  /**
   * Gets the tiene vivienda.
   *
   * @return the tiene vivienda
   */
  public Boolean getTieneVivienda() {
    return tieneVivienda;
  }

  /**
   * Sets the tiene vivienda.
   *
   * @param tieneVivienda the new tiene vivienda
   */
  public void setTieneVivienda(final Boolean tieneVivienda) {
    this.tieneVivienda = tieneVivienda;
  }

  /**
   * Gets the nueva version.
   *
   * @return the nueva version
   */
  public Boolean getNuevaVersion() {
    return ConstantesWeb.DEFAULT_VERSION_INFSOCIAL.equals(version);
  }

  /**
   * Gets the conservacion.
   *
   * @return the conservacion
   */
  public Boolean getConservacion() {
    return conservacion;
  }

  /**
   * Sets the conservacion.
   *
   * @param conservacion the new conservacion
   */
  public void setConservacion(final Boolean conservacion) {
    this.conservacion = conservacion;
  }

  /**
   * Gets the habitabilidad.
   *
   * @return the habitabilidad
   */
  public Boolean getHabitabilidad() {
    return habitabilidad;
  }

  /**
   * Sets the habitabilidad.
   *
   * @param habitabilidad the new habitabilidad
   */
  public void setHabitabilidad(final Boolean habitabilidad) {
    this.habitabilidad = habitabilidad;
  }

  /**
   * Gets the salubridad.
   *
   * @return the salubridad
   */
  public Boolean getSalubridad() {
    return salubridad;
  }

  /**
   * Sets the salubridad.
   *
   * @param salubridad the new salubridad
   */
  public void setSalubridad(final Boolean salubridad) {
    this.salubridad = salubridad;
  }

  /**
   * Gets the adaptacion hogar actual.
   *
   * @return the adaptacion hogar actual
   */
  public String getAdaptacionHogarActual() {
    return adaptacionHogarActual;
  }

  /**
   * Sets the adaptacion hogar actual.
   *
   * @param adaptacionHogarActual the new adaptacion hogar actual
   */
  public void setAdaptacionHogarActual(final String adaptacionHogarActual) {
    this.adaptacionHogarActual = adaptacionHogarActual;
  }

  /**
   * Gets the situacion socio familiar.
   *
   * @return the situacion socio familiar
   */
  public String getSituacionSocioFamiliar() {
    return situacionSocioFamiliar;
  }

  /**
   * Sets the situacion socio familiar.
   *
   * @param situacionSocioFamiliar the new situacion socio familiar
   */
  public void setSituacionSocioFamiliar(final String situacionSocioFamiliar) {
    this.situacionSocioFamiliar = situacionSocioFamiliar;
  }

  /**
   * Gets the riesgo claudica.
   *
   * @return the riesgo claudica
   */
  public Boolean getRiesgoClaudica() {
    return riesgoClaudica;
  }

  /**
   * Sets the riesgo claudica.
   *
   * @param riesgoClaudica the new riesgo claudica
   */
  public void setRiesgoClaudica(final Boolean riesgoClaudica) {
    this.riesgoClaudica = riesgoClaudica;
  }

  /**
   * Gets the abandono.
   *
   * @return the abandono
   */
  public Boolean getAbandono() {
    return abandono;
  }

  /**
   * Sets the abandono.
   *
   * @param abandono the new abandono
   */
  public void setAbandono(final Boolean abandono) {
    this.abandono = abandono;
  }

  /**
   * Gets the version.
   *
   * @return the version
   */
  public Long getVersion() {
    return version;
  }

  /**
   * Sets the version.
   *
   * @param version the new version
   */
  public void setVersion(final Long version) {
    this.version = version;
  }

  /**
   * Gets the centro trabajador social.
   *
   * @return the centro trabajador social
   */
  public String getCentroTrabajadorSocial() {
    return centroTrabajadorSocial;
  }

  /**
   * Sets the centro trabajador social.
   *
   * @param centroTrabajadorSocial the new centro trabajador social
   */
  public void setCentroTrabajadorSocial(final String centroTrabajadorSocial) {
    this.centroTrabajadorSocial = centroTrabajadorSocial;
  }

  /**
   * Gets the ayuda comunicacion.
   *
   * @return the ayuda comunicacion
   */
  public Boolean getAyudaComunicacion() {
    return ayudaComunicacion;
  }

  /**
   * Sets the ayuda comunicacion.
   *
   * @param ayudaComunicacion the new ayuda comunicacion
   */
  public void setAyudaComunicacion(final Boolean ayudaComunicacion) {
    this.ayudaComunicacion = ayudaComunicacion;
  }

  /**
   * Gets the ayuda desplazamiento.
   *
   * @return the ayuda desplazamiento
   */
  public Boolean getAyudaDesplazamiento() {
    return ayudaDesplazamiento;
  }

  /**
   * Sets the ayuda desplazamiento.
   *
   * @param ayudaDesplazamiento the new ayuda desplazamiento
   */
  public void setAyudaDesplazamiento(final Boolean ayudaDesplazamiento) {
    this.ayudaDesplazamiento = ayudaDesplazamiento;
  }

  /**
   * Gets the ayuda acceso recursos.
   *
   * @return the ayuda acceso recursos
   */
  public Boolean getAyudaAccesoRecursos() {
    return ayudaAccesoRecursos;
  }

  /**
   * Sets the ayuda acceso recursos.
   *
   * @param ayudaAccesoRecursos the new ayuda acceso recursos
   */
  public void setAyudaAccesoRecursos(final Boolean ayudaAccesoRecursos) {
    this.ayudaAccesoRecursos = ayudaAccesoRecursos;
  }

  /**
   * Gets the realiza formacion.
   *
   * @return the realiza formacion
   */
  public Boolean getRealizaFormacion() {
    return realizaFormacion;
  }

  /**
   * Sets the realiza formacion.
   *
   * @param realizaFormacion the new realiza formacion
   */
  public void setRealizaFormacion(final Boolean realizaFormacion) {
    this.realizaFormacion = realizaFormacion;
  }

  /**
   * Gets the nombre formacion.
   *
   * @return the nombre formacion
   */
  public String getNombreFormacion() {
    return nombreFormacion;
  }

  /**
   * Sets the nombre formacion.
   *
   * @param nombreFormacion the new nombre formacion
   */
  public void setNombreFormacion(final String nombreFormacion) {
    this.nombreFormacion = nombreFormacion;
  }

  /**
   * Gets the formacion ayuda AP.
   *
   * @return the formacion ayuda AP
   */
  public Boolean getFormacionAyudaAP() {
    return formacionAyudaAP;
  }

  /**
   * Sets the formacion ayuda AP.
   *
   * @param formacionAyudaAP the new formacion ayuda AP
   */
  public void setFormacionAyudaAP(final Boolean formacionAyudaAP) {
    this.formacionAyudaAP = formacionAyudaAP;
  }

  /**
   * Gets the realiza actividad.
   *
   * @return the realiza actividad
   */
  public Boolean getRealizaActividad() {
    return realizaActividad;
  }

  /**
   * Sets the realiza actividad.
   *
   * @param realizaActividad the new realiza actividad
   */
  public void setRealizaActividad(final Boolean realizaActividad) {
    this.realizaActividad = realizaActividad;
  }

  /**
   * Gets the tipo actividad.
   *
   * @return the tipo actividad
   */
  public String getTipoActividad() {
    return tipoActividad;
  }

  /**
   * Sets the tipo actividad.
   *
   * @param tipoActividad the new tipo actividad
   */
  public void setTipoActividad(final String tipoActividad) {
    this.tipoActividad = tipoActividad;
  }

  /**
   * Gets the nombre entidad actividad.
   *
   * @return the nombre entidad actividad
   */
  public String getNombreEntidadActividad() {
    return nombreEntidadActividad;
  }

  /**
   * Sets the nombre entidad actividad.
   *
   * @param nombreEntidadActividad the new nombre entidad actividad
   */
  public void setNombreEntidadActividad(final String nombreEntidadActividad) {
    this.nombreEntidadActividad = nombreEntidadActividad;
  }

  /**
   * Gets the actividad ayuda AP.
   *
   * @return the actividad ayuda AP
   */
  public Boolean getActividadAyudaAP() {
    return actividadAyudaAP;
  }

  /**
   * Sets the actividad ayuda AP.
   *
   * @param actividadAyudaAP the new actividad ayuda AP
   */
  public void setActividadAyudaAP(final Boolean actividadAyudaAP) {
    this.actividadAyudaAP = actividadAyudaAP;
  }

  /**
   * Gets the proyecto adecuado.
   *
   * @return the proyecto adecuado
   */
  public Boolean getProyectoAdecuado() {
    return proyectoAdecuado;
  }

  /**
   * Sets the proyecto adecuado.
   *
   * @param proyectoAdecuado the new proyecto adecuado
   */
  public void setProyectoAdecuado(final Boolean proyectoAdecuado) {
    this.proyectoAdecuado = proyectoAdecuado;
  }

  /**
   * Gets the otro recurso.
   *
   * @return the otro recurso
   */
  public Boolean getOtroRecurso() {
    return otroRecurso;
  }

  /**
   * Sets the otro recurso.
   *
   * @param otroRecurso the new otro recurso
   */
  public void setOtroRecurso(final Boolean otroRecurso) {
    this.otroRecurso = otroRecurso;
  }

  /**
   * Gets the otro recurso motivos.
   *
   * @return the otro recurso motivos
   */
  public String getOtroRecursoMotivos() {
    return otroRecursoMotivos;
  }

  /**
   * Sets the otro recurso motivos.
   *
   * @param otroRecursoMotivos the new otro recurso motivos
   */
  public void setOtroRecursoMotivos(final String otroRecursoMotivos) {
    this.otroRecursoMotivos = otroRecursoMotivos;
  }

  /**
   * Gets the tipo persona AP fisica.
   *
   * @return the tipo persona AP fisica
   */
  public Boolean getTipoPersonaAPFisica() {
    return tipoPersonaAPFisica;
  }

  /**
   * Sets the tipo persona AP fisica.
   *
   * @param tipoPersonaAPFisica the new tipo persona AP fisica
   */
  public void setTipoPersonaAPFisica(final Boolean tipoPersonaAPFisica) {
    this.tipoPersonaAPFisica = tipoPersonaAPFisica;
  }

  /**
   * Gets the tipo persona AP juridica.
   *
   * @return the tipo persona AP juridica
   */
  public Boolean getTipoPersonaAPJuridica() {
    return tipoPersonaAPJuridica;
  }

  /**
   * Sets the tipo persona AP juridica.
   *
   * @param tipoPersonaAPJuridica the new tipo persona AP juridica
   */
  public void setTipoPersonaAPJuridica(final Boolean tipoPersonaAPJuridica) {
    this.tipoPersonaAPJuridica = tipoPersonaAPJuridica;
  }

  /**
   * Gets the horario lectivo AP.
   *
   * @return the horario lectivo AP
   */
  public Boolean getHorarioLectivoAP() {
    return horarioLectivoAP;
  }

  /**
   * Sets the horario lectivo AP.
   *
   * @param horarioLectivoAP the new horario lectivo AP
   */
  public void setHorarioLectivoAP(final Boolean horarioLectivoAP) {
    this.horarioLectivoAP = horarioLectivoAP;
  }

  /**
   * Gets the asistentes informe social.
   *
   * @return the asistentes informe social
   */

  public List<AsistenteInformeSocialDTO> getAsistentesInformeSocial() {
    return UtilidadesCommons.collectorsToList(asistentesInformeSocial);
  }

  /**
   * Sets the asistentes informe social.
   *
   * @param asistentesInformeSocial the new asistentes informe social
   */

  public void setAsistentesInformeSocial(
      final List<AsistenteInformeSocialDTO> asistentesInformeSocial) {
    this.asistentesInformeSocial =
        UtilidadesCommons.collectorsToList(asistentesInformeSocial);
  }

  /**
   * Gets the motivo no idoneidad.
   *
   * @return the motivo no idoneidad
   */
  public String getMotivoNoIdoneidad() {
    return motivoNoIdoneidad;
  }

  /**
   * Sets the motivo no idoneidad.
   *
   * @param motivoNoIdoneidad the new motivo no idoneidad
   */
  public void setMotivoNoIdoneidad(final String motivoNoIdoneidad) {
    this.motivoNoIdoneidad = motivoNoIdoneidad;
  }

  /**
   * Gets the mantener preferencia 1.
   *
   * @return the mantener preferencia 1
   */
  public Boolean getMantenerPreferencia1() {
    return mantenerPreferencia1;
  }

  /**
   * Sets the mantener preferencia 1.
   *
   * @param mantenerPreferencia1 the new mantener preferencia 1
   */
  public void setMantenerPreferencia1(final Boolean mantenerPreferencia1) {
    this.mantenerPreferencia1 = mantenerPreferencia1;
  }

  /**
   * Gets the mantener preferencia 2.
   *
   * @return the mantener preferencia 2
   */
  public Boolean getMantenerPreferencia2() {
    return mantenerPreferencia2;
  }

  /**
   * Sets the mantener preferencia 2.
   *
   * @param mantenerPreferencia2 the new mantener preferencia 2
   */
  public void setMantenerPreferencia2(final Boolean mantenerPreferencia2) {
    this.mantenerPreferencia2 = mantenerPreferencia2;
  }

  /**
   * Gets the trabajo ayuda AP.
   *
   * @return the trabajo ayuda AP
   */
  public Boolean getTrabajoAyudaAP() {
    return trabajoAyudaAP;
  }

  /**
   * Sets the trabajo ayuda AP.
   *
   * @param trabajoAyudaAP the new trabajo ayuda AP
   */
  public void setTrabajoAyudaAP(final Boolean trabajoAyudaAP) {
    this.trabajoAyudaAP = trabajoAyudaAP;
  }

  /**
   * Gets the mantener teleasistencia.
   *
   * @return the mantener teleasistencia
   */
  public Boolean getMantenerTeleasistencia() {
    return mantenerTeleasistencia;
  }

  /**
   * Sets the mantener teleasistencia.
   *
   * @param mantenerTeleasistencia the new mantener teleasistencia
   */
  public void setMantenerTeleasistencia(final Boolean mantenerTeleasistencia) {
    this.mantenerTeleasistencia = mantenerTeleasistencia;
  }

  /**
   * Gets the tipo persona asispers.
   *
   * @return the tipo persona asispers
   */
  public Boolean getTipoAsispers() {
    return tipoAsispers;
  }

  /**
   * Sets the tipo persona asispers.
   *
   * @param tipoAsispers the new tipo asispers
   */
  public void setTipoAsispers(final Boolean tipoAsispers) {
    this.tipoAsispers = tipoAsispers;
  }

  /**
   * Gets the propietario informe social.
   *
   * @return the propietario informe social
   */
  public String getPropietarioInformeSocial() {
    return getNombreCompletoUsuarioAsignado();
  }

  /**
   * Gets the fecha grabacion.
   *
   * @return the fecha grabacion
   */
  public Date getFechaGrabacion() {
    return fechaGrabacion;
  }

  /**
   * Sets the fecha grabacion.
   *
   * @param fechaGrabacion the new fecha grabacion
   */
  public void setFechaGrabacion(final Date fechaGrabacion) {
    this.fechaGrabacion = fechaGrabacion;
  }

  /**
   * Gets the otro recurso seleccionado.
   *
   * @return the otro recurso seleccionado
   */
  public CatalogoServicioDTO getOtroRecursoSeleccionado() {
    return otroRecursoSeleccionado;
  }

  /**
   * Sets the otro recurso seleccionado.
   *
   * @param otroRecursoSeleccionado the new otro recurso seleccionado
   */
  public void setOtroRecursoSeleccionado(
      final CatalogoServicioDTO otroRecursoSeleccionado) {
    this.otroRecursoSeleccionado = otroRecursoSeleccionado;
  }

  /**
   * Gets the aumento horas.
   *
   * @return the aumentoHoras
   */
  public Boolean getAumentoHoras() {
    return aumentoHoras;
  }

  /**
   * Sets the aumento horas.
   *
   * @param aumentoHoras the aumentoHoras to set
   */
  public void setAumentoHoras(final Boolean aumentoHoras) {
    this.aumentoHoras = aumentoHoras;
  }

}
