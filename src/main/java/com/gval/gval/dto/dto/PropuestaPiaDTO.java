package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;
import java.util.Date;

/**
 * The Class PropuestaPiaDTO.
 */
public class PropuestaPiaDTO extends BaseDTO
    implements Comparable<PropuestaPiaDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk proppia. */
  private long pkProppia;

  /** The comunicado por. */
  private Integer cobroComplementosComunicadoPor;

  /** The es revision confirmatoria nivel 20. */
  private Boolean esRevisionConfirmatoriaNivel20;

  /** The fecha escrito revision. */
  private Date fechaEscritoRevision;

  /** The migrado sidep. */
  @Length(max = 1)
  private String migradoSidep;

  /** The importe tutelados. */
  private BigDecimal importeTutelados;

  /** The activo. */
  private boolean activo;

  /** The acuerdo inicio. */
  private boolean acuerdoInicio;

  /** The alegada. */
  private Boolean alegada;

  /** The anual PEI. */
  private BigDecimal anualPEI;

  /** The tipo prestador AP. */
  @Length(max = 50)
  private String tipoPrestadorAP;

  /** The caducada. */
  private boolean caducada;

  /** The capacidad economica. */
  private BigDecimal capacidadEconomica;

  /** The codigo centro. */
  @Length(max = 6)
  private String codigoCentro;

  /** The coincide preferencia solicitante. */
  private Boolean coincidePreferenciaSolicitante;

  /** The Coincide preferencia trabajador social. */
  private Boolean coincidePreferenciaTrabajadorSocial;

  /** The copago. */
  private BigDecimal copago;

  /** The coste servicio. */
  private BigDecimal costeServicio;

  /** The estado datos economicos prestaciones. */
  @Length(max = 1)
  private String estadoDatosEconomicosPrestaciones;

  /** The estado datos economicos servicios. */
  @Length(max = 1)
  private String estadoDatosEconomicosServicios;

  /** The detalle importe. */
  @Length(max = 4000)
  private String detalleImporte;

  /** The discapacidad fisica. */
  private Boolean discapacidadFisica;

  /** The discapacidad psiquica. */
  private Boolean discapacidadPsiquica;

  /** The discapacidad sensorial. */
  private Boolean discapacidadSensorial;

  /** The enfermedad mental. */
  private Boolean enfermedadMental;

  /** The entidad excluida. */
  private Boolean entidadExcluida;

  /** The prestado. */
  @Length(max = 2)
  private String estado;

  /** The fecha propuesta. */
  private Date fechaPropuesta;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The fecha modificacion numero pagas. */
  private Date fechaModificacionNumeroPagas;

  /** The fecha notificacion. */
  private Date fechaNotificacion;

  /** The horas cuidados. */
  private Integer horasCuidados;

  /** The horas hogar. */
  private Integer horasHogar;

  /** The horas prestacion. */
  private Integer horasPrestacion;

  /** The importe adicional. */
  private BigDecimal importeAdicional;

  /** The coste pvs residencia. */
  private BigDecimal costePvsResidencia;

  /** The importe prestacion. */
  private BigDecimal importePrestacion;

  /** The importe PEI. */
  private BigDecimal importePEI;

  /** The ingresado siintegra. */
  private boolean ingresadoSiintegra;

  /** The ingresado simetta. */
  private boolean ingresadoSimetta;

  /** The numero pagas. */
  private Integer numeroPagas;

  /** The observaciones propuesta. */
  @Length(max = 250)
  private String observacionesPropuesta;

  /** The tipo revision. */
  private Boolean tipoRevision;

  /** The pluripatologia. */
  private Boolean pluripatologia;

  /** The sector. */
  @Length(max = 1)
  private String sector;

  /** The cambio sector excepcional. */
  private boolean cambioSectorExcepcional;

  /** The transporte. */
  private Boolean transporte;

  /** The valida. */
  private boolean valida;

  /** The documento propuesta pia. */
  private DocumentoGeneradoDTO documentoPropuestaPia;

  /** The pk tipo asispers. */
  // TODO: Mapear cuando se cree la entidad
  private Long pkTipoAsispers;

  /** The pk motivo. */
  // TODO: Mapear cuando se cree la entidad
  private Long pkMotivo;

  /** The pk pia. */
  // TODO: Mapear cuando se cree la entidad
  private Long pkPia;

  /** The resolucion revisada. */
  private ResolucionDTO resolucionRevisada;

  /** The solicitud. */
  private SolicitudDTO solicitud;

  /** The ejercicio. */
  private EjercicioDTO ejercicio;

  /** The usuario. */
  private UsuarioDTO usuario;

  /**
   * Sdm proppia.
   */
  public PropuestaPiaDTO() {
    // empty constructor
  }

  /**
   * Gets the pk proppia.
   *
   * @return the pk proppia
   */
  public long getPkProppia() {
    return pkProppia;
  }

  /**
   * Sets the pk proppia.
   *
   * @param pkProppia the new pk proppia
   */
  public void setPkProppia(final long pkProppia) {
    this.pkProppia = pkProppia;
  }

  /**
   * Gets the cobro complementos comunicado por.
   *
   * @return the cobro complementos comunicado por
   */
  public Integer getCobroComplementosComunicadoPor() {
    return cobroComplementosComunicadoPor;
  }

  /**
   * Sets the cobro complementos comunicado por.
   *
   * @param cobroComplementosComunicadoPor the new cobro complementos comunicado
   *        por
   */
  public void setCobroComplementosComunicadoPor(
      final Integer cobroComplementosComunicadoPor) {
    this.cobroComplementosComunicadoPor = cobroComplementosComunicadoPor;
  }

  /**
   * Gets the es revision confirmatoria nivel 20.
   *
   * @return the es revision confirmatoria nivel 20
   */
  public Boolean getEsRevisionConfirmatoriaNivel20() {
    return esRevisionConfirmatoriaNivel20;
  }

  /**
   * Sets the es revision confirmatoria nivel 20.
   *
   * @param esRevisionConfirmatoriaNivel20 the new es revision confirmatoria
   *        nivel 20
   */
  public void setEsRevisionConfirmatoriaNivel20(
      final Boolean esRevisionConfirmatoriaNivel20) {
    this.esRevisionConfirmatoriaNivel20 = esRevisionConfirmatoriaNivel20;
  }

  /**
   * Gets the fecha escrito revision.
   *
   * @return the fecha escrito revision
   */
  public Date getFechaEscritoRevision() {
    return UtilidadesCommons.cloneDate(fechaEscritoRevision);
  }

  /**
   * Sets the fecha escrito revision.
   *
   * @param fechaEscritoRevision the new fecha escrito revision
   */
  public void setFechaEscritoRevision(final Date fechaEscritoRevision) {
    this.fechaEscritoRevision =
        UtilidadesCommons.cloneDate(fechaEscritoRevision);
  }

  /**
   * Gets the migrado sidep.
   *
   * @return the migrado sidep
   */
  public String getMigradoSidep() {
    return migradoSidep;
  }

  /**
   * Sets the migrado sidep.
   *
   * @param migradoSidep the new migrado sidep
   */
  public void setMigradoSidep(final String migradoSidep) {
    this.migradoSidep = migradoSidep;
  }

  /**
   * Gets the importe tutelados.
   *
   * @return the importe tutelados
   */
  public BigDecimal getImporteTutelados() {
    return importeTutelados;
  }

  /**
   * Sets the importe tutelados.
   *
   * @param importeTutelados the new importe tutelados
   */
  public void setImporteTutelados(final BigDecimal importeTutelados) {
    this.importeTutelados = importeTutelados;
  }

  /**
   * Checks if is activo.
   *
   * @return true, if is activo
   */
  public boolean isActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final boolean activo) {
    this.activo = activo;
  }

  /**
   * Checks if is acuerdo inicio.
   *
   * @return true, if is acuerdo inicio
   */
  public boolean isAcuerdoInicio() {
    return acuerdoInicio;
  }

  /**
   * Sets the acuerdo inicio.
   *
   * @param acuerdoInicio the new acuerdo inicio
   */
  public void setAcuerdoInicio(final boolean acuerdoInicio) {
    this.acuerdoInicio = acuerdoInicio;
  }

  /**
   * Gets the alegada.
   *
   * @return the alegada
   */
  public Boolean getAlegada() {
    return alegada;
  }

  /**
   * Sets the alegada.
   *
   * @param alegada the new alegada
   */
  public void setAlegada(final Boolean alegada) {
    this.alegada = alegada;
  }

  /**
   * Gets the anual PEI.
   *
   * @return the anual PEI
   */
  public BigDecimal getAnualPEI() {
    return anualPEI;
  }

  /**
   * Sets the anual PEI.
   *
   * @param anualPEI the new anual PEI
   */
  public void setAnualPEI(final BigDecimal anualPEI) {
    this.anualPEI = anualPEI;
  }

  /**
   * Gets the tipo prestador AP.
   *
   * @return the tipo prestador AP
   */
  public String getTipoPrestadorAP() {
    return tipoPrestadorAP;
  }

  /**
   * Sets the tipo prestador AP.
   *
   * @param tipoPrestadorAP the new tipo prestador AP
   */
  public void setTipoPrestadorAP(final String tipoPrestadorAP) {
    this.tipoPrestadorAP = tipoPrestadorAP;
  }

  /**
   * Checks if is caducada.
   *
   * @return true, if is caducada
   */
  public boolean isCaducada() {
    return caducada;
  }

  /**
   * Sets the caducada.
   *
   * @param caducada the new caducada
   */
  public void setCaducada(final boolean caducada) {
    this.caducada = caducada;
  }

  /**
   * Gets the capacidad economica.
   *
   * @return the capacidad economica
   */
  public BigDecimal getCapacidadEconomica() {
    return capacidadEconomica;
  }

  /**
   * Sets the capacidad economica.
   *
   * @param capacidadEconomica the new capacidad economica
   */
  public void setCapacidadEconomica(final BigDecimal capacidadEconomica) {
    this.capacidadEconomica = capacidadEconomica;
  }

  /**
   * Gets the codigo centro.
   *
   * @return the codigo centro
   */
  public String getCodigoCentro() {
    return codigoCentro;
  }

  /**
   * Sets the codigo centro.
   *
   * @param codigoCentro the new codigo centro
   */
  public void setCodigoCentro(final String codigoCentro) {
    this.codigoCentro = codigoCentro;
  }

  /**
   * Gets the coincide preferencia solicitante.
   *
   * @return the coincide preferencia solicitante
   */
  public Boolean getCoincidePreferenciaSolicitante() {
    return coincidePreferenciaSolicitante;
  }

  /**
   * Sets the coincide preferencia solicitante.
   *
   * @param coincidePreferenciaSolicitante the new coincide preferencia
   *        solicitante
   */
  public void setCoincidePreferenciaSolicitante(
      final Boolean coincidePreferenciaSolicitante) {
    this.coincidePreferenciaSolicitante = coincidePreferenciaSolicitante;
  }

  /**
   * Gets the coincide preferencia trabajador social.
   *
   * @return the coincide preferencia trabajador social
   */
  public Boolean getCoincidePreferenciaTrabajadorSocial() {
    return coincidePreferenciaTrabajadorSocial;
  }

  /**
   * Sets the coincide preferencia trabajador social.
   *
   * @param coincidePreferenciaTrabajadorSocial the new coincide preferencia
   *        trabajador social
   */
  public void setCoincidePreferenciaTrabajadorSocial(
      final Boolean coincidePreferenciaTrabajadorSocial) {
    this.coincidePreferenciaTrabajadorSocial =
        coincidePreferenciaTrabajadorSocial;
  }

  /**
   * Gets the copago.
   *
   * @return the copago
   */
  public BigDecimal getCopago() {
    return copago;
  }

  /**
   * Sets the copago.
   *
   * @param copago the new copago
   */
  public void setCopago(final BigDecimal copago) {
    this.copago = copago;
  }

  /**
   * Gets the coste servicio.
   *
   * @return the coste servicio
   */
  public BigDecimal getCosteServicio() {
    return costeServicio;
  }

  /**
   * Sets the coste servicio.
   *
   * @param costeServicio the new coste servicio
   */
  public void setCosteServicio(final BigDecimal costeServicio) {
    this.costeServicio = costeServicio;
  }

  /**
   * Gets the estado datos economicos prestaciones.
   *
   * @return the estado datos economicos prestaciones
   */
  public String getEstadoDatosEconomicosPrestaciones() {
    return estadoDatosEconomicosPrestaciones;
  }

  /**
   * Sets the estado datos economicos prestaciones.
   *
   * @param estadoDatosEconomicosPrestaciones the new estado datos economicos
   *        prestaciones
   */
  public void setEstadoDatosEconomicosPrestaciones(
      final String estadoDatosEconomicosPrestaciones) {
    this.estadoDatosEconomicosPrestaciones = estadoDatosEconomicosPrestaciones;
  }

  /**
   * Gets the estado datos economicos servicios.
   *
   * @return the estado datos economicos servicios
   */
  public String getEstadoDatosEconomicosServicios() {
    return estadoDatosEconomicosServicios;
  }

  /**
   * Sets the estado datos economicos servicios.
   *
   * @param estadoDatosEconomicosServicios the new estado datos economicos
   *        servicios
   */
  public void setEstadoDatosEconomicosServicios(
      final String estadoDatosEconomicosServicios) {
    this.estadoDatosEconomicosServicios = estadoDatosEconomicosServicios;
  }

  /**
   * Gets the detalle importe.
   *
   * @return the detalle importe
   */
  public String getDetalleImporte() {
    return detalleImporte;
  }

  /**
   * Sets the detalle importe.
   *
   * @param detalleImporte the new detalle importe
   */
  public void setDetalleImporte(final String detalleImporte) {
    this.detalleImporte = detalleImporte;
  }

  /**
   * Gets the discapacidad fisica.
   *
   * @return the discapacidad fisica
   */
  public Boolean getDiscapacidadFisica() {
    return discapacidadFisica;
  }

  /**
   * Sets the discapacidad fisica.
   *
   * @param discapacidadFisica the new discapacidad fisica
   */
  public void setDiscapacidadFisica(final Boolean discapacidadFisica) {
    this.discapacidadFisica = discapacidadFisica;
  }

  /**
   * Gets the discapacidad psiquica.
   *
   * @return the discapacidad psiquica
   */
  public Boolean getDiscapacidadPsiquica() {
    return discapacidadPsiquica;
  }

  /**
   * Sets the discapacidad psiquica.
   *
   * @param discapacidadPsiquica the new discapacidad psiquica
   */
  public void setDiscapacidadPsiquica(final Boolean discapacidadPsiquica) {
    this.discapacidadPsiquica = discapacidadPsiquica;
  }

  /**
   * Gets the discapacidad sensorial.
   *
   * @return the discapacidad sensorial
   */
  public Boolean getDiscapacidadSensorial() {
    return discapacidadSensorial;
  }

  /**
   * Sets the discapacidad sensorial.
   *
   * @param discapacidadSensorial the new discapacidad sensorial
   */
  public void setDiscapacidadSensorial(final Boolean discapacidadSensorial) {
    this.discapacidadSensorial = discapacidadSensorial;
  }

  /**
   * Gets the enfermedad mental.
   *
   * @return the enfermedad mental
   */
  public Boolean getEnfermedadMental() {
    return enfermedadMental;
  }

  /**
   * Sets the enfermedad mental.
   *
   * @param enfermedadMental the new enfermedad mental
   */
  public void setEnfermedadMental(final Boolean enfermedadMental) {
    this.enfermedadMental = enfermedadMental;
  }

  /**
   * Gets the entidad excluida.
   *
   * @return the entidad excluida
   */
  public Boolean getEntidadExcluida() {
    return entidadExcluida;
  }

  /**
   * Sets the entidad excluida.
   *
   * @param entidadExcluida the new entidad excluida
   */
  public void setEntidadExcluida(final Boolean entidadExcluida) {
    this.entidadExcluida = entidadExcluida;
  }


  /**
   * Gets the estado.
   *
   * @return the estado
   */
  public String getEstado() {
    return estado;
  }

  /**
   * Sets the estado.
   *
   * @param estado the new estado
   */
  public void setEstado(String estado) {
    this.estado = estado;
  }

  /**
   * Gets the fecha propuesta.
   *
   * @return the fecha propuesta
   */
  public Date getFechaPropuesta() {
    return UtilidadesCommons.cloneDate(fechaPropuesta);
  }

  /**
   * Sets the fecha propuesta.
   *
   * @param fechaPropuesta the new fecha propuesta
   */
  public void setFechaPropuesta(final Date fechaPropuesta) {
    this.fechaPropuesta = UtilidadesCommons.cloneDate(fechaPropuesta);
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha modificacion numero pagas.
   *
   * @return the fecha modificacion numero pagas
   */
  public Date getFechaModificacionNumeroPagas() {
    return UtilidadesCommons.cloneDate(fechaModificacionNumeroPagas);
  }

  /**
   * Sets the fecha modificacion numero pagas.
   *
   * @param fechaModificacionNumeroPagas the new fecha modificacion numero pagas
   */
  public void setFechaModificacionNumeroPagas(
      final Date fechaModificacionNumeroPagas) {
    this.fechaModificacionNumeroPagas =
        UtilidadesCommons.cloneDate(fechaModificacionNumeroPagas);
  }

  /**
   * Gets the fecha notificacion.
   *
   * @return the fecha notificacion
   */
  public Date getFechaNotificacion() {
    return UtilidadesCommons.cloneDate(fechaNotificacion);
  }

  /**
   * Sets the fecha notificacion.
   *
   * @param fechaNotificacion the new fecha notificacion
   */
  public void setFechaNotificacion(final Date fechaNotificacion) {
    this.fechaNotificacion = UtilidadesCommons.cloneDate(fechaNotificacion);
  }

  /**
   * Gets the horas cuidados.
   *
   * @return the horas cuidados
   */
  public Integer getHorasCuidados() {
    return horasCuidados;
  }

  /**
   * Sets the horas cuidados.
   *
   * @param horasCuidados the new horas cuidados
   */
  public void setHorasCuidados(final Integer horasCuidados) {
    this.horasCuidados = horasCuidados;
  }

  /**
   * Gets the horas hogar.
   *
   * @return the horas hogar
   */
  public Integer getHorasHogar() {
    return horasHogar;
  }

  /**
   * Sets the horas hogar.
   *
   * @param horasHogar the new horas hogar
   */
  public void setHorasHogar(final Integer horasHogar) {
    this.horasHogar = horasHogar;
  }

  /**
   * Gets the horas prestacion.
   *
   * @return the horas prestacion
   */
  public Integer getHorasPrestacion() {
    return horasPrestacion;
  }

  /**
   * Sets the horas prestacion.
   *
   * @param horasPrestacion the new horas prestacion
   */
  public void setHorasPrestacion(final Integer horasPrestacion) {
    this.horasPrestacion = horasPrestacion;
  }

  /**
   * Gets the importe adicional.
   *
   * @return the importe adicional
   */
  public BigDecimal getImporteAdicional() {
    return importeAdicional;
  }

  /**
   * Sets the importe adicional.
   *
   * @param importeAdicional the new importe adicional
   */
  public void setImporteAdicional(final BigDecimal importeAdicional) {
    this.importeAdicional = importeAdicional;
  }

  /**
   * Gets the coste pvs residencia.
   *
   * @return the coste pvs residencia
   */
  public BigDecimal getCostePvsResidencia() {
    return costePvsResidencia;
  }

  /**
   * Sets the coste pvs residencia.
   *
   * @param costePvsResidencia the new coste pvs residencia
   */
  public void setCostePvsResidencia(final BigDecimal costePvsResidencia) {
    this.costePvsResidencia = costePvsResidencia;
  }

  /**
   * Gets the importe prestacion.
   *
   * @return the importe prestacion
   */
  public BigDecimal getImportePrestacion() {
    return importePrestacion;
  }

  /**
   * Sets the importe prestacion.
   *
   * @param importePrestacion the new importe prestacion
   */
  public void setImportePrestacion(final BigDecimal importePrestacion) {
    this.importePrestacion = importePrestacion;
  }

  /**
   * Gets the importe PEI.
   *
   * @return the importe PEI
   */
  public BigDecimal getImportePEI() {
    return importePEI;
  }

  /**
   * Sets the importe PEI.
   *
   * @param importePEI the new importe PEI
   */
  public void setImportePEI(final BigDecimal importePEI) {
    this.importePEI = importePEI;
  }

  /**
   * Checks if is ingresado siintegra.
   *
   * @return true, if is ingresado siintegra
   */
  public boolean isIngresadoSiintegra() {
    return ingresadoSiintegra;
  }

  /**
   * Sets the ingresado siintegra.
   *
   * @param ingresadoSiintegra the new ingresado siintegra
   */
  public void setIngresadoSiintegra(final boolean ingresadoSiintegra) {
    this.ingresadoSiintegra = ingresadoSiintegra;
  }

  /**
   * Checks if is ingresado simetta.
   *
   * @return true, if is ingresado simetta
   */
  public boolean isIngresadoSimetta() {
    return ingresadoSimetta;
  }

  /**
   * Sets the ingresado simetta.
   *
   * @param ingresadoSimetta the new ingresado simetta
   */
  public void setIngresadoSimetta(final boolean ingresadoSimetta) {
    this.ingresadoSimetta = ingresadoSimetta;
  }

  /**
   * Gets the numero pagas.
   *
   * @return the numero pagas
   */
  public Integer getNumeroPagas() {
    return numeroPagas;
  }

  /**
   * Sets the numero pagas.
   *
   * @param numeroPagas the new numero pagas
   */
  public void setNumeroPagas(final Integer numeroPagas) {
    this.numeroPagas = numeroPagas;
  }

  /**
   * Gets the observaciones propuesta.
   *
   * @return the observaciones propuesta
   */
  public String getObservacionesPropuesta() {
    return observacionesPropuesta;
  }

  /**
   * Sets the observaciones propuesta.
   *
   * @param observacionesPropuesta the new observaciones propuesta
   */
  public void setObservacionesPropuesta(final String observacionesPropuesta) {
    this.observacionesPropuesta = observacionesPropuesta;
  }

  /**
   * Gets the tipo revision.
   *
   * @return the tipo revision
   */
  public Boolean getTipoRevision() {
    return tipoRevision;
  }

  /**
   * Sets the tipo revision.
   *
   * @param tipoRevision the new tipo revision
   */
  public void setTipoRevision(final Boolean tipoRevision) {
    this.tipoRevision = tipoRevision;
  }

  /**
   * Gets the pluripatologia.
   *
   * @return the pluripatologia
   */
  public Boolean getPluripatologia() {
    return pluripatologia;
  }

  /**
   * Sets the pluripatologia.
   *
   * @param pluripatologia the new pluripatologia
   */
  public void setPluripatologia(final Boolean pluripatologia) {
    this.pluripatologia = pluripatologia;
  }

  /**
   * Gets the sector.
   *
   * @return the sector
   */
  public String getSector() {
    return sector;
  }

  /**
   * Sets the sector.
   *
   * @param sector the new sector
   */
  public void setSector(final String sector) {
    this.sector = sector;
  }

  /**
   * Checks if is cambio sector excepcional.
   *
   * @return true, if is cambio sector excepcional
   */
  public boolean isCambioSectorExcepcional() {
    return cambioSectorExcepcional;
  }

  /**
   * Sets the cambio sector excepcional.
   *
   * @param cambioSectorExcepcional the new cambio sector excepcional
   */
  public void setCambioSectorExcepcional(
      final boolean cambioSectorExcepcional) {
    this.cambioSectorExcepcional = cambioSectorExcepcional;
  }

  /**
   * Gets the transporte.
   *
   * @return the transporte
   */
  public Boolean getTransporte() {
    return transporte;
  }

  /**
   * Sets the transporte.
   *
   * @param transporte the new transporte
   */
  public void setTransporte(final Boolean transporte) {
    this.transporte = transporte;
  }

  /**
   * Checks if is valida.
   *
   * @return true, if is valida
   */
  public boolean isValida() {
    return valida;
  }

  /**
   * Sets the valida.
   *
   * @param valida the new valida
   */
  public void setValida(final boolean valida) {
    this.valida = valida;
  }

  /**
   * Gets the documento propuesta pia.
   *
   * @return the documento propuesta pia
   */
  public DocumentoGeneradoDTO getDocumentoPropuestaPia() {
    return documentoPropuestaPia;
  }

  /**
   * Sets the documento propuesta pia.
   *
   * @param documentoPropuestaPia the new documento propuesta pia
   */
  public void setDocumentoPropuestaPia(
      final DocumentoGeneradoDTO documentoPropuestaPia) {
    this.documentoPropuestaPia = documentoPropuestaPia;
  }

  /**
   * Gets the pk tipo asispers.
   *
   * @return the pk tipo asispers
   */
  public Long getPkTipoAsispers() {
    return pkTipoAsispers;
  }

  /**
   * Sets the pk tipo asispers.
   *
   * @param pkTipoAsispers the new pk tipo asispers
   */
  public void setPkTipoAsispers(final Long pkTipoAsispers) {
    this.pkTipoAsispers = pkTipoAsispers;
  }

  /**
   * Gets the pk motivo.
   *
   * @return the pk motivo
   */
  public Long getPkMotivo() {
    return pkMotivo;
  }

  /**
   * Sets the pk motivo.
   *
   * @param pkMotivo the new pk motivo
   */
  public void setPkMotivo(final Long pkMotivo) {
    this.pkMotivo = pkMotivo;
  }

  /**
   * Gets the pk pia.
   *
   * @return the pk pia
   */
  public Long getPkPia() {
    return pkPia;
  }

  /**
   * Sets the pk pia.
   *
   * @param pkPia the new pk pia
   */
  public void setPkPia(final Long pkPia) {
    this.pkPia = pkPia;
  }

  /**
   * Gets the resolucion revisada.
   *
   * @return the resolucion revisada
   */
  public ResolucionDTO getResolucionRevisada() {
    return resolucionRevisada;
  }

  /**
   * Sets the resolucion revisada.
   *
   * @param resolucionRevisada the new resolucion revisada
   */
  public void setResolucionRevisada(final ResolucionDTO resolucionRevisada) {
    this.resolucionRevisada = resolucionRevisada;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public SolicitudDTO getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the new solicitud
   */
  public void setSolicitud(final SolicitudDTO solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the ejercicio.
   *
   * @return the ejercicio
   */
  public EjercicioDTO getEjercicio() {
    return ejercicio;
  }

  /**
   * Sets the ejercicio.
   *
   * @param ejercicio the new ejercicio
   */
  public void setEjercicio(final EjercicioDTO ejercicio) {
    this.ejercicio = ejercicio;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final PropuestaPiaDTO o) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
