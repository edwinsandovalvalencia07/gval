/*
 *
 */
package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import es.gva.dependencia.ada.common.enums.TipoEstudioEnum;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.zkoss.util.resource.Labels;

import java.util.Date;


// TODO: Auto-generated Javadoc
/**
 * The Class EstudioDTO.
 */
public class EstudioDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -4605804345009620792L;

  /** The pk estudio. */
  private Long pkEstudio;

  /** The activo. */
  private Boolean activo;

  /** The estado. */
  private String estado;

  /** The fecha fin. */
  private Date fechaFin;

  /** The fecha inicio. */
  private Date fechaInicio;

  /** The fecha aprobacion. */
  private Date fechaAprobacion;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The motivo informe salud fisico. */
  private String motivoInformeSaludFisico;

  /** The motivo informe salud mental. */
  private String motivoInformeSaludMental;

  /** The motivo informe salud. */
  private String motivoInformeSalud;

  /** The motivo informe social. */
  private String motivoInformeSocial;

  /** The motivo valoracion. */
  private String motivoValoracion;

  /** The observaciones. */
  private String observaciones;

  /** The otro tipo estudio. */
  private String otroTipoEstudio;

  /** The repeticion informe salud fisico. */
  private Boolean repeticionInformeSaludFisico;

  /** The repeticion informe salud mental. */
  private Boolean repeticionInformeSaludMental;

  /** The repeticion informe salud. */
  private Boolean repeticionInformeSalud;

  /** The repeticion informe social. */
  private Boolean repeticionInformeSocial;

  /** The repeticion informe valoracion. */
  private Boolean repeticionInformeValoracion;

  /** The resultado. */
  private Boolean resultado;

  /** The tipo. */
  private String tipo;

  /** The tipo dictamen. */
  private String tipoDictamen;

  /** The urgente. */
  private Boolean urgente;

  /** The migrado sidep. */
  private String migradoSidep;

  /** The dictamen. */
  private DictamenDTO dictamen;

  /** The documento urgencia. */
  private DocumentoAportadoDTO documentoUrgencia;

  /** The documento. */
  private DocumentoDTO documento;

  /** The solicitud. */
  private SolicitudDTO solicitud;

  /** The usuario 1. */
  private UsuarioDTO usuarioCreador;

  /** The usuario 2. */
  private UsuarioDTO usuarioCierre;

  /** The sdm acta. */
  private ActaDTO sdmActa;

  /** The sdm actacorrectiva. */
  private ActaDTO sdmActacorrectiva;

  /** The es permanente. */
  private Integer esPermanente;


  /**
   * Instantiates a new estudioDTO.
   */
  public EstudioDTO() {
    super();
  }

  /**
   * Gets the pk estudio.
   *
   * @return the pk estudio
   */
  public Long getPkEstudio() {
    return pkEstudio;
  }

  /**
   * Sets the pk estudio.
   *
   * @param pkEstudio the new pk estudio
   */
  public void setPkEstudio(final Long pkEstudio) {
    this.pkEstudio = pkEstudio;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the estado.
   *
   * @return the estado
   */
  public String getEstado() {
    return estado;
  }

  /**
   * Sets the estado.
   *
   * @param estado the new estado
   */
  public void setEstado(final String estado) {
    this.estado = estado;
  }

  /**
   * Gets the fecha fin.
   *
   * @return the fecha fin
   */
  public Date getFechaFin() {
    return UtilidadesCommons.cloneDate(fechaFin);
  }

  /**
   * Sets the fecha fin.
   *
   * @param fechaFin the new fecha fin
   */
  public void setFechaFin(final Date fechaFin) {
    this.fechaFin = UtilidadesCommons.cloneDate(fechaFin);
  }

  /**
   * Gets the fecha inicio.
   *
   * @return the fecha inicio
   */
  public Date getFechaInicio() {
    return UtilidadesCommons.cloneDate(fechaInicio);
  }

  /**
   * Sets the fecha inicio.
   *
   * @param fechaInicio the new fecha inicio
   */
  public void setFechaInicio(final Date fechaInicio) {
    this.fechaInicio = UtilidadesCommons.cloneDate(fechaInicio);
  }

  /**
   * Gets the fecha aprobacion.
   *
   * @return the fecha aprobacion
   */
  public Date getFechaAprobacion() {
    return UtilidadesCommons.cloneDate(fechaAprobacion);
  }

  /**
   * Sets the fecha aprobacion.
   *
   * @param fechaAprobacion the new fecha aprobacion
   */
  public void setFechaAprobacion(final Date fechaAprobacion) {
    this.fechaAprobacion = UtilidadesCommons.cloneDate(fechaAprobacion);
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the motivo informe salud fisico.
   *
   * @return the motivo informe salud fisico
   */
  public String getMotivoInformeSaludFisico() {
    return motivoInformeSaludFisico;
  }

  /**
   * Sets the motivo informe salud fisico.
   *
   * @param motivoInformeSaludFisico the new motivo informe salud fisico
   */
  public void setMotivoInformeSaludFisico(
      final String motivoInformeSaludFisico) {
    this.motivoInformeSaludFisico = motivoInformeSaludFisico;
  }

  /**
   * Gets the motivo informe salud mental.
   *
   * @return the motivo informe salud mental
   */
  public String getMotivoInformeSaludMental() {
    return motivoInformeSaludMental;
  }

  /**
   * Sets the motivo informe salud mental.
   *
   * @param motivoInformeSaludMental the new motivo informe salud mental
   */
  public void setMotivoInformeSaludMental(
      final String motivoInformeSaludMental) {
    this.motivoInformeSaludMental = motivoInformeSaludMental;
  }

  /**
   * Gets the motivo informe salud.
   *
   * @return the motivo informe salud
   */
  public String getMotivoInformeSalud() {
    return motivoInformeSalud;
  }

  /**
   * Sets the motivo informe salud.
   *
   * @param motivoInformeSalud the new motivo informe salud
   */
  public void setMotivoInformeSalud(final String motivoInformeSalud) {
    this.motivoInformeSalud = motivoInformeSalud;
  }

  /**
   * Gets the motivo informe social.
   *
   * @return the motivo informe social
   */
  public String getMotivoInformeSocial() {
    return motivoInformeSocial;
  }

  /**
   * Sets the motivo informe social.
   *
   * @param motivoInformeSocial the new motivo informe social
   */
  public void setMotivoInformeSocial(final String motivoInformeSocial) {
    this.motivoInformeSocial = motivoInformeSocial;
  }

  /**
   * Gets the motivo valoracion.
   *
   * @return the motivo valoracion
   */
  public String getMotivoValoracion() {
    return motivoValoracion;
  }

  /**
   * Sets the motivo valoracion.
   *
   * @param motivoValoracion the new motivo valoracion
   */
  public void setMotivoValoracion(final String motivoValoracion) {
    this.motivoValoracion = motivoValoracion;
  }

  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return observaciones;
  }

  /**
   * Sets the observaciones.
   *
   * @param observaciones the new observaciones
   */
  public void setObservaciones(final String observaciones) {
    this.observaciones = observaciones;
  }

  /**
   * Gets the otro tipo estudio.
   *
   * @return the otro tipo estudio
   */
  public String getOtroTipoEstudio() {
    return otroTipoEstudio;
  }

  /**
   * Sets the otro tipo estudio.
   *
   * @param otroTipoEstudio the new otro tipo estudio
   */
  public void setOtroTipoEstudio(final String otroTipoEstudio) {
    this.otroTipoEstudio = otroTipoEstudio;
  }

  /**
   * Gets the repeticion informe salud fisico.
   *
   * @return the repeticion informe salud fisico
   */
  public Boolean getRepeticionInformeSaludFisico() {
    return repeticionInformeSaludFisico != null && repeticionInformeSaludFisico;
  }

  /**
   * Sets the repeticion informe salud fisico.
   *
   * @param repeticionInformeSaludFisico the new repeticion informe salud fisico
   */
  public void setRepeticionInformeSaludFisico(
      final Boolean repeticionInformeSaludFisico) {
    this.repeticionInformeSaludFisico = repeticionInformeSaludFisico;
  }

  /**
   * Gets the repeticion informe salud mental.
   *
   * @return the repeticion informe salud mental
   */
  public Boolean getRepeticionInformeSaludMental() {
    return repeticionInformeSaludMental != null && repeticionInformeSaludMental;
  }

  /**
   * Sets the repeticion informe salud mental.
   *
   * @param repeticionInformeSaludMental the new repeticion informe salud mental
   */
  public void setRepeticionInformeSaludMental(
      final Boolean repeticionInformeSaludMental) {
    this.repeticionInformeSaludMental = repeticionInformeSaludMental;
  }

  /**
   * Gets the repeticion informe salud.
   *
   * @return the repeticion informe salud
   */
  public Boolean getRepeticionInformeSalud() {
    return repeticionInformeSalud != null && repeticionInformeSalud;
  }

  /**
   * Sets the repeticion informe salud.
   *
   * @param repeticionInformeSalud the new repeticion informe salud
   */
  public void setRepeticionInformeSalud(final Boolean repeticionInformeSalud) {
    this.repeticionInformeSalud = repeticionInformeSalud;
  }

  /**
   * Gets the repeticion informe social.
   *
   * @return the repeticion informe social
   */
  public Boolean getRepeticionInformeSocial() {
    return repeticionInformeSocial != null && repeticionInformeSocial;
  }

  /**
   * Sets the repeticion informe social.
   *
   * @param repeticionInformeSocial the new repeticion informe social
   */
  public void setRepeticionInformeSocial(
      final Boolean repeticionInformeSocial) {
    this.repeticionInformeSocial = repeticionInformeSocial;
  }

  /**
   * Gets the repeticion informe valoracion.
   *
   * @return the repeticion informe valoracion
   */
  public Boolean getRepeticionInformeValoracion() {
    return repeticionInformeValoracion != null && repeticionInformeValoracion;
  }

  /**
   * Sets the repeticion informe valoracion.
   *
   * @param repeticionInformeValoracion the new repeticion informe valoracion
   */
  public void setRepeticionInformeValoracion(
      final Boolean repeticionInformeValoracion) {
    this.repeticionInformeValoracion = repeticionInformeValoracion;
  }

  /**
   * Gets the resultado.
   *
   * @return the resultado
   */
  public Boolean getResultado() {
    return resultado;
  }

  /**
   * Sets the resultado.
   *
   * @param resultado the new resultado
   */
  public void setResultado(final Boolean resultado) {
    this.resultado = resultado;
  }

  /**
   * Gets the tipo.
   *
   * @return the tipo
   */
  public String getTipo() {
    return tipo;
  }

  /**
   * Sets the tipo.
   *
   * @param tipo the new tipo
   */
  public void setTipo(final String tipo) {
    this.tipo = tipo;
  }

  /**
   * Gets the tipo dictamen.
   *
   * @return the tipo dictamen
   */
  public String getTipoDictamen() {
    return tipoDictamen;
  }

  /**
   * Sets the tipo dictamen.
   *
   * @param tipoDictamen the new tipo dictamen
   */
  public void setTipoDictamen(final String tipoDictamen) {
    this.tipoDictamen = tipoDictamen;
  }

  /**
   * Gets the urgente.
   *
   * @return the urgente
   */
  public Boolean getUrgente() {
    return urgente;
  }

  /**
   * Sets the urgente.
   *
   * @param urgente the new urgente
   */
  public void setUrgente(final Boolean urgente) {
    this.urgente = urgente;
  }

  /**
   * Gets the migrado sidep.
   *
   * @return the migrado sidep
   */
  public String getMigradoSidep() {
    return migradoSidep;
  }

  /**
   * Sets the migrado sidep.
   *
   * @param migradoSidep the new migrado sidep
   */
  public void setMigradoSidep(final String migradoSidep) {
    this.migradoSidep = migradoSidep;
  }

  /**
   * Gets the dictamen.
   *
   * @return the dictamen
   */
  public DictamenDTO getDictamen() {
    return dictamen;
  }

  /**
   * Sets the dictamen.
   *
   * @param dictamen the new dictamen
   */
  public void setDictamen(final DictamenDTO dictamen) {
    this.dictamen = dictamen;
  }

  /**
   * Gets the documento urgencia.
   *
   * @return the documento urgencia
   */
  public DocumentoAportadoDTO getDocumentoUrgencia() {
    return documentoUrgencia;
  }

  /**
   * Sets the documento urgencia.
   *
   * @param documentoUrgencia the new documento urgencia
   */
  public void setDocumentoUrgencia(
      final DocumentoAportadoDTO documentoUrgencia) {
    this.documentoUrgencia = documentoUrgencia;
  }

  /**
   * Gets the documento.
   *
   * @return the documento
   */
  public DocumentoDTO getDocumento() {
    return documento;
  }

  /**
   * Sets the documento.
   *
   * @param documento the new documento
   */
  public void setDocumento(final DocumentoDTO documento) {
    this.documento = documento;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public SolicitudDTO getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the new solicitud
   */
  public void setSolicitud(final SolicitudDTO solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the usuario 1.
   *
   * @return the usuario 1
   */
  public UsuarioDTO getUsuarioCreador() {
    return usuarioCreador;
  }

  /**
   * Sets the usuario 1.
   *
   * @param usuarioCreador the new usuario 1
   */
  public void setUsuarioCreador(final UsuarioDTO usuarioCreador) {
    this.usuarioCreador = usuarioCreador;
  }

  /**
   * Gets the usuario 2.
   *
   * @return the usuario 2
   */
  public UsuarioDTO getUsuarioCierre() {
    return usuarioCierre;
  }

  /**
   * Sets the usuario 2.
   *
   * @param usuarioCierre the new usuario 2
   */
  public void setUsuarioCierre(final UsuarioDTO usuarioCierre) {
    this.usuarioCierre = usuarioCierre;
  }

  /**
   * Gets the sdm acta.
   *
   * @return the sdm acta
   */
  public ActaDTO getSdmActa() {
    return sdmActa;
  }

  /**
   * Sets the sdm acta.
   *
   * @param sdmActa the new sdm acta
   */
  public void setSdmActa(final ActaDTO sdmActa) {
    this.sdmActa = sdmActa;
  }

  /**
   * Gets the sdm actacorrectiva.
   *
   * @return the sdm actacorrectiva
   */
  public ActaDTO getSdmActacorrectiva() {
    return sdmActacorrectiva;
  }

  /**
   * Sets the sdm actacorrectiva.
   *
   * @param sdmActacorrectiva the new sdm actacorrectiva
   */
  public void setSdmActacorrectiva(final ActaDTO sdmActacorrectiva) {
    this.sdmActacorrectiva = sdmActacorrectiva;
  }

  /**
   * Gets the tipo estudio.
   *
   * @return the tipo estudio
   */
  public String getTipoEstudio() {
    return Labels.getLabel(TipoEstudioEnum.fromString(getTipo()).getLabel());
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

  /**
   * Gets the es permanente.
   *
   * @return the es permanente
   */
  public Integer getEsPermanente() {
    return esPermanente;
  }

  /**
   * Sets the es permanente.
   *
   * @param esPermanente the new es permanente
   */
  public void setEsPermanente(final Integer esPermanente) {
    this.esPermanente = esPermanente;
  }

  /**
   * Gets the respuesta.
   *
   * @return the respuesta
   */
  public String getRespuesta() {
    if (this.getResultado() == null) {
        return "";
    }
    return Boolean.TRUE.equals(this.getResultado()) ? "Sí" : "No";
  }

}
