package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;


/**
 * The Class TipoIdentificadorDTO.
 */
public class TipoIdentificadorDTO extends BaseDTO
    implements Comparable<TipoIdentificadorDTO> {


  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1713021478365743625L;

  /** The pk tipoiden. */
  private Long pkTipoiden;

  /** The codinss. */
  private Long codinss;

  /** The activo. */
  private boolean activo;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The codigo imserso. */
  private String codigoImserso;

  /** The nombre. */
  private String nombre;

  /**
   * Instantiates a new tipo identificador DTO.
   */
  public TipoIdentificadorDTO() {
    super();
  }

  /**
   * Gets the pk tipoiden.
   *
   * @return the pk tipoiden
   */
  public Long getPkTipoiden() {
    return pkTipoiden;
  }

  /**
   * Sets the pk tipoiden.
   *
   * @param pkTipoiden the new pk tipoiden
   */
  public void setPkTipoiden(final Long pkTipoiden) {
    this.pkTipoiden = pkTipoiden;
  }

  /**
   * Gets the codinss.
   *
   * @return the codinss
   */
  public Long getCodinss() {
    return codinss;
  }

  /**
   * Sets the codinss.
   *
   * @param codinss the new codinss
   */
  public void setCodinss(final Long codinss) {
    this.codinss = codinss;
  }

  /**
   * Checks if is activo.
   *
   * @return true, if is activo
   */
  public boolean isActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the codigo imserso.
   *
   * @return the codigo imserso
   */
  public String getCodigoImserso() {
    return codigoImserso;
  }

  /**
   * Sets the codigo imserso.
   *
   * @param codigoImserso the new codigo imserso
   */
  public void setCodigoImserso(final String codigoImserso) {
    this.codigoImserso = codigoImserso;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final TipoIdentificadorDTO o) {
    return 0;
  }

}
