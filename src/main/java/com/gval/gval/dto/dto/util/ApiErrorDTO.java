package com.gval.gval.dto.dto.util;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.List;

/**
 * The Class ApiErrorDto.
 *
 * Recoge los errores del Api
 */
public class ApiErrorDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -5856188479737271869L;

  /** The status. */
  private String status;

  /** The timestamp. */
  private String timestamp;

  /** The message. */
  private String message;

  /** The debug message. */
  private String debugMessage;

  /** The sub errors. */
  private List<ApiSubErrorDTO> subErrors;

  /**
   * Instantiates a new api error dto.
   */
  public ApiErrorDTO() {
    super();
  }

  /**
   * Gets the status.
   *
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * Sets the status.
   *
   * @param status the new status
   */
  public void setStatus(final String status) {
    this.status = status;
  }

  /**
   * Gets the timestamp.
   *
   * @return the timestamp
   */
  public String getTimestamp() {
    return timestamp;
  }

  /**
   * Sets the timestamp.
   *
   * @param timestamp the new timestamp
   */
  public void setTimestamp(final String timestamp) {
    this.timestamp = timestamp;
  }

  /**
   * Gets the message.
   *
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  /**
   * Sets the message.
   *
   * @param message the new message
   */
  public void setMessage(final String message) {
    this.message = message;
  }

  /**
   * Gets the debug message.
   *
   * @return the debug message
   */
  public String getDebugMessage() {
    return debugMessage;
  }

  /**
   * Sets the debug message.
   *
   * @param debugMessage the new debug message
   */
  public void setDebugMessage(final String debugMessage) {
    this.debugMessage = debugMessage;
  }

  /**
   * Gets the sub errors.
   *
   * @return the sub errors
   */
  public List<ApiSubErrorDTO> getSubErrors() {
    return UtilidadesCommons.collectorsToList(subErrors);
  }

  /**
   * Sets the sub errors.
   *
   * @param subErrors the new sub errors
   */
  public void setSubErrors(final List<ApiSubErrorDTO> subErrors) {
    this.subErrors = UtilidadesCommons.collectorsToList(subErrors);
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
