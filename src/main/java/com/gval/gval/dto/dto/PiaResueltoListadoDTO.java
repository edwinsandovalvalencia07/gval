package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import java.util.Date;

/**
 * The Class PiaResueltoListadoDTO.
 */
public class PiaResueltoListadoDTO extends BaseDTO
    implements Comparable<PiaResueltoListadoDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -419440574040012666L;
  /** The pk pia. */
  private Long pkPia;

  /** The pk expedien. */
  private Long pkExpedien;

  /** The pk solicit. */
  private Long pkSolicitud;

  /** The codigo solicitud. */
  private String codigoSolicitud;

  /** The tipo pia. */
  private String tipoPia;

  /** The tipo solicit pia. */
  private String tipoSolicitPia;

  /** The fecha resolucion. */
  private Date fechaResolucion;

  /** The fecha efecto. */
  private Date fechaEfecto;

  /** The fecha baja. */
  private Date fechaBaja;

  /** The fecha inicio pago. */
  private Date fechaInicioPago;

  /** The terceros. */
  private Boolean terceros;

  /** The fiscalizacion previa. */
  private Boolean fiscalizacionPrevia;

  /** The contabilizacion. */
  private Boolean contabilizacion;

  /** The resolucion economica. */
  private Boolean resolucionEconomica;

  /** The pipago. */
  private Boolean pipago;

  /**
   * Instantiates a new pia resuelto listado DTO.
   */
  public PiaResueltoListadoDTO() {
    super();
  }

  /**
   * Gets the pk pia.
   *
   * @return the pk pia
   */
  public Long getPkPia() {
    return pkPia;
  }

  /**
   * Sets the pk pia.
   *
   * @param pkPia the new pk pia
   */
  public void setPkPia(final Long pkPia) {
    this.pkPia = pkPia;
  }

  /**
   * Gets the pk expedien.
   *
   * @return the pk expedien
   */
  public Long getPkExpedien() {
    return pkExpedien;
  }

  /**
   * Sets the pk expedien.
   *
   * @param pkExpedien the new pk expedien
   */
  public void setPkExpedien(final Long pkExpedien) {
    this.pkExpedien = pkExpedien;
  }

  /**
   * Gets the pk solicit.
   *
   * @return the pk solicit
   */
  public Long getPkSolicitud() {
    return pkSolicitud;
  }

  /**
   * Sets the pk solicit.
   *
   * @param pkSolicitud the new pk solicitud
   */
  public void setPkSolicitud(final Long pkSolicitud) {
    this.pkSolicitud = pkSolicitud;
  }

  /**
   * Gets the codigo solicitud.
   *
   * @return the codigo solicitud
   */
  public String getCodigoSolicitud() {
    return codigoSolicitud;
  }

  /**
   * Sets the codigo solicitud.
   *
   * @param codigoSolicitud the new codigo solicitud
   */
  public void setCodigoSolicitud(final String codigoSolicitud) {
    this.codigoSolicitud = codigoSolicitud;
  }

  /**
   * Gets the tipo pia.
   *
   * @return the tipo pia
   */
  public String getTipoPia() {
    return tipoPia;
  }

  /**
   * Sets the tipo pia.
   *
   * @param tipoPia the new tipo pia
   */
  public void setTipoPia(final String tipoPia) {
    this.tipoPia = tipoPia;
  }

  /**
   * Gets the fecha resolucion.
   *
   * @return the fecha resolucion
   */
  public Date getFechaResolucion() {
    return UtilidadesCommons.cloneDate(fechaResolucion);
  }

  /**
   * Sets the fecha resolucion.
   *
   * @param fechaResolucion the new fecha resolucion
   */
  public void setFechaResolucion(final Date fechaResolucion) {
    this.fechaResolucion = UtilidadesCommons.cloneDate(fechaResolucion);;
  }

  /**
   * Gets the fecha efecto.
   *
   * @return the fecha efecto
   */
  public Date getFechaEfecto() {
    return UtilidadesCommons.cloneDate(fechaEfecto);
  }

  /**
   * Sets the fecha efecto.
   *
   * @param fechaEfecto the new fecha efecto
   */
  public void setFechaEfecto(final Date fechaEfecto) {
    this.fechaEfecto = UtilidadesCommons.cloneDate(fechaEfecto);
  }

  /**
   * Gets the fecha baja.
   *
   * @return the fecha baja
   */
  public Date getFechaBaja() {
    return UtilidadesCommons.cloneDate(fechaBaja);
  }

  /**
   * Sets the fecha baja.
   *
   * @param fechaBaja the new fecha baja
   */
  public void setFechaBaja(final Date fechaBaja) {
    this.fechaBaja = UtilidadesCommons.cloneDate(fechaBaja);
  }

  /**
   * Gets the fecha inicio pago.
   *
   * @return the fecha inicio pago
   */
  public Date getFechaInicioPago() {
    return UtilidadesCommons.cloneDate(fechaInicioPago);
  }

  /**
   * Sets the fecha inicio pago.
   *
   * @param fechaInicioPago the new fecha inicio pago
   */
  public void setFechaInicioPago(final Date fechaInicioPago) {
    this.fechaInicioPago = UtilidadesCommons.cloneDate(fechaInicioPago);
  }

  /**
   * Gets the terceros.
   *
   * @return the terceros
   */
  public Boolean getTerceros() {
    return terceros;
  }

  /**
   * Sets the terceros.
   *
   * @param terceros the new terceros
   */
  public void setTerceros(final Boolean terceros) {
    this.terceros = terceros;
  }

  /**
   * Gets the fiscalizacion previa.
   *
   * @return the fiscalizacion previa
   */
  public Boolean getFiscalizacionPrevia() {
    return fiscalizacionPrevia;
  }

  /**
   * Sets the fiscalizacion previa.
   *
   * @param fiscalizacionPrevia the new fiscalizacion previa
   */
  public void setFiscalizacionPrevia(final Boolean fiscalizacionPrevia) {
    this.fiscalizacionPrevia = fiscalizacionPrevia;
  }

  /**
   * Gets the contabilizacion.
   *
   * @return the contabilizacion
   */
  public Boolean getContabilizacion() {
    return contabilizacion;
  }

  /**
   * Sets the contabilizacion.
   *
   * @param contabilizacion the new contabilizacion
   */
  public void setContabilizacion(final Boolean contabilizacion) {
    this.contabilizacion = contabilizacion;
  }

  /**
   * Gets the resolucion economica.
   *
   * @return the resolucion economica
   */
  public Boolean getResolucionEconomica() {
    return resolucionEconomica;
  }

  /**
   * Sets the resolucion economica.
   *
   * @param resolucionEconomica the new resolucion economica
   */
  public void setResolucionEconomica(final Boolean resolucionEconomica) {
    this.resolucionEconomica = resolucionEconomica;
  }

  /**
   * Gets the tipo solicit pia.
   *
   * @return the tipo solicit pia
   */
  public String getTipoSolicitPia() {
    return tipoSolicitPia;
  }

  /**
   * Sets the tipo solicit pia.
   *
   * @param tipoSolicitPia the new tipo solicit pia
   */
  public void setTipoSolicitPia(final String tipoSolicitPia) {
    this.tipoSolicitPia = tipoSolicitPia;
  }

  /**
   * Gets the pipago.
   *
   * @return the pipago
   */
  public Boolean getPipago() {
    return pipago;
  }


  /**
   * Sets the pipago.
   *
   * @param pipago the new pipago
   */
  public void setPipago(final Boolean pipago) {
    this.pipago = pipago;
  }


  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final PiaResueltoListadoDTO o) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
