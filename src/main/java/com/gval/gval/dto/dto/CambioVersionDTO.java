package com.gval.gval.dto.dto;


import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * The Class MejorasVersionDTO.
 */
public class CambioVersionDTO extends BaseDTO
    implements Comparable<CambioVersionDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 3725138035280300568L;

  /** The pk mejora version. */
  private Long pkCambioVersion;

  /** The titulo. */
  private String titulo;

  /** The descripcion. */
  private String descripcion;

  /** The fecha crea. */
  private Date fechaCrea;

  /** The activo. */
  private Boolean activo;

  /** The version ada. */
  private VersionAdaDTO versionAda;


  /** The es correccion. */
  private String tipo;

  /**
   * Version ada DTO.
   */
  public CambioVersionDTO() {
    super();
  }


  /**
   * Gets the pk cambio version.
   *
   * @return the pk cambio version
   */
  public Long getPkCambioVersion() {
    return this.pkCambioVersion;
  }

  /**
   * Sets the pk cambio version.
   *
   * @param pkMejoraVersion the new pk cambio version
   */
  public void setPkCambioVersion(final Long pkMejoraVersion) {
    this.pkCambioVersion = pkMejoraVersion;
  }

  /**
   * Gets the titulo.
   *
   * @return the titulo
   */
  public String getTitulo() {
    return this.titulo;
  }

  /**
   * Sets the titulo.
   *
   * @param titulo the new titulo
   */
  public void setTitulo(final String titulo) {
    this.titulo = titulo;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return this.descripcion;
  }

  /**
   * Gets the descripciones.
   *
   * @return the descripciones
   */
  public List<String> getDescripciones() {
    return StringUtils.isBlank(this.descripcion) ? new ArrayList<>()
        : Arrays.asList(this.descripcion.split(StringUtils.LF));
  }


  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the fecha crea.
   *
   * @return the fecha crea
   */
  public Date getFechaCrea() {
    return UtilidadesCommons.cloneDate(this.fechaCrea);
  }

  /**
   * Sets the fecha crea.
   *
   * @param fechaCrea the new fecha crea
   */
  public void setFechaCrea(final Date fechaCrea) {
    this.fechaCrea = UtilidadesCommons.cloneDate(fechaCrea);
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return this.activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the version ada.
   *
   * @return the version ada
   */
  public VersionAdaDTO getVersionAda() {
    return this.versionAda;
  }

  /**
   * Sets the version ada.
   *
   * @param versionAda the new version ada
   */
  public void setVersionAda(final VersionAdaDTO versionAda) {
    this.versionAda = versionAda;
  }

  /**
   * Gets the tipo.
   *
   * @return the tipo
   */
  public String getTipo() {
    return tipo;
  }

  /**
   * Sets the tipo.
   *
   * @param tipo the new tipo
   */
  public void setTipo(final String tipo) {
    this.tipo = tipo;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final CambioVersionDTO o) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
