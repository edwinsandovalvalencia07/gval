package com.gval.gval.dto.dto.util;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

/**
 * The Class InsertarPIAErrorDTO.
 */
public class InsertarPIAMensajeDTO implements Serializable {


  /** The Constant RESULTADO_PL_CORRECTO. */
  public static final String CONFIRMAR_CAPACIDAD_ECONOMICA = "CONF_CAPA";

  /** The Constant CONFIRMAR_PRESUPUESTO_NEGATIVO. */
  public static final String CONFIRMAR_PRESUPUESTO_NEGATIVO = "PRES_NEG";

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The codigo error. */
  private String codigoError;

  /** The error. */
  private String error;


  /**
   * Instantiates a new insertar PIA error DTO.
   *
   * @param codigoError the codigo error
   * @param error the error
   */
  public InsertarPIAMensajeDTO(final String codigoError, final String error) {
    super();
    this.codigoError = codigoError;
    this.error = error;
  }


  /**
   * Instantiates a new insertar PIA error DTO.
   *
   * @param error the error
   */
  public InsertarPIAMensajeDTO(final String error) {
    this(null, error);
  }

  /**
   * Gets the codigo error.
   *
   * @return the codigo error
   */
  public String getCodigoError() {
    return codigoError;
  }

  /**
   * Sets the codigo error.
   *
   * @param codigoError the new codigo error
   */
  public void setCodigoError(final String codigoError) {
    this.codigoError = codigoError;
  }

  /**
   * Gets the error.
   *
   * @return the error
   */
  public String getError() {
    return error;
  }

  /**
   * Sets the error.
   *
   * @param error the new error
   */
  public void setError(final String error) {
    this.error = error;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }


}

