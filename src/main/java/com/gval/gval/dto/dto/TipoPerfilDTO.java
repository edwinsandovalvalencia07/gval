package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;

/**
 * The Class TipoPerfilDTO.
 */
public class TipoPerfilDTO extends BaseDTO
    implements Comparable<TipoPerfilDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 4759463869995830929L;

  /** The pk tipoperf. */
  private Long pkTipoperf;

  /** The codigo. */
  private String codigo;

  /** The descripcion. */
  private String descripcion;

  /** The fecha creacion. */
  private Date fechaCreacion;


  /** The activo. */
  private Boolean activo;

  /** The grupo clau. */
  private String grupoClau;

  /** The valorador. */
  private Boolean valorador;

  /** The social. */
  private Boolean social;

  /**
   * Instantiates a new tipo perfil DTO.
   */
  public TipoPerfilDTO() {
    super();
  }

  /**
   * Instantiates a new tipo perfil DTO.
   *
   * @param pkTipoperf the pk tipoperf
   * @param codigo the codigo
   * @param descripcion the descripcion
   * @param fechaCreacion the fecha creacion
   * @param activo the activo
   * @param grupoClau the grupo clau
   * @param valorador the valorador
   * @param social the social
   */
  public TipoPerfilDTO(final Long pkTipoperf, final String codigo,
      final String descripcion, final Date fechaCreacion, final Boolean activo,
      final String grupoClau, final Boolean valorador, final Boolean social) {
    super();
    this.pkTipoperf = pkTipoperf;
    this.codigo = codigo;
    this.descripcion = descripcion;
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
    this.activo = activo;
    this.grupoClau = grupoClau;
    this.valorador = valorador;
    this.social = social;
  }

  /**
   * Gets the pk tipoperf.
   *
   * @return the pk tipoperf
   */
  public Long getPkTipoperf() {
    return pkTipoperf;
  }

  /**
   * Sets the pk tipoperf.
   *
   * @param pkTipoperf the new pk tipoperf
   */
  public void setPkTipoperf(final Long pkTipoperf) {
    this.pkTipoperf = pkTipoperf;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return (fechaCreacion == null) ? null : (Date) fechaCreacion.clone();
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion =
        (fechaCreacion == null) ? null : (Date) fechaCreacion.clone();
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }



  /**
   * Gets the grupo clau.
   *
   * @return the grupo clau
   */
  public String getGrupoClau() {
    return grupoClau;
  }

  /**
   * Sets the grupo clau.
   *
   * @param grupoClau the new grupo clau
   */
  public void setGrupoClau(final String grupoClau) {
    this.grupoClau = grupoClau;
  }

  /**
   * Gets the valorador.
   *
   * @return the valorador
   */
  public Boolean getValorador() {
    return valorador;
  }

  /**
   * Sets the valorador.
   *
   * @param valorador the new valorador
   */
  public void setValorador(final Boolean valorador) {
    this.valorador = valorador;
  }

  /**
   * Gets the social.
   *
   * @return the social
   */
  public Boolean getSocial() {
    return social;
  }

  /**
   * Sets the social.
   *
   * @param social the new social
   */
  public void setSocial(final Boolean social) {
    this.social = social;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final TipoPerfilDTO o) {
    return 0;
  }

}
