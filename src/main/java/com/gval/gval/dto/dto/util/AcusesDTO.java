package com.gval.gval.dto.dto.util;



import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.DocumentoDTO;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;
import java.util.Objects;

import javax.validation.constraints.Size;

/**
 * The Class AcusesDTO.
 */
public class AcusesDTO extends BaseDTO implements Comparable<DocumentoDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 7881811397666101320L;

  /** The pk documento. */
  private Long pkDocumento;

  /** The enviado acuse 1. */
  @Size(max = 1)
  private String enviadoAcuse1;

  /** The enviado acuse 2. */
  @Size(max = 1000)
  private String enviadoAcuse2;

  /** The fecha acuse 1. */
  private Date fechaAcuse1;

  /** The fecha acuse 2. */
  private Date fechaAcuse2;


  /** The nuacre acuse 1. */
  @Size(max = 50)
  private String nuacreAcuse1;

  /** The nuacre acuse 2. */
  @Size(max = 50)
  private String nuacreAcuse2;

  /** The tipo acuse 1. */
  @Size(max = 2)
  private String tipoAcuse1;

  /** The tipo acuse 2. */
  @Size(max = 2)
  private String tipoAcuse2;

  /** The fecha publicacion BOE. */
  private Date fechaPublicacionBOE;

  /** The numero BOE. */
  private String numeroBOE;


  /**
   * Instantiates a new acuses DTO.
   */
  public AcusesDTO() {
    super();
  }

  /**
   * Instantiates a new acuses DTO.
   *
   * @param pkDocumento the pk documento
   * @param fechaAcuse1 the fecha acuse 1
   * @param nuacreAcuse1 the nuacre acuse 1
   * @param tipoAcuse1 the tipo acuse 1
   * @param enviadoAcuse1 the enviado acuse 1
   * @param fechaAcuse2 the fecha acuse 2
   * @param nuacreAcuse2 the nuacre acuse 2
   * @param tipoAcuse2 the tipo acuse 2
   * @param enviadoAcuse2 the enviado acuse 2
   * @param fechaPublicacionBOE the fecha publicacion BOE
   * @param numeroBOE the numero BOE
   */
  public AcusesDTO(final Long pkDocumento, final Date fechaAcuse1,
      final String nuacreAcuse1, final String tipoAcuse1,
      final String enviadoAcuse1, final Date fechaAcuse2,
      final String nuacreAcuse2, final String tipoAcuse2,
      final String enviadoAcuse2, final Date fechaPublicacionBOE,
      final String numeroBOE) {
    super();
    this.pkDocumento = pkDocumento;
    this.fechaAcuse1 = UtilidadesCommons.cloneDate(fechaAcuse1);
    this.nuacreAcuse1 = nuacreAcuse1;
    this.tipoAcuse1 = tipoAcuse1;
    this.enviadoAcuse1 = enviadoAcuse1;
    this.fechaAcuse2 = UtilidadesCommons.cloneDate(fechaAcuse2);
    this.nuacreAcuse2 = nuacreAcuse2;
    this.tipoAcuse2 = tipoAcuse2;
    this.enviadoAcuse2 = enviadoAcuse2;
    this.fechaPublicacionBOE = UtilidadesCommons.cloneDate(fechaPublicacionBOE);
    this.numeroBOE = numeroBOE;
  }



  /**
   * Gets the pk documento.
   *
   * @return the pkDocumento
   */
  public Long getPkDocumento() {
    return pkDocumento;
  }



  /**
   * Sets the pk documento.
   *
   * @param pkDocumento the pkDocumento to set
   */
  public void setPkDocumento(final Long pkDocumento) {
    this.pkDocumento = pkDocumento;
  }



  /**
   * Gets the enviado acuse 1.
   *
   * @return the enviadoAcuse1
   */
  public String getEnviadoAcuse1() {
    return enviadoAcuse1;
  }

  /**
   * Sets the enviado acuse 1.
   *
   * @param enviadoAcuse1 the enviadoAcuse1 to set
   */
  public void setEnviadoAcuse1(final String enviadoAcuse1) {
    this.enviadoAcuse1 = enviadoAcuse1;
  }

  /**
   * Gets the enviado acuse 2.
   *
   * @return the enviadoAcuse2
   */
  public String getEnviadoAcuse2() {
    return enviadoAcuse2;
  }



  /**
   * Sets the enviado acuse 2.
   *
   * @param enviadoAcuse2 the enviadoAcuse2 to set
   */
  public void setEnviadoAcuse2(final String enviadoAcuse2) {
    this.enviadoAcuse2 = enviadoAcuse2;
  }



  /**
   * Gets the fecha acuse 1.
   *
   * @return the fechaAcuse1
   */
  public Date getFechaAcuse1() {
    return UtilidadesCommons.cloneDate(fechaAcuse1);
  }



  /**
   * Sets the fecha acuse 1.
   *
   * @param fechaAcuse1 the fechaAcuse1 to set
   */
  public void setFechaAcuse1(final Date fechaAcuse1) {
    this.fechaAcuse1 = UtilidadesCommons.cloneDate(fechaAcuse1);
  }



  /**
   * Gets the fecha acuse 2.
   *
   * @return the fechaAcuse2
   */
  public Date getFechaAcuse2() {
    return UtilidadesCommons.cloneDate(fechaAcuse2);
  }



  /**
   * Sets the fecha acuse 2.
   *
   * @param fechaAcuse2 the fechaAcuse2 to set
   */
  public void setFechaAcuse2(final Date fechaAcuse2) {
    this.fechaAcuse2 = UtilidadesCommons.cloneDate(fechaAcuse2);
  }



  /**
   * Gets the nuacre acuse 1.
   *
   * @return the nuacreAcuse1
   */
  public String getNuacreAcuse1() {
    return nuacreAcuse1;
  }



  /**
   * Sets the nuacre acuse 1.
   *
   * @param nuacreAcuse1 the nuacreAcuse1 to set
   */
  public void setNuacreAcuse1(final String nuacreAcuse1) {
    this.nuacreAcuse1 = nuacreAcuse1;
  }



  /**
   * Gets the nuacre acuse 2.
   *
   * @return the nuacreAcuse2
   */
  public String getNuacreAcuse2() {
    return nuacreAcuse2;
  }



  /**
   * Sets the nuacre acuse 2.
   *
   * @param nuacreAcuse2 the nuacreAcuse2 to set
   */
  public void setNuacreAcuse2(final String nuacreAcuse2) {
    this.nuacreAcuse2 = nuacreAcuse2;
  }



  /**
   * Gets the tipo acuse 1.
   *
   * @return the tipoAcuse1
   */
  public String getTipoAcuse1() {
    return tipoAcuse1;
  }



  /**
   * Sets the tipo acuse 1.
   *
   * @param tipoAcuse1 the tipoAcuse1 to set
   */
  public void setTipoAcuse1(final String tipoAcuse1) {
    this.tipoAcuse1 = tipoAcuse1;
  }



  /**
   * Gets the tipo acuse 2.
   *
   * @return the tipoAcuse2
   */
  public String getTipoAcuse2() {
    return tipoAcuse2;
  }



  /**
   * Sets the tipo acuse 2.
   *
   * @param tipoAcuse2 the tipoAcuse2 to set
   */
  public void setTipoAcuse2(final String tipoAcuse2) {
    this.tipoAcuse2 = tipoAcuse2;
  }


  /**
   * Gets the fecha publicacion BOE.
   *
   * @return the fecha publicacion BOE
   */
  public Date getFechaPublicacionBOE() {
    return UtilidadesCommons.cloneDate(fechaPublicacionBOE);
  }

  /**
   * Sets the fecha publicacion BOE.
   *
   * @param fechaPublicacionBOE the new fecha publicacion BOE
   */
  public void setFechaPublicacionBOE(final Date fechaPublicacionBOE) {
    this.fechaPublicacionBOE = UtilidadesCommons.cloneDate(fechaPublicacionBOE);
  }

  /**
   * Gets the numero BOE.
   *
   * @return the numero BOE
   */
  public String getNumeroBOE() {
    return numeroBOE;
  }

  /**
   * Sets the numero BOE.
   *
   * @param numeroBOE the new numero BOE
   */
  public void setNumeroBOE(final String numeroBOE) {
    this.numeroBOE = numeroBOE;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final DocumentoDTO o) {
    return 0;
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(pkDocumento);
  }

  /**
   * Equals.
   *
   * @param obj the obj
   * @return true, if successful
   */
  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    // null check
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final AcusesDTO other = (AcusesDTO) obj;
    return other.getPkDocumento() != null && this.pkDocumento != null
        && Objects.equals(this.pkDocumento, other.getPkDocumento());
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
