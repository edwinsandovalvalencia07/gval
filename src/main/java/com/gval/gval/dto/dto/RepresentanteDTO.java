/**
 * Copyright (c) 2020 Generalitat Valenciana - Todos los derechos reservados.
 */
package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import es.gva.dependencia.ada.common.enums.TipoRepresentacionEnum;
import com.gval.gval.dto.dto.util.BaseDTO;
import com.gval.gval.entity.model.Constantes;
import es.gva.dependencia.ada.validator.FechaNoFutura;

import org.hibernate.validator.constraints.NotBlank;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Clase que tiene la información de un Representante.
 *
 * @author Indra
 */
public final class RepresentanteDTO extends BaseDTO
    implements Comparable<RepresentanteDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -872555818785644643L;

  /** The Constant ERROR_TIPO_REPRESENTACION. */
  private static final String ERROR_TIPO_REPRESENTACION =
      "error.representante.tipoRepresentacion.vacio";

  /** The Constant ERROR_NUMERO_TUTELADA_LONGITUD. */
  private static final String ERROR_NUMERO_TUTELADA_LONGITUD =
      "error.representante.numeroTutelada.longitud";

  /** The Constant ERROR_MUNICIPIO_TUTELADA_LONGITUD. */
  private static final String ERROR_MUNICIPIO_TUTELADA_LONGITUD =
      "error.representante.municipioTutelada.longitud";

  /** Identificador en la tabla *. */
  private Long pkRepresen;

  /** Indica si el representante esta activo o inactivo *. */
  @NotNull
  private Boolean activo;

  /** Fecha de representacion legal *. */
  @FechaNoFutura(
      message = "error.representante.fecha_representacion_legal.nofutura")
  private Date fechaRepresentacionLegal;

  /** Fecha de sentencia judicial *. */
  @FechaNoFutura(
      message = "error.representante.fecha_sentencia_judicial.nofutura")
  private Date fechaSentenciaJudicial;

  /** Fecha de creación del registro *. */
  @NotNull
  private Date fechaCreacion;

  /** Indica si el representante firma la solicitud *. */
  private Boolean firmaSolicitud;

  /** Nombre del municipio tutelada *. */
  @Size(max = 50, message = ERROR_MUNICIPIO_TUTELADA_LONGITUD)
  private String municipioTutelada;

  /** Número de tutelada *. */
  @Size(max = 3, message = ERROR_NUMERO_TUTELADA_LONGITUD)
  private String numeroTutelada;

  /** Tipo de Representación Legal *. */
  @NotBlank(message = ERROR_TIPO_REPRESENTACION)
  private String tipoRepresentacionLegal;

  /** The ultimo. */
  private Boolean ultimo;

  /** Direccion del representante *. */
  private DireccionAdaDTO direccionAda;

  /** Información del representante legal *. */
  @NotNull
  private PersonaAdaDTO persona;

  /** Solicitud asociada al representante *. */
  @NotNull
  private SolicitudDTO solicitud;

  /** Unidad tutela *. */
  private UnidadTutelaDTO unidadTutela;

  /** Estado civil del representante *. */
  private EstadoCivilDTO estadoCivil;

  /** The app origen. */
  private String appOrigen;


  /**
   * Instantiates a new representante DTO.
   */
  public RepresentanteDTO() {
    super();
    this.activo = Boolean.TRUE;
  }

  /**
   * Instantiates a new representante DTO.
   *
   * @param defaultData the default data
   */
  public RepresentanteDTO(final boolean defaultData) {
    super();
    if (defaultData) {
      this.activo = Boolean.TRUE;
      this.fechaCreacion = new Date();
      this.tipoRepresentacionLegal = Constantes.VACIO;
      this.persona = new PersonaAdaDTO(true);
      this.direccionAda = new DireccionAdaDTO(true);
      this.solicitud = new SolicitudDTO();
      this.ultimo = Boolean.TRUE;
      this.tipoRepresentacionLegal =
          TipoRepresentacionEnum.REPRESENTANTE_LEGAL.getValue();
    }
  }

  /**
   * Gets the identificador en la tabla *.
   *
   * @return the pkRepresen
   */
  public Long getPkRepresen() {
    return pkRepresen;
  }

  /**
   * Sets the identificador en la tabla *.
   *
   * @param pkRepresen the pkRepresen to set
   */
  public void setPkRepresen(final Long pkRepresen) {
    this.pkRepresen = pkRepresen;
  }

  /**
   * Gets the indica si el representante esta activo o inactivo *.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the indica si el representante esta activo o inactivo *.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha de representacion legal *.
   *
   * @return the fechaRepresentacionLegal
   */
  public Date getFechaRepresentacionLegal() {
    return UtilidadesCommons.cloneDate(fechaRepresentacionLegal);
  }

  /**
   * Sets the fecha de representacion legal *.
   *
   * @param fechaRepresentacionLegal the fechaRepresentacionLegal to set
   */
  public void setFechaRepresentacionLegal(final Date fechaRepresentacionLegal) {
    this.fechaRepresentacionLegal =
        UtilidadesCommons.cloneDate(fechaRepresentacionLegal);
  }

  /**
   * Gets the fecha de sentencia judicial *.
   *
   * @return the fechaSentenciaJudicial
   */
  public Date getFechaSentenciaJudicial() {
    return UtilidadesCommons.cloneDate(fechaSentenciaJudicial);
  }

  /**
   * Sets the fecha de sentencia judicial *.
   *
   * @param fechaSentenciaJudicial the fechaSentenciaJudicial to set
   */
  public void setFechaSentenciaJudicial(final Date fechaSentenciaJudicial) {
    this.fechaSentenciaJudicial =
        UtilidadesCommons.cloneDate(fechaSentenciaJudicial);
  }

  /**
   * Gets the fecha de creación del registro *.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha de creación del registro *.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the indica si el representante firma la solicitud *.
   *
   * @return the firmaSolicitud
   */
  public Boolean getFirmaSolicitud() {
    return firmaSolicitud;
  }

  /**
   * Sets the indica si el representante firma la solicitud *.
   *
   * @param firmaSolicitud the firmaSolicitud to set
   */
  public void setFirmaSolicitud(final Boolean firmaSolicitud) {
    this.firmaSolicitud = firmaSolicitud;
  }

  /**
   * Gets the nombre del municipio tutelada *.
   *
   * @return the municipioTutelada
   */
  public String getMunicipioTutelada() {
    return municipioTutelada;
  }

  /**
   * Sets the nombre del municipio tutelada *.
   *
   * @param municipioTutelada the municipioTutelada to set
   */
  public void setMunicipioTutelada(final String municipioTutelada) {
    this.municipioTutelada = municipioTutelada;
  }

  /**
   * Gets the número de tutelada *.
   *
   * @return the numeroTutelada
   */
  public String getNumeroTutelada() {
    return numeroTutelada;
  }

  /**
   * Sets the número de tutelada *.
   *
   * @param numeroTutelada the numeroTutelada to set
   */
  public void setNumeroTutelada(final String numeroTutelada) {
    this.numeroTutelada = numeroTutelada;
  }

  /**
   * Gets the tipo de Representación Legal *.
   *
   * @return the tipoRepresentacionLegal
   */
  public String getTipoRepresentacionLegal() {
    return tipoRepresentacionLegal;
  }

  /**
   * Sets the tipo de Representación Legal *.
   *
   * @param tipoRepresentacionLegal the tipoRepresentacionLegal to set
   */
  public void setTipoRepresentacionLegal(final String tipoRepresentacionLegal) {
    this.tipoRepresentacionLegal = tipoRepresentacionLegal;
  }


  /**
   * Gets the ultimo.
   *
   * @return the ultimo
   */
  public Boolean getUltimo() {
    return ultimo;
  }

  /**
   * Sets the ultimo.
   *
   * @param ultimo the new ultimo
   */
  public void setUltimo(final Boolean ultimo) {
    this.ultimo = ultimo;
  }

  /**
   * Gets the direccion del representante *.
   *
   * @return the direccion
   */
  public DireccionAdaDTO getDireccionAda() {
    return direccionAda;
  }

  /**
   * Sets the direccion del representante *.
   *
   * @param direccionAda the new direccion del representante *
   */
  public void setDireccionAda(final DireccionAdaDTO direccionAda) {
    this.direccionAda = direccionAda;
  }

  /**
   * Gets the información del representante legal *.
   *
   * @return the persona
   */
  public PersonaAdaDTO getPersona() {
    return persona;
  }

  /**
   * Sets the información del representante legal *.
   *
   * @param persona the persona to set
   */
  public void setPersona(final PersonaAdaDTO persona) {
    this.persona = persona;
  }

  /**
   * Gets the solicitud asociada al representante *.
   *
   * @return the solicitud
   */
  public SolicitudDTO getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud asociada al representante *.
   *
   * @param solicitud the solicitud to set
   */
  public void setSolicitud(final SolicitudDTO solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the unidad tutela *.
   *
   * @return the unidadTutela
   */
  public UnidadTutelaDTO getUnidadTutela() {
    return unidadTutela;
  }

  /**
   * Sets the unidad tutela *.
   *
   * @param unidadTutela the unidadTutela to set
   */
  public void setUnidadTutela(final UnidadTutelaDTO unidadTutela) {
    this.unidadTutela = unidadTutela;
  }

  /**
   * Gets the estado civil del representante *.
   *
   * @return the estadoCivil
   */
  public EstadoCivilDTO getEstadoCivil() {
    return estadoCivil;
  }

  /**
   * Sets the estado civil del representante *.
   *
   * @param estadoCivil the estadoCivil to set
   */
  public void setEstadoCivil(final EstadoCivilDTO estadoCivil) {
    this.estadoCivil = estadoCivil;
  }

  /**
   * Gets the app origen.
   *
   * @return the app origen
   */
  public String getAppOrigen() {
    return appOrigen;
  }

  /**
   * Sets the app origen.
   *
   * @param appOrigen the new app origen
   */
  public void setAppOrigen(final String appOrigen) {
    this.appOrigen = appOrigen;
  }

  /**
   * Compare to.
   *
   * @param arg0 the arg 0
   * @return the int
   */
  @Override
  public int compareTo(final RepresentanteDTO arg0) {
    return 0;
  }
}
