package com.gval.gval.dto.dto.util;

import java.time.LocalDate;

/**
 * Class for testing global variable in Drools engine.
 */
public class OutputDisplay {

  public void showText(final String text) {
    System.out.println("==================================================");
    System.out.println("Texto: " + text + ",  date: " + LocalDate.now());
    System.out.println("==================================================");
  }

}
