package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;
import java.util.List;
import java.util.function.Predicate;

/**
 * The Class PreferenciaDTO.
 */
public class PreferenciaDTO extends BaseDTO
    implements Comparable<PreferenciaDTO> {


  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -1127023570237088905L;

  /** The pk prefsol. */
  private Long pkPrefsol;

  /** The solicitud. */
  private SolicitudDTO solicitud;

  /** The tipo preferencia solicitud. */
  private TipoPreferenciaDTO tipoPreferencia;

  /** The pk documento. */
  private DocumentoDTO documento;

  /** The fecha peticion. */
  private Date fechaPeticion;

  /** The estado. */
  private Boolean estado;

  /** The motivo. */
  private String motivo;

  /** The preferencias catalogo servicio. */
  private List<PreferenciaCatalogoServicioDTO> preferenciasCatalogoServicio;

  /** The app origen. */
  private String appOrigen;

  /** The ultima preferencia. */
  private boolean ultimaPreferenciaExpediente;

  /**
   * Instantiates a new preferencia DTO.
   */
  public PreferenciaDTO() {
    super();
  }

  /**
   * Gets the pk prefsol.
   *
   * @return the pk prefsol
   */
  public Long getPkPrefsol() {
    return pkPrefsol;
  }

  /**
   * Sets the pk prefsol.
   *
   * @param pkPrefsol the new pk prefsol
   */
  public void setPkPrefsol(final Long pkPrefsol) {
    this.pkPrefsol = pkPrefsol;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public SolicitudDTO getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the new solicitud
   */
  public void setSolicitud(final SolicitudDTO solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the tipo preferencia.
   *
   * @return the tipo preferencia
   */
  public TipoPreferenciaDTO getTipoPreferencia() {
    return tipoPreferencia;
  }

  /**
   * Sets the tipo preferencia .
   *
   * @param tipoPreferencia the new tipo preferencia
   */
  public void setTipoPreferencia(final TipoPreferenciaDTO tipoPreferencia) {
    this.tipoPreferencia = tipoPreferencia;
  }

  /**
   * Gets the documento.
   *
   * @return the documento
   */
  public DocumentoDTO getDocumento() {
    return documento;
  }

  /**
   * Sets the documento.
   *
   * @param documento the new documento
   */
  public void setDocumento(final DocumentoDTO documento) {
    this.documento = documento;
  }

  /**
   * Gets the fecha peticion.
   *
   * @return the fecha peticion
   */
  public Date getFechaPeticion() {
    return UtilidadesCommons.cloneDate(fechaPeticion);
  }

  /**
   * Sets the fecha peticion.
   *
   * @param fechaPeticion the new fecha peticion
   */
  public void setFechaPeticion(final Date fechaPeticion) {
    this.fechaPeticion = UtilidadesCommons.cloneDate(fechaPeticion);
  }

  /**
   * Gets the estado.
   *
   * @return the estado
   */
  public Boolean getEstado() {
    return estado;
  }

  /**
   * Sets the estado.
   *
   * @param estado the new estado
   */
  public void setEstado(final Boolean estado) {
    this.estado = estado;
  }

  /**
   * Gets the motivo.
   *
   * @return the motivo
   */
  public String getMotivo() {
    return motivo;
  }

  /**
   * Sets the motivo.
   *
   * @param motivo the new motivo
   */
  public void setMotivo(final String motivo) {
    this.motivo = motivo;
  }

  /**
   * Son nuevas preferencias.
   *
   * @return true, if successful
   */
  public boolean getEsNuevaPreferencia() {
    return !esViejaPreferencia();
  }

  /**
   * Es vieja preferencia.
   *
   * @return true, if successful
   */
  public boolean esViejaPreferencia() {
    return estado == null;
  }

  /**
   * Gets the preferencias catalogo servicio.
   *
   * @return the preferencias catalogo servicio
   */
  public List<PreferenciaCatalogoServicioDTO> getPreferenciasCatalogoServicio() {
    return UtilidadesCommons.collectorsToList(preferenciasCatalogoServicio);
  }

  /**
   * Sets the preferencias catalogo servicio.
   *
   * @param preferenciasCatalogoServicio the new preferencias catalogo servicio
   */
  public void setPreferenciasCatalogoServicio(
      final List<PreferenciaCatalogoServicioDTO> preferenciasCatalogoServicio) {
    this.preferenciasCatalogoServicio =
        UtilidadesCommons.collectorsToList(preferenciasCatalogoServicio);
  }

  /**
   * Gets the primera preferencia catalago servicio.
   *
   * @return the primera preferencia catalago servicio
   */
  public PreferenciaCatalogoServicioDTO getPrimeraPreferenciaCatalagoServicio() {
    return getPreferenciaCatalogoServicio(
        PreferenciaCatalogoServicioDTO::esPrimeraPref);
  }

  /**
   * Gets the segunda preferencia catalago servicio.
   *
   * @return the segunda preferencia catalago servicio
   */
  public PreferenciaCatalogoServicioDTO getSegundaPreferenciaCatalagoServicio() {
    return getPreferenciaCatalogoServicio(
        PreferenciaCatalogoServicioDTO::esSegundaPref);
  }

  /**
   * Gets the preferencia catalogo servicio nombre.
   *
   * @param preferenciaCatalogoServicioDTO the preferencia catalogo servicio DTO
   * @return the preferencia catalogo servicio nombre
   */
  private String getPreferenciaCatalogoServicioNombre(
      final PreferenciaCatalogoServicioDTO preferenciaCatalogoServicioDTO) {
    return preferenciaCatalogoServicioDTO == null ? ""
        : preferenciaCatalogoServicioDTO.getCatalogoServicio().getNombre();
  }

  /**
   * Gets the teleasistencia preferencia catalago servicio.
   *
   * @return the teleasistencia preferencia catalago servicio
   */
  public PreferenciaCatalogoServicioDTO getTeleasistenciaPreferenciaCatalagoServicio() {
    return getPreferenciaCatalogoServicio(
        PreferenciaCatalogoServicioDTO::esTeleasisPref);
  }

  /**
   * Gets the primera preferencia catalogo servicio nombre.
   *
   * @return the primera preferencia catalogo servicio nombre
   */
  public String getPrimeraPreferenciaCatalogoServicioNombre() {
    return getPreferenciaCatalogoServicioNombre(
        getPrimeraPreferenciaCatalagoServicio());
  }


  /**
   * Gets the segunda preferencia catalogo servicio nombre.
   *
   * @return the segunda preferencia catalogo servicio nombre
   */
  public String getSegundaPreferenciaCatalogoServicioNombre() {
    return getPreferenciaCatalogoServicioNombre(
        getSegundaPreferenciaCatalagoServicio());
  }

  /**
   * Gets the preferencia catalogo servicio.
   *
   * @param predicate the predicate
   * @return the preferencia catalogo servicio
   */
  private PreferenciaCatalogoServicioDTO getPreferenciaCatalogoServicio(
      final Predicate<? super PreferenciaCatalogoServicioDTO> predicate) {
    final boolean valoresPorDefecto = true;
    return preferenciasCatalogoServicio.stream().filter(predicate).findFirst()
        .orElse(new PreferenciaCatalogoServicioDTO(valoresPorDefecto));
  }

  /**
   * Gets the app origen.
   *
   * @return the app origen
   */
  public String getAppOrigen() {
    return appOrigen;
  }

  /**
   * Sets the app origen.
   *
   * @param appOrigen the new app origen
   */
  public void setAppOrigen(final String appOrigen) {
    this.appOrigen = appOrigen;
  }


  /**
   * Checks if is ultima preferencia.
   *
   * @return true, if is ultima preferencia
   */
  public boolean isUltimaPreferenciaExpediente() {
    return ultimaPreferenciaExpediente;
  }

  /**
   * Sets the ultima preferencia.
   *
   * @param ultimaPreferencia the new ultima preferencia
   */
  public void setUltimaPreferenciaExpediente(final boolean ultimaPreferencia) {
    this.ultimaPreferenciaExpediente = ultimaPreferencia;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final PreferenciaDTO o) {
    return 0;
  }

}
