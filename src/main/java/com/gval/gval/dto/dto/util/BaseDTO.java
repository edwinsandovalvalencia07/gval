package com.gval.gval.dto.dto.util;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

/**
 * The Class BaseDTO.
 */
public abstract class BaseDTO implements Serializable {
  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;
  
  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }  
}
