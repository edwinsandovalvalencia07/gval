package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * The Class ZonaCoberturaConsulta012DTO.
 */
public class ZonaCoberturaConsulta012DTO extends BaseDTO {
  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 3344650315597853971L;

  /** The poblacion. */
  private String poblacion;

  /** The zona cobertura. */
  private String zonaCobertura;

  /** The telefono entidad local. */
  private String telefonoEntidadLocal;


  /**
   * Instantiates a new zona cobertura consulta 012 DTO.
   */
  public ZonaCoberturaConsulta012DTO() {
    super();
  }

  /**
   * Instantiates a new zona cobertura consulta 012 DTO.
   *
   * @param poblacion the poblacion
   * @param zonaCobertura the zona cobertura
   * @param telefonoEntidadLocal the telefono entidad local
   */
  public ZonaCoberturaConsulta012DTO(final String poblacion,
      final String zonaCobertura, final String telefonoEntidadLocal) {
    super();
    this.poblacion = poblacion;
    this.zonaCobertura = zonaCobertura;
    this.telefonoEntidadLocal = telefonoEntidadLocal;
  }

  /**
   * Gets the poblacion.
   *
   * @return the poblacion
   */
  public String getPoblacion() {
    return poblacion;
  }

  /**
   * Sets the poblacion.
   *
   * @param poblacion the new poblacion
   */
  public void setPoblacion(final String poblacion) {
    this.poblacion = poblacion;
  }

  /**
   * Gets the zona cobertura.
   *
   * @return the zona cobertura
   */
  public String getZonaCobertura() {
    return zonaCobertura;
  }

  /**
   * Sets the zona cobertura.
   *
   * @param zonaCobertura the new zona cobertura
   */
  public void setZonaCobertura(final String zonaCobertura) {
    this.zonaCobertura = zonaCobertura;
  }

  /**
   * Gets the telefono entidad local.
   *
   * @return the telefono entidad local
   */
  public String getTelefonoEntidadLocal() {
    return telefonoEntidadLocal;
  }

  /**
   * Sets the telefono entidad local.
   *
   * @param telefonoEntidadLocal the new telefono entidad local
   */
  public void setTelefonoEntidadLocal(final String telefonoEntidadLocal) {
    this.telefonoEntidadLocal = telefonoEntidadLocal;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
