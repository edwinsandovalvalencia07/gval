package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * The Class DetalleConsulta012DTO.
 */
public class DetalleConsulta012DTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 3344650315597853971L;

  /** The pk expedien. */
  private Long pkExpedien;

  /** The dni. */
  private String dni;

  /** The nombre. */
  private String nombre;

  /** The apellido 1. */
  private String primerApellido;

  /** The apellido 2. */
  private String segundoApellido;

  /** The expediente. */
  private String expediente;

  /** The poblacion. */
  private String poblacion;

  /** The entidad local referencia. */
  private String entidadLocalReferencia;

  /** The telefono entidad local. */
  private String telefonoEntidadLocal;

  /** The estado expediente. */
  private String estadoExpediente;

  /** The grabada. */
  private Long grabada;

  /** The valorada. */
  private Long valorada;

  /** The requerimientos. */
  private Long requerimientos;

  /** The res gn notificada. */
  private String resGnNotificada;

  /** The res PIA notificada. */
  private String resPIANotificada;

  /** The estado desc. */
  private String estadoDesc;

  /** The res pia desc. */
  private String resPiaDesc;

  /** The ultimo grado. */
  private String ultimoGrado;

  /** The pia 1. */
  private String primerPia;

  /** The pia 2. */
  private String segundoPia;

  /** The teleasistencia. */
  private String teleasistencia;

  /** The zona cobertura. */
  private String zonaCobertura;

  /** The poblacion zona cobertura. */
  private PoblacionDTO poblacionZonaCobertura;

  /**
   * Instantiates a new detalle consulta 012 DTO.
   */
  public DetalleConsulta012DTO() {
    super();
  }

  /**
   * Gets the pk expedien.
   *
   * @return the pk expedien
   */
  public Long getPkExpedien() {
    return pkExpedien;
  }

  /**
   * Sets the pk expedien.
   *
   * @param pkExpedien the new pk expedien
   */
  public void setPkExpedien(final Long pkExpedien) {
    this.pkExpedien = pkExpedien;
  }

  /**
   * Gets the dni.
   *
   * @return the dni
   */
  public String getDni() {
    return dni;
  }

  /**
   * Sets the dni.
   *
   * @param dni the new dni
   */
  public void setDni(final String dni) {
    this.dni = dni;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the primer apellido.
   *
   * @return the primer apellido
   */
  public String getPrimerApellido() {
    return primerApellido;
  }

  /**
   * Sets the primer apellido.
   *
   * @param primerApellido the new primer apellido
   */
  public void setPrimerApellido(final String primerApellido) {
    this.primerApellido = primerApellido;
  }

  /**
   * Gets the segundo apellido.
   *
   * @return the segundo apellido
   */
  public String getSegundoApellido() {
    return segundoApellido;
  }

  /**
   * Sets the segundo apellido.
   *
   * @param segundoApellido the new segundo apellido
   */
  public void setSegundoApellido(final String segundoApellido) {
    this.segundoApellido = segundoApellido;
  }

  /**
   * Gets the expediente.
   *
   * @return the expediente
   */
  public String getExpediente() {
    return expediente;
  }

  /**
   * Sets the expediente.
   *
   * @param expediente the new expediente
   */
  public void setExpediente(final String expediente) {
    this.expediente = expediente;
  }

  /**
   * Gets the poblacion.
   *
   * @return the poblacion
   */
  public String getPoblacion() {
    return poblacion;
  }

  /**
   * Sets the poblacion.
   *
   * @param poblacion the new poblacion
   */
  public void setPoblacion(final String poblacion) {
    this.poblacion = poblacion;
  }

  /**
   * Gets the entidad local referencia.
   *
   * @return the entidad local referencia
   */
  public String getEntidadLocalReferencia() {
    return entidadLocalReferencia;
  }

  /**
   * Sets the entidad local referencia.
   *
   * @param entidadLocalReferencia the new entidad local referencia
   */
  public void setEntidadLocalReferencia(final String entidadLocalReferencia) {
    this.entidadLocalReferencia = entidadLocalReferencia;
  }

  /**
   * Gets the telefono entidad local.
   *
   * @return the telefono entidad local
   */
  public String getTelefonoEntidadLocal() {
    return telefonoEntidadLocal;
  }

  /**
   * Sets the telefono entidad local.
   *
   * @param telefonoEntidadLocal the new telefono entidad local
   */
  public void setTelefonoEntidadLocal(final String telefonoEntidadLocal) {
    this.telefonoEntidadLocal = telefonoEntidadLocal;
  }

  /**
   * Gets the estado expediente.
   *
   * @return the estado expediente
   */
  public String getEstadoExpediente() {
    return estadoExpediente;
  }

  /**
   * Sets the estado expediente.
   *
   * @param estadoExpediente the new estado expediente
   */
  public void setEstadoExpediente(final String estadoExpediente) {
    this.estadoExpediente = estadoExpediente;
  }

  /**
   * Gets the grabada.
   *
   * @return the grabada
   */
  public Long getGrabada() {
    return grabada;
  }

  /**
   * Sets the grabada.
   *
   * @param grabada the new grabada
   */
  public void setGrabada(final Long grabada) {
    this.grabada = grabada;
  }

  /**
   * Gets the valorada.
   *
   * @return the valorada
   */
  public Long getValorada() {
    return valorada;
  }

  /**
   * Sets the valorada.
   *
   * @param valorada the new valorada
   */
  public void setValorada(final Long valorada) {
    this.valorada = valorada;
  }

  /**
   * Gets the requerimientos.
   *
   * @return the requerimientos
   */
  public Long getRequerimientos() {
    return requerimientos;
  }

  /**
   * Sets the requerimientos.
   *
   * @param requerimientos the new requerimientos
   */
  public void setRequerimientos(final Long requerimientos) {
    this.requerimientos = requerimientos;
  }

  /**
   * Gets the res gn notificada.
   *
   * @return the res gn notificada
   */
  public String getResGnNotificada() {
    return resGnNotificada;
  }

  /**
   * Sets the res gn notificada.
   *
   * @param resGnNotificada the new res gn notificada
   */
  public void setResGnNotificada(final String resGnNotificada) {
    this.resGnNotificada = resGnNotificada;
  }

  /**
   * Gets the res PIA notificada.
   *
   * @return the res PIA notificada
   */
  public String getResPIANotificada() {
    return resPIANotificada;
  }

  /**
   * Sets the res PIA notificada.
   *
   * @param resPIANotificada the new res PIA notificada
   */
  public void setResPIANotificada(final String resPIANotificada) {
    this.resPIANotificada = resPIANotificada;
  }

  /**
   * Gets the estado desc.
   *
   * @return the estado desc
   */
  public String getEstadoDesc() {
    return estadoDesc;
  }

  /**
   * Sets the estado desc.
   *
   * @param estadoDesc the new estado desc
   */
  public void setEstadoDesc(final String estadoDesc) {
    this.estadoDesc = estadoDesc;
  }

  /**
   * Gets the res pia desc.
   *
   * @return the res pia desc
   */
  public String getResPiaDesc() {
    return resPiaDesc;
  }

  /**
   * Sets the res pia desc.
   *
   * @param resPiaDesc the new res pia desc
   */
  public void setResPiaDesc(final String resPiaDesc) {
    this.resPiaDesc = resPiaDesc;
  }

  /**
   * Gets the ultimo grado.
   *
   * @return the ultimo grado
   */
  public String getUltimoGrado() {
    return ultimoGrado;
  }

  /**
   * Sets the ultimo grado.
   *
   * @param ultimoGrado the new ultimo grado
   */
  public void setUltimoGrado(final String ultimoGrado) {
    this.ultimoGrado = ultimoGrado;
  }

  /**
   * Gets the primer pia.
   *
   * @return the primer pia
   */
  public String getPrimerPia() {
    return primerPia;
  }

  /**
   * Sets the primer pia.
   *
   * @param primerPia the new primer pia
   */
  public void setPrimerPia(final String primerPia) {
    this.primerPia = primerPia;
  }

  /**
   * Gets the segundo pia.
   *
   * @return the segundo pia
   */
  public String getSegundoPia() {
    return segundoPia;
  }

  /**
   * Sets the segundo pia.
   *
   * @param segundoPia the new segundo pia
   */
  public void setSegundoPia(final String segundoPia) {
    this.segundoPia = segundoPia;
  }

  /**
   * Gets the teleasistencia.
   *
   * @return the teleasistencia
   */
  public String getTeleasistencia() {
    return teleasistencia;
  }

  /**
   * Sets the teleasistencia.
   *
   * @param teleasistencia the new teleasistencia
   */
  public void setTeleasistencia(final String teleasistencia) {
    this.teleasistencia = teleasistencia;
  }

  /**
   * Gets the zona cobertura.
   *
   * @return the zona cobertura
   */
  public String getZonaCobertura() {
    return zonaCobertura;
  }

  /**
   * Sets the zona cobertura.
   *
   * @param zonaCobertura the new zona cobertura
   */
  public void setZonaCobertura(final String zonaCobertura) {
    this.zonaCobertura = zonaCobertura;
  }

  /**
   * Gets the poblacion zona cobertura.
   *
   * @return the poblacion zona cobertura
   */
  public PoblacionDTO getPoblacionZonaCobertura() {
    return poblacionZonaCobertura;
  }

  /**
   * Sets the poblacion zona cobertura.
   *
   * @param poblacionZonaCobertura the new poblacion zona cobertura
   */
  public void setPoblacionZonaCobertura(
      final PoblacionDTO poblacionZonaCobertura) {
    this.poblacionZonaCobertura = poblacionZonaCobertura;
  }

  /**
   * Gets the beneficiario.
   *
   * @return the beneficiario
   */
  public String getBeneficiario() {
    return nombre + " " + primerApellido + " " + segundoApellido;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
