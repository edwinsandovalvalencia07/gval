package com.gval.gval.dto.dto;

import java.util.Date;
import java.util.List;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

/**
 * The Class DictamenDTO.
 */
public class DictamenDTO extends BaseDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2104131145705907774L;

	/** The pk dictamen. */
	private Long pkDictamen;

	/** The activo. */
	private Boolean activo;

	/** The discapacidad fisica. */
	private Boolean discapacidadFisica;

	/** The discapacidad psiquica. */
	private Boolean discapacidadPsiquica;

	/** The discapacidad sensorial. */
	private Boolean discapacidadSensorial;

	/** The enfermedad mental. */
	private Boolean enfermedadMental;

	/** The fecha revisable. */
	private Date fechaRevisable;

	/** The grado. */
	private String grado;

	/** The nivel. */
	private String nivel;

	/** The otros cuidados. */
	private String otrosCuidados;

	/** The puntuacion. */
	private Long puntuacion;

	/** The pluripatologia. */
	private Boolean pluripatologia;

	private EstudioDTO estudio;

	private List<DiagdictDTO> diagnosticoDictamenList;

	/**
	 * Instantiates a new dictamen.
	 */
	public DictamenDTO() {
		super();
	}

	/**
	 * @return the pkDictamen
	 */
	public Long getPkDictamen() {
		return pkDictamen;
	}

	/**
	 * @param pkDictamen the pkDictamen to set
	 */
	public void setPkDictamen(Long pkDictamen) {
		this.pkDictamen = pkDictamen;
	}

	/**
	 * Gets the activo.
	 *
	 * @return the activo
	 */
	public Boolean getActivo() {
		return activo;
	}

	/**
	 * Sets the activo.
	 *
	 * @param activo the activo to set
	 */
	public void setActivo(final Boolean activo) {
		this.activo = activo;
	}

	/**
	 * Gets the discapacidad fisica.
	 *
	 * @return the discapacidadFisica
	 */
	public Boolean getDiscapacidadFisica() {
		return discapacidadFisica;
	}

	/**
	 * Sets the discapacidad fisica.
	 *
	 * @param discapacidadFisica the discapacidadFisica to set
	 */
	public void setDiscapacidadFisica(final Boolean discapacidadFisica) {
		this.discapacidadFisica = discapacidadFisica;
	}

	/**
	 * Gets the discapacidad psiquica.
	 *
	 * @return the discapacidadPsiquica
	 */
	public Boolean getDiscapacidadPsiquica() {
		return discapacidadPsiquica;
	}

	/**
	 * Sets the discapacidad psiquica.
	 *
	 * @param discapacidadPsiquica the discapacidadPsiquica to set
	 */
	public void setDiscapacidadPsiquica(final Boolean discapacidadPsiquica) {
		this.discapacidadPsiquica = discapacidadPsiquica;
	}

	/**
	 * Gets the discapacidad sensorial.
	 *
	 * @return the discapacidadSensorial
	 */
	public Boolean getDiscapacidadSensorial() {
		return discapacidadSensorial;
	}

	/**
	 * Sets the discapacidad sensorial.
	 *
	 * @param discapacidadSensorial the discapacidadSensorial to set
	 */
	public void setDiscapacidadSensorial(final Boolean discapacidadSensorial) {
		this.discapacidadSensorial = discapacidadSensorial;
	}

	/**
	 * Gets the enfermedad mental.
	 *
	 * @return the enfermedadMental
	 */
	public Boolean getEnfermedadMental() {
		return enfermedadMental;
	}

	/**
	 * Sets the enfermedad mental.
	 *
	 * @param enfermedadMental the enfermedadMental to set
	 */
	public void setEnfermedadMental(final Boolean enfermedadMental) {
		this.enfermedadMental = enfermedadMental;
	}

	/**
	 * Gets the fecha revisable.
	 *
	 * @return the fechaRevisable
	 */
	public Date getFechaRevisable() {
		return UtilidadesCommons.cloneDate(fechaRevisable);
	}

	/**
	 * Sets the fecha revisable.
	 *
	 * @param fechaRevisable the fechaRevisable to set
	 */
	public void setFechaRevisable(final Date fechaRevisable) {
		this.fechaRevisable = UtilidadesCommons.cloneDate(fechaRevisable);
	}

	/**
	 * Gets the grado.
	 *
	 * @return the grado
	 */
	public String getGrado() {
		return grado;
	}

	/**
	 * Sets the grado.
	 *
	 * @param grado the grado to set
	 */
	public void setGrado(final String grado) {
		this.grado = grado;
	}

	/**
	 * Gets the nivel.
	 *
	 * @return the nivel
	 */
	public String getNivel() {
		return nivel;
	}

	/**
	 * Sets the nivel.
	 *
	 * @param nivel the nivel to set
	 */
	public void setNivel(final String nivel) {
		this.nivel = nivel;
	}

	/**
	 * Gets the otros cuidados.
	 *
	 * @return the otrosCuidados
	 */
	public String getOtrosCuidados() {
		return otrosCuidados;
	}

	/**
	 * Sets the otros cuidados.
	 *
	 * @param otrosCuidados the otrosCuidados to set
	 */
	public void setOtrosCuidados(final String otrosCuidados) {
		this.otrosCuidados = otrosCuidados;
	}

	/**
	 * @return the puntuacion
	 */
	public Long getPuntuacion() {
		return puntuacion;
	}

	/**
	 * @param puntuacion the puntuacion to set
	 */
	public void setPuntuacion(Long puntuacion) {
		this.puntuacion = puntuacion;
	}

	/**
	 * Gets the pluripatologia.
	 *
	 * @return the pluripatologia
	 */
	public Boolean getPluripatologia() {
		return pluripatologia;
	}

	/**
	 * Sets the pluripatologia.
	 *
	 * @param pluripatologia the pluripatologia to set
	 */
	public void setPluripatologia(final Boolean pluripatologia) {
		this.pluripatologia = pluripatologia;
	}

	public EstudioDTO getEstudio() {
		return estudio;
	}

	public void setEstudio(EstudioDTO estudio) {
		this.estudio = estudio;
	}

	public List<DiagdictDTO> getDiagnosticoDictamenList() {
		return diagnosticoDictamenList;
	}

	public void setDiagnosticoDictamenList(List<DiagdictDTO> diagnosticoDictamenList) {
		this.diagnosticoDictamenList = diagnosticoDictamenList;
	}

}
