package com.gval.gval.dto.dto;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.gval.gval.common.ConstantesCommons;
import com.gval.gval.common.ConstantesValoraciones;
import com.gval.gval.common.ConstantesWeb;
import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.common.enums.ValoracionesTipoEnum;
import com.gval.gval.dto.dto.util.BaseDTO;
import jakarta.annotation.Nullable;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.time.DateUtils;


// TODO: Auto-generated Javadoc
/**
 * The Class ValoracionDTO.
 */
public final class ValoracionDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -2781429829187642184L;

  /** The pk valoracion. */
  private Long pkValoracion;

  /** The migrado sidep. */
  @Size(max = 1)
  private String migradoSidep;

  /** The documento valoracion. */
  private Long documentoValoracion;

  /** The pk grupo valoracion. */
  private Long pkGrupoValoracion;

  /** The activo. */
  @NotNull
  private Boolean activo;

  /** The cerrado. */
  @NotNull
  private Boolean cerrado;

  /** The fecha creacion. */
  @NotNull
  private Date fechaCreacion;

  /** The fecha real. */
  private Date fechaReal;

  /** The grado. */
  @Size(max = 1)
  private String grado;

  /** The nivel. */
  @Size(max = 1)
  private String nivel;

  /** The tipo valoracion. */
  @Size(max = 3)
  private String tipoValoracion;

  /** The sub tipo valoracion. */
  private String subtipoValoracion;

  /** The version. */
  @Size(max = 50)
  private String version;

  /** The asignar solicitud. */
  @NotNull
  private AsignarSolicitudDTO asignarSolicitud;

  /** The documento generado. */
  private DocumentoGeneradoDTO documentoGenerado;

  /** The usuario. */
  @NotNull
  private UsuarioDTO usuario;

  /** The puntuacion. */
  private Long puntuacion;

  /** The app origen. */
  private String appOrigen;

  /**
   * Instantiates a new valoracion DTO.
   */
  public ValoracionDTO() {
    super();
    this.activo = Boolean.TRUE;
    this.cerrado = Boolean.FALSE;
//    this.fechaCreacion = Utilidades.getFechaCorrectamente(new Date());
    this.asignarSolicitud = new AsignarSolicitudDTO();
    this.usuario = new UsuarioDTO();
    this.version = ConstantesWeb.VALORACION_VERSION_DEFECTO;
    this.migradoSidep = ConstantesCommons.VALOR_STRING_BBBDD_FALSE;
  }


  /**
   * On pre persist.
   */
  @PrePersist
  public void onPrePersist() {
    appOrigen = ConstantesCommons.APLICACION_ADA3;
  }

  /**
   * On pre update.
   */
  @PreUpdate
  public void onPreUpdate() {
    appOrigen = ConstantesCommons.APLICACION_ADA3;
  }


  /**
   * Configura inicial.
   *
   * @param tipoValoracion the tipo valoracion
   */
  public void configuraInicial(final String tipoValoracion) {
    if (StringUtils.isEmpty(this.tipoValoracion)) {
      this.tipoValoracion = tipoValoracion;
    }
    if (StringUtils.isEmpty(this.subtipoValoracion) && StringUtils
        .equals(this.tipoValoracion, ValoracionesTipoEnum.BVD.id)) {
      this.subtipoValoracion = ConstantesValoraciones.BAREMO_GENERAL;
    }
  }

  /**
   * Cerrar.
   */
  public void cerrar() {
    if (!this.cerrado) {
      this.fechaReal = DateUtils.truncate(new Date(), Calendar.DATE);
      this.version = ConstantesWeb.VALORACION_VERSION_DEFECTO;
    }
    this.cerrado = Boolean.TRUE;
  }

  /**
   * Abrir.
   */
  public void abrir() {
    this.fechaReal = null;
    this.cerrado = Boolean.FALSE;
  }

  /**
   * Gets the solicitante.
   *
   * @return the solicitante
   */
  public PersonaAdaDTO getSolicitante() {
    if (getAsignarSolicitud() != null
        && getAsignarSolicitud().getSolicitud() != null
        && getAsignarSolicitud().getSolicitud().getExpediente() != null) {
      return this.getAsignarSolicitud().getSolicitud().getExpediente()
          .getSolicitante();
    }
    return new PersonaAdaDTO();
  }

  /**
   * Gets the fecha cita valoracion. Este método se usa para obtener la edad en
   * meses del solicitante en las reglas (leyes y baremos).
   *
   * @return the fecha cita valoracion
   */
  @Nullable
  public Date getFechaCitaValoracion() {
    if (asignarSolicitud != null && asignarSolicitud.getFechaCita() != null) {
      return new Date(asignarSolicitud.getFechaCita().getTime());
    }
    return null;
  }

  /**
   * Edad en meses que ten�a el solicitante en la fecha de valoración.
   *
   * @return the int
   */
//  public int getEdadEnMesesEnFechaValoracion() {
//    final PersonaAdaDTO solicitante = getSolicitante();
//    if (solicitante.getFechaNacimiento() == null) {
//      return -1;
//    }
//    final LocalDate fechaNacimiento =
//        new Date(solicitante.getFechaNacimiento().getTime()).toInstant()
//            .atZone(ZoneId.systemDefault()).toLocalDate();
//    final LocalDate fechaValoracion =
//        new Date(this.getFechaCitaValoracion().getTime()).toInstant()
//            .atZone(ZoneId.systemDefault()).toLocalDate();
//    return FechasCalculator.calcularMeses(fechaNacimiento, fechaValoracion);
//  }

  /**
   * Gets the pk valoracion.
   *
   * @return the pkValoracion
   */
  public Long getPkValoracion() {
    return pkValoracion;
  }

  /**
   * Sets the pk valoracion.
   *
   * @param pkValoracion the pkValoracion to set
   */
  public void setPkValoracion(final Long pkValoracion) {
    this.pkValoracion = pkValoracion;
  }

  /**
   * Gets the migrado sidep.
   *
   * @return the migradoSidep
   */
  public String getMigradoSidep() {
    return migradoSidep;
  }

  /**
   * Sets the migrado sidep.
   *
   * @param migradoSidep the migradoSidep to set
   */
  public void setMigradoSidep(final String migradoSidep) {
    this.migradoSidep = migradoSidep;
  }

  /**
   * Gets the documento valoracion.
   *
   * @return the documentoValoracion
   */
  public Long getDocumentoValoracion() {
    return documentoValoracion;
  }

  /**
   * Sets the documento valoracion.
   *
   * @param documentoValoracion the documentoValoracion to set
   */
  public void setDocumentoValoracion(final Long documentoValoracion) {
    this.documentoValoracion = documentoValoracion;
  }

  /**
   * Gets the pk grupo valoracion.
   *
   * @return the pkGrupoValoracion
   */
  public Long getPkGrupoValoracion() {
    return pkGrupoValoracion;
  }

  /**
   * Sets the pk grupo valoracion.
   *
   * @param pkGrupoValoracion the pkGrupoValoracion to set
   */
  public void setPkGrupoValoracion(final Long pkGrupoValoracion) {
    this.pkGrupoValoracion = pkGrupoValoracion;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the cerrado.
   *
   * @return the cerrado
   */
  public Boolean getCerrado() {
    return cerrado;
  }

  /**
   * Sets the cerrado.
   *
   * @param cerrado the cerrado to set
   */
  public void setCerrado(final Boolean cerrado) {
    this.cerrado = cerrado;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
//  public String getFechaCreacionFormateada() {
//    return fechaCreacion == null ? ""
//        : new SimpleDateFormat(Utilidades.FORMAT_DATE_FECHA)
//            .format(fechaCreacion);
//  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha real.
   *
   * @return the fechaReal
   */
  public Date getFechaReal() {
    return UtilidadesCommons.cloneDate(fechaReal);
  }

  /**
   * Gets the fecha R real.
   *
   * @return the fecha R real
   */
//  @Nullable
//  public String getFechaCierreFormateada() {
//    if (cerrado && fechaReal != null) {
//      return new SimpleDateFormat(Utilidades.FORMAT_DATE_FECHA)
//          .format(fechaReal);
//    }
//    return null;
//  }

  /**
   * Gets the fecha valoracion formateada.
   *
   * @return the fecha valoracion formateada
   */
//  @Nullable
//  public String getFechaValoracionFormateada() {
//    if (StringUtils.equals(ConstantesWeb.VALORACION_VERSION_DEFECTO,
//        this.version) && asignarSolicitud != null
//        && asignarSolicitud.getFechaCita() != null) {
//      return new SimpleDateFormat(Utilidades.FORMAT_DATE_FECHA)
//          .format(asignarSolicitud.getFechaCita());
//    }
//    return null;
//  }

  /**
   * Sets the fecha real.
   *
   * @param fechaReal the fechaReal to set
   */
  public void setFechaReal(final Date fechaReal) {
    this.fechaReal = UtilidadesCommons.cloneDate(fechaReal);
  }

  /**
   * Gets the grado.
   *
   * @return the grado
   */
  public String getGrado() {
    return grado;
  }

  /**
   * Sets the grado.
   *
   * @param grado the grado to set
   */
  public void setGrado(final String grado) {
    this.grado = grado;
  }

  /**
   * Gets the nivel.
   *
   * @return the nivel
   */
  public String getNivel() {
    return nivel;
  }

  /**
   * Sets the nivel.
   *
   * @param nivel the nivel to set
   */
  public void setNivel(final String nivel) {
    this.nivel = nivel;
  }

  /**
   * Gets the tipo valoracion.
   *
   * @return the tipoValoracion
   */
  public String getTipoValoracion() {
    return tipoValoracion;
  }

  /**
   * Sets the tipo valoracion.
   *
   * @param tipoValoracion the tipoValoracion to set
   */
  public void setTipoValoracion(final String tipoValoracion) {
    this.tipoValoracion = tipoValoracion;
  }

  /**
   * Gets the version.
   *
   * @return the version
   */
  public String getVersion() {
    return version;
  }

  /**
   * Sets the version.
   *
   * @param version the version to set
   */
  public void setVersion(final String version) {
    this.version = version;
  }

  /**
   * Gets the asignar solicitud.
   *
   * @return the asignarSolicitud
   */
  public AsignarSolicitudDTO getAsignarSolicitud() {
    return asignarSolicitud;
  }

  /**
   * Sets the asignar solicitud.
   *
   * @param asignarSolicitud the asignarSolicitud to set
   */
  public void setAsignarSolicitud(final AsignarSolicitudDTO asignarSolicitud) {
    this.asignarSolicitud = asignarSolicitud;
  }

  /**
   * Gets the documento generado.
   *
   * @return the documentoGenerado
   */
  public DocumentoGeneradoDTO getDocumentoGenerado() {
    return documentoGenerado;
  }

  /**
   * Sets the documento generado.
   *
   * @param documentoGenerado the documentoGenerado to set
   */
  public void setDocumentoGenerado(
      final DocumentoGeneradoDTO documentoGenerado) {
    this.documentoGenerado = documentoGenerado;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the usuario to set
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the subtipo valoracion.
   *
   * @return the subtipo valoracion
   */
  public String getSubtipoValoracion() {
    return subtipoValoracion;
  }

  /**
   * Sets the subtipo valoracion.
   *
   * @param subtipoValoracion the new subtipo valoracion
   */
  public void setSubtipoValoracion(final String subtipoValoracion) {
    this.subtipoValoracion = subtipoValoracion;
  }

  /**
   * Gets the puntuacion.
   *
   * @return the puntuacion
   */
  public Long getPuntuacion() {
    return puntuacion;
  }

  /**
   * Sets the puntuacion.
   *
   * @param puntuacion the new puntuacion
   */
  public void setPuntuacion(final Long puntuacion) {
    this.puntuacion = puntuacion;
  }

  /**
   * Gets the app origen.
   *
   * @return the app origen
   */
  public String getAppOrigen() {
    return appOrigen;
  }

  /**
   * Sets the app origen.
   *
   * @param appOrigen the new app origen
   */
  public void setAppOrigen(final String appOrigen) {
    this.appOrigen = appOrigen;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
