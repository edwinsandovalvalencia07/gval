/**
 * Copyright (c) 2020 Generalitat Valenciana - Todos los derechos reservados.
 */
package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import es.gva.dependencia.ada.common.enums.TipoOrigenEnum;
import com.gval.gval.dto.dto.util.BaseDTO;
import es.gva.dependencia.ada.util.Utilidades;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;



/**
 * The Class CuidadorInformeSocialDTO.
 */
public class CuidadorInformeSocialDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 3344650315597853971L;

  /** The pk cnp infosoci. */
  private Long pkCnpInfosoci;

  /** The activo. */
  @NotNull
  private Boolean activo;

  /** The fecha creacion. */
  @NotNull
  private Date fechaCreacion;

  /** The idoneo. */
  @NotNull
  private Boolean idoneo;

  /** The motivo. */
  @NotNull
  @Size(max = 250)
  private String motivo;

  /** The verificado. */
  @NotNull
  private Boolean verificado;

  /** The origen. */
  private String origen;

  /** The cuidador. */
  @NotNull
  private CuidadorDTO cuidador;

  /** The informe social. */
  @NotNull
  private InformeSocialDTO informeSocial;

  /** The asociado IS. */
  private Boolean asociadoIS;

  /**
   * Instantiates a new cuidador informe social DTO.
   */
  public CuidadorInformeSocialDTO() {
    super();
    this.activo = Boolean.TRUE;
    this.fechaCreacion = Utilidades.getFechaCorrectamente(new Date());
    this.cuidador = new CuidadorDTO();
    this.informeSocial = new InformeSocialDTO();
  }

  /**
   * Instantiates a new cuidador informe social DTO.
   *
   * @param cuidadorDTO the cuidador DTO
   * @param informeSocialDTO the informe social DTO
   * @param origen the origen
   */
  public CuidadorInformeSocialDTO(final CuidadorDTO cuidadorDTO,
      final InformeSocialDTO informeSocialDTO, final TipoOrigenEnum origen) {
    this();
    this.cuidador = cuidadorDTO;
    this.informeSocial = informeSocialDTO;
    this.origen = origen.getCodigo();
  }

  /**
   * Gets the pk cnp infosoci.
   *
   * @return the pkCnpInfosoci
   */
  public Long getPkCnpInfosoci() {
    return pkCnpInfosoci;
  }

  /**
   * Sets the pk cnp infosoci.
   *
   * @param pkCnpInfosoci the pkCnpInfosoci to set
   */
  public void setPkCnpInfosoci(final Long pkCnpInfosoci) {
    this.pkCnpInfosoci = pkCnpInfosoci;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the idoneo.
   *
   * @return the idoneo
   */
  public Boolean getIdoneo() {
    return idoneo;
  }

  /**
   * Sets the idoneo.
   *
   * @param idoneo the idoneo to set
   */
  public void setIdoneo(final Boolean idoneo) {
    this.idoneo = idoneo;
  }

  /**
   * Gets the motivo.
   *
   * @return the motivo
   */
  public String getMotivo() {
    return motivo;
  }

  /**
   * Sets the motivo.
   *
   * @param motivo the new motivo
   */
  public void setMotivo(final String motivo) {
    this.motivo = motivo;
  }

  /**
   * Gets the cuidador.
   *
   * @return the cuidador
   */
  public CuidadorDTO getCuidador() {
    return cuidador;
  }

  /**
   * Sets the cuidador.
   *
   * @param cuidador the cuidador to set
   */
  public void setCuidador(final CuidadorDTO cuidador) {
    this.cuidador = cuidador;
  }

  /**
   * Gets the informe social.
   *
   * @return the informeSocial
   */
  public InformeSocialDTO getInformeSocial() {
    return informeSocial;
  }

  /**
   * Sets the informe social.
   *
   * @param informeSocial the informeSocial to set
   */
  public void setInformeSocial(final InformeSocialDTO informeSocial) {
    this.informeSocial = informeSocial;
  }

  /**
   * Gets the origen.
   *
   * @return the origen
   */
  public String getOrigen() {
    return origen;
  }

  /**
   * Sets the origen.
   *
   * @param origen the new origen
   */
  public void setOrigen(final String origen) {
    this.origen = origen;
  }


  /**
   * Tiene origen tipo.
   *
   * @param tipo the tipo
   * @return true, if successful
   */
  public boolean tieneOrigenTipo(final TipoOrigenEnum tipo) {
    return tipo.getCodigo().equals(origen);
  }

  /**
   * Gets the verificado.
   *
   * @return the verificado
   */
  public Boolean getVerificado() {
    return verificado;
  }

  /**
   * Sets the verificado.
   *
   * @param verificado the new verificado
   */
  public void setVerificado(final Boolean verificado) {
    this.verificado = verificado;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

  /**
   * Gets the asociado IS.
   *
   * @return the asociado IS
   */
  public Boolean getAsociadoIS() {
    return asociadoIS;
  }

  /**
   * Sets the asociado IS.
   *
   * @param asociadoIS the new asociado IS
   */
  public void setAsociadoIS(final Boolean asociadoIS) {
    this.asociadoIS = asociadoIS;
  }

}
