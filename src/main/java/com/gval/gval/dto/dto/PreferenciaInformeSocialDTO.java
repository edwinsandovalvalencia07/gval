package com.gval.gval.dto.dto;

import es.gva.dependencia.ada.common.enums.ModalidadEnum;
import es.gva.dependencia.ada.common.enums.TipoCatalogoServicioEnum;
import es.gva.dependencia.ada.common.enums.TipoPreferenciaInformeSocialEnum;
import es.gva.dependencia.ada.common.enums.listados.IntensidadEnum;
import es.gva.dependencia.ada.common.enums.listados.TipoModalidadEnum;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang.BooleanUtils;
import org.zkoss.util.resource.Labels;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * The Class PreferenciaInformeSocialDTO.
 */
public final class PreferenciaInformeSocialDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 4983437657340003241L;

  /** The pk preferencia. */
  private Long pkPreferencia;

  /** The codigo primer centro. */
  @Size(max = 6)
  private String codigoPrimerCentro;

  /** The codigo segundo centro. */
  @Size(max = 6)
  private String codigoSegundoCentro;

  /** The codigo tercer centro. */
  @Size(max = 6)
  private String codigoTercerCentro;

  /** The nombre primer centro. */
  @Size(max = 100)
  private String nombrePrimerCentro;

  /** The nombre segundo centro. */
  @Size(max = 100)
  private String nombreSegundoCentro;

  /** The nombre tercer centro. */
  @Size(max = 100)
  private String nombreTercerCentro;

  /** The orden profesional. */
  private Long ordenProfesional;

  /** The orden solicitud. */
  private Long ordenSolicitud;

  /** The tipo preferencia. */
  private Long tipoPreferencia;

  /** The tipo. */
  @NotNull
  private boolean nuevaPreferencia;

  /** The tipo modalidad. */
  @Size(max = 1)
  private String tipoModalidad;

  /** The intensidad. */
  @Size(max = 1)
  private String intensidad;

  /** The informe social. */
  @NotNull
  private InformeSocialDTO informeSocial;

  /** The preferencia catalogo servicio. */
  private PreferenciaCatalogoServicioDTO preferenciaCatalogoServicio;

  /** The catalogo servicio. */
  @NotNull
  private CatalogoServicioDTO catalogoServicio;

  /** The recurso prescripcion. */
  private CatalogoServicioDTO recursoPrescripcion;

  /** The modalidad teleasistencia. */
  private Long modalidadTeleasistencia;

  /** The motivo movil. */
  private Long motivoMovil;

  /** The motivo no idoneidad. */
  private String motivoNoIdoneidad;

  /** The recurso idoneo. */
  private Boolean recursoIdoneo;

  /** The no idoneo informado. */
  private Boolean noIdoneoInformado;

  /** The catalogo explicado. */
  private Boolean catalogoExplicado;

  private Boolean continuarPrestacion;

  /** The solicita internamiento. */
  private Boolean solicitaInternamiento;

  /** The informado fiscalia. */
  private Boolean informadoFiscalia;

  /** The informado fiscalia. */
  private Boolean ingresoVoluntario;

  /** The recurso alternativo. */
  @Size(max = 250)
  private String recursoAdecuadoValoracion;

  /** The aumento horas. */
  private Boolean aumentoHoras;

  /** The tipo centro. */
  private Long tipoCentroPreferencia;

  // IT de Seguimiento PIA AP

  /** The incumple proyecto AP. */
  private Boolean incumpleProyectoAP;

  /** The incumple requisitos AP. */
  private Boolean incumpleRequisitosAP;

  /** The incumple persona dep. */
  private Boolean incumplePersonaDep;

  /** The cambio situacion persona. */
  private Boolean cambioSituacionPersona;

  /** The motivo inclumple PVI. */
  private Boolean otrosIncumplePVI;

  /** The otros motivos incumple PVI. */
  private String otrosMotivosIncumplePVI;

  /** The cambio permite PVI. */
  private Boolean cambioPermitePVI;


  /**
   * Instantiates a new preferencia informe social DTO.
   */
  public PreferenciaInformeSocialDTO() {
    super();
    nuevaPreferencia = true;
    informeSocial = new InformeSocialDTO();
    catalogoServicio = new CatalogoServicioDTO();
  }

  /**
   * Instantiates a new preferencia informe social DTO.
   *
   * @param preferenciaCatalogoServicio the preferencia catalogo servicio
   * @param coincidePreferencias the coincide preferencias
   */
  public PreferenciaInformeSocialDTO(
      final PreferenciaCatalogoServicioDTO preferenciaCatalogoServicio,
      final boolean coincidePreferencias) {
    this();
    this.tipoPreferencia =
        TipoPreferenciaInformeSocialEnum.PREFERENCIA_SOLICITANTE.getValor();
    this.catalogoServicio = preferenciaCatalogoServicio.getCatalogoServicio();
    if (coincidePreferencias) {
      this.preferenciaCatalogoServicio = preferenciaCatalogoServicio;
    }
  }

  /**
   * Gets the pk preferencia.
   *
   * @return the pkPreferencia
   */
  public Long getPkPreferencia() {
    return pkPreferencia;
  }

  /**
   * Sets the pk preferencia.
   *
   * @param pkPreferencia the pkPreferencia to set
   */
  public void setPkPreferencia(final Long pkPreferencia) {
    this.pkPreferencia = pkPreferencia;
  }

  /**
   * Gets the codigo primer centro.
   *
   * @return the codigoPrimerCentro
   */
  public String getCodigoPrimerCentro() {
    return codigoPrimerCentro;
  }

  /**
   * Sets the codigo primer centro.
   *
   * @param codigoPrimerCentro the codigoPrimerCentro to set
   */
  public void setCodigoPrimerCentro(final String codigoPrimerCentro) {
    this.codigoPrimerCentro = codigoPrimerCentro;
  }

  /**
   * Gets the codigo segundo centro.
   *
   * @return the codigoSegundoCentro
   */
  public String getCodigoSegundoCentro() {
    return codigoSegundoCentro;
  }

  /**
   * Sets the codigo segundo centro.
   *
   * @param codigoSegundoCentro the codigoSegundoCentro to set
   */
  public void setCodigoSegundoCentro(final String codigoSegundoCentro) {
    this.codigoSegundoCentro = codigoSegundoCentro;
  }

  /**
   * Gets the codigo tercer centro.
   *
   * @return the codigoTercerCentro
   */
  public String getCodigoTercerCentro() {
    return codigoTercerCentro;
  }

  /**
   * Sets the codigo tercer centro.
   *
   * @param codigoTercerCentro the codigoTercerCentro to set
   */
  public void setCodigoTercerCentro(final String codigoTercerCentro) {
    this.codigoTercerCentro = codigoTercerCentro;
  }

  /**
   * Gets the nombre primer centro.
   *
   * @return the nombrePrimerCentro
   */
  public String getNombrePrimerCentro() {
    return nombrePrimerCentro;
  }

  /**
   * Sets the nombre primer centro.
   *
   * @param nombrePrimerCentro the nombrePrimerCentro to set
   */
  public void setNombrePrimerCentro(final String nombrePrimerCentro) {
    this.nombrePrimerCentro = nombrePrimerCentro;
  }

  /**
   * Gets the nombre segundo centro.
   *
   * @return the nombreSegundoCentro
   */
  public String getNombreSegundoCentro() {
    return nombreSegundoCentro;
  }

  /**
   * Sets the nombre segundo centro.
   *
   * @param nombreSegundoCentro the nombreSegundoCentro to set
   */
  public void setNombreSegundoCentro(final String nombreSegundoCentro) {
    this.nombreSegundoCentro = nombreSegundoCentro;
  }

  /**
   * Gets the nombre tercer centro.
   *
   * @return the nombreTercerCentro
   */
  public String getNombreTercerCentro() {
    return nombreTercerCentro;
  }

  /**
   * Sets the nombre tercer centro.
   *
   * @param nombreTercerCentro the nombreTercerCentro to set
   */
  public void setNombreTercerCentro(final String nombreTercerCentro) {
    this.nombreTercerCentro = nombreTercerCentro;
  }

  /**
   * Gets the orden profesional.
   *
   * @return the ordenProfesional
   */
  public Long getOrdenProfesional() {
    return ordenProfesional;
  }

  /**
   * Sets the orden profesional.
   *
   * @param ordenProfesional the ordenProfesional to set
   */
  public void setOrdenProfesional(final Long ordenProfesional) {
    this.ordenProfesional = ordenProfesional;
  }

  /**
   * Gets the orden solicitud.
   *
   * @return the ordenSolicitud
   */
  public Long getOrdenSolicitud() {
    return ordenSolicitud;
  }

  /**
   * Sets the orden solicitud.
   *
   * @param ordenSolicitud the ordenSolicitud to set
   */
  public void setOrdenSolicitud(final Long ordenSolicitud) {
    this.ordenSolicitud = ordenSolicitud;
  }

  /**
   * Gets the tipo preferencia.
   *
   * @return the tipoPreferencia
   */
  public Long getTipoPreferencia() {
    return tipoPreferencia;
  }

  /**
   * Sets the tipo preferencia.
   *
   * @param tipoPreferencia the tipoPreferencia to set
   */
  public void setTipoPreferencia(final Long tipoPreferencia) {
    this.tipoPreferencia = tipoPreferencia;
  }

  /**
   * Sets the tipo preferencia.
   *
   * @param tipoPreferenciaInformeSocial the new tipo preferencia
   */
  public void setTipoPreferencia(
      final TipoPreferenciaInformeSocialEnum tipoPreferenciaInformeSocial) {
    tipoPreferencia = tipoPreferenciaInformeSocial.getValor();
  }

  /**
   * Gets the tipo modalidad.
   *
   * @return the tipoModalidad
   */
  public String getTipoModalidad() {
    return tipoModalidad;
  }

  /**
   * Sets the tipo modalidad.
   *
   * @param tipoModalidad the tipoModalidad to set
   */
  public void setTipoModalidad(final String tipoModalidad) {
    this.tipoModalidad = tipoModalidad;
  }

  /**
   * Gets the intensidad.
   *
   * @return the intensidad
   */
  public String getIntensidad() {
    return intensidad;
  }

  /**
   * Sets the intensidad.
   *
   * @param intensidad the intensidad to set
   */
  public void setIntensidad(final String intensidad) {
    this.intensidad = intensidad;
  }

  /**
   * Gets the informe social.
   *
   * @return the informeSocial
   */
  public InformeSocialDTO getInformeSocial() {
    return informeSocial;
  }

  /**
   * Sets the informe social.
   *
   * @param informeSocial the informeSocial to set
   */
  public void setInformeSocial(final InformeSocialDTO informeSocial) {
    this.informeSocial = informeSocial;
  }

  /**
   * Gets the preferencia catalogo servicio.
   *
   * @return the preferenciaCatalogoServicio
   */
  public PreferenciaCatalogoServicioDTO getPreferenciaCatalogoServicio() {
    return preferenciaCatalogoServicio;
  }

  /**
   * Sets the preferencia catalogo servicio.
   *
   * @param preferenciaCatalogoServicio the preferenciaCatalogoServicio to set
   */
  public void setPreferenciaCatalogoServicio(
      final PreferenciaCatalogoServicioDTO preferenciaCatalogoServicio) {
    this.preferenciaCatalogoServicio = preferenciaCatalogoServicio;
  }

  /**
   * Gets the catalogo servicio.
   *
   * @return the catalogoServicio
   */
  public CatalogoServicioDTO getCatalogoServicio() {
    return catalogoServicio;
  }

  /**
   * Sets the catalogo servicio.
   *
   * @param catalogoServicio the catalogoServicio to set
   */
  public void setCatalogoServicio(final CatalogoServicioDTO catalogoServicio) {
    this.catalogoServicio = catalogoServicio;
  }

  /**
   * Checks if is nueva preferencia.
   *
   * @return true, if is nueva preferencia
   */
  public boolean isNuevaPreferencia() {
    return nuevaPreferencia;
  }

  /**
   * Sets the nueva preferencia.
   *
   * @param nuevaPreferencia the new nueva preferencia
   */
  public void setNuevaPreferencia(final boolean nuevaPreferencia) {
    this.nuevaPreferencia = nuevaPreferencia;
  }

  /**
   * Gets the orden.
   *
   * @return the orden
   */
  public Long getOrden() {
    return TipoPreferenciaInformeSocialEnum.VALORACION_PROFESIONAL.getValor()
        .equals(tipoPreferencia) ? ordenProfesional : ordenSolicitud;
  }


  /**
   * Gets the intensidad label.
   *
   * @return the intensidad label
   */
  public String getIntensidadLabel() {
    return intensidad == null ? null
        : Labels.getLabel(IntensidadEnum.labelFromValor(intensidad));
  }

  /**
   * Gets the tipo modalidad label.
   *
   * @return the tipo modalidad label
   */
  public String getTipoModalidadLabel() {
    return tipoModalidad == null ? null
        : Labels.getLabel(TipoModalidadEnum.labelFromValor(tipoModalidad));
  }

  /**
   * Es teleasis pref.
   *
   * @return true, if successful
   */
  public boolean esTeleasisPref() {
    return this.catalogoServicio != null
        && TipoCatalogoServicioEnum.TELEASISTENCIA.getCodigo()
        .equals(catalogoServicio.getCodigo());
  }

  /**
   * Gets the modalidad teleasistencia.
   *
   * @return the modalidad teleasistencia
   */
  public Long getModalidadTeleasistencia() {
    return modalidadTeleasistencia;
  }

  /**
   * Sets the modalidad teleasistencia.
   *
   * @param modalidadTeleasistencia the new modalidad teleasistencia
   */
  public void setModalidadTeleasistencia(final Long modalidadTeleasistencia) {
    this.modalidadTeleasistencia = modalidadTeleasistencia;
    if (ModalidadEnum.FIJA.getValor().equals(modalidadTeleasistencia)) {
      this.motivoMovil = null;
    }
  }

  /**
   * Gets the motivo movil.
   *
   * @return the motivo movil
   */
  public Long getMotivoMovil() {
    return motivoMovil;
  }

  /**
   * Sets the motivo movil.
   *
   * @param motivoMovil the new motivo movil
   */
  public void setMotivoMovil(final Long motivoMovil) {
    this.motivoMovil = motivoMovil;
  }

  /**
   * Gets the motivo no idoneidad.
   *
   * @return the motivo no idoneidad
   */
  public String getMotivoNoIdoneidad() {
    return motivoNoIdoneidad;
  }

  /**
   * Sets the motivo no idoneidad.
   *
   * @param motivoNoIdoneidad the new motivo no idoneidad
   */
  public void setMotivoNoIdoneidad(final String motivoNoIdoneidad) {
    this.motivoNoIdoneidad = motivoNoIdoneidad;
  }

  /**
   * Gets the recurso idoneo.
   *
   * @return the recurso idoneo
   */
  public Boolean getRecursoIdoneo() {
    return recursoIdoneo;
  }

  /**
   * Sets the recurso idoneo.
   *
   * @param recursoIdoneo the new recurso idoneo
   */
  public void setRecursoIdoneo(final Boolean recursoIdoneo) {
    this.recursoIdoneo = recursoIdoneo;
    if (BooleanUtils.isTrue(recursoIdoneo)) {
      this.motivoNoIdoneidad = null;
    }
  }

  /**
   * Gets the no idoneo informado.
   *
   * @return the no idoneo informado
   */
  public Boolean getNoIdoneoInformado() {
    return noIdoneoInformado;
  }

  /**
   * Sets the no idoneo informado.
   *
   * @param noIdoneoInformado the new no idoneo informado
   */
  public void setNoIdoneoInformado(final Boolean noIdoneoInformado) {
    this.noIdoneoInformado = noIdoneoInformado;
  }

  /**
   * Gets the catalogo explicado.
   *
   * @return the catalogo explicado
   */
  public Boolean getCatalogoExplicado() {
    return catalogoExplicado;
  }

  /**
   * Sets the catalogo explicado.
   *
   * @param catalogoExplicado the new catalogo explicado
   */
  public void setCatalogoExplicado(final Boolean catalogoExplicado) {
    this.catalogoExplicado = catalogoExplicado;
  }

  /**
   * Gets the recurso prescripcion.
   *
   * @return the recurso prescripcion
   */
  public CatalogoServicioDTO getRecursoPrescripcion() {
    return recursoPrescripcion;
  }

  /**
   * Sets the recurso prescripcion.
   *
   * @param recursoPrescripcion the new recurso prescripcion
   */
  public void setRecursoPrescripcion(
      final CatalogoServicioDTO recursoPrescripcion) {
    this.recursoPrescripcion = recursoPrescripcion;
  }

  /**
   * Gets the solicita internamiento.
   *
   * @return the solicita internamiento
   */
  public Boolean getSolicitaInternamiento() {
    return solicitaInternamiento;
  }

  /**
   * Sets the solicita internamiento.
   *
   * @param solicitaInternamiento the new solicita internamiento
   */
  public void setSolicitaInternamiento(final Boolean solicitaInternamiento) {
    this.solicitaInternamiento = solicitaInternamiento;
  }

  /**
   * Gets the informado fiscalia.
   *
   * @return the informado fiscalia
   */
  public Boolean getInformadoFiscalia() {
    return informadoFiscalia;
  }

  /**
   * Sets the informado fiscalia.
   *
   * @param informadoFiscalia the new informado fiscalia
   */
  public void setInformadoFiscalia(final Boolean informadoFiscalia) {
    this.informadoFiscalia = informadoFiscalia;
  }

  /**
   * Gets the recurso adecuado valoracion.
   *
   * @return the recurso adecuado valoracion
   */
  public String getRecursoAdecuadoValoracion() {
    return recursoAdecuadoValoracion;
  }

  /**
   * Sets the recurso adecuado valoracion.
   *
   * @param recursoAdecuadoValoracion the new recurso adecuado valoracion
   */
  public void setRecursoAdecuadoValoracion(
      final String recursoAdecuadoValoracion) {
    this.recursoAdecuadoValoracion = recursoAdecuadoValoracion;
  }

  /**
   * Gets the aumento horas.
   *
   * @return the aumento horas
   */
  public Boolean getAumentoHoras() {
    return aumentoHoras;
  }

  /**
   * Sets the aumento horas.
   *
   * @param aumentoHoras the new aumento horas
   */
  public void setAumentoHoras(final Boolean aumentoHoras) {
    this.aumentoHoras = aumentoHoras;
  }

  /**
   * Gets the tipo centro preferencia.
   *
   * @return the tipo centro preferencia
   */
  public Long getTipoCentroPreferencia() {
    return tipoCentroPreferencia;
  }

  /**
   * Sets the tipo centro preferencia.
   *
   * @param tipoCentroPreferencia the new tipo centro preferencia
   */
  public void setTipoCentroPreferencia(final Long tipoCentroPreferencia) {
    this.tipoCentroPreferencia = tipoCentroPreferencia;
  }



  /**
   * Gets the otros incumple PVI.
   *
   * @return the otros incumple PVI
   */
  public Boolean getOtrosIncumplePVI() {
    return otrosIncumplePVI;
  }

  /**
   * Sets the otros incumple PVI.
   *
   * @param otrosIncumplePVI the new otros incumple PVI
   */
  public void setOtrosIncumplePVI(final Boolean otrosIncumplePVI) {
    this.otrosIncumplePVI = otrosIncumplePVI;
  }

  /**
   * Gets the otros motivos incumple PVI.
   *
   * @return the otros motivos incumple PVI
   */
  public String getOtrosMotivosIncumplePVI() {
    return otrosMotivosIncumplePVI;
  }

  /**
   * Sets the otros motivos incumple PVI.
   *
   * @param otrosMotivosIncumplePVI the new otros motivos incumple PVI
   */
  public void setOtrosMotivosIncumplePVI(final String otrosMotivosIncumplePVI) {
    this.otrosMotivosIncumplePVI = otrosMotivosIncumplePVI;
  }

  /**
   * Gets the cambio permite PVI.
   *
   * @return the cambio permite PVI
   */
  public Boolean getCambioPermitePVI() {
    return cambioPermitePVI;
  }

  /**
   * Sets the cambio permite PVI.
   *
   * @param cambioPermitePVI the new cambio permite PVI
   */
  public void setCambioPermitePVI(final Boolean cambioPermitePVI) {
    this.cambioPermitePVI = cambioPermitePVI;
  }

  /**
   * Gets the incumple proyecto AP.
   *
   * @return the incumple proyecto AP
   */
  public Boolean getIncumpleProyectoAP() {
    return incumpleProyectoAP;
  }

  /**
   * Sets the incumple proyecto AP.
   *
   * @param incumpleProyectoAP the new incumple proyecto AP
   */
  public void setIncumpleProyectoAP(final Boolean incumpleProyectoAP) {
    this.incumpleProyectoAP = incumpleProyectoAP;
  }

  /**
   * Gets the incumple requisitos AP.
   *
   * @return the incumple requisitos AP
   */
  public Boolean getIncumpleRequisitosAP() {
    return incumpleRequisitosAP;
  }

  /**
   * Sets the incumple requisitos AP.
   *
   * @param incumpleRequisitosAP the new incumple requisitos AP
   */
  public void setIncumpleRequisitosAP(final Boolean incumpleRequisitosAP) {
    this.incumpleRequisitosAP = incumpleRequisitosAP;
  }

  /**
   * Gets the incumple persona dep.
   *
   * @return the incumple persona dep
   */
  public Boolean getIncumplePersonaDep() {
    return incumplePersonaDep;
  }

  /**
   * Sets the incumple persona dep.
   *
   * @param incumplePersonaDep the new incumple persona dep
   */
  public void setIncumplePersonaDep(final Boolean incumplePersonaDep) {
    this.incumplePersonaDep = incumplePersonaDep;
  }

  /**
   * Gets the cambio situacion persona.
   *
   * @return the cambio situacion persona
   */
  public Boolean getCambioSituacionPersona() {
    return cambioSituacionPersona;
  }

  /**
   * Sets the cambio situacion persona.
   *
   * @param cambioSituacionPersona the new cambio situacion persona
   */
  public void setCambioSituacionPersona(final Boolean cambioSituacionPersona) {
    this.cambioSituacionPersona = cambioSituacionPersona;
  }

  @Override
  public int hashCode() {
    return Objects.hash(catalogoServicio, tipoModalidad, tipoPreferencia);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final PreferenciaInformeSocialDTO other = (PreferenciaInformeSocialDTO) obj;
    return Objects.equals(catalogoServicio, other.catalogoServicio)
        && Objects.equals(tipoModalidad, other.tipoModalidad)
        && Objects.equals(tipoPreferencia, other.tipoPreferencia);
  }

  public Boolean getIngresoVoluntario() {
    return ingresoVoluntario;
  }

  public void setIngresoVoluntario(final Boolean ingresoVoluntario) {
    this.ingresoVoluntario = ingresoVoluntario;
  }

  public Boolean getContinuarPrestacion() {
    return continuarPrestacion;
  }

  public void setContinuarPrestacion(final Boolean continuarPrestacion) {
    this.continuarPrestacion = continuarPrestacion;
  }

}
