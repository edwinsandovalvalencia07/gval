package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.validator.constraints.Length;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * The Class IncidenciaDTO.
 */
public class IncidenciaDTO extends BaseDTO
    implements Comparable<IncidenciaDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -6286692086811858307L;

  /** The pk incidenc. */
  @NotNull
  private Long pkIncidenc;

  /** The activo. */
  @NotNull
  private Boolean activo;

  /** The descripcion. */
  @Length(max = 500)
  private String descripcion;

  /** The estado. */
  @NotNull
  @Length(max = 1)
  private String estado;

  /** The fecha creacion. */
  @NotNull
  private Date fechaCreacion;

  /** The pk docu. */
  private Long pkDocu;

  /** The solicitud. */
  @NotNull
  private SolicitudDTO solicitud;

  /** The tipo incidencia. */
  @NotNull
  private TipoIncidenciaDTO tipoIncidencia;

  /** The comentarios. */
  private List<ComentarioIncidenciaDTO> comentarios;

  /** The usuario. */
  @NotNull
  private UsuarioDTO usuario;

  /**
   * Instantiates a new incidencia DTO.
   */
  public IncidenciaDTO() {
    super();
  }

  /**
   * Instantiates a new incidencia DTO.
   *
   * @param pkIncidenc the pk incidenc
   * @param activo the activo
   * @param descripcion the descripcion
   * @param estado the estado
   * @param fechaCreacion the fecha creacion
   * @param pkDocu the pk docu
   * @param solicitud the solicitud
   * @param tipoIncidencia the tipo incidencia
   * @param usuario the usuario
   * @param comentarios the comentarios
   */
  public IncidenciaDTO(final Long pkIncidenc, final Boolean activo,
      final String descripcion, final String estado, final Date fechaCreacion,
      final Long pkDocu, final SolicitudDTO solicitud,
      final TipoIncidenciaDTO tipoIncidencia, final UsuarioDTO usuario,
      final List<ComentarioIncidenciaDTO> comentarios) {
    super();
    this.pkIncidenc = pkIncidenc;
    this.activo = activo;
    this.descripcion = descripcion;
    this.estado = estado;
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
    this.pkDocu = pkDocu;
    this.solicitud = solicitud;
    this.tipoIncidencia = tipoIncidencia;
    this.usuario = usuario;
    this.comentarios = UtilidadesCommons.collectorsToList(comentarios);
  }

  /**
   * Gets the pk incidenc.
   *
   * @return the pk incidenc
   */
  public Long getPkIncidenc() {
    return pkIncidenc;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Gets the estado.
   *
   * @return the estado
   */
  public String getEstado() {
    return estado;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the pk docu.
   *
   * @return the pk docu
   */
  public Long getPkDocu() {
    return pkDocu;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public SolicitudDTO getSolicitud() {
    return solicitud;
  }

  /**
   * Gets the tipo incidencia.
   *
   * @return the tipo incidencia
   */
  public TipoIncidenciaDTO getTipoIncidencia() {
    return tipoIncidencia;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }

  /**
   * Sets the pk incidenc.
   *
   * @param pkIncidenc the new pk incidenc
   */
  public void setPkIncidenc(final Long pkIncidenc) {
    this.pkIncidenc = pkIncidenc;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Sets the estado.
   *
   * @param estado the new estado
   */
  public void setEstado(final String estado) {
    this.estado = estado;
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the pk docu.
   *
   * @param pkDocu the new pk docu
   */
  public void setPkDocu(final Long pkDocu) {
    this.pkDocu = pkDocu;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the new solicitud
   */
  public void setSolicitud(final SolicitudDTO solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Sets the tipo incidencia.
   *
   * @param tipoIncidencia the new tipo incidencia
   */
  public void setTipoIncidencia(final TipoIncidenciaDTO tipoIncidencia) {
    this.tipoIncidencia = tipoIncidencia;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the comentarios.
   *
   * @return the comentarios
   */
  public List<ComentarioIncidenciaDTO> getComentarios() {
    return UtilidadesCommons.collectorsToList(comentarios);
  }

  /**
   * Sets the comentarios.
   *
   * @param comentarios the new comentarios
   */
  public void setComentarios(final List<ComentarioIncidenciaDTO> comentarios) {
    this.comentarios = UtilidadesCommons.collectorsToList(comentarios);
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final IncidenciaDTO o) {
    return 0;
  }

  /**
   * Inicializa Incidencia nueva.
   *
   * @return the incidencia DTO
   */
  public static IncidenciaDTO createInstance() {
    final IncidenciaDTO incidencia = new IncidenciaDTO();

    incidencia.setActivo(Boolean.TRUE);
    incidencia.setFechaCreacion(new Date());

    return incidencia;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
