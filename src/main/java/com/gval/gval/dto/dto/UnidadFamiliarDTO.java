package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import es.gva.dependencia.ada.common.enums.DiscapacidadMayor33FormularioEnum;
import com.gval.gval.dto.dto.util.BaseDTO;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import org.zkoss.util.resource.Labels;

import java.util.Date;

import javax.validation.constraints.NotNull;

/**
 * The Class UnidadFamiliarDTO.
 */
public class UnidadFamiliarDTO extends BaseDTO
    implements Comparable<UnidadFamiliarDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -3134942700986227608L;

  /** The Constant ERROR_PARENTESCO_VACIO. */
  private static final String ERROR_PARENTESCO_VACIO =
      "error.unidadfamiliar.parentesco.vacio";

  /** The pk unidfami. */
  private Long pkUnidfami;

  /** The activo. */
  @NotNull
  private Boolean activo;

  /** The excluido. */
  private Boolean excluido;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The ingresado. */
  private Boolean ingresado;

  /** The ingreso. */
  private Boolean ingreso;

  /** The persona. */
  @NotNull
  private PersonaAdaDTO persona;

  /** The solicitud. */
  @JsonManagedReference
  @NotNull
  private SolicitudDTO solicitud;

  /** The tipo familiar. */
  @NotNull(message = ERROR_PARENTESCO_VACIO)
  private TipoFamiliarDTO tipoFamiliar;

  /** The app origen. */
  private String appOrigen;

  /** The discapacidad mayor 33. */
  private Integer discapacidadMayor33;


  /**
   * Instantiates a new unidad familiar DTO.
   */
  public UnidadFamiliarDTO() {
    super();
    this.activo = Boolean.FALSE;
  }

  /**
   * Instantiates a new unidad familiar DTO.
   *
   * @param defaultData the default data
   */
  public UnidadFamiliarDTO(final boolean defaultData) {
    super();
    if (defaultData) {
      this.activo = Boolean.TRUE;
      this.setFechaCreacion(new Date());
      this.persona = new PersonaAdaDTO(true);
      this.ingreso = Boolean.FALSE;
      this.ingresado = Boolean.FALSE;
      this.excluido = Boolean.FALSE;
    }
  }

  /**
   * Gets the label discapacidad.
   *
   * @return the label discapacidad
   */
  public String getLabelDiscapacidad() {

    return Labels.getLabel(DiscapacidadMayor33FormularioEnum
        .getLabelFromValue(this.discapacidadMayor33));
  }

  /**
   * Gets the pk unidfami.
   *
   * @return the pk unidfami
   */
  public Long getPkUnidfami() {
    return pkUnidfami;
  }

  /**
   * Sets the pk unidfami.
   *
   * @param pkUnidfami the new pk unidfami
   */
  public void setPkUnidfami(final Long pkUnidfami) {
    this.pkUnidfami = pkUnidfami;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the excluido.
   *
   * @return the excluido
   */
  public Boolean getExcluido() {
    return excluido;
  }

  /**
   * Sets the excluido.
   *
   * @param excluido the new excluido
   */
  public void setExcluido(final Boolean excluido) {
    this.excluido = excluido;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the ingresado.
   *
   * @return the ingresado
   */
  public Boolean getIngresado() {
    return ingresado;
  }

  /**
   * Sets the ingresado.
   *
   * @param ingresado the new ingresado
   */
  public void setIngresado(final Boolean ingresado) {
    this.ingresado = ingresado;
  }

  /**
   * Gets the ingreso.
   *
   * @return the ingreso
   */
  public Boolean getIngreso() {
    return ingreso;
  }

  /**
   * Sets the ingreso.
   *
   * @param ingreso the new ingreso
   */
  public void setIngreso(final Boolean ingreso) {
    this.ingreso = ingreso;
  }

  /**
   * Gets the persona.
   *
   * @return the persona
   */
  public PersonaAdaDTO getPersona() {
    return persona;
  }

  /**
   * Sets the persona.
   *
   * @param persona the new persona
   */
  public void setPersona(final PersonaAdaDTO persona) {
    this.persona = persona;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public SolicitudDTO getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the new solicitud
   */
  public void setSolicitud(final SolicitudDTO solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the tipo familiar.
   *
   * @return the tipo familiar
   */
  public TipoFamiliarDTO getTipoFamiliar() {
    return tipoFamiliar;
  }

  /**
   * Sets the tipo familiar.
   *
   * @param tipoFamiliar the new tipo familiar
   */
  public void setTipoFamiliar(final TipoFamiliarDTO tipoFamiliar) {
    this.tipoFamiliar = tipoFamiliar;
  }

  /**
   * Gets the app origen.
   *
   * @return the app origen
   */
  public String getAppOrigen() {
    return appOrigen;
  }

  /**
   * Sets the app origen.
   *
   * @param appOrigen the new app origen
   */
  public void setAppOrigen(final String appOrigen) {
    this.appOrigen = appOrigen;
  }

  /**
   * Checks if is discapacidad mayor 33.
   *
   * @return true, if is discapacidad mayor 33
   */
  public Integer getDiscapacidadMayor33() {
    return discapacidadMayor33;
  }

  /**
   * Sets the discapacidad mayor 33.
   *
   * @param discapacidadMayor33 the new discapacidad mayor 33
   */
  public void setDiscapacidadMayor33(final Integer discapacidadMayor33) {
    this.discapacidadMayor33 = discapacidadMayor33;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final UnidadFamiliarDTO o) {
    return 0;
  }
}
