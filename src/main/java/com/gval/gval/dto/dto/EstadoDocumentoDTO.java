package com.gval.gval.dto.dto;

import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * The Class EstadoDocumentoDTO.
 *
 */
public class EstadoDocumentoDTO extends BaseDTO
    implements Comparable<EstadoDocumentoDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -954532743629801996L;

  /** The pk estado documento. */
  private Long pkEstadoDocumento;

  /** The activo. */
  private Boolean activo;

  /** The codigo. */
  private String codigo;

  /** The nombre. */
  private String nombre;


  /**
   * Instantiates a new estado documento.
   */
  public EstadoDocumentoDTO() {
    super();
  }


  /**
   * Gets the pk estado documento.
   *
   * @return the pk estado documento
   */
  public Long getPkEstadoDocumento() {
    return pkEstadoDocumento;
  }


  /**
   * Sets the pk estado documento.
   *
   * @param pkEstadoDocumento the new pk estado documento
   */
  public void setPkEstadoDocumento(final Long pkEstadoDocumento) {
    this.pkEstadoDocumento = pkEstadoDocumento;
  }


  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }


  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }


  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }


  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }


  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }


  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }


  /**
   * Compare to.
   *
   * @param arg0 the arg 0
   * @return the int
   */
  @Override
  public int compareTo(final EstadoDocumentoDTO arg0) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
