package com.gval.gval.dto.dto;



import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;
import java.util.Objects;

/**
 * The Class CatalogoServicioDTO.
 */
public class CatalogoServicioDTO extends BaseDTO
    implements Comparable<CatalogoServicioDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -7804935991420205625L;

  /** The pk cataserv. */
  private Long pkCataserv;

  /** The codigo. */
  private String codigo;

  /** The nombre. */
  private String nombre;

  /** The orden. */
  private Long orden;

  /** The activo. */
  private Boolean activo;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The intensidad fija. */
  private String intensidadFija;

  /** The empresas. */
  private String empresas;

  /** The transportes. */
  private String transportes;

  /** The tipo. */
  private String tipo;

  /** The nombre abreviado. */
  private String nombreAbreviado;

  /** The imserso. */
  private String imserso;

  /** The imserso superior. */
  private String imsersoSuperior;

  /** The pagos. */
  private Boolean pagos;

  /** The combo preferencias. */
  private Boolean comboPreferencias;

  /** The combo prestaciones PIA. */
  private Boolean comboPrestacionesPIA;

  /** The prestaciones servicios. */
  private Boolean prestacionesServicios;

  /** The necesita contrato. */
  private Boolean necesitaContrato;

  /** The id servicio equivalente. */
  private Long idServicioEquivalente;
  
  /** The indica si el servicio tiene centros asociados */
  private boolean tieneCentros;
  
  /** The El tipo de centro que se puede asociar al servicio */
  private String tipoCentro;

  /**
   * Instantiates a new catalogo servicio DTO.
   */
  public CatalogoServicioDTO() {
    super();
  }

  /**
   * Instantiates a new catalogo servicio DTO.
   *
   * @param pkCataserv the pk cataserv
   * @param codigo the codigo
   * @param nombre the nombre
   * @param orden the orden
   * @param activo the activo
   * @param fechaCreacion the fecha creacion
   * @param intensidadFija the intensidad fija
   * @param empresas the empresas
   * @param transportes the transportes
   * @param tipo the tipo
   * @param nombreAbreviado the nombre abreviado
   * @param imserso the imserso
   * @param imsersoSuperior the imserso superior
   * @param pagos the pagos
   * @param comboPreferencias the combo preferencias
   * @param comboPrestacionesPIA the combo prestaciones PIA
   * @param prestacionesServicios the prestaciones servicios
   * @param necesitaContrato the necesita contrato
   * @param idServicioEquivalente the id servicio equivalente
   * @param tieneCentros the tiene centros asociados
   * @param tipoCentro the tipo de centro
   */
  public CatalogoServicioDTO(final Long pkCataserv, final String codigo,
      final String nombre, final Long orden, final Boolean activo,
      final Date fechaCreacion, final String intensidadFija,
      final String empresas, final String transportes, final String tipo,
      final String nombreAbreviado, final String imserso,
      final String imsersoSuperior, final Boolean pagos,
      final Boolean comboPreferencias, final Boolean comboPrestacionesPIA,
      final Boolean prestacionesServicios, final Boolean necesitaContrato,
      final Long idServicioEquivalente, final Boolean tieneCentros, final String tipoCentro) {
    super();
    this.pkCataserv = pkCataserv;
    this.codigo = codigo;
    this.nombre = nombre;
    this.orden = orden;
    this.activo = activo;
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
    this.intensidadFija = intensidadFija;
    this.empresas = empresas;
    this.transportes = transportes;
    this.tipo = tipo;
    this.nombreAbreviado = nombreAbreviado;
    this.imserso = imserso;
    this.imsersoSuperior = imsersoSuperior;
    this.pagos = pagos;
    this.comboPreferencias = comboPreferencias;
    this.comboPrestacionesPIA = comboPrestacionesPIA;
    this.prestacionesServicios = prestacionesServicios;
    this.necesitaContrato = necesitaContrato;
    this.idServicioEquivalente = idServicioEquivalente;
    this.tieneCentros = tieneCentros;
    this.tipoCentro = tipoCentro;
  }

  /**
   * Gets the pk cataserv.
   *
   * @return the pk cataserv
   */
  public Long getPkCataserv() {
    return pkCataserv;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Gets the orden.
   *
   * @return the orden
   */
  public Long getOrden() {
    return orden;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the intensidad fija.
   *
   * @return the intensidad fija
   */
  public String getIntensidadFija() {
    return intensidadFija;
  }

  /**
   * Gets the empresas.
   *
   * @return the empresas
   */
  public String getEmpresas() {
    return empresas;
  }

  /**
   * Gets the transportes.
   *
   * @return the transportes
   */
  public String getTransportes() {
    return transportes;
  }

  /**
   * Gets the tipo.
   *
   * @return the tipo
   */
  public String getTipo() {
    return tipo;
  }

  /**
   * Gets the nombre abreviado.
   *
   * @return the nombre abreviado
   */
  public String getNombreAbreviado() {
    return nombreAbreviado;
  }

  /**
   * Gets the imserso.
   *
   * @return the imserso
   */
  public String getImserso() {
    return imserso;
  }

  /**
   * Gets the imserso superior.
   *
   * @return the imserso superior
   */
  public String getImsersoSuperior() {
    return imsersoSuperior;
  }

  /**
   * Gets the pagos.
   *
   * @return the pagos
   */
  public Boolean getPagos() {
    return pagos;
  }

  /**
   * Gets the combo preferencias.
   *
   * @return the combo preferencias
   */
  public Boolean getComboPreferencias() {
    return comboPreferencias;
  }

  /**
   * Gets the combo prestaciones PIA.
   *
   * @return the combo prestaciones PIA
   */
  public Boolean getComboPrestacionesPIA() {
    return comboPrestacionesPIA;
  }

  /**
   * Gets the prestaciones servicios.
   *
   * @return the prestaciones servicios
   */
  public Boolean getPrestacionesServicios() {
    return prestacionesServicios;
  }

  /**
   * Gets the necesita contrato.
   *
   * @return the necesita contrato
   */
  public Boolean getNecesitaContrato() {
    return necesitaContrato;
  }

  /**
   * Sets the pk cataserv.
   *
   * @param pkCataserv the new pk cataserv
   */
  public void setPkCataserv(final Long pkCataserv) {
    this.pkCataserv = pkCataserv;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Sets the orden.
   *
   * @param orden the new orden
   */
  public void setOrden(final Long orden) {
    this.orden = orden;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the intensidad fija.
   *
   * @param intensidadFija the new intensidad fija
   */
  public void setIntensidadFija(final String intensidadFija) {
    this.intensidadFija = intensidadFija;
  }

  /**
   * Sets the empresas.
   *
   * @param empresas the new empresas
   */
  public void setEmpresas(final String empresas) {
    this.empresas = empresas;
  }

  /**
   * Sets the transportes.
   *
   * @param transportes the new transportes
   */
  public void setTransportes(final String transportes) {
    this.transportes = transportes;
  }

  /**
   * Sets the tipo.
   *
   * @param tipo the new tipo
   */
  public void setTipo(final String tipo) {
    this.tipo = tipo;
  }

  /**
   * Sets the nombre abreviado.
   *
   * @param nombreAbreviado the new nombre abreviado
   */
  public void setNombreAbreviado(final String nombreAbreviado) {
    this.nombreAbreviado = nombreAbreviado;
  }

  /**
   * Sets the imserso.
   *
   * @param imserso the new imserso
   */
  public void setImserso(final String imserso) {
    this.imserso = imserso;
  }

  /**
   * Sets the imserso superior.
   *
   * @param imsersoSuperior the new imserso superior
   */
  public void setImsersoSuperior(final String imsersoSuperior) {
    this.imsersoSuperior = imsersoSuperior;
  }

  /**
   * Sets the pagos.
   *
   * @param pagos the new pagos
   */
  public void setPagos(final Boolean pagos) {
    this.pagos = pagos;
  }

  /**
   * Sets the combo preferencias.
   *
   * @param comboPreferencias the new combo preferencias
   */
  public void setComboPreferencias(final Boolean comboPreferencias) {
    this.comboPreferencias = comboPreferencias;
  }

  /**
   * Sets the combo prestaciones PIA.
   *
   * @param comboPrestacionesPIA the new combo prestaciones PIA
   */
  public void setComboPrestacionesPIA(final Boolean comboPrestacionesPIA) {
    this.comboPrestacionesPIA = comboPrestacionesPIA;
  }

  /**
   * Sets the prestaciones servicios.
   *
   * @param prestacionesServicios the new prestaciones servicios
   */
  public void setPrestacionesServicios(final Boolean prestacionesServicios) {
    this.prestacionesServicios = prestacionesServicios;
  }

  /**
   * Sets the necesita contrato.
   *
   * @param necesitaContrato the new necesita contrato
   */
  public void setNecesitaContrato(final Boolean necesitaContrato) {
    this.necesitaContrato = necesitaContrato;
  }

  /**
   * Gets the id servicio equivalente.
   *
   * @return the id servicio equivalente
   */
  public Long getIdServicioEquivalente() {
    return idServicioEquivalente;
  }

  /**
   * Sets the id servicio equivalente.
   *
   * @param idServicioEquivalente the new id servicio equivalente
   */
  public void setIdServicioEquivalente(final Long idServicioEquivalente) {
    this.idServicioEquivalente = idServicioEquivalente;
  }
  
  /**
   * Gets the tiene centros asociados.
   *
   * @return the tiene centros asociados
   */
  public boolean getTieneCentros() {
    return tieneCentros;
  }

  /**
   * Sets the tiene centros asociados.
   *
   * @param tieneCentros the new  tiene centros asociados
   */
  public void setTieneCentros(boolean tieneCentros) {
    this.tieneCentros = tieneCentros;
  }

  /**
   * Gets the tipo de centro asociado.
   *
   * @return the tipo de centro asociado
   */
  public String getTipoCentro() {
    return tipoCentro;
  }

  /**
   * Sets the tipo de centro.
   *
   * @param tipoCentro the new id tipo de centro
   */
  public void setTipoCentro(String tipoCentro) {
    this.tipoCentro = tipoCentro;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final CatalogoServicioDTO o) {
    return 0;
  }

  /**
   * Equals.
   *
   * @param obj the obj
   * @return true, if successful
   */
  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    // null check
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final CatalogoServicioDTO other = (CatalogoServicioDTO) obj;
    return other.getPkCataserv() != null && this.pkCataserv != null
        && other.getCodigo() != null && this.codigo != null
        && this.pkCataserv.equals(other.getPkCataserv())
        && Objects.equals(this.codigo, other.getCodigo());
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
