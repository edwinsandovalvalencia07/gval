package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import java.util.Date;

/**
 * The Class PoblacionDTO.
 */
public class PoblacionDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 312566498753321272L;

  /** The pk poblacion. */
  private Long pkPoblacion;

  /** The activo. */
  private Boolean activo;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The imserso. */
  private String imserso;

  /** The nombre. */
  private String nombre;

  /** The codigo provincia. */
  private String codigoProvincia;

  /** The codigo municipio. */
  private String codigoMunicipio;

  /** The codigo poblacion. */
  private String codigoPoblacion;

  /** The municipio. */
  private MunicipioDTO municipio;

  /**
   * Instantiates a new poblacion DTO.
   */
  public PoblacionDTO() {
    super();
  }

  /**
   * Instantiates a new poblacion DTO.
   *
   * @param pkPoblacion the pk poblacion
   * @param activo the activo
   * @param fechaCreacion the fecha creacion
   * @param imserso the imserso
   * @param nombre the nombre
   * @param municipio the municipio
   */
  public PoblacionDTO(final Long pkPoblacion, final Boolean activo,
      final Date fechaCreacion, final String imserso, final String nombre,
      final MunicipioDTO municipio) {
    super();
    this.pkPoblacion = pkPoblacion;
    this.activo = activo;
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
    this.imserso = imserso;
    this.nombre = nombre;
    this.municipio = municipio;
  }

  /**
   * Gets the pk poblacion.
   *
   * @return the pk poblacion
   */
  public Long getPkPoblacion() {
    return pkPoblacion;
  }

  /**
   * Sets the pk poblacion.
   *
   * @param pkPoblacion the new pk poblacion
   */
  public void setPkPoblacion(final Long pkPoblacion) {
    this.pkPoblacion = pkPoblacion;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the imserso.
   *
   * @return the imserso
   */
  public String getImserso() {
    return imserso;
  }

  /**
   * Sets the imserso.
   *
   * @param imserso the new imserso
   */
  public void setImserso(final String imserso) {
    this.imserso = imserso;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }


  /**
   * Gets the codigo provincia.
   *
   * @return the codigoProvincia
   */
  public String getCodigoProvincia() {
    return codigoProvincia;
  }

  /**
   * Sets the codigo provincia.
   *
   * @param codigoProvincia the codigoProvincia to set
   */
  public void setCodigoProvincia(final String codigoProvincia) {
    this.codigoProvincia = codigoProvincia;
  }

  /**
   * Gets the codigo municipio.
   *
   * @return the codigoMunicipio
   */
  public String getCodigoMunicipio() {
    return codigoMunicipio;
  }

  /**
   * Sets the codigo municipio.
   *
   * @param codigoMunicipio the codigoMunicipio to set
   */
  public void setCodigoMunicipio(final String codigoMunicipio) {
    this.codigoMunicipio = codigoMunicipio;
  }

  /**
   * Gets the codigo poblacion.
   *
   * @return the codigoPoblacion
   */
  public String getCodigoPoblacion() {
    return codigoPoblacion;
  }

  /**
   * Sets the codigo poblacion.
   *
   * @param codigoPoblacion the codigoPoblacion to set
   */
  public void setCodigoPoblacion(final String codigoPoblacion) {
    this.codigoPoblacion = codigoPoblacion;
  }

  /**
   * Gets the municipio.
   *
   * @return the municipio
   */
  public MunicipioDTO getMunicipio() {
    return municipio;
  }

  /**
   * Sets the municipio.
   *
   * @param municipio the new municipio
   */
  public void setMunicipio(final MunicipioDTO municipio) {
    this.municipio = municipio;
  }


}
