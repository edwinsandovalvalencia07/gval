package com.gval.gval.dto.dto;


import com.gval.gval.common.ConstantesCommons;
import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;



/**
 * The Class AsistentePersonalDTO.
 *
 */
public class AsistentePersonalDTO extends BaseDTO
implements Comparable<AsistentePersonalDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk asistente personal. */
  private Long pkAsispers;

  /** The activo. */
  @NotNull(message = "error.asistente.notnull.activo")
  private Boolean activo;

  /** The cambio situacion dependiente. */
  private BigDecimal cambioSituacion;

  /** The compromiso formacion. */
  private Boolean compromisoFormacion;

  /** The tiempo dedicacion. */
  private String tiempoDedicacion;

  /** The fecha creacion registro. */
  private Date fechaCreacion;

  /** The fecha fin servicio. */
  private Date fechaFin;

  /** The fecha inicio servicio. */
  private Date fechaInicio;

  /** The residencia legal. */
  private BigDecimal residenciaLegal;

  /** The servicio prestado por. */
  private String servicioPrestadoPor;

  /** The direccion. */
  // bi-directional many-to-one association to DireccionAdaDTO
  @NotNull(message = "error.asistente.notnull.direccion")
  private DireccionAdaDTO direccion;

  /** The persona. */
  // bi-directional many-to-one association to PersonaAdaDTO
  @NotNull(message = "error.asistente.notnull.persona")
  private PersonaAdaDTO persona;

  /** The pk pia. */
  private Long pkPia;

  /** The Pref catalogo servicio. */
  // bi-directional many-to-one association to PrefCatalogoServicio
  private PreferenciaCatalogoServicioDTO preferenciaCatalogoServicio;

  /**
   * The tipo persona. 1 si es persona juridica/profesional prestadora de
   * servicio 0 si es persona fisica.
   *
   */
  @NotNull(message = "error.asistente.notnull.tipo_persona")
  private Boolean tipoPersona;

  /** The aporta contrato. */
  private Boolean aportaContrato;

  /** The residente cva. */
  private Boolean residenteCva;

  /** The tiene parentesco. */
  private Boolean tieneParentesco;

  /** The tiene certificado. */
  private Boolean tieneCertificado;

  /** The tipo asistencia. */
  private Boolean tipoAsistencia;

  /** The tiene formacion. */
  private Boolean tieneFormacion;

  /** The cod formacion. */
  private CatalogoFormacionDTO formacion;

  /** The identificador. */
  private String identificador;

  /** The identificador. */
  private String nombreCompleto;

  /** The tipo identificador. */
  private Long tipoIdentificador;

  /** The activo pia. */
  private Boolean activoPia;

  /**
   * Instantiates a new asistente personal DTO.
   */
  @SuppressWarnings("squid:S2637")
  public AsistentePersonalDTO() {
    super();
    // Creacion del registro
    this.activo = Boolean.TRUE;
    construirPersonaDireccionDefault();
  }

  /**
   * Instantiates a new asistente personal DTO.
   *
   * @param pkAsispers the pk asispers
   * @param tipoPersona the tipo persona
   * @param identificador the identificador
   * @param activoPia the activo pia
   */
  public AsistentePersonalDTO(final Long pkAsispers, final Boolean tipoPersona,
      final String identificador,final String nombreCompleto, final Boolean activoPia) {
    super();
    this.pkAsispers = pkAsispers;
    this.tipoPersona = tipoPersona;
    this.identificador = identificador;
    this.nombreCompleto = nombreCompleto;
    this.activoPia = activoPia;
  }

  /**
   * Construir persona direccion default.
   */
  private void construirPersonaDireccionDefault() {
    // Relativo a la persona y direccion
    final PersonaAdaDTO personaDTO =
        new PersonaAdaDTO(ConstantesCommons.VALORES_POR_DEFECTO);
    final DireccionAdaDTO direccionDTO =
        new DireccionAdaDTO(ConstantesCommons.VALORES_POR_DEFECTO);
    direccionDTO.setPersonaAdaDTO(personaDTO);
    this.direccion = direccionDTO;
    this.persona = personaDTO;
  }

  /**
   * Gets the pk asistente personal.
   *
   * @return the pk asispers
   */
  public Long getPkAsispers() {
    return this.pkAsispers;
  }

  /**
   * Sets the pk asistente personal.
   *
   * @param pkAsispers the new pk asispers
   */
  public void setPkAsispers(final Long pkAsispers) {
    this.pkAsispers = pkAsispers;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the cambio situacion dependiente.
   *
   * @return the cambioSituacion
   */
  public BigDecimal getCambioSituacion() {
    return this.cambioSituacion;
  }

  /**
   * Sets the cambio situacion dependiente.
   *
   * @param cambioSituacion the new cambio situacion
   */
  public void setCambioSituacion(final BigDecimal cambioSituacion) {
    this.cambioSituacion = cambioSituacion;
  }

  /**
   * Gets the compromiso formacion.
   *
   * @return the compromiso formacion
   */
  public Boolean getCompromisoFormacion() {
    return compromisoFormacion;
  }

  /**
   * Sets the compromiso formacion.
   *
   * @param compromisoFormacion the new compromiso formacion
   */
  public void setCompromisoFormacion(final Boolean compromisoFormacion) {
    this.compromisoFormacion = compromisoFormacion;
  }

  /**
   * Gets the tiempo dedicacion.
   *
   * @return the tiempo dedicacion
   */
  public String getTiempoDedicacion() {
    return this.tiempoDedicacion;
  }

  /**
   * Sets the tiempo dedicacion.
   *
   * @param tiempoDedicacion the new tiempo dedicacion
   */
  public void setTiempoDedicacion(final String tiempoDedicacion) {
    this.tiempoDedicacion = tiempoDedicacion;
  }

  /**
   * Gets the fecha creacion registro.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion registro.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha fin servicio.
   *
   * @return the fecha fin
   */
  public Date getFechaFin() {
    return UtilidadesCommons.cloneDate(fechaFin);
  }

  /**
   * Sets the fecha fin servicio.
   *
   * @param fechaFin the new fecha fin
   */
  public void setFechaFin(final Date fechaFin) {
    this.fechaFin = UtilidadesCommons.cloneDate(fechaFin);
  }

  /**
   * Gets the fecha inicio servicio.
   *
   * @return the fechaInicio
   */
  public Date getFechaInicio() {
    return UtilidadesCommons.cloneDate(fechaInicio);
  }

  /**
   * Sets the fecha inicio servicio.
   *
   * @param fechaInicio the new fecha inicio
   */
  public void setFechaInicio(final Date fechaInicio) {
    this.fechaInicio = UtilidadesCommons.cloneDate(fechaInicio);
  }

  /**
   * Gets the residencia legal.
   *
   * @return the residencia legal
   */
  public BigDecimal getResidenciaLegal() {
    return this.residenciaLegal;
  }

  /**
   * Sets the residencia legal.
   *
   * @param residenciaLegal the new residencia legal
   */
  public void setResidenciaLegal(final BigDecimal residenciaLegal) {
    this.residenciaLegal = residenciaLegal;
  }

  /**
   * Gets the servicio prestado por.
   *
   * @return the servicio prestado por
   */
  public String getServicioPrestadoPor() {
    return this.servicioPrestadoPor;
  }

  /**
   * Sets the servicio prestado por.
   *
   * @param servicioPrestadoPor the new servicio prestado por
   */
  public void setServicioPrestadoPor(final String servicioPrestadoPor) {
    this.servicioPrestadoPor = servicioPrestadoPor;
  }

  /**
   * Gets the direccion.
   *
   * @return the direccion
   */
  public DireccionAdaDTO getDireccion() {
    return this.direccion;
  }

  /**
   * Sets the direccion.
   *
   * @param direccion the new direccion
   */
  public void setDireccion(final DireccionAdaDTO direccion) {
    this.direccion = direccion;
  }

  /**
   * Gets the persona.
   *
   * @return the persona
   */
  public PersonaAdaDTO getPersona() {
    return this.persona;
  }

  /**
   * Sets the persona.
   *
   * @param persona the new persona
   */
  public void setPersona(final PersonaAdaDTO persona) {
    this.persona = persona;
  }

  /**
   * Gets the preferencia catalogo servicio.
   *
   * @return the preferencia catalogo servicio
   */
  public PreferenciaCatalogoServicioDTO getPreferenciaCatalogoServicio() {
    return preferenciaCatalogoServicio;
  }



  /**
   * Sets the preferencia catalogo servicio.
   *
   * @param preferenciaCatalogoServicio the new preferencia catalogo servicio
   */
  public void setPreferenciaCatalogoServicio(
      final PreferenciaCatalogoServicioDTO preferenciaCatalogoServicio) {
    this.preferenciaCatalogoServicio = preferenciaCatalogoServicio;
  }

  /**
   * Gets the pk pia.
   *
   * @return the pk pia
   */
  public Long getPkPia() {
    return pkPia;
  }

  /**
   * Sets the pk pia.
   *
   * @param pkPia the new pk pia
   */
  public void setPkPia(final Long pkPia) {
    this.pkPia = pkPia;
  }

  /**
   * Gets the tipo persona.
   *
   * @return the tipo persona
   */
  public Boolean getTipoPersona() {
    return tipoPersona;
  }

  /**
   * Sets the tipo persona.
   *
   * @param tipoPersona the new tipo persona
   */
  public void setTipoPersona(final Boolean tipoPersona) {
    this.tipoPersona = tipoPersona;
  }

  /**
   * Gets the aporta contrato.
   *
   * @return the aporta contrato
   */
  public Boolean getAportaContrato() {
    return aportaContrato;
  }

  /**
   * Sets the aporta contrato.
   *
   * @param aportaContrato the new aporta contrato
   */
  public void setAportaContrato(final Boolean aportaContrato) {
    this.aportaContrato = aportaContrato;
  }

  /**
   * Gets the residente cva.
   *
   * @return the residente cva
   */
  public Boolean getResidenteCva() {
    return residenteCva;
  }

  /**
   * Sets the residente cva.
   *
   * @param residenteCva the new residente cva
   */
  public void setResidenteCva(final Boolean residenteCva) {
    this.residenteCva = residenteCva;
  }

  /**
   * Gets the tiene parentesco.
   *
   * @return the tiene parentesco
   */
  public Boolean getTieneParentesco() {
    return tieneParentesco;
  }

  /**
   * Sets the tiene parentesco.
   *
   * @param tieneParentesco the new tiene parentesco
   */
  public void setTieneParentesco(final Boolean tieneParentesco) {
    this.tieneParentesco = tieneParentesco;
  }

  /**
   * Gets the tiene certificado.
   *
   * @return the tiene certificado
   */
  public Boolean getTieneCertificado() {
    return tieneCertificado;
  }

  /**
   * Sets the tiene certificado.
   *
   * @param tieneCertificado the new tiene certificado
   */
  public void setTieneCertificado(final Boolean tieneCertificado) {
    this.tieneCertificado = tieneCertificado;
  }

  /**
   * Gets the tipo asistencia.
   *
   * @return the tipo asistencia
   */
  public Boolean getTipoAsistencia() {
    return tipoAsistencia;
  }

  /**
   * Sets the tipo asistencia.
   *
   * @param tipoAsistencia the new tipo asistencia
   */
  public void setTipoAsistencia(final Boolean tipoAsistencia) {
    this.tipoAsistencia = tipoAsistencia;
  }

  /**
   * Gets the tiene formacion.
   *
   * @return the tiene formacion
   */
  public Boolean getTieneFormacion() {
    return tieneFormacion;
  }

  /**
   * Sets the tiene formacion.
   *
   * @param tieneFormacion the new tiene formacion
   */
  public void setTieneFormacion(final Boolean tieneFormacion) {
    this.tieneFormacion = tieneFormacion;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final AsistentePersonalDTO o) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

  /**
   * Gets the formacion.
   *
   * @return the formacion
   */
  public CatalogoFormacionDTO getFormacion() {
    return formacion;
  }

  /**
   * Sets the formacion.
   *
   * @param formacion the new formacion
   */
  public void setFormacion(final CatalogoFormacionDTO formacion) {
    this.formacion = formacion;
  }

  /**
   * Gets the identificador.
   *
   * @return the identificador
   */
  public String getIdentificador() {
    return identificador;
  }

  /**
   * Sets the identificador.
   *
   * @param identificador the new identificador
   */
  public void setIdentificador(final String identificador) {
    this.identificador = identificador;
  }

  /**
   * Gets the tipo identificador.
   *
   * @return the tipo identificador
   */
  public Long getTipoIdentificador() {
    return tipoIdentificador;
  }

  /**
   * Sets the tipo identificador.
   *
   * @param tipoIdentificador the new tipo identificador
   */
  public void setTipoIdentificador(final Long tipoIdentificador) {
    this.tipoIdentificador = tipoIdentificador;
  }

  /**
   * Gets the activo pia.
   *
   * @return the activo pia
   */
  public Boolean getActivoPia() {
    return activoPia;
  }

  /**
   * Sets the activo pia.
   *
   * @param activoPia the new activo pia
   */
  public void setActivoPia(final Boolean activoPia) {
    this.activoPia = activoPia;
  }

  public String getNombreCompleto() {
    return nombreCompleto;
  }

  public void setNombreCompleto(final String nombreCompleto) {
    this.nombreCompleto = nombreCompleto;
  }

}
