package com.gval.gval.dto.dto.util;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.List;
import java.util.Map;

/**
 * The Class ButtonDTO.
 */
public class AlertaDTO extends BaseDTO {

  /** The Constant ON_CLOSE. */
  public static final String ON_CLOSE = "onClose";

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 8269900117926736795L;

  /** The label. */
  private List<Integer> botones;

  /** The command. */
  private String titulo;

  /** The visible. */
  private String mensaje;

  /** The icono. */
  private String icono;

  /** The disabled. */
  private Map<String, String> comandosBotones;



  /**
   * Instantiates a new alerta DTO.
   *
   * @param botones the botones
   * @param titulo the titulo
   * @param mensaje the mensaje
   * @param icono the icono
   * @param comandosBotones the comandos botones
   */
  public AlertaDTO(final List<Integer> botones, final String titulo,
      final String mensaje, final String icono,
      final Map<String, String> comandosBotones) {
    super();
    this.botones = UtilidadesCommons.collectorsToList(botones);
    this.titulo = titulo;
    this.mensaje = mensaje;
    this.icono = icono;
    this.comandosBotones = comandosBotones;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public List<Integer> getBotones() {
    return UtilidadesCommons.collectorsToList(botones);
  }

  /**
   * Gets the botonera.
   *
   * @return the botonera
   */
  public int getBotonera() {
    Integer botonera = null;
    for (final Integer boton : botones) {
      botonera = (botonera == null) ? boton : botonera | boton;
    }
    return botonera;
  }

  /**
   * Sets the label.
   *
   * @param botones the new label
   */
  public void setBotones(final List<Integer> botones) {
    this.botones = UtilidadesCommons.collectorsToList(botones);
  }

  /**
   * Gets the command.
   *
   * @return the command
   */
  public String getTitulo() {
    return titulo;
  }

  /**
   * Sets the command.
   *
   * @param titulo the new command
   */
  public void setTitulo(final String titulo) {
    this.titulo = titulo;
  }

  /**
   * Gets the visible.
   *
   * @return the visible
   */
  public String getMensaje() {
    return mensaje;
  }

  /**
   * Sets the visible.
   *
   * @param mensaje the new visible
   */
  public void setMensaje(final String mensaje) {
    this.mensaje = mensaje;
  }

  /**
   * Gets the disabled.
   *
   * @return the disabled
   */
  public Map<String, String> getComandosBotones() {
    return comandosBotones;
  }

  /**
   * Sets the disabled.
   *
   * @param comandosBotones the new disabled
   */
  public void setComandosBotones(final Map<String, String> comandosBotones) {
    this.comandosBotones = comandosBotones;
  }

  /**
   * Gets the icono.
   *
   * @return the icono
   */
  public String getIcono() {
    return icono;
  }

  /**
   * Sets the icono.
   *
   * @param icono the new icono
   */
  public void setIcono(final String icono) {
    this.icono = icono;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
