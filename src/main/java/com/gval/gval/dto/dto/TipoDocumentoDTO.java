package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.hibernate.validator.constraints.NotBlank;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

// TODO: Auto-generated Javadoc
/**
 * The Class TipoDocumentoDTO.
 */
public final class TipoDocumentoDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 6286739913171854311L;

  /** The pk tipo documento. */
  private Long pkTipoDocumento;

  /** The anyadir paginas. */
  private Boolean anyadirPaginas;

  /** The tipo aplicacion. */
  @Size(max = 5)
  private String tipoAplicacion;

  /** The aporta ayuntamiento. */
  private Boolean aportaAyuntamiento;

  /** The aporta ayuntamiento finalizado. */
  private Boolean aportaAyuntamientoFinalizado;

  /** The autoriza acceso. */
  private Boolean autorizaAcceso;

  /** The caracter. */
  @Size(max = 1)
  private String caracter;

  /** The codigo. */
  @NotNull
  @Size(max = 15)
  private String codigo;

  /** The activo. */
  @NotNull
  private Boolean activo;

  /** The fecha creacion. */
  @NotNull
  private Date fechaCreacion;

  /** The identificador sforms. */
  @Size(max = 80)
  private String identificadorSforms;

  /** The mastin. */
  private Boolean mastin;

  /** The nombre. */
  @Size(max = 80)
  private String nombre;

  /** The orden. */
  private Long orden;

  /** The pagina blanco. */
  private Boolean paginaBlanco;

  /** The tipo impresion. */
  @Size(max = 50)
  private String tipoImpresion;

  /** The puede imprimirse. */
  @NotNull
  private Boolean puedeImprimirse;

  /** The tipuedepedirse. */
  private Boolean tipuedepedirse;

  /** The subsanable. */
  private Boolean subsanable;

  /** The subsanable heredero. */
  private Boolean subsanableHeredero;

  /** The tiene estado. */
  @NotNull
  private Boolean tieneEstado;

  /** The tipo. */
  @NotNull
  @NotBlank
  private String tipo;

  /** The tipo documento resolucion. */
  @Size(max = 3)
  private String tipoDocumentoResolucion;

  /** The tipo resolucion. */
  @Size(max = 3)
  private String tipoResolucion;

  /** The subsanable ayuntamiento. */
  private Boolean subsanableAyuntamiento;

  /** The verificacion. */
  private Boolean verificacion;

  /** The bloqueo. */
  private Boolean bloqueo;

  /** The escontrato prestacion. */
  private Boolean esContratoPrestacion;

  /** The imprimible. */
  private Boolean imprimible;


  /** The tibloqaporta. */
  private String tibloqaporta;
  /** The sdm plantilla activa. */
  private List<PlantillaDTO> sdmPlantillaActiva;

  /** The firmas documento. */
  private Set<FirmaDocumentoDTO> firmasDocumento;

  /**
   * Instantiates a new tipo documento DTO.
   */
  public TipoDocumentoDTO() {
    super();
  }

  /**
   * Gets the pk tipo documento.
   *
   * @return the pkTipoDocumento
   */
  public Long getPkTipoDocumento() {
    return pkTipoDocumento;
  }


  /**
   * Sets the pk tipo documento.
   *
   * @param pkTipoDocumento the pkTipoDocumento to set
   */
  public void setPkTipoDocumento(final Long pkTipoDocumento) {
    this.pkTipoDocumento = pkTipoDocumento;
  }


  /**
   * Gets the anyadir paginas.
   *
   * @return the anyadirPaginas
   */
  public Boolean getAnyadirPaginas() {
    return anyadirPaginas;
  }


  /**
   * Sets the anyadir paginas.
   *
   * @param anyadirPaginas the anyadirPaginas to set
   */
  public void setAnyadirPaginas(final Boolean anyadirPaginas) {
    this.anyadirPaginas = anyadirPaginas;
  }


  /**
   * Gets the tipo aplicacion.
   *
   * @return the tipoAplicacion
   */
  public String getTipoAplicacion() {
    return tipoAplicacion;
  }


  /**
   * Sets the tipo aplicacion.
   *
   * @param tipoAplicacion the tipoAplicacion to set
   */
  public void setTipoAplicacion(final String tipoAplicacion) {
    this.tipoAplicacion = tipoAplicacion;
  }


  /**
   * Gets the aporta ayuntamiento.
   *
   * @return the aportaAyuntamiento
   */
  public Boolean getAportaAyuntamiento() {
    return aportaAyuntamiento;
  }


  /**
   * Sets the aporta ayuntamiento.
   *
   * @param aportaAyuntamiento the aportaAyuntamiento to set
   */
  public void setAportaAyuntamiento(final Boolean aportaAyuntamiento) {
    this.aportaAyuntamiento = aportaAyuntamiento;
  }


  /**
   * Gets the aporta ayuntamiento finalizado.
   *
   * @return the aportaAyuntamientoFinalizado
   */
  public Boolean getAportaAyuntamientoFinalizado() {
    return aportaAyuntamientoFinalizado;
  }


  /**
   * Sets the aporta ayuntamiento finalizado.
   *
   * @param aportaAyuntamientoFinalizado the aportaAyuntamientoFinalizado to set
   */
  public void setAportaAyuntamientoFinalizado(
      final Boolean aportaAyuntamientoFinalizado) {
    this.aportaAyuntamientoFinalizado = aportaAyuntamientoFinalizado;
  }

  /**
   * Gets the autoriza acceso.
   *
   * @return the autorizaAcceso
   */
  public Boolean getAutorizaAcceso() {
    return autorizaAcceso;
  }

  /**
   * Sets the autoriza acceso.
   *
   * @param autorizaAcceso the autorizaAcceso to set
   */
  public void setAutorizaAcceso(final Boolean autorizaAcceso) {
    this.autorizaAcceso = autorizaAcceso;
  }


  /**
   * Gets the caracter.
   *
   * @return the caracter
   */
  public String getCaracter() {
    return caracter;
  }


  /**
   * Sets the caracter.
   *
   * @param caracter the caracter to set
   */
  public void setCaracter(final String caracter) {
    this.caracter = caracter;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the codigo to set
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the identificador sforms.
   *
   * @return the identificadorSforms
   */
  public String getIdentificadorSforms() {
    return identificadorSforms;
  }

  /**
   * Sets the identificador sforms.
   *
   * @param identificadorSforms the identificadorSforms to set
   */
  public void setIdentificadorSforms(final String identificadorSforms) {
    this.identificadorSforms = identificadorSforms;
  }

  /**
   * Gets the mastin.
   *
   * @return the mastin
   */
  public Boolean getMastin() {
    return mastin;
  }

  /**
   * Sets the mastin.
   *
   * @param mastin the mastin to set
   */
  public void setMastin(final Boolean mastin) {
    this.mastin = mastin;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the nombre to set
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the orden.
   *
   * @return the orden
   */
  public Long getOrden() {
    return orden;
  }

  /**
   * Sets the orden.
   *
   * @param orden the orden to set
   */
  public void setOrden(final Long orden) {
    this.orden = orden;
  }

  /**
   * Gets the pagina blanco.
   *
   * @return the paginaBlanco
   */
  public Boolean getPaginaBlanco() {
    return paginaBlanco;
  }

  /**
   * Sets the pagina blanco.
   *
   * @param paginaBlanco the paginaBlanco to set
   */
  public void setPaginaBlanco(final Boolean paginaBlanco) {
    this.paginaBlanco = paginaBlanco;
  }

  /**
   * Gets the tipo impresion.
   *
   * @return the tipoImpresion
   */
  public String getTipoImpresion() {
    return tipoImpresion;
  }

  /**
   * Sets the tipo impresion.
   *
   * @param tipoImpresion the tipoImpresion to set
   */
  public void setTipoImpresion(final String tipoImpresion) {
    this.tipoImpresion = tipoImpresion;
  }

  /**
   * Gets the puede imprimirse.
   *
   * @return the puedeImprimirse
   */
  public Boolean getPuedeImprimirse() {
    return puedeImprimirse;
  }

  /**
   * Sets the puede imprimirse.
   *
   * @param puedeImprimirse the puedeImprimirse to set
   */
  public void setPuedeImprimirse(final Boolean puedeImprimirse) {
    this.puedeImprimirse = puedeImprimirse;
  }

  /**
   * Gets the tipuedepedirse.
   *
   * @return the tipuedepedirse
   */
  public Boolean getTipuedepedirse() {
    return tipuedepedirse;
  }

  /**
   * Sets the tipuedepedirse.
   *
   * @param tipuedepedirse the tipuedepedirse to set
   */
  public void setTipuedepedirse(final Boolean tipuedepedirse) {
    this.tipuedepedirse = tipuedepedirse;
  }

  /**
   * Gets the subsanable.
   *
   * @return the subsanable
   */
  public Boolean getSubsanable() {
    return subsanable;
  }

  /**
   * Sets the subsanable.
   *
   * @param subsanable the subsanable to set
   */
  public void setSubsanable(final Boolean subsanable) {
    this.subsanable = subsanable;
  }

  /**
   * Gets the subsanable heredero.
   *
   * @return the subsanableHeredero
   */
  public Boolean getSubsanableHeredero() {
    return subsanableHeredero;
  }

  /**
   * Sets the subsanable heredero.
   *
   * @param subsanableHeredero the subsanableHeredero to set
   */
  public void setSubsanableHeredero(final Boolean subsanableHeredero) {
    this.subsanableHeredero = subsanableHeredero;
  }

  /**
   * Gets the tiene estado.
   *
   * @return the tieneEstado
   */
  public Boolean getTieneEstado() {
    return tieneEstado;
  }

  /**
   * Sets the tiene estado.
   *
   * @param tieneEstado the tieneEstado to set
   */
  public void setTieneEstado(final Boolean tieneEstado) {
    this.tieneEstado = tieneEstado;
  }

  /**
   * Gets the tipo.
   *
   * @return the tipo
   */
  public String getTipo() {
    return tipo;
  }

  /**
   * Sets the tipo.
   *
   * @param tipo the tipo to set
   */
  public void setTipo(final String tipo) {
    this.tipo = tipo;
  }

  /**
   * Gets the tipo documento resolucion.
   *
   * @return the tipoDocumentoResolucion
   */
  public String getTipoDocumentoResolucion() {
    return tipoDocumentoResolucion;
  }

  /**
   * Sets the tipo documento resolucion.
   *
   * @param tipoDocumentoResolucion the tipoDocumentoResolucion to set
   */
  public void setTipoDocumentoResolucion(final String tipoDocumentoResolucion) {
    this.tipoDocumentoResolucion = tipoDocumentoResolucion;
  }

  /**
   * Gets the tipo resolucion.
   *
   * @return the tipoResolucion
   */
  public String getTipoResolucion() {
    return tipoResolucion;
  }

  /**
   * Sets the tipo resolucion.
   *
   * @param tipoResolucion the tipoResolucion to set
   */
  public void setTipoResolucion(final String tipoResolucion) {
    this.tipoResolucion = tipoResolucion;
  }

  /**
   * Gets the subsanable ayuntamiento.
   *
   * @return the subsanable ayuntamiento
   */
  public Boolean getSubsanableAyuntamiento() {
    return subsanableAyuntamiento;
  }

  /**
   * Sets the subsanable ayuntamiento.
   *
   * @param subsanableAyuntamiento the new subsanable ayuntamiento
   */
  public void setSubsanableAyuntamiento(final Boolean subsanableAyuntamiento) {
    this.subsanableAyuntamiento = subsanableAyuntamiento;
  }

  /**
   * Gets the verificacion.
   *
   * @return the verificacion
   */
  public Boolean getVerificacion() {
    return verificacion;
  }

  /**
   * Sets the verificacion.
   *
   * @param verificacion the new verificacion
   */
  public void setVerificacion(final Boolean verificacion) {
    this.verificacion = verificacion;
  }

  /**
   * Gets the bloqueo.
   *
   * @return the bloqueo
   */
  public Boolean getBloqueo() {
    return bloqueo;
  }

  /**
   * Sets the bloqueo.
   *
   * @param bloqueo the new bloqueo
   */
  public void setBloqueo(final Boolean bloqueo) {
    this.bloqueo = bloqueo;
  }

  /**
   * Gets the es contrato prestacion.
   *
   * @return the es contrato prestacion
   */
  public Boolean getEsContratoPrestacion() {
    return esContratoPrestacion;
  }

  /**
   * Sets the es contrato prestacion.
   *
   * @param esContratoPrestacion the new es contrato prestacion
   */
  public void setEsContratoPrestacion(final Boolean esContratoPrestacion) {
    this.esContratoPrestacion = esContratoPrestacion;
  }

  /**
   * Gets the imprimible.
   *
   * @return the imprimible
   */
  public Boolean getImprimible() {
    return imprimible;
  }

  /**
   * Sets the imprimible.
   *
   * @param imprimible the new imprimible
   */
  public void setImprimible(final Boolean imprimible) {
    this.imprimible = imprimible;
  }

  /**
   * Gets the tibloqaporta.
   *
   * @return the tibloqaporta
   */
  public String getTibloqaporta() {
    return tibloqaporta;
  }

  /**
   * Sets the tibloqaporta.
   *
   * @param tibloqaporta the new tibloqaporta
   */
  public void setTibloqaporta(final String tibloqaporta) {
    this.tibloqaporta = tibloqaporta;
  }

  /**
   * Gets the sdm plantilla activa.
   *
   * @return the sdm plantilla activa
   */
  /*
   * Gets the sdm plantilla activa.
   *
   * @return the sdm plantilla activa
   */
  public List<PlantillaDTO> getSdmPlantillaActiva() {
    return sdmPlantillaActiva;
  }

  /**
   * Sets the sdm plantilla activa.
   *
   * @param sdmPlantillaActiva the new sdm plantilla activa
   */
  public void setSdmPlantillaActiva(
      final List<PlantillaDTO> sdmPlantillaActiva) {
    this.sdmPlantillaActiva = sdmPlantillaActiva;
  }

  /**
   * Gets the firmas documento.
   *
   * @return the firmas documento
   */
  public Set<FirmaDocumentoDTO> getFirmasDocumento() {
    return firmasDocumento;
  }

  /**
   * Sets the firmas documento.
   *
   * @param firmasDocumento the new firmas documento
   */
  public void setFirmasDocumento(final Set<FirmaDocumentoDTO> firmasDocumento) {
    this.firmasDocumento = firmasDocumento;
  }

}
