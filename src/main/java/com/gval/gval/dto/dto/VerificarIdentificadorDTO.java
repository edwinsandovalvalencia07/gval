package com.gval.gval.dto.dto;

import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.gval.gval.common.UtilidadesCommons;
import es.gva.dependencia.ada.common.enums.RolPersonaEnum;
import com.gval.gval.dto.dto.util.BaseDTO;


/**
 * The Class VerificarIdentificadorDTO.
 */
public class VerificarIdentificadorDTO extends BaseDTO
    implements Comparable<CuidadorDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 9003897454448570381L;

  /** The identificador. */
  private final String identificador; // dni

  /** The expediente. */
  private final ExpedienteDTO expediente;

  /** The rol persona. */
  private RolPersonaEnum rolPersona;

  /** The solicitante. */
  private final String solicitante;

  /** The cuidadores. */
  private List<String> cuidadores;

  /** The asistentes. */
  private List<String> asistentes;

  /** The representantes. */
  private List<String> representantes;

  /** The familiares. */
  private List<String> familiares;

  /**
   * Instantiates a new verificar identificador DTO.
   *
   * @param identificador the identificador
   * @param expediente the expediente
   * @param rolPersona the rol persona
   */
  public VerificarIdentificadorDTO(final String identificador,
      final ExpedienteDTO expediente, final RolPersonaEnum rolPersona) {
    super();
    this.identificador = identificador;
    this.expediente = expediente;
    this.rolPersona = rolPersona;
    this.solicitante = expediente.getSolicitante().getId().getNif();
  }

  /**
   * Gets the cuidadores.
   *
   * @return the cuidadores
   */
  public List<String> getCuidadores() {
    return UtilidadesCommons.collectorsToList(cuidadores);
  }

  /**
   * Sets the cuidadores.
   *
   * @param cuidadores the new cuidadores
   */
  public void setCuidadores(final List<String> cuidadores) {
    this.cuidadores = UtilidadesCommons.collectorsToList(cuidadores);
  }

  /**
   * Gets the asistentes.
   *
   * @return the asistentes
   */
  public List<String> getAsistentes() {
    return UtilidadesCommons.collectorsToList(asistentes);
  }

  /**
   * Sets the asistentes.
   *
   * @param asistentes the new asistentes
   */
  public void setAsistentes(final List<String> asistentes) {
    this.asistentes = UtilidadesCommons.collectorsToList(asistentes);
  }

  /**
   * Gets the representantes.
   *
   * @return the representantes
   */
  public List<String> getRepresentantes() {
    return UtilidadesCommons.collectorsToList(representantes);
  }

  /**
   * Sets the representantes.
   *
   * @param representantes the new representantes
   */
  public void setRepresentantes(final List<String> representantes) {
    this.representantes = UtilidadesCommons.collectorsToList(representantes);
  }

  /**
   * Gets the familiares.
   *
   * @return the familiares
   */
  public List<String> getFamiliares() {
    return UtilidadesCommons.collectorsToList(familiares);
  }

  /**
   * Sets the familiares.
   *
   * @param familiares the new familiares
   */
  public void setFamiliares(final List<String> familiares) {
    this.familiares = UtilidadesCommons.collectorsToList(familiares);
  }

  /**
   * Gets the identificador.
   *
   * @return the identificador
   */
  public String getIdentificador() {
    return identificador;
  }

  /**
   * Gets the expediente.
   *
   * @return the expediente
   */
  public ExpedienteDTO getExpediente() {
    return expediente;
  }

  /**
   * Gets the solicitante.
   *
   * @return the solicitante
   */
  public String getSolicitante() {
    return solicitante;
  }

  /**
   * Gets the rol persona.
   *
   * @return the rol persona
   */
  public RolPersonaEnum getRolPersona() {
    return rolPersona;
  }

  /**
   * Sets the rol persona.
   *
   * @param rolPersona the new rol persona
   */
  public void setRolPersona(final RolPersonaEnum rolPersona) {
    this.rolPersona = rolPersona;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

  /**
   * Compare to.
   *
   * @param arg0 the arg 0
   * @return the int
   */
  @Override
  public int compareTo(final CuidadorDTO arg0) {
    return 0;
  }

}
