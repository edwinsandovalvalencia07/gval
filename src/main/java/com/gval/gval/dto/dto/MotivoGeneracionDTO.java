/**
 * Copyright (c) 2020 Generalitat Valenciana - Todos los derechos reservados.
 */
package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.hibernate.validator.constraints.NotBlank;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * The Class MotivoGeneracionDTO.
 */
public final class MotivoGeneracionDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 2634481546564540771L;

  /** The pk motgeneracion. */
  private Long pkMotgeneracion;

  /** The activo. */
  @NotNull
  private Boolean activo;

  /** The auto. */
  @NotNull
  private Boolean auto;

  /** The codigo. */
  @NotBlank
  @Size(max = 3)
  private String codigo;

  /** The descripcion. */
  @NotBlank
  @Size(max = 100)
  private String descripcion;

  /** The fecha creacion. */
  @NotNull
  private Date fechaCreacion;

  /** The manual. */
  @NotNull
  private Boolean manual;

  /** The nombre. */
  @NotBlank
  @Size(max = 30)
  private String nombre;

  /**
   * Instantiates a new motivo generacion DTO.
   */
  public MotivoGeneracionDTO() {
    super();
    this.activo = Boolean.TRUE;
    this.auto = Boolean.TRUE;
    this.manual = Boolean.TRUE;
    this.fechaCreacion = new Date();
  }

  /**
   * Gets the pk motgeneracion.
   *
   * @return the pkMotgeneracion
   */
  public Long getPkMotgeneracion() {
    return pkMotgeneracion;
  }

  /**
   * Sets the pk motgeneracion.
   *
   * @param pkMotgeneracion the pkMotgeneracion to set
   */
  public void setPkMotgeneracion(final Long pkMotgeneracion) {
    this.pkMotgeneracion = pkMotgeneracion;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the auto.
   *
   * @return the auto
   */
  public Boolean getAuto() {
    return auto;
  }

  /**
   * Sets the auto.
   *
   * @param auto the auto to set
   */
  public void setAuto(final Boolean auto) {
    this.auto = auto;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the codigo to set
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the descripcion to set
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the manual.
   *
   * @return the manual
   */
  public Boolean getManual() {
    return manual;
  }

  /**
   * Sets the manual.
   *
   * @param manual the manual to set
   */
  public void setManual(final Boolean manual) {
    this.manual = manual;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the nombre to set
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }
}
