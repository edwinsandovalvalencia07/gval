package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import es.gva.dependencia.ada.common.enums.TipoCambioEnum;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.zkoss.util.resource.Labels;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.Size;

/**
 * The Class VersionAdaDTO.
 */
public class VersionAdaDTO extends BaseDTO
    implements Comparable<VersionAdaDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 3481264772823145832L;

  /** The pk version. */
  private Long pkVersion;

  /** The fecha version. */
  private Date fechaVersion;

  /** The fecha crea. */
  private Date fechaCrea;

  /** The num version. */
  @Size(max = 20)
  private String numVersion;

  /** The activo. */
  private Boolean activo;

  /** The versiones ada. */
  private List<CambioVersionDTO> cambiosVersion;

  /** The es ultima version. */
  private boolean esUltimaVersion;

  /**
   * Instantiates a new version ada DTO.
   */
  public VersionAdaDTO() {
    super();
    esUltimaVersion = false;
  }

  /**
   * Gets the titulo fecha.
   *
   * @return the titulo fecha
   */
  public String getTituloFecha() {
    return Labels.getLabel("label.historial-versiones.version")
        .concat(StringUtils.SPACE).concat(numVersion).concat(StringUtils.SPACE)
        .concat(UtilidadesCommons.dateToString(fechaVersion,
            UtilidadesCommons.FORMAT_DATE_FECHA));
  }

  /**
   * Gets the pk version.
   *
   * @return the pk version
   */
  public Long getPkVersion() {
    return this.pkVersion;
  }

  /**
   * Sets the pk version.
   *
   * @param pkVersion the new pk version
   */
  public void setPkVersion(final Long pkVersion) {
    this.pkVersion = pkVersion;
  }

  /**
   * Gets the fecha version.
   *
   * @return the fecha version
   */
  public Date getFechaVersion() {
    return UtilidadesCommons.cloneDate(this.fechaVersion);
  }

  /**
   * Sets the fecha version.
   *
   * @param fechaVersion the new fecha version
   */
  public void setFechaVersion(final Date fechaVersion) {
    this.fechaVersion = UtilidadesCommons.cloneDate(fechaVersion);
  }

  /**
   * Gets the fecha crea.
   *
   * @return the fecha crea
   */
  public Date getFechaCrea() {
    return UtilidadesCommons.cloneDate(this.fechaCrea);
  }

  /**
   * Sets the fecha crea.
   *
   * @param fechaCrea the new fecha crea
   */
  public void setFechaCrea(final Date fechaCrea) {
    this.fechaCrea = UtilidadesCommons.cloneDate(fechaCrea);
  }

  /**
   * Gets the num version.
   *
   * @return the num version
   */
  public String getNumVersion() {
    return this.numVersion;
  }

  /**
   * Sets the num version.
   *
   * @param numVersion the new num version
   */
  public void setNumVersion(final String numVersion) {
    this.numVersion = numVersion;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return this.activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }


  /**
   * Checks if is es ultima version.
   *
   * @return true, if is es ultima version
   */
  public boolean isEsUltimaVersion() {
    return this.esUltimaVersion;
  }

  /**
   * Sets the es ultima version.
   *
   * @param esUltimaVersion the new es ultima version
   */
  public void setEsUltimaVersion(final boolean esUltimaVersion) {
    this.esUltimaVersion = esUltimaVersion;
  }

  /**
   * Gets the cambios version.
   *
   * @return the cambios version
   */
  public List<CambioVersionDTO> getCambiosVersion() {
    return UtilidadesCommons.collectorsToList(cambiosVersion);
  }

  /**
   * Sets the cambios version.
   *
   * @param cambiosVersion the new cambios version
   */
  public void setCambiosVersion(final List<CambioVersionDTO> cambiosVersion) {
    this.cambiosVersion = UtilidadesCommons.collectorsToList(cambiosVersion);
  }

  /**
   * Gets the mejoras version.
   *
   * @return the mejoras version
   */
  public List<CambioVersionDTO> getMejorasVersion() {
    return CollectionUtils.isEmpty(cambiosVersion) ? new ArrayList<>()
        : cambiosVersion.stream().filter(
            cambio -> TipoCambioEnum.MEJORA.getValue().equals(cambio.getTipo()))
            .collect(Collectors.toList());
  }

  /**
   * Gets the correcciones version.
   *
   * @return the correcciones version
   */
  public List<CambioVersionDTO> getCorreccionesVersion() {
    return CollectionUtils
        .isEmpty(cambiosVersion)
            ? new ArrayList<>()
            : cambiosVersion.stream().filter(cambio -> TipoCambioEnum.CORRECCION
                .getValue().equals(cambio.getTipo()))
                .collect(Collectors.toList());
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final VersionAdaDTO o) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
