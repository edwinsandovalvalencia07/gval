package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;

/**
 * The Class RelacionCambioCuidadorDTO.
 */
public class RelacionCambioCuidadorDTO extends BaseDTO
    implements Comparable<RelacionCambioCuidadorDTO> {


  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -6734229811994334195L;

  /** The pk cuidador. */
  private Long pkRelacionCambioCuidador;

  /** The cuidador. */
  // bi-directional many-to-one association to SdmCnp
  private CuidadorDTO cuidador;

  /** The activo. */
  private Boolean activo;

  /** The fecha crea registro. */
  private Date fechaCreacion;

  /** Usuario asociado. */
  private UsuarioDTO usuario;


  // TODO:Completar cuando se desarrolle PIA
  // bi-directional many-to-one association to SdmPia
  // private SdmPiaDTO sdmPia;

  // TODO:Completar cuando se desarrolle RESOLUCIONES
  // bi-directional many-to-one association to SdmResoluci
  // private SdmResoluciDTO sdmResoluci;

  /**
   * Instantiates a new relacion cambio cuidador DTO.
   */
  public RelacionCambioCuidadorDTO() {
    super();
  }


  /**
   * Gets the pk relacion cambio cuidador.
   *
   * @return the pk relacion cambio cuidador
   */
  public Long getPkRelacionCambioCuidador() {
    return pkRelacionCambioCuidador;
  }


  /**
   * Sets the pk relacion cambio cuidador.
   *
   * @param pkRelacionCambioCuidador the new pk relacion cambio cuidador
   */
  public void setPkRelacionCambioCuidador(final Long pkRelacionCambioCuidador) {
    this.pkRelacionCambioCuidador = pkRelacionCambioCuidador;
  }


  /**
   * Gets the cuidador.
   *
   * @return the cuidador
   */
  public CuidadorDTO getCuidador() {
    return cuidador;
  }


  /**
   * Sets the cuidador.
   *
   * @param cuidador the new cuidador
   */
  public void setCuidador(final CuidadorDTO cuidador) {
    this.cuidador = cuidador;
  }


  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }


  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }


  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }


  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }


  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public UsuarioDTO getUsuario() {
    return usuario;
  }


  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final UsuarioDTO usuario) {
    this.usuario = usuario;
  }


  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final RelacionCambioCuidadorDTO o) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
