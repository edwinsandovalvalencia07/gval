package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import es.gva.dependencia.ada.common.enums.listados.CategoriaTipoPersonaEnum;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.zkoss.util.resource.Labels;

/**
 * The Class DocumentoRequeridoDTO.
 */
public class DocumentoRequeridoDTO extends BaseDTO
    implements Comparable<DocumentoRequeridoDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 6514712594072150686L;

  // Posiciones del array que devuelve la consulta
  /** The Constant POS_CATEGORIA. */
  private static final int POS_CATEGORIA = 0;

  /** The Constant POS_PK_TIPO_DOCUMENTO. */
  private static final int POS_PK_TIPO_DOCUMENTO = 1;

  /** The Constant POS_NOMBRE_TIPO_DOCUMENTO. */
  private static final int POS_NOMBRE_TIPO_DOCUMENTO = 2;

  /** The Constant POS_PK_PERSONA. */
  private static final int POS_PK_PERSONA = 3;

  /** The Constant POS_NOMBRE_COMPLETO_PERSONA. */
  private static final int POS_NOMBRE_COMPLETO_PERSONA = 4;


  /** The categoria. */
  private String categoria;

  /** The pk persona. */
  private Long pkPersona;

  /** The nombre completo persona. */
  private String nombreCompletoPersona;

  /** The pk persona. */
  private Long pkTipoDocumento;

  /** The nombre completo persona. */
  private String nombreTipoDocumento;

  /** The obligatorio. */
  private Boolean obligatorio;

  /** The bloqueante. */
  private Boolean bloqueante;


  /**
   * Instantiates a new documento requerido DTO.
   */
  public DocumentoRequeridoDTO() {
    super();
  }

  /**
   * Instantiates a new documento requerido DTO.
   *
   * @param categoria the categoria
   * @param pkTipoDocumento the pk tipo documento
   * @param nombreTipoDocumento the nombre tipo documento
   * @param pkPersona the pk persona
   * @param nombreCompletoPersona the nombre completo persona
   */
  public DocumentoRequeridoDTO(final String categoria,
      final Long pkTipoDocumento, final String nombreTipoDocumento,
      final Long pkPersona, final String nombreCompletoPersona) {
    super();
    this.categoria = categoria;
    this.pkTipoDocumento = pkTipoDocumento;
    this.nombreTipoDocumento = nombreTipoDocumento;
    this.pkPersona = pkPersona;
    this.nombreCompletoPersona = nombreCompletoPersona;
  }



  /**
   * Instantiates a new documento requerido DTO.
   *
   * @param documentoRequeridoRaw the documento requerido raw
   */
  public DocumentoRequeridoDTO(final Object[] documentoRequeridoRaw) {
    this(UtilidadesCommons.convertObjectToString(documentoRequeridoRaw[POS_CATEGORIA]),
        UtilidadesCommons
            .convertObjectToLong(documentoRequeridoRaw[POS_PK_TIPO_DOCUMENTO]),
        UtilidadesCommons
            .convertObjectToString(documentoRequeridoRaw[POS_NOMBRE_TIPO_DOCUMENTO]),
        UtilidadesCommons.convertObjectToLong(documentoRequeridoRaw[POS_PK_PERSONA]),
        UtilidadesCommons.convertObjectToString(
            documentoRequeridoRaw[POS_NOMBRE_COMPLETO_PERSONA]));
  }


  /**
   * Gets the categoria.
   *
   * @return the categoria
   */
  public String getCategoria() {
    return categoria;
  }

  /**
   * Gets the categoria label.
   *
   * @return the categoria label
   */
  public String getCategoriaLabel() {
    final String label = CategoriaTipoPersonaEnum.labelFromValor(categoria);
    return label == null ? null : Labels.getLabel(label);
  }

  /**
   * Sets the categoria.
   *
   * @param categoria the new categoria
   */
  public void setCategoria(final String categoria) {
    this.categoria = categoria;
  }

  /**
   * Gets the pk persona.
   *
   * @return the pk persona
   */
  public Long getPkPersona() {
    return pkPersona;
  }

  /**
   * Sets the pk persona.
   *
   * @param pkPersona the new pk persona
   */
  public void setPkPersona(final Long pkPersona) {
    this.pkPersona = pkPersona;
  }

  /**
   * Gets the nombre completo persona.
   *
   * @return the nombre completo persona
   */
  public String getNombreCompletoPersona() {
    return nombreCompletoPersona;
  }

  /**
   * Sets the nombre completo persona.
   *
   * @param nombreCompletoPersona the new nombre completo persona
   */
  public void setNombreCompletoPersona(final String nombreCompletoPersona) {
    this.nombreCompletoPersona = nombreCompletoPersona;
  }

  /**
   * Gets the pk tipo documento.
   *
   * @return the pk tipo documento
   */
  public Long getPkTipoDocumento() {
    return pkTipoDocumento;
  }

  /**
   * Sets the pk tipo documento.
   *
   * @param pkTipoDocumento the new pk tipo documento
   */
  public void setPkTipoDocumento(final Long pkTipoDocumento) {
    this.pkTipoDocumento = pkTipoDocumento;
  }

  /**
   * Gets the nombre tipo documento.
   *
   * @return the nombre tipo documento
   */
  public String getNombreTipoDocumento() {
    return nombreTipoDocumento;
  }

  /**
   * Sets the nombre tipo documento.
   *
   * @param nombreTipoDocumento the new nombre tipo documento
   */
  public void setNombreTipoDocumento(final String nombreTipoDocumento) {
    this.nombreTipoDocumento = nombreTipoDocumento;
  }



  /**
   * Gets the obligatorio.
   *
   * @return the obligatorio
   */
  public Boolean getObligatorio() {
    return obligatorio;
  }

  /**
   * Sets the obligatorio.
   *
   * @param obligatorio the new obligatorio
   */
  public void setObligatorio(final Boolean obligatorio) {
    this.obligatorio = obligatorio;
  }

  /**
   * Gets the bloqueante.
   *
   * @return the bloqueante
   */
  public Boolean getBloqueante() {
    return bloqueante;
  }

  /**
   * Sets the bloqueante.
   *
   * @param bloqueante the new bloqueante
   */
  public void setBloqueante(final Boolean bloqueante) {
    this.bloqueante = bloqueante;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final DocumentoRequeridoDTO o) {
    return 0;
  }
}
