package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;


/**
 * The Class PlantillaDTO.
 */
public class PlantillaDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk plantilla. */
  private Long pkPlantilla;

  /** The activo. */
  private Boolean activo;

  /** The codigo. */
  private String codigo;

  /** The fecha hasta. */
  private Date fechaHasta;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The fecha desde. */
  private Date fechaDesde;

  /** The fichero. */
  private byte[] fichero;

  /** The nombre. */
  private String nombre;

  /** The documento plantilla. */
  private String documentoPlantilla;

  /** The tipo documento. */
  private TipoDocumentoDTO tipoDocumento;


  /**
   * Instantiates a new plantilla.
   */
  public PlantillaDTO() {
    super();
  }


  /**
   * Gets the pk plantilla.
   *
   * @return the pk plantilla
   */
  public Long getPkPlantilla() {
    return pkPlantilla;
  }


  /**
   * Sets the pk plantilla.
   *
   * @param pkPlantilla the new pk plantilla
   */
  public void setPkPlantilla(final Long pkPlantilla) {
    this.pkPlantilla = pkPlantilla;
  }


  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }


  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }


  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the fecha hasta.
   *
   * @return the fecha hasta
   */
  public Date getFechaHasta() {
    return UtilidadesCommons.cloneDate(fechaHasta);
  }


  /**
   * Sets the fecha hasta.
   *
   * @param fechaHasta the new fecha hasta
   */
  public void setFechaHasta(final Date fechaHasta) {
    this.fechaHasta = UtilidadesCommons.cloneDate(fechaHasta);
  }


  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }


  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }


  /**
   * Gets the fecha desde.
   *
   * @return the fecha desde
   */
  public Date getFechaDesde() {
    return UtilidadesCommons.cloneDate(fechaDesde);
  }


  /**
   * Sets the fecha desde.
   *
   * @param fechaDesde the new fecha desde
   */
  public void setFechaDesde(final Date fechaDesde) {
    this.fechaDesde = UtilidadesCommons.cloneDate(fechaDesde);
  }


  /**
   * Gets the fichero.
   *
   * @return the fichero
   */
  public byte[] getFichero() {
    return UtilidadesCommons.cloneBytes(fichero);
  }


  /**
   * Sets the fichero.
   *
   * @param fichero the new fichero
   */
  public void setFichero(final byte[] fichero) {
    this.fichero = UtilidadesCommons.cloneBytes(fichero);
  }


  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }


  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }


  /**
   * Gets the documento plantilla.
   *
   * @return the documento plantilla
   */
  public String getDocumentoPlantilla() {
    return documentoPlantilla;
  }


  /**
   * Sets the documento plantilla.
   *
   * @param documentoPlantilla the new documento plantilla
   */
  public void setDocumentoPlantilla(final String documentoPlantilla) {
    this.documentoPlantilla = documentoPlantilla;
  }


  /**
   * Gets the tipo documento.
   *
   * @return the tipo documento
   */
  public TipoDocumentoDTO getTipoDocumento() {
    return tipoDocumento;
  }


  /**
   * Sets the tipo documento.
   *
   * @param tipoDocumento the new tipo documento
   */
  public void setTipoDocumento(final TipoDocumentoDTO tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
