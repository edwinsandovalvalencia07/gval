package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import java.util.Date;

/**
 * The Class ResolucionListadoDTO.
 */
public class ResolucionListadoDTO extends BaseDTO
    implements Comparable<ResolucionListadoDTO> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -419440574040012666L;

  /** The pk resolucion. */
  private Long pkResolucion;

  /** The retipo. */
  private String retipo;

  /** The pk resolucion caducada. */
  private Long pkResolucionCaducada;

  /** The pk expedien. */
  private Long pkExpedien;

  /** The pk solicitud. */
  private Long pkSolicitud;

  /** The codigo solicitud. */
  private String codigoSolicitud;

  /** The tipo resolucion. */
  private String tipoResolucion;

  /** The fecha resolucion. */
  private Date fechaResolucion;

  /** The fecha efecto. */
  private Date fechaEfecto;

  /** The fecha notificacion. */
  private Date fechaNotificacion;

  /** The fecha firma. */
  private Date fechaFirma;

  /** The fecha baja. */
  private Date fechaBaja;

  /** The usuario. */
  private String usuario;

  /** The motivo. */
  private String motivo;

  /** The fecha recurso. */
  private Date fechaRecurso;

  /** The revocada. */
  private Boolean revocada;

  /** The fecha publicacion boe. */
  private Date fechaPublicacionBoe;

  /** The lote. */
  private Integer lote;

  /** The estado lote. */
  private String estadoLote;

  /** The notas migracion. */
  private String notasMigracion;

  /** The pk pia. */
  private Long pkPia;

  /** The pk retro pia. */
  private Long pkRetroPia;

  /** The pia en listado fisc. */
  private String piaEnListadoFisc;

  /** The retro en listado fisc. */
  private String retroEnListadoFisc;

  /** The nombre documento. */
  private String doNombre;

  /** The pk tipodocu. */
  private Long pkTipodocu;

  /** The pk docu. */
  private Long pkDocu;

  /** The prestacion en presdep. */
  private String prestacionEnPresdep;

  /** The ti nombre. */
  private String tiNombre;

  /** The estado nsisaad. */
  private String estadoNsisaad;

  /** The es ultima reso pia expediente. */
  private Long esUltimaResoPiaExpediente;

  /** The caducada. */
  private Boolean caducada;

  /** The grado. */
  private String grado;


  /**
   * Instantiates a new resolucion listado DTO.
   */
  public ResolucionListadoDTO() {
    super();
  }


  /**
   * Instantiates a new resolucion listado DTO.
   *
   * @param pkResolucion the pk resolucion
   * @param codigoSolicitud the codigo solicitud
   * @param tipoResolucion the tipo resolucion
   * @param fechaResolucion the fecha resolucion
   * @param fechaEfecto the fecha efecto
   * @param fechaNotificacion the fecha notificacion
   * @param fechaBaja the fecha baja
   * @param usuario the usuario
   * @param motivo the motivo
   * @param fechaRecurso the fecha recurso
   * @param revocada the revocada
   * @param fechaPublicacionBoe the fecha publicacion boe
   * @param lote the lote
   * @param estadoLote the estado lote
   * @param notasMigracion the notas migracion
   * @param estadoNsisaad the estado NSISAAD
   * @param grado the grado
   */
  public ResolucionListadoDTO(final Long pkResolucion,
      final String codigoSolicitud, final String tipoResolucion,
      final Date fechaResolucion, final Date fechaEfecto,
      final Date fechaNotificacion, final Date fechaBaja, final String usuario,
      final String motivo, final Date fechaRecurso, final Boolean revocada,
      final Date fechaPublicacionBoe, final Integer lote,
      final String estadoLote, final String notasMigracion,
      final String estadoNsisaad, final String grado) {
    super();
    this.pkResolucion = pkResolucion;
    this.codigoSolicitud = codigoSolicitud;
    this.tipoResolucion = tipoResolucion;
    this.fechaResolucion = UtilidadesCommons.cloneDate(fechaResolucion);
    this.fechaEfecto = UtilidadesCommons.cloneDate(fechaEfecto);
    this.fechaNotificacion = UtilidadesCommons.cloneDate(fechaNotificacion);
    this.fechaBaja = UtilidadesCommons.cloneDate(fechaBaja);
    this.usuario = usuario;
    this.motivo = motivo;
    this.fechaRecurso = UtilidadesCommons.cloneDate(fechaRecurso);
    this.revocada = revocada;
    this.fechaPublicacionBoe = UtilidadesCommons.cloneDate(fechaPublicacionBoe);
    this.lote = lote;
    this.estadoLote = estadoLote;
    this.notasMigracion = notasMigracion;
    this.estadoNsisaad = estadoNsisaad;
    this.grado = grado;
  }


  /**
   * Gets the pk resolucion.
   *
   * @return the pkResolucion
   */
  public Long getPkResolucion() {
    return pkResolucion;
  }

  /**
   * Sets the pk resolucion.
   *
   * @param pkResolucion the pkResolucion to set
   */
  public void setPkResolucion(final Long pkResolucion) {
    this.pkResolucion = pkResolucion;
  }

  /**
   * Gets the pk expedien.
   *
   * @return the pkExpedien
   */
  public Long getPkExpedien() {
    return pkExpedien;
  }

  /**
   * Sets the pk expedien.
   *
   * @param pkExpedien the pkExpedien to set
   */
  public void setPkExpedien(final Long pkExpedien) {
    this.pkExpedien = pkExpedien;
  }

  /**
   * Gets the pk solicitud.
   *
   * @return the pkSolicitud
   */
  public Long getPkSolicitud() {
    return pkSolicitud;
  }

  /**
   * Sets the pk solicitud.
   *
   * @param pkSolicitud the pkSolicitud to set
   */
  public void setPkSolicitud(final Long pkSolicitud) {
    this.pkSolicitud = pkSolicitud;
  }

  /**
   * Gets the codigo solicitud.
   *
   * @return the codigoSolicitud
   */
  public String getCodigoSolicitud() {
    return codigoSolicitud;
  }

  /**
   * Sets the codigo solicitud.
   *
   * @param codigoSolicitud the codigoSolicitud to set
   */
  public void setCodigoSolicitud(final String codigoSolicitud) {
    this.codigoSolicitud = codigoSolicitud;
  }

  /**
   * Gets the tipo resolucion.
   *
   * @return the tipoResolucion
   */
  public String getTipoResolucion() {
    return tipoResolucion;
  }

  /**
   * Sets the tipo resolucion.
   *
   * @param tipoResolucion the tipoResolucion to set
   */
  public void setTipoResolucion(final String tipoResolucion) {
    this.tipoResolucion = tipoResolucion;
  }

  /**
   * Gets the fecha resolucion.
   *
   * @return the fechaResolucion
   */
  public Date getFechaResolucion() {
    return UtilidadesCommons.cloneDate(fechaResolucion);
  }

  /**
   * Sets the fecha resolucion.
   *
   * @param fechaResolucion the fechaResolucion to set
   */
  public void setFechaResolucion(final Date fechaResolucion) {
    this.fechaResolucion = UtilidadesCommons.cloneDate(fechaResolucion);
  }

  /**
   * Gets the fecha efecto.
   *
   * @return the fechaEfecto
   */
  public Date getFechaEfecto() {
    return UtilidadesCommons.cloneDate(fechaEfecto);
  }

  /**
   * Sets the fecha efecto.
   *
   * @param fechaEfecto the fechaEfecto to set
   */
  public void setFechaEfecto(final Date fechaEfecto) {
    this.fechaEfecto = UtilidadesCommons.cloneDate(fechaEfecto);
  }

  /**
   * Gets the fecha notificacion.
   *
   * @return the fechaNotificacion
   */
  public Date getFechaNotificacion() {
    return UtilidadesCommons.cloneDate(fechaNotificacion);
  }

  /**
   * Sets the fecha notificacion.
   *
   * @param fechaNotificacion the fechaNotificacion to set
   */
  public void setFechaNotificacion(final Date fechaNotificacion) {
    this.fechaNotificacion = UtilidadesCommons.cloneDate(fechaNotificacion);
  }

  /**
   * Gets the fecha firma.
   *
   * @return the fecha firma
   */
  public Date getFechaFirma() {
    return UtilidadesCommons.cloneDate(fechaFirma);
  }

  /**
   * Sets the fecha firma.
   *
   * @param fechaFirma the new fecha firma
   */
  public void setFechaFirma(final Date fechaFirma) {
    this.fechaFirma = UtilidadesCommons.cloneDate(fechaFirma);
  }

  /**
   * Gets the fecha baja.
   *
   * @return the fechaBaja
   */
  public Date getFechaBaja() {
    return UtilidadesCommons.cloneDate(fechaBaja);
  }

  /**
   * Sets the fecha baja.
   *
   * @param fechaBaja the fechaBaja to set
   */
  public void setFechaBaja(final Date fechaBaja) {
    this.fechaBaja = UtilidadesCommons.cloneDate(fechaBaja);
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public String getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the usuario to set
   */
  public void setUsuario(final String usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the motivo.
   *
   * @return the motivo
   */
  public String getMotivo() {
    return motivo;
  }

  /**
   * Sets the motivo.
   *
   * @param motivo the motivo to set
   */
  public void setMotivo(final String motivo) {
    this.motivo = motivo;
  }

  /**
   * Gets the fecha recurso.
   *
   * @return the fechaRecurso
   */
  public Date getFechaRecurso() {
    return UtilidadesCommons.cloneDate(fechaRecurso);
  }

  /**
   * Sets the fecha recurso.
   *
   * @param fechaRecurso the fechaRecurso to set
   */
  public void setFechaRecurso(final Date fechaRecurso) {
    this.fechaRecurso = UtilidadesCommons.cloneDate(fechaRecurso);
  }

  /**
   * Gets the revocada.
   *
   * @return the revocada
   */
  public Boolean getRevocada() {
    return revocada;
  }

  /**
   * Sets the revocada.
   *
   * @param revocada the revocada to set
   */
  public void setRevocada(final Boolean revocada) {
    this.revocada = revocada;
  }

  /**
   * Gets the fecha publicacion boe.
   *
   * @return the fechaPublicacionBoe
   */
  public Date getFechaPublicacionBoe() {
    return UtilidadesCommons.cloneDate(fechaPublicacionBoe);
  }

  /**
   * Sets the fecha publicacion boe.
   *
   * @param fechaPublicacionBoe the fechaPublicacionBoe to set
   */
  public void setFechaPublicacionBoe(final Date fechaPublicacionBoe) {
    this.fechaPublicacionBoe = UtilidadesCommons.cloneDate(fechaPublicacionBoe);
  }

  /**
   * Gets the lote.
   *
   * @return the lote
   */
  public Integer getLote() {
    return lote;
  }

  /**
   * Sets the lote.
   *
   * @param lote the lote to set
   */
  public void setLote(final Integer lote) {
    this.lote = lote;
  }

  /**
   * Gets the estado lote.
   *
   * @return the estadoLote
   */
  public String getEstadoLote() {
    return estadoLote;
  }

  /**
   * Sets the estado lote.
   *
   * @param estadoLote the estadoLote to set
   */
  public void setEstadoLote(final String estadoLote) {
    this.estadoLote = estadoLote;
  }

  /**
   * Gets the notas migracion.
   *
   * @return the notasMigracion
   */
  public String getNotasMigracion() {
    return notasMigracion;
  }

  /**
   * Sets the notas migracion.
   *
   * @param notasMigracion the notasMigracion to set
   */
  public void setNotasMigracion(final String notasMigracion) {
    this.notasMigracion = notasMigracion;
  }

  /**
   * Compare to.
   *
   * @param o the o
   * @return the int
   */
  @Override
  public int compareTo(final ResolucionListadoDTO o) {
    return 0;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }


  /**
   * Gets the pk pia.
   *
   * @return the pk pia
   */
  public Long getPkPia() {
    return pkPia;
  }


  /**
   * Sets the pk pia.
   *
   * @param pkPia the new pk pia
   */
  public void setPkPia(final Long pkPia) {
    this.pkPia = pkPia;
  }


  /**
   * Gets the pk retro pia.
   *
   * @return the pk retro pia
   */
  public Long getPkRetroPia() {
    return pkRetroPia;
  }


  /**
   * Sets the pk retro pia.
   *
   * @param pkRetroPia the new pk retro pia
   */
  public void setPkRetroPia(final Long pkRetroPia) {
    this.pkRetroPia = pkRetroPia;
  }

  /**
   * Gets the pk tipodocu.
   *
   * @return the pkTipodocu
   */
  public Long getPkTipodocu() {
    return pkTipodocu;
  }

  /**
   * Sets the pk tipodocu.
   *
   * @param pkTipodocu the pkTipodocu to set
   */
  public void setPkTipodocu(final Long pkTipodocu) {
    this.pkTipodocu = pkTipodocu;
  }

  /**
   * Gets the es ultima reso pia expediente.
   *
   * @return the esUltimaResoPiaExpediente
   */
  public Long getEsUltimaResoPiaExpediente() {
    return esUltimaResoPiaExpediente;
  }

  /**
   * Sets the es ultima reso pia expediente.
   *
   * @param esUltimaResoPiaExpediente the esUltimaResoPiaExpediente to set
   */
  public void setEsUltimaResoPiaExpediente(
      final Long esUltimaResoPiaExpediente) {
    this.esUltimaResoPiaExpediente = esUltimaResoPiaExpediente;
  }

  /**
   * Gets the pia en listado fisc.
   *
   * @return the piaEnListadoFisc
   */
  public String getPiaEnListadoFisc() {
    return piaEnListadoFisc;
  }

  /**
   * Sets the pia en listado fisc.
   *
   * @param piaEnListadoFisc the piaEnListadoFisc to set
   */
  public void setPiaEnListadoFisc(final String piaEnListadoFisc) {
    this.piaEnListadoFisc = piaEnListadoFisc;
  }

  /**
   * Gets the retro en listado fisc.
   *
   * @return the retroEnListadoFisc
   */
  public String getRetroEnListadoFisc() {
    return retroEnListadoFisc;
  }

  /**
   * Sets the retro en listado fisc.
   *
   * @param retroEnListadoFisc the retroEnListadoFisc to set
   */
  public void setRetroEnListadoFisc(final String retroEnListadoFisc) {
    this.retroEnListadoFisc = retroEnListadoFisc;
  }

  /**
   * Gets the do nombre.
   *
   * @return the doNombre
   */
  public String getDoNombre() {
    return doNombre;
  }

  /**
   * Sets the do nombre.
   *
   * @param doNombre the doNombre to set
   */
  public void setDoNombre(final String doNombre) {
    this.doNombre = doNombre;
  }

  /**
   * Gets the prestacion en presdep.
   *
   * @return the prestacionEnPresdep
   */
  public String getPrestacionEnPresdep() {
    return prestacionEnPresdep;
  }

  /**
   * Sets the prestacion en presdep.
   *
   * @param prestacionEnPresdep the prestacionEnPresdep to set
   */
  public void setPrestacionEnPresdep(final String prestacionEnPresdep) {
    this.prestacionEnPresdep = prestacionEnPresdep;
  }

  /**
   * Gets the ti nombre.
   *
   * @return the tiNombre
   */
  public String getTiNombre() {
    return tiNombre;
  }

  /**
   * Sets the ti nombre.
   *
   * @param tiNombre the tiNombre to set
   */
  public void setTiNombre(final String tiNombre) {
    this.tiNombre = tiNombre;
  }

  /**
   * Gets the estado nsisaad.
   *
   * @return the estadoNsisaad
   */
  public String getEstadoNsisaad() {
    return estadoNsisaad;
  }

  /**
   * Sets the estado nsisaad.
   *
   * @param estadoNsisaad the estadoNsisaad to set
   */
  public void setEstadoNsisaad(final String estadoNsisaad) {
    this.estadoNsisaad = estadoNsisaad;
  }

  /**
   * Gets the pk docu.
   *
   * @return the pkDocu
   */
  public Long getPkDocu() {
    return pkDocu;
  }


  /**
   * Sets the pk docu.
   *
   * @param pkDocu the pkDocu to set
   */
  public void setPkDocu(final Long pkDocu) {
    this.pkDocu = pkDocu;
  }


  /**
   * Gets the pk resolucion caducada.
   *
   * @return the pkResolucionCaducada
   */
  public Long getPkResolucionCaducada() {
    return pkResolucionCaducada;
  }


  /**
   * Sets the pk resolucion caducada.
   *
   * @param pkResolucionCaducada the pkResolucionCaducada to set
   */
  public void setPkResolucionCaducada(final Long pkResolucionCaducada) {
    this.pkResolucionCaducada = pkResolucionCaducada;
  }


  /**
   * Gets the retipo.
   *
   * @return the retipo
   */
  public String getRetipo() {
    return retipo;
  }


  /**
   * Sets the retipo.
   *
   * @param retipo the retipo to set
   */
  public void setRetipo(final String retipo) {
    this.retipo = retipo;
  }


  /**
   * Gets the caducada.
   *
   * @return the caducada
   */
  public Boolean getCaducada() {
    return caducada;
  }


  /**
   * Sets the caducada.
   *
   * @param caducada the new caducada
   */
  public void setCaducada(final Boolean caducada) {
    this.caducada = caducada;
  }

  /**
   * Gets the grado.
   *
   * @return the grado
   */
  public String getGrado() {
    return grado;
  }

  /**
   * Sets the grado.
   *
   * @param grado the grado to set
   */
  public void setGrado(final String grado) {
    this.grado = grado;
  }

}
