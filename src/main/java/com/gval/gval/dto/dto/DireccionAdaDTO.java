package com.gval.gval.dto.dto;

import com.gval.gval.common.UtilidadesCommons;
import es.gva.dependencia.ada.common.enums.TipoDireccionEnum;
import es.gva.dependencia.ada.personas.dto.DireccionDTO;
import es.gva.dependencia.ada.personas.dto.DireccionIdDTO;
import es.gva.dependencia.ada.personas.dto.builder.DireccionIdDTOBuilder;
import es.gva.dependencia.ada.util.Utilidades;
import es.gva.dependencia.ada.validators.NotNullIfAnotherFieldHasValue;
import es.gva.dependencia.ada.vistas.dto.CentroDTO;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * The Class DireccionAdaDTO.
 */
@NotNullIfAnotherFieldHasValue(fieldName = "tipoDireccion",
    fieldValue = TipoDireccionEnum.TiposDireccion.DOMICILIO,
    dependFieldName = "numero", message = "error.direccion.numero.vacio")
@NotNullIfAnotherFieldHasValue(fieldName = "tipoDireccion",
    fieldValue = TipoDireccionEnum.TiposDireccion.DOMICILIO,
    dependFieldName = "codigoPostal",
    message = "error.direccion.codigopostal.vacio")
@NotNullIfAnotherFieldHasValue(fieldName = "tipoDireccion",
    fieldValue = TipoDireccionEnum.TiposDireccion.DOMICILIO,
    dependFieldName = "poblacion", message = "error.direccion.poblacion.vacio")
@NotNullIfAnotherFieldHasValue(fieldName = "tipoDireccion",
    fieldValue = TipoDireccionEnum.TiposDireccion.DOMICILIO,
    dependFieldName = "nombreVia", message = "error.direccion.nombrevia.vacio")
@NotNullIfAnotherFieldHasValue(fieldName = "tipoDireccion",
    fieldValue = TipoDireccionEnum.TiposDireccion.DOMICILIO,
    dependFieldName = "tipoVia", message = "error.direccion.tipovia.vacio")
@NotNullIfAnotherFieldHasValue(fieldName = "tipoDireccion",
    fieldValue = TipoDireccionEnum.TiposDireccion.RESIDENCIA,
    dependFieldName = "centro", message = "error.direccion.centro.vacio")
@NotNullIfAnotherFieldHasValue(fieldName = "tipoDireccion",
    fieldValue = TipoDireccionEnum.TiposDireccion.RESIDENCIA,
    dependFieldName = "codigoPostal",
    message = "error.direccion.codigopostal.vacio")
@NotNullIfAnotherFieldHasValue(fieldName = "tipoDireccion",
    fieldValue = TipoDireccionEnum.TiposDireccion.RESIDENCIA,
    dependFieldName = "poblacion", message = "error.direccion.poblacion.vacio")
@NotNullIfAnotherFieldHasValue(fieldName = "tipoDireccion",
    fieldValue = TipoDireccionEnum.TiposDireccion.EXTRANJERO,
    dependFieldName = "direccionExtranjera",
    message = "error.direccion.direccionextranjera.vacio")
public class DireccionAdaDTO extends DireccionDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 3632946504101818367L;

  /** The pk direccion. */
  private Long pkDireccion;

  /** The activo. */
  private Boolean activo;

  /** The centro servicios sociales. */
  private CentroServiciosSocialesDTO centroServiciosSociales;

  /** The direccion extranjera. */
  private String direccionExtranjera;

  /** The fecha creacion. */
  private Date fechaCreacion;

  /** The habitual. */
  private Boolean habitual;

  /** The habitual anterior. */
  private Boolean habitualAnterior;

  /** The notificacion. */
  private Boolean notificacion;

  /** The observaciones. */
  private String observaciones;

  /** The persona contacto. */
  private String personaContacto;

  /** The tipo contacto. */
  private String tipoContacto;

  /** The zaguan. */
  private String zaguan;

  /** The tipo direccion ada. */
  private String tipoDireccionAda;

  /** The persona ada DTO. */
  @JsonManagedReference
  @NotNull(message = "error.direccion.sin_persona")
  private PersonaAdaDTO personaAdaDTO;

  /** The codigo postal DTO. */
  private CodigoPostalDTO codigoPostalDTO;

  /** The poblacion. */
  private PoblacionDTO poblacion;

  /** The provincia. */
  private ProvinciaDTO provincia;

  /** The tipo via. */
  private TipoViaDTO tipoVia;

  /** The centro. */
  private CentroDTO centro;

  /** The unidades. */
  private List<UnidadDTO> unidades;

  /** The esOtraCCAA. */
  private Boolean esOtraCCAA;

  /** The piso. */
  private BigDecimal piso;

  /**
   * Instantiates a new direccion ada DTO.
   */
  public DireccionAdaDTO() {
    super();
  }

  /**
   * Instantiates a new direccion ada DTO.
   *
   * @param defaultData the default data
   */
  public DireccionAdaDTO(final boolean defaultData) {
    super();
    if (defaultData) {
      super.setId(new DireccionIdDTOBuilder().build());
      personaAdaDTO = new PersonaAdaDTO(true);
      super.setPersona(personaAdaDTO);
      activo = Boolean.TRUE;
      notificacion = Boolean.FALSE;
      habitual = Boolean.FALSE;
      habitualAnterior = Boolean.FALSE;
      esOtraCCAA = Boolean.FALSE;
      fechaCreacion = new Date();
    }
  }

  /**
   * Gets the pk direccion.
   *
   * @return the pk direccion
   */
  public Long getPkDireccion() {
    return pkDireccion;
  }

  /**
   * Sets the pk direccion.
   *
   * @param pkDireccion the new pk direccion
   */
  public void setPkDireccion(final Long pkDireccion) {
    this.pkDireccion = pkDireccion;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the centro servicios sociales.
   *
   * @return the centro servicios sociales
   */
  public CentroServiciosSocialesDTO getCentroServiciosSociales() {
    return centroServiciosSociales;
  }

  /**
   * Sets the centro servicios sociales.
   *
   * @param centroServiciosSociales the new centro servicios sociales
   */
  public void setCentroServiciosSociales(
      final CentroServiciosSocialesDTO centroServiciosSociales) {
    this.centroServiciosSociales = centroServiciosSociales;
  }

  /**
   * Gets the direccion extranjera.
   *
   * @return the direccion extranjera
   */
  public String getDireccionExtranjera() {
    return direccionExtranjera;
  }

  /**
   * Sets the direccion extranjera.
   *
   * @param direccionExtranjera the new direccion extranjera
   */
  public void setDireccionExtranjera(final String direccionExtranjera) {
    this.direccionExtranjera = direccionExtranjera;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha creacion formateada.
   *
   * @return the fecha creacion formateada
   */
  public String getFechaCreacionFormateada() {
    return fechaCreacion == null ? ""
        : new SimpleDateFormat(Utilidades.FORMAT_DATE_FECHA)
            .format(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the habitual.
   *
   * @return the habitual
   */
  public Boolean getHabitual() {
    return habitual;
  }

  /**
   * Sets the habitual.
   *
   * @param habitual the new habitual
   */
  public void setHabitual(final Boolean habitual) {
    this.habitual = habitual;
  }

  /**
   * Gets the habitual anterior.
   *
   * @return the habitual anterior
   */
  public Boolean getHabitualAnterior() {
    return habitualAnterior;
  }

  /**
   * Sets the habitual anterior.
   *
   * @param habitualAnterior the new habitual anterior
   */
  public void setHabitualAnterior(final Boolean habitualAnterior) {
    this.habitualAnterior = habitualAnterior;
  }

  /**
   * Gets the notificacion.
   *
   * @return the notificacion
   */
  public Boolean getNotificacion() {
    return notificacion;
  }

  /**
   * Sets the notificacion.
   *
   * @param notificacion the new notificacion
   */
  public void setNotificacion(final Boolean notificacion) {
    this.notificacion = notificacion;
  }

  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return observaciones;
  }

  /**
   * Sets the observaciones.
   *
   * @param observaciones the new observaciones
   */
  public void setObservaciones(final String observaciones) {
    this.observaciones = observaciones;
  }

  /**
   * Gets the persona contacto.
   *
   * @return the persona contacto
   */
  public String getPersonaContacto() {
    return personaContacto;
  }

  /**
   * Sets the persona contacto.
   *
   * @param personaContacto the new persona contacto
   */
  public void setPersonaContacto(final String personaContacto) {
    this.personaContacto = personaContacto;
  }

  /**
   * Gets the tipo contacto.
   *
   * @return the tipo contacto
   */
  public String getTipoContacto() {
    return tipoContacto;
  }

  /**
   * Sets the tipo contacto.
   *
   * @param tipoContacto the new tipo contacto
   */
  public void setTipoContacto(final String tipoContacto) {
    this.tipoContacto = tipoContacto;
  }

  /**
   * Gets the zaguan.
   *
   * @return the zaguan
   */
  public String getZaguan() {
    return zaguan;
  }

  /**
   * Sets the zaguan.
   *
   * @param zaguan the new zaguan
   */
  public void setZaguan(final String zaguan) {
    this.zaguan = zaguan;
  }

  /**
   * Gets the centro.
   *
   * @return the centro
   */
  public CentroDTO getCentro() {
    return centro;
  }

  /**
   * Sets the centro.
   *
   * @param centro the new centro
   */
  public void setCentro(final CentroDTO centro) {
    this.centro = centro;
  }

  /**
   * Gets the persona ada DTO.
   *
   * @return the persona ada DTO
   */
  public PersonaAdaDTO getPersonaAdaDTO() {
    return personaAdaDTO;
  }

  /**
   * Sets the persona ada DTO.
   *
   * @param personaAdaDTO the new persona ada DTO
   */
  public void setPersonaAdaDTO(final PersonaAdaDTO personaAdaDTO) {
    this.personaAdaDTO = personaAdaDTO;
  }

  /**
   * Gets the codigo postal DTO.
   *
   * @return the codigo postal DTO
   */
  public CodigoPostalDTO getCodigoPostalDTO() {
    return codigoPostalDTO;
  }

  /**
   * Sets the codigo postal DTO.
   *
   * @param codigoPostalDTO the new codigo postal DTO
   */
  public void setCodigoPostalDTO(final CodigoPostalDTO codigoPostalDTO) {
    this.codigoPostalDTO = codigoPostalDTO;
  }

  /**
   * Gets the poblacion.
   *
   * @return the poblacion
   */
  public PoblacionDTO getPoblacion() {
    return poblacion;
  }

  /**
   * Sets the poblacion.
   *
   * @param poblacion the new poblacion
   */
  public void setPoblacion(final PoblacionDTO poblacion) {
    this.poblacion = poblacion;
  }

  /**
   * Gets the provincia.
   *
   * @return the provincia
   */
  public ProvinciaDTO getProvincia() {
    return provincia;
  }

  /**
   * Sets the provincia.
   *
   * @param provincia the new provincia
   */
  public void setProvincia(final ProvinciaDTO provincia) {
    this.provincia = provincia;
  }

  /**
   * Gets the tipo via.
   *
   * @return the tipo via
   */
  public TipoViaDTO getTipoVia() {
    return tipoVia;
  }

  /**
   * Sets the tipo via.
   *
   * @param tipoVia the new tipo via
   */
  public void setTipoVia(final TipoViaDTO tipoVia) {
    this.tipoVia = tipoVia;
  }

  /**
   * Gets the unidades.
   *
   * @return the unidades
   */
  public List<UnidadDTO> getUnidades() {
    return UtilidadesCommons.collectorsToList(unidades);
  }

  /**
   * Sets the unidades.
   *
   * @param unidades the new unidades
   */
  public void setUnidades(final List<UnidadDTO> unidades) {
    this.unidades = UtilidadesCommons.collectorsToList(unidades);
  }

  /**
   * Gets the tipo direccion ada.
   *
   * @return the tipo direccion ada
   */
  public String getTipoDireccionAda() {
    return tipoDireccionAda;
  }

  /**
   * Sets the tipo direccion ada.
   *
   * @param tipoDireccionAda the new tipo direccion ada
   */
  public void setTipoDireccionAda(final String tipoDireccionAda) {
    this.tipoDireccionAda = tipoDireccionAda;
  }

  /**
   * Gets the nombre direccion.
   *
   * @return the nombre direccion
   */
  public String getNombreDireccion() {
    return (centro == null) ? this.getNombreVia() : centro.getNombre();
  }

  /**
   * Gets the es otra CCAA.
   *
   * @return the es otra CCAA
   */
  public Boolean getEsOtraCCAA() {
    return esOtraCCAA;
  }

  /**
   * Sets the es otra CCAA.
   *
   * @param esOtraCCAA the new es otra CCAA
   */
  public void setEsOtraCCAA(final Boolean esOtraCCAA) {
    this.esOtraCCAA = esOtraCCAA;
  }

  /**
   * Gets the piso.
   *
   * @return the piso
   */
  public BigDecimal getPiso() {
    return piso;
  }

  /**
   * Sets the piso.
   *
   * @param piso the new piso
   */
  public void setPiso(BigDecimal piso) {
    this.piso = piso;
  }

  /**
   * Sets the relacion persona.
   *
   * @param persona the new relacion persona
   */
  public void crearRelacionPersona(final PersonaAdaDTO persona) {
    if (this.getId() == null) {
      this.setId(new DireccionIdDTO());
    }
    // se relaciona el id
    getId().setIdPersona(persona.getId().getId());
    getId().setInddupPersona(persona.getId().getInddup());
    getId().setNifPersona(persona.getId().getNif());

    // se relaciona el objeto persona de la api y de ada
    this.setPersona(persona);
    this.setPersonaAdaDTO(persona);
  }
}
