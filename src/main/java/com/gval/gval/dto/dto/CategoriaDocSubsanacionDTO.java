package com.gval.gval.dto.dto;



import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.util.BaseDTO;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;

/**
 * The Class CategoriaDocSubsanacionDTO.
 */
public class CategoriaDocSubsanacionDTO extends BaseDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 7770928001874629131L;

  /** The pk categoria doc sub. */
  private Long pkCategoriaDocSub;

  /** The fecha creado. */
  private Date fechaCreado;

  /** The categoria. */
  private String categoria;

  /** The relativo. */
  private Boolean relativo;

  /** The activo. */
  private Boolean activo;

  /** The tipo documento. */
  private TipoDocumentoDTO tipoDocumento;

  /**
   * Instantiates a new categoria doc subsanacion DTO.
   */
  public CategoriaDocSubsanacionDTO() {
    super();
  }

  /**
   * Instantiates a new categoria doc subsanacion DTO.
   *
   * @param pkCategoriaDocSub the pk categoria doc sub
   * @param fechaCreado the fecha creado
   * @param categoria the categoria
   * @param relativo the relativo
   * @param activo the activo
   * @param tipoDocumento the tipo documento
   */
  public CategoriaDocSubsanacionDTO(final Long pkCategoriaDocSub,
      final Date fechaCreado, final String categoria, final Boolean relativo,
      final Boolean activo, final TipoDocumentoDTO tipoDocumento) {
    super();
    this.pkCategoriaDocSub = pkCategoriaDocSub;
    this.fechaCreado = UtilidadesCommons.cloneDate(fechaCreado);
    this.categoria = categoria;
    this.relativo = relativo;
    this.activo = activo;
    this.tipoDocumento = tipoDocumento;
  }

  /**
   * Gets the pk categoria doc sub.
   *
   * @return the pk categoria doc sub
   */
  public Long getPkCategoriaDocSub() {
    return pkCategoriaDocSub;
  }

  /**
   * Sets the pk categoria doc sub.
   *
   * @param pkCategoriaDocSub the new pk categoria doc sub
   */
  public void setPkCategoriaDocSub(final Long pkCategoriaDocSub) {
    this.pkCategoriaDocSub = pkCategoriaDocSub;
  }

  /**
   * Gets the fecha creado.
   *
   * @return the fecha creado
   */
  public Date getFechaCreado() {
    return UtilidadesCommons.cloneDate(fechaCreado);
  }

  /**
   * Sets the fecha creado.
   *
   * @param fechaCreado the new fecha creado
   */
  public void setFechaCreado(final Date fechaCreado) {
    this.fechaCreado = UtilidadesCommons.cloneDate(fechaCreado);
  }

  /**
   * Gets the categoria.
   *
   * @return the categoria
   */
  public String getCategoria() {
    return categoria;
  }

  /**
   * Sets the categoria.
   *
   * @param categoria the new categoria
   */
  public void setCategoria(final String categoria) {
    this.categoria = categoria;
  }

  /**
   * Gets the relativo.
   *
   * @return the relativo
   */
  public Boolean getRelativo() {
    return relativo;
  }

  /**
   * Sets the relativo.
   *
   * @param relativo the new relativo
   */
  public void setRelativo(final Boolean relativo) {
    this.relativo = relativo;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the tipo documento.
   *
   * @return the tipo documento
   */
  public TipoDocumentoDTO getTipoDocumento() {
    return tipoDocumento;
  }

  /**
   * Sets the tipo documento.
   *
   * @param tipoDocumento the new tipo documento
   */
  public void setTipoDocumento(final TipoDocumentoDTO tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}

