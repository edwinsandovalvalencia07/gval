package com.gval.gval.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioRequestDto {
    private String username;
    private String password;
    private Boolean active;
    private Integer idEmpleado;
}
