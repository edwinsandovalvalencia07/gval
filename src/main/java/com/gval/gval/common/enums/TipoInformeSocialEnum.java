package com.gval.gval.common.enums;

import java.util.Arrays;

/**
 * The Enum TipoInformeEnum.
 */
public enum TipoInformeSocialEnum {

  /** The informe entorno. */
  INFORME_ENTORNO(1L, "label.informe_social.tipo_informe_entorno"),
  /** The informe tecnico. */
  INFORME_TECNICO(2L, "label.informe_social.tipo_informe_tecnico");

  /** The valor. */
  private Long valor;

  /** The label. */
  private String label;

  /**
   * Instantiates a new situacion no activo enum.
   *
   * @param valor the valor
   * @param label the label
   */
  private TipoInformeSocialEnum(final Long valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public Long getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * From integer.
   *
   * @param valor the valor
   * @return the tipo informe enum
   */
  public static TipoInformeSocialEnum fromValor(final Long valor) {
    return Arrays.asList(TipoInformeSocialEnum.values()).stream()
        .filter(ti -> ti.valor.equals(valor)).findFirst().orElse(null);
  }


  /**
   * Label from valor.
   *
   * @param valor the valor
   * @return the string
   */
  public static String labelFromValor(final Long valor) {
    final TipoInformeSocialEnum e = fromValor(valor);
    return e == null ? null : e.label;
  }
}
