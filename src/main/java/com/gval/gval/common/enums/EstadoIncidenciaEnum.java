package com.gval.gval.common.enums;


import com.gval.gval.common.enums.listados.InterfazListadoLabels;

import java.util.Arrays;

/**
 * The Enum EstadoIncidenciaEnum.
 */
public enum EstadoIncidenciaEnum implements InterfazListadoLabels<String> {

  /** The nueva. */
  // Estado Incidencia Nueva
  NUEVA("N", "label.incidencia.estado_N"),

  /** The en curso. */
  // Estado Incidencia En Curso
  EN_CURSO("I", "label.incidencia.estado_I"),

  /** The cerrada. */
  // Estado Incidencia Cerrada
  CERRADA("C", "label.incidencia.estado_C");

  /** The valor. */
  private String valor;

  /** The label. */
  private String label;

  /**
   * Constructor. *
   *
   * @param valor the valor
   * @param label the label
   */
  private EstadoIncidenciaEnum(final String valor, final String label) {
    this.valor = valor;
    this.label = label;
  }



  /**
   * From string.
   *
   * @param text the text
   * @return the estado incidencia enum
   */
  public static EstadoIncidenciaEnum fromString(final String text) {
    return Arrays.asList(EstadoIncidenciaEnum.values()).stream()
        .filter(opsc -> opsc.valor.equals(text)).findFirst().orElse(null);
  }

  /**
   * Convierte enum a String.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return valor;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  @Override
  public String getValor() {
    return valor;
  }



  /**
   * Gets the label.
   *
   * @return the label
   */
  @Override
  public String getLabel() {
    return label;
  }



}
