package com.gval.gval.common.enums;

import com.gval.gval.common.enums.listados.InterfazListadoLabels;

import java.util.Arrays;

/**
 * The Enum EstadoIncidenciaEnum.
 */
public enum HorasExcepcionalesEnum implements InterfazListadoLabels<String> {

  /** The si. */
  SI("Si", "label.si"),

  /** The no. */
  NO("No", "label.no"),

  /** The vacio. */
  VACIO("", "");

  /** The valor. */
  private String valor;

  /** The label. */
  private String label;

  /**
   * Constructor. *
   *
   * @param i the valor
   * @param label the label
   */
  private HorasExcepcionalesEnum(final String i, final String label) {
    this.valor = i;
    this.label = label;
  }



  /**
   * From string.
   *
   * @param text the text
   * @return the estado incidencia enum
   */
  public static HorasExcepcionalesEnum fromString(final String text) {
    return Arrays.asList(HorasExcepcionalesEnum.values()).stream()
        .filter(opsc -> opsc.valor.equals(text)).findFirst().orElse(null);
  }

  /**
   * Convierte enum a String.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return valor;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  @Override
  public String getValor() {
    return valor;
  }



  /**
   * Gets the label.
   *
   * @return the label
   */
  @Override
  public String getLabel() {
    return label;
  }



}
