package com.gval.gval.common.enums;


/**
 * The Enum TipoPreferenciaInformeSocialEnum.
 */
public enum TipoPreferenciaInformeSocialEnum {


  /** The preferencia solicitante. */
  PREFERENCIA_SOLICITANTE(1L),

  /** The valoracion profesional. */
  VALORACION_PROFESIONAL(2L);

  /** The valor. */
  private Long valor;

  /**
   * Instantiates a new tipo preferencia informe social enum.
   *
   * @param valor the valor
   */
  private TipoPreferenciaInformeSocialEnum(final Long valor) {
    this.valor = valor;
  }


  /**
   * From valor.
   *
   * @param valorParam the valor param
   * @return the tipo preferencia informe social enum
   */
  public static TipoPreferenciaInformeSocialEnum fromValor(
      final Long valorParam) {
    TipoPreferenciaInformeSocialEnum tipo = null;
    for (final TipoPreferenciaInformeSocialEnum type : TipoPreferenciaInformeSocialEnum
        .values()) {
      if (type.getValor().intValue() == valorParam) {
        tipo = type;
        break;
      }
    }
    return tipo;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public Long getValor() {
    return valor;
  }

}
