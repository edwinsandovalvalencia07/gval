package com.gval.gval.common.enums;

/**
 * The Enum TipoOrigenEnum.
 */
public enum TipoOrigenEnum {

  /** The preferencia. */
  PREFERENCIA("P"),

  /** The valoracion. */
  VALORACION("V");

  /** The codigo. */
  private String codigo;


  /**
   * Instantiates a new tipo origen enum.
   *
   * @param codigo the codigo
   */
  TipoOrigenEnum(final String codigo) {
    this.codigo = codigo;
  }


  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }
}
