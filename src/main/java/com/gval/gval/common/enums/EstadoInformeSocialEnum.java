package com.gval.gval.common.enums;

import java.util.Arrays;

/**
 * The Enum EstadoInformeSocialEnum.
 */
public enum EstadoInformeSocialEnum {

  /** The pendiente. */
  PENDIENTE("P", "label.pendiente"),

  /** The en curso. */
  EN_CURSO("I", "label.en-curso"),

  /** The cerrado. */
  CERRADO("C", "label.cerrado");

  /** The valor. */
  private String valor;

  /** The label. */
  private String label;

  /**
   * Instantiates a new estado informe social enum.
   *
   * @param codigo the codigo
   * @param label the label
   */
  private EstadoInformeSocialEnum(final String valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }


  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }


  /**
   * Obtiene enumerado a partir de texto.
   *
   * @param codigo the codigo
   * @return the estado informe social enum
   */
  public static EstadoInformeSocialEnum fromValor(final String valor) {
    return Arrays.asList(EstadoInformeSocialEnum.values()).stream()
        .filter(tc -> tc.valor.equals(valor)).findFirst().orElse(null);
  }

  /**
   * Label from codigo.
   *
   * @param codigo the codigo
   * @return the string
   */
  public static String labelFromValor(final String valor) {
    final EstadoInformeSocialEnum e = fromValor(valor);
    return e == null ? null : e.label;
  }

}


