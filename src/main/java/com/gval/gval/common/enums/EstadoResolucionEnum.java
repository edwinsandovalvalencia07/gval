package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;


/**
 * The Enum EstadoResolucionEnum.
 */
public enum EstadoResolucionEnum {


  /** The pendiente. */
  // @formatter:off
  PENDIENTE("P", "label.estudio.comision.estado.reso_pendiente"),

  /** The emitida. */
  EMITIDA("E", "label.estudio.comision.estado.reso_emitida"),

  /** The notificada. */
  NOTIFICADA("N", "label.estudio.comision.estado.reso_notificada");
  // @formatter:on

  /** The value. */
  private final String value;

  /** The label. */
  private final String label;

  /**
   * Instantiates a new estado resolucion enum.
   *
   * @param value the value
   * @param label the label
   */
  private EstadoResolucionEnum(final String value, final String label) {
    this.value = value;
    this.label = label;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public String getValue() {
    return value;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return value;
  }


  /**
   * From string.
   *
   * @param text the text
   * @return the estado resolucion enum
   */
  @Nullable
  public static EstadoResolucionEnum fromString(final String text) {

    if (text == null) {
      return null;
    }

    for (final EstadoResolucionEnum estado : EstadoResolucionEnum.values()) {
      if (estado.value.equalsIgnoreCase(text)) {
        return estado;
      }
    }
    return null;
  }
}
