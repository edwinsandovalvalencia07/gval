package com.gval.gval.common.enums.listados;

import java.util.Arrays;

/**
 * The Enum TipoCuidador.
 */
public enum TipoCuidadorEnum implements InterfazListadoLabels<Long> {


  /** The primero. */
  PRIMERO(0L, "label.listado-enum.tipo-cuidador.primero"),


  /** The segundo. */
  SEGUNDO(1L, "label.listado-enum.tipo-cuidador.segundo");


  /** The valor. */
  private Long valor;

  /** The label. */
  private String label;



  /**
   * Instantiates a new tipo cuidador.
   *
   * @param valor the valor
   * @param label the label
   */
  TipoCuidadorEnum(final Long valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  @Override
  public Long getValor() {
    return valor;
  }



  /**
   * Gets the label.
   *
   * @return the label
   */
  @Override
  public String getLabel() {
    return label;
  }

  /**
   * From string.
   *
   * @param valor the valor
   * @return the tipo representacion enum
   */
  public static TipoCuidadorEnum fromValor(final Long valor) {
    return Arrays.asList(TipoCuidadorEnum.values()).stream()
        .filter(tc -> tc.valor.equals(valor)).findFirst().orElse(null);
  }

  /**
   * Label from valor.
   *
   * @param valor the valor
   * @return the string
   */
  public static String labelFromValor(final Long valor) {
    final TipoCuidadorEnum tipoCuidadorEnum = fromValor(valor);
    return tipoCuidadorEnum == null ? null : tipoCuidadorEnum.label;
  }
}
