package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;

/**
 * The Enum TipoTercero.
 */
public enum TipoCuentaBancaria {


  /** The nacional. */
  NACIONAL("NACIONAL", "label.form-cuentas-bancarias.nacional"),

  /** The extranjero. */
  IBAN("IBAN", "label.form-cuentas-bancarias.iban");


  /** The valor. */
  private String valor;

  /** The label. */
  private String label;

  /**
   * Instantiates a new tipo cuenta bancaria.
   *
   * @param valor the valor
   * @param label the label
   */
  TipoCuentaBancaria(final String valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }


  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }


  /**
   * From valor.
   *
   * @param value the value
   * @return the tipo tercero
   */
  @Nullable
  public static TipoCuentaBancaria fromValor(final String value) {
    for (final TipoCuentaBancaria e : TipoCuentaBancaria.values()) {
      if (e.valor.equals(value)) {
        return e;
      }
    }
    return null;
  }
}
