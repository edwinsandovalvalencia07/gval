package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;

/**
 * The Enum EstadoEstudioEnum.
 */
public enum EstadoEstudioEnum {


  // @formatter:off

  /** The pendiente. */
  PENDIENTE("P", "label.estudio.comision.estado.pendiente"),

  /** The encurso. */
  ENCURSO("I", "label.estudio.comision.estado.en_curso"),

  /** The cerrado. */
  CERRADO("C", "label.estudio.comision.estado.cerrado");
  // @formatter:on

  /** The value. */
  private final String value;

  /** The label. */
  private final String label;

  /**
   * Instantiates a new estado estudio enum.
   *
   * @param value the value
   * @param label the label
   */
  private EstadoEstudioEnum(final String value, final String label) {
    this.value = value;
    this.label = label;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public String getValue() {
    return value;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return value;
  }


  /**
   * From string.
   *
   * @param text the text
   * @return the tipo estudio enum
   */
  @Nullable
  public static EstadoEstudioEnum fromString(final String text) {

    if (text == null) {
      return null;
    }

    for (final EstadoEstudioEnum ee : EstadoEstudioEnum.values()) {
      if (ee.value.equalsIgnoreCase(text)) {
        return ee;
      }
    }
    return null;
  }

}
