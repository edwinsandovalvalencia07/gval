package com.gval.gval.common.enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import jakarta.annotation.Nullable;

/**
 * Enumerado para controlar el Tipo Identificacion.
 *
 * @author Indra
 */
public enum FiltroListado {

  /** The texto. */
  TEXTO("texto"),
  /** The texto avanzado. */
  TEXTO_AVANZADO("texto-avanzado"),
  /** The fecha. */
  FECHA("fecha"),
  /** The fecha avanzado. */
  FECHA_AVANZADO("fecha-avanzado"),
  /** The hora. */
  HORA("hora"),
  /** The desplegable. */
  DESPLEGABLE("desplegable"),
  /** The nombre completo. */
  NOMBRE_COMPLETO("nombre-completo"),
  /** The checkbox. */
  CHECKBOX("checkbox"),
  /** The imagen. */
  IMAGEN("imagen"),
  /** The catalogo. */
  CATALOGO("catalogo");

  /** The value. */
  public final String value;

  /**
   * Instantiates a new filtro listado.
   *
   * @param value the value
   */
  private FiltroListado(final String value) {
    this.value = value;
  }

  /**
   * Value of desc.
   *
   * @param value the value
   * @return the filtro listado
   */
  @Nullable
  public static FiltroListado valueOfDesc(final String value) {
    for (final FiltroListado e : values()) {
      if (e.value.equals(value)) {
        return e;
      }
    }
    return null;
  }

  /**
   * Convertir valores.
   *
   * @param value the value
   * @return true, if successful
   */
  public static boolean convertirValores(final String value) {
    final List<String> filtros = Arrays.asList(DESPLEGABLE.value, IMAGEN.value);
    return filtros.contains(value);
  }

  /**
   * Convertir valores excel.
   *
   * @param value the value
   * @return true, if successful
   */
  public static boolean convertirValoresExcel(final String value) {
    final List<String> filtros = Collections.singletonList(DESPLEGABLE.value);
    return filtros.contains(value);
  }
}
