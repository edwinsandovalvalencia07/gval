package com.gval.gval.common.enums;

import java.util.Arrays;

/**
 * The Enum TipoRepresentacionEnum.
 */
public enum TipoRepresentacionEnum {

  /** The representante legal. */
  // @formatter:off
  REPRESENTANTE_LEGAL("R", "label.representante.tipo.representante"),

  /** The tutelado administracion. */
  TUTELADO_ADMINISTRACION("T", "label.representante.tipo.tutelado"),

  /** The guardardor de hecho. */
  GUARDARDOR_DE_HECHO("G", "label.representante.tipo.guardador"),

  /** The curador. */
  CURADOR_ASISTENCIAL_PERSONA_FISICA("C", "label.representante.tipo.curador"),

  /** The Curador representativo Persona física. */
  CURADOR_PERSONA_FISICA("F", "label.representante.tipo.persona_fisica"),

  /** The Curador representativo Administración. */
  CURADOR_ADMINISTRACION("D", "label.representante.tipo.administracion"),

  /** The curador asistencial administracion. */
  CURADOR_ASISTENCIAL_ADMINISTRACION("L",
      "label.representante.tipo.asistencial.administracion"),

  /** The Guardador de hecho judicial. */
  GUARDADOR_JUDICIAL("K", "label.representante.tipo.guardador_judicial"),

  /** The Guardador de hecho notarial. */
  GUARDADOR_NOTARIAL("Q", "label.representante.tipo.guardador_notarial"),

  /** The Defensor judicial. */
  DEFENSOR_JUDICIAL_PERSONA_FISICA("J",
      "label.representante.tipo.defensor_persona_fisica"),

  /** The defensor judicial administracion. */
  DEFENSOR_JUDICIAL_ADMINISTRACION("E",
      "label.representante.tipo.defensor_judicial_administracion"),

  /** The Poder notarial general. */
  PODER_NOTARIAL("N", "label.representante.tipo.poder"),

  /** The Representante administrativo. */
  ADMINISTRATIVO("A", "label.representante.tipo.administrativo");
  // @formatter:on

  /** The value. */
  private String value;

  /** The label. */
  private String label;


  /**
   * Instantiates a new tipo representacion enum.
   *
   * @param value the value
   * @param label the label
   */
  private TipoRepresentacionEnum(final String value, final String label) {
    this.value = value;
    this.label = label;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public String getValue() {
    return value;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return value;
  }

  /**
   * From string.
   *
   * @param text the text
   * @return the tipo representacion enum
   */
  public static TipoRepresentacionEnum fromString(final String text) {
    return Arrays.asList(TipoRepresentacionEnum.values()).stream()
        .filter(tr -> tr.value.equalsIgnoreCase(text)).findFirst().orElse(null);
  }

}
