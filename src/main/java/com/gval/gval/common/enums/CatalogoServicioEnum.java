package com.gval.gval.common.enums;

import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import jakarta.annotation.Nullable;



/**
 * The Enum CatalogoServicioEnum.
 */
public enum CatalogoServicioEnum {
  // @formatter:off

  /** The teleasistencia. */
  TELEASISTENCIA("TA", false),

  /** The Servicio de ayuda a domicilio. */
  SERVICIO_ATENCION_DOMICILIO("AD", false),

  /** The Servicios centro de dia. */
  CENTRO_DIA("CD", false),

  /** The Servicio Atencion Residencial. */
  RESIDENCIA("AR", false),

  /** The PE Cuidados en el entorno familiar. */
  CNP("PC", false),

  /** The PE asistencia personal. */
  ASISTENCIA_PERSONAL("PA", false),

  /** The pvs sad. */
  PVS_SAD("PY", false),

  /** The PVS Centro de dia. */
  PVS_CENTRO_DIA("PD", false),

  /** The PVS Residencia. */
  PVS_RES("PR", false),

  /** The pvs pei tutelados. */
  PVS_PEI_TUTELADOS("PI", false),

  /** The pvs pei no tutelados. */
  PVS_PEI_NO_TUTELADOS("PH", false),

  /** The sp habilitacion terapia ocupacional. */
  SP_HABILITACION_TERAPIA_OCUPACIONAL("HT", true),

  /** The sp estimulacion temprana. */
  SP_ESTIMULACION_TEMPRANA("ET", true),

  /** The sp estimulacion cognitiva. */
  SP_ESTIMULACION_COGNITIVA("EC", true),

  /** The sp ceam. */
  SP_CEAM("AP", true),

  /** The SP Habilitacion Psicosocial(CRIS). */
  SP_CRIS("HP", true),

  /** The sp apoyos personales cuidados vivienda tuteladas. */
  SP_APOYOS_PERSONALES_CUIDADOS_VIVIENDA_TUTELADAS("VTS", true),

  /**Servicio de centro ocupacional **/
  SERVICIO_CENTRO_OCUPACIONAL("SCO",true),

  /**Servicio de atención residencial en Vivienda Tutelada Asistida**/
  SERVICIO_ATENCION_RESIDENCIAL_VIVIENDA_TUTELADA_ASISTIDA("VTA",true),

  /** The pvs tuteleados. */
  PVS_TUTELEADOS("PT", false),

  /** The sp otros servicios. */
  SP_OTROS_SERVICIOS("OT", true),

  /** The pvs garantia. */
  PVS_GARANTIA("PG", false),

  /** The vivienda tutelada. */
  VIVIENDA_TUTELADA("VI", false),

  /** The pvs sp asesor tecn apoyo. */
  PVS_SP_ASESOR_TECN_APOYO("AA", false),

  /** The pvs sp habilitacion desarrollo personal. */
  PVS_SP_HABILITACION_DESARROLLO_PERSONAL("HD", false),

  /** The pvs sp terapia ocupacional. */
  PVS_SP_TERAPIA_OCUPACIONAL("TO", false),

  /** The pvs sp atencion temprana. */
  PVS_SP_ATENCION_TEMPRANA("AT", false),

  /** The pvs sp estimulacion cognitiva. */
  PVS_SP_ESTIMULACION_COGNITIVA("AC", false),

  /** The pvs sp ata biopsico trastorno mental centro rehabilitacion. */
  PVS_SP_ATA_BIOPSICO_TRASTORNO_MENTAL_CENTRO_REHABILITACION("AB", false),

  /** The pvs sp promo mant recuperacion autonoma funcional. */
  PVS_SP_PROMO_MANT_RECUPERACION_AUTONOMA_FUNCIONAL("AF", false),

  /** The pvs sp apoyos personales cuidados vivienda tuteladas. */
  PVS_SP_APOYOS_PERSONALES_CUIDADOS_VIVIENDA_TUTELADAS("AV", false),

  /** The pvs promocion. */
  PVS_PROMOCION("PVP", false),

  /** The pvs sp hab prof centr ocup. */
  CENTRO_OCUPACIONAL("CO", false),

  PVS_PRO_HABILITACION   ("PHO" ,false),
  PVS_PRO_ATENCION  ("PAT" ,false) ,
  PVS_PRO_ESTIMULACION_COGNITIVA    ("PEC" ,false),
  PVS_PRO_MANT  ("PMR" ,false),
  PVS_PRO_HABILITACION_PSICOSOCIAL  ("PHP" ,false),
  PVS_PRO_APOYOS  ("PVT" ,false),
  ;




  /** The codigo. */
  private String codigo;

  /** The servicio promocion. */
  private boolean checkPromocionViejasPreferencias;

  /**
   * Instantiates a new pantalla enum.
   *
   * @param codigo the codigo
   * @param checkPromocionViejasPreferencias the servicio promocion
   */
  private CatalogoServicioEnum(final String codigo,
      final boolean checkPromocionViejasPreferencias) {
    this.codigo = codigo;
    this.checkPromocionViejasPreferencias = checkPromocionViejasPreferencias;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  // metodo que devuelve el nombre de la pantalla
  public String getCodigo() {
    return codigo;
  }


  /**
   * Checks if is check promocion viejas preferencias.
   *
   * @return true, if is check promocion viejas preferencias
   */
  public boolean isCheckPromocionViejasPreferencias() {
    return checkPromocionViejasPreferencias;
  }


  /**
   * From codigo.
   *
   * @param codigo the codigo
   * @return the pantalla enum
   */
  @Nullable
  public static CatalogoServicioEnum fromCodigo(final String codigo) {
    for (final CatalogoServicioEnum cs : values()) {
      if (cs.codigo.equals(codigo)) {
        return cs;
      }
    }
    return null;
  }

  /**
   * Obtener codigos servicios promocionales.
   *
   * @return the string
   */
  public static List<String> obtenerCodigosCheckPromocionViejasPreferencias() {
    return Arrays.asList(CatalogoServicioEnum.values()).stream()
        .filter(CatalogoServicioEnum::isCheckPromocionViejasPreferencias)
        .map(CatalogoServicioEnum::getCodigo).collect(Collectors.toList());
  }

  /**
   * Obtener codigos.
   *
   * @param enumsCatalogo the enums catalogo
   * @return the list
   */
  public static List<String> obtenerCodigos(
      final List<CatalogoServicioEnum> enumsCatalogo) {
    return CollectionUtils.emptyIfNull(enumsCatalogo).stream()
        .map(CatalogoServicioEnum::getCodigo).collect(Collectors.toList());
  }

  /**
   * Obtener catalogo servicio cambio tipologia.
   *
   * @param codigo the codigo
   * @return the catalogo servicio enum
   */
  @Nullable
  public static boolean esMismoTipoRecurso(
		  final String codigo, List<String> listaCodigoPrefAnterior) {

	  boolean mismoTipo = false;
	  
	  final CatalogoServicioEnum catalogo = fromCodigo(codigo);
	  if (catalogo == null) {
		  return false;
	  }
	  
	  switch (catalogo) {
	  //Tipos SERVICIO DE ATENCION RESIDENCIAL
	  case PVS_RES:
	  case RESIDENCIA: 
	  case SERVICIO_ATENCION_RESIDENCIAL_VIVIENDA_TUTELADA_ASISTIDA:
	  case SP_APOYOS_PERSONALES_CUIDADOS_VIVIENDA_TUTELADAS:
		  List<String> codigosAtenciónResidencial = new ArrayList<String>(Arrays.asList(
				  RESIDENCIA.getCodigo(),
				  PVS_RES.getCodigo(),
				  SERVICIO_ATENCION_RESIDENCIAL_VIVIENDA_TUTELADA_ASISTIDA.getCodigo(),
				  SP_APOYOS_PERSONALES_CUIDADOS_VIVIENDA_TUTELADAS.getCodigo()));
		  mismoTipo = listaCodigoPrefAnterior.stream().anyMatch(codigosAtenciónResidencial::contains);
		  break;
		  
	//Tipos CENTRO DE DIA
	  case CENTRO_DIA:
	  case PVS_CENTRO_DIA:
	  case SERVICIO_CENTRO_OCUPACIONAL:
		  List<String> codigosCentroDeDia = new ArrayList<String>(Arrays.asList(
				  CENTRO_DIA.getCodigo(),
				  PVS_CENTRO_DIA.getCodigo(),
				  SERVICIO_CENTRO_OCUPACIONAL.getCodigo()));
		  mismoTipo = listaCodigoPrefAnterior.stream().anyMatch(codigosCentroDeDia::contains);
		  break;
		  
	//Tipos ATENCION DOMICILIO
	  case PVS_SAD:
	  case SERVICIO_ATENCION_DOMICILIO:
		  List<String> codigosAtencionDomicilio = new ArrayList<String>(Arrays.asList(
				  PVS_SAD.getCodigo(),
				  SERVICIO_ATENCION_DOMICILIO.getCodigo()));
		  mismoTipo = listaCodigoPrefAnterior.stream().anyMatch(codigosAtencionDomicilio::contains);
		  break;
		  
	  default:
		  break;
	  }
	  return mismoTipo;
  }
}
