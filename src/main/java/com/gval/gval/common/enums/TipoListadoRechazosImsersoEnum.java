package com.gval.gval.common.enums;

import java.util.Arrays;

/**
 * The Enum TipoListadoRechazosImsersoEnum.
 */
public enum TipoListadoRechazosImsersoEnum {

  /** The Listados Rechazos Imserso menor fecha grado. */
  LISTADOS_RECHAZOS_MENOR_FECHA_GRADO(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_RECHAZO_GRADO.getValor(),
      "LRMENGRA", "item_desplegable.rechazos_grado"),

  /** The Listados Rechazos Imserso menor solicitud. */
  LISTADOS_RECHAZOS_MENOR_SOLICITUD(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_RECHAZO_SOLICITUD.getValor(),
      "LRMENSOL", "item_desplegable.rechazos_solicitud"),

  /** The Listados Rechazos Imserso menor inicio prestacion. */
  LISTADOS_RECHAZOS_MENOR_PRESTACION(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_RECHAZO_PRESTACION.getValor(),
      "LRMENPRE", "item_desplegable.rechazos_prestaciones");


  /** The valor. */
  private String valor;

  /** The label. */
  private String label;

  /** The access role. */
  private String accessRole;

  /**
   * Instantiates a new ListadoRechazosImsersoEnum.
   *
   * @param accessRole the access role
   * @param valor the valor
   * @param label the label
   */
  private TipoListadoRechazosImsersoEnum(final String accessRole,
      final String valor, final String label) {
    this.accessRole = accessRole;
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * Gets the access role.
   *
   * @return the accessRole
   */
  public String getAccessRole() {
    return accessRole;
  }

  /**
   * From string.
   *
   * @param text the text
   * @return the tipo ListadoRechazosImserso listado enum
   */
  public static TipoListadoRechazosImsersoEnum fromString(final String text) {
    return Arrays.asList(TipoListadoRechazosImsersoEnum.values()).stream()
        .filter(opsc -> opsc.valor.equals(text)).findFirst().orElse(null);
  }
}
