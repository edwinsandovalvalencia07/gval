package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;

/**
 * The Enum TipoCatalogoServicioEnum.
 */
public enum TipoCatalogoServicioEnum {

  /** The teleasistencia. */
  TELEASISTENCIA("TA", 1L),
  /** The ayuda domicilio. */
  AYUDA_DOMICILIO("AD", 2L),
  /** The centro dia. */
  CENTRO_DIA("CD", 3L),
  /** The cnp. */
  CNP("PC", 7L),
  /** The pvs sad. */
  PVS_SAD("PY", 21L),
  /** The pvs centro dia. */
  PVS_CENTRO_DIA("PD", 22L),
  /** The pe cuidador. */
  PE_CUIDADOR("PC", 7L),
  /** The centro ocupacional. */
  CENTRO_OCUPACIONAL("CO", 171L),
  /** The pe asistencia personal. */
  PE_ASISTENCIA_PERSONAL("PA", 8L);


  /** The codigo. */
  private String codigo;

  /** The pk cataserv. */
  private Long pkCataserv;

  /**
   * Instantiates a new tipo catalogo servicio enum.
   *
   * @param codigo the codigo
   * @param pkCataserv the pk cataserv
   */
  TipoCatalogoServicioEnum(final String codigo, final Long pkCataserv) {
    this.codigo = codigo;
    this.pkCataserv = pkCataserv;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Gets the pk cataserv.
   *
   * @return the pk cataserv
   */
  public Long getPkCataserv() {
    return pkCataserv;
  }

  /**
   * From codigo.
   *
   * @param codigo the codigo
   * @return the tipo catalogo servicio enum
   */
  @Nullable
  public static TipoCatalogoServicioEnum fromCodigo(final String codigo) {

    if (codigo == null) {
      return null;
    }

    for (final TipoCatalogoServicioEnum td : TipoCatalogoServicioEnum
        .values()) {
      if (td.codigo.equalsIgnoreCase(codigo)) {
        return td;
      }
    }
    return null;
  }
}
