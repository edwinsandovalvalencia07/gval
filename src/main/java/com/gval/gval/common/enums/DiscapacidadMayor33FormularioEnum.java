package com.gval.gval.common.enums;

import com.gval.gval.common.UtilidadesCommons;
import jakarta.annotation.Nullable;

//import jakarta.annotation.Nullable;

/**
 * The Enum DiscapacidadMayor33FormularioEnum.
 */
public enum DiscapacidadMayor33FormularioEnum {

  /** The no cumplimentado. */
  NO_CUMPLIMENTADO(0, "label.no_cumplimentado"),

  /** The no. */
  NO(1, "label.no"),

  /** The si. */
  SI(2, "label.si");


  /** The value. */
  private Integer value;

  /** The label. */
  private String label;

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return this.label;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public Integer getValue() {
    return this.value;
  }

  /**
   * Instantiates a new discapacidad mayor 33 formulario enum.
   *
   * @param value the value
   * @param label the label
   */
  DiscapacidadMayor33FormularioEnum(final Integer value, final String label) {
    this.value = value;
    this.label = label;
  }

  /**
   * From codigo.
   *
   * @param valor the valor
   * @return the estado documento enum
   */
  @Nullable
  public static String getLabelFromValue(final Integer valor) {
    if (UtilidadesCommons.isNull(valor)) {
      return null;
    }
    for (final DiscapacidadMayor33FormularioEnum enumValue : DiscapacidadMayor33FormularioEnum
        .values()) {
      if (valor.equals(enumValue.getValue())) {
        return enumValue.getLabel();
      }
    }
    return null;
  }
}
