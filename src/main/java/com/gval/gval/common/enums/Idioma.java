package com.gval.gval.common.enums;

import java.util.Locale;

/**
 * Enumerado para controlar el idioma.
 *
 * @author Indra
 */
public enum Idioma {

  /** The castellano. */
  CASTELLANO(new Locale("es", "ES")),

  /** The valenciano. */
  VALENCIANO(new Locale("ca", "ES"));

  /** The value. */
  public final Locale value;

  /**
   * Instantiates a new idioma.
   *
   * @param value the value
   */
  private Idioma(final Locale value) {
    this.value = value;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public Locale getValue() {
    return value;
  }

  /**
   * Gets the language.
   *
   * @return the language
   */
  public String getLanguage() {
    return value.getLanguage();
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return value.toString();
  }
}
