package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;

/**
 * The Enum TipoTerceroPersona.
 */
public enum TipoIdentificadorTerceroEnum {

  /** The nif. */
  NIF(2L, 1L),

  /** The nie. */
  NIE(5L, 5L);


  /** The tipo identificador. */
  private Long tipoIdentificador;

  /** The tipo persona. */
  private Long tipoPersona;

  /**
   * Instantiates a new tipo tercero.
   *
   * @param tipoIdentificador the tipo identificador
   * @param tipoPersona the tipo persona
   */
  TipoIdentificadorTerceroEnum(final Long tipoIdentificador,
      final Long tipoPersona) {
    this.tipoIdentificador = tipoIdentificador;
    this.tipoPersona = tipoPersona;
  }

  /**
   * From valor.
   *
   * @param tipoIdentificador the tipo identificador
   * @return the tipo tercero
   */
  @Nullable
  public static Long tipoIdentificadorToTipoPersona(
      final Long tipoIdentificador) {
    for (final TipoIdentificadorTerceroEnum e : TipoIdentificadorTerceroEnum
        .values()) {
      if (e.tipoIdentificador.equals(tipoIdentificador)) {
        return e.tipoPersona;
      }
    }
    return null;
  }


  /**
   * Tipo identificador valido.
   *
   * @param tipoIdentificador the tipo identificador
   * @return the boolean
   */
  public static boolean isValid(final Long tipoIdentificador) {
    for (final TipoIdentificadorTerceroEnum e : TipoIdentificadorTerceroEnum
        .values()) {
      if (e.tipoIdentificador.equals(tipoIdentificador)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Gets the tipo identificador.
   *
   * @return the tipo identificador
   */
  public Long getTipoIdentificador() {
    return tipoIdentificador;
  }

  /**
   * Gets the tipo persona.
   *
   * @return the tipo persona
   */
  public Long getTipoPersona() {
    return tipoPersona;
  }

}
