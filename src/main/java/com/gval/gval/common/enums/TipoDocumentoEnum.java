package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;

// TODO: Auto-generated Javadoc
/**
 * The Enum TipoDocumentoEnum.
 */
public enum TipoDocumentoEnum {

  /** The aad. */
  AAD("AAD"),

  /** The acrrepleg. */
  ACRREPLEG("ACRREPLEG"),

  /** The actdehe. */
  // @formatter:off
  ACTDEHE("ACTDEHE"),

  /** The acuse. */
  ACUSE("ACUSE"),

  /** The ale pia. */
  ALE_PIA("ALE_PIA"),

  /** The cambio cuidador. */
  CAMBIO_CUIDADOR("CAMCUID"),

  /** The cambio asistente. */
  CAMBIO_ASISTENTE("CAMASIS"),

  /** The ccnf. */
  CCNF("CCNF"),

  /** The ccnp. */
  CCNP("CCNP"),

  /** The ceap. */
  CEAP("CEAP"),

  /** The cebareco. */
  CEBARECO("CEBARECO"),

  /** The cecnp. */
  CECNP("CECNP"),

  /** The cedef. */
  CEDEF("CEDEF"),

  /** The ceraeat. */
  CERAEAT("CERAEAT"),

  /** The cerempsol. */
  CEREMPSOL("CEREMPSOL"),

  /** The cerresleg. */
  CERRESLEG("CERRESLEG"),

  /** The cert exp cnp. */
  CERT_EXP_CNP("CERT_EXP_CNP"),

  /** The cert uf. */
  CERT_UF("CERT_UF"),

  /** The certempahis. */
  CERTEMPAHIS("CERTEMPAHIS"),

  /** The ceuvo. */
  CEUVO("CEUVO"),

  /** The cfap. */
  CFAP("CFAP"),

  /** The cfspro. */
  CFSPRO("CFSPRO"),

  /** The cnrds. */
  CNRDS("CNRDS"),

  /** The cobcom31. */
  COBCOM31("COBCOM31"),

  /** The declaracion responsable retro cnp. */
  DECLARACION_RESPONSABLE_RETRO_CNP("DRRCNP"),

  /** The comun. */
  COMUN(""),

  /** The comunicacion ingreso centro. */
  COMUNICACION_INGRESO_CENTRO("COM_ING_CENTR"),

  /** The comunicacion prestacion baja. */
  COMUNICACION_PRESTACION_BAJA("COM_BAJ_PIA"),

  /** The contrato facturas centro dia. */
  CONTRATO_FACTURAS_CENTRO_DIA("CFCDIA"),

  /** The contrato facturas residencia. */
  CONTRATO_FACTURAS_RESIDENCIA("CFRES"),

  /** The contrato facturas sad. */
  CONTRATO_FACTURAS_SAD("CFSAD"),

  /** The contrato empresa sad. */
  CONTRATO_EMPRESA_SAD("CONTRATOSAD"),

  /** The coplibfam. */
  COPLIBFAM("COPLIBFAM"),

  /** The decguahec. */
  DECGUAHEC("DECGUAHEC"),

  /** The declirpf. */
  DECLIRPF("DECLIRPF"),

  /** The dedehe. */
  DEDEHE("DEDEHE"),

  /** The dil. */
  DIL("DIL"),

  /** The diliconservauto. */
  DILICONSERVAUTO("DILICONSERVAUTO"),

  /** The diliimpsad. */
  DILIIMPSAD("DILIIMPSAD"),

  /** The diligencia pia sad. */
  DILIGENCIA_PIA_SAD("DILI_SAD"),

  /** The dniap. */
  DNIAP("DNIAP"),

  /** The dniguahe. */
  DNIGUAHE("DNIGUAHE"),

  /** The dnihere. */
  DNIHERE("DNIHERE"),

  /** The dnirep. */
  DNIREP("DNIREP"),

  /** The dniuf. */
  DNIUF("DNIUF"),

  /** The doc pet rev. */
  DOC_PET_REV("DOC_PET_REV"),

  /** The docdatap. */
  DOCDATAP("DOCDATAP"),

  /** The docfevida. */
  DOCFEVIDA("DOCFEVIDA"),

  /** The docpenext. */
  DOCPENEXT("DOCPENEXT"),

  /** The docpenmie. */
  DOCPENMIE("DOCPENMIE"),

  /** The fdnis. */
  FDNIS("FDNIS"),

  /** The fococaba. */
  FOCOCABA("FOCOCABA"),

  /** The fodni. */
  FODNI("FODNI"),

  /** The fotcermin1. */
  FOTCERMIN1("FOTCERMIN1"),

  /** The fotcomdec. */
  FOTCOMDEC("FOTCOMDEC"),

  /** The infentorno. */
  INFENTORNO("INFENTORNO"),

  /** The infsalud. */
  INFORME_SALUD("INFSALUD"),

  /** The informe salud extendido fisico. */
  INFORME_SALUD_EXTENDIDO_FISICO("INFSALEXTFIS"),

  /** The informe salud extendido mental. */
  INFORME_SALUD_EXTENDIDO_MENTAL("INFSALEXTMEN"),

  /** The informe social. */
  INFORME_SOCIAL("IS"),

  /** The informe social tecnico. */
  INFORME_SOCIAL_TECNICO("ITM"),

  /** The infsalmenamp. */
  INFSALMENAMP("INFSALMENAMP"),

  /** The infsalrev. */
  INFSALREV("INFSALREV"),

  /** The infsalrevextfi. */
  INFSALREVEXTFI("INFSALREVEXTFI"),

  /** The infsalrevextme. */
  INFSALREVEXTME("INFSALREVEXTME"),

  /** The inftecnico. */
  INFTECNICO("INFTECNICO"),

  /** The inftut. */
  INFTUT("INFTUT"),

  /** The inshefa. */
  INSHEFA("INSHEFA"),

  /** The is 5e. */
  IS_5E("IS_5E"),

  /** The just val. */
  JUST_VAL("JUST_VAL"),

  /** The manterher. */
  MANTERHER("MANTERHER"),

  /** The modelo domiciliacion bancaria. */
  MODELO_DOMICILIACION_BANCARIA("MDT"),

  /** The mos. */
  MOS("MOS"),

  /** The otros. */
  OTROS("OTROS"),

  /** The pengrainv. */
  PENGRAINV("PENGRAINV"),

  /** The pensinss. */
  PENSINSS("PENSINSS"),

  /** The peticion nuevas preferencias. */
  PETICION_NUEVAS_PREFERENCIAS("PNPRDSO"),


  /** The peticion responsabilidad patrimonial. */
  PETICION_RESPONSABILIDAD_PATRIMONIAL("PET_RESP_PATRI"),

  /** The prdso. */
  PRDSO("PRDSO"),

  /** The propuesta pia aceptada smad. */
  PROPUESTA_PIA_ACEPTADA_SMAD("PROP_PIA_A_SMAD"),

  /** The prvidaind. */
  PRVIDAIND("PRVIDAIND"),

  /** The qej rec. */
  QEJ_REC("QEJ_REC"),

  /** The rcsc. */
  RCSC("RCSC"),

  /** The recibo. */
  RECIBO("RECIBO"),

  /** The recurso alzada. */
  RECURSO_ALZADA("codrecurso"),

  /** The recurso contecioso administrativo. */
  RECURSO_CONTECIOSO_ADMINISTRATIVO("codcontencioso"),

  /** The renuncia desistimiento interesado. */
  RENUNCIA_DESISTIMIENTO_INTERESADO("ARRENVOL"),

  /** The req exp tras e. */
  REQ_EXP_TRAS_E("REQ_EXP_TRAS_E"),

  /** The res ra. */
  RES_RA("RES_RA"),

  /** The res tras cao. */
  RES_TRAS_CAO("RES_TRAS_CAO"),

  /** The rpsrs disca. */
  RPSRS_DISCA("RPSRS_DISCA"),

  /** The rpvsgcimp. */
  RPVSGCIMP("RPVSGCIMP"),

  /** The revision pia ingreso centro. */
  REVISION_PIA_INGRESO_CENTRO("REVPIA_ING_CENT"),

  /** The rrevpiapsd. */
  RREVPIAPSD("RREVPIAPSD"),

  /** The ruta valoracion. */
  RUTA_VALORACION("RUTVAL"),

  /** The senjudinc. */
  SENJUDINC("SENJUDINC"),

  /** The sete. */
  SETE("SETE"),

  /** The smanlef. */
  SMANLEF("SMANLEF"),

  /** The sol tras e. */
  SOL_TRAS_E("SOL_TRAS_E"),

  /** The sol tras s. */
  SOL_TRAS_S("SOL_TRAS_S"),

  /** The solicitud inscripcion censo cnp. */
  SOLICITUD_INSCRIPCION_CENSO_CNP("SOLI_CENSO_CNP"),

  /** The solicitud revision horas. */
  SOLICITUD_REVISION_HORAS("SRHSAD"),

  /** The solicitud tramite urgencia. */
  SOLICITUD_TRAMITE_URGENCIA("codurgenci"),

  /** The srp. */
  SRP("SRP"),

  /** The subsan grado. */
  SUBSAN_GRADO("SUBSAN_GRADO"),

  /** The subsan heredero. */
  SUBSAN_HEREDERO("SUBSAN_HEREDERO"),

  /** The subsan pia. */
  SUBSAN_PIA("SUBSAN_PIA"),

  /** The mdb devoluciones tesoreria. */
  SUBSAN_DEV_TES("SUBSAN_DEV_TES"),

  /** The testsol. */
  TESTSOL("TESTSOL"),

  /** The val manual. */
  VAL_MANUAL("VAL_MANUAL"),

  /** The valoracion bvd. */
  VALORACION_BVD("VAL_BVD_BOE"),
  
  /** The valoracion val bvd. */
  VALORACION_VAL_BVD("VAL_BVD"),

  /** The valoracion bvd sin observaciones. */
  VALORACION_BVD_SIN_OBSERVACIONES("VAL_BVD_BOE_SO"),

  /** The valoracion eve. */
  VALORACION_EVE("VAL_EVE_BOE"),

  /** The valoracion eve. */
  VALORACION_EVE_SIN_OBSERVACIONES("VAL_EVE_BOE_SO"),

  /** The notificacion traslado comunidad autonoma. */
  NOTIFICACION_TRASLADO_COMUNIDAD_AUTONOMA("NTCA"),
  /** The hoja calculo aumento minimo. */
  HOJA_CALCULO_AUMENTO_MINIMO("HCAUMIN"),
  /** The hoja calculo aumento maximo. */
  HOJA_CALCULO_AUMENTO_MAXIMO("HCAUMAX"),
  /** The hoja calculo revision cnp33. */
  HOJA_CALCULO_REVISION_CNP("HCPCPC"),

  /** The mdb devolucion tesoreria cuenta cancelada. */
  MDB_DEVOLUCION_TESORERIA_CUENTA_CANCELADA("DTC");

  // @formatter:on

  /** The codigo. */
  private final String codigo;

  /**
   * Instantiates a new tipo documento enum.
   *
   * @param codigo the codigo
   */
  TipoDocumentoEnum(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * From codigo.
   *
   * @param codigo the codigo
   * @return the tipo documento enum
   */
  @Nullable
  public static TipoDocumentoEnum fromCodigo(final String codigo) {

    if (codigo == null) {
      return null;
    }

    for (final TipoDocumentoEnum td : TipoDocumentoEnum.values()) {
      if (td.codigo.equalsIgnoreCase(codigo)) {
        return td;
      }
    }
    return null;
  }
}
