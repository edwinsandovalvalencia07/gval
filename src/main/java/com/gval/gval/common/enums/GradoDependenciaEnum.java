package com.gval.gval.common.enums;

import org.apache.commons.lang3.StringUtils;

import jakarta.annotation.Nullable;

/**
 * The Enum GradoDependenciaEnum.
 */
public enum GradoDependenciaEnum {

  /** The grado cero. */
  GRADO_0("0", 0L, 14L),

  /** The mayores. */
  GRADO_1("1", 15L, 29L),

  /** The segundo grado. */
  GRADO_2("2", 30L, 44L),

  /** The tercer grado. */
  GRADO_3("3", 45L, 99L);

  /** The codigo. */
  private String valor;

  /** The margen inferior. */
  private Long margenInferior;

  /** The margen superior. */
  private Long margenSuperior;

  /**
   * Instantiates a new grado dependencia enum.
   *
   * @param valor the valor
   * @param margenInferior the margen inferior
   * @param margenSuperior the margen superior
   */
  private GradoDependenciaEnum(final String valor, final Long margenInferior,
      final Long margenSuperior) {
    this.valor = valor;
    this.margenInferior = margenInferior;
    this.margenSuperior = margenSuperior;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getValor() {
    return valor;
  }

  /**
   * Gets the margen inferior.
   *
   * @return the margen inferior
   */
  public Long getMargenInferior() {
    return margenInferior;
  }

  /**
   * Gets the margen superior.
   *
   * @return the margen superior
   */
  public Long getMargenSuperior() {
    return margenSuperior;
  }

  /**
   * From string.
   *
   * @param text the text
   * @return the grado dependencia enum
   */
  @Nullable
  public static GradoDependenciaEnum fromString(final String text) {

    if (text == null) {
      return null;
    }

    for (final GradoDependenciaEnum td : GradoDependenciaEnum.values()) {
      if (td.valor.equalsIgnoreCase(text)) {
        return td;
      }
    }
    return null;
  }


  /**
   * Es dependiente.
   *
   * @param grado the grado
   * @return true, if successful
   */
  public static boolean esDependiente(final String grado) {
    if (StringUtils.isNumeric(grado)) {
      return Integer.parseInt(GRADO_0.getValor()) < Integer.parseInt(grado);
    }
    return false;
  }

}
