package com.gval.gval.common.enums;

import java.util.Arrays;



/**
 * The Enum TipoListadoEnumExpedientesPIA.
 */
public enum TipoExpedientesPIAListadoEnum {

  /** The expedientes pia. */
  EXPEDIENTES_PIA(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_EXPEDIENTES_PIA.getValor(),
      "LEXPPIA", "label.expedientes_pia.expedientes_pia"),

  /** The expedientes pias suscetibles IST seguimiento. */
  EXPEDIENTES_SUSCEPTIBLES_IST_SEGUIMIENTO(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_EXPEDIENTES_IST_SEGUIMIENTO
          .getValor(),
      "LSSEGUI",
      "label.expedientes_pia.expedientes_susceptibles_IST_seguimiento");


  /** The valor. */
  private String valor;

  /** The label. */
  private String label;

  /** The access role. */
  private String accessRole;

  /**
   * Instantiates a new expediente PIA.
   *
   * @param accessRole the access role
   * @param valor the valor
   * @param label the label
   */
  private TipoExpedientesPIAListadoEnum(final String accessRole,
      final String valor, final String label) {
    this.accessRole = accessRole;
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * Gets the access role.
   *
   * @return the accessRole
   */
  public String getAccessRole() {
    return accessRole;
  }

  /**
   * From string.
   *
   * @param text the text
   * @return the tipo expedientes PIA listado enum
   */
  public static TipoExpedientesPIAListadoEnum fromString(final String text) {
    return Arrays.asList(TipoExpedientesPIAListadoEnum.values()).stream()
        .filter(opsc -> opsc.valor.equals(text)).findFirst().orElse(null);
  }
}
