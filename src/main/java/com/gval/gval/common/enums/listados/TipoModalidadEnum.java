package com.gval.gval.common.enums.listados;

import java.util.Arrays;

/**
 * The Enum IntensidadEnum.
 */
public enum TipoModalidadEnum implements InterfazListadoLabels<String> {


  /** The minimo. */
  EDUCATIVO("E", "label.listado-enum.preferencia.modalidad.educativo"),

  /** The completo. */
  SOCIO_LABORAL("S", "label.listado-enum.preferencia.modalidad.socio-laboral");


  /** The valor. */
  private String valor;

  /** The label. */
  private String label;



  /**
   * Instantiates a new tipo cuidador.
   *
   * @param valor the valor
   * @param label the label
   */
  TipoModalidadEnum(final String valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  @Override
  public String getValor() {
    return valor;
  }



  /**
   * Gets the label.
   *
   * @return the label
   */
  @Override
  public String getLabel() {
    return label;
  }

  /**
   * From valor.
   *
   * @param valor the valor
   * @return the intensidad enum
   */
  public static TipoModalidadEnum fromValor(final String valor) {
    return Arrays.asList(TipoModalidadEnum.values()).stream()
        .filter(tc -> tc.valor.equals(valor)).findFirst().orElse(null);
  }

  /**
   * Label from valor.
   *
   * @param valor the valor
   * @return the string
   */
  public static String labelFromValor(final String valor) {
    final TipoModalidadEnum intensidadEnum = fromValor(valor);
    return intensidadEnum == null ? null : intensidadEnum.label;
  }
}
