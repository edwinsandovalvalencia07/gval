package com.gval.gval.common.enums;

import com.gval.gval.common.UtilidadesCommons;

/**
 * Mapea los direfentes popUp de mensajes para accder a ellos de manera sencilla
 *
 * @author jptorres
 */
public enum PopUpMsgs {

  /**
   * INFO (Código String: modalInfo.zul).
   */
  INFO("/comun/popups/modalInfo.zul"),
  /**
   * QUESTION (Código String: modalQuestion.zul).
   */
  QUESTION("/comun/popups/modalQuestion.zul"),
  /**
   * SUCCES (Código String: modalSuccess.zul).
   */
  SUCCESS("/comun/popups/modalSuccess.zul"),
  /**
   * WARNING (Código String: modalWarningIcon.zul).
   */
  WARNING("/comun/popups/modalWarningIcon.zul"),
  /**
   * LOGOUT (Código String: modalWarningIcon.zul).
   */
  LOGOUT("/comun/popups/modalDesconectarIcon.zul");

  /**
   * Valor como string.
   */
  private final String stringPopUpMsgs;

  /**
   * Constructor.
   *
   * @param popUpMsgs Valor como string.
   */
  private PopUpMsgs(final String popUpMsgs) {
    this.stringPopUpMsgs = popUpMsgs;
  }


  /*
   * (non-Javadoc)
   *
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString() {
    return stringPopUpMsgs;
  }

  /**
   * Obtiene enum desde string.
   *
   * @param text string
   * @return TypeSiNo
   */
  public static PopUpMsgs fromString(final String text) {
    PopUpMsgs respuesta = PopUpMsgs.INFO;
    if (UtilidadesCommons.isNotNull(text)) {
      for (final PopUpMsgs b : PopUpMsgs.values()) {
        if (text.equalsIgnoreCase(b.toString())) {
          respuesta = b;
          break;
        }
      }
    }
    return respuesta;
  }

  /**
   * Devuelve el indice de la contante en la enumeracion
   *
   * @return En indice en el enumerado
   */
  public int toIndex() {
    int index = 0;

    for (final PopUpMsgs tmp : PopUpMsgs.values()) {
      if (stringPopUpMsgs.equalsIgnoreCase(tmp.toString())) {
        break;
      } else {
        index++;
      }
    }
    return index;
  }
}
