package com.gval.gval.common.enums;

/**
 * The Enum TipoPreferenciaEnum.
 */
public enum TipoPreferenciaEnum {

  /** The iniciales. */
  // @formatter:off
  INICIALES(1L),


  /** The cambio recurso. */
  CAMBIO_RECURSO(2L),


  /** The pia compatible. */
  PIA_COMPATIBLE(3L),


  /** The varios recursos compatibles. */
  VARIOS_RECURSOS_COMPATIBLES(4L);

  // @formatter:on
  /** The value. */
  private Long id;

  /**
   * Instantiates a new tipo incidencia enum.
   *
   * @param value the value
   */
  TipoPreferenciaEnum(final Long value) {
    this.id = value;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public Long getValue() {
    return id;
  }

}
