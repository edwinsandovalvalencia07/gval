package com.gval.gval.common.enums.listados;

import es.gva.dependencia.ada.common.enums.IdentificadorCasoUsoEnum;

import java.util.Arrays;

/**
 * The Enum ListadoValidarDocumentosEnum.
 */
public enum ListadoValidarDocumentosEnum {

  /** The todos. */
  TODOS(IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_TODOS
      .getValor(), "VDTODOS", "VDTODOS", "label.item.documento.todos"),

  /** The no procede. */
  NO_PROCEDE(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_NO_PROCEDE
      .getValor(),
      "VDNOPRO", "VDNOPRO", "label.item.documento.no_procede"),
  /** The peticion nuevas preferencias. */
  PETICION_NUEVAS_PREFERENCIAS(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_PETICION_NUEVAS_PREFERENCIAS
      .getValor(),
      "VPNPAYTO", "VPNPPIA",
      "label.item.documento.peticion_nuevas_preferencias"),
  /** The modelo domiciliacion bancaria. */
  MODELO_DOMICILIACION_BANCARIA(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_MDB
      .getValor(),
      "VMDBAYTO", "VMDBPIA",
      "label.item.documento.modelo_domiciliacion_bancaria"),
  /** The cambio cuidador. */
  CAMBIO_CUIDADOR(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_CAMBIO_CUIDADOR
      .getValor(),
      "VCCUAYTO", "VCCUIPIA", "label.item.documento.cambio_de_cuidador"),
  CAMBIO_ASISTENTE_PERSONAL(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_CAMBIO_ASISTENTE
      .getValor(),
      "VCASAYTO", "VCASPPIA", "label.item.documento.cambio_de_asistente_personal"),
  /** The modelo solicitud. */
  MODELO_SOLICITUD(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_MODELO_SOLICITUD
      .getValor(),
      "VMSAYTO", "VMSPIA", "label.item.documento.modelo_solicitrud"),
  /** The peticion revision. */
  PETICION_REVISION(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_DOCUMENTO_PETI_REVISION
      .getValor(),
      "VPRAYTO", "VPRVPIA", "label.item.documento.peticion_revision"),
  /** The compromiso cnp. */
  COMPROMISO_CNP(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_DOCUMENTO_PETI_REVISION
      .getValor(),
      "VCCAYTO", "VCCPIA", "label.item.documento.compromiso_cnp"),
  /** The contrato pvs rs. */
  CONTRATO_PVS_RS(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_CONTRATO_PVS_RS
      .getValor(),
      "VCRSAYTO", "VCRSAPIA", "label.item.documento.contrato_pvs_rs"),
  /** The contrato pvs cd. */
  CONTRATO_PVS_CD(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_CONTRATO_PVS_CD
      .getValor(),
      "VCCDAYTO", "VCCDPIA", "label.item.documento.contrato_pvs_cd"),
  /** The contrato pvs sad. */
  CONTRATO_PVS_SAD(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_CONTRATO_PVS_SAD
      .getValor(),
      "VCSDAYTO", "VCSDPIA", "label.item.documento.contrato_pvs_sad"),
  /** The contrato pvs sp. */
  CONTRATO_PVS_SP(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_CONTRATO_PVS_SP
      .getValor(),
      "VCSPAYTO", "VCSPPIA", "label.item.documento.contrato_pvs_sp"),
  /** The contrato ap. */
  CONTRATO_AP(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_CONTRATO_AP
      .getValor(),
      "VCAPAYTO", "VCAPPIA", "label.item.documento.contrato_ap"),
  /** The declarion responsable retro cnp. */
  DECLARION_RESPONSABLE_RETRO_CNP(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_RESPO_RETRO_CNP
      .getValor(),
      "VRRPAYTO", "VRRPPIA",
      "label.item.documento.declaracion_responsable_retro_cnp"), 
 /** The solicitud revision importe. */
 SOLICITUD_REVISION_IMPORTE(
          IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_REVISION_IMPORTE
              .getValor(),
          "VSRIAYTO", "VSRIPIA",
          "label.item.documento.solicitud_revision_importe"), 
 /** The solicitud revision horas. */
 SOLICITUD_REVISION_HORAS(
              IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_REVISION_HORAS
                  .getValor(),
              "VSRHAYTO", "VSRHPIA",
              "label.item.documento.solicitud_revision_horas");

  /** The access role. */
  private String accessRole;

  /** The valor AYTO. */
  private String valorAYTO;

  /** The valor PIA. */
  private String valorPIA;

  /** The label. */
  private String label;

  /**
   * Tipo listado validar documentos enum.
   *
   * @param accessRole the access role
   * @param valorAYTO the valor AYTO
   * @param valorPIA the valor PIA
   * @param label the label
   */
  private ListadoValidarDocumentosEnum(final String accessRole,
      final String valorAYTO, final String valorPIA, final String label) {
    this.accessRole = accessRole;
    this.valorAYTO = valorAYTO;
    this.valorPIA = valorPIA;
    this.label = label;
  }

  /**
   * Gets the access role.
   *
   * @return the access role
   */
  public String getAccessRole() {
    return accessRole;
  }

  /**
   * Gets the valor AYTO.
   *
   * @return the valor AYTO
   */
  public String getValorAYTO() {
    return valorAYTO;
  }

  /**
   * Gets the valor PIA.
   *
   * @return the valor PIA
   */
  public String getValorPIA() {
    return valorPIA;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * From string.
   *
   * @param text the text
   * @return the listado validar documentos enum
   */
  public static ListadoValidarDocumentosEnum fromString(final String text) {
    return Arrays.asList(ListadoValidarDocumentosEnum.values()).stream()
        .filter(opsc -> opsc.valorAYTO.equals(text)).findFirst().orElse(null);
  }
}
