package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;


// TODO: Auto-generated Javadoc
/**
 * The Enum PermisoEnum.
 */
public enum PermisoEnum {

  /** The datafield desc. */
  DATAFIELD_DESC("pedesc"),

  /** The boton incidencia. */
  BOTON_INCIDENCIA("btnIncidencia"),
  /** The boton leido. */
  BOTON_LEIDO("btnLeido"),

  /** The boton imprimir valoracion. */
  // BOTON_ANYADIR_SERVICIO("btnAnyadirServicio"),
  BOTON_IMPRIMIR_VALORACION("btnImprimirValoracion"),
  /** The boton imprimir valoracion sin observ. */
  BOTON_IMPRIMIR_VALORACION_SIN_OBSERV("btnImprimirValoracionSinObserv"),
  /** The boton imprimir comision. */
  BOTON_IMPRIMIR_COMISION("btnImprimirComision"),
  /** The boton imprimir comision sin observ. */
  BOTON_IMPRIMIR_COMISION_SIN_OBSERV("btnImprimirComisionSinObserv"),
  /** The boton imprimir proppia. */
  BOTON_IMPRIMIR_PROPPIA("btnImprimirPropPia"),
  /** The boton imprimir alegaciones. */
  BOTON_IMPRIMIR_ALEGACIONES("btnImprimirAlegaciones"),
  /** The boton imprimir archivofallecimiento. */
  BOTON_IMPRIMIR_ARCHIVOFALLECIMIENTO("btnArchivoFallecimiento"),


  /** The boton imprimir resolucion. */
  BOTON_IMPRIMIR_RESOLUCION("btnImprimirResolucion"),
  /** The boton imprimir resolucion pia. */
  BOTON_IMPRIMIR_RESOLUCION_PIA("btnImprimirResolucionPia"),
  /** The boton imprimir otras resoluciones. */
  BOTON_IMPRIMIR_OTRAS_RESOLUCIONES("btnImprimirOtraResolucion"),
  /** The boton imprimir documentos. */
  BOTON_IMPRIMIR_DOCUMENTOS("btnImprimirDocumentos"),
  /** The boton imprimir incidencias. */
  BOTON_IMPRIMIR_INCIDENCIAS("btnImprimirIncidencias"),
  /** The boton imprimir infosoci. */
  BOTON_IMPRIMIR_INFOSOCI("btnImprimirInformeSocial"),
  /** The boton nuevo solicitud. */
  BOTON_NUEVO_SOLICITUD("btnNuevoSolicitud"),
  /** The boton guardar solicitud. */
  BOTON_GUARDAR_SOLICITUD("btnGuardarSolicitud"),


  /** The boton eliminar solicitud. */
  BOTON_ELIMINAR_SOLICITUD("btnEliminarSolicitud"),
  /** The boton eliminar expediente. */
  BOTON_ELIMINAR_EXPEDIENTE("btnEliminarExpediente"),
  /** The boton nuevo valoracion. */
  BOTON_NUEVO_VALORACION("btnNuevoValoracion"),

  /** The boton nuevo resolucion. */
  // BOTON_NUEVO_PIA("btnNuevoPia"),
  BOTON_NUEVO_RESOLUCION("btnNuevoResolucion"),
  /** The boton nuevo documento aportado. */
  BOTON_NUEVO_DOCUMENTO_APORTADO("btnNuevoDocumentoAportado"),
  /** The boton nuevo documento generado. */
  BOTON_NUEVO_DOCUMENTO_GENERADO("btnNuevoDocumentoGenerado"),
  /** The boton nuevo incidencias. */
  BOTON_NUEVO_INCIDENCIAS("btnNuevoIncidencias"),
  /** The boton nuevo is. */
  BOTON_NUEVO_IS("btnNuevoIS"),
  /** The boton nuevo it. */
  BOTON_NUEVO_IT("btnNuevoIT"),
  /** The boton nuevo itmotivo. */
  BOTON_NUEVO_ITMOTIVO("btnNuevoITMotivo"),
  /** The boton nuevo tercero. */
  BOTON_NUEVO_TERCERO("btnNuevoTercero"),
  /** The boton nuevo heredero. */
  BOTON_NUEVO_HEREDERO("btnNuevoHeredero"),
  /** The boton tercero. */
  BOTON_TERCERO("btnTercero"),
  /** The boton retro. */
  BOTON_RETRO("btnRetro"),


  /** The boton listado fiscalizacion. */
  BOTON_LISTADO_FISCALIZACION("btnListadoFiscalizacion"),
  /** The boton diagnostico. */
  BOTON_DIAGNOSTICO("btnDiagnostico"),
  /** The boton anular inter aeat. */
  BOTON_ANULAR_INTER_AEAT("btnAnularIntercambioAeat"),

  /** The boton anular capaecon. */
  BOTON_ANULAR_CAPAECON("btnAnularConfirmaciónCapacidadEconómica"),
  /** The boton anular acuerdo inicio. */
  BOTON_ANULAR_ACUERDO_INICIO("btnAnularAcuerdoInicio"),
  /** The boton nuevo acuerdo inicio. */
  BOTON_NUEVO_ACUERDO_INICIO("btnNuevoAcuerdoInicio"),
  /** The boton liberar proppia. */
  BOTON_LIBERAR_PROPPIA("btnLiberarProppia"),
  /** The boton liberar pia. */
  BOTON_LIBERAR_PIA("btnLiberarPia"),
  /** The boton echar atras proppia. */
  BOTON_ECHAR_ATRAS_PROPPIA("btnEcharAtrasProppia"),

  /** The boton eliminar not resol gyn. */
  BOTON_ELIMINAR_NOT_RESOL_GYN("btnEliminarNotResolGYN"),
  /** The boton eliminar not proppia. */
  BOTON_ELIMINAR_NOT_PROPPIA("btnEliminarNotPropPia"),
  /** The boton eliminar not resol ra. */
  BOTON_ELIMINAR_NOT_RESOL_RA("btnEliminarNotResolRA"),
  /** The boton eliminar not resol rca. */
  BOTON_ELIMINAR_NOT_RESOL_RCA("btnEliminarNotResolRCA"),

  /** The boton eliminar not resol pdh. */
  BOTON_ELIMINAR_NOT_RESOL_PDH("btnEliminacionResolucionesPDH"),

  /** The boton heredero pdh. */
  BOTON_HEREDERO_PDH("btnHeredero"),
  /** The boton guardar capaecon. */
  BOTON_GUARDAR_CAPAECON("btnGuardarCapacidadEconomica"),
  /** The boton guardar infosoci. */
  BOTON_GUARDAR_INFOSOCI("btnGuardarInformeSocial"),
  /** The boton guardar valoracion. */
  BOTON_GUARDAR_VALORACION("btnGuardarValoracion"),
  /** The boton guardar comision. */
  BOTON_GUARDAR_COMISION("btnGuardarComision"),
  /** The boton guardar proppia. */
  BOTON_GUARDAR_PROPPIA("btnGuardarPropPia"),
  /** The boton guardar alegaciones. */
  BOTON_GUARDAR_ALEGACIONES("btnGuardarAlegaciones"),
  /** The boton guardar pia. */
  BOTON_GUARDAR_PIA("btnGuardarPia"),
  /** The boton guardar prestacion. */
  BOTON_GUARDAR_PRESTACION("btnGuardarPrestacion"),
  /** The boton eliminar docuapor. */
  BOTON_ELIMINAR_DOCUAPOR("btnEliminarDocuAportado"),
  /** The boton eliminar docugenerado. */
  BOTON_ELIMINAR_DOCUGENERADO("btnEliminarDocuGenerado"),
  /** The boton confirmar solicitud. */
  BOTON_CONFIRMAR_SOLICITUD("btnConfirmarSolicitud"),
  /** The boton confirmar capaecon. */
  BOTON_CONFIRMAR_CAPAECON("btnConfirmarCapacidadEconomica"),

  /** The boton confirmar proppia. */
  BOTON_CONFIRMAR_PROPPIA("btnConfirmarPropPia"),
  /** The boton confirmar alegaciones. */
  BOTON_CONFIRMAR_ALEGACIONES("btnConfirmarAlegaciones"),
  /** The boton confirmar pia. */
  BOTON_CONFIRMAR_PIA("btnConfirmarPia"),
  /** The boton validar solicitud. */
  BOTON_VALIDAR_SOLICITUD("btnValidarSolicitud"),
  /** The boton validar documentacion. */
  BOTON_VALIDAR_DOCUMENTACION("btnValidarDocumentacion"),
  /** The boton validar capaecon. */
  BOTON_VALIDAR_CAPAECON("btnValidarCapacidadEconomica"),
  /** The boton subsanar. */
  BOTON_SUBSANAR("btnSubsanar"),
  /** The boton cerrar infosoci. */
  BOTON_CERRAR_INFOSOCI("btnCerrarInformeSocial"),
  /** The boton eliminar infosoci. */
  BOTON_ELIMINAR_INFOSOCI("btnEliminarIS"),
  /** The boton asociar infosoci. */
  BOTON_ASOCIAR_INFOSOCI("btnAsociarIS"),
  /** The boton cerrar valoracion. */
  BOTON_CERRAR_VALORACION("btnCerrarValoracion"),
  /** The boton cambiar sector. */
  BOTON_CAMBIAR_SECTOR("btnCambiarSector"),
  /** The boton cambiar recurso. */
  BOTON_CAMBIAR_RECURSO("btnCambiarRecurso"),
  /** The boton liberar estudio. */
  BOTON_LIBERAR_ESTUDIO("btnLiberarEstudio"),
  /** The boton liberar valoracion. */
  BOTON_LIBERAR_VALORACION("btnLiberarValoracion"),
  /** The boton eliminar incidencia. */
  BOTON_ELIMINAR_INCIDENCIA("btnEliminarIncidencia"),
  /** The boton guardar subsanacion. */
  BOTON_GUARDAR_SUBSANACION("btnGuardarSubsanacion"),
  /** The boton guardar preferencia. */
  BOTON_GUARDAR_PREFERENCIA("btnGuardarPreferencia"),

  /** The boton configurar envios. */
  BOTON_CONFIGURAR_ENVIOS("btnConfigurarImserso"),
  /** The boton envios pia. */
  BOTON_ENVIOS_PIA("btnEnvioPia"),

  /** The boton copiar datos solicitud. */
  BOTON_COPIAR_DATOS_SOLICITUD("btnCopiarDatosSolicitud"),

  /** The boton atras confirmacion pia. */
  BOTON_ATRAS_CONFIRMACION_PIA("btnAtrasConfirmacionPia"),

  /** The permiso modificar datos solicitante. */
  PERMISO_MODIFICAR_DATOS_SOLICITANTE("permisosModificarDatosSolicitante"),
  /** The permiso modificar solicitud resuelta. */
  PERMISO_MODIFICAR_SOLICITUD_RESUELTA("permisosModificarSolicitudResuelta"),

  /** The permiso modificar datos tercero lote. */
  // Modificación cambiar version de terceros
  PERMISO_MODIFICAR_DATOS_TERCERO_LOTE("permisosModificarDatosTerceroLote"),

  /** The permiso modificar datos tec tutelas. */
  // Periso para la UTT
  PERMISO_MODIFICAR_DATOS_TEC_TUTELAS("permisosModificarDatosTecTutelas"),

  /** The boton recalcular sad. */
  BOTON_RECALCULAR_SAD("btnRecalcularSAD"),
  // martinez_danizq 28/08/2012 MOD#2002 El check de urgencia debe desbloquearse
  /** The permiso modificar urgencia. */
  // INICIO 28/08/2012 MOD#2002 Se anyade permiso
  PERMISO_MODIFICAR_URGENCIA("permisosModificarUrgencia"),
  // FIN MOD#2002

  // martinez_danizq 14/03/2013 MOD#2488 Dar acceso siempre al perfil COMISION al botón de CBASE de la pestaña Situación de Depedencia de la solicitud
  /** The permiso btncbase situaciondependencia. */
  // INICIO 14/03/2013 MOD#2488 Se anyade permiso
  PERMISO_BTNCBASE_SITUACIONDEPENDENCIA("btnCBASESituacionDependencia"),
  // FIN MOD#2488
  // martinez_danizq 26/08/2013 MOD#2834 Controlar quien introduce bajas por fallecimiento
  /** The permiso chk fallecidos avapsa. */
  // INICIO 26/08/2013 MOD#2834 Anyadimos el permiso
  PERMISO_CHK_FALLECIDOS_AVAPSA("chkFallecidosAvapsa"),
  /** The permiso chk fallecidos prestacion. */
  PERMISO_CHK_FALLECIDOS_PRESTACION("chkFallecidosPrestacion"),
  // FIN MOD#2834

  /** The permiso guardar fecha baja pia. */
  // ENT-420196
  PERMISO_GUARDAR_FECHA_BAJA_PIA("permisosGuardarFechaBajaPia"),

  /** The permiso nueva incidencia exitus. */
  PERMISO_NUEVA_INCIDENCIA_EXITUS("permisoNuevaIncidenciaExitus"),

  /** The retroactividad pago unico. */
  RETROACTIVIDAD_PAGO_UNICO("btnRetroactidadPagoUnico"),


  /** The boton guardar expediente. */
  BOTON_GUARDAR_EXPEDIENTE("btnGuardar"),



  /** The boton generacion listado boe. */
  BOTON_GENERACION_LISTADO_BOE("btnGeneracionListadoBOE"),

  /** The permiso grabar tercero lote. */
  PERMISO_GRABAR_TERCERO_LOTE("permisoGrabLote"),

  /** The boton nuevo lote. */
  // SForms
  BOTON_NUEVO_LOTE("btnNuevoLote"),
  /** The boton modificar lote. */
  BOTON_MODIFICAR_LOTE("btnModificarLote"),
  /** The boton atras lote. */
  BOTON_ATRAS_LOTE("btnAtrasLote"),

  /** The boton subida documentacion. */
  BOTON_SUBIDA_DOCUMENTACION("btnSubidaDocumentacion"),


  /** The boton informe cal sad. */
  BOTON_INFORME_CAL_SAD("btnImprInformeSAD"),


  /** The boton pte validacion. */
  BOTON_PTE_VALIDACION("btnPteValidacion"),

  /** The boton suspension expediente. */
  BOTON_SUSPENSION_EXPEDIENTE("btnSuspensionExpediente"),

  /** The boton cancelar archivo provisional. */
  BOTON_CANCELAR_ARCHIVO_PROVISIONAL("btnCancelarArchivoProvisional"),

  /** The boton intercambio cuentas. */
  BOTON_INTERCAMBIO_CUENTAS("btnImportarCuentas"),
  /** The boton pte validacion docu. */
  BOTON_PTE_VALIDACION_DOCU("btnPteValidacionDocumentacion"),

  /** The boton guardar situacion dep. */
  BOTON_GUARDAR_SITUACION_DEP("btnGuardarSituacionDep"),

  /** The boton nuevo cnp pia. */
  // ENT-350029
  BOTON_NUEVO_CNP_PIA("btnNuevoCNPPia"),
  /** The boton eliminar cnp pia. */
  BOTON_ELIMINAR_CNP_PIA("btnEliminarCNPPia"),
  /** The boton incidencia cnp pia. */
  BOTON_INCIDENCIA_CNP_PIA("btnInciCNP"),
  /** The boton historico cnp pia. */
  BOTON_HISTORICO_CNP_PIA("btnHistoricoCNP"),


  /** The boton marcha atras solicitud censo cnp. */
  // Censo cuidador
  BOTON_MARCHA_ATRAS_SOLICITUD_CENSO_CNP("btnMarchaAtrasSolicitudCensoCNP"),
  /** The boton marcha atras lote censo cnp. */
  BOTON_MARCHA_ATRAS_LOTE_CENSO_CNP("btnMarchaAtrasLoteCensoCNP"),

  /** The boton bloqueo temporal. */
  BOTON_BLOQUEO_TEMPORAL("btnBloqueoTempSolicitud"),


  /** The boton nuevo informe resp patrimonial. */
  BOTON_NUEVO_INFORME_RESP_PATRIMONIAL("btnNuevoInformeRespPatrimonial"),

  /** The boton guardar informe resp patrimonial. */
  BOTON_GUARDAR_INFORME_RESP_PATRIMONIAL("btnGuardarInformeRespPatrimonial"),
  /** The boton finalizar informe resp patrimonial. */
  BOTON_FINALIZAR_INFORME_RESP_PATRIMONIAL("btnFinalizarInformeRespPatrimonial"),
  /** The boton imprimir peticion informe resp patrimonial. */
  BOTON_IMPRIMIR_PETICION_INFORME_RESP_PATRIMONIAL("btnImprimirPeticionInformeRespPatrimonial"),
  /** The boton imprimir informe resp patrimonial. */
  BOTON_IMPRIMIR_INFORME_RESP_PATRIMONIAL("btnImprimirInformeRespPatrimonial"),
  /** The boton marcha atras informe resp patrimonial. */
  BOTON_MARCHA_ATRAS_INFORME_RESP_PATRIMONIAL("btnMarchaAtrasPeticionInformeRespPatrimonial"),

  /** The boton actualizar prestaciones. */
  BOTON_ACTUALIZAR_PRESTACIONES("btnActualizarPrestaciones"),

  /** The permiso guardar pia bloqueado. */
  PERMISO_GUARDAR_PIA_BLOQUEADO("permisosGuardarPiaBloqueado");


  /** The codigo. */
  private final String codigo;


  /**
   * Instantiates a new permiso enum.
   *
   * @param codigo the codigo
   */
  PermisoEnum(final String codigo) {
    this.codigo = codigo;
  }


  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }


  /**
   * From codigo.
   *
   * @param codigo the codigo
   * @return the permiso enum
   */
  @Nullable
  public static PermisoEnum fromCodigo(final String codigo) {

    if (codigo == null) {
      return null;
    }

    for (final PermisoEnum td : PermisoEnum.values()) {
      if (td.codigo.equalsIgnoreCase(codigo)) {
        return td;
      }
    }
    return null;
  }

}
