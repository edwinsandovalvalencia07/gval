package com.gval.gval.common.enums;

import java.util.Arrays;

/**
 * The Class RespuestaEnum.
 */
public enum RespuestaEnum {


  /** The si. */
  SI(Boolean.TRUE, "label.si"),

  /** The no. */
  NO(Boolean.FALSE, "label.no");

  /** The valor. */
  private final Boolean valor;

  /** The label. */
  private final String label;

  /**
   * Instantiates a new respuesta enum.
   *
   * @param valor the valor
   * @param label the label
   */
  RespuestaEnum(final Boolean valor, final String label) {
    this.valor = valor;
    this.label = label;
  }


  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public Boolean getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * From boolean.
   *
   * @param valor the valor
   * @return the respuesta enum
   */
  public static RespuestaEnum fromValor(final Boolean valor) {
    return Arrays.asList(RespuestaEnum.values()).stream()
        .filter(tc -> tc.valor.equals(valor)).findFirst().orElse(null);
  }

}
