package com.gval.gval.common.enums;

import java.util.stream.Stream;

import org.apache.commons.codec.binary.StringUtils;

/**
 * The Enum ValoracionesRespuestasClaveEnum.
 */
public enum ValoracionesTipoActividadesEnum {


  /** The ninguno. */
  NINGUNO(null),

  /** The motriz. */
  MOTRIZ("MOTRIZ"),

  /** The adaptativa. */
  ADAPTATIVA("ADAPTATIVA"),

  /** The vital. */
  VITAL("VITAL"),

  /** The movilidad. */
  MOVILIDAD("MOVILIDAD"),

  /** The peso. */
  PESO("PESO"),

  /** The desarrollo. */
  DESARROLLO("DESARROLLO"),
  
  /** The salud. */
  SALUD("SALUD", "MANTSALUD"),
  
  /** The comer. */
  COMER("COMER", "COMERBEBER"),
  
  /** The corporales. */
  CORPORALES("CORPORALES", "OTROSCUIDADOS"),
  
  /** The decisiones. */
  DECISIONES("DECISIONES", "TOMADECISIONES"),
  
  /** The desplazad. */
  DESPLAZAD("DESPLAZAD", "DESPLDENHOGAR"),
  
  /** The desplazaf. */
  DESPLAZAF("DESPLAZAF", "DESPLFUEHOGAR"),
  
  /** The domesticas. */
  DOMESTICAS("DOMESTICAS", "TAREASDOMESTICAS"),
  
  /** The higiene. */
  HIGIENE("HIGIENE", "HIGIENEPERSONAL"),
  
  /** The lavarse. */
  LAVARSE("LAVARSE", "LAVARSE"),
  
  /** The posicion. */
  POSICION("POSICION", "POSICIONCUERPO"),
  
  /** The vestirse. */
  VESTIRSE("VESTIRSE", "VESTIRSE");

  /** The pre id. */
  public final String id;
  
  /** The id sigma. */
  public final String idSigma;

  /**
   * Instantiates a new valoraciones sub preguntas tipo enum.
   *
   * @param id the pre id
   */
  private ValoracionesTipoActividadesEnum(final String id) {
    this.id = id;
    this.idSigma = null;
  }
  
  private ValoracionesTipoActividadesEnum(final String id, final String idSigma) {
    this.id = id;
    this.idSigma = idSigma;
  }

  /**
   * Gets the pre id.
   *
   * @return the pre id
   */
  public String getId() {
    return id;
  }

  /**
   * Stream.
   *
   * @return the stream
   */
  public static Stream<ValoracionesTipoActividadesEnum> stream() {
    return Stream.of(ValoracionesTipoActividadesEnum.values());
  }

  /**
   * Gets the by id.
   *
   * @param id the id
   * @return the by id
   */
  public static ValoracionesTipoActividadesEnum getById(final String id) {
    for (final ValoracionesTipoActividadesEnum e : values()) {
      if (e.name().equals(id)) {
        return e;
      }
    }
    return NINGUNO;
  }
  
  /**
   * Gets the by id sigma.
   *
   * @param idSigma the id sigma
   * @return the by id sigma
   */
  public static ValoracionesTipoActividadesEnum getByIdSigma(final String idSigma) {
    for (final ValoracionesTipoActividadesEnum e : values()) {
      if (StringUtils.equals(e.idSigma, idSigma)) {
        return e;
      }
    }
    return NINGUNO;
  }
}
