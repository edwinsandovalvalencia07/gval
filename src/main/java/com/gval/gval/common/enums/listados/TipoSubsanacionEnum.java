package com.gval.gval.common.enums.listados;

import java.util.Arrays;


/**
 * The Enum TipoSubsanacionEnum.
 */
public enum TipoSubsanacionEnum {

  /** The grado. */
  GRADO("G", "label.columna.grado"),

  /** The pia. */
  PIA("P", "label.pia"),

  /** The heredero. */
  HEREDERO("H", "refcode.heredero"),

  /** The mdb. */
  MDB("M", "refcode.mdb");

  /** The codigo. */
  private String codigo;

  /** The label. */
  private String label;


  /**
   * Instantiates a new tipo subsanacion enum.
   *
   * @param codigo the codigo
   * @param label the label
   */
  TipoSubsanacionEnum(final String codigo, final String label) {
    this.codigo = codigo;
    this.label = label;
  }


  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getCodigo() {
    return codigo;
  }


  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * From valor.
   *
   * @param codigo the codigo
   * @return the tipo subsanacion enum
   */
  public static TipoSubsanacionEnum fromValor(final String codigo) {
    return Arrays.asList(TipoSubsanacionEnum.values()).stream()
        .filter(ts -> ts.codigo.equals(codigo)).findFirst().orElse(null);
  }

  /**
   * Label from codigo.
   *
   * @param codigo the codigo
   * @return the string
   */
  public static String labelFromCodigo(final String codigo) {
    final TipoSubsanacionEnum tipoCuidadorEnum = fromValor(codigo);
    return tipoCuidadorEnum == null ? null : tipoCuidadorEnum.label;
  }
}
