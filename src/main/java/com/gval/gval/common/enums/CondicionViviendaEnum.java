package com.gval.gval.common.enums;

import java.util.Arrays;

/**
 * The Enum CondicionViviendaEnum.
 */
public enum CondicionViviendaEnum {

  /** The adecuada. */
  ADECUADA("1", "label.informe_social_condicion_vivienda_adecuada"),

  /** The habitabilidad. */
  HABITABILIDAD("2", "label.informe_social_condicion_vivienda_habitabilidad"),

  /** The dificulta habitabilidad. */
  DIFICULTA_HABITABILIDAD("3", "label.informe_social_condicion_vivienda_dificulta_habitabilidad"),

  /** The inaceptable. */
  INACEPTABLE("4", "label.informe_social_condicion_vivienda_inaceptable");

  /** The orden. */
  private String valor;

  /** The valor. */
  private String label;

  /**
   * Instantiates a new regimen vivienda enum.
   *
   * @param valor the valor
   * @param label the label
   */
  private CondicionViviendaEnum(final String valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * From valor.
   *
   * @param valor the valor
   * @return the condicion vivienda enum
   */
  public static CondicionViviendaEnum fromValor(final String valor) {
    return Arrays.asList(CondicionViviendaEnum.values()).stream()
        .filter(rv -> rv.valor.equals(valor)).findFirst().orElse(null);
  }


  /**
   * Label from valor.
   *
   * @param valor the valor
   * @return the string
   */
  public static String labelFromValor(final String valor) {
    final CondicionViviendaEnum regimenVivienda = fromValor(valor);
    return regimenVivienda == null ? null : regimenVivienda.label;
  }

}
