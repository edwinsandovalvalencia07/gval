package com.gval.gval.common.enums;

/**
 * The Enum TipoIncidenciaEnum.
 */
public enum TipoIncidenciaEnum {


  /** The revisada. */
  REVISADA(13L, null, null),

  /** The repeticion valoracion. */
  REPETICION_VALORACION(9L, null, null),

  /** The repeticion informe social. */
  REPETICION_INFORME_SOCIAL(10L, null, null),

  /** The repeticion informe salud. */
  REPETICION_INFORME_SALUD(11L, null, null),

  /** The cambio homologacion valoracion. */
  CAMBIO_HOMOLOGACION_VALORACION(19L, null, null),

  /** The documentacion aportada. */
  DOCUMENTACION_APORTADA(1L, null, null),

  /** The nueva subsanacion. */
  NUEVA_SUBSANACION(20L, null, null),

  /** The finaliza subsanacion. */
  FINALIZA_SUBSANACION(21L, null, null),

  /** The solici registrada. */
  SOLICI_REGISTRADA(23L, null, null),

  /** The citacion valoracion. */
  CITACION_VALORACION(24L, null, null),

  /** The cita aplazada. */
  CITA_APLAZADA(25L, null, null),

  /** The cita cancelada. */
  CITA_CANCELADA(26L, null, null),

  /** The valoracion realizada. */
  VALORACION_REALIZADA(27L, null, null),

  /** The dictamen emitido. */
  DICTAMEN_EMITIDO(28L, null, null),

  /** The dictamen aprobado acta. */
  DICTAMEN_APROBADO_ACTA(29L, null, null),

  /** The resol emitida. */
  RESOL_EMITIDA(30L, null, null),

  /** The resol notificada. */
  RESOL_NOTIFICADA(31L, null, null),

  /** The recurso aceptado. */
  RECURSO_ACEPTADO(32L, null, null),

  /** The recurso estimado. */
  RECURSO_ESTIMADO(33L, null, null),

  /** The recurso desestimado. */
  RECURSO_DESESTIMADO(34L, null, null),

  /** The revision estimada. */
  REVISION_ESTIMADA(35L, null, null),

  /** The revision desestimada. */
  REVISION_DESESTIMADA(36L, null, null),

  /** The cambio nivel urgencia. */
  CAMBIO_NIVEL_URGENCIA(37L, null, null),

  /** The bloqueo solicit. */
  BLOQUEO_SOLICITUD(38L, null, null),

  /** The desbloque solicit. */
  DESBLOQUEO_SOLICITUD(39L, null, null),

  /** The recurso no aceptado. */
  RECURSO_NO_ACEPTADO(40L, null, null),

  /** The alegacion cerrada. */
  ALEGACION_CERRADA(41L, null, null),

  /** The propuesta pia realizada. */
  PROPUESTA_PIA_REALIZADA(42L, null, null),

  /** The otros tipo incidencia. */
  OTROS_TIPO_INCIDENCIA(161L, "Otros tipos de incidencia", null),

  /** The resolucion firmada. */
  RESOLUCION_FIRMADA(321L, null, null),

  /** The verificacion peticion nuevas pref. */
  VERIFICACION_PETICION_NUEVAS_PREF(358L, null, null),

  /** The verificacion cambio cuidador. */
  VERIFICACION_CAMBIO_CUIDADOR(359L, null, null),

  /** The verificacion domiciliacion bancaria. */
  VERIFICACION_DOMICILIACION_BANCARIA(360L, null, null),

  /** The bloqueo por discrepancia. */
  BLOQUEO_POR_DISCREPANCIA(364L, "Bloqueo por discrepancia",
      "Se elimina el bloqueo cuando se cierre la incidencia por discrepancia "),

  /** The bloqueo por subsanacion. */
  BLOQUEO_POR_SUBSANACION(367L, "Bloqueo por Subsanación",
      "Se elimina el bloqueo cuando se finalice el proceso de subsanación de fecha "),

  /** The bloqueo por fallecimiento. */
  BLOQUEO_POR_FALLECIMIENTO(368L, "Bloqueo por Fallecimiento", null),

  /** The bloqueo por peticion informe tecnico motivo. */
  BLOQUEO_POR_PETICION_INFORME_TECNICO_MOTIVO(365L,
      "Bloqueo por Petición informe técnico con motivo",
      "Se elimina el bloqueo cuando se cierre el informe técnico con motivo de fecha "),

  /** The bloqueo por peticion informe salud mental. */
  BLOQUEO_POR_PETICION_INFORME_SALUD_MENTAL(363L,
      "Bloqueo por Petición informe salud mental",
      "Se elimina el bloqueo cuando se aporte el documento informe de salud extendido mental."),

  /** The bloqueo por peticion informe salud fisico. */
  BLOQUEO_POR_PETICION_INFORME_SALUD_FISICO(362L,
      "Bloqueo por Petición informe salud físico",
      "Se elimina el bloqueo cuando se aporte el documento informe de salud extendido físico."),

  /** The bloqueo por peticion informe salud fisico. */
  BLOQUEO_POR_CAMBIO_ESTADO_FINAL(366L, "Bloqueo por Cambio estado final",
      "Se elimina el bloqueo cuando se reactive la solicitud."),

  /** The consulta pai ultima residencia. */
  CONSULTA_PAI_ULTIMA_RESIDENCIA(422L, "Consulta a PAI la última residencia",
      null),

  /** The consulta pai prestacion. */
  CONSULTA_PAI_PRESTACION(423L, "Consulta a la Pai la Prestación", null),

  /** The consulta imserso identidad. */
  CONSULTA_IMSERSO_IDENTIDAD(424L, "Consulta al Imserso la identidad", null),

  /** The consulta pai identidad. */
  CONSULTA_PAI_IDENTIDAD(425L, "Consulta a PAI la identidad", null),

  /** The bloqueo solicitud no validada. */
  BLOQUEO_SOLICITUD_NO_VALIDADA(441L, "Bloqueo por no validación de solicitud",
      "Se elimina el bloqueo cuando se cierre la incidencia por solicitud no validada."),

  /** The solicitud no validada. */
  SOLICITUD_NO_VALIDADA(442L,
      "Solicitud no validada por técnico de Consellería", null),

  /** The no validacion por discrepancia. */
  NO_VALIDACION_POR_DISCREPANCIA(461L,
      "Documento %s no validado por discrepancia", null),

  /** The bloqueo solicit. */
  BORRADO_CENTRO_PREFERENCIAS(462L, null, null),

  /** The cambio de dirección. */
  CAMBIO_DE_DIRECCION(463L, "Cambio de dirección", null),

  /** The bloqueo temporal solicit. */
  BLOQUEO_TEMPORAL(421L, "Bloqueo temporal de la solicitud",
      "Motivo del bloqueo temporal: "),

  /** The cierre preferencias manual. */
  CIERRE_PREFERENCIAS_MANUAL(482L, "Cierre preferencias manual", null);


  // @formatter:on

  /** The value. */
  private final Long value;

  /** The descripcion. */
  private final String descripcion;

  /** The comentario inicial. */
  private final String comentarioInicial;

  /**
   * Instantiates a new tipo incidencia enum.
   *
   * @param value the value
   * @param descripcion the descripcion
   * @param comentarioInicial the comentario inicial
   */
  TipoIncidenciaEnum(final Long value, final String descripcion,
      final String comentarioInicial) {
    this.value = value;
    this.descripcion = descripcion;
    this.comentarioInicial = comentarioInicial;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public Long getValue() {
    return value;
  }


  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Gets the comentario inicial.
   *
   * @return the comentario inicial
   */
  public String getComentarioInicial() {
    return comentarioInicial;
  }

  /**
   * From long.
   *
   * @param valorParam the valor param
   * @return the tipo incidencia enum
   */
  public static TipoIncidenciaEnum fromLong(final Long valorParam) {
    TipoIncidenciaEnum tipo = null;
    for (final TipoIncidenciaEnum type : TipoIncidenciaEnum.values()) {
      if (type.getValue().intValue() == valorParam) {
        tipo = type;
        break;
      }
    }
    return tipo;
  }

}
