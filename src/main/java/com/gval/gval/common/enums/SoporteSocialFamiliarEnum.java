package com.gval.gval.common.enums;

import java.util.Arrays;

/**
 * The Enum SoporteSocialFamiliarEnum.
 */
public enum SoporteSocialFamiliarEnum {

  /** The per gran disponibilidad. */
  PER_GRAN_DISPONIBILIDAD(1, "label.informe_social.gran_disponibilidad"),

  /** The per disp noche. */
  PER_DISP_NOCHE(2, "label.informe_social.disponibilidad_noche"),

  /** The per disp concret. */
  PER_DISP_CONCRET(3, "label.informe_social.disponibilidad_concreta"),

  /** The per disp 2 dias. */
  PER_DISP_2_DIAS(4, "label.informe_social.disponibilidad_2_dias"),

  /** The per disp semanal. */
  PER_DISP_SEMANAL(5, "label.informe_social.disponibilidad_semanal"),

  /** The per no disp. */
  PER_NO_DISP(6, "label.informe_social.no_disponibilidad");

  /** The valor. */
  private Integer valor;

  /** The label. */
  private String label;

  /**
   * Instantiates a new soporte social familiar enum.
   *
   * @param valor the valor
   * @param label the label
   */
  private SoporteSocialFamiliarEnum(final Integer valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public Integer getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * From integer.
   *
   * @param text the text
   * @return the soporte social familiar enum
   */
  public static SoporteSocialFamiliarEnum fromInteger(final Integer text) {
    return Arrays.asList(SoporteSocialFamiliarEnum.values()).stream()
        .filter(ssf -> ssf.valor.equals(text)).findFirst().orElse(null);
  }

}
