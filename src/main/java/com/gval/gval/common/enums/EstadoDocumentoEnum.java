package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;

/**
 * The Enum EstadoDocumentoEnum.
 */
public enum EstadoDocumentoEnum {

  // @formatter:off

  /** The en proceso. */
  EN_PROCESO("PRO", true),

  /** The pendiente verificar. */
  PENDIENTE_VERIFICAR("PVA", true),

  /** The comprobado. */
  COMPROBADO("COM", false),

  /** The no validado. */
  NO_VALIDADO("NVA", true),

  /** The nop procede. */
  NO_PROCEDE("NPC", false);

  // @formatter:on

  /** The codigo. */
  private String codigo;

  /** The fase verificacion. */
  private boolean faseVerificacion;


  /**
   * Instantiates a new estado documento enum.
   *
   * @param codigo the codigo
   * @param faseVerificacion the fase verificacion
   */
  EstadoDocumentoEnum(final String codigo, final boolean faseVerificacion) {
    this.codigo = codigo;
    this.faseVerificacion = faseVerificacion;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  // metodo que devuelve el codigo del estado
  public String getCodigo() {
    return codigo;
  }

  /**
   * Checks if is fase verificacion.
   *
   * @return true, if is fase verificacion
   */
  public boolean isFaseVerificacion() {
    return faseVerificacion;
  }

  /**
   * From codigo.
   *
   * @param codigo the codigo
   * @return the estado documento enum
   */
  @Nullable
  public static EstadoDocumentoEnum fromCodigo(final String codigo) {

    if (codigo == null) {
      return null;
    }

    for (final EstadoDocumentoEnum ed : EstadoDocumentoEnum.values()) {
      if (ed.codigo.equalsIgnoreCase(codigo)) {
        return ed;
      }
    }
    return null;
  }

}
