package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;

/**
 * The Enum TipoTercero.
 */
public enum TipoTercero {

  /** The nacional. */
  NACIONAL("N", "label.terceros.nacional",
      TipoCuentaBancaria.NACIONAL.getValor()),

  /** The extranjero. */
  EXTRANJERO("E", "label.terceros.extranjero",
      TipoCuentaBancaria.IBAN.getValor());


  /** The valor. */
  private String valor;

  /** The label. */
  private String label;

  /** The valor api. */
  private String valorApi;

  /**
   * Instantiates a new tipo tercero.
   *
   * @param valor the valor
   * @param label the label
   * @param valorApi the valor api
   */
  TipoTercero(final String valor, final String label, final String valorApi) {
    this.valor = valor;
    this.label = label;
    this.valorApi = valorApi;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }


  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * Gets the valor api.
   *
   * @return the valor api
   */
  public String getValorApi() {
    return valorApi;
  }


  /**
   * From valor.
   *
   * @param valorApi the valor api
   * @return the tipo tercero
   */
  @Nullable
  public static String valorApiToAda(final String valorApi) {
    for (final TipoTercero e : TipoTercero.values()) {
      if (e.valorApi.equals(valorApi)) {
        return e.valor;
      }
    }
    return null;
  }
}
