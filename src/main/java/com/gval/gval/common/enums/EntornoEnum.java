package com.gval.gval.common.enums;

import java.util.Arrays;


/**
 * The Enum EntornoEnum.
 */
public enum EntornoEnum {
  /** The desarrollo. */
  DESARROLLO("DESA", "aplicacion.entorno.desarrollo"),

  /** The preproduccion. */
  PREPRODUCCION("PRE", "aplicacion.entorno.preproduccion");


  /** The valor. */
  private String valor;

  /** The label. */
  private String label;

  /**
   * Instantiates a new entorno enum.
   *
   * @param valor the valor
   * @param label the label
   */
  EntornoEnum(final String valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }



  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * From valor.
   *
   * @param valor the valor
   * @return the tipo cuidador informe social enum
   */
  public static EntornoEnum fromValor(final String valor) {
    return Arrays.asList(EntornoEnum.values()).stream()
        .filter(tc -> tc.valor.equals(valor)).findFirst().orElse(null);
  }


  /**
   * Label from valor.
   *
   * @param valor the valor
   * @return the string
   */
  public static String labelFromValor(final String valor) {
    final EntornoEnum tipoCuidadorEnum = fromValor(valor);
    return tipoCuidadorEnum == null ? null : tipoCuidadorEnum.label;
  }
}
