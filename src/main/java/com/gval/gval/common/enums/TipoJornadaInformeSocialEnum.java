package com.gval.gval.common.enums;

import java.util.Arrays;


/**
 * The Enum TipoTercero.
 */
public enum TipoJornadaInformeSocialEnum {


  /** The convivencia. */
  JORNADA_COMPLETA(1, "label.informesocial.tipojornada.completa"),

  /** The vive alternativo. */
  JORNADA_MEDIA(2, "label.informesocial.tipojornada.media"),

  /** The cuidador. */
  FIJO_DISCONTINUO(3, "label.informesocial.tipojornada.fijodiscontinuo");


  /** The valor. */
  private Integer valor;

  /** The label. */
  private String label;


  /**
   * Instantiates a new tipo cuidador enum.
   *
   * @param valor the valor
   * @param label the label
   */
  TipoJornadaInformeSocialEnum(final Integer valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public Integer getValor() {
    return valor;
  }



  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * From valor.
   *
   * @param valor the valor
   * @return the tipo jornada informe social enum
   */
  public static TipoJornadaInformeSocialEnum fromValor(final Integer valor) {
    return Arrays.asList(TipoJornadaInformeSocialEnum.values()).stream()
        .filter(tc -> tc.valor.intValue() == valor).findFirst().orElse(null);
  }


  /**
   * Label from valor.
   *
   * @param valor the valor
   * @return the string
   */
  public static String labelFromValor(final Integer valor) {
    final TipoJornadaInformeSocialEnum tipoCuidadorEnum = fromValor(valor);
    return tipoCuidadorEnum == null ? null : tipoCuidadorEnum.label;
  }
}
