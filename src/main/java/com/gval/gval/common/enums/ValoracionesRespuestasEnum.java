package com.gval.gval.common.enums;

import java.util.stream.Stream;


/**
 * The Enum ValoracionesRespuestasClaveEnum.
 */
public enum ValoracionesRespuestasEnum {


  /** The ninguno. */
  NINGUNO("NINGUNO", null),

  /** The si. */
  SI("SI", "S"),

  /** The no. */
  NO("NO", "N"),

  /** The positivo. */
  POSITIVO("POSITIVO", "P1"),

  /** The no otros. */
  NO_OTROS("NO_OTROS", "N2"),

  /** The no dependencia. */
  NO_DEPENDENCIA("NO_DEPENDENCIA", "N1"),

  /** The no aplica. */
  NO_APLICA("NO_APLICA", "NA"),

  /** The fisico. */
  FISICO("FISICO", "F"),

  /** The mental. */
  MENTAL("MENTAL", "M"),

  /** The ambos. */
  AMBOS("AMBOS", "A");


  /** The nombre. */
  public final String nombre;

  /** The pre id. */
  public final String id;

  /**
   * Instantiates a new valoraciones sub preguntas tipo enum.
   *
   * @param nombre the nombre
   * @param id the pre id
   */
  private ValoracionesRespuestasEnum(final String nombre, final String id) {
    this.nombre = nombre;
    this.id = id;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Gets the pre id.
   *
   * @return the pre id
   */
  public String getId() {
    return id;
  }

  /**
   * Stream.
   *
   * @return the stream
   */
  public static Stream<ValoracionesRespuestasEnum> stream() {
    return Stream.of(ValoracionesRespuestasEnum.values());
  }

  /**
   * Gets the by id.
   *
   * @param id the id
   * @return the by id
   */
  public static ValoracionesRespuestasEnum getById(final String id) {
    for (final ValoracionesRespuestasEnum e : values()) {
      if (e.name().equals(id)) {
        return e;
      }
    }
    return NINGUNO;
  }
}
