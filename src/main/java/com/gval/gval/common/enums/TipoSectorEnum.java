package com.gval.gval.common.enums;

/**
 * The Enum TipoSectorEnum.
 */
public enum TipoSectorEnum {

  /** The todas. */
  TODOS("T", "TODOS"),

  /** The mayores. */
  MAYORES("M", "MAYORES"),

  /** The discapacitados. */
  DISCAPACITADOS("D", "DISCAPACITADOS");

  /** The codigo. */
  private final String codigo;

  /** The descripcion. */
  private final String descripcion;

  /**
   * Instantiates a new tipo sector enum.
   *
   * @param codigo the codigo
   * @param descripcion the descripcion
   */
  private TipoSectorEnum(final String codigo, final String descripcion) {
    this.codigo = codigo;
    this.descripcion = descripcion;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }


  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

}