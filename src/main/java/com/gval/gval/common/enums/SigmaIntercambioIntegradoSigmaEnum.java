package com.gval.gval.common.enums;

import org.springframework.lang.Nullable;

public enum SigmaIntercambioIntegradoSigmaEnum {

  NO_TRATADO("N"), //
  CORRECTO("S"), //
  ERRORES("E");

  final String codigo;

  SigmaIntercambioIntegradoSigmaEnum(final String codigo) {
    this.codigo = codigo;
  }

  public String getCodigo() {
    return this.codigo;
  }

  @Nullable
  public static SigmaIntercambioIntegradoSigmaEnum getByCodigo(final String codigo) {
    for (final SigmaIntercambioIntegradoSigmaEnum iter : SigmaIntercambioIntegradoSigmaEnum
        .values()) {
      if (iter.getCodigo().equals(codigo)) {
        return iter;
      }
    }
    return null;
  }

}
