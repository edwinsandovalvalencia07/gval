package com.gval.gval.common.enums;

import com.gval.gval.common.enums.listados.InterfazListadoLabels;

import java.util.Arrays;

/**
 * The Enum SituacionNoActivoEnum.
 */
public enum SituacionInactividadEnum implements InterfazListadoLabels<Long> {

  /** The desempleo. */
  DESEMPLEO(1L, "label.informe_social.desempleo"),

  /** The desempleomas3anyos. */
  DESEMPLEOMAS3ANYOS(2L, "label.informe_social.desempleo3anyos"),

  /** The ilt. */
  ILT(3L, "label.informe_social.incapacidad_temporal"),

  /** The ilpp. */
  ILPP(4L, "label.informe_social.incapacidad_parcial"),

  /** The ilpt. */
  ILPT(5L, "label.informe_social.incapacidad_total"),

  /** The ilpa. */
  ILPA(6L, "label.informe_social.incapacidad_absoluta"),

  /** The ilpgi. */
  ILPGI(7L, "label.informe_social.incapacidad_invalidez");

  /** The valor. */
  private Long valor;

  /** The label. */
  private String label;

  /**
   * Instantiates a new situacion no activo enum.
   *
   * @param valor the valor
   * @param label the label
   */
  private SituacionInactividadEnum(final Long valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  @Override
  public Long getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  @Override
  public String getLabel() {
    return label;
  }

  /**
   * From Long.
   *
   * @param valor the text
   * @return the situacion no activo enum
   */
  public static SituacionInactividadEnum fromLong(final Long valor) {
    return Arrays.asList(SituacionInactividadEnum.values()).stream()
        .filter(sna -> sna.valor.equals(valor)).findFirst().orElse(null);
  }
}
