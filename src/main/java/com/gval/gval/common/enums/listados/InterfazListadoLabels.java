package com.gval.gval.common.enums.listados;

/**
 * The Interface TipoListado.
 *
 * @param <T> el tipo del valor del enum, generalmente Long o String.
 */
public interface InterfazListadoLabels<T> {


  /**
   * Gets the label.
   *
   * @return the label
   */
  String getLabel();

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  T getValor();

}
