package com.gval.gval.common.enums;

/**
 * The Enum SituacionLaboralEnum.
 */
public enum SituacionLaboralEnum {


  /** The jubilado pensionista. */
  // @formatter:off
  JUBILADO_PENSIONISTA(1L),

  /** The estudiante. */
  ESTUDIANTE(2L),

  /** The ama casa. */
  AMA_CASA(3L),

  /** The desempleado. */
  DESEMPLEADO(4L),

  /** The trabajador cuenta ajena. */
  TRABAJADOR_CUENTA_AJENA(5L),

  /** The autonomo. */
  AUTONOMO(6L),

  /** The otros. */
  OTROS(7L);
  // @formatter:on

  /** The value. */
  private Long value;

  /**
   * Instantiates a new autorizacion acceso datos enum.
   *
   *
   * @param value the value
   */
  SituacionLaboralEnum(final Long value) {
    this.value = value;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public Long getValue() {
    return value;
  }

}
