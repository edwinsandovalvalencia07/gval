package com.gval.gval.common.enums;

import org.springframework.lang.Nullable;

import java.util.Arrays;

/**
 * The Enum TipoRevisionSolicitudEnum.
 */
public enum TipoRevisionEnum {

  /** The oficio. */
  OFICIO(Boolean.TRUE, "1", "label.solicitud.motivo.oficio"),
  /** The de parte. */
  DE_PARTE(Boolean.FALSE, "0", "label.solicitud.motivo.departe");

  /** The valor. */
  private Boolean valor;

  /** The valor string. */
  private String valorString;

  /** The valori 18. */
  private String valori18;


  /**
   * Constructor. *
   *
   * @param valor the valor
   * @param valorString the valor string
   * @param valori18 the valori 18
   */
  private TipoRevisionEnum(final Boolean valor, final String valorString,
      final String valori18) {
    this.valor = valor;
    this.valorString = valorString;
    this.valori18 = valori18;
  }

  /**
   * Checks if is valor.
   *
   * @return the boolean
   */
  public Boolean getValor() {
    return valor;
  }

  /**
   * Gets the valor string.
   *
   * @return the valor string
   */
  public String getValorString() {
    return valorString;
  }

  /**
   * Gets the valori 18.
   *
   * @return the valori 18
   */
  public String getValori18() {
    return valori18;
  }


  /**
   * From string.
   *
   * @param s the s
   * @return the action enum
   */
  @Nullable
  public static TipoRevisionEnum fromBoolean(final Boolean s) {
    if (s == null) {
      return null;
    }

    return Arrays.stream(TipoRevisionEnum.values())
        .filter(v -> v.valor.compareTo(s) == 0).findFirst().orElseThrow(
            () -> new IllegalArgumentException("Valor desconocido: " + s));
  }

  /**
   * From string.
   *
   * @param s the s
   * @return the tipo revision solicitud enum
   */
  @Nullable
  public static TipoRevisionEnum fromString(final String s) {
    if (s == null) {
      return null;
    }
    return Arrays.stream(TipoRevisionEnum.values())
        .filter(v -> v.valorString.equals(s)).findFirst().orElseThrow(
            () -> new IllegalArgumentException("Valor desconocido: " + s));
  }

}
