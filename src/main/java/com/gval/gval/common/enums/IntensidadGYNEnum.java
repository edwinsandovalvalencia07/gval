package com.gval.gval.common.enums;

import com.gval.gval.common.enums.listados.InterfazListadoLabels;

import java.util.Arrays;

/**
 * The Enum IntensidadEnum.
 */
public enum IntensidadGYNEnum implements InterfazListadoLabels<String> {



  /** The g3n2. */
  G3N2("G3N2", "label.enum.intensidadesGYN.HORASSADG3N2"),

  /** The g3n1. */
  G3N1("G3N1", "label.enum.intensidadesGYN.HORASSADG3N1"),

  /** The g3n0. */
  G3N0("G3N0", "label.enum.intensidadesGYN.HORASSADG3N0"),

  /** The g2n2. */
  G2N2("G2N2", "label.enum.intensidadesGYN.HORASSADG2N2"),

  /** The g2n1. */
  G2N1("G2N1", "label.enum.intensidadesGYN.HORASSADG2N1"),

  /** The g2n0. */
  G2N0("G2N0", "label.enum.intensidadesGYN.HORASSADG2N0"),

  /** The g1n2. */
  G1N2("G1N2", "label.enum.intensidadesGYN.HORASSADG1N2"),

  /** The g1n1. */
  G1N1("G1N1", "label.enum.intensidadesGYN.HORASSADG1N1"),

  /** The g1n0. */
  G1N0("G1N0", "label.enum.intensidadesGYN.HORASSADG1N0"),

  /** The compl. */
  COMPL("COMPL", "label.enum.intensidadesGYN.HORASSADCOMPL");


  /** The valor. */
  private String valor;

  /** The label. */
  private String label;



  /**
   * Instantiates a new tipo cuidador.
   *
   * @param valor the valor
   * @param label the label
   */
  IntensidadGYNEnum(final String valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  @Override
  public String getValor() {
    return valor;
  }



  /**
   * Gets the label.
   *
   * @return the label
   */
  @Override
  public String getLabel() {
    return label;
  }

  /**
   * From valor.
   *
   * @param valor the valor
   * @return the intensidad enum
   */
  public static IntensidadGYNEnum fromValor(final String valor) {
    return Arrays.asList(IntensidadGYNEnum.values()).stream()
        .filter(tc -> tc.valor.equals(valor)).findFirst().orElse(null);
  }

  /**
   * Label from valor.
   *
   * @param valor the valor
   * @return the string
   */
  public static String labelFromValor(final String valor) {
    final IntensidadGYNEnum intensidadEnum = fromValor(valor);
    return intensidadEnum == null ? null : intensidadEnum.label;
  }
}
