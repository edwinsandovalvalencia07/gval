package com.gval.gval.common.enums;

import java.util.Arrays;

/**
 * The Enum OpinionPersonaSituacionCentroEnum.
 */
public enum OpinionPersonaSituacionCentroEnum {

  /** The vacio. */
  VACIO("", " "),

  /** The excelente. */
  EXCELENTE("E", "label.informe_social.excelente"),

  /** The buena. */
  BUENA("B", "label.informe_social.buena"),

  /** The regular. */
  REGULAR("R", "label.informe_social.regular"),

  /** The mala. */
  MALA("M", "label.informe_social.mala");

  /** The valor. */
  private String valor;

  /** The label. */
  private String label;

  /**
   * Instantiates a new opinion persona situacion centro enum.
   *
   * @param valor the valor
   * @param label the label
   */
  private OpinionPersonaSituacionCentroEnum(final String valor,
      final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * From integer.
   *
   * @param text the text
   * @return the opinion persona situacion centro enum
   */
  public static OpinionPersonaSituacionCentroEnum fromString(
      final String text) {
    return Arrays.asList(OpinionPersonaSituacionCentroEnum.values()).stream()
        .filter(opsc -> opsc.valor.equals(text)).findFirst().orElse(null);
  }

}
