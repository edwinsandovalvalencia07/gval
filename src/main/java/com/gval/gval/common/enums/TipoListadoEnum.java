package com.gval.gval.common.enums;

import java.util.Arrays;

// TODO: Auto-generated Javadoc
/**
 * The Enum TipoListadoEnum.
 */
public enum TipoListadoEnum {

	/** The asignar solicitud. */
	ASIGNAR_SOLICITUD("ASIGSOLI", "label.titulo.asignar_solicitud"),

	/** The acuses generados. */
	ACUSES_GENERADOS("LACUSGEN", "label.titulo.listado_acuses_generados"),

	/** The centros. */
	CENTROS("LCENTROS", "label.direccion.centros"),

	/** The propuestas pia. */
	PROPUESTAS_PIA("LPROPPIA", "label.titulo.asignar_propuesta_pia"),

	/** The solicitudes. */
	SOLICITUDES("LSOLICIT", "label.titulo.buscar_solicitudes"),

	/** The subsanaciones pendientes. */
	SUBSANACIONES_PENDIENTES("LSUBSPEN", "label.titulo.subsanaciones_pendientes"),

	/** The subsanaciones tesoreria. */
	SUBSANACIONES_TESORERIA("LREQTES", "label.subsanacion_tesoreria.devolucion_tesoreria"),

	/** The subsanaciones tesoreria documento aportado. */
	SUBSANACIONES_TESORERIA_DOCUMENTO_APORTADO("LSUBDA", "label.titulo.subsanaciones_documento_aportado"),

	/** The expedientes. */
	EXPEDIENTES("LXPEDIEN", "label.titulo.consulta_expedientes"),

	/** The validar documentos. */
	VALIDAR_DOCUMENTOS("VPNPAYTO", "label.titulo.documentacion_pdte_verificar"),

	/** The cambio cuidador pendientes verificar. */
	CAMBIO_CUIDADOR_PENDIENTES_VERIFICAR("VERFCCUI", "label.titulo.verifica_cambio_cuidador"),

	/** The preferencias pendientes verificar. */
	PREFERENCIAS_PENDIENTES_VERIFICAR("VERFPREF", "label.titulo.verifica_preferencias"),

	/** The terceros pendientes verificar. */
	TERCEROS_PENDIENTES_VERIFICAR("VERFTERC", "label.titulo.verifica_terceros"),

	/** The cuidadores. */
	CUIDADORES("LCUIDAD", "label.titulo.cuidadores"),

	/** The listados diarios. */
	LISTADOS_DIARIOS("LDIARIOS", "label.titulo.home_listados_diarios"),

	/** The listados cambios asistente personal. */
	LISTADOS_CAMBIOS_ASISTENTE_PERSONAL("LCAMBAP", "label.titulo.home_listados_cambios_asistente_personal"),

	/** The listados consulta 012. */
	LISTADOS_CONSULTA_012("LCONS012", "label.titulo.home_listados_consulta012"),

	/** The listados tramite telematico. */
	LISTADOS_TRAMITE_TELEMATICO("LTRATELE", "label.titulo.home_tramite_telematico"),

	/** The citar solicitudes. */
	CITAR_SOLICITUDES("CITAVAL", "label.titulo.citacion"),

	/** The informes sociales pendientes. */
	INFORMES_SOCIALES_PENDIENTES("ISTPENDT", "label.informe_social.informes_sociales_pendientes"),

	/** The solicitudes extendidas. */
	SOLICITUDES_EXT("LSOLEXT", "label.titulo.buscar_solicitudes_ext"),

	/** The expedientes con PIA. */
	EXPEDIENTES_PIA("LEXPPIA", "label.expedientes_pia.expedientes_pia"),

	/** The expedientes con PIA. */
	INFORMES_TECNICOS_DESFAVORABLE("LITDESF", "label.informes_tecnicos_desfavorables"),

	/** The Listados Espera Imserso Pia. */
	LISTADOS_ESPERA_IMSERSO_PIA("LESPIMPI", "label.titulo.home_listados_espera"),

	/** The Listados Rechazos Menor Fecha Grado. */
	LISTADOS_RECHAZOS_MENOR_FECHA_GRADO("LRMENGRA", "label.listado-enum.rechazos_grado"),

	/** The Listados Nefis. */
	LISTADOS_NEFIS("NEFISPET", "label.titulo.menu_listados_nefis"),

	/** The Listados Ingresos Pendientes. */
	LISTADOS_INGRESOS_PENDIENTES("INGPEND", "label.titulo.ingresos_pendientes"),

	/** The Listados Rechazos Menor Fecha SOLICITUD. */
	LISTADOS_RECHAZOS_MENOR_FECHA_SOLICITUD("LRMENSOL", "label.listado-enum.rechazos_solicitud"),

	/** The Listados Rechazos Menor Fecha PRESTACIONES. */
	LISTADOS_RECHAZOS_MENOR_FECHA_PRESTACION("LRMENPRE", "label.listado-enum.rechazos_prestaciones"),

	/** The informes sociales pendientes. */
	CONSULTA_VALORACIONES_SIGMA("VALSIGMA", "label.titulo.home_valoraciones_sigma"),

	/** The listados resolucion masiva. */
	LISTADOS_RESOLUCION_MASIVA("LTELMAS", "label.titulo.home_resolucion_masiva"),

	/** The listado documentos contables. */
	LISTADO_DOCUMENTOS_CONTABLES("LDOCCONT", "label.titulo.home_documentos_contables"),

	/** The listado configuracion nefis. */
	LISTADO_CONFIGURACION_NEFIS("CONFNEF", "label.titulo.home_configuracion_nefis"),

	/** The listado reimpresion. */
	LISTADO_REIMPRESION("LDREIM", "label.titulo.menu.impresion"),

  /** The listado estudios pendientes. */
  LISTADO_ESTUDIOS_PENDIENTES("LTESTPEN", "label.titulo.consulta_estudios_pendientes"),

  /** The listado acta masiva. */
  LISTADO_ACTA_MASIVA("LTACTAM", "label.titulo.consulta_acta_masiva");

	/** The valor. */
	private final String valor;

	/** The label. */
	private final String label;

	/**
	 * Instantiates a new opinion persona situacion centro enum.
	 *
	 * @param valor the valor
	 * @param label the label
	 */
	private TipoListadoEnum(final String valor, final String label) {
		this.valor = valor;
		this.label = label;
	}

	/**
	 * Gets the valor.
	 *
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * Gets the label.
	 *
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * From string.
	 *
	 * @param text the text
	 * @return the tipo informe social listado enum
	 */
	public static TipoListadoEnum fromString(final String text) {
		return Arrays.asList(TipoListadoEnum.values()).stream().filter(opsc -> opsc.valor.equals(text)).findFirst()
				.orElse(null);
	}

}
