package com.gval.gval.common.enums;

import java.util.Arrays;

/**
 * The Enum TipoConvivencia.
 */
public enum TipoConvivenciaEnum {

  /** The vive sola. */
  VIVE_SOLA("S"),

  /** The convive. */
  CONVIVE("C"),

  /** The alterna domicilio. */
  ALTERNA_DOMICILIO("A");

  /** The tipo convivencia. */
  private String convivencia;

  /**
   * Instantiates a new tipo convivencia.
   *
   * @param valor the valor
   * @param tipoConvivencia the tipo convivencia
   */
  private TipoConvivenciaEnum(final String convivencia) {
    this.convivencia = convivencia;
  }

  /**
   * Gets the tipo convivencia.
   *
   * @return the tipo convivencia
   */
  public String getConvivencia() {
    return convivencia;
  }

  /**
   * From integer.
   *
   * @param text the text
   * @return the tipo convivencia
   */
  public static TipoConvivenciaEnum fromString(final String text) {
    return Arrays.asList(TipoConvivenciaEnum.values()).stream()
        .filter(tc -> tc.convivencia.equals(text)).findFirst().orElse(null);
  }
}
