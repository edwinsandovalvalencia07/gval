package com.gval.gval.common.enums;

import java.util.Arrays;
import java.util.List;

import jakarta.annotation.Nullable;

/**
 * The Enum TipoIdentificadorEnum.
 */
public enum TipoIdentificadorEnum {

  /** The nif. */
  NIF("NIF"),

  /** The nie. */
  NIE("NIE"),

  /** The nif ficticio. */
  NIF_FICTICIO("NIFF"),

  /** The pasaporte. */
  PASAPORTE("PASS"),
  
  /** The dni pai. */
  DNI_PAI("DNI");



  /** The value. */
  private String value;

  /**
   * Instantiates a new tipo estudio enum.
   *
   * @param value the value
   */
  private TipoIdentificadorEnum(final String value) {
    this.value = value;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public String getValue() {
    return value;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return value;
  }


  /**
   * From string.
   *
   * @param text the text
   * @return the tipo estudio enum
   */
  @Nullable
  public static TipoIdentificadorEnum fromString(final String text) {

    if (text == null) {
      return null;
    }

    for (final TipoIdentificadorEnum te : TipoIdentificadorEnum.values()) {
      if (te.value.equalsIgnoreCase(text)) {
        return te;
      }
    }
    return null;
  }

  /**
   * Es nif nie.
   *
   * @param tipoIdentificador the tipo identificador
   * @return true, if successful
   */
  public static boolean esNifNie(String tipoIdentificador) {
    final List<String> tipoIdentificadoresValidar =
        Arrays.asList(NIE.getValue(), NIF.getValue());
    return tipoIdentificadoresValidar.contains(tipoIdentificador);
  }

}
