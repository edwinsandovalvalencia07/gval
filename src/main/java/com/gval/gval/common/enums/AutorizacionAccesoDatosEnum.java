package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;
import org.apache.commons.lang3.StringUtils;

//import jakarta.annotation.Nullable;

/**
 * The Enum AutorizacionAccesoDatosEnum.
 */
public enum AutorizacionAccesoDatosEnum {

  /** The economicos. */
  ECONOMICOS("ECO"),
  /** The sanitarios. */
  SANITARIOS("SAN"),
  /** The residencia. */
  RESIDENCIA("RES"),
  /** The identidad. */
  IDENTIDAD("IDE"),
  /** The otras. */
  OTRAS("OTR"),
  /** The nacimiento. */
  NACIMIENTO("NAC"),
  /** The pensiones. */
  PENSIONES("PEN"),
  /** The representantes. */
  REPRESENTANTES("REP");

  /** The value. */
  private String value;

  /**
   * Instantiates a new autorizacion acceso datos enum.
   *
   *
   * @param value the value
   */
  AutorizacionAccesoDatosEnum(final String value) {
    this.value = value;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public String getValue() {
    return value;
  }

  /**
   * From value.
   *
   * @param value the value
   * @return the autorizacion acceso datos enum
   */
  @Nullable
  public static AutorizacionAccesoDatosEnum fromValue(final String value) {
    final AutorizacionAccesoDatosEnum[] enums = values();
    for (final AutorizacionAccesoDatosEnum se : enums) {
      if (StringUtils.equals(value, se.value)) {
        return se;
      }
    }
    return null;
  }
}
