package com.gval.gval.common.enums;


import org.springframework.lang.Nullable;

public enum SigmaIntercambioIntegradoAdaEnum {

  NO_TRATADO("N"), //
  CORRECTO("S"), //
  ERRORES("E");

  final String codigo;

  SigmaIntercambioIntegradoAdaEnum(final String codigo) {
    this.codigo = codigo;
  }

  public String getCodigo() {
    return this.codigo;
  }

  @Nullable
  public static SigmaIntercambioIntegradoAdaEnum getByCodigo(final String codigo) {
    for (final SigmaIntercambioIntegradoAdaEnum iter : SigmaIntercambioIntegradoAdaEnum
        .values()) {
      if (iter.getCodigo().equals(codigo)) {
        return iter;
      }
    }
    return null;
  }

}
