package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;

// TODO: Auto-generated Javadoc
/**
 * The Enum TipoEstudioEnum.
 */
public enum TipoEstudioEnum {

  /** The urgencia. */
  URGENCIA("U", "label.estudio.comision.tipo_estudio.urgencia"),

  /** The dictamen. */
  DICTAMEN("D", "label.estudio.comision.tipo_estudio.dictamen_sol_ini"),

  /** The revision. */
  REVISION("R", "label.estudio.comision.tipo_estudio.revision"),

  /** The recurso. */
  RECURSO("A", "label.estudio.comision.tipo_estudio.recurso"),

  /** The homologacion. */
  HOMOLOGACION("H", "label.estudio.comision.tipo_estudio.homologacion_sol_ini"),

  /** The recurso denegacion. */
  RECURSO_DENEGACION("E", "label.estudio.comision.tipo_estudio.recurso_denegacion");

  /** The value. */
  private final String value;

  /** The label. */
  private final String label;

  /**
   * Instantiates a new tipo estudio enum.
   *
   * @param value the value
   * @param label the label
   */
  private TipoEstudioEnum(final String value, final String label) {
    this.value = value;
    this.label = label;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public String getValue() {
    return value;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return value;
  }


  /**
   * From string.
   *
   * @param text the text
   * @return the tipo estudio enum
   */
  @Nullable
  public static TipoEstudioEnum fromString(final String text) {

    if (text == null) {
      return null;
    }

    for (final TipoEstudioEnum te : TipoEstudioEnum.values()) {
      if (te.value.equalsIgnoreCase(text)) {
        return te;
      }
    }
    return null;
  }

}
