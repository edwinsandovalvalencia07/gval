package com.gval.gval.common.enums;

/**
 * The Enum MotivoCancelacionEnum.
 */
public enum MotivoCancelacionEnum {

  /** The nuevas preferencias. */
  NUEVAS_PREFERENCIAS(1L, "rPreferencias"),

  /** The dictamen. */
  REVISION(2L, "rRevision");

  /** The codigo. */
  private Long codigo;

  /** The descripcion. */
  private String descripcion;

  /**
   * Instantiates a new motivo cancelacion enum.
   *
   * @param codigo the codigo
   * @param descripcion the descripcion
   */
  MotivoCancelacionEnum(final Long codigo, final String descripcion) {
    this.codigo = codigo;
    this.descripcion = descripcion;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public Long getCodigo() {
    return this.codigo;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return this.descripcion;
  }

}
