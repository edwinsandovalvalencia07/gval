package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;


/**
 * The Enum TipoCentroPrescripcionEnum.
 */
public enum TipoCentroPrescripcionEnum {


  /** The enfermedad mental. */
  ENFERMEDAD_MENTAL("MEN", 0L),

  /** The discapacidad intelectual. */
  DISCAPACIDAD_INTELECTUAL("INT", 1L),

  /** The discapacidad fisica. */
  DISCAPACIDAD_FISICA("FIS", 2L),

  /** The personas mayores. */
  PERSONAS_MAYORES("MAY", 3L);

  /** The codigo. */
  private String codigo;

  /** The valor. */
  private Long valor;


  /**
   * Instantiates a new tipo centro prescripcion enum.
   *
   * @param codigo the codigo
   * @param valor the valor
   */
  private TipoCentroPrescripcionEnum(final String codigo, final Long valor) {
    this.codigo = codigo;
    this.valor = valor;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public Long getValor() {
    return valor;
  }


  /**
   * From codigo.
   *
   * @param codigo the codigo
   * @return the tipo centro prescripcion enum
   */
  @Nullable
  public static TipoCentroPrescripcionEnum fromCodigo(final String codigo) {

    if (codigo == null) {
      return null;
    }

    for (final TipoCentroPrescripcionEnum tipo : TipoCentroPrescripcionEnum
        .values()) {
      if (tipo.codigo.equalsIgnoreCase(codigo)) {
        return tipo;
      }
    }
    return null;
  }
}
