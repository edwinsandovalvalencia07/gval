package com.gval.gval.common.enums;


/**
 * The Enum EstadoPreferenciaEnum.
 */
public enum EstadoPreferenciaEnum {

  // @formatter:off

  /** The antigua. */
  ANTIGUA(null),

  /** The abierta. */
  ABIERTA(Boolean.FALSE),

  /** The cerrada. */
  CERRADA(Boolean.TRUE);
  //@formatter:on

  /** The codigo. */
  private Boolean codigo;


  /**
   * Instantiates a new estado preferencia enum.
   *
   * @param codigo the codigo
   */
  EstadoPreferenciaEnum(final Boolean codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public Boolean getCodigo() {
    return codigo;
  }

}
