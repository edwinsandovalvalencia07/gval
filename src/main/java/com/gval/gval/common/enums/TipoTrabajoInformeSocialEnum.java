package com.gval.gval.common.enums;

import java.util.Arrays;


/**
 * The Enum TipoTrabajoInformeSocialEnum.
 */
public enum TipoTrabajoInformeSocialEnum {

  /** The cuenta ajena. */
  CUENTA_AJENA(1, "label.informesocial.tipotrabajo.cuentaajena"),

  /** The cuenta propia. */
  CUENTA_PROPIA(2, "label.informesocial.tipotrabajo.cuentapropia");


  /** The valor. */
  private Integer valor;

  /** The label. */
  private String label;


  /**
   * Instantiates a new tipo trabajo informe social enum.
   *
   * @param valor the valor
   * @param label the label
   */
  TipoTrabajoInformeSocialEnum(final Integer valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public Integer getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * From valor.
   *
   * @param valor the valor
   * @return the tipo trabajo informe social enum
   */
  public static TipoTrabajoInformeSocialEnum fromValor(final Integer valor) {
    return Arrays.asList(TipoTrabajoInformeSocialEnum.values()).stream()
        .filter(tc -> tc.valor.intValue() == valor).findFirst().orElse(null);
  }

  /**
   * Label from valor.
   *
   * @param valor the valor
   * @return the string
   */
  public static String labelFromValor(final Integer valor) {
    final TipoTrabajoInformeSocialEnum tipoCuidadorEnum = fromValor(valor);
    return tipoCuidadorEnum == null ? null : tipoCuidadorEnum.label;
  }
}
