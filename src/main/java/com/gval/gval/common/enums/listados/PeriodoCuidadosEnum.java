package com.gval.gval.common.enums.listados;

/**
 * The Enum PeriodoCuidadosEnum.
 */
public enum PeriodoCuidadosEnum implements InterfazListadoLabels<Long> {


  /** The minimo. */
  TODO_ANYO(0L, "label.listado-enum.periodo-cuidados.todo-anyo"),

  /** The parcial. */
  POR_MESES(1L, "label.listado-enum.periodo-cuidados.por-meses");


  /** The valor. */
  private Long valor;

  /** The label. */
  private String label;

  /**
   * Instantiates a new tipo cuidador.
   *
   * @param valor the valor
   * @param label the label
   */
  PeriodoCuidadosEnum(final Long valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  @Override
  public Long getValor() {
    return valor;
  }



  /**
   * Gets the label.
   *
   * @return the label
   */
  @Override
  public String getLabel() {
    return label;
  }
}
