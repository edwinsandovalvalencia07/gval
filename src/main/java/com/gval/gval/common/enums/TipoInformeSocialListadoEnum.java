package com.gval.gval.common.enums;

import java.util.Arrays;



/**
 * The Enum TipoInformeSocialListadoEnum.
 */
public enum TipoInformeSocialListadoEnum {

  /** The informes sociales pendientes. */
  INFORMES_SOCIALES_PENDIENTES("ISTPENDT",
      "label.informe_social.informes_sociales_pendientes"),

  /** The informes sociales completados. */
  INFORMES_SOCIALES_COMPLETADOS("ISTCOMPL",
      "label.informe_social.informes_sociales_completados"),

  /** The informes sociales seguimiento. */
  INFORMES_SOCIALES_SEGUIMIENTO("ITSEGUIM",
      "label.informe_social.informes_sociales_seguimiento"),

  /** The informes sociales nuevas preferencias. */
  INFORMES_SOCIALES_NUEVAS_PREFERENCIAS("ITNPREFE",
      "label.informe_social.informes_sociales_preferencias"),

  /** The informes sociales cambio cuidador. */
  INFORMES_SOCIALES_CAMBIO_CUIDADOR("ITCUIDAD",
      "label.informe_social.informes_sociales_ccuidador"),

  /** The informes sociales otros. */
  INFORMES_SOCIALES_OTROS("ITOTROS",
      "label.informe_social.informes_sociales_otros"),

  /** The informes sociales cambio asistente. */
  INFORMES_SOCIALES_CAMBIO_ASISTENTE("ITASIST",
      "label.informe_social.informes_sociales_casistente"),

  /** The informes sociales revision horas. */
  INFORMES_SOCIALES_REVISION_HORAS("ITAHORAS",
      "label.informe_social.informes_sociales_aumhoras");


  /** The valor. */
  private String valor;

  /** The label. */
  private String label;

  /**
   * Instantiates a new opinion persona situacion centro enum.
   *
   * @param valor the valor
   * @param label the label
   */
  private TipoInformeSocialListadoEnum(final String valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }


  /**
   * From string.
   *
   * @param text the text
   * @return the tipo informe social listado enum
   */
  public static TipoInformeSocialListadoEnum fromString(final String text) {
    return Arrays.asList(TipoInformeSocialListadoEnum.values()).stream()
        .filter(opsc -> opsc.valor.equals(text)).findFirst().orElse(null);
  }

}
