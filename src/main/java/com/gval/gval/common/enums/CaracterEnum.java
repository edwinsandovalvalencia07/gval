package com.gval.gval.common.enums;

import java.util.Arrays;

/**
 * The Enum CaracterEnum.
 */
public enum CaracterEnum {

  /** The temporal. */
  TEMPORAL(0, "label.estudio.comision.caracter.temporal"),
  /** The permanente. */
  PERMANENTE(1, "label.estudio.comision.caracter.permanente");

  /** The value. */
  private final int value;

  /** The label. */
  private final String label;

  /**
   * Instantiates a new caracter enum.
   *
   * @param value the value
   * @param label the label
   */
  private CaracterEnum(final int value, final String label) {
    this.value = value;
    this.label = label;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public int getValue() {
    return value;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * From valor.
   *
   * @param valor the valor
   * @return the caracter enum
   */
  public static CaracterEnum fromValor(final int valor) {
    return Arrays.asList(CaracterEnum.values()).stream()
        .filter(tc -> tc.value == valor).findFirst().orElse(null);
  }



}
