package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;


/**
 * The Enum PantallaEnum.
 *
 * @return the on
 */
public enum PantallaEnum {
  // @formatter:off
  /** The datos expediente. */
  DATOS_EXPEDIENTE("datosExpediente"),

  /** The solicitudes. */
  SOLICITUDES("solicitudes"),

  /** The informes sociales. */
  INFORMES_SOCIALES("informesSociales"),

  /** The resolucion expediente. */
  RESOLUCION_EXPEDIENTE("listResoluciones"),

  /** The incidencias. */
  INCIDENCIAS("incidencias"),

  /** The solicitud solicitud. */
  SOLICITUD_SOL("solicitud"),

  /** The informes sociales solicitud *. */
  INFORME_SOCIAL_SOL("informeSocial"),

  /** The incidencias solicitud *. */
  INCIDENCIAS_SOL("incidenciasSolicitud"),

  /** The datos personales *. */
  DATOS_PERSONALES("datosPersonales"),

  /** The datos discapacidad *. */
  DATOS_DISCAPACIDAD("datosDiscapacidad"),

  /** The representante legal. */
  REPRESENTANTE_LEGAL("representanteLegal"),

  /** The unidad familiar *. */
  UNIDAD_FAMILIAR("unidadFamiliar"),

  /** The direccion adicional *. */
  DIRECCION_ADICIONAL("direccionAdicional"),

  /** The preferencias *. */
  PREFERENCIAS("preferencias"),

  /** The Traslados. */
  TRASLADOS("traslados"),

  /** The historico solicitud. */
  HISTORICO_SOLICITUD("historicoSolicitud"),

  /** The documentos. */
  DOCUMENTOS("documentos"),

  /** The documentos solicitud. */
  DOCUMENTOS_SOLICITUD("documentosSolicitud"),

  /** The valoraciones. */
  VALORACIONES("valoraciones"),

  /** The estudios. */
  ESTUDIOS("estudios"),

  /** The valoracion cuidados entorno familiar. */
  VALORACION_CUIDADOS_ENTORNO_FAMILIAR("valoracionCuidados"),

  /** The valoracion asistencia personal. */
  VALORACION_ASISTENCIA_PERSONAL("valoracionAsistenciaPersonal"),

  /** The mantenimiento cuidador. */
  MANTENIMIENTO_CUIDADOR("mantenimientoCuidador"),

  /** The mantenimiento asistente personal. */
  MANTENIMIENTO_ASISTENTE_PERSONAL("mantenimientoAsistente"),

  /** The terceros. */
  TERCEROS("terceros"),

  /** The resoluciones. */
  RESOLUCIONES("resoluciones"),

  /** The resoluciones solicitud. */
  RESOLUCIONES_SOLICITUD("resolucionesSolicitud"),

  /** The viejas preferencias. */
  VIEJAS_PREFERENCIAS("viejasPreferencias"),

  /** The preferencias cerradas. */
  PREFERENCIAS_CERRADAS("preferenciasCerradas"),

  /** The propuesta pia. */
  PROPUESTA_PIA("propuestaPIA"),

  /** The informe social pop up. */
  INFORME_SOCIAL_POP_UP("informeSocialPopUp"),
  
  /** The comision. */
  // @formatter:on
  COMISION("comision");
  /** The value. */
  private final String value;

  /**
   * Instantiates a new pantalla enum.
   *
   * @param value the value
   */
  private PantallaEnum(final String value) {
    this.value = value;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  // metodo que devuelve el nombre de la pantalla
  public String getValue() {
    return value;
  }

  /**
   * From value.
   *
   * @param value the value
   * @return the pantalla enum
   */
  @Nullable
  public static PantallaEnum fromValue(final String value) {
    for (final PantallaEnum e : values()) {
      if (e.value.equals(value)) {
        return e;
      }
    }
    return null;
  }
}
