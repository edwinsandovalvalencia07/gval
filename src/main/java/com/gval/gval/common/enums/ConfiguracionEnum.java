package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;

// TODO: Auto-generated Javadoc
/**
 * The Enum ConfiguracionEnum.
 */
public enum ConfiguracionEnum {

  // @formatter:off

  /** The gde habilitado. */
  GDE_HABILITADO("GDE_HABILITADO"),

  /** The disco habilitado. */
  DISCO_HABILITADO("DISCO_HABILITADO"),

  /** ********************************************. */
  /******************* TUTELA **********************/
  /***********************************************/

  /** The comision tutelas valencia. */
  COMISION_TUTELAS_VALENCIA("COMISION_TUTELAS_VALENCIA"),

  /** The comision tutelas alicante. */
  COMISION_TUTELAS_ALICANTE("COMISION_TUTELAS_ALICANTE"),

  /** The comision tutelas castellon. */
  COMISION_TUTELAS_CASTELLON("COMISION_TUTELAS_CASTELLON"),

  /** The comision tutelas ivass. */
  COMISION_TUTELAS_IVASS("COMISION_TUTELAS_IVASS"),

  /** The servicio infancia alicante. */
  SERVICIO_INFANCIA_ALICANTE("SERVICIO_INFANCIA_ALICANTE"),

  /** The servicio infancia castellon. */
  SERVICIO_INFANCIA_CASTELLON("SERVICIO_INFANCIA_CASTELLON"),

  /** The servicio infancia ivass. */
  SERVICIO_INFANCIA_IVASS("SERVICIO_INFANCIA_IVASS"),

  /** The servicio infancia valencia. */
  SERVICIO_INFANCIA_VALENCIA("SERVICIO_INFANCIA_VALENCIA"),

  /** ********************************************. */
  /********* SINCRONIZACION_FICHEROS ***************/
  /***********************************************/

  /** The sincronizacion activa. */
  SINCRONIZACION_ACTIVA("SINCRONIZACION_ACTIVA"),

  /** The fecha desde sincronizacion ficheros. */
  FECHA_DESDE_SINCRONIZACION_FICHEROS("FECHA_DESDE_SINCRONIZACION_FICHEROS"),

  /** The hora hasta sincronizacion ficheros. */
  HORA_HASTA_SINCRONIZACION_FICHEROS("HORA_HASTA_SINCRONIZACION_FICHEROS"),

  /** ********************************************. */
  /*************** OTRAS RESOLUCIONES ************/
  /***********************************************/
  RES_CADUCIDAD("RCAD"),

  /** The res renuncia voluntaria. */
  RES_RENUNCIA_VOLUNTARIA("RRENVOL"),

  /** The res reintegro. */
  RES_REINTEGRO("RREI"),

  /** The res archivo. */
  RES_ARCHIVO("RARCH"),

  /** The res archivo no acepta llamada. */
  RES_ARCHIVO_NO_ACEPTA_LLAMADA("RANASR"),

  /** The res archivo no acepta cdia. */
  RES_ARCHIVO_NO_ACEPTA_CDIA("RANASD"),

  /** The res renuncia voluntaria pia. */
  RES_RENUNCIA_VOLUNTARIA_PIA("RRENVOLPIA"),

  /** The avis desis solic. */
  AVIS_DESIS_SOLIC("AVDS"),

  /** The ren desistimiento interesado. */
  REN_DESISTIMIENTO_INTERESADO("ARRENVOL"),

  /** The res revocacion sad. */
  RES_REVOCACION_SAD("RESPIARSAD"),

  /** ********************************************. */
  /****** PLANTILLAS PARA LOS DOCUMENTOS **********/
  /********* QUE NO TIENEN PLANTILLA ***************/
  /***********************************************/

  PLANTILLA_PEND("PLANTILLA_PEND"),

  /** ********************************************. */
  /*************** OTRAS NOTIFICACIONES ************/
  /***********************************************/
  NOT_CADUCIDAD("NCAD"),

  /** The not desistimiento. */
  NOT_DESISTIMIENTO("NDIS"),

  /** The not renuncia voluntaria. */
  NOT_RENUNCIA_VOLUNTARIA("NRENVOL"),

  /** The not reintegro. */
  NOT_REINTEGRO("NREI"),

  /** The not archivo. */
  NOT_ARCHIVO("NARCH"),

  /** The res arc fall pia. */
  RES_ARC_FALL_PIA("RARCFALLPIA"),

  /** The res arc fall npia. */
  RES_ARC_FALL_NPIA("RARCFALLNPIA"),

  /** The not arc fall. */
  NOT_ARC_FALL("NARCFALL"),

  /** The res rev arc fall. */
  RES_REV_ARC_FALL("RREVARCFALL"),

  /** The not rev arc fall. */
  NOT_REV_ARC_FALL("NREVARCFALL"),

  /** The not revocacion sad. */
  NOT_REVOCACION_SAD("NOTPIARSAD"),

  /** The res arc no servicio residencia. */
  RES_ARC_NO_SERVICIO_RESIDENCIA("RARCNOSERVRES"),
  /** The not res arc no servicio residencia. */
  NOT_RES_ARC_NO_SERVICIO_RESIDENCIA("NRARCNOSERVRES"),
  /** The res arc no servicio cdia. */
  RES_ARC_NO_SERVICIO_CDIA("RARCNOSERVRES"),
  /** The not res arc no servicio cdia. */
  NOT_RES_ARC_NO_SERVICIO_CDIA("NRARCNOSERVRES"),

  /** ********************************************. */
  /***************** RESOLUCIÓN ******************/
  /***********************************************/
  RES_ARC_PVS_SAD("RACPVSSAD"),

  /** ********************************************. */
  /**************** NOTIFICACIÓN *****************/
  /***********************************************/
  NOT_ARC_PVS_SAD("RACPVSSAD"),


  /** The cad pia sin contrato. */
  CAD_PIA_SIN_CONTRATO("RCSC"),

  /** ********************************************. */
  /********** RESOLUCIÓN DESESTIMATORIA **********/
  /**************** REVISIÓN PIA *****************/
  /***********************************************/
  RES_DESEST_REV_PIA("RDRP"),

  /** ********************************************. */
  /********* NOTIFICACIÓN DESESTIMATORIA *********/
  /**************** REVISIÓN PIA *****************/
  /***********************************************/
  NOT_DESEST_REV_PIA("NDRP"),

  /** ********************************************. */
  /********** RESOLUCIÓN Y NOTIFICACIÓN **********/
  /*********** SILENCIO ADMINISTRATIVO ***********/
  /***********************************************/
  RES_SILENCIO_ADMINISTRATIVO("RSIL"),

  /** The not silencio administrativo. */
  NOT_SILENCIO_ADMINISTRATIVO("NSIL"),

  /** ********************************************. */
  /********** RESOLUCIÓN Y NOTIFICACIÓN **********/
  /********** CAMBIO DE CNP EN UNA PIA ***********/
  /***********************************************/
  RES_CAMBIO_CNP("RCNP"),
  /** The not cambio cnp. */
  NOT_CAMBIO_CNP("NCNP"),

  /** ********************************************. */
  /***************** RESOLUCIÓN ******************/
  /************ DE EXTINCIÓN DE PIA **************/
  /***********************************************/
  RESOLUCION_EXTINCION_PIA("REP"),

  /** ********************************************. */
  /*************** NOTIFICACIÓN ******************/
  /************ DE EXTINCIÓN DE PIA **************/
  /***********************************************/
  NOTIFICACION_EXTINCION_PIA("NEP"),

  /** ********************************************. */
  /**************** RESOLUCIONES *****************/
  /*************** DE INADMISIÓN *****************/
  /***********************************************/
  RES_INADMISION("RINAD"),
  /** The not inadmision. */
  NOT_INADMISION("NINAD"),
  /** The res inadmision falta legitimacion. */
  RES_INADMISION_FALTA_LEGITIMACION("RIFL"),

  /** ********************************************. */
  /**************** RESOLUCIONES *****************/
  /******** DE REVISIÓN DE INADMISIÓN ************/
  /***********************************************/
  RES_REVISION_INADMISION("RREVINAD"),

  /** ********************************************. */
  /***************** HEREDEROS *******************/
  /***********************************************/
  RHERDIF("RPDH"),
  /** The nherdif. */
  NHERDIF("NPDH"),

  /** ********************************************. */
  /***************** HEREDEROS *******************/
  /**************** PAGO UNICO *******************/
  /***********************************************/
  RESOLUCION_PAGO_UNICO_HEREDEROS("RPUH"),
  /** The notificacion pago unico herederos. */
  NOTIFICACION_PAGO_UNICO_HEREDEROS("NPUH"),

  /** ********************************************. */
  /***************** APERTURA ********************/
  /************* REVISIÓN OFICIO *****************/
  /***********************************************/
  NOT_ARO("NOTARO"),
  /** The res aro. */
  RES_ARO("RESARO"),

  /** ********************************************. */
  /************** REVISIÓN ABONO *****************/
  /************* COMPLETO DIFERIDO ***************/
  /***********************************************/
  NOT_REV_ABONO_DIFERIDO("NOTABONODIF"),
  /** The res rev abono diferido. */
  RES_REV_ABONO_DIFERIDO("RESABONODIF"),

  /** ********************************************. */
  /************** RETROACTIVIDADES ***************/
  /***********************************************/
  RRETRO_GENERICA_EXITUS("RGEE"),
  /** The nretro generica exitus. */
  NRETRO_GENERICA_EXITUS("NGEE"),
  /** The rretro generica. */
  RRETRO_GENERICA("RGEA"),
  /** The nretro generica. */
  NRETRO_GENERICA("NGEA"),
  /** The rretro rev grado. */
  RRETRO_REV_GRADO("RRGA"),
  /** The nretro rev grado. */
  NRETRO_REV_GRADO("NRGA"),
  /** The rretro atrasos exitus. */
  RRETRO_ATRASOS_EXITUS("RATE"),
  /** The nretro atrasos exitus. */
  NRETRO_ATRASOS_EXITUS("NATE"),
  /** The rretro atrasos. */
  RRETRO_ATRASOS("RATA"),
  /** The nretro atrasos. */
  NRETRO_ATRASOS("NATA"),
  /** The rretro rec alzada exitus. */
  RRETRO_REC_ALZADA_EXITUS("RREE"),
  /** The nretro rec alzada exitus. */
  NRETRO_REC_ALZADA_EXITUS("NREE"),
  /** The rretro rec alzada. */
  RRETRO_REC_ALZADA("RREA"),
  /** The nretro rec alzada. */
  NRETRO_REC_ALZADA("NREA"),
  /** The rretro dm1 exitus. */
  RRETRO_DM1_EXITUS("RM1E"),
  /** The rretro dm3 exitus. */
  RRETRO_DM3_EXITUS("RM3E"),
  /** The rretro dm2 exitus. */
  RRETRO_DM2_EXITUS("RM2E"),
  /** The rretro dm4 exitus. */
  RRETRO_DM4_EXITUS("RM4E"),

  /** ********************************************. */
  /******* ARCHIVO CENTRO NO ACREDITADO **********/
  /***********************************************/

  /** Resolucion **/
  RES_ARC_CNA("RACNA"),

  /** ********************************************. */
  /**************** PROPUESTA PIA ****************/
  /***********************************************/
  PROPUESTA_PIA("PROP_PIA"),
  /** The propuesta pia smad. */
  PROPUESTA_PIA_SMAD("PROP_PIA_SMAD"),
  /** The res cad 95. */
  RES_CAD_95("RC95"),
  /** The otro codigo. */
  OTRO_CODIGO("OTRO"),
  /** The propuesta pia ingresado. */
  PROPUESTA_PIA_INGRESADO("PROP_PIA_INGRES"),
  /** The propuesta pia metta. */
  PROPUESTA_PIA_METTA("PROP_PIA_METTA"),
  /** The propuesta pia enviada smad. */
  PROPUESTA_PIA_ENVIADA_SMAD("PROP_PIA_A_SMAD"),

  /** ********************************************. */
  /******** ARCHIVO SERVICIO RESIDENCIA **********/
  /*************** MAYORES G1 ********************/
  /***********************************************/
  /** The archivo servicio residencia mayores G1. */
  RES_ARC_SERV_RES_MAY_G1("RASRG1"),

  /** ********************************************. */
  /******* RESOLUCIÓN ARCHIVO GRADO MINORADO *****/
  /***********************************************/
  /** The notificación resolución caducidad grado minorado. */
  NOT_RES_CAD_GRA_MIN("NRCGM"),
  /** The notificación resolución revisión grado minorado. */
  NOT_RES_REV_GRA_MIN("NRRGM"),
  /** The resolución caducidad grado minorado. */
  RES_CAD_GRA_MIN("RCGM"),
  /** The resolución revisión grado minorado. */
  RES_REV_GRA_MIN("RRGM"),

  /** ********************************************. */
  /******** RESOLUCIÓN FIN PROCEDIMIENTO ********/
  /********** POR BAJA DE SERVICIO **************/
  /***********************************************/
  /** The resolución fin procedimiento por baja de servicio. */
  RES_BAJA_SERVICIO("RESBAJASERV"),

  /** The res desistimiento. */
  RES_DESISTIMIENTO("RESDES68"),

  /** ********************************************. */
  /*********** NOTIFICACIÓN Y RESOLUCIÓN *********/
  /*********** CAMBIO CNP ADMINISTRATIVO *********/
  /***********************************************/
  RES_CAMBIO_CNP_ADM("RESCCNP"),

  /** ********************************************. */
  /*********** RESOLUCIONES CONTENCIOSO **********/
  /**************** ADMINISTRATIVO ***************/
  /***********************************************/
  RES_RCA_GYN_("REJS"),

  /** ********************************************. */
  /*********** NOTIFICACIÓN CONTENCIOSO **********/
  /**************** ADMINISTRATIVO ***************/
  /***********************************************/
  NOT_RCA_GYN_("NEJS"),

  /** ********************************************. */
  /************** RESOLUCIÓN EXTINCIÓN ***********/
  /*********************** PIA *******************/
  /***********************************************/
  RES_EXTINCION_PIA_("REXTPIAFP"),

  /** ********************************************. */
  /************* RESOLUCIONES RECURSO ************/
  /***************** DE ALZADA *******************/
  /***********************************************/
  RES_ALZ_GYN_COMISION("RAGC"),

  /** The res alz gyn sincomision. */
  RES_ALZ_GYN_SINCOMISION("RAGSC"),
 /** The res alz gyn inadmision. */
 RES_ALZ_GYN_INADMISION("RAGI"),
 /** The res alz correccion gyn comision. */
 RES_ALZ_CORRECCION_GYN_COMISION("RAGCEC"),
 /** The res alz correccion gyn sincomision. */
 RES_ALZ_CORRECCION_GYN_SINCOMISION("RAGCESC"),
 /** The res alz correccion gyn inadmision. */
  RES_ALZ_CORRECCION_GYN_INADMISION("RAGCEI"),
 /** The res alz pia estimada. */
 RES_ALZ_PIA_ESTIMADA("RAPE"),
 /** The res alz pia desestimada. */
 RES_ALZ_PIA_DESESTIMADA("RAPD"),
 /** The res alz retro pia cnp. */
 RES_ALZ_RETRO_PIA_CNP("RARPCNP"),
 /** The res alz retro pia rs. */
  RES_ALZ_RETRO_PIA_RS("RARPRS"),
 /** The res alz retro pia pvs. */
 RES_ALZ_RETRO_PIA_PVS("RARPPVS"),
 /** The res alz retro pia inadmision cnp. */
 RES_ALZ_RETRO_PIA_INADMISION_CNP("RARPICNP"),
 /** The res alz retro pia inadmision resto. */
 RES_ALZ_RETRO_PIA_INADMISION_RESTO("RARPIRESTO"),
 /** The res alz resto. */
  RES_ALZ_RESTO("RARESTO"),

  /** Resolución Alzada Denegada Inadmisión. */
  RES_ALZ_DENEG_INADMISION("RADINAD"),

  /** Resolución Alzada Denegada Estimada. */
  RES_ALZ_DENEG_ESTIMADO("RADEST"),

  /** Resolución Alzada Denegada Desestimada Sin Comisión. */
  RES_ALZ_DENEG_DESESTIMADO_SIN_COMISION("RADDSPPC"),

  /** Resolución Alzada Denegada Desestimada Con Comisión. */
  RES_ALZ_DENEG_DESESTIMADO_CON_COMISION("RADDPPC"),

  /** Resolución Alzada Denegada Inadmisión Revisión. */
  RES_ALZ_DENEG_INADMISION_REV("RADINADREV"),

  /** Resolución Alzada Denegada Estimada Revisión. */
  RES_ALZ_DENEG_ESTIMADO_REV("RADESTREV"),

  /** Resolución Alzada Denegada Desestimada Sin Comisión Revisión. */
  RES_ALZ_DENEG_DESESTIMADO_SIN_COMISION_REV("RADDSPPCREV"),

  /** Resolución Alzada Denegada Desestimada Con Comisión Revisión. */
  RES_ALZ_DENEG_DESESTIMADO_CON_COMISION_REV("RADDPPCREV"),

  /** ********************************************. */
  /*********** NOTIFICACIONES RECURSO ************/
  /***************** DE ALZADA *******************/
  /***********************************************/

  /** Notificación Alzada GYN. */
  NOT_ALZ_GYN("NAG"),

  /** Notificación Alzada GYN Inadmisión. */
  NOT_ALZ_GYN_INADMISION("NAGI"),

  /** Notificación Alzada Corrección GYN. */
  NOT_ALZ_CORRECCION_GYN("NAGCE"),

  /** Notificación Alzada Corrección GYN Inadmisión. */
  NOT_ALZ_CORRECCION_GYN_INADMISION("NAGCEI"),

  /** Notificación Alzada PIA Estimada. */
  NOT_ALZ_PIA_ESTIMADA("NAPE"),

  /** Notificación Alzada PIA Desestimada. */
  NOT_ALZ_PIA_DESESTIMADA("NAPD"),

  /** Notificación Alzada Retro PIA CNP PVS RS. */
  NOT_ALZ_RETRO_PIA_CNP_PVS_RS("NARP"),

  /** Notificación Alzada Retro PIA Inadmisión CNP. */
  NOT_ALZ_RETRO_PIA_INADMISION_CNP("NARPICNP"),

  /** Notificación Alzada Retro PIA Inadmisión Resto. */
  NOT_ALZ_RETRO_PIA_INADMISION_RESTO("NARPIRESTO"),

  /** Notificación Alzada Resto. */
  NOT_ALZ_RESTO("NARESTO"),

  /** Notificación Alzada Denegación. */
  NOT_ALZ_DENEG("NOTRAD"),

  /** Notificación Alzada Denegación Revisión. */
  NOT_ALZ_DENEG_REV("NOTRADREV"),



/** The res r c gn ac. */
  // Resoluciones Revisión Confirmación
  RES_R_C_GN_AC("REVGNNODEP"),
  /** The res r c ndep ac. */
  RES_R_C_NDEP_AC("REVGNNODEP"),
  /** The res r c eve ac. */
  RES_R_C_EVE_AC("REVGNNODEP"),
  /** The res r c eve ndep ac. */
  RES_R_C_EVE_NDEP_AC("REVGNNODEP"),
  /** The res r c h ac. */
  RES_R_C_H_AC("REVGNNODEP"),
  /** The res r c eve bvd ac. */
  RES_R_C_EVE_BVD_AC("REVGNNODEP"),
  /** The res r n g ac. */
  RES_R_N_G_AC("REVGNNODEP"),

  /** The res r m gn vig ac. */
  // Resoluciones Revisión Modificación
  RES_R_M_GN_VIG_AC("REVGNNODEP"),
  /** The res r m gn nvig ac. */
  RES_R_M_GN_NVIG_AC("REVGNNODEP"),
  /** The res r m ndep ac. */
  RES_R_M_NDEP_AC("REVGNNODEP"),
  /** The res r m eve vig ac. */
  RES_R_M_EVE_VIG_AC("REVGNNODEP"),
  /** The res r m eve nvig ac. */
  RES_R_M_EVE_NVIG_AC("REVGNNODEP"),
  /** The res r m eve ndep ac. */
  RES_R_M_EVE_NDEP_AC("REVGNNODEP"),
  /** The res r m h vig ac. */
  RES_R_M_H_VIG_AC("REVGNNODEP"),
  /** The res r m h nvig ac. */
  RES_R_M_H_NVIG_AC("REVGNNODEP"),
  /** The res r m eve bvd vig ac. */
  RES_R_M_EVE_BVD_VIG_AC("REVGNNODEP"),
  /** The res r m eve bvd nvig ac. */
  RES_R_M_EVE_BVD_NVIG_AC("REVGNNODEP"),
  /** The res r m eve bvd ndep ac. */
  RES_R_M_EVE_BVD_NDEP_AC("REVGNNODEP"),
 /** The not rev. */
  NOT_REV("NOTREV"),
 /** The plantilla pend. */

 RES_CORR_REV_GN_SG("RCEREVGNSIN"),
 /** The not corr gyn. */
  NOT_CORR_GYN("NOT_CORR_GYN"),

  /** The res r c gn. */
  // Resoluciones Revisión Confirmación
  RES_R_C_GN("REVGNDEP"),
 /** The res r c ndep. */
 RES_R_C_NDEP("REVGNDEP"),
 /** The res r c eve. */
 RES_R_C_EVE("REVGNDEP"),
 /** The res r c eve ndep. */
 RES_R_C_EVE_NDEP("REVGNDEP"),
 /** The res r c h. */
 RES_R_C_H("REVGNDEP"),
 /** The res r c eve bvd. */
 RES_R_C_EVE_BVD("REVGNDEP"),
 /** The res r n g. */
 RES_R_N_G("REVGNDEP"),

  /** The res r m gn vig. */
  RES_R_M_GN_VIG("REVGNDEP"),
  /** The res r m gn nvig. */
  RES_R_M_GN_NVIG("REVGNDEP"),
  /** The res r m ndep. */
  RES_R_M_NDEP("REVGNDEP"),
  /** The res r m eve vig. */
  RES_R_M_EVE_VIG("REVGNDEP"),
  /** The res r m eve nvig. */
  RES_R_M_EVE_NVIG("REVGNDEP"),
  /** The res r m eve ndep. */
  RES_R_M_EVE_NDEP("REVGNDEP"),
  /** The res r m h vig. */
  RES_R_M_H_VIG("REVGNDEP"),
  /** The res r m h nvig. */
  RES_R_M_H_NVIG("REVGNDEP"),
  /** The res r m eve bvd vig. */
  RES_R_M_EVE_BVD_VIG("REVGNDEP"),
  /** The res r m eve bvd nvig. */
  RES_R_M_EVE_BVD_NVIG("REVGNDEP"),

  /** The res r m eve bvd ndep. */
  RES_R_M_EVE_BVD_NDEP("REVGNDEP"),
 /** The res corr rev gn cg. */
 RES_CORR_REV_GN_CG("RCEREVGNCON"),

 /** The res corr gyn. */
  RES_CORR_GYN("RES_CORR_GYN"),
 /** The not gn. */
  NOT_GN("NOT_GN"),
 /** The adjunto carta servicios res gn. */
  ADJUNTO_CARTA_SERVICIOS_RES_GN("ANOTADJGNSERV"),


  /** ********************************************. */
  /************ RESOLUCIÓN CORECCIÓN ************/
  /********************* PIA *********************/
  /***********************************************/
  RES_CORRECCION_PIA_SERVICIOS("RCPS"),

  /** The res correccion pia prestaciones. */
  RES_CORRECCION_PIA_PRESTACIONES("RCPP"),


  /** ********************************************. */
  /*********** NOTIFICACIÓN CORECCIÓN ************/
  /******************** PIA **********************/
  /***********************************************/
  NOT_CORRECCION_PIA_("NCP");

  /** The codigo. */
  private String codigo;

  /**
   * Instantiates a new configuracion enum.
   *
   * @param codigo the codigo
   */
  ConfiguracionEnum(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }


  /**
   * From codigo.
   *
   * @param codigo the codigo
   * @return the configuracion enum
   */
  @Nullable
  public static ConfiguracionEnum fromCodigo(final String codigo) {
    for (final ConfiguracionEnum tr : ConfiguracionEnum.values()) {
      if (tr.codigo.equalsIgnoreCase(codigo)) {
        return tr;
      }
    }
    return null;
  }
}
