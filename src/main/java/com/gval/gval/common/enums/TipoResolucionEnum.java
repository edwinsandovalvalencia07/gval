package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;

// TODO: Auto-generated Javadoc
/**
 * The Enum TipoResolucionEnum.
 */
public enum TipoResolucionEnum {

  /** The rarr. */
  RARR("RARR"),

  /** The rata. */
  RATA("RATA"),

  /** The rate. */
  RATE("RATE"),

  /** The rcee. */
  RCEE("RCEE"),

  /** The rcer. */
  RCER("RCER"),

  /** The rcrg. */
  RCRG("RCRG"),

  /** The rcsc. */
  RCSC("RCSC"),

  /** The rdmi. */
  RDMI("RDMI"),

  /** The rejs. */
  REJS("REJS"),

  /** The rera. */
  RERA("RERA"),

  /** The rere. */
  RERE("RERE"),

  /** The tipo tia. */
  RETSINPIA("RETSINPIA"),

  /** The rgea. */
  RGEA("RGEA"),

  /** The rgee. */
  RGEE("RGEE"),

  /** The rm1e. */
  RM1E("RM1E"),

  /** The rm2e. */
  RM2E("RM2E"),

  /** The rm3e. */
  RM3E("RM3E"),

  /** The rm4e. */
  RM4E("RM4E"),

  /** The rpuh. */
  RPUH("RPUH"),

  /** The revocacion archivo caducidad. */
  REVOCACION_ARCHIVO_CADUCIDAD("RAC"),

  /** The rrad. */
  RRAD("RRAD"),

  /** The rrae. */
  RRAE("RRAE"),

  /** The revocacion caducidad pia. */
  REVOCACION_CADUCIDAD_PIA("RCP"),

  /** The caducidad recurso pia. */
  CADUCIDAD_RECURSO_PIA("CSC"),

  /** The archivo caducidad pvs sad. */
  ARCHIVO_CADUCIDAD_PVS_SAD("RCS"),

  /** The rrds. */
  RRDS("RRDS"),

  /** The rrea. */
  RREA("RREA"),

  /** The rree. */
  RREE("RREE"),

  /** The rrga. */
  RRGA("RRGA"),

  /** The rria. */
  RRIA("RRIA"),

  /** The rrin. */
  RRIN("RRIN"),

  /** The rrra. */
  RRRA("RRRA"),

  /** The rrre. */
  RRRE("RRRE"),

  /** The rrrn. */
  RRRN("RRRN"),

  /** The rrtd. */
  RRTD("RRTD"),

  /** The resolucion recurso. */
  RRXR("RRXR"),
  /** The comun. */
  COMUN("COMUN"),
  /** The gnt. */
  GRADO_NIVEL_TRASLADO("GNT"),
  /** The dnt. */
  NO_DEPENDIENTE_TRASLADO("DNT"),

  /** The tipo denegacion traslado. */
  TIPO_DENEGACION_TRASLADO("DNT"),

  /** The arf. */
  ARCHIVO_FALLECIMIENTO("ARF"),

  /** The grado y nivel. */
  GRADO_Y_NIVEL("GYN"),

  /** The inadmision. */
  INADMISION("INV"),

  /** The proceso agudo. */
  PROCESO_AGUDO("IPA"),

  /** The revision proceso agudo. */
  REVISION_PROCESO_AGUDO("RPA"),

  /** The revison proceso agravamiento. */
  REVISON_PROCESO_AGRAVAMIENTO("RNA"),

  /** The correccion grado y nivel. */
  CORRECCION_GRADO_Y_NIVEL("CGN"),
  /** The tipo pia. */
  TIPO_PIA("PIA"),

  /** The tipo renuncia res gyn. */
  TIPO_RENUNCIA_RES_GYN("DSG"),

  /** The tipo renuncia res pia. */
  TIPO_RENUNCIA_RES_PIA("DSP"),

  /** The tipo renuncia pia. */
  TIPO_RENUNCIA_PIA("REP"),

  /** The tipo desistimiento. */
  TIPO_DESISTIMIENTO("DES"),
  /** The tipo resolucion archivo no acepta servicio residencia. */
  TIPO_RESOLUCION_ARCHIVO_NO_ACEPTA_SERVICIO_RESIDENCIA("ANR"),
  /** The tipo resolucion archivo no acepta servicio centro dia. */
  TIPO_RESOLUCION_ARCHIVO_NO_ACEPTA_SERVICIO_CENTRO_DIA("AND"),
  /** The tipo caducidad. */
  TIPO_CADUCIDAD("CAD"),
  /** The tipo reintegro. */
  TIPO_REINTEGRO("REI"),
  /** The tipo desest rev pia. */
  TIPO_DESEST_REV_PIA("DRP"),
  /** The tipo silencio administrativo. */
  TIPO_SILENCIO_ADMINISTRATIVO("SIL"),
  /** The tipo extincion. */
  TIPO_EXTINCION("EXT"),
  /** The tipo inadmision no dep. */
  TIPO_INADMISION_NO_DEP("IND"),
  /** The tipo situacion clic no val. */
  TIPO_SITUACION_CLIC_NO_VAL("INV"),
  /** The tipo error admin. */
  TIPO_ERROR_ADMIN("IEA"),
  /** The tipo no agrava. */
  TIPO_NO_AGRAVA("INA"),
  /** The tipo proceso agudo. */
  TIPO_PROCESO_AGUDO("IPA"),
  /** The tipo resolucion correccion gyn. */
  TIPO_RESOLUCION_CORRECCION_GYN("CGN"),
  /** The tipo resolucion revision proceso agudo. */
  TIPO_RESOLUCION_REVISION_PROCESO_AGUDO("RPA"),
  /** The tipo resolucion revison proceso agravamiento. */
  TIPO_RESOLUCION_REVISON_PROCESO_AGRAVAMIENTO("RNA"),

  /** The tipo archivo. */
  TIPO_ARCHIVO("AEX"),
  /** The tipo archivo imposibilidad material sobr. */
  TIPO_ARCHIVO_IMPOSIBILIDAD_MATERIAL_SOBR("AIM"),
  /** The tipo archivo centro noacr. */
  TIPO_ARCHIVO_CENTRO_NOACR("ACN"),
  /** The tipo archivo traslado. */
  TIPO_ARCHIVO_TRASLADO("ATR"),
  /** The tipo archivo fallecimiento. */
  TIPO_ARCHIVO_FALLECIMIENTO("ARF"),
  /** The tipo archivo caducidad pvs. */
  TIPO_ARCHIVO_CADUCIDAD_PVS("RCS"),
  /** The tipo resolucion archivo provisional. */
  TIPO_RESOLUCION_ARCHIVO_PROVISIONAL("ASR"),
 /** The tipo caducidad pia sin contrato. */
 TIPO_CADUCIDAD_PIA_SIN_CONTRATO("CSC"),
 /** The tipo revocacion caducidad pia. */
 TIPO_REVOCACION_CADUCIDAD_PIA("RCP"),
 /** The tipo revocacion archivo pvs sad. */
 TIPO_REVOCACION_ARCHIVO_PVS_SAD("RAC"),
 /** The tipo revocacion arc serv resi g1. */
  TIPO_REVOCACION_ARC_SERV_RESI_G1("RG1"),
 /** The tipo arc serv resi g1. */
  TIPO_ARC_SERV_RESI_G1("ASR"),
 /** The tipo cambio cnp. */
 TIPO_CAMBIO_CNP("CNP"),
 /** The tipo resolucion cambio cuidador. */
  TIPO_RESOLUCION_CAMBIO_CUIDADOR("CCN"),
 /** The tipo pago diferido herederos. */
 TIPO_PAGO_DIFERIDO_HEREDEROS("ACH"),
 /** The tipo pago unico herederos. */
 TIPO_PAGO_UNICO_HEREDEROS("PUH"),
 /** The tipo correccion pago unico herederos. */
 TIPO_CORRECCION_PAGO_UNICO_HEREDEROS("CPU"),
 /** The tipo apertura rev ofi. */
 TIPO_APERTURA_REV_OFI(
      "ARO"),
 /** The tipo pendiente revocacion fallecimiento. */
 TIPO_PENDIENTE_REVOCACION_FALLECIMIENTO("PRFV"),
 /** The tipo revocacion fallecimiento. */
 TIPO_REVOCACION_FALLECIMIENTO("RRF"),
 /** The tipo abono diferido. */
  TIPO_ABONO_DIFERIDO("ACD"),
 /** The tipo revocacion sad. */
 TIPO_REVOCACION_SAD("RPS"),
 /** The tipo revocacion arc imp. */
 TIPO_REVOCACION_ARC_IMP("RAI"),
 /** The tipo revocacion desestimiento. */
 TIPO_REVOCACION_DESESTIMIENTO("RRD"),
 /** The tipo baja voluntaria servicio. */
 TIPO_BAJA_VOLUNTARIA_SERVICIO("SBV"),
 /** The tipo revocacion caducidad 95. */
  TIPO_REVOCACION_CADUCIDAD_95("R95"),
 /** The tipo retroactividad rec alzada. */
 TIPO_RETROACTIVIDAD_REC_ALZADA("REA"),
 /** The tipo retroactividad rec alzada exitus. */
 TIPO_RETROACTIVIDAD_REC_ALZADA_EXITUS("REE"),
 /** The tipo retroactividad atrasos. */
 TIPO_RETROACTIVIDAD_ATRASOS("ATA"),
 /** The tipo retroactividad atrasos exitus. */
  TIPO_RETROACTIVIDAD_ATRASOS_EXITUS("ATE"),
 /** The tipo retroactividad revision grado. */
 TIPO_RETROACTIVIDAD_REVISION_GRADO("RGA"),
 /** The tipo retroactividad generica. */
  TIPO_RETROACTIVIDAD_GENERICA("GEA"),
 /** The tipo retroactividad generica exitus. */
 TIPO_RETROACTIVIDAD_GENERICA_EXITUS("GEE"),
 /** The tipo retroactividad cnp. */
 TIPO_RETROACTIVIDAD_CNP("RCC"),
 /** The tipo retroactividad cnp sentencia judicial. */
 TIPO_RETROACTIVIDAD_CNP_SENTENCIA_JUDICIAL("RCJ"),
 /** The tipo retroactividad pvs rs sentencia judicial. */
  TIPO_RETROACTIVIDAD_PVS_RS_SENTENCIA_JUDICIAL("RPJ"),
 /** The tipo retroactividad devolucion minoracion l1. */
 TIPO_RETROACTIVIDAD_DEVOLUCION_MINORACION_L1("DM1"),
 /** The tipo retroactividad devolucion minoracion l1 exitus. */
  TIPO_RETROACTIVIDAD_DEVOLUCION_MINORACION_L1_EXITUS("M1E"),
 /** The tipo retroactividad devolucion minoracion l2. */
 TIPO_RETROACTIVIDAD_DEVOLUCION_MINORACION_L2("DM2"),
 /** The tipo retroactividad devolucion minoracion l2 exitus. */
 TIPO_RETROACTIVIDAD_DEVOLUCION_MINORACION_L2_EXITUS("M2E"),
 /** The tipo retroactividad devolucion minoracion l3. */
  TIPO_RETROACTIVIDAD_DEVOLUCION_MINORACION_L3("DM3"),
 /** The tipo retroactividad devolucion minoracion l3 exitus. */
 TIPO_RETROACTIVIDAD_DEVOLUCION_MINORACION_L3_EXITUS("M3E"),
 /** The tipo retroactividad devolucion minoracion l4. */
 TIPO_RETROACTIVIDAD_DEVOLUCION_MINORACION_L4("DM4"),
 /** The tipo retroactividad devolucion minoracion l4 exitus. */
  TIPO_RETROACTIVIDAD_DEVOLUCION_MINORACION_L4_EXITUS("M4E"),
 /** The tipo archivo caducidad 95. */
  TIPO_ARCHIVO_CADUCIDAD_95("C95"),
 /** The tipo resolucion caducidad grado minorado. */
 TIPO_RESOLUCION_CADUCIDAD_GRADO_MINORADO("CGM"),
 /** The tipo resolucion revocacion grado minorado. */
  TIPO_RESOLUCION_REVOCACION_GRADO_MINORADO("RGM"),
 /** The tipo inadmision falta legitimacion. */
 TIPO_INADMISION_FALTA_LEGITIMACION("IFL"),
 /** The tipo revocacion inadmision legitimacion. */
  TIPO_REVOCACION_INADMISION_LEGITIMACION("RIL"),
 /** The tipo resolucion. */
 TIPO_RESOLUCION("RESOLUCION"),
 /** The tipo notificacion. */
 TIPO_NOTIFICACION("NOTIFICACION"),
 /** The modificatoria alza. */
 MODIFICATORIA_ALZA("MODIFICATORIA AL ALZA"),
 /** The modificatoria baja. */
  MODIFICATORIA_BAJA("MODIFICATORIA A LA BAJA"),
 /** The confirmatoria. */
 CONFIRMATORIA("CONFIRMATORIA"),

 /** The tipo resolucion correccion pia. */
  TIPO_RESOLUCION_CORRECCION_PIA("CPI"),
 /** The tipo retroactividad de pia. */
  TIPO_RETROACTIVIDAD_DE_PIA("PIR"),
 /** The tipo recurso alzada. */
  TIPO_RECURSO_ALZADA("RRA"),
 /** The tipo ejecucion de sentencia. */
  TIPO_EJECUCION_DE_SENTENCIA("EJS"),
 /** The tipo extincion pia. */
  TIPO_EXTINCION_PIA("EXP"),
 /** The tipo cambio cnp adm. */
 TIPO_CAMBIO_CNP_ADM("CCN");

  /** The codigo. */
  private final String codigo;

  /**
   * Instantiates a new tipo resolucion enum.
   *
   * @param codigo the codigo
   */
  TipoResolucionEnum(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * From codigo.
   *
   * @param codigo the codigo
   * @return the tipo resolucion enum
   */
  @Nullable
  public static TipoResolucionEnum fromCodigo(final String codigo) {

    if (codigo == null) {
      return null;
    }

    for (final TipoResolucionEnum td : TipoResolucionEnum.values()) {
      if (td.codigo.equalsIgnoreCase(codigo)) {
        return td;
      }
    }
    return null;
  }
}
