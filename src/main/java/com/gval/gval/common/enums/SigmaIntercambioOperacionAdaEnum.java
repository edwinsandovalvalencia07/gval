package com.gval.gval.common.enums;


import org.springframework.lang.Nullable;

public enum SigmaIntercambioOperacionAdaEnum {

  INSERTAR("I"), //
  ACTUALIZAR("A"), //
  ELIMINAR("D");

  final String codigo;

  SigmaIntercambioOperacionAdaEnum(final String codigo) {
    this.codigo = codigo;
  }

  public String getCodigo() {
    return this.codigo;
  }

  @Nullable
  public static SigmaIntercambioOperacionAdaEnum getByCodigo(final String codigo) {
    for (final SigmaIntercambioOperacionAdaEnum iter : SigmaIntercambioOperacionAdaEnum
        .values()) {
      if (iter.getCodigo().equals(codigo)) {
        return iter;
      }
    }
    return null;
  }

}
