package com.gval.gval.common.enums.listados;

import java.util.Arrays;

/**
 * The Enum CategoriaTipoPersonaEnum.
 */
public enum CategoriaTipoPersonaEnum implements InterfazListadoLabels<String> {

  // @formatter:off
  /** The solicitante. */
  SOLICITANTE("SO", "label.solicitante"),

  /** The representante. */
  REPRESENTANTE("RP", "label.representante"),

  /** The unidad familiar. */
  UNIDAD_FAMILIAR("UF", "label.unidad-familiar"),

  /** The cuidador. */
  CUIDADOR("CU", "label.cuidador"),

  /** The asistente. */
  ASISTENTE("AS", "label.asistente"),

  /** The guardador de hecho. */
  GUARDADOR_DE_HECHO("GH", "label.guardador-hecho"),

  /** The revision. */
  REVISION("RV", "label.revision");
  // @formatter:on


  /** The value. */
  private String valor;

  /** The label. */
  private String label;


  /**
   * Instantiates a new categoria tipo persona enum.
   *
   * @param valor the valor
   * @param label the label
   */
  CategoriaTipoPersonaEnum(final String valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  @Override
  public String getValor() {
    return valor;
  }



  /**
   * Gets the label.
   *
   * @return the label
   */
  @Override
  public String getLabel() {
    return label;
  }


  /**
   * From string.
   *
   * @param valor the valor
   * @return the tipo representacion enum
   */
  public static CategoriaTipoPersonaEnum fromValor(final String valor) {
    return Arrays.asList(CategoriaTipoPersonaEnum.values()).stream()
        .filter(ct -> ct.valor.equals(valor)).findFirst().orElse(null);
  }

  /**
   * Label from valor.
   *
   * @param valor the valor
   * @return the string
   */
  public static String labelFromValor(final String valor) {
    final CategoriaTipoPersonaEnum categoriaTipoPersona = fromValor(valor);
    return categoriaTipoPersona == null ? null : categoriaTipoPersona.label;
  }
}
