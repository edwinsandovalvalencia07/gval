package com.gval.gval.common.enums.listados;

/**
 * The Enum MotivosDenegacionCuidadorEnum.
 */
public enum MotivoDenegacionCuidadorEnum
    implements InterfazListadoLabels<Long> {


  /** The pensionista jubilacion. */
  PENSIONISTA_JUBILACION(1L,
      "label.listado-enum.motivo-denegacion-cuidador.pensionista_jubilacion"),

  /** The empleado hogar. */
  EMPLEADO_HOGAR(2L,
      "label.listado-enum.motivo-denegacion-cuidador.empleado_hogar"),

  /** The trabajador muface. */
  TRABAJADOR_MUFACE(3L,
      "label.listado-enum.motivo-denegacion-cuidador.trabajador_muface"),

  /** The regimen banca. */
  REGIMEN_BANCA(4L,
      "label.listado-enum.motivo-denegacion-cuidador.regimen_banca"),

  /** The excedencia laboral. */
  EXCEDENCIA_LABORAL(5L,
      "label.listado-enum.motivo-denegacion-cuidador.excedencia_laboral"),

  /** The prestacion desempleo. */
  PRESTACION_DESEMPLEO(6L,
      "label.listado-enum.motivo-denegacion-cuidador.prestacion_desempleo"),

  /** The pensionista incapacidad permanente total. */
  PENSIONISTA_INCAPACIDAD_PERMANENTE_TOTAL(7L,
      "label.listado-enum.motivo-denegacion-cuidador.pensionista_incapacidad_permanente_total"),

  /** The pensionista viudedad. */
  PENSIONISTA_VIUDEDAD(8L,
      "label.listado-enum.motivo-denegacion-cuidador.pensionista_viudedad"),

  /** The asistente personal. */
  ASISTENTE_PERSONAL(9L,
      "label.listado-enum.motivo-denegacion-cuidador.asistente_personal"),

  /** The trabajador cuenta ajena. */
  TRABAJADOR_CUENTA_AJENA(10L,
      "label.listado-enum.motivo-denegacion-cuidador.trabajador_cuenta_ajena"),

  /** The otros. */
  OTROS(11L, "label.listado-enum.motivo-denegacion-cuidador.otros"),

  /** The s agrario. */
  S_AGRARIO(12L,
      "label.listado-enum.motivo-denegacion-cuidador.sector-agrario"),

  /** The trabajador tiempo parcial. */
  TRABAJADOR_TIEMPO_PARCIAL(13L,
      "label.listado-enum.motivo-denegacion-cuidador.trabajador_tiempo_parcial");


  /** The valor. */
  private Long valor;

  /** The label. */
  private String label;



  /**
   * Instantiates a new tipo cuidador.
   *
   * @param valor the valor
   * @param label the label
   */
  MotivoDenegacionCuidadorEnum(final Long valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  @Override
  public Long getValor() {
    return valor;
  }



  /**
   * Gets the label.
   *
   * @return the label
   */
  @Override
  public String getLabel() {
    return label;
  }
}
