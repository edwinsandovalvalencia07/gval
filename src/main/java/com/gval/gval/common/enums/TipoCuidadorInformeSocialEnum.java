package com.gval.gval.common.enums;

import java.util.Arrays;


/**
 * The Enum TipoCuidadorInformeSocialEnum.
 */
public enum TipoCuidadorInformeSocialEnum {


  /** The convivencia. */
  CONVIVENCIA(1, "label.informesocial.tipocuidador.convivencia"),

  /** The vive alternativo. */
  VIVE_ALTERNATIVAMENTE(2,
      "label.informesocial.tipocuidador.vivealternativamente"),

  /** The cuidador. */
  CUIDADOR(3, "label.informesocial.tipocuidador.cuidador");


  /** The valor. */
  private Integer valor;

  /** The label. */
  private String label;



  /**
   * Instantiates a new tipo cuidador informe social enum.
   *
   * @param valor the valor
   * @param label the label
   */
  TipoCuidadorInformeSocialEnum(final Integer valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public Integer getValor() {
    return valor;
  }



  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * From valor.
   *
   * @param valor the valor
   * @return the tipo cuidador informe social enum
   */
  public static TipoCuidadorInformeSocialEnum fromValor(final Integer valor) {
    return Arrays.asList(TipoCuidadorInformeSocialEnum.values()).stream()
        .filter(tc -> tc.valor.intValue() == valor).findFirst().orElse(null);
  }


  /**
   * Label from valor.
   *
   * @param valor the valor
   * @return the string
   */
  public static String labelFromValor(final Integer valor) {
    final TipoCuidadorInformeSocialEnum tipoCuidadorEnum = fromValor(valor);
    return tipoCuidadorEnum == null ? null : tipoCuidadorEnum.label;
  }
}
