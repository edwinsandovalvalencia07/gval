package com.gval.gval.common.enums.listados;

/**
 * The Enum TipoPrestacionLaboralEnum.
 */
public enum TipoPrestacionLaboralEnum implements InterfazListadoLabels<Long> {


  /** The primero. */
  DESEMPLEO(0L, "label.listado-enum.tipo-prestacion-laboral.desempleo"),


  /** The segundo. */
  SUBSIDIO(1L, "label.listado-enum.tipo-prestacion-laboral.subsidio");


  /** The valor. */
  private Long valor;

  /** The label. */
  private String label;



  /**
   * Instantiates a new tipo cuidador.
   *
   * @param valor the valor
   * @param label the label
   */
  TipoPrestacionLaboralEnum(final Long valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  @Override
  public Long getValor() {
    return valor;
  }



  /**
   * Gets the label.
   *
   * @return the label
   */
  @Override
  public String getLabel() {
    return label;
  }
}
