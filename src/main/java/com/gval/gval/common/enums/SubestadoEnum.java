package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;

/**
 * The Enum SubestadoEnum.
 */
public enum SubestadoEnum {

  /** The en proceso. */
  // @formatter:off
  EN_PROCESO("PRO"),
  /** The pendiente validar. */
  PENDIENTE_VALIDAR("PVA"),
  /** The validada. */
  VALIDADA("VAL"),
  /** The no validada. */
  NO_VALIDADA("NVA");
  //@formatter:on

  /** The codigo. */
  private String codigo;

  /**
   * Instantiates a new subestado enum.
   *
   * @param codigo the codigo
   */
  SubestadoEnum(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  // metodo que devuelve el codigo del estado
  public String getCodigo() {
    return codigo;
  }

  /**
   * From codigo.
   *
   * @param codigo the codigo
   * @return the subestado enum
   */
  @Nullable
  public static SubestadoEnum fromCodigo(final String codigo) {
    for (final SubestadoEnum se : values()) {
      if (se.codigo.equals(codigo)) {
        return se;
      }
    }
    return null;
  }
}