package com.gval.gval.common.enums;

import java.util.Arrays;

/**
 * The Enum TipoNefisListadoEnum.
 */
public enum TipoNefisListadoEnum {

  /** The nefis peticiones. */
  NEFIS_PETICIONES(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_NEFIS_PETICIONES.getValor(),
      "NEFISPET", "label.nefis.nefis_peticiones"),

  /** The nefis terceros erroneos. */
  NEFIS_TERCEROS_ERRONEOS(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_NEFIS_TERCEROS_ERRONEOS
          .getValor(),
      "NEFISTER", "label.nefis.nefis_terceros_erroneos");


  /** The valor. */
  private String valor;

  /** The label. */
  private String label;

  /** The access role. */
  private String accessRole;

  /**
   * Instantiates a new NEFIS.
   *
   * @param accessRole the access role
   * @param valor the valor
   * @param label the label
   */
  private TipoNefisListadoEnum(final String accessRole, final String valor,
      final String label) {
    this.accessRole = accessRole;
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * Gets the access role.
   *
   * @return the accessRole
   */
  public String getAccessRole() {
    return accessRole;
  }

  /**
   * From string.
   *
   * @param text the text
   * @return the tipo NEFIS listado enum
   */
  public static TipoNefisListadoEnum fromString(final String text) {
    return Arrays.asList(TipoNefisListadoEnum.values()).stream()
        .filter(opsc -> opsc.valor.equals(text)).findFirst().orElse(null);
  }
}
