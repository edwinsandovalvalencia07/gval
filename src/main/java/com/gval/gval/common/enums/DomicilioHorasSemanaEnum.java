package com.gval.gval.common.enums;

import java.util.Arrays;

/**
 * The Enum DomicilioHorasSemanaEnum.
 */
public enum DomicilioHorasSemanaEnum {

  /** The vacio. */
  VACIO(0, " "),

  /** The mas de 22. */
  MAS_DE_22(1, "label.informe_social.mas22horas"),

  /** The menos de 22. */
  MENOS_DE_22(2, "label.informe_social.menos22horas");

  /** The valor. */
  private Integer valor;

  /** The label. */
  private String label;

  /**
   * Instantiates a new domicilio horas semana enum.
   *
   * @param valor the valor
   * @param label the label
   */
  private DomicilioHorasSemanaEnum(final Integer valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public Integer getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * From integer.
   *
   * @param text the text
   * @return the sexo enum
   */
  public static DomicilioHorasSemanaEnum fromInteger(final Integer text) {
    return Arrays.asList(DomicilioHorasSemanaEnum.values()).stream()
        .filter(dhs -> dhs.valor.equals(text)).findFirst().orElse(null);
  }

}
