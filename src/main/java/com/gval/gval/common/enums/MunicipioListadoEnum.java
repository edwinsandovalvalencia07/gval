package com.gval.gval.common.enums;

import java.util.Arrays;

/**
 * The Enum ProvinciaListadoEnum.
 */
public enum MunicipioListadoEnum {


  /** The poblaciones todas. */
  MUNICIPIOS_TODAS("LTACTAM", "label.todos", "label.titulo.pagina.todos"),

  /** The poblaciones navia. */
  MUNICIPIOS_NAVIA("LTACTMNA", "label.navia", "label.titulo.pagina.navia"),

  /** The poblaciones candamo. */
  MUNICIPIOS_CANDAMO("LTACTMCA", "label.candamo", "label.titulo.pagina.candamos");


  /** The valor. */
  private final String valor;

  /** The label. */
  private final String label;

  /** The title. */
  private final String title;

  /**
   * Instantiates a new opinion persona situacion centro enum.
   *
   * @param valor the valor
   * @param label the label
   * @param title the title
   */
  private MunicipioListadoEnum(final String valor, final String label,
      final String title) {
    this.valor = valor;
    this.label = label;
    this.title = title;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * Gets the title.
   *
   * @return the title
   */
  public String getTitle() {
    return title;
  }


  /**
   * From string.
   *
   * @param text the text
   * @return the municipio listado enum
   */
  public static MunicipioListadoEnum fromString(final String text) {
    return Arrays.asList(MunicipioListadoEnum.values()).stream()
        .filter(opsc -> opsc.valor.equals(text)).findFirst().orElse(null);
  }


}
