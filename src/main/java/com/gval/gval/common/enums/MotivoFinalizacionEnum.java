package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;


/**
 * The Enum MotivoFinalizacionSolicitudEnum.
 */
public enum MotivoFinalizacionEnum {

  /** The notificado revision gyn. */
  // @formatter:off
  NOTIFICADO_REVISION_GYN("SRGN"),
  
  /** The trasladada fuera cv. */
  TRASLADADA_FUERA_CV("STRS"),
  
  /** The fallecido no confirmado. */
  FALLECIDO_NO_CONFIRMADO("FNCA"),
  
  /** The fallecido si confirmado. */
  FALLECIDO_SI_CONFIRMADO("FSCA"),
  
  /** The desestimiento renuncia interesado. */
  DESESTIMIENTO_RENUNCIA_INTERESADO("DADR"),
  
  /** The desestimiento falta documentacion. */
  DESESTIMIENTO_FALTA_DOCUMENTACION("RDES"),
  
  /** The archivo caducidad pvs sad. */
  ARCHIVO_CADUCIDAD_PVS_SAD("ACPS"),
  
  /** The archivo fallecimiento. */
  ARCHIVO_FALLECIMIENTO("AFAL"),
  
  /** The archivo centro no acreditado. */
  ARCHIVO_CENTRO_NO_ACREDITADO("ACNA"),
  
  /** The extincion pia incumplimiento requisitos. */
  EXTINCION_PIA_INCUMPLIMIENTO_REQUISITOS("REIR"),
  
  /** The extincion pia cambio grado. */
  EXTINCION_PIA_CAMBIO_GRADO("RECG"),
  
  /** The ilocalizable valorar. */
  ILOCALIZABLE_VALORAR("IPV"),
  
  /** The ilocalizable firmar pia. */
  ILOCALIZABLE_FIRMAR_PIA("IFP"),
  
  /** The ilocalizable continuar tramite. */
  ILOCALIZABLE_CONTINUAR_TRAMITE("ICT"),
  
  /** The renuncia. */
  RENUNCIA("RNUN"),
  
  /** The caducidad art 95. */
  CADUCIDAD_ART_95("AC95"),
  
  /** The archivo procedimiento revision oficio. */
  ARCHIVO_PROCEDIMIENTO_REVISION_OFICIO("APRO"),
  
  /** The archivo imposibilidad material sobrevenida. */
  ARCHIVO_IMPOSIBILIDAD_MATERIAL_SOBREVENIDA("AIMS"),
  
  /** The archivo baja servicio. */
  ARCHIVO_BAJA_SERVICIO("ABSV"),                    
  
  /** The archivo procedimiento no iniciado. */
  ARCHIVO_PROCEDIMIENTO_NO_INICIADO("APIN"),
  
  /** The arcvhio servicio residencia g1. */
  ARCVHIO_SERVICIO_RESIDENCIA_G1("ASRG"),
  
  /** The archivo caducidad grado. */
  ARCHIVO_CADUCIDAD_GRADO("ACG"),
  
  /** The archivo revocacion grado. */
  ARCHIVO_REVOCACION_GRADO("ARG"),
  
  /** The no acepta plaza residencia. */
  NO_ACEPTA_PLAZA_RESIDENCIA("NAPR");
  //@formatter:on


  /** The codigo. */
  private String codigo;


  /**
   * Instantiates a new motivo finalizacion enum.
   *
   * @param codigo the codigo
   */
  MotivoFinalizacionEnum(final String codigo) {
    this.codigo = codigo;
  }


  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  // metodo que devuelve el codigo del estado
  public String getCodigo() {
    return codigo;
  }


  /**
   * From codigo.
   *
   * @param codigo the codigo
   * @return the motivo finalizacion enum
   */
  @Nullable
  public static MotivoFinalizacionEnum fromCodigo(
      final String codigo) {

    if (codigo == null) {
      return null;
    }

    for (final MotivoFinalizacionEnum estado : MotivoFinalizacionEnum
        .values()) {
      if (estado.codigo.equalsIgnoreCase(codigo)) {
        return estado;
      }
    }
    return null;
  }
}
