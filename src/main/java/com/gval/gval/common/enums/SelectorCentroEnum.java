package com.gval.gval.common.enums;

/**
 * The Enum SelectorCentroEnum.
 */
public enum SelectorCentroEnum {

  /** Pantalla teleasistencia. */
  NUEVAS_PREFERENCIAS_CD1("NPCD1"),

  /** Pantalla nuevas preferencias centro dia 2. */
  NUEVAS_PREFERENCIAS_CD2("NPCD2"),

  /** Pantalla nuevas preferencias centro dia 3. */
  NUEVAS_PREFERENCIAS_CD3("NPCD3"),


  /** Pantalla nuevas preferencias centro residencial 1. */
  NUEVAS_PREFERENCIAS_CR1("NPCR1"),

  /** Pantalla nuevas preferencias centro residencial 2. */
  NUEVAS_PREFERENCIAS_CR2("NPCR2"),

  /** Pantalla nuevas preferencias centro residencial 3. */
  NUEVAS_PREFERENCIAS_CR3("NPCR3"),


  /** Pantalla antecedentes sociales centro dia . */
  ANTECEDENTES_SOCIALES_CD("ASCD"),

  /** Pantalla antecedentes sociales centro residencial . */
  ANTECEDENTES_SOCIALES_CR("ASCR");

  /** The id. */
  private String id;

  /**
   * Instantiates a new selector centro enum.
   *
   * @param id the id
   */
  private SelectorCentroEnum(final String id) {
    this.id = id;
  }

  /**
   * Gets the id.
   *
   * @return the id
   */
  public String getId() {
    return id;
  }
}
