package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;

// TODO: Auto-generated Javadoc
/**
 * The Enum TipoEstudioEnum.
 */
public enum TipoPerfilEnum {

	/** The urgencia. */
	DESCONOCIDO("DE", "DESCONOCIDO"),

	/** The dictamen. */
	TECNICO_SMAD("SM", "TECNICO_SMAD"),

	/** The revision. */
	GRABADOR("GR", "GRABADOR"),

	/** The recurso. */
	COORDINADOR("CO", "COORDINADOR"),

	/** The homologacion. */
	VALORADOR("VA", "VALORADOR"),

	/** The recurso denegacion. */
	COMISION("CM", "COMISION"),

	/** The tecnico pia. */
	TECNICO_PIA("TP", "TECNICO_PIA"),

	/** The prestaciones. */
	PRESTACIONES("PS", "PRESTACIONES"),

	/** The prestaciones discapacidad. */
	PRESTACIONES_DISCAPACIDAD("PD", "PRESTACIONES_DISCAPACIDAD"),

	/** The auxiliar administrativo. */
	AUXILIAR_ADMINISTRATIVO("AA", "AUXILIAR_ADMINISTRATIVO"),

	/** The administrativo. */
	ADMINISTRATIVO("AD", "ADMINISTRATIVO"),

	/** The administrativo valoracion. */
	ADMINISTRATIVO_VALORACION("AV", "ADMINISTRATIVO_VALORACION"),

	/** The recursos alzada. */
	RECURSOS_ALZADA("RA", "RECURSOS_ALZADA"),

	/** The administrativo comision. */
	ADMINISTRATIVO_COMISION("AC", "ADMINISTRATIVO_COMISION"),

	/** The administrativo pia. */
	ADMINISTRATIVO_PIA("PI", "ADMINISTRATIVO_PIA"),

	/** The dt pia. */
	DT_PIA("DP", "DT_PIA"),

	/** The dt nominas. */
	DT_NOMINAS("DN", "DT_NOMINAS"),

	/** The teleasistencia. */
	TELEASISTENCIA("TE", "TELEASISTENCIA"),

	/** The imserso. */
	IMSERSO("IM", "IMSERSO"),

	/** The consulta. */
	CONSULTA("CN", "CONSULTA"),

	/** The consulta ext. */
	CONSULTA_EXT("CE", "CONSULTA_EXT"),

	/** The administrador. */
	ADMINISTRADOR("SP", "ADMINISTRADOR"),

	/** The administrador smad. */
	ADMINISTRADOR_SMAD("AS", "ADMINISTRADOR_SMAD"),

	/** The coordinador pia. */
	COORDINADOR_PIA("CP", "COORDINADOR_PIA"),

	/** The centro base. */
	CENTRO_BASE("CB", "CENTRO_BASE"),

	/** The coordinador grabacion. */
	COORDINADOR_GRABACION("CG", "COORDINADOR_GRABACION"),

	/** The incidencias anexo i. */
	INCIDENCIAS_ANEXO_I("A1", "INCIDENCIAS_ANEXO_I"),

	/** The incidencias anexo ii. */
	INCIDENCIAS_ANEXO_II("A2", "INCIDENCIAS_ANEXO_II"),

	/** The eliminar solicitudes. */
	ELIMINAR_SOLICITUDES("ES", "ELIMINAR_SOLICITUDES"),

	/** The exitus avapsa. */
	EXITUS_AVAPSA("EA", "EXITUS_AVAPSA"),

	/** The exitus prestaciones. */
	EXITUS_PRESTACIONES("EP", "EXITUS_PRESTACIONES"),

	/** The eliminar expedientes. */
	ELIMINAR_EXPEDIENTES("EX", "ELIMINAR_EXPEDIENTES"),

	/** The linea 900. */
	LINEA_900("LI", "LINEA_900"),

	/** The tecnico tutelas. */
	TECNICO_TUTELAS("TU", "TECNICO_TUTELAS"),

	/** The firmante. */
	FIRMANTE("FI", "FIRMANTE"),

	/** The linea 012. */
	LINEA_012("LD", "LINEA_012"),

	/** The acta masiva. */
	ACTA_MASIVA("AM", "ACTA_MASIVA"),

	/** The estadisticas. */
	ESTADISTICAS("ET", "ESTADISTICAS"),

	/** The grabador empresa. */
	GRABADOR_EMPRESA("GE", "GRABADOR_EMPRESA"),

	/** The tecnico pia ayto. */
	TECNICO_PIA_AYTO("TA", "TECNICO_PIA_AYTO"),

	/** The grabador ayto. */
	GRABADOR_AYTO("GA", "GRABADOR_AYTO"),

	/** The tecnico ssgg. */
	TECNICO_SSGG("TG", "TECNICO_SSGG"),

	/** The resp patrimonial. */
	RESP_PATRIMONIAL("RP", "RESP_PATRIMONIAL"),

	/** The consulta documentos. */
	CONSULTA_DOCUMENTOS("CD", "CONSULTA_DOCUMENTOS"),

	/** The validar documentos. */
	VALIDAR_DOCUMENTOS("VD", "VALIDAR_DOCUMENTOS");

	/** The value. */
	private final String value;

	/** The label. */
	private final String label;

	/**
	 * Instantiates a new tipo estudio enum.
	 *
	 * @param value the value
	 * @param label the label
	 */
	private TipoPerfilEnum(final String value, final String label) {
		this.value = value;
		this.label = label;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Gets the label.
	 *
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return value;
	}

	/**
	 * From string.
	 *
	 * @param text the text
	 * @return the tipo estudio enum
	 */
	@Nullable
	public static TipoPerfilEnum fromString(final String text) {

		if (text == null) {
			return null;
		}

		for (final TipoPerfilEnum te : TipoPerfilEnum.values()) {
			if (te.value.equalsIgnoreCase(text)) {
				return te;
			}
		}
		return null;
	}

}
