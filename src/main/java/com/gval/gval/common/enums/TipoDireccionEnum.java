package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;

/**
 * The Enum TipoPersonaEnum.
 */
public enum TipoDireccionEnum {

  /** The domicilio. */
  DOMICILIO(TiposDireccion.DOMICILIO),

  /** The residencia. */
  RESIDENCIA(TiposDireccion.RESIDENCIA),

  /** The extranjero. */
  EXTRANJERO(TiposDireccion.EXTRANJERO),
  
  /** The solicitante. Solo para el guardado inicial */
  SOLICITANTE(TiposDireccion.SOLICITANTE);

  /**
   * The Class TiposDireccion.
   */
  public class TiposDireccion {

    /** The Constant DOMICILIO. */
    public static final String DOMICILIO = "D";

    /** The Constant RESIDENCIA. */
    public static final String RESIDENCIA = "R";

    /** The Constant EXTRANJERO. */
    public static final String EXTRANJERO = "E";
    
    /** The Constant SOLICITANTE. */
    public static final String SOLICITANTE = "S";
  }

  /** The value. */
  private final String value;

  /**
   * Instantiates a new tipo direccion enum.
   *
   * @param value the value
   */
  private TipoDireccionEnum(final String value) {
    this.value = value;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public String getValue() {
    return value;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return value;
  }

  /**
   * From string.
   *
   * @param text the text
   * @return the tipo direccion enum
   */
  @Nullable
  public static TipoDireccionEnum fromString(final String text) {

    if (text == null) {
      return null;
    }

    for (final TipoDireccionEnum te : TipoDireccionEnum.values()) {
      if (te.value.equalsIgnoreCase(text)) {
        return te;
      }
    }
    return null;
  }



}
