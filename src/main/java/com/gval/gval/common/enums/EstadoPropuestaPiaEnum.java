package com.gval.gval.common.enums;

import com.gval.gval.common.enums.listados.InterfazListadoLabels;

import java.util.Arrays;

/**
 * The Enum EstadoIncidenciaEnum.
 */
public enum EstadoPropuestaPiaEnum implements InterfazListadoLabels<String> {

  /** The borrador. */
  BORRADOR("BO", "label.propuesta_pia.estado_BO"),

  /** The grabada. */
  GRABADA("GR", "label.propuesta_pia.estado_GR");

  /** The valor. */
  private String valor;

  /** The label. */
  private String label;

  /**
   * Constructor. *
   *
   * @param valor the valor
   * @param label the label
   */
  private EstadoPropuestaPiaEnum(final String valor, final String label) {
    this.valor = valor;
    this.label = label;
  }



  /**
   * From string.
   *
   * @param text the text
   * @return the estado incidencia enum
   */
  public static EstadoPropuestaPiaEnum fromString(final String text) {
    return Arrays.asList(EstadoPropuestaPiaEnum.values()).stream()
        .filter(opsc -> opsc.valor.equals(text)).findFirst().orElse(null);
  }

  /**
   * Convierte enum a String.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return valor;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  @Override
  public String getValor() {
    return valor;
  }



  /**
   * Gets the label.
   *
   * @return the label
   */
  @Override
  public String getLabel() {
    return label;
  }



}
