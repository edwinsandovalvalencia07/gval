package com.gval.gval.common.enums;

/**
 * The Enum DocumentoPersonaEnum.
 */
public enum DocumentoPersonaEnum {

  /** The nuss. */
  //@formatter:off
   NUSS("NUSS"),
   /** The sip. */
   SIP("SIP");
   // @formatter:on

  /** The value. */
  private String value;

  /**
   * Instantiates a new documento persona enum.
   *
   * @param value the value
   */
  DocumentoPersonaEnum(final String value) {
    this.value = value;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public String getValue() {
    return value;
  }
}
