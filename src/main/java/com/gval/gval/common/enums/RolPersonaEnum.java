package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;


/**
 * The Enum TipoPersonaEnum.
 */
public enum RolPersonaEnum {
  /** The solicitante. */
  SOLICITANTE(Roles.SOLICITANTE),

  /** The representante. */
  REPRESENTANTE(Roles.REPRESENTANTE),

  /** The familiar. */
  FAMILIAR(Roles.FAMILIAR),

  /** The cuidador. */
  CUIDADOR(Roles.CUIDADOR),

  /** The asistente personal. */
  ASISTENTE(Roles.ASISTENTE),

  /** The asistente pro. */
  ASISTENTE_PRO(Roles.ASISTENTE_PRO);

  /**
   * The Class Nombres.
   */
  public class Roles {

    /** The Constant SOLICITANTE. */
    public static final String SOLICITANTE = "solicitante";

    /** The Constant REPRESENTANTE. */
    public static final String REPRESENTANTE = "representante";

    /** The Constant FAMILIAR. */
    public static final String FAMILIAR = "familiar";

    /** The Constant CUIDADOR. */
    public static final String CUIDADOR = "cuidador";

    /** The Constant ASISTENTE. */
    public static final String ASISTENTE = "asistente";

    /** The Constant ASISTENTE_PRO. */
    public static final String ASISTENTE_PRO = "asispro";
  }

  /** The value. */
  private final String value;

  /**
   * Instantiates a new tipo persona enum.
   *
   * @param value the value
   */
  private RolPersonaEnum(final String value) {
    this.value = value;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public String getValue() {
    return value;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return value;
  }

  /**
   * From string.
   *
   * @param text the text
   * @return the rol persona enum
   */
  @Nullable
  public static RolPersonaEnum fromString(final String text) {

    if (text == null) {
      return null;
    }

    for (final RolPersonaEnum te : RolPersonaEnum.values()) {
      if (te.value.equalsIgnoreCase(text)) {
        return te;
      }
    }
    return null;
  }



}
