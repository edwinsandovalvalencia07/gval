package com.gval.gval.common.enums;

import java.util.Arrays;

/**
 * The Enum RiesgoClaudicacionEnum.
 */
public enum RiesgoClaudicacionEnum {

  /** The seleccione. */
  SELECCIONE(1, "4", "label.informe_social.seleccione_opcion"),

  /** The alto. */
  ALTO(2, "3", "label.informe_social.alto"),

  /** The medio. */
  MEDIO(3, "2", "label.informe_social.medio"),

  /** The bajo. */
  BAJO(4, "1", "label.informe_social.bajo");

  /** The orden. */
  private Integer orden;

  /** The valor. */
  private String valor;

  /** The label. */
  private String label;

  /**
   * Instantiates a new nivel relacion enum.
   *
   * @param orden the orden
   * @param valor the valor
   * @param label the label
   */
  private RiesgoClaudicacionEnum(final Integer orden, final String valor,
      final String label) {
    this.orden = orden;
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the orden.
   *
   * @return the orden
   */
  public Integer getOrden() {
    return orden;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * From integer.
   *
   * @param text the text
   * @return the nivel relacion enum
   */
  public static RiesgoClaudicacionEnum fromString(final String text) {
    return Arrays.asList(RiesgoClaudicacionEnum.values()).stream()
        .filter(opsc -> opsc.valor.equals(text)).findFirst().orElse(null);
  }

}
