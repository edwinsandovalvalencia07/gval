package com.gval.gval.common.enums.listados;

/**
 * The Enum TipoPrestacionDocumentoContableEnum.
 */
public enum TipoPrestacionDocumentoContableEnum {

  /** The todas. */
  TODAS("TODAS"),
  /** The cnp. */
  CNP("CNP"),
  /** The pvs. */
  PVS("PVS"),
  /** The ap. */
  AP("AP");


  /** The valor. */
  private final String valor;


  /**
   * Instantiates a new tipo resolucion masiva listado enum.
   *
   * @param valor the valor
   */
  private TipoPrestacionDocumentoContableEnum(final String valor) {
    this.valor = valor;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

}
