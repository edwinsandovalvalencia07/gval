package com.gval.gval.common.enums;


import com.gval.gval.common.enums.listados.InterfazListadoLabels;

import java.util.Arrays;

/**
 * The Enum EstadoIncidenciaEnum.
 */
public enum EstadoDatosEconomicosEnum implements InterfazListadoLabels<String> {

  /** The no existe. */
  NO_EXISTE("0", "label.propuesta_pia.datos_economicos.estado_0"),

  /** The confirmados. */
  CONFIRMADOS("1", "label.propuesta_pia.datos_economicos.estado_1"),

  /** The no confirmados. */
  NO_CONFIRMADOS("2", "label.propuesta_pia.datos_economicos.estado_2"),

  /** The modificados. */
  MODIFICADOS("3", "label.propuesta_pia.datos_economicos.estado_3");

  /** The valor. */
  private String valor;

  /** The label. */
  private String label;

  /**
   * Constructor. *
   *
   * @param valor the valor
   * @param label the label
   */
  private EstadoDatosEconomicosEnum(final String valor, final String label) {
    this.valor = valor;
    this.label = label;
  }



  /**
   * From string.
   *
   * @param text the text
   * @return the estado incidencia enum
   */
  public static EstadoDatosEconomicosEnum fromString(final String text) {
    return Arrays.asList(EstadoDatosEconomicosEnum.values()).stream()
        .filter(opsc -> opsc.valor.equals(text)).findFirst().orElse(null);
  }

  /**
   * Convierte enum a String.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return valor;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  @Override
  public String getValor() {
    return valor;
  }



  /**
   * Gets the label.
   *
   * @return the label
   */
  @Override
  public String getLabel() {
    return label;
  }



}
