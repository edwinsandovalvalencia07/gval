package com.gval.gval.common.enums.listados;

/**
 * The Enum TipoDocumentoContableEnum.
 */
public enum TipoDocumentoContableEnum {

  /** The r. */
  R("R", "label.documentos_contables.tipo_documento_R", null, false),
  /** The a. */
  A("A", "label.documentos_contables.tipo_documento_A", null, false),
  /** The oda. */
  ODA("ODA", "label.documentos_contables.tipo_documento_ODA", null, false),
  /** The r com. */
  R_COM_PLUS("R_COM", "label.documentos_contables.tipo_documento_R_COM_PLUS",
      "PLUS", false),

  /** The r com min. */
  R_COM_MIN("R_COM", "label.documentos_contables.tipo_documento_R_COM_MIN",
      "MINUS", false),
  /** The a com. */
  A_COM_PLUS("A_COM", "label.documentos_contables.tipo_documento_A_COM_PLUS",
      "PLUS", false),

  /** The a com min. */
  A_COM_MIN("A_COM", "label.documentos_contables.tipo_documento_A_COM_MIN",
      "MINUS", false);

  /** The valor. */
  private final String valor;

  /** The label. */
  private final String label;

  /** The tipo complementario. */
  private final String tipoComplementario;

  /** The activa combos. */
  private final Boolean activaCombos;



  /**
   * Instantiates a new tipo resolucion masiva listado enum.
   *
   * @param valor the valor
   * @param label the label
   * @param tipoComplementario the tipo complementario
   * @param activaCombos the activa combos
   */
  private TipoDocumentoContableEnum(final String valor, final String label,
      final String tipoComplementario, final Boolean activaCombos) {
    this.valor = valor;
    this.label = label;
    this.tipoComplementario = tipoComplementario;
    this.activaCombos = activaCombos;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * Gets the tipo complementario.
   *
   * @return the tipo complementario
   */
  public String getTipoComplementario() {
    return tipoComplementario;
  }

  /**
   * Gets the activa combos.
   *
   * @return the activa combos
   */
  public Boolean getActivaCombos() {
    return activaCombos;
  }

}
