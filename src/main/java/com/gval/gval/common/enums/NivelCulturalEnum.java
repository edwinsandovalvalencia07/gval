package com.gval.gval.common.enums;

import com.gval.gval.common.enums.listados.InterfazListadoLabels;

import java.util.Arrays;

/**
 * The Enum NivelCulturalEnum.
 */
public enum NivelCulturalEnum implements InterfazListadoLabels<Long> {

  /** The sin lectoescritura. */
  SIN_LECTOESCRITURA(1L, "label.informe_social.lectoescritura"),

  /** The certificado escolaridad. */
  CERTIFICADO_ESCOLARIDAD(2L, "label.informe_social.certificado_escolaridad"),

  /** The estudios primarios. */
  ESTUDIOS_PRIMARIOS(3L, "label.informe_social.estudio_primario"),

  /** The graduado escolar. */
  GRADUADO_ESCOLAR(4L, "label.informe_social.graduado_escolar"),

  /** The graduado eso. */
  GRADUADO_ESO(5L, "label.informe_social.graduado_eso"),

  /** The ensenyanza grado medio. */
  ENSENYANZA_GRADO_MEDIO(6L, "label.informe_social.ensenyanza_media"),

  /** The ensenyanza grado superior. */
  ENSENYANZA_GRADO_SUPERIOR(7L, "label.informe_social.ensenyanza_superior"),

  /** The bachiller. */
  BACHILLER(8L, "label.informe_social.bachiller"),

  /** The universitario. */
  UNIVERSITARIO(9L, "label.informe_social.estudios_universitarios");

  /** The valor. */
  private Long valor;

  /** The label. */
  private String label;

  /**
   * Instantiates a new nivel cultural enum.
   *
   * @param valor the valor
   * @param label the label
   */
  private NivelCulturalEnum(final Long valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  @Override
  public Long getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  @Override
  public String getLabel() {
    return label;
  }


  /**
   * From Long.
   *
   * @param valor the text
   * @return the nivel cultural enum
   */
  public static NivelCulturalEnum fromLong(final Long valor) {
    return Arrays.asList(NivelCulturalEnum.values()).stream()
        .filter(nc -> nc.valor.equals(valor)).findFirst().orElse(null);
  }
}
