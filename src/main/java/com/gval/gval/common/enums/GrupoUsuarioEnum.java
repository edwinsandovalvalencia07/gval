package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;

/**
 * The Enum GrupoValoracionEnum.
 */
public enum GrupoUsuarioEnum {

  // @formatter:off
  /** The sanidad. */
  SANIDAD("SA", 1L),
  
  /** The Residencia. */
  RESIDENCIA("RE", 2L),
  
  /** The cipi. */
  CIPI("CI", 3L),
  
  /** The ss gg. */
  SS_GG("SS", 4L),
  
  /** The ivas. */
  IVAS("IV", 5L);
  // @formatter:on

  /** The codigo. */
  private String codigo;

  /** The tipo clau. */
  private Long tipoClau;

  /**
   * Instantiates a new pantalla enum.
   *
   * @param codigo the codigo
   * @param tipoClau the tipo clau
   */
  private GrupoUsuarioEnum(final String codigo, final Long tipoClau) {
    this.codigo = codigo;
    this.tipoClau = tipoClau;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Gets the tipo clau.
   *
   * @return the tipo clau
   */
  public Long getTipoClau() {
    return tipoClau;
  }

  /**
   * From codigo.
   *
   * @param codigo the codigo
   * @return the pantalla enum
   */
  @Nullable
  public static GrupoUsuarioEnum fromCodigo(final String codigo) {
    for (final GrupoUsuarioEnum gu : values()) {
      if (gu.codigo.equals(codigo)) {
        return gu;
      }
    }
    return null;
  }

  @Nullable
  public static GrupoUsuarioEnum fromTipoClau(final Long tipoClau) {
    for (final GrupoUsuarioEnum gu : values()) {
      if (gu.tipoClau.equals(tipoClau)) {
        return gu;
      }
    }
    return null;
  }
}
