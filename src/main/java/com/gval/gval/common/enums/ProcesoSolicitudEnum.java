package com.gval.gval.common.enums;

/**
 * The Enum ProcesoSolicitudEnum.
 */
public enum ProcesoSolicitudEnum {

  /** The grabacion comprobacion. */
  // @formatter:off
  GRABACION_COMPROBACION(1L),
  /** The valoracion. */
  VALORACION(2L),
  /** The estudio grado. */
  ESTUDIO_GRADO(4L),
  /** The resuelto grado. */
  RESUELTO_GRADO(5L),
  /** The fase propuesta pia. */
  FASE_PROPUESTA_PIA(6L),
  /** The resuelto pia. */
  RESUELTO_PIA(7L),
  /** The finalizado. */
  FINALIZADO(10L);
  // @formatter:on

  /** The value. */
  private Long value;

  /**
   * Instantiates a new proceso solicitud enum.
   *
   * @param value the value
   */
  ProcesoSolicitudEnum(final Long value) {
    this.value = value;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public Long getValue() {
    return value;
  }
}
