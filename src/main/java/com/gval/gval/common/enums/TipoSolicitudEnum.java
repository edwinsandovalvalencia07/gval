package com.gval.gval.common.enums;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.Nullable;



// TODO: Auto-generated Javadoc
/**
 * The Enum TipoSolicitudEnum.
 */
public enum TipoSolicitudEnum {


  /** The tipo solicitud homologacion. */
  HOMOLOGACION("H", "Homologacion", 2L),

  /** The tipo solicitud revision. */
  REVISION("R", "Revision", 3L),

  /** The tipo solicitud valoracion. */
  VALORACION("V", "Valoracion", 1L);


  // @formatter:on
  /** The value. */

  private final String valor;

  /** The descripcion. */
  private final String descripcion;

  /** The pk tipo soli. */
  private final Long pkTipoSoli;


  /**
   * Instantiates a new tipo solicitud enum.
   *
   * @param valor the valor
   * @param descripcion the descripcion
   * @param pkTipoSoli the pk tipo soli
   */
  TipoSolicitudEnum(final String valor, final String descripcion, Long pkTipoSoli) {
    this.valor = valor;
    this.descripcion = descripcion;
    this.pkTipoSoli = pkTipoSoli;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }


  /**
   * Gets the pk tipo soli.
   *
   * @return the pk tipo soli
   */
  public Long getPkTipoSoli() {
    return pkTipoSoli;
  }

  /**
   * From string.
   *
   * @param valor the valor
   * @return the tipo solicitud enum
   */
  @Nullable
  public static TipoSolicitudEnum fromValor(final String valor) {
    if (valor == null) {
      return null;
    }
    return Arrays.stream(TipoSolicitudEnum.values())
        .filter(v -> v.valor.equals(valor)).findFirst().orElseThrow(
            () -> new IllegalArgumentException("Valor desconocido: " + valor));
  }


  /**
   * From descripcion.
   *
   * @param descripcion the descripcion
   * @return the tipo solicitud enum
   */
  @Nullable
  public static TipoSolicitudEnum fromDescripcion(final String descripcion) {
    if (descripcion == null) {
      return null;
    }
    return Arrays.stream(TipoSolicitudEnum.values())
        .filter(v -> StringUtils.stripAccents(v.descripcion)
            .equalsIgnoreCase(StringUtils.stripAccents(descripcion)))
        .findFirst().orElseThrow(() -> new IllegalArgumentException(
            "Valor desconocido: " + descripcion));
  }

  /**
   * From string.
   *
   * @param text the text
   * @return the tipo solicitud enum
   */
  @Nullable
  public static TipoSolicitudEnum fromString(final String text) {

    if (text == null) {
      return null;
    }

    for (final TipoSolicitudEnum ee : TipoSolicitudEnum.values()) {
      if (ee.valor.equalsIgnoreCase(text)) {
        return ee;
      }
    }
    return null;
  }
}


