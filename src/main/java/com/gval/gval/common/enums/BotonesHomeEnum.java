//package com.gval.gval.common.enums;
//
//import es.gva.dependencia.ada.common.ConstantesCommand;
//import es.gva.dependencia.ada.common.ConstantesIconos;
//
//import java.util.Arrays;
//
//// TODO: Auto-generated Javadoc
///**
// * The Enum TipoListadoEnum.
// */
//public enum BotonesHomeEnum {
//
//	/** ****************** ACCESO EXPEDIENTE ********************************. */
//	/** The btn consulta expedientes. */
//	BTN_CONSULTA_EXPEDIENTES(IdentificadorCasoUsoEnum.MENU_CONSULTA_EXPEDIENTE.getValor(), Grupos.ACCESO_EXPEDIENTE,
//			"label.titulo.menu_consulta_expedientes", ConstantesIconos.ICO_BUSCAR_EXPEDIENTE_SVG,
//			ConstantesIconos.ICO_BUSCAR_EXPEDIENTE_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.EXPEDIENTES.getValor()),
//
//	/** The btn buscar solicitudes. */
//	BTN_BUSCAR_SOLICITUDES(IdentificadorCasoUsoEnum.MENU_BUSCAR_SOLICITUDES.getValor(), Grupos.ACCESO_EXPEDIENTE,
//			"label.titulo.menu_buscar_solicitudes", ConstantesIconos.ICO_SOLICITUDES_SVG,
//			ConstantesIconos.ICO_SOLICITUDES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.SOLICITUDES.getValor()),
//
//	/** ****************** GRABACION ********************************. */
//	/** The btn nueva solicitud. */
//	BTN_NUEVA_SOLICITUD(IdentificadorCasoUsoEnum.MENU_NUEVO_EXPEDIENTE.getValor(), Grupos.GRABACION,
//			"label.titulo.nuevo-expediente", ConstantesIconos.ICO_NUEVA_SOLICITUDES_SVG,
//			ConstantesIconos.ICO_NUEVA_SOLICITUDES_HOVER_SVG, ConstantesCommand.NAVEGAR_DETALLE, null),
//
//	/** The btn verificar documentos. */
//	BTN_VERIFICAR_DOCUMENTOS(IdentificadorCasoUsoEnum.MENU_LISTADO_DOCUMENTOS_PDTE_VERIFICAR.getValor(),
//			Grupos.GRABACION, "label.titulo.menu_documentacion_pdte_verificar", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.VALIDAR_DOCUMENTOS.getValor()),
//
//	/** ****************** VALORACION ********************************. */
//	/** The btn asignar solicitud. */
//	BTN_ASIGNAR_SOLICITUD(IdentificadorCasoUsoEnum.MENU_ASIGNAR_SOLICITUD.getValor(), Grupos.VALORACION,
//			"label.titulo.asignar_solicitud", ConstantesIconos.ICO_ASIGNAR_SOLICITUDES_SVG,
//			ConstantesIconos.ICO_ASIGNAR_SOLICITUDES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.ASIGNAR_SOLICITUD.getValor()),
//
//	/** The btn citar valoracion. */
//	BTN_CITAR_VALORACION(IdentificadorCasoUsoEnum.MENU_CITACION_VALORACION.getValor(), Grupos.VALORACION,
//			"label.titulo.citacion", ConstantesIconos.ICO_CITACION_SVG, ConstantesIconos.ICO_CITACION_HOVER_SVG,
//			ConstantesCommand.NAVEGAR_LISTADO, TipoListadoEnum.CITAR_SOLICITUDES.getValor()),
//
//	/** ****************** INFORME SOCIAL ****************************. */
//	/** The btn informe social. */
//	BTN_INFORME_SOCIAL(IdentificadorCasoUsoEnum.MENU_INFORME_SOCIAL.getValor(), Grupos.INFORME_SOCIAL,
//			"label.titulo.informes_sociales", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoInformeSocialListadoEnum.INFORMES_SOCIALES_PENDIENTES.getValor()),
//
//	/** ****************** PIA ****************************************. */
//	/** The btn pias sad. */
//	BTN_PIAS_SAD(IdentificadorCasoUsoEnum.MENU_LISTADO_PROPUESTA_PIA.getValor(), Grupos.PIA,
//			"label.titulo.menu-pias-sad", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.PROPUESTAS_PIA.getValor()),
//
//	/** ****************** DOCUMENTACION *******************************. */
//	/** The btn acuses generados. */
//	BTN_ACUSES_GENERADOS(IdentificadorCasoUsoEnum.MENU_LISTADO_ACUSES_GENERADOS.getValor(), Grupos.DOCUMENTACION,
//			"label.titulo.menu_listado_acuses_generados", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.ACUSES_GENERADOS.getValor()),
//
//	/** ****************** LISTADOS ************************************. */
//	/** The btn subsanaciones pendientes. */
//	BTN_SUBSANACIONES_PENDIENTES(IdentificadorCasoUsoEnum.MENU_LISTADO_SUBSANACIONES_PENDIENTES.getValor(),
//			Grupos.LISTADOS, "label.titulo.menu_subsanaciones_pendientes", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.SUBSANACIONES_PENDIENTES.getValor()),
//
//	/** The btn subsanaciones tesoreria. */
//	BTN_SUBSANACIONES_TESORERIA(IdentificadorCasoUsoEnum.MENU_LISTADO_SUBSANACIONES_TESORERIA.getValor(),
//			Grupos.LISTADOS, "label.titulo.menu_subsanaciones_tesoreria", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.SUBSANACIONES_TESORERIA.getValor()),
//
//	/** The btn cuidadores. */
//	BTN_CUIDADORES(IdentificadorCasoUsoEnum.MENU_LISTADO_CUIDADORES.getValor(), Grupos.LISTADOS,
//			"label.titulo.menu_listado_cuidadores", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.CUIDADORES.getValor()),
//
//	/** The btn diarios. */
//	BTN_DIARIOS(IdentificadorCasoUsoEnum.MENU_LISTADOS_DIARIOS.getValor(), Grupos.LISTADOS,
//			"label.titulo.home_listados_diarios", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.LISTADOS_DIARIOS.getValor()),
//
//	/** The btn cambios asistente personal. */
//	BTN_CAMBIOS_ASISTENTE_PERSONAL(IdentificadorCasoUsoEnum.MENU_LISTADOS_CAMBIOS_ASISTENTE_PERSONAL.getValor(),
//			Grupos.LISTADOS, "label.titulo.home_listados_cambios_asistente_personal",
//			ConstantesIconos.ICO_RESOLUCIONES_SVG, ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG,
//			ConstantesCommand.NAVEGAR_LISTADO, TipoListadoEnum.LISTADOS_CAMBIOS_ASISTENTE_PERSONAL.getValor()),
//
//	/** The btn nefis. */
//	BTN_NEFIS(IdentificadorCasoUsoEnum.MENU_LISTADO_NEFIS.getValor(), Grupos.NEFIS, "label.titulo.menu_listados_nefis",
//			ConstantesIconos.ICO_RESOLUCIONES_SVG, ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG,
//			ConstantesCommand.NAVEGAR_LISTADO, TipoListadoEnum.LISTADOS_NEFIS.getValor()),
//
//	/** The btn generacion doc contable. */
//	BTN_GENERACION_DOC_CONTABLE(IdentificadorCasoUsoEnum.MENU_CONSULTA_EXPEDIENTE.getValor(),
//			/** The btn ingresos pendientes. */
//			// IdentificadorCasoUsoEnum.MENU_LISTADO_GENERACION_DOC_CONTABLE.getValor()
//			Grupos.NEFIS, "label.titulo.home_documentos_contables", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.LISTADO_DOCUMENTOS_CONTABLES.getValor()),
//
//	/** The btn ingresos pendientes. */
//	BTN_INGRESOS_PENDIENTES(IdentificadorCasoUsoEnum.MENU_LISTADO_INGRESOS_PENDIENTES.getValor(),
//			Grupos.INGRESOS_PENDIENTES, "label.titulo.ingresos_pendientes_btn", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.LISTADOS_INGRESOS_PENDIENTES.getValor()),
//
//	/** ****************** CONSULTA ********************************. */
//	/** The btn consulta 012. */
//	BTN_CONSULTA_012(IdentificadorCasoUsoEnum.MENU_CONSULTA_012.getValor(), Grupos.CONSULTA,
//			"label.titulo.home_listados_consulta012", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.LISTADOS_CONSULTA_012.getValor()),
//
//	/** The btn tramite solicitud. */
//	BTN_TRAMITE_SOLICITUD(IdentificadorCasoUsoEnum.MENU_TRAMITE_TELEMATICO.getValor(), Grupos.CONSULTA,
//			"label.titulo.home_tramite_telematico", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.LISTADOS_TRAMITE_TELEMATICO.getValor()),
//
//	/** The btn buscar solicitudes extendidas. */
//	BTN_BUSCAR_SOLICITUDES_EXT(IdentificadorCasoUsoEnum.MENU_BUSCAR_SOLICITUDES_EXT.getValor(), Grupos.LISTADOS,
//			"label.titulo.menu_buscar_solicitudes_ext", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.SOLICITUDES_EXT.getValor()),
//
//	/** The btn buscar solicitudes ist seguimiento. */
//	BTN_EXPEDIENTES_PIA(IdentificadorCasoUsoEnum.MENU_BUSCAR_EXPEDIENTES_PIA.getValor(), Grupos.LISTADOS,
//			"label.titulo.menu_buscar_expedientes_pia", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.EXPEDIENTES_PIA.getValor()),
//
//	/** The btn Informes técnicos de seguimiento desfavorables. */
//	BTN_INFORMES_TECNICOS_DESFAVORABLES(IdentificadorCasoUsoEnum.MENU_BUSCAR_IT_DESFABORABLES.getValor(),
//			Grupos.LISTADOS, "label.titulo.menu_seguimiento_desfavorables", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.INFORMES_TECNICOS_DESFAVORABLE.getValor()),
//
//	/** The btn espera imserso pia. */
//	BTN_ESPERA_IMSERSO_PIA(IdentificadorCasoUsoEnum.MENU_LISTADO_ESPERA_IMSERSO_PIA.getValor(), Grupos.NSISAAD,
//			"label.titulo.home_listados_espera", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.LISTADOS_ESPERA_IMSERSO_PIA.getValor()),
//	/** The btn rechazos imserso. */
//	BTN_RECHAZOS_IMSERSO(IdentificadorCasoUsoEnum.MENU_LISTADO_RECHAZOS_IMSERSO.getValor(), Grupos.NSISAAD,
//			"label.titulo.home_rechazos", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.LISTADOS_RECHAZOS_MENOR_FECHA_GRADO.getValor()),
//	/** The consulta valoraciones sigma. */
//	CONSULTA_VALORACIONES_SIGMA(IdentificadorCasoUsoEnum.MENU_VALORACIONES_SIGMA.getValor(), Grupos.VALORACION,
//			"label.titulo.home_valoraciones_sigma", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.CONSULTA_VALORACIONES_SIGMA.getValor()),
//
//	/** The btn resolucion masiva. */
//	BTN_RESOLUCION_MASIVA(IdentificadorCasoUsoEnum.MENU_LISTADO_RESOLUCION_MASIVA.getValor(), Grupos.RESOLUCIONES,
//			"label.titulo.home_resolucion_masiva", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.LISTADOS_RESOLUCION_MASIVA.getValor()),
//
//	/** The btn impresion. */
//	BTN_REIMPRESION(IdentificadorCasoUsoEnum.MENU_LISTADO_IMPRESIONES.getValor(), Grupos.DOCUMENTACION,
//			"label.titulo.menu.impresion", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//			TipoListadoEnum.LISTADO_REIMPRESION.getValor()),
//
//	/** The btn estudios pendientes. */
//  BTN_ESTUDIOS_PENDIENTES(IdentificadorCasoUsoEnum.MENU_ESTUDIOS_PENDIENTES.getValor(), Grupos.ESTUDIOS_PENDIENTES,
//			"label.titulo.menu_estudios.pendientes", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//			ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO,
//      TipoListadoEnum.LISTADO_ESTUDIOS_PENDIENTES.getValor()),
//
//  /** The btn acta masiva. */
//  BTN_ACTA_MASIVA(IdentificadorCasoUsoEnum.MENU_CONSULTA_EXPEDIENTE.getValor(), Grupos.ACTAS, "label.titulo.menu_acta_masiva", ConstantesIconos.ICO_RESOLUCIONES_SVG,
//      ConstantesIconos.ICO_RESOLUCIONES_HOVER_SVG, ConstantesCommand.NAVEGAR_LISTADO, TipoListadoEnum.LISTADO_ACTA_MASIVA.getValor());
//
//	/** The valor. */
//	private final String accessRole;
//
//	/** The grupo label. */
//	private final String grupoLabel;
//
//	/** The label. */
//	private final String label;
//
//	/** The image. */
//	private final String image;
//
//	/** The image hover. */
//	private final String hoverImage;
//
//	/** The command. */
//	private final String command;
//
//	/** The id listado. */
//	private final String idListado;
//
//	/**
//	 * Instantiates a new aplicacion externa enum.
//	 *
//	 * @param accessRole the access role
//	 * @param grupoLabel the grupo label
//	 * @param label      the label
//	 * @param image      the image
//	 * @param hoverImage the hover image
//	 * @param command    the command
//	 * @param idListado  the id listado
//	 */
//	private BotonesHomeEnum(final String accessRole, final String grupoLabel, final String label, final String image,
//			final String hoverImage, final String command, final String idListado) {
//		this.accessRole = accessRole;
//		this.grupoLabel = grupoLabel;
//		this.label = label;
//		this.image = image;
//		this.hoverImage = hoverImage;
//		this.command = command;
//		this.idListado = idListado;
//	}
//
//	/**
//	 * Gets the valor.
//	 *
//	 * @return the valor
//	 */
//	public String getAccessRole() {
//		return accessRole;
//	}
//
//	/**
//	 * Gets the grupo label.
//	 *
//	 * @return the grupo label
//	 */
//	public String getGrupoLabel() {
//		return grupoLabel;
//	}
//
//	/**
//	 * Gets the label.
//	 *
//	 * @return the label
//	 */
//	public String getLabel() {
//		return label;
//	}
//
//	/**
//	 * Gets the image.
//	 *
//	 * @return the image
//	 */
//	public String getImage() {
//		return image;
//	}
//
//	/**
//	 * Gets the hover image.
//	 *
//	 * @return the hover image
//	 */
//	public String getHoverImage() {
//		return hoverImage;
//	}
//
//	/**
//	 * Gets the command.
//	 *
//	 * @return the command
//	 */
//	public String getCommand() {
//		return command;
//	}
//
//	/**
//	 * Gets the id listado.
//	 *
//	 * @return the id listado
//	 */
//	public String getIdListado() {
//		return idListado;
//	}
//
//	/**
//	 * From string.
//	 *
//	 * @param text the text
//	 * @return the tipo informe social listado enum
//	 */
//	public static BotonesHomeEnum fromString(final String text) {
//		return Arrays.asList(BotonesHomeEnum.values()).stream().filter(opsc -> opsc.accessRole.equals(text)).findFirst()
//				.orElse(null);
//	}
//
//	/**
//	 * The Class Grupos.
//	 */
//	private static class Grupos {
//
//		/** The Constant ACCESO_EXPEDIENTE. */
//		public static final String ACCESO_EXPEDIENTE = "label.titulo.acceso-expediente";
//
//		/** The Constant CONSULTA. */
//		public static final String CONSULTA = "label.titulo.consulta";
//
//		/** The Constant GRABACION. */
//		public static final String GRABACION = "label.titulo.grabacion";
//
//		/** The Constant VALORACION. */
//		public static final String VALORACION = "label.titulo.valoracion";
//
//		/** The Constant INFORME_SOCIAL. */
//		public static final String INFORME_SOCIAL = "label.titulo.informe_social";
//
//		/** The Constant PIA. */
//		public static final String PIA = "label.titulo.pia";
//
//		/** The Constant DOCUMENTACION. */
//		public static final String DOCUMENTACION = "label.titulo.menu_documentacion";
//
//		/** The Constant LISTADOS. */
//		public static final String LISTADOS = "label.titulo.menu_listados";
//
//		/** The Constant NSISAAD. */
//		public static final String NSISAAD = "label.titulo.nsisaad";
//
//		/** The Constant NEFIS. */
//		public static final String NEFIS = "label.titulo.nefis";
//
//		/** The Constant NEFIS. */
//		public static final String INGRESOS_PENDIENTES = "label.titulo.ingresos_pendientes";
//
//		/** The Constant RESOLUCIONES. */
//		public static final String RESOLUCIONES = "label.titulo.resoluciones";
//
//		/** The Constant IMPRESIONES. */
//		public static final String IMPRESIONES = "label.titulo.impresion";
//
//		/** The Constant ACTAS. */
//		public static final String ACTAS = "label.titulo.actas";
//
//		/** The Constant ESTUDIOS_PENDIENTES. */
//		public static final String ESTUDIOS_PENDIENTES = "label.titulo.estudios.pendientes";
//
//	}
//
//}
