package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;

/**
 * The Enum ModalidadEnum.
 */
public enum ModalidadEnum {

  /** The fija. */
  FIJA("FIJ", 0L),

  /** The movil. */
  MOVIL("MOV", 1L),

  /** The suficiente autonomia. */
  SUFICIENTE_AUTONOMIA("AUT", 0L),

  /** The deterioro cognitivo. */
  DETERIORO_COGNITIVO("DCO", 1L),

  /** The atendido familiares. */
  ATENDIDO_FAMILIARES("AFA", 2L);

  /** The codigo. */
  private String codigo;

  /** The valor. */
  private Long valor;

  /**
   * Instantiates a new modalidad enum.
   *
   * @param codigo the codigo
   * @param valor the valor
   */
  private ModalidadEnum(final String codigo, final Long valor) {
    this.codigo = codigo;
    this.valor = valor;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public Long getValor() {
    return valor;
  }

  /**
   * From codigo.
   *
   * @param codigo the codigo
   * @return the modalidad enum
   */
  @Nullable
  public static ModalidadEnum fromCodigo(final String codigo) {

    if (codigo == null) {
      return null;
    }

    for (final ModalidadEnum mod : ModalidadEnum.values()) {
      if (mod.codigo.equalsIgnoreCase(codigo)) {
        return mod;
      }
    }
    return null;
  }
}
