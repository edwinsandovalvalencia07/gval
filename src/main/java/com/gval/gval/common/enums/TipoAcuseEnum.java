package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;

/**
 * The Enum TipoAcuseEnum.
 */
public enum TipoAcuseEnum {

  /** The entregado. */
  ENTREGADO("OK", "label.tipoacuse.entregado"),

  /** The direccion incorrecta. */
  DIRECCION_INCORRECTA("DI", "label.tipoacuse.direccion_incorrecta"),

  /** The ausente reparto. */
  AUSENTE_REPARTO("AR", "label.tipoacuse.ausente_reparto"),

  /** The desconocido. */
  DESCONOCIDO("DE", "label.tipoacuse.desconocido"),

  /** The rehusado. */
  REHUSADO("RE", "label.tipoacuse.rehusado"),

  /** The fallecido. */
  FALLECIDO("FA", "label.tipoacuse.fallecido"),

  /** The no hace cargo. */
  NO_HACE_CARGO("NC", "label.tipoacuse.no_hace_cargo");

  /** The valor. */
  private String valor;

  /** The label. */
  private String label;

  /**
   * Instantiates a new tipo acuse enum.
   *
   * @param codigo the codigo
   * @param label the label
   */
  TipoAcuseEnum(final String codigo, final String label) {
    this.valor = codigo;
    this.label = label;
  }

  /**
   * From valor.
   *
   * @param valor the valor
   * @return the tipo acuse enum
   */
  @Nullable
  public static TipoAcuseEnum fromValor(final String valor) {

    if (valor == null) {
      return null;
    }

    for (final TipoAcuseEnum td : TipoAcuseEnum.values()) {
      if (td.valor.equalsIgnoreCase(valor)) {
        return td;
      }
    }
    return null;
  }


  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }


  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }
}
