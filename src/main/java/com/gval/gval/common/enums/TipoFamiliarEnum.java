package com.gval.gval.common.enums;

/**
 * The Enum TipoFamiliarEnum.
 */
public enum TipoFamiliarEnum {


  /** The abuelo a. */
  // @formatter:off
  ABUELO_A(6L),
  
  /** The allegado a. */
  ALLEGADO_A(22L),
  
  /** The bisabuelo a. */
  BISABUELO_A(12L),
  
  /** The biznieto a. */
  BIZNIETO_A(13L),
  
  /** The con contrato laboral. */
  CON_CONTRATO_LABORAL(23L),
  
  /** The cunyado a. */
  CUNYADO_A(8L),
  
  /** The conyuge. */
  CONYUGE(1L),
  
  /** The hermano a. */
  HERMANO_A(7L),
  
  /** The hijo a. */
  HIJO_A(4L),
  
  /** The nieto a. */
  NIETO_A(9L),
  
  /** The otros familiares. */
  OTROS_FAMILIARES(17L),
  
  /** The padre madre. */
  PADRE_MADRE(2L),
  
  /** The pareja de hecho. */
  PAREJA_DE_HECHO(18L),
  
  /** The primer grado. */
  PRIMER_GRADO(15L),
  
  /** The primo a. */
  PRIMO_A(19L),
  
  /** The segundo grado. */
  SEGUNDO_GRADO(16L),
  
  /** The sin relacion de parentesco. */
  SIN_RELACION_DE_PARENTESCO(14L),
  
  /** The sobrino a. */
  SOBRINO_A(11L),
  
  /** The suegro a. */
  SUEGRO_A(3L),
  
  /** The tatarabuelo a. */
  TATARABUELO_A(20L),
  
  /** The tataranieto a. */
  TATARANIETO_A(21L),
  
  /** The tio a. */
  TIO_A(10L),
  
  /** The yerno nuera. */
  YERNO_NUERA(5L);
  // @formatter:on


  /** The value. */
  private Long value;

  /**
   * Instantiates a new autorizacion acceso datos enum.
   *
   *
   * @param value the value
   */
  TipoFamiliarEnum(final Long value) {
    this.value = value;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public Long getValue() {
    return value;
  }
}
