package com.gval.gval.common.enums;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import jakarta.annotation.Nullable;


/**
 * The Enum MotivoGeneracionEnum.
 */
public enum MotivoGeneracionEnum {

  /** The alegacion. */
  ALEGACION("ALE", false),

  /** The dictamen. */
  DICTAMEN("DIC", false),

  /** The traslado entrante. */
  TRASLADO_ENTRANTE("TRA", false),

  /** The subsanacion. */
  SUBSANACION("SUB", false),

  /** The nuevas preferencias. */
  NUEVAS_PREFERENCIAS("PNP", true),

  /** The propuesta pia discordante. */
  PROPUESTA_PIA_DISCORDANTE("PPD", false),

  /** The cambio de cuidador. */
  CAMBIO_DE_CUIDADOR("PCC", true),

  /** The cambio de asistente. */
  CAMBIO_DE_ASISTENTE("CAP", true),

  /** The seguimiento. */
  SEGUIMIENTO("SEG", true),

  /** The otros. */
  OTROS("OTR", true),

  /** The paso comprobada. */
  PASO_COMPROBADA("COM", false),

  /** The no indicado. */
  NO_INDICADO("NIN", false),

  /** The comun. */
  REV_HORAS("SRH", true),

  /** The comun. */
  COMUN("", false);


  /** The label. */
  private String codigo;

  /** The manual. */
  private boolean manual;

  /**
   * Instantiates a new situacion no activo enum.
   *
   * @param codigo the codigo
   * @param manual the manual
   */
  private MotivoGeneracionEnum(final String codigo, final boolean manual) {
    this.codigo = codigo;
    this.manual = manual;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Checks if is manual.
   *
   * @return true, if is manual
   */
  public boolean isManual() {
    return manual;
  }

  /**
   * Gets the motivos manuales.
   *
   * @return the motivos manuales
   */
  public static List<MotivoGeneracionEnum> getMotivosManuales() {
    return Arrays.asList(MotivoGeneracionEnum.values()).stream()
        .filter(mg -> mg.manual).collect(Collectors.toList());
  }

  /**
   * From codigo.
   *
   * @param codigo the codigo
   * @return the motivo generacion enum
   */
  @Nullable
  public static MotivoGeneracionEnum fromCodigo(final String codigo) {

    if (codigo == null) {
      return null;
    }

    for (final MotivoGeneracionEnum mg : MotivoGeneracionEnum.values()) {
      if (mg.codigo.equalsIgnoreCase(codigo)) {
        return mg;
      }
    }
    return null;
  }
}
