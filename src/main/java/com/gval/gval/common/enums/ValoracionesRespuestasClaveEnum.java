package com.gval.gval.common.enums;

import java.util.stream.Stream;

/**
 * The Enum ValoracionesRespuestasClaveEnum.
 */
public enum ValoracionesRespuestasClaveEnum {


  /** The ninguno. */
  NINGUNO("NINGUNO", null),

  /** The label. */
  LABEL("LABEL", "LB"),

  /** The sino. */
  SINO("SINO", "SN"),

  /** The DESEMPEÑO. */
  DESEMPENYO("DESEMPEÑO", "D"),

  /** The frecuencia. */
  FRECUENCIA("FRECUENCIA", "FR"),

  /** The notas. */
  NOTAS("NOTAS", "NT"),

  /** The problema. */
  PROBLEMA("PROBLEMA", "P"),

  /** The peso. */
  PESO("PESO", "PE"),

  /** The tipo apoyo. */
  TIPO_APOYO("TIPO_APOYO", "TA");

  /** The nombre. */
  public final String nombre;

  /** The pre id. */
  public final String id;

  /**
   * Instantiates a new valoraciones sub preguntas tipo enum.
   *
   * @param nombre the nombre
   * @param id the pre id
   */
  private ValoracionesRespuestasClaveEnum(final String nombre,
      final String id) {
    this.nombre = nombre;
    this.id = id;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Gets the pre id.
   *
   * @return the pre id
   */
  public String getId() {
    return id;
  }

  /**
   * Stream.
   *
   * @return the stream
   */
  public static Stream<ValoracionesRespuestasClaveEnum> stream() {
    return Stream.of(ValoracionesRespuestasClaveEnum.values());
  }

  /**
   * Gets the by id.
   *
   * @param id the id
   * @return the by id
   */
  public static ValoracionesRespuestasClaveEnum getById(final String id) {
    for (final ValoracionesRespuestasClaveEnum e : values()) {
      if (e.name().equals(id)) {
        return e;
      }
    }
    return NINGUNO;
  }
}
