package com.gval.gval.common.enums;

import java.util.Arrays;
import java.util.List;

import jakarta.annotation.Nullable;

// TODO: Auto-generated Javadoc
/**
 * The Enum EstadoSolicitudEnum.
 */
public enum EstadoSolicitudEnum {

  /** The borrador. */
  // @formatter:off
  BORRADOR("BO"),

  /** The grabada. */
  GRABADA("GR"),

  /** The comprobada. */
  COMPROBADA("CO"),

  /** The asignada. */
  ASIGNADA("AS"),

  /** The citada. */
  CITADA("CI"),

  /** The valorada. */
  VALORADA("VA"),

  /** The aplazada. */
  APLAZADA("AZ"),

  /** The estudiada. */
  ESTUDIADA("ES"),

  /** The dictamen tecnico. */
  DICTAMEN_TECNICO("DT"),

  /** The aprobada. */
  APROBADA("AP"),

  /** The resuelta gyn. */
  RESUELTA_GYN("GN"),

  /** The resuelta desestimacion. */
  RESUELTA_DESESTIMACION("RD"),

  /** The resuelta renuncia. */
  RESUELTA_RENUNCIA("RR"),

  /** The resolucion fallecimiento. */
  RESOLUCION_FALLECIMIENTO("RF"),

  /** The resuelta caducidad. */
  RESUELTA_CADUCIDAD("RC"),

  /** The resuelta traslado. */
  RESUELTA_TRASLADO("RT"),

  /** The resuelta desistimiento. */
  RESUELTA_DESISTIMIENTO("DS"),

  /** The resuelta archivo. */
  RESUELTA_ARCHIVO("RA"),

  /** The resuelta pia. */
  RESUELTA_PIA("PI"),

  /** The resuelta inadmision. */
  RESUELTA_INADMISION("RI"),

  /** The resuelta fallecimiento. */
  RESUELTA_FALLECIMIENTO("RF"),

  /** The resolucion gyn notificada. */
  RESOLUCION_GYN_NOTIFICADA("GNN"),

  /** The propuesta pia confirmada. */
  PROPUESTA_PIA_CONFIRMADA("PIC"),

  /** The propuesta pia notificada. */
  PROPUESTA_PIA_NOTIFICADA("PIN"),

  /** The propuesta pia en proceso. */
  PROPUESTA_PIA_EN_PROCESO("PIP"),

  /** The resolucion pia notificada. */
  RESOLUCION_PIA_NOTIFICADA("RPN"),

  /** The archivo caducidad pvs sad. */
  ARCHIVO_CADUCIDAD_PVS_SAD("ACS"),

  /** The resuelto archivo caducidad pvs sad. */
  RESUELTO_ARCHIVO_CADUCIDAD_PVS_SAD("RACS"),

  /** The archivo fallecimiento. */
  ARCHIVO_FALLECIMIENTO("AF"),

  /** The pdte resolucion fallecimiento. */
  PDTE_RESOLUCION_FALLECIMIENTO("PRF"),

  /** The resolucion fallecimiento notificada. */
  RESOLUCION_FALLECIMIENTO_NOTIFICADA("RFN"),

  /** The pdte revocacion fallecido. */
  PDTE_REVOCACION_FALLECIDO("PRVF"),

  /** The archivo expediente. */
  ARCHIVO_EXPEDIENTE("AREX"),

  /** The resuelta centro no acreditado. */
  RESUELTA_CENTRO_NO_ACREDITADO("RACN"),

  /** The pdte resolucion traslado. */
  PDTE_RESOLUCION_TRASLADO("PRT"),

  /** The resolucion traslado notificada. */
  RESOLUCION_TRASLADO_NOTIFICADA("RTN"),

  /** The resuelto revocacion. */
  RESUELTO_REVOCACION("RSAD"),

  /** The pdte resolucion extincion pia. */
  PDTE_RESOLUCION_EXTINCION_PIA("PEP"),

  /** The resuelta extincion pia. */
  RESUELTA_EXTINCION_PIA("REP"),

  /** The resuelta baja servicio. */
  RESUELTA_BAJA_SERVICIO("RBS"),

  /** The resuelta caducidad art95. */
  RESUELTA_CADUCIDAD_ART95("RC95"),

  /** The resuelta notificada. */
  RESUELTA_NOTIFICADA("NT"),

  /** The resuelta gyn notificada. */
  RESUELTA_GYN_NOTIFICADA("GNN"),

  /** The ppia confirmada. */
  PPIA_CONFIRMADA("PIC"),

  /** The ppia notificada. */
  PPIA_NOTIFICADA("PIN"),

  /** The ppia enproceso. */
  PPIA_ENPROCESO("PIP"),

  /** The resuelta pia notificada. */
  RESUELTA_PIA_NOTIFICADA("RPN"),

  /** The suspendido. */
  SUSPENDIDO("SUS"),

  /** The archivo provisional mayores. */
  ARCHIVO_PROVISIONAL_MAYORES("APM"),

  /** The traslado saliente pdte aprobacion. */
  TRASLADO_SALIENTE_PDTE_APROBACION("TSPA"),

  /** The mdb devoluciones tesoreria. */
  MDB_DEVOLUCIONES_TESORERIA("DTC");
  // @formatter:on

  /** The codigo. */
  private final String codigo;

  /**
   * Instantiates a new estado solicitud enum.
   *
   * @param codigo the codigo
   */
  EstadoSolicitudEnum(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  // metodo que devuelve el codigo del estado
  public String getCodigo() {
    return codigo;
  }


  /**
   * From codigo.
   *
   * @param codigo the codigo
   * @return the estado solicitud enum
   */
  @Nullable
  public static EstadoSolicitudEnum fromCodigo(final String codigo) {

    if (codigo == null) {
      return null;
    }

    for (final EstadoSolicitudEnum estado : EstadoSolicitudEnum.values()) {
      if (estado.codigo.equalsIgnoreCase(codigo)) {
        return estado;
      }
    }
    return null;
  }

  /**
   * Es anterior comprobada.
   *
   * @param codigo the codigo
   * @return true, if successful
   */
  public static boolean esAnteriorComprobada(final String codigo) {
    final List<String> estados = Arrays.asList(BORRADOR.codigo, GRABADA.codigo);
    return estados.contains(codigo);
  }


  /**
   * Es posterior borrador.
   *
   * @param codigo the codigo
   * @return true, if successful
   */
  public static boolean esPosteriorBorrador(final String codigo) {
    final String estado = (BORRADOR.codigo);
    return !estado.contains(codigo);
  }
}
