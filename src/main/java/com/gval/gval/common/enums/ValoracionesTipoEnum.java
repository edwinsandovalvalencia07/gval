package com.gval.gval.common.enums;

import java.util.stream.Stream;


/**
 * The Enum ValoracionesRespuestasClaveEnum.
 */
public enum ValoracionesTipoEnum {


  /** The ninguno. */
  NINGUNO("NINGUNO"),

  /** The EVE. */
  EVE("EVE"),

  /** The BVD. */
  BVD("BVD");


  /** The pre id. */
  public final String id;

  /**
   * Instantiates a new valoraciones sub preguntas tipo enum.
   *
   * @param nombre the nombre
   * @param id the pre id
   */
  private ValoracionesTipoEnum(final String id) {
    this.id = id;
  }

  /**
   * Gets the pre id.
   *
   * @return the pre id
   */
  public String getId() {
    return id;
  }

  /**
   * Stream.
   *
   * @return the stream
   */
  public static Stream<ValoracionesTipoEnum> stream() {
    return Stream.of(ValoracionesTipoEnum.values());
  }

  /**
   * Gets the by id.
   *
   * @param id the id
   * @return the by id
   */
  public static ValoracionesTipoEnum getById(final String id) {
    for (final ValoracionesTipoEnum e : values()) {
      if (e.name().equals(id)) {
        return e;
      }
    }
    return NINGUNO;
  }
}
