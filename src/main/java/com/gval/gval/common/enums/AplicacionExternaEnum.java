package com.gval.gval.common.enums;

import java.util.Arrays;



/**
 * The Enum TipoListadoEnum.
 */
public enum AplicacionExternaEnum {


  /** The contador sad. */
  CONTADOR_SAD("CONTSAD", "aplicaciones-externas.contador-sad",
      "/static/public/ico/home/ico_control_presupuestario.svg",
      "/static/public/ico/home/ico_control_presupuestario_hover.svg");


  /** The valor. */
  private String valor;

  /** The label. */
  private String label;

  /** The image. */
  private String image;

  /** The image hover. */
  private String hoverImage;


  /**
   * Instantiates a new aplicacion externa enum.
   *
   * @param valor the valor
   * @param label the label
   * @param image the image
   * @param hoverImage the hover image
   */
  private AplicacionExternaEnum(final String valor, final String label,
      final String image, final String hoverImage) {
    this.valor = valor;
    this.label = label;
    this.image = image;
    this.hoverImage = hoverImage;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }


  /**
   * Gets the image.
   *
   * @return the image
   */
  public String getImage() {
    return image;
  }

  /**
   * Gets the hover image.
   *
   * @return the hover image
   */
  public String getHoverImage() {
    return hoverImage;
  }

  /**
   * From string.
   *
   * @param text the text
   * @return the tipo informe social listado enum
   */
  public static AplicacionExternaEnum fromString(final String text) {
    return Arrays.asList(AplicacionExternaEnum.values()).stream()
        .filter(opsc -> opsc.valor.equals(text)).findFirst().orElse(null);
  }

}
