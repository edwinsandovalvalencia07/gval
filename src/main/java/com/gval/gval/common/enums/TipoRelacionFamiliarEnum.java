package com.gval.gval.common.enums;

/**
 * The Enum TipoRelacionFamiliarEnum.
 */
public enum TipoRelacionFamiliarEnum {

  /** The con parentesco. */
  // @formatter:off
  CON_PARENTESCO(1L),

  /** The sin parentesco. */
  SIN_PARENTESCO(0L),

  /** The allegado. */
  ALLEGADO(2L);
  // @formatter:on

  /** The value. */
  private Long value;

  /**
   * Instantiates a new autorizacion acceso datos enum.
   *
   *
   * @param value the value
   */
  TipoRelacionFamiliarEnum(final Long value) {
    this.value = value;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public Long getValue() {
    return value;
  }
}
