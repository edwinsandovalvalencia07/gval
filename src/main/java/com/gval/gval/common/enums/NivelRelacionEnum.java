package com.gval.gval.common.enums;

import java.util.Arrays;

/**
 * The Enum NivelRelacionEnum.
 */
public enum NivelRelacionEnum {

  /** The relacion satisfactoria. */
  RELACION_SATISFACTORIA("1", "label.informe_social.relacion_satisfactoria"),

  /** The relacion aceptable. */
  RELACION_ACEPTABLE("2", "label.informe_social.relacion_aceptable"),

  /** The relacion insatisfactoria. */
  RELACION_INSATISFACTORIA("3",
      "label.informe_social.relacion_insatisfactoria"),

  /** The relacion ausencia. */
  RELACION_AUSENCIA("4", "label.informe_social.relacion_ausencia");

  /** The valor. */
  private String valor;

  /** The label. */
  private String label;

  /**
   * Instantiates a new nivel relacion enum.
   *
   * @param valor the valor
   * @param label the label
   */
  private NivelRelacionEnum(final String valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * From integer.
   *
   * @param text the text
   * @return the nivel relacion enum
   */
  public static NivelRelacionEnum fromString(final String text) {
    return Arrays.asList(NivelRelacionEnum.values()).stream()
        .filter(opsc -> opsc.valor.equals(text)).findFirst().orElse(null);
  }

}
