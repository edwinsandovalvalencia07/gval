/**
 * Copyright (c) 2020 Generalitat Valenciana - Todos los derechos reservados.
 */
package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;

// TODO: Auto-generated Javadoc
/**
 * The Enum IdentificadorCasoUsoEnum.
 */
public enum IdentificadorCasoUsoEnum {

  /** The menu consulta expediente. */
  MENU_CONSULTA_EXPEDIENTE("menu.consultaExpedientes"),
  /** The menu listado imserso pia. */
  MENU_LISTADO_ESPERA_IMSERSO_PIA("menu.listaEspera"),
  /** The menu listado resolucion masiva. */
  MENU_LISTADO_RESOLUCION_MASIVA("menu.resolucionMasiva"),
  /** The menu buscar solicitudes. */
  MENU_BUSCAR_SOLICITUDES("menu.buscarSolicitudes"),
  /** The menu buscar solicitudes extendidas. */
  MENU_BUSCAR_SOLICITUDES_EXT("menu.buscarSolicitudesExt"),
  /** The menu buscar solicitudes expedientes con PIA. */
  MENU_BUSCAR_EXPEDIENTES_PIA("menu.buscarExpedientesPIA"),
  /** The menu buscar solicitudes expedientes con PIA. */
  MENU_BUSCAR_IT_DESFABORABLES("menu.buscarITDesfavorables"),
  /** The menu nuevo expediente. */
  MENU_NUEVO_EXPEDIENTE("menu.nuevoExpediente"),
  /** The menu listado subsanaciones pendientes. */
  MENU_LISTADO_SUBSANACIONES_PENDIENTES("menu.listadoSubsanacionesPdtes"),
  /** The menu listado subsanaciones tesoreria. */
  MENU_LISTADO_SUBSANACIONES_TESORERIA("menu.listadoSubsanacionesTesoreria"),
  /** The menu listado acuses generados. */
  MENU_LISTADO_ACUSES_GENERADOS("menu.listadoAcusesGenerado"),
  /** The menu listado documentos pdte verificar. */
  MENU_LISTADO_DOCUMENTOS_PDTE_VERIFICAR("menu.listadoDocumentosPdteVerificar"),
  /** The menu listado preferencias pdte verificar. */
  MENU_LISTADO_PREFERENCIAS_PDTE_VERIFICAR(
      "menu.listadoPreferenciasPdteVerificar"),
  /** The menu listado terceros pdte verificar. */
  MENU_LISTADO_TERCEROS_PDTE_VERIFICAR(
      "menu.listadoTercerosPdteVerificar"),
 /** The menu listado generacion doc contable. */
 MENU_LISTADO_GENERACION_DOC_CONTABLE(
          "menu.listadoGeneracionDocContable"),
  /** The menu cambio cuidador pdet verificar. */
  MENU_CAMBIO_CUIDADOR_PDET_VERIFICAR(
      "menu.listadoCambiosCuidadorPdteVerificar"),
  /** The menu asignar solicitud. */
  MENU_ASIGNAR_SOLICITUD("menu.asignarSolicitud"),
  /** The menu citacion valor. */
  MENU_CITACION_VALORACION("menu.citacionValoracion"),
  /** The menu valoraciones sigma. */
  MENU_VALORACIONES_SIGMA("menu.valoracionesSigma"),
  /** The menu informe social. */
  MENU_INFORME_SOCIAL("menu.informeSocial"),
  /** The menu listado propuesta pia. */
  MENU_LISTADO_PROPUESTA_PIA("menu.listadoPropuestaPia"),
  /** The menu detalle exp datos personales. */
  MENU_DETALLE_EXP_DATOS_PERSONALES(
      "detalleExpediente.datosExpediente.datosPersonales"),
  /** The menu detalle exp direcciones. */
  MENU_DETALLE_EXP_DIRECCIONES("detalleExpediente.datosExpediente.direcciones"),
  /** The menu detalle exp representante. */
  MENU_DETALLE_EXP_REPRESENTANTE(
      "detalleExpediente.datosExpediente.representante"),
  /** The menu detalle exp unidad familiar. */
  MENU_DETALLE_EXP_UNIDAD_FAMILIAR(
      "detalleExpediente.datosExpediente.unidadFamiliar"),
  /** The menu detalle exp capacidad economica. */
  MENU_DETALLE_EXP_CAPACIDAD_ECONOMICA(
      "detalleExpediente.datosExpediente.capacidadEconomica"),
  /** The menu detalle exp preferencias. */
  MENU_DETALLE_EXP_PREFERENCIAS(
      "detalleExpediente.datosExpediente.preferencias"),
  /** The menu detalle exp terceros. */
  MENU_DETALLE_EXP_TERCEROS("detalleExpediente.datosExpediente.terceros"),
  /** The menu detalle exp traslados. */
  MENU_DETALLE_EXP_TRASLADOS("detalleExpediente.datosExpediente.traslados"),
  /** The menu detalle exp solicitudes. */
  MENU_DETALLE_EXP_SOLICITUDES("detalleExpediente.solicitudes"),
  /** The menu detalle exp solicitudes solicitud. */
  MENU_DETALLE_EXP_SOLICITUDES_SOLICITUD(
      "detalleExpediente.solicitudes.solicitud"),
  /** The menu detalle exp solicitudes datos capacidad. */
  MENU_DETALLE_EXP_SOLICITUDES_DATOS_CAPACIDAD(
      "detalleExpediente.solicitudes.datosDiscapacidad"),
  /** The menu detalle exp solicitudes informe social. */
  MENU_DETALLE_EXP_SOLICITUDES_INFORME_SOCIAL(
      "detalleExpediente.solicitudes.InformeSocial"),
  /** The menu detalle exp solicitudes valoraciones. */
  MENU_DETALLE_EXP_SOLICITUDES_VALORACIONES(
      "detalleExpediente.solicitudes.valoracion"),
  /** The menu detalle exp solicitudes comision. */
  MENU_DETALLE_EXP_SOLICITUDES_COMISION(
      "detalleExpediente.solicitudes.comision"),
  /** The menu detalle exp solicitudes resoluciones. */
  MENU_DETALLE_EXP_SOLICITUDES_RESOLUCIONES(
      "detalleExpediente.solicitudes.resoluciones"),
  /** The menu detalle exp solicitudes documentos. */
  MENU_DETALLE_EXP_SOLICITUDES_DOCUMENTOS(
      "detalleExpediente.solicitudes.documentos"),
  /** The menu detalle exp solicitudes incidencias. */
  MENU_DETALLE_EXP_SOLICITUDES_INCIDENCIAS(
      "detalleExpediente.solicitudes.incidencias"),
  /** The menu detalle exp solicitudes historico. */
  MENU_DETALLE_EXP_SOLICITUDES_HISTORICO(
      "detalleExpediente.solicitudes.historico"),
  /** The menu detalle exp informes sociales. */
  MENU_DETALLE_EXP_INFORMES_SOCIALES("detalleExpediente.informesSociales"),
  /** The menu detalle exp valoraciones. */
  MENU_DETALLE_EXP_VALORACIONES("detalleExpediente.valoraciones"),
  /** The menu detalle exp estudios. */
  MENU_DETALLE_EXP_ESTUDIOS("detalleExpediente.estudios"),
  /** The menu detalle exp resoluciones. */
  MENU_DETALLE_EXP_RESOLUCIONES("detalleExpediente.resoluciones"),
  /** The menu detalle exp pias. */
  MENU_DETALLE_EXP_PIAS("detalleExpediente.pias"),
  /** The menu detalle exp documentos. */
  MENU_DETALLE_EXP_DOCUMENTOS("detalleExpediente.documentos"),
  /** The menu detalle exp incidencias. */
  MENU_DETALLE_EXP_INCIDENCIAS("detalleExpediente.incidencias"),
  /** The menu detalle exp imserso. */
  MENU_DETALLE_EXP_IMSERSO("detalleExpediente.imserso"),

  /** The menu listado impresiones. */
  MENU_LISTADO_IMPRESIONES("menu.listadoImpresiones"),

  /** The menu estudios pendientes. */
  MENU_ESTUDIOS_PENDIENTES("menu.estudiosPendientes"),

  /** The menu listado rechazos Imserso. */
  MENU_LISTADO_RECHAZOS_IMSERSO("menu.listadoRechazosImserso"),

  /** The item desplegable rechazo grado. */
  ITEM_DESPLEGABLE_RECHAZO_GRADO("item_desplegable.rechazos_grado"),

  /** The item desplegable rechazo solicitud. */
  ITEM_DESPLEGABLE_RECHAZO_SOLICITUD("item_desplegable.rechazos_solicitud"),

  /** The item desplegable rechazo prestacion. */
  ITEM_DESPLEGABLE_RECHAZO_PRESTACION("item_desplegable.rechazos_prestaciones"),

  /** The listado cuidadores. */
  MENU_LISTADO_CUIDADORES("menu.listadoCuidadoreActivos"),
  /** The menu listados diarios. */
  MENU_LISTADOS_DIARIOS("menu.listadosDiarios"),
  /** The menu listados cambios asistente personal. */
  MENU_LISTADOS_CAMBIOS_ASISTENTE_PERSONAL(
      "menu.listadosCambiosAsistentePersonal"),
  /** The menu consulta 012. */
  MENU_CONSULTA_012("menu.consulta012"),
  /** The menu tramite telematico. */
  MENU_TRAMITE_TELEMATICO("menu.tramiteTelematico"),
  /** The menu listado nefis. */
  MENU_LISTADO_NEFIS("menu.listadoNefis"),
  /** The menu listado ingresos pendientes. */
  MENU_LISTADO_INGRESOS_PENDIENTES("menu.listadoIngresosPendientes"),
  /** The detalle expediente datos personales guardar. */
  DETALLE_EXPEDIENTE_DATOS_PERSONALES_GUARDAR(
      "detalleExpediente.datosExpediente.datosPersonales.guardar"),
  /** The detalle expediente datos personales padron. */
  DETALLE_EXPEDIENTE_DATOS_PERSONALES_PADRON(
      "detalleExpediente.datosExpediente.datosPersonales.padron"),
  /** The detalle expediente datos personales periodo anyadir. */
  DETALLE_EXPEDIENTE_DATOS_PERSONALES_PERIODO_ANYADIR(
      "detalleExpediente.datosExpediente.datosPersonales.periodo.anyadir"),
  /** The detalle expediente datos personales periodo eliminar. */
  DETALLE_EXPEDIENTE_DATOS_PERSONALES_PERIODO_ELIMINAR(
      "detalleExpediente.datosExpediente.datosPersonales.periodo.eliminar"),
  /** The detalle expediente direcciones anyadir. */
  DETALLE_EXPEDIENTE_DIRECCIONES_ANYADIR(
      "detalleExpediente.datosExpediente.direcciones.anyadir"),
  /** The detalle expediente direcciones eliminar. */
  DETALLE_EXPEDIENTE_DIRECCIONES_ELIMINAR(
      "detalleExpediente.datosExpediente.direcciones.eliminar"),
  /** The detalle expediente direcciones guardar. */
  DETALLE_EXPEDIENTE_DIRECCIONES_GUARDAR(
      "detalleExpediente.datosExpediente.direcciones.guardar"),
  /** The detalle expediente representante guardar. */
  DETALLE_EXPEDIENTE_REPRESENTANTE_GUARDAR(
      "detalleExpediente.datosExpediente.representante.guardar"),
  /** The detalle expediente representante nuevo. */
  DETALLE_EXPEDIENTE_REPRESENTANTE_NUEVO(
      "detalleExpediente.datosExpediente.representante.nuevo"),
  /** The detalle expediente representante eliminar. */
  DETALLE_EXPEDIENTE_REPRESENTANTE_ELIMINAR(
      "detalleExpediente.datosExpediente.representante.eliminar"),
  /** The detalle expediente unidad familiar anyadir. */
  DETALLE_EXPEDIENTE_UNIDAD_FAMILIAR_ANYADIR(
      "detalleExpediente.datosExpediente.unidadFamiliar.anyadir"),
  /** The detalle expediente unidad familiar eliminar. */
  DETALLE_EXPEDIENTE_UNIDAD_FAMILIAR_ELIMINAR(
      "detalleExpediente.datosExpediente.unidadFamiliar.eliminar"),
  /** The detalle expediente unidad familiar guardar. */
  DETALLE_EXPEDIENTE_UNIDAD_FAMILIAR_GUARDAR(
      "detalleExpediente.datosExpediente.unidadFamiliar.guardar"),
  /** The detalle expediente preferencias guardar. */
  DETALLE_EXPEDIENTE_PREFERENCIAS_GUARDAR(
      "detalleExpediente.datosExpediente.preferencias.guardar"),
  /** The detalle expediente terceros anyadir. */
  DETALLE_EXPEDIENTE_TERCEROS_ANYADIR(
      "detalleExpediente.datosExpediente.terceros.anyadir"),
  /** The detalle expediente terceros guardar. */
  DETALLE_EXPEDIENTE_TERCEROS_GUARDAR(
      "detalleExpediente.datosExpediente.terceros.guardar"),
  /** The detalle expediente terceros ver nacional. */
  DETALLE_EXPEDIENTE_TERCEROS_VER_NACIONAL(
      "detalleExpediente.datosExpediente.terceros.verNacional"),
  /** The detalle expediente terceros grabar activo. */
  DETALLE_EXPEDIENTE_TERCEROS_GRABAR_ACTIVO(
      "detalleExpediente.datosExpediente.terceros.grabarActivo"),
  /** The detalle expediente traslados guardar. */
  DETALLE_EXPEDIENTE_TRASLADOS_GUARDAR(
      "detalleExpediente.datosExpediente.traslados.guardar"),
  /** The detalle expediente solicitudes solicitud guardar. */
  DETALLE_EXPEDIENTE_SOLICITUDES_SOLICITUD_GUARDAR(
      "detalleExpediente.solicitudes.solicitud.guardar"),
  /** The detalle expediente solicitudes solicitud modificar fecharegistro. */
  DETALLE_EXPEDIENTE_SOLICITUDES_SOLICITUD_MODIFICAR_FECHAREGISTRO(
      "detalleExpediente.solicitudes.solicitud.modificar.fechaRegistro"),
  /** The detalle expediente solicitudes solicitud modificar nif. */
  DETALLE_EXPEDIENTE_SOLICITUDES_SOLICITUD_MODIFICAR_NIF(
      "detalleExpediente.solicitudes.datosExpediente.modificar.nif"),
  /** The detalle expediente solicitudes solicitud nueva. */
  DETALLE_EXPEDIENTE_SOLICITUDES_SOLICITUD_NUEVA(
      "detalleExpediente.solicitudes.solicitud.nueva"),
  /** The detalle expediente solicitudes solicitud eliminar. */
  DETALLE_EXPEDIENTE_SOLICITUDES_SOLICITUD_ELIMINAR(
      "detalleExpediente.solicitudes.solicitud.eliminar"),
  /** The detalle expediente solicitudes discapacidad guardar. */
  DETALLE_EXPEDIENTE_SOLICITUDES_DISCAPACIDAD_GUARDAR(
      "detalleExpediente.solicitudes.datosDiscapacidad.guardar"),
  /** The detalle expediente solicitudes incidencias nueva. */
  DETALLE_EXPEDIENTE_SOLICITUDES_INCIDENCIAS_NUEVA(
      "detalleExpediente.solicitudes.incidencias.nueva"),
  /** The detalle expediente informes sociales anyadir. */
  DETALLE_EXPEDIENTE_INFORMES_SOCIALES_ANYADIR(
      "detalleExpediente.InformesSociales.anyadir"),
  /** The detalle expediente informes sociales guardar. */
  DETALLE_EXPEDIENTE_INFORMES_SOCIALES_GUARDAR(
      "detalleExpediente.InformesSociales.guardar"),
  /** The detalle expediente informes sociales liberar. */
  DETALLE_EXPEDIENTE_INFORMES_SOCIALES_LIBERAR(
      "detalleExpediente.InformesSociales.liberar"),
  /** The detalle expediente informes sociales marcha atras. */
  DETALLE_EXPEDIENTE_INFORMES_SOCIALES_MARCHA_ATRAS(
      "detalleExpediente.informesSociales.marchaAtras"),
  /** The detalle expediente documentos requerir. */
  DETALLE_EXPEDIENTE_DOCUMENTOS_REQUERIR(
      "detalleExpediente.documentos.requerir"),
  /** The detalle expediente documentos nuevo aportado. */
  DETALLE_EXPEDIENTE_DOCUMENTOS_NUEVO_APORTADO(
      "detalleExpediente.documentos.nuevoAportado"),
  /** The detalle expediente documentos leido aportado. */
  DETALLE_EXPEDIENTE_DOCUMENTOS_LEIDO_APORTADO(
      "detalleExpediente.documentos.leidoAportado"),
  /** The detalle expediente documentos imprimir aportado. */
  DETALLE_EXPEDIENTE_DOCUMENTOS_IMPRIMIR_APORTADO(
      "detalleExpediente.documentos.imprimirAportado"),
  /** The detalle expediente documentos eliminar aportado. */
  DETALLE_EXPEDIENTE_DOCUMENTOS_ELIMINAR_APORTADO(
      "detalleExpediente.documentos.eliminarAportado"),
  /** The detalle expediente documentos imprimir generado. */
  DETALLE_EXPEDIENTE_DOCUMENTOS_IMPRIMIR_GENERADO(
      "detalleExpediente.documentos.imprimirGenerado"),
  /** The detalle expediente documentos eliminar generado. */
  DETALLE_EXPEDIENTE_DOCUMENTOS_ELIMINAR_GENERADO(
      "detalleExpediente.documentos.eliminarGenerado"),
  /** The detalle expediente documentos pdte verificar aportado. */
  DETALLE_EXPEDIENTE_DOCUMENTOS_PDTE_VERIFICAR_APORTADO(
      "detalleExpediente.documentos.pdteVerificarAportado"),
  /** The detalle expediente valoraciones guardar. */
  DETALLE_EXPEDIENTE_VALORACIONES_GUARDAR(
      "detalleExpediente.valoraciones.guardar"),
  /** The detalle expediente valoraciones liberar. */
  DETALLE_EXPEDIENTE_VALORACIONES_LIBERAR(
      "detalleExpediente.valoraciones.liberar"),
  /** The detalle expediente valoraciones cancelar aplazamiento. */
  DETALLE_EXPEDIENTE_VALORACIONES_CANCELAR_APLAZAMIENTO(
      "detalleExpediente.valoraciones.cancelarAplazamiento"),
  /** The detalle expediente valoraciones reabrir. */
  DETALLE_EXPEDIENTE_VALORACIONES_REABRIR(
      "detalleExpediente.valoraciones.reAbrir"),
  /** The detalle expediente solicitudes solicitud confirmar. */
  DETALLE_EXPEDIENTE_SOLICITUDES_SOLICITUD_CONFIRMAR(
      "detalleExpediente.solicitudes.solicitud.confirmar"),
  /** The detalle expediente solicitudes solicitud pdte validar. */
  DETALLE_EXPEDIENTE_SOLICITUDES_SOLICITUD_PDTE_VALIDAR(
      "detalleExpediente.solicitudes.solicitud.pdteValidar"),
  /** The detalle expediente solicitudes solicitud a pdte validar. */
  DETALLE_EXPEDIENTE_SOLICITUDES_SOLICITUD_A_PDTE_VALIDAR(
      "detalleExpediente.solicitudes.solicitud.aPdteValidar"),
  /** The detalle expediente solicitudes solicitud validar. */
  DETALLE_EXPEDIENTE_SOLICITUDES_SOLICITUD_VALIDAR(
      "detalleExpediente.solicitudes.solicitud.validar"),
  /** The detalle expediente solicitudes solicitud no validar. */
  DETALLE_EXPEDIENTE_SOLICITUDES_SOLICITUD_NO_VALIDAR(
      "detalleExpediente.solicitudes.solicitud.noValidar"),
  /** The detalle expediente solicitudes solicitud tramite urgencia. */
  DETALLE_EXPEDIENTE_SOLICITUDES_SOLICITUD_TRAMITE_URGENCIA(
      "detalleExpediente.solicitudes.solicitud.tramiteUrgencia"),
  /** The detalle expediente solicitudes solicitud cancelar g1. */
  DETALLE_EXPEDIENTE_SOLICITUDES_SOLICITUD_CANCELAR_G1(
      "detalleExpediente.solicitudes.solicitud.cancelarG1"),
  /** The detalle expediente datos personales anyadir fecha fallecimiento. */
  DETALLE_EXPEDIENTE_DATOS_PERSONALES_ANYADIR_FECHA_FALLECIMIENTO(
      "detalleExpediente.datosPersonales.anyadirFechaFallecimiento"),
  /** The detalle expediente datos personales modificar fecha fallecimiento. */
  DETALLE_EXPEDIENTE_DATOS_PERSONALES_MODIFICAR_FECHA_FALLECIMIENTO(
      "detalleExpediente.datosPersonales.modificarFechaFallecimiento"),
  /** The detalle expediente datos personales modificables. */
  DETALLE_EXPEDIENTE_DATOS_PERSONALES_MODIFICABLES(
      "detalleExpediente.datosPersonales.modificarDatosPersonales"),
  /** The desplegable expedientes pia. */
  ITEM_DESPLEGABLE_EXPEDIENTES_PIA("item_desplegable.expedientes_pia.minimo"),
  /** The desplegable nefis peticiones. */
  ITEM_DESPLEGABLE_NEFIS_PETICIONES("item_desplegable.nefis.nefis_peticiones"),
  /** The desplegable nefis terceros erroneos. */
  ITEM_DESPLEGABLE_NEFIS_TERCEROS_ERRONEOS(
      "item_desplegable.nefis.nefis_terceros_erroneos"),
  /** The desplegable nefis peticiones. */
  ITEM_INGRESOS_PENDIENTES("item.ingresos.ingresos_pendientes"),
  /** The desplegable expedientes ist seguimiento. */
  ITEM_DESPLEGABLE_EXPEDIENTES_IST_SEGUIMIENTO(
      "item_desplegable.expedientes_pia.ist_seguimiento"),
  /** The listado acuses generados notificar. */
  LISTADO_ACUSES_GENERADOS_NOTIFICAR("listadoAcusesGenerados.notificar"),
  /** The listado acuses generados previsualizar. */
  LISTADO_ACUSES_GENERADOS_PREVISUALIZAR(
      "listadoAcusesGenerados.previsualizar"),
  /** The listado acuses generados generar excel notificaciones. */
  LISTADO_ACUSES_GENERADOS_GENERAR_EXCEL_NOTIFICACIONES(
      "listadoAcusesGenerados.generarExcelNotificaciones"),
  /** The listado acuses generados ver documento. */
  LISTADO_ACUSES_GENERADOS_VER_DOCUMENTO("listadoAcusesGenerados.verDocumento"),
  /** The listado acuses generados ver documento. */
  LISTADO_ACUSES_GENERADOS_IMPORTAR("listadoAcusesGenerados.importar"),
  /** The listado acuses generados marcha atras. */
  LISTADO_ACUSES_GENERADOS_MARCHA_ATRAS("listadoAcusesGenerados.marchaAtras"),
  /** The consulta imserso datos personales. */
  CONSULTA_IMSERSO_DATOS_PERSONALES("consultaImsersoDatosPersonales"),
  /** The consulta identidad pai datos personales. */
  CONSULTA_IDENTIDAD_PAI_DATOS_PERSONALES(
      "consultaIdentidadPaiDatosPersonales"),
  /** The propuesta pia marcha atras. */
  PROPUESTA_PIA_MARCHA_ATRAS("propuestaPia.marchaAtras"),
  /** The propuesta pia resolver. */
  PROPUESTA_PIA_RESOLVER("propuestaPia.resolver"),
  /** The detalle expediente preferencias abrir cerrar. */
  DETALLE_EXPEDIENTE_PREFERENCIAS_ABRIR_CERRAR(
      "detalleExpediente.preferencias.abrirCerrar"),
  /** The detalle expediente preferencias borrar centros. */
  DETALLE_EXPEDIENTE_PREFERENCIAS_BORRAR_CENTROS(
      "detalleExpediente.preferencias.borrarCentros"),
  /** The tramite telematico boton marcha atras. */
  DETALLE_TRAMITE_TELEMATICO_MARCHA_ATRAS("tramiteTelematico.marchaAtras"),
  /** The item desplegable caducidad pia masiva. */
  ITEM_DESPLEGABLE_CADUCIDAD_PIA_MASIVA(
      "item_desplegable.resolucion_masiva.caducidad_pia"),
  /** The item desplegable caducidad pia masiva. */
  DETALLE_EXPEDIENTE_DATOS_PERSONALES_HABILITAR_DIRECCION(
      "detalleExpediente.datosPersonales.habilitarDireccion"),
  /** The item desplegable teleasistencia pia masiva. */
  ITEM_DESPLEGABLE_TELEASISTENCIA_PIA_MASIVA(
      "item_desplegable.resolucion_masiva.teleasistencia"),
  /** The item desplegable revision prestaciones masiva. */
  ITEM_DESPLEGABLE_REVISION_PRESTACIONES(
      "item_desplegable.resolucion_masiva.revision_prestaciones"),
  /** The item desplegable revision prestaciones m�ximas masiva.. */
  ITEM_DESPLEGABLE_REVISION_PRESTACIONES_MAX(
      "item_desplegable.resolucion_masiva.revision_prestaciones_max"),
  /** The item desplegable revision cnp. */
  ITEM_DESPLEGABLE_REVISION_CNP_CONTRATO(
      "item_desplegable.resolucion_masiva.revision_cnp"),
  /** The item desplegable revision cnp aumento grado. */
  ITEM_DESPLEGABLE_REVISION_CNP_AUMENTO_GRADO(
      "item_desplegable.resolucion_masiva.revision_cnp_aumento_grado"),
  /** The check marcha atras caducidad pia masiva. */
  CHECK_MARCHA_ATRAS_CADUCIDAD_PIA_MASIVA(
      "checkbox.resolucion_masiva.marcha_atras_caducidad_pia"),
  /** The item desplegable archivo caducidad masiva. */
  ITEM_DESPLEGABLE_ARCHIVO_CADUCIDAD_MASIVA(
      "item_desplegable.resolucion_masiva.archivo_caducidad"),
  /** The resoluciones anyadir documentacion. */
  RESOLUCIONES_ANYADIR_DOCUMENTACION(
      "detalleExpediente.resoluciones.anyadirDocumentacion"),
  /** The resoluciones anyadir nueva resolucion. */
  RESOLUCIONES_ANYADIR_RESOLUCION(
      "detalleExpediente.resoluciones.anyadirResolucion"),
  /** The item desplegable subsanacion documento aportado. */
  ITEM_DESPLEGABLE_SUBSANACION_DOCUMENTO_APORTADO(
      "item_desplegable.subsanacion.documento_aportado"),
  /** The item desplegable subsanacion devolucion tesoreria. */
  ITEM_DESPLEGABLE_SUBSANACION_DEVOLUCION_TESORERIA(
      "item_desplegable.subsanacion.devolucion_tesoreria"),
  /** The detalle expediente documentos varificar documento. */
  DETALLE_EXPEDIENTE_DOCUMENTOS_VERIFICAR_DOCUMENTO(
      "detalleExpediente.documentos.verificarDocumento"),
  /** The detalle expediente documentos varificar documento. */
  DETALLE_EXPEDIENTE_DOCUMENTOS_MARCHA_ATRAS(
      "detalleExpediente.documentos.marchaAtras"),
  /** The item desplegable validar documentos peticion nuevas preferencias. */
  ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_PETICION_NUEVAS_PREFERENCIAS(
      "item.litado.validarDocumentos.peticionNuevasPreferencias"),
  /** The item desplegable validar documentos peticion nuevas preferencias. */
  ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_MDB("item.litado.validarDocumentos.mdb"),
  /** The item desplegable validar documentos cambio cuidador. */
  ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_CAMBIO_CUIDADOR(
      "item.litado.validarDocumentos.cambioCuidador"),
  /** The item desplegable validar documentos cambio cuidador. */
  ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_CAMBIO_ASISTENTE(
      "item.litado.validarDocumentos.cambioAsistentePersonal"),
  /** The item desplegable validar documentos modelo solicitud. */
  ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_MODELO_SOLICITUD(
      "item.litado.validarDocumentos.modeloSolicitud"),
  /** The item desplegable validar documentos documento peti revision. */
  ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_DOCUMENTO_PETI_REVISION(
      "item.litado.validarDocumentos.documentoPeticionRevision"),
  /** The item desplegable validar documentos compromiso cnp. */
  ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_COMPROMISO_CNP(
      "item.litado.validarDocumentos.compromisoCNP"),
  /** The item desplegable validar documentos contrato pvs rs. */
  ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_CONTRATO_PVS_RS(
      "item.litado.validarDocumentos.contratoPVSRS"),
  /** The item desplegable validar documentos contrato pvs cd. */
  ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_CONTRATO_PVS_CD(
      "item.litado.validarDocumentos.contratoPVSCD"),
  /** The item desplegable validar documentos contrato pvs sad. */
  ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_CONTRATO_PVS_SAD(
      "item.litado.validarDocumentos.contratoPVSSAD"),
  /** The item desplegable validar documentos contrato pvs sp. */
  ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_CONTRATO_PVS_SP(
      "item.litado.validarDocumentos.contratoPVSSP"),
  /** The item desplegable validar documentos contrato ap. */
  ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_CONTRATO_AP(
      "item.litado.validarDocumentos.contratoAP"),
  /** The item desplegable validar documentos respo retro cnp. */
  ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_RESPO_RETRO_CNP(
      "item.litado.validarDocumentos.decalracionResponsableRetroCNP"),
  /** The item desplegable validar documentos todos. */
  ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_TODOS(
      "item.litado.validarDocumentos.todos"),
  /** The item desplegable validar documentos no procede. */
  ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_NO_PROCEDE(
      "item.litado.validarDocumentos.noProcede"),
  /** The item desplegable validar documentos revision importe. */
  ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_REVISION_IMPORTE(
      "item.litado.validarDocumentos.revImporte"),
  /** The item desplegable validar documentos revision horas. */
  ITEM_DESPLEGABLE_VALIDAR_DOCUMENTOS_REVISION_HORAS(
      "item.litado.validarDocumentos.revHoras"),
  /** The detalle expediente solicitudes solicitud bloquear. */
  DETALLE_EXPEDIENTE_SOLICITUDES_SOLICITUD_BLOQUEAR_TEMPORALMENTE(
      "detalleExpediente.solicitudes.solicitud.bloquear.temporalmente"),
  /** The detalle expediente solicitudes solicitud desbloquear. */
  DETALLE_EXPEDIENTE_SOLICITUDES_SOLICITUD_DESBLOQUEAR_TEMPORALMENTE(
      "detalleExpediente.solicitudes.solicitud.desbloquear.temporalmente"),
  /** The detalle expediente datos personales habilitar direccion adicional. */
  DETALLE_EXPEDIENTE_DATOS_PERSONALES_MODIFICAR_DIRECCION_ADICIONAL(
      "detalleExpediente.datosExpediente.direcciones.modificar"),
  /** The boton marcha atras resolucion masiva caducidad pia. */
  BOTON_MARCHA_ATRAS_RESOLUCION_MASIVA_CADUCIDAD_PIA(
      "boton_marcha_atras.resolucion_masiva.revision_caducidad_pia"),
  /** The boton marcha atras resolucion masiva archivo caducidad. */
  BOTON_MARCHA_ATRAS_RESOLUCION_MASIVA_ARCHIVO_CADUCIDAD(
      "boton_marcha_atras.resolucion_masiva.revision_archivo_caducidad"),
  /** The boton marcha atras resolucion masiva teleasistencia. */
  BOTON_MARCHA_ATRAS_RESOLUCION_MASIVA_TELEASISTENCIA(
      "boton_marcha_atras.resolucion_masiva.revision_teleasistencia"),
  /** The boton marcha atras resolucion masiva revision prestaciones. */
  BOTON_MARCHA_ATRAS_RESOLUCION_MASIVA_REVISION_PRESTACIONES(
      "boton_marcha_atras.resolucion_masiva.revision_prestaciones"),
  /** The boton marcha atras resolucion masiva cnp contrato. */
  BOTON_MARCHA_ATRAS_RESOLUCION_MASIVA_CNP_CONTRATO(
      "boton_marcha_atras.resolucion_masiva.revision_cnp_contrato"),
  /** The boton marcha atras resolucion masiva cnp aumento contrato. */
  BOTON_MARCHA_ATRAS_RESOLUCION_MASIVA_CNP_AUMENTO_CONTRATO(
      "boton_marcha_atras.resolucion_masiva.revision_cnp_aumento_grado"),
  /** The boton marcha atras resolucion masiva revision prestaciones m�ximas. */
  BOTON_MARCHA_ATRAS_RESOLUCION_MASIVA_REVISION_PRESTACIONES_MAX(
      "boton_marcha_atras.resolucion_masiva.revision_prestaciones_max"),
  /** The boton guardar comision popup comision. */
  BOTON_GUARDAR_COMISION_POPUP_COMISION("popupComision.btnGuardarComision"),
  /** The boton liberar estudio popup comision. */
  BOTON_LIBERAR_ESTUDIO_POPUP_COMISION("popupComision.btnLiberarEstudio"),
  /** The boton diagnostico popup comision. */
  BOTON_DIAGNOSTICO_POPUP_COMISION("popupComision.btnDiagnostico"),
  /** The boton impr comision popup comision. */
  BOTON_IMPRIMIR_COMISION_POPUP_COMISION("popupComision.btnImprimirComision"),
  /** The boton impr comision sin obs popup comision. */
  BOTON_IMPRIMIR_COMISION_SIN_OBSERV_POPUP_COMISION("popupComision.btnImprimirComisionSinObserv");



  /** The valor. */
  String valor;

  /**
   * Instantiates a new identificador caso uso enum.
   *
   * @param identificador the identificador
   */
  IdentificadorCasoUsoEnum(final String identificador) {
    this.valor = identificador;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

  /**
   * From valor.
   *
   * @param valor the valor
   * @return the identificador caso uso enum
   */
  @Nullable
  public static IdentificadorCasoUsoEnum fromValor(final String valor) {
    for (final IdentificadorCasoUsoEnum casoUso : values()) {
      if (casoUso.valor.equals(valor)) {
        return casoUso;
      }
    }
    return null;
  }


}
