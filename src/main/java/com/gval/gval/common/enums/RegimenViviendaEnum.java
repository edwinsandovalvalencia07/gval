package com.gval.gval.common.enums;

import java.util.Arrays;

/**
 * The Enum RegimenViviendaEnum.
 */
public enum RegimenViviendaEnum {

  /** The propia. */
  PROPIA("1", "label.informe_social.regimen_vivienda_propia"),

  /** The cedida. */
  CEDIDA("2", "label.informe_social.regimen_vivienda_cedida"),

  /** The alquiler. */
  ALQUILER("3", "label.informe_social.regimen_vivienda_alquiler"),

  /** The realquilado. */
  REALQUILADO("4", "label.informe_social.regimen_vivienda_realquilado"),

  /** The tutelada. */
  TUTELADA("5", "label.informe_social.regimen_vivienda_tutelada"),

  /** The hotel. */
  HOTEL("6", "label.informe_social.regimen_vivienda_hotel"),

  /** The otros. */
  OTROS("7", "label.listado-enum.motivo-denegacion-cuidador.otros"),

  /** The propiedad familiares. */
  PROPIEDAD_FAMILIARES("8", "label.informe_social.regimen_vivienda_propiedad_familiar");

  /** The orden. */
  private String valor;

  /** The valor. */
  private String label;

  /**
   * Instantiates a new regimen vivienda enum.
   *
   * @param valor the valor
   * @param label the label
   */
  private RegimenViviendaEnum(final String valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * From valor.
   *
   * @param valor the valor
   * @return the intensidad enum
   */
  public static RegimenViviendaEnum fromValor(final String valor) {
    return Arrays.asList(RegimenViviendaEnum.values()).stream()
        .filter(rv -> rv.valor.equals(valor)).findFirst().orElse(null);
  }

  /**
   * Label from valor.
   *
   * @param valor the valor
   * @return the string
   */
  public static String labelFromValor(final String valor) {
    final RegimenViviendaEnum regimenVivienda = fromValor(valor);
    return regimenVivienda == null ? null : regimenVivienda.label;
  }

}
