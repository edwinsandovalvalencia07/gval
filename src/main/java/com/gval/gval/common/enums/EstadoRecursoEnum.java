package com.gval.gval.common.enums;

/**
 * The Enum EstadoRecurso.
 */
public enum EstadoRecursoEnum {

  /** The pendiente. */
  PENDIENTE("1", "label.estado.recurso.pendiente", "1"),

  /** The proceso. */
  PROCESO("2", "label.estado.recurso.proceso", "1"),

  /** The resuelto. */
  RESUELTO("3", "label.estado.recurso.resuelto", "1"),

  /** The resuelto y notificado. */
  RESUELTO_Y_NOTIFICADO("4", "label.estado.recurso.resuelto_y_notificado", "1");

  /** The orden. */
  private String valor;

  /** The valor. */
  private String label;

  /** The es activo. */
  private String activo;

  /**
   * Instantiates a new estado recurso.
   *
   * @param valor the valor
   * @param label the label
   * @param activo the activo
   */
  private EstadoRecursoEnum(final String valor, final String label,
      final String activo) {
    this.valor = valor;
    this.label = label;
    this.activo = activo;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * Gets the es activo.
   *
   * @return the es activo
   */
  public String getActivo() {
    return this.activo;
  }
}
