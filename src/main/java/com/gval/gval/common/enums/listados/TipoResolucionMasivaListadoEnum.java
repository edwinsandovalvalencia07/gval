package com.gval.gval.common.enums.listados;

import es.gva.dependencia.ada.common.enums.IdentificadorCasoUsoEnum;

import java.util.Arrays;


/**
 * The Enum TipoResolucionMasivaListadoEnum.
 */
public enum TipoResolucionMasivaListadoEnum {

  /** The caducidad pia. */
  CADUCIDAD_PIA(0L,
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_CADUCIDAD_PIA_MASIVA.getValor(),
      IdentificadorCasoUsoEnum.BOTON_MARCHA_ATRAS_RESOLUCION_MASIVA_CADUCIDAD_PIA
          .getValor(),
      "LRESMAS", "label.resolucion_masiva.caducidad_pia", "LATRCAD",
      "label.resolucion_masiva.marcha_atras_caducidad_pia"),
  /** The archivo caducidad. */
  ARCHIVO_CADUCIDAD(1L,
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_ARCHIVO_CADUCIDAD_MASIVA
          .getValor(),
      IdentificadorCasoUsoEnum.BOTON_MARCHA_ATRAS_RESOLUCION_MASIVA_ARCHIVO_CADUCIDAD
          .getValor(),
      "LARCHCAD", "label.resolucion_masiva.archivo_caducidad", "LATRARCS",
      "label.resolucion_masiva.marcha_atras_archivo_caducidad"),
  /** The teleasistecia pia. */
  TELEASISTENCIA_PIA(2L,
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_TELEASISTENCIA_PIA_MASIVA
          .getValor(),
      IdentificadorCasoUsoEnum.BOTON_MARCHA_ATRAS_RESOLUCION_MASIVA_TELEASISTENCIA
          .getValor(),
      "LTELMAS", "label.resolucion_masiva.teleasistencia", "LTELATR",
      "label.resolucion_masiva.teleasistencia_marcha_atras"),
  /** The resolucion prestaciones (nivel minimo). */
  REVISION_PRESTACIONES(3L,
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_REVISION_PRESTACIONES
          .getValor(),
      IdentificadorCasoUsoEnum.BOTON_MARCHA_ATRAS_RESOLUCION_MASIVA_REVISION_PRESTACIONES
          .getValor(),
      "LREPRMAS", "label.resolucion_masiva.revision_prestaciones", "LREPRRE",
      "label.resolucion_masiva.revision_prestaciones_marcha_atras"),

  /** The resolucion prestaciones (nivel maximo). */
  REVISION_PRESTACIONES_MAX(4L,
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_REVISION_PRESTACIONES_MAX
          .getValor(),
      IdentificadorCasoUsoEnum.BOTON_MARCHA_ATRAS_RESOLUCION_MASIVA_REVISION_PRESTACIONES_MAX
          .getValor(),
      "LREPRMAX", "label.resolucion_masiva.revision_prestaciones_max",
      "LREPRREX",
      "label.resolucion_masiva.revision_prestaciones_marcha_atras_max"),

  /** The revision cnp contrato. */
  REVISION_CNP_CONTRATO(5L,
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_REVISION_CNP_CONTRATO
          .getValor(),
      IdentificadorCasoUsoEnum.BOTON_MARCHA_ATRAS_RESOLUCION_MASIVA_CNP_CONTRATO
          .getValor(),
      "LRECNP", "label.resolucion_masiva.revision_cnp", "LRECNPA",
      "label.resolucion_masiva.revision_cnp_marcha_atras"),
  /** The revision cnp aumento grado. */
  REVISION_CNP_AUMENTO_GRADO(6L,
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_REVISION_CNP_AUMENTO_GRADO
          .getValor(),
      IdentificadorCasoUsoEnum.BOTON_MARCHA_ATRAS_RESOLUCION_MASIVA_CNP_AUMENTO_CONTRATO
          .getValor(),
      "LRECNPAG", "label.resolucion_masiva.revision_cnp_aumento_grado",
      "LRCNPGAT",
      "label.resolucion_masiva.revision_cnp_aumento_grado_marcha_atras");

  /** The id. */
  private final Long id;

  /** The valor. */
  private final String valor;

  /** The valor marcha atras. */
  private final String valorMarchaAtras;

  /** The label. */
  private final String label;

  /** The label marcha atras. */
  private final String labelMarchaAtras;

  /** The access role. */
  private final String accessRole;

  /** The access role marcha atras. */
  private final String accessRoleMarchaAtras;


  /**
   * Instantiates a new tipo resolucion masiva listado enum.
   *
   * @param id the id
   * @param accessRole the access role
   * @param accessRoleMarchaAtras the access role marcha atras
   * @param valor the valor
   * @param label the label
   * @param valorMarchaAtras the valor marcha atras
   * @param labelMarchaAtras the label marcha atras
   */
  private TipoResolucionMasivaListadoEnum(final Long id,
      final String accessRole, final String accessRoleMarchaAtras,
      final String valor, final String label, final String valorMarchaAtras,
      final String labelMarchaAtras) {
    this.id = id;
    this.accessRole = accessRole;
    this.accessRoleMarchaAtras = accessRoleMarchaAtras;
    this.valor = valor;
    this.label = label;
    this.valorMarchaAtras = valorMarchaAtras;
    this.labelMarchaAtras = labelMarchaAtras;
  }

  /**
   * Es de tipo.
   *
   * @param tipoResolucionMasiva the tipo resolucion masiva
   * @return true, if successful
   */
  public boolean esDeTipo(
      final TipoResolucionMasivaListadoEnum tipoResolucionMasiva) {
    return this.getValor().equals(tipoResolucionMasiva.getValor());
  }

  /**
   * No es de tipo.
   *
   * @param tipoResolucionMasiva the tipo resolucion masiva
   * @return true, if successful
   */
  public boolean noEsDeTipo(
      final TipoResolucionMasivaListadoEnum tipoResolucionMasiva) {
    return !this.getValor().equals(tipoResolucionMasiva.getValor());
  }

  /**
   * Gets the id.
   *
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * Gets the access role.
   *
   * @return the accessRole
   */
  public String getAccessRole() {
    return accessRole;
  }

  /**
   * Gets the valor marcha atras.
   *
   * @return the valor marcha atras
   */
  public String getValorMarchaAtras() {
    return valorMarchaAtras;
  }

  /**
   * Gets the label marcha atras.
   *
   * @return the label marcha atras
   */
  public String getLabelMarchaAtras() {
    return labelMarchaAtras;
  }

  /**
   * Gets the access role marcha atras.
   *
   * @return the access role marcha atras
   */
  public String getAccessRoleMarchaAtras() {
    return accessRoleMarchaAtras;
  }

  /**
   * From string.
   *
   * @param text the text
   * @return the tipo expedientes PIA listado enum
   */
  public static TipoResolucionMasivaListadoEnum fromString(final String text) {
    return Arrays.asList(TipoResolucionMasivaListadoEnum.values()).stream()
        .filter(opsc -> opsc.valor.equals(text)).findFirst().orElse(null);
  }


}
