package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;

/**
 * The Enum RoleUsuarioEnum.
 */
public enum RoleUsuarioEnum {

  /** The coordinador sad. */
  // @formatter:off
  COORDINADOR_SAD("COORD_SAD", "ADA_COORDINADOR_SAD"),

  /** The coordinador grabacion. */
  COORDINADOR_GRABACION("COORD_GRAB", "ADA_COORDINADOR_GRABACION"),

  /** The grabador ayto. */
  GRABADOR_AYTO("GR_AYTO", "ADA_GRABADOR_AYTO"),

  /** The grabador. */
  GRABADOR("GR_CIPI", "ADA_GRABADOR"),

  /** The valorador. */
  VALORADOR("TSOCIAL", "ADA_VALORADOR"),

  /** The trabajador social. */
  TRABAJADOR_SOCIAL("VALORADOR", "ADA_TRABAJADOR_SOCIAL"),

  /** The tecnico pia ayto. */
  TECNICO_PIA_AYTO("TP_AYTO", "ADA_TECNICO_PIA_AYTO"),

  /** The tecnico pia. */
  TECNICO_PIA("TP_CIPI", "ADA_TECNICO_PIA"),

  /** The admin. */
  ADMINISTRADOR("ADMIN", "ADA_ADMIN"),

  /** The consulta. */
  CONSULTA("CONSULTA", "ADA_CONSULTA"),

  /** The entidad local. */
  ENTIDAD_LOCAL("ENT_LOCAL", "ADA_ENTIDAD_LOCAL"),

  /** The comision. */
  COMISION("CM", "ADA_COMISION");
  // @formatter:on

  /** The codigo. */
  private final String codigo;

  /** The role. */
  private final String role;


  /**
   * Instantiates a new role usuario enum.
   *
   * @param codigo the codigo
   * @param role the role
   */
  RoleUsuarioEnum(final String codigo, final String role) {
    this.codigo = codigo;
    this.role = role;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Gets the role.
   *
   * @return the role
   */
  public String getRole() {
    return role;
  }

  /**
   * From codigo.
   *
   * @param codigo the codigo
   * @return the role usuario enum
   */
  @Nullable
  public static RoleUsuarioEnum fromCodigo(final String codigo) {
    for (final RoleUsuarioEnum e : values()) {
      if (e.codigo.equals(codigo)) {
        return e;
      }
    }
    return null;
  }

  /**
   * From role.
   *
   * @param role the role
   * @return the role usuario enum
   */
  @Nullable
  public static RoleUsuarioEnum fromRole(final String role) {
    for (final RoleUsuarioEnum e : values()) {
      if (e.role.equals(role)) {
        return e;
      }
    }
    return null;
  }
}
