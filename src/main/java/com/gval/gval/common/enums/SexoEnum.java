package com.gval.gval.common.enums;

import java.util.Arrays;

/**
 * The Enum SexoEnum.
 */
public enum SexoEnum {

  /** The mujer. */
  MUJER(Long.valueOf("2"), "refcode.mujer"),

  /** The hombre. */
  HOMBRE(Long.valueOf("1"), "refcode.hombre"),

  /** The no consta. */
  NO_CONSTA(Long.valueOf("3"), "refcode.no_consta");

  /** The valor. */
  private Long valor;

  /** The label. */
  private String label;

  /**
   * Instantiates a new sexo enum.
   *
   * @param valor the valor
   * @param label the label
   */
  private SexoEnum(final Long valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public Long getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * From integer.
   *
   * @param text the text
   * @return the sexo enum
   */
  public static SexoEnum fromInteger(final Long text) {
    return Arrays.asList(SexoEnum.values()).stream()
        .filter(s -> s.valor.equals(text)).findFirst().orElse(null);
  }

}
