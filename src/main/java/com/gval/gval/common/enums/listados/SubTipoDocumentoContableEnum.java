package com.gval.gval.common.enums.listados;

/**
 * The Enum SubTipoDocumentoContableEnum.
 */
public enum SubTipoDocumentoContableEnum {

  /** The inicial. */
  INICIAL("I", "label.documentos_contables.subtipo-inicial"),
  /** The retroactivo. */
  RETROACTIVO("RET", "label.documentos_contables.subtipo-retroactivo"),
  /** The minorado. */
  MINORADO("MIN", "label.documentos_contables.subtipo-minorado"),
  /** The inicial FP. */
  INICIAL_FP("I_FP", "label.documentos_contables.subtipo-inicial_FP"),
  /** The retroactivo FP. */
  RETROACTIVO_FP("RET_FP", "label.documentos_contables.subtipo-retroactivo_FP");


  /** The valor. */
  private final String valor;

  /** The label. */
  private final String label;


  /**
   * Instantiates a new tipo resolucion masiva listado enum.
   *
   * @param valor the valor
   * @param label the label
   */
  private SubTipoDocumentoContableEnum(final String valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }
}
