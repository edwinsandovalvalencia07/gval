package com.gval.gval.common.enums;

import com.gval.gval.common.enums.listados.InterfazListadoLabels;

import java.util.Arrays;

/**
 * The Enum SituacionIncapacidadEnum.
 */
public enum SituacionCapacidadLaboralEnum implements InterfazListadoLabels<Long> {

  /** The capaz. */
  CAPAZ(1L, "label.informe_social.capaz"),

  /** The incapaz. */
  INCAPAZ(2L, "label.informe_social.incapacitado_legal"),

  /** The presunto incapaz. */
  PRESUNTO_INCAPAZ(3L, "label.informe_social.presunto_incapaz");

  /** The valor. */
  private Long valor;

  /** The label. */
  private String label;

  /**
   * Instantiates a new situacion incapacidad enum.
   *
   * @param valor the valor
   * @param label the label
   */
  private SituacionCapacidadLaboralEnum(final Long valor, final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  @Override
  public Long getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  @Override
  public String getLabel() {
    return label;
  }

  /**
   * From Long.
   *
   * @param valor the text
   * @return the situacion incapacidad enum
   */
  public static SituacionCapacidadLaboralEnum fromLong(final Long valor) {
    return Arrays.asList(SituacionCapacidadLaboralEnum.values()).stream()
        .filter(si -> si.valor.equals(valor)).findFirst().orElse(null);
  }

}
