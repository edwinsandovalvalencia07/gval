package com.gval.gval.common.enums;

import java.util.Arrays;

/**
 * The Enum TipoSubsanacionListadoEnum.
 */
public enum TipoSubsanacionListadoEnum {

  /** The subsanacion devolucion tesoreria. */
  SUBSANACION_DEVOLUCION_TESORERIA(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_SUBSANACION_DEVOLUCION_TESORERIA
          .getValor(),
      "LREQTES", "label.subsanacion_tesoreria.devolucion_tesoreria"),
  /** The subsanacion documento aportado. */
  SUBSANACION_DOCUMENTO_APORTADO(
      IdentificadorCasoUsoEnum.ITEM_DESPLEGABLE_SUBSANACION_DOCUMENTO_APORTADO
          .getValor(),
      "LSUBDA", "label.subsanacion_tesoreria.documento_aportado");


  /** The valor. */
  private String valor;

  /** The label. */
  private String label;

  /** The access role. */
  private String accessRole;

  /**
   * Instantiates a new tipo subsanacion listado enum.
   *
   * @param accessRole the access role
   * @param valor the valor
   * @param label the label
   */
  private TipoSubsanacionListadoEnum(final String accessRole,
      final String valor, final String label) {
    this.accessRole = accessRole;
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * Gets the access role.
   *
   * @return the access role
   */
  public String getAccessRole() {
    return accessRole;
  }

  /**
   * From string.
   *
   * @param text the text
   * @return the tipo subsanacion listado enum
   */
  public static TipoSubsanacionListadoEnum fromString(final String text) {
    return Arrays.asList(TipoSubsanacionListadoEnum.values()).stream()
        .filter(opsc -> opsc.valor.equals(text)).findFirst().orElse(null);
  }

}
