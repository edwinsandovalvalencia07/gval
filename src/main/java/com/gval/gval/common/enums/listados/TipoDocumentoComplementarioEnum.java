package com.gval.gval.common.enums.listados;

/**
 * The Enum TipoDocumentoComplementario.
 */
public enum TipoDocumentoComplementarioEnum {

  /** The inicial. */
  PLUS("PLUS", "label.documentos_contables.positivo"),
  /** The retroactivo. */
  MINUS("MINUS", "label.documentos_contables.negativo");

  /** The valor. */
  private String valor;

  /** The label. */
  private String label;


  /**
   * Instantiates a new tipo resolucion masiva listado enum.
   *
   * @param valor the valor
   * @param label the label
   */
  private TipoDocumentoComplementarioEnum(final String valor,
      final String label) {
    this.valor = valor;
    this.label = label;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }
}

