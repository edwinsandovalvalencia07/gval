package com.gval.gval.common.enums;

import java.util.stream.Stream;

/**
 * Enumerado para controlar ValoracionesSubPreguntasTipoEnum.
 *
 * @author Indra
 */
public enum ValoracionesSubPreguntasTipoEnum {

  /** The ninguno. */
  NINGUNO("NINGUNO", null),
  /** The problemas. */
  PROBLEMAS("PROBLEMAS", -1L),
  /** The grado. */
  GRADO("GRADO", -2L),
  /** The frecuencia. */
  FRECUENCIA("FRECUENCIA", -3L),
  /** The notas. */
  NOTAS("NOTAS", -4L);

  /** The nombre. */
  public final String nombre;

  /** The pre id. */
  public final Long preId;

  /**
   * Instantiates a new valoraciones sub preguntas tipo enum.
   *
   * @param nombre the nombre
   * @param preId the pre id
   */
  private ValoracionesSubPreguntasTipoEnum(final String nombre,
      final Long preId) {
    this.nombre = nombre;
    this.preId = preId;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Gets the pre id.
   *
   * @return the pre id
   */
  public Long getPreId() {
    return preId;
  }

  /**
   * Stream.
   *
   * @return the stream
   */
  public static Stream<ValoracionesSubPreguntasTipoEnum> stream() {
    return Stream.of(ValoracionesSubPreguntasTipoEnum.values());
  }

  public static ValoracionesSubPreguntasTipoEnum getById(final String id) {
    for (final ValoracionesSubPreguntasTipoEnum e : values()) {
      if (e.name().equals(id)) {
        return e;
      }
    }
    return NINGUNO;
  }

}
