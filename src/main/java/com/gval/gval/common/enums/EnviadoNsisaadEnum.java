package com.gval.gval.common.enums;

/**
 * The Enum EnviadoNsisaadEnum.
 */
public enum EnviadoNsisaadEnum {

  /** The no enviado. */
  NO_ENVIADO("NO"),

  /** The enviado rechazo. */
  ENVIADO_RECHAZO("ER"),

  /** The envio correcto. */
  ENVIO_CORRECTO("EN");

  /** The codigo. */
  private String codigo;

  /**
   * Instantiates a new enviado nsisaad enum.
   *
   * @param codigo the codigo
   */
  EnviadoNsisaadEnum(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }
}
