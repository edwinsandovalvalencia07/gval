package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;

/**
 * The Enum TipoDictamenEnum.
 */
public enum TipoDictamenEnum {

  /** The recurso. */
  VALORACION("V", "label.estudio.comision.valoracion"),

  /** The homologacion. */
  HOMOLOGACION("H", "label.estudio.comision.homologacion");


  /** The value. */
  private final String value;

  /** The label. */
  private final String label;

  /**
   * Instantiates a new tipo estudio enum.
   *
   * @param value the value
   * @param label the label
   */
  private TipoDictamenEnum(final String value, final String label) {
    this.value = value;
    this.label = label;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public String getValue() {
    return value;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return value;
  }



  /**
   * From string.
   *
   * @param text the text
   * @return the tipo dictamen enum
   */
  @Nullable
  public static TipoDictamenEnum fromString(final String text) {

    if (text == null) {
      return null;
    }

    for (final TipoDictamenEnum td : TipoDictamenEnum.values()) {
      if (td.value.equalsIgnoreCase(text)) {
        return td;
      }
    }
    return null;
  }

}
