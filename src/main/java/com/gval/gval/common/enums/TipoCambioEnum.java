package com.gval.gval.common.enums;

import jakarta.annotation.Nullable;

/**
 * The Enum TipoCambioEnum.
 */
public enum TipoCambioEnum {


  /** The mejora. */
  MEJORA("M"),

  /** The correccion. */
  CORRECCION("C"),;


  /** The value. */
  private String value;

  /**
   * Instantiates a new tipo estudio enum.
   *
   * @param value the value
   */
  private TipoCambioEnum(final String value) {
    this.value = value;
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public String getValue() {
    return value;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return value;
  }


  /**
   * From string.
   *
   * @param text the text
   * @return the tipo estudio enum
   */
  @Nullable
  public static TipoCambioEnum fromString(final String text) {

    if (text == null) {
      return null;
    }

    for (final TipoCambioEnum te : TipoCambioEnum.values()) {
      if (te.value.equalsIgnoreCase(text)) {
        return te;
      }
    }
    return null;
  }

}
