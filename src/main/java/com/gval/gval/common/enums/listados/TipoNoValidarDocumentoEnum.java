package com.gval.gval.common.enums.listados;

import java.util.Arrays;

/**
 * The Enum TipoNoValidarDocumento.
 */
public enum TipoNoValidarDocumentoEnum
    implements InterfazListadoLabels<String> {

  /** The discrepancia. */
  DISCREPANCIA("DIS",
      "label.listado-enum.motivo-no-verificacion-documento.discrepancia"),
  /** The requerimiento. */
  REQUERIMIENTO("REQ",
      "label.listado-enum.motivo-no-verificacion-documento.requerimiento"),
  /** The no procede. */
  NO_PROCEDE("NP",
      "label.listado-enum.motivo-no-verificacion-documento.no-procede");



  /** The valor. */
  private String valor;

  /** The label. */
  private String label;

  /**
   * Instantiates a new tipo no validar documento.
   *
   * @param valor the valor
   * @param label the label
   */
  TipoNoValidarDocumentoEnum(final String valor, final String label) {
    this.valor = valor;
    this.label = label;
  }


  /**
   * Gets the label.
   *
   * @return the label
   */
  @Override
  public String getLabel() {
    return label;
  }


  /**
   * Gets the valor.
   *
   * @return the valor
   */
  @Override
  public String getValor() {
    return valor;
  }

  /**
   * From valor.
   *
   * @param valor the valor
   * @return the tipo no validar documento enum
   */
  public static TipoNoValidarDocumentoEnum fromValor(final String valor) {
    return Arrays.asList(TipoNoValidarDocumentoEnum.values()).stream()
        .filter(tc -> tc.valor.equals(valor)).findFirst().orElse(null);
  }

  /**
   * Label from valor.
   *
   * @param valor the valor
   * @return the string
   */
  public static String labelFromValor(final String valor) {
    final TipoNoValidarDocumentoEnum tipoNoValidarDocumentoEnum =
        fromValor(valor);
    return tipoNoValidarDocumentoEnum == null ? null
        : tipoNoValidarDocumentoEnum.label;
  }

}
