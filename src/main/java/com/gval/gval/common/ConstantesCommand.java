package com.gval.gval.common;


/**
 * The Class ConstantesCommand.
 */
public class ConstantesCommand {


  /** The Constant A_PENDIENTE_VALIDACION_SOLICITUD. */
  public static final String A_PENDIENTE_VALIDACION_SOLICITUD =
      "aPendienteValidacionSolicitud";

  /** The Constant ABRIR_POP_UP_ANYADIR_CONVIVIENTE_INFORME_SOCIAL. */
  public static final String ABRIR_POP_UP_ANYADIR_CONVIVIENTE_INFORME_SOCIAL =
      "abrirPopUpAnyadirConvivienteInformeSocial";

  /** The Constant ABRIR_POP_UP_ANYADIR_CONVIVIENTES. */
  public static final String ABRIR_POP_UP_ANYADIR_CONVIVIENTES =
      "abrirPopUpAnyadirConvivienteInformeSocial";

  /** The Constant ABRIR_POP_UP_ANYADIR_DOCUMENTOS_REQUERIDOS. */
  public static final String ABRIR_POP_UP_ANYADIR_DOCUMENTOS_REQUERIDOS =
      "abrirPopUpAnyadirDocumentosRequeridos";

  /** The Constant ABRIR_POP_UP_ANYADIR_PRESTACION_INFORME_SOCIAL. */
  public static final String ABRIR_POP_UP_ANYADIR_PRESTACION_INFORME_SOCIAL =
      "abrirPopUpAnyadirPrestacionInformeSocial";

  /** The Constant ABRIR_POP_UP_CANCELAR_ARCHIVO_G1. */
  public static final String ABRIR_POP_UP_CANCELAR_ARCHIVO_G1 =
      "abrirPopUpCancelarArchivoG1";

  /** The Constant ABRIR_POP_UP_HISTORIAL_VERSIONES. */
  public static final String ABRIR_POP_UP_HISTORIAL_VERSIONES =
      "abrirPopUpHistorialVersiones";

  /** The Constant ABRIR_POP_UP_REQUERIR_DOCUMENTOS. */
  public static final String ABRIR_POP_UP_REQUERIR_DOCUMENTOS =
      "abrirPopUpRequerirDocumentos";

  /** The Constant ABRIR_POP_UP_REQUERIR_DOCUMENTOS_DESDE_SOLICITUD. */
  public static final String ABRIR_POP_UP_REQUERIR_DOCUMENTOS_DESDE_SOLICITUD =
      "abrirPopUpRequerirDocumentosDesdeSolicitud";

  /** The Constant ACTUALIZA_FILTRO. */
  public static final String ACTUALIZA_FILTRO = "actualizaFiltro";

  /** The Constant ACTUALIZAR_DESHABILITAR_FECHA_FALLECIMIENTO. */
  public static final String ACTUALIZAR_DESHABILITAR_FECHA_FALLECIMIENTO =
      "actualizarDeshabilitarFechaFallecimiento";

  /** The Constant ACTUALIZAR_FORMULARIO_DIRECCION. */
  public static final String ACTUALIZAR_FORMULARIO_DIRECCION =
      "actualizarFormularioDireccion";

  /** The Constant ACTUALIZAR_LISTADO_DIRECCIONES_ADICIONALES. */
  public static final String ACTUALIZAR_LISTADO_DIRECCIONES_ADICIONALES =
      "actualizarListadoDirecciones";

  /** The Constant ACTUALIZAR_LISTADO_HISTORICO. */
  public static final String ACTUALIZAR_LISTADO_HISTORICO =
      "actualizarListadoHistorico";

  /** The Constant ACTUALIZAR_PESTANYA_SITUACION_CONVIVENCIA. */
  public static final String ACTUALIZAR_PESTANYA_SITUACION_CONVIVENCIA =
      "actualizarPestanyaSituacionConvivencia";

  /** The Constant ACTUALIZAR_PREFERENCIA_SELECCIONADA. */
  public static final String ACTUALIZAR_PREFERENCIA_SELECCIONADA =
      "actualizarPreferenciaSeleccionada";

  /** The Constant ACTUALIZAR_SOLICITUD_SELECCIONADA. */
  public static final String ACTUALIZAR_SOLICITUD_SELECCIONADA =
      "actualizarSolicitudSeleccionada";

  /** The Constant ACTUALIZAR_TITULO_PANTALLA. */
  public static final String ACTUALIZAR_TITULO_PANTALLA =
      "actualizarTituloPantalla";

  /** The Constant AGREGAR_PERIODO_RESIDENCIA. */
  public static final String AGREGAR_PERIODO_RESIDENCIA =
      "agregarPeriodoResidencia";

  /** The Constant ANYADIR_BOTONES_CABECERA. */
  public static final String ANYADIR_BOTONES_CABECERA =
      "anyadirBotonesCabecera";

  /** The Constant ASOCIAR_DOCUMENTO_MOTIVO_GENERACION_INFORME_SOCIAL_ACTIVO. */
  public static final String ASOCIAR_DOCUMENTO_MOTIVO_GENERACION_INFORME_SOCIAL_ACTIVO =
      "asociarDocumentoYMotivoGeneracionInformeSocialActivo";

  /** The Constant BLOQUEAR_APORTACION_DOCUMENTO. */
  public static final String BLOQUEAR_APORTACION_DOCUMENTO =
      "bloquearAportacionDocumento";

  /** The Constant BLOQUEAR_CABECERA. */
  public static final String BLOQUEAR_CABECERA = "bloquearCabecera";

  /** The Constant BLOQUEAR_GUARDADO. */
  public static final String BLOQUEAR_GUARDADO = "bloquearGuardado";

  /** The Constant BUILD_LIST_DOCUMENTOS_PERSONA. */
  public static final String BUILD_LIST_DOCUMENTOS_PERSONA =
      "buildListDocumentosPersona";

  /** The Constant BUILD_PREFERENCIAS_CERRAR_INFORME_SOCIAL. */
  public static final String BUILD_PREFERENCIAS_CERRAR_INFORME_SOCIAL =
      "buildPreferenciasCerrarInformeSocial";

  /** The Constant BUILD_PREFERENCIAS_GUARDAR_INFORME_SOCIAL. */
  public static final String BUILD_PREFERENCIAS_GUARDAR_INFORME_SOCIAL =
      "buildPreferenciasGuardarInformeSocial";

  /** The Constant BUILD_PREFERENCIAS_INFORME_SOCIAL. */
  public static final String BUILD_PREFERENCIAS_INFORME_SOCIAL =
      "buildPreferenciasInformeSocial";

  /** The Constant CAMBIAR_TAB_DATOS_EXPEDIENTE. */
  public static final String CAMBIAR_TAB_DATOS_EXPEDIENTE =
      "cambiarTabDatosExpediente";

  /** The Constant CAMBIAR_TAB_Y_SUBTAB. */
  public static final String CAMBIAR_TAB_Y_SUBTAB = "cambiarTabYSubtab";

  /** The Constant CAMBIAR_TIPO_REVISION. */
  public static final String CAMBIAR_TIPO_REVISION = "cambiarTipoRevision";

  /** The Constant CAMBIO_DATOS_PERSONA. */
  public static final String CAMBIO_DATOS_PERSONA = "cambioDatosPersona";

  /** The Constant CANCELAR_APLAZAMIENTO_SOLICITUD. */
  public static final String CANCELAR_APLAZAMIENTO_SOLICITUD =
      "cancelarAplazamientoSolicitud";

  /** The Constant CANCELAR_ARCHIVO_G1. */
  public static final String CANCELAR_ARCHIVO_G1 = "confirmarCancelarArchivoG1";

  /** The Constant CERRAR_INFORME_SOCIAL. */
  public static final String CERRAR_INFORME_SOCIAL = "cerrarInformeSocial";

  /** The Constant CERRAR_INFORME_SOCIAL. */
  public static final String CERRAR_INFORME_SOCIAL_GLOBAL =
      "cerrarInformeSocialGlobal";

  /** The Constant CERRAR_POP_UP. */
  public static final String CERRAR_POP_UP = "cerrarPopUp";

  /** The Constant CERRAR_POP_UP_REQUERIMIENTOS. */
  public static final String CERRAR_POP_UP_REQUERIMIENTOS =
      "cerrarPopUpRequerimientos";

  /** The Constant CERRAR_SEGUNDO_POP_UP. */
  public static final String CERRAR_SEGUNDO_POP_UP = "cerrarSegundoPopUp";

  /** The Constant CERRAR_VALORACION. */
  public static final String CERRAR_VALORACION = "cerrarValoracion";

  /** The Constant COMMAND_REFRESCAR_LISTA_CITAR_SOLICITUDES. */
  public static final String COMMAND_REFRESCAR_LISTA_CITAR_SOLICITUDES =
      "refrescarListado";

  /** The Constant CONFIG_HEADER_DOCUMENTOS_SOLICITUD. */
  public static final String CONFIG_HEADER_DOCUMENTOS_SOLICITUD =
      "configHeaderDocumentosSolicitud";

  /** The Constant CIERRE_VALORACION. */
  public static final String CONFIRMAR_CIERRE_VALORACION =
      "confirmarCierreValoracion";

  /** The Constant CONFIRMAR_PIA. */
  public static final String CONFIRMAR_PIA = "confirmarPia";

  /** The Constant CONFIRMAR_SOLICITUD. */
  public static final String CONFIRMAR_SOLICITUD = "confirmarSolicitud";

  /** The Constant CREAR_INFORME_SOCIAL_SIN_VALIDACIONES. */
  public static final String CREAR_INFORME_SOCIAL_SIN_VALIDACIONES =
      "crearInformeSocialSinValidaciones";

  /** The Constant DAR_MARCHA_ATRAS_ACUSES. */
  public static final String DAR_MARCHA_ATRAS_ACUSES = "darMarchaAtrasAcuses";

  /** The Constant DESBLOQUEAR_APORTACION_DOCUMENTO. */
  public static final String DESBLOQUEAR_APORTACION_DOCUMENTO =
      "desbloquearAportacionDocumento";

  /** The Constant DESBLOQUEAR_CABECERA. */
  public static final String DESBLOQUEAR_CABECERA = "desbloquearCabecera";

  /** The Constant DESBLOQUEAR_GUARDADO. */
  public static final String DESBLOQUEAR_GUARDADO = "desbloquearGuardado";

  /** The Constant DESHABILITAR_FORMULARIO_DIRECCION. */
  public static final String DESHABILITAR_FORMULARIO_DIRECCION =
      "deshabilitarFormularioDireccion";

  /** The Constant HABILITAR_FORMULARIO_DIRECCION. */
  public static final String HABILITAR_FORMULARIO_DIRECCION =
      "habilitarFormularioDireccion";

  /** The Constant EDITAR_PERIODO_RESIDENCIA. */
  public static final String EDITAR_PERIODO_RESIDENCIA =
      "editarPeriodoResidencia";

  /** The Constant EJECUTAR_CREAR_VALORACION_SOLICITUD. */
  public static final String EJECUTAR_CREAR_VALORACION_SOLICITUD =
      "ejecutarCrearValoracionSolicitud";

  /** The Constant ELIMINAR_DOCUMENTOS_APORTADOS. */
  public static final String ELIMINAR_DOCUMENTOS_APORTADOS =
      "eliminarDocumentosAportados";

  /** The Constant ELIMINAR_DOCUMENTOS_APORTADOS_SOLICITUD. */
  public static final String ELIMINAR_DOCUMENTOS_APORTADOS_SOLICITUD =
      "eliminarDocumentosAportadosSolicitud";

  /** The Constant ELIMINAR_DOCUMENTOS_GENERADOS. */
  public static final String ELIMINAR_DOCUMENTOS_GENERADOS =
      "eliminarDocumentosGenerados";

  /** The Constant ELIMINAR_DOCUMENTOS_GENERADOS_SOLICITUD. */
  public static final String ELIMINAR_DOCUMENTOS_GENERADOS_SOLICITUD =
      "eliminarDocumentosGeneradosSolicitud";

  /** The Constant ELIMINAR_REPRESENTACION. */
  public static final String ELIMINAR_REPRESENTACION = "eliminarRepresentacion";

  /** The Constant ELIMINAR_SOLICITUD. */
  public static final String ELIMINAR_SOLICITUD = "eliminarSolicitud";

  /** The Constant FILA_POPUP_SELECTED. */
  public static final String FILA_POPUP_SELECTED = "filaPopUpSelected";

  /** The Constant GENERAR_REVOCACION. */
  public static final String GENERAR_RESOLUCION = "generarResolucion";

  /** The Constant GUARDAR_CUIDADOR_INFORME_SOCIAL. */
  public static final String GUARDAR_CUIDADOR_INFORME_SOCIAL =
      "guardarCuidadorInformeSocial";

  /** The Constant GUARDAR_ASISTENTE_INFORME_SOCIAL. */
  public static final String GUARDAR_ASISTENTE_INFORME_SOCIAL =
      "guardarAsistenteInformeSocial";

  /** The Constant GUARDAR_CUIDADOR_PREFERENCIAS. */
  public static final String GUARDAR_CUIDADOR_PREFERENCIAS =
      "guardarCuidadorPreferencias";

  /** The Constant GUARDAR_ASISTENTE_PREFERENCIAS. */
  public static final String GUARDAR_ASISTENTE_PREFERENCIAS =
      "guardarAsistentePreferencias";

  /** The Constant GUARDAR_CUIDADOR_SITUACION_CONVIVENCIA. */
  public static final String GUARDAR_CUIDADOR_SITUACION_CONVIVENCIA =
      "guardarCuidadorSituacionConvivencia";

  /** The Constant GUARDAR_DATOS_DISCAPACIDAD. */
  public static final String GUARDAR_DATOS_DISCAPACIDAD =
      "guardarDatosDiscapacidad";

  /** The Constant GUARDAR_DOCUS_REQ. */
  public static final String GUARDAR_DOCUS_REQ = "guardarDocusRequeridos";

  /** The Constant GUARDAR_INFORME_SOCIAL. */
  public static final String GUARDAR_INFORME_SOCIAL = "guardarInformeSocial";

  /** The Constant GUARDAR_INFORME_SOCIAL_SIN_VALIDACIONES. */
  public static final String GUARDAR_INFORME_SOCIAL_SIN_VALIDACIONES =
      "guardarInformeSocialSinValidaciones";

  /** The Constant GUARDAR_MANTENIMIENTO_CUIDADOR_INFORME_SOCIAL. */
  public static final String GUARDAR_MANTENIMIENTO_CUIDADOR_INFORME_SOCIAL =
      "guardarMantenimientoCuidadorInformeSocial";

  /** The Constant GUARDAR_MANTENIMIENTO_ASISTENTE_INFORME_SOCIAL. */
  public static final String GUARDAR_MANTENIMIENTO_ASISTENTE_INFORME_SOCIAL =
      "guardarMantenimientoAsistenteInformeSocial";

  /** The Constant GUARDAR_NUEVAS_PREFERENCIAS. */
  public static final String GUARDAR_NUEVAS_PREFERENCIAS =
      "guardarNuevasPReferencias";

  /** The Constant GUARDAR_PREFERENCIAS. */
  public static final String GUARDAR_PREFERENCIAS = "guardarPreferencias";

  /** The Constant GUARDAR_REPRESENTACION. */
  public static final String GUARDAR_REPRESENTACION = "guardarRepresentacion";

  /** The Constant GUARDAR_REQUERIMIENTO. */
  public static final String GUARDAR_REQUERIMIENTO = "guardarRequerimiento";

  /** The Constant GUARDAR_SOLICITUD. */
  public static final String GUARDAR_SOLICITUD = "guardarSolicitud";

  /** The Constant GUARDAR_VALORACION. */
  public static final String GUARDAR_VALORACION = "guardarValoracion";

  /** The Constant IMPRIMIR_DOCUMENTOS_APORTADOS. */
  public static final String IMPRIMIR_DOCUMENTOS_APORTADOS =
      "imprimirDocumentosAportados";

  /** The Constant IMPRIMIR_DOCUMENTOS_APORTADOS_SOLICITUD. */
  public static final String IMPRIMIR_DOCUMENTOS_APORTADOS_SOLICITUD =
      "imprimirDocumentosAportadosSolicitud";

  /** The Constant IMPRIMIR_DOCUMENTOS_GENERADOS. */
  public static final String IMPRIMIR_DOCUMENTOS_GENERADOS =
      "imprimirDocumentosGenerados";

  /** The Constant IMPRIMIR_DOCUMENTOS_GENERADOS_SOLICITUD. */
  public static final String IMPRIMIR_DOCUMENTOS_GENERADOS_SOLICITUD =
      "imprimirDocumentosGeneradosSolicitud";

  /** The Constant INFORME_SOCIAL_CAMBIO_TIPO_CONV. */
  public static final String INFORME_SOCIAL_CAMBIO_TIPO_CONV =
      "cambioTipoConvivencia";

  /** The Constant INICIALIZAR_BOTONES_HOME. */
  public static final String INICIALIZAR_BOTONES_HOME =
      "inicializarBotonesHome";

  /** The Constant INICIALIZAR_DATOS_DISCAPACIDAD_VM. */
  public static final String INICIALIZAR_DATOS_DISCAPACIDAD_VM =
      "inicializarDatosDiscapacidadVM";

  /** The Constant INICIALIZAR_DATOS_PERSONALES_VM. */
  public static final String INICIALIZAR_DATOS_PERSONALES_VM =
      "inicializarDatosPersonalesVM";

  /** The Constant INICIALIZAR_PESTANYAS. */
  public static final String INICIALIZAR_PESTANYAS = "inicializarPestanyas";

  /** The Constant INICIALIZAR_PROPUESTA_PIA_VM. */
  public static final String INICIALIZAR_PROPUESTA_PIA_VM =
      "inicializarPropuestaPia";

  /** The Constant INICIALIZAR_SOLICITUD_VM. */
  public static final String INICIALIZAR_SOLICITUD_VM =
      "inicializarSolicitudVM";

  /** The Constant INICIALIZAR_TRASLADOS_VM. */
  public static final String INICIALIZAR_TRASLADOS_VM =
      "inicializarTrasladosVM";

  /** The Constant INICIALIZAR_VALORACION_SOLICITUD_VM. */
  public static final String INICIALIZAR_VALORACION_SOLICITUD_VM =
      "inicializarValoracionSolicitudVM";

  /** The Constant LIBERAR_VALORACION. */
  public static final String LIBERAR_VALORACION = "liberarValoracion";

  /** The Constant LIBERAR_VALORACION_SOLICITUD. */
  public static final String LIBERAR_VALORACION_SOLICITUD =
      "liberarValoracionSolicitud";

  /** The Constant LIMPIAR_CABECERA. */
  public static final String LIMPIAR_CABECERA = "limpiarCabecera";

  /** The Constant LIMPIAR_CHECK_SELECCION. */
  public static final String LIMPIAR_CHECK_SELECCION = "limpiarCheckSeleccion";

  /** The Constant LIMPIAR_ELEMENTOS_SELECCIONADOS. */
  public static final String LIMPIAR_ELEMENTOS_SELECCIONADOS =
      "limpiarElementosSeleccionados";

  /** The Constant LIMPIAR_FILTROS_LISTAS. */
  public static final String LIMPIAR_FILTROS_LISTAS = "limpiarFiltrosListas";

  /** The Constant MARCAR_LEIDO_DOCUMENTOS_APORTADOS. */
  public static final String MARCAR_LEIDO_DOCUMENTOS_APORTADOS =
      "marcarLeidoDocumentosAportados";

  /** The Constant MARCAR_LEIDO_DOCUMENTOS_APORTADOS_SOLICITUD. */
  public static final String MARCAR_LEIDO_DOCUMENTOS_APORTADOS_SOLICITUD =
      "marcarLeidoDocumentosAportadosSolicitud";

  /** The Constant MARCHA_ATRAS_INFORME_SOCIAL. */
  public static final String MARCHA_ATRAS_INFORME_SOCIAL =
      "marchaAtrasInformeSocial";

  /** The Constant MARCHA_ATRAS_INFORME_SOCIAL_SOLICITUD. */
  public static final String MARCHA_ATRAS_INFORME_SOCIAL_SOLICITUD =
      "marchaAtrasInformeSocialSolicitud";

  /** The Constant MARCHA_ATRAS_PIA. */
  public static final String MARCHA_ATRAS_PIA = "marchaAtrasPia";

  /** The Constant MODIFICAR_DOCUMENTO. */
  public static final String MODIFICAR_DOCUMENTO = "modificarDocumento";

  /** The Constant MODIFICAR_LABEL_DETALLE_SOLICITUD. */
  public static final String MODIFICAR_LABEL_DETALLE_SOLICITUD =
      "modificarLabelDetalleSolicitud";

  /** The Constant MOSTRAR_MENU_SUPERPUESTO. */
  public static final String MOSTRAR_MENU_SUPERPUESTO =
      "mostrarMenuSuperpuesto";

  /** The Constant NAVEGAR_ATRAS. */
  public static final String NAVEGAR_ATRAS = "navegarAtras";

  /** The Constant NAVEGAR_DETALLE. */
  public static final String NAVEGAR_DETALLE = "navegarDetalle";

  /** The Constant NAVEGAR_DETALLE_INFORME. */
  public static final String NAVEGAR_DETALLE_INFORME = "navegarDetalleInforme";

  /** The Constant NAVEGAR_DETALLE_PROPUESTA_PIA. */
  public static final String NAVEGAR_DETALLE_PROPUESTA_PIA =
      "navegarDetallePropuestaPia";

  /** The Constant NAVEGAR_DETALLE_SOLICITUD. */
  public static final String NAVEGAR_DETALLE_SOLICITUD =
      "navegarDetalleSolicitud";

  /** The Constant NAVEGAR_INFORME_SOLICITUD. */
  public static final String NAVEGAR_INFORME_SOLICITUD =
      "navegarInformeSolicitud";

  /** The Constant NAVEGAR_RESOLUCION_EXPEDIENTE. */
  public static final String NAVEGAR_DETALLE_RESOLUCION =
      "navegarDetalleResolucion";

  /** The Constant NAVEGAR_LISTADO. */
  public static final String NAVEGAR_LISTADO = "navegarListado";

  /** The Constant NAVEGAR_PREFERENCIAS. */
  public static final String NAVEGAR_PREFERENCIAS = "navegarPreferencias";

  /** The Constant NAVEGAR_TERCEROS. */
  public static final String NAVEGAR_TERCEROS = "navegarTerceros";

  /** The Constant NAVEGAR_DOCUMENTOS_EXPEDIENTE. */
  public static final String NAVEGAR_DOCUMENTOS_EXPEDIENTE =
      "navegarDocumentosExpediente";

  /** The Constant NO_VALIDAR_SOLICITUD. */
  public static final String NO_VALIDAR_SOLICITUD = "noValidarSolicitud";

  /** The Constant NOTIFICAR_ACUSES. */
  public static final String NOTIFICAR_ACUSES = "notificarAcuses";

  /** The Constant NUEVA_INCIDENCIA. */
  public static final String NUEVA_INCIDENCIA = "nuevaIncidencia";

  /** The Constant NUEVA_SOLICITUD. */
  public static final String NUEVA_SOLICITUD = "nuevaSolicitud";

  /** The Constant NUEVO_DOCUMENTO. */
  public static final String NUEVO_DOCUMENTO = "nuevoDocumento";

  /** The Constant NUEVO_DOCUMENTO_SOLICITUD. */
  public static final String NUEVO_DOCUMENTO_SOLICITUD =
      "nuevoDocumentoSolicitud";

  /** The Constant NUEVO_REPRESENTANTE. */
  public static final String NUEVO_REPRESENTANTE = "nuevoRepresentante";

  /** The Constant OBTENER_CENTRO_DATOSPERSONALES_POPUP. */
  public static final String OBTENER_CENTRO_DATOSPERSONALES_POPUP =
      "obtenerCentroDatosPersonalesPopUp";

  /** The Constant OBTENER_CENTRO_DIRECCION_POPUP. */
  public static final String OBTENER_CENTRO_DIRECCION_POPUP =
      "obtenerCentroDireccionPopUp";

  /** The Constant OBTENER_CENTRO_POPUP. */
  public static final String OBTENER_CENTRO_POPUP = "obtenerCentroPopUp";

  /** The Constant OBTENER_CENTRO_PREFERENCIA_POPUP. */
  public static final String OBTENER_CENTRO_PREFERENCIA_POPUP =
      "obtenerCentroPreferenciaPopUp";

  /** The Constant OCULTAR_MENU_SUPERPUESTO. */
  public static final String OCULTAR_MENU_SUPERPUESTO =
      "ocultarMenuSuperpuesto";

  /** The Constant ON_INFORME_SOCIAL_GLOBAL. */
  public static final String ON_INFORME_SOCIAL_GLOBAL = "onInformeSocialGlobal";

  /** The Constant PENDIENTE_VALIDAR_SOLICITUD. */
  public static final String PENDIENTE_VALIDAR_SOLICITUD =
      "pendienteValidarSolicitud";

  /** The Constant PREVISUALIZAR_ACUSES. */
  public static final String PREVISUALIZAR_ACUSES = "previsualizarAcuses";

  /** The Constant GENERAR_EXCEL_NOTIFICACIONES_ACUSES. */
  public static final String GENERAR_EXCEL_NOTIFICACIONES_ACUSES =
      "generarExcelNotificaciones";

  /** The Constant REABRIR_VALORACION. */
  public static final String REABRIR_VALORACION = "reabrirValoracion";

  /** The Constant RECIBIR_DOCUMENTOS_REQUERIDOS. */
  public static final String RECIBIR_DOCUMENTOS_REQUERIDOS =
      "recibirDocumentosRequeridos";

  /** The Constant REFRESCAR_CABECERA_EXPEDIENTE. */
  public static final String REFRESCAR_CABECERA_EXPEDIENTE =
      "refrescarCabeceraExpediente";

  /** The Constant REFRESCAR_CENTROS. */
  public static final String REFRESCAR_CENTROS = "refrescarCentros";

  /** The Constant REFRESCAR_CUIDADORES_INFORME_SOCIAL. */
  public static final String REFRESCAR_CUIDADORES_INFORME_SOCIAL =
      "refrescarCuidadoresInformeSocial";

  /** The Constant REFRESCAR_ASISTENTES_INFORME_SOCIAL. */
  public static final String REFRESCAR_ASISTENTES_INFORME_SOCIAL =
      "refrescarAsistentesInformeSocial";

  /** The Constant REFRESCAR_FORMULARIO. */
  public static final String REFRESCAR_FORMULARIO = "refrescarFormulario";

  /** The Constant REFRESCAR_HEADER_PREFERENCIAS_TRAS_GUARDAR. */
  public static final String REFRESCAR_HEADER_PREFERENCIAS_TRAS_GUARDAR =
      "setConfigHeaderPreferenciasTrasGuardar";

  /** The Constant REFRESCAR_HEADER_PREFERENCIAS_TRAS_SELECCIONAR. */
  public static final String REFRESCAR_HEADER_PREFERENCIAS_TRAS_SELECCIONAR =
      "setConfigHeaderPreferenciasTrasSeleccionar";

  /** The Constant REFRESCAR_INFO_EXPEDIENTE. */
  public static final String REFRESCAR_INFO_EXPEDIENTE =
      "refrescarInfoExpediente";

  /** The Constant REFRESCAR_INFORME_SOCIAL_TRAS_GUARDADO. */
  public static final String REFRESCAR_INFORME_SOCIAL_TRAS_GUARDADO =
      "refrescarInformeSocialTrasGuardado";

  /** The Constant REFRESCAR_LISTADO_CUIDADORES. */
  public static final String REFRESCAR_LISTADO_CUIDADORES =
      "recargarListadoCuidadores";

  /** The Constant REFRESCAR_LISTADO_ASISTENTES. */
  public static final String REFRESCAR_LISTADO_ASISTENTES =
      "recargarListadoAsistentes";

  /** The Constant REFRESCAR_LISTADO_DOCUMENTOS_APORTADOS. */
  public static final String REFRESCAR_LISTADO_DOCUMENTOS_APORTADOS =
      "refrescarDocumentosAportados";

  /** The Constant REFRESCAR_LISTADO_DOCUMENTOS_GENERADOS. */
  public static final String REFRESCAR_LISTADO_DOCUMENTOS_GENERADOS =
      "refrescarDocumentosGenerados";

  /** The Constant REFRESCAR_LISTADO_INCIDENCIAS. */
  public static final String REFRESCAR_LISTADO_INCIDENCIAS =
      "refrescarListadoIncidencias";

  /** The Constant REFRESCAR_LISTADO_INFORMES_SOCIALES. */
  public static final String REFRESCAR_LISTADO_INFORMES_SOCIALES =
      "refrescarListadoInformesSociales";

  /** The Constant REFRESCAR_LISTADO_PERSONA_VIVE_ALTERNATIVA_INFORME_SOCIAL. */
  public static final String REFRESCAR_LISTADO_PERSONA_VIVE_ALTERNATIVA_INFORME_SOCIAL =
      "recargarListadoPersonaViveAlternativaInformeSocial";

  /** The Constant REFRESCAR_LISTADO_PERSONA_VIVE_CON_INFORME_SOCIAL. */
  public static final String REFRESCAR_LISTADO_PERSONA_VIVE_CON_INFORME_SOCIAL =
      "recargarListadoPersonaViveConInformeSocial";

  /** The Constant REFRESCAR_LISTADO_PREFERENCIAS. */
  public static final String REFRESCAR_LISTADO_PREFERENCIAS =
      "refrescarListadoPreferencias";

  /** The Constant REFRESCAR_LISTADO_PRESTACIONES_INFORME_SOCIAL. */
  public static final String REFRESCAR_LISTADO_PRESTACIONES_INFORME_SOCIAL =
      "recargarListadoPrestacionesInformeSocial";

  /** The Constant REFRESCAR_LISTADO_RESOLUCIONES. */
  public static final String REFRESCAR_LISTADO_RESOLUCIONES =
      "refrescarResoluciones";

  /** The Constant REFRESCAR_LISTADO_SOLICITUDES. */
  public static final String REFRESCAR_LISTADO_SOLICITUDES =
      "refrescarListadoSolicitudes";

  /** The Constant REFRESCAR_LISTADO_VALORACIONES_EXPEDIENTE. */
  public static final String REFRESCAR_LISTADO_VALORACIONES_EXPEDIENTE =
      "refrescarValoracionesPorExpediente";

  /** The Constant REFRESCAR_LISTADO_DOCUMENTOS. */
  public static final String REFRESCAR_LISTADOS_DOCUMENTOS =
      "refrescarDocumentos";

  /** The Constant REFRESCAR_LISTADOS_EXPEDIENTE. */
  public static final String REFRESCAR_LISTADOS_EXPEDIENTE =
      "refrescarListadosExpediente";

  /** The Constant REFRESCAR_PANTALLA_PREFERENCIAS. */
  public static final String REFRESCAR_PANTALLA_PREFERENCIAS =
      "setConfigPantallaInit";

  /** The Constant REFRESCAR_PERSONA_MANTENIMIENTO_CUIDADOR. */
  public static final String REFRESCAR_PERSONA_MANTENIMIENTO_CUIDADOR =
      "refrescarPersonaMantenimientoCuidador";

  /** The Constant REFRESCAR_PERSONA_REPRESENTANTE_LEGAL. */
  public static final String REFRESCAR_PERSONA_REPRESENTANTE_LEGAL =
      "refrescarPersonaRepresentanteLegal";

  /** The Constant REFRESCAR_PERSONA_UNIDAD_FAMILIAR. */
  public static final String REFRESCAR_PERSONA_UNIDAD_FAMILIAR =
      "refrescarPersonaUnidadFamiliar";

  /** The Constant REFRESCAR_PERSONA_VENTANA_SELECCIONADA. */
  public static final String REFRESCAR_PERSONA_VENTANA_SELECCIONADA =
      "refrescarPersonaVentanaSeleccionada";

  /** The Constant REFRESCAR_POP_UP_INFORME_SOCIAL. */
  public static final String REFRESCAR_POP_UP_INFORME_SOCIAL =
      "refrescarPopUpInformeSocial";

  /** The Constant REFRESCAR_VALORACIONES_POR_EXPEDIENTE. */
  public static final String REFRESCAR_VALORACIONES_POR_EXPEDIENTE =
      "refrescarValoracionesPorExpediente";

  /** The Constant REQUERIR. */
  public static final String REQUERIR = "requerir";

  /** The Constant RESETEAR_FILTROS. */
  public static final String RESETEAR_FILTROS = "resetearFiltros";

  /** The Constant RESOLVER_PIA. */
  public static final String RESOLVER_PIA = "resolverPia";

  /** The Constant SELECCIONAR_SOLICITUD. */
  public static final String SELECCIONAR_SOLICITUD = "seleccionarSolicitud";

  /** The Constant SET_CABECERA_DATOS_EXPEDIENTE. */
  public static final String SET_CABECERA_DATOS_EXPEDIENTE =
      "setCabeceraDatosExpediente";

  /** The Constant SET_CABECERA_SOLICITUDES. */
  public static final String SET_CABECERA_SOLICITUDES =
      "setCabeceraSolicitudes";

  /** The Constant SET_CABECERA_RESOLUCIONES. */
  public static final String SET_CABECERA_RESOLUCIONES =
      "setCabeceraResoluciones";

  /** The Constant SET_CENTRO_POR_ID. */
  public static final String SET_CENTRO_POR_ID = "setCentroPorId";

  /** The Constant SET_PROVINCIA_POR_ID. */
  public static final String SET_PROVINCIA_POR_ID = "setProvinciaPorId";

  /** The Constant SET_TAB_PANTALLA. */
  public static final String SET_TAB_PANTALLA = "setTabPantalla";

  /** The Constant SET_TEXTO_BOTONERA. */
  public static final String SET_TEXTO_BOTONERA = "setTextoBotonera";

  /** The Constant SUBSANAR. */
  public static final String SUBSANAR = "subsanar";

  /** The Constant VALIDA_VERSION. */
  public static final String VALIDA_VERSION = "validaVersion";

  /** The Constant VALIDAR_INFORME_SOCIAL. */
  public static final String VALIDAR_INFORME_SOCIAL = "validarInformeSocial";

  /** The Constant VALIDAR_SOLICITUD. */
  public static final String VALIDAR_SOLICITUD = "validarSolicitud";

  /** The Constant VER_DOCUMENTOS_ACUSES. */
  public static final String VER_DOCUMENTOS_ACUSES = "verDocumentosAcuses";

  /** The Constant VER_DOCUMENTOS_ACUSES. */
  public static final String IMPORTAR_ACUSES = "importarAcuses";

  /** The Constant VERIFICAR_DOCUMENTO. */
  public static final String VERIFICAR_DOCUMENTO = "verificarDocumento";

  /** The Constant MARCHA_ATRAS_DOCUMENTOS. */
  public static final String MARCHA_ATRAS_DOCUMENTOS = "marchaAtrasDocumento";

  /** The Constant ACCIONES_TRAS_GUARDAR_INCIDENCIA. */
  public static final String ACCIONES_TRAS_GUARDAR_INCIDENCIA =
      "accionesTrasGuardarIncidencia";

  /** The Constant BLOQUEAR_SOLICITUD. */
  public static final String BLOQUEAR_SOLICITUD_TEMPORAL =
      "bloquearSolicitudTemporalmente";

  /** The Constant DESBLOQUEAR_SOLICITUD. */
  public static final String DESBLOQUEAR_SOLICITUD_TEMPORAL =
      "desbloquearSolicitudTemporal";

  /** The Constant REFRESCAR_PERSONA_MANTENIMIENTO_CUIDADOR. */
  public static final String REFRESCAR_PERSONA_MANTENIMIENTO_ASISTENTE =
      "refrescarPersonaMantenimientoAsistente";

  /** The Constant INICIALIZAR_REPRESENTANTE_VM. */
  public static final String INICIALIZAR_REPRESENTANTE_VM =
      "inicializarRepresentanteVM";

  /** The Constant NUEVO_ESTUDIO_COMISION. */
  public static final String NUEVO_ESTUDIO_COMISION = "estudioComision";

  /** The Constant MOSTRAR_MODAL_DIAGNOSTICO. */
  public static final String MOSTRAR_POPUP_DIAGNOSTICO =
      "mostrarPopUpDiagnostico";

  /** The Constant NAVEGAR_DETALLE_SOLICITUD_POPUP. */
  public static final String NAVEGAR_DETALLE_SOLICITUD_POPUP =
      "navegarDetalleSolicitudPopup";

  /** The Constant ABRIR_POPUP_ESTUDIO_COMISION. */
  public static final String ABRIR_POPUP_ESTUDIO_COMISION = "abrirPopupEstudioComision";

  /** The Constant ACTUALIZAR_LISTADO_ESTUDIOS. */
  public static final String ACTUALIZAR_LISTADO_ESTUDIOS = "actualizarListadoEstudio";

  /**
   * Instantiates a new constantes command.
   */
  private ConstantesCommand() {
    // Empty constructor
  }
}
