package com.gval.gval.common;

import com.google.common.collect.ImmutableList;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * The Class WebUtils.
 */
public class WebUtils {


  /** The Constant FORMATO_URL. */
  public static final String FORMATO_URL = "%s%s";

  /** The Constant ERROR_MENSAJE. */
  public static final String ERROR_MENSAJE =
      "##### restTemplate error, url = {} ";

  /** The Constant GET_MENSAJE. */
  public static final String GET_MENSAJE =
      "Inicio restTemplate /GET {} request!";

  /** The Constant ERROR_MENSAJE_EXC. */
  public static final String ERROR_MENSAJE_EXC = "Error servicio REST ";

  /** The Constant SEPARADOR_LOG. */
  public static final String SEPARADOR_LOG =
      "-------------------------------------------------------------------- ";

  /** The Constant COMPARE_HTTP_OK. */
  public static final int COMPARE_HTTP_OK = 0;

  /** The Constant BLANCO. */
  public static final String BLANCO = "";

  /** The Constant CERO. */
  public static final int CERO = 0;

  /** The Constant UNO. */
  public static final int UNO = 1;

  /** The Constant DOS. */
  public static final int DOS = 2;

  /** The Constant FLOAT_MENOS_UNO. */
  public static final float FLOAT_MENOS_UNO = -1f;

  /** The Constant FLOAT_UNO. */
  public static final float FLOAT_UNO = 1F;

  /** The Constant CASO_90. */
  public static final int CASO_90 = 90;

  /** The Constant CASO_180. */
  public static final int CASO_180 = 180;

  /** The Constant CASO_270. */
  public static final int CASO_270 = 270;

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(WebUtils.class);


  /**
   * Instantiates a new web utils.
   */
  private WebUtils() {
    // constructor WebUtils
  }

  /**
   * Concat PD fs.
   *
   * @param streamOfPDFFiles the stream of PDF files
   * @param outputStream the output stream
   * @param paginate the paginate
   * @throws IOException Signals that an I/O exception has occurred.
   * @throws DocumentException the document exception
   */
  public static void concatPDFs(final List<InputStream> streamOfPDFFiles,
      final OutputStream outputStream, final boolean paginate)
      throws IOException, DocumentException {
    final Document document = new Document();
    try {
      final List<InputStream> pdfs = ImmutableList.copyOf(streamOfPDFFiles);
      final List<PdfReader> readers = new ArrayList<>();
      final Iterator<InputStream> iteratorPDFs = pdfs.iterator();

      while (iteratorPDFs.hasNext()) {
        final InputStream pdf = iteratorPDFs.next();

        final PdfReader pdfReader = new PdfReader(pdf);
        readers.add(pdfReader);

        if (esImpar(pdfReader.getNumberOfPages())) {
          readers.add(new PdfReader(crearPaginaBlanco()));
        }

      }

      final PdfWriter writer = PdfWriter.getInstance(document, outputStream);
      writer.setPageEmpty(false);

      document.open();
      final PdfContentByte cb = writer.getDirectContent();

      int pageOfCurrentReaderPDF = CERO;
      final Iterator<PdfReader> iteratorPDFReader = readers.iterator();

      while (iteratorPDFReader.hasNext()) {
        final PdfReader pdfReader = iteratorPDFReader.next();
        int pagina = 0;
        while (pageOfCurrentReaderPDF < pdfReader.getNumberOfPages()) {
          pagina++;
          // por cada pagina cogemos el rectangulo para saber si hay que
          // voltearla o no
          // (vertical/horizontal)

          final Rectangle rectangle = pdfReader.getPageSizeWithRotation(pagina);
          document.setPageSize(rectangle);
          document.newPage();

          pageOfCurrentReaderPDF++;
          final PdfImportedPage page =
              writer.getImportedPage(pdfReader, pageOfCurrentReaderPDF);

          voltearPagina(pdfReader, page, rectangle, cb, paginate);

        }
        pageOfCurrentReaderPDF = CERO;
      }
      outputStream.flush();
      document.close();
      outputStream.close();
    } catch (final Exception e) {
      LOG.error(e.getMessage(), e);
    } finally {
      if (document.isOpen()) {
        document.close();
      }
      try {
        if (outputStream != null) {
          outputStream.close();
        }
      } catch (final IOException ioe) {
        LOG.error("error al cerrar el output {}", ioe.getCause().getMessage());
      }
    }
  }

  /**
   * Voltear pagina.
   *
   * @param pdfReader the pdf reader
   * @param page the page
   * @param rectangle the rectangle
   * @param cb the cb
   * @param paginate the paginate
   */
  public static void voltearPagina(final PdfReader pdfReader,
      final PdfImportedPage page, final Rectangle rectangle,
      final PdfContentByte cb, final boolean paginate) {

    switch (rectangle.getRotation()) {
      case CERO:
        cb.addTemplate(page, FLOAT_UNO, CERO, CERO, FLOAT_UNO, CERO, CERO);
        break;
      case CASO_90:
        cb.addTemplate(page, CERO, FLOAT_MENOS_UNO, FLOAT_UNO, CERO, CERO,
            pdfReader.getPageSizeWithRotation(UNO).getHeight());
        break;
      case CASO_180:
        cb.addTemplate(page, FLOAT_MENOS_UNO, CERO, CERO, FLOAT_MENOS_UNO, CERO,
            CERO);
        break;
      case CASO_270:
        cb.addTemplate(page, CERO, FLOAT_UNO, FLOAT_MENOS_UNO, CERO,
            pdfReader.getPageSizeWithRotation(UNO).getWidth(), CERO);
        break;
      default:
        break;
    }
    if (paginate) {
      cb.beginText();
      cb.getPdfDocument().getPageSize();
      cb.endText();
    }
  }


  /**
   * Crear pagina blanco.
   *
   * @return the pdf reader
   * @throws IOException Signals that an I/O exception has occurred.
   * @throws DocumentException the document exception
   */
  public static byte[] crearPaginaBlanco() throws DocumentException {
    // Creación de la página en blanco.
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    final Document document = new Document();
    final PdfWriter writer = PdfWriter.getInstance(document, out);

    document.open();
    document.newPage();
    writer.setPageEmpty(false);
    document.close();

    return out.toByteArray();
  }

  /**
   * Es impar.
   *
   * @param numero the numero
   * @return true, if successful
   */
  public static boolean esImpar(final int numero) {
    return (numero % NumberUtils.INTEGER_TWO
        .intValue() != NumberUtils.INTEGER_ZERO.intValue());
  }

}


