package com.gval.gval.common.exception;

/**
 * 
 * nota de jarmero: Esta clase sólo debería usarse para enviar excepciones hasta
 * la capa de aplicación.
 * 
 */
public class CustomException extends RuntimeException {

    public static final Integer BUSINESS = 1;
    public static final Integer DATA_ACCES_EXCEPTION = 2;
    public static final Integer UNDEFINED = 3;
    public static final Integer ERROR = 4;
    public static final Integer FIRST_AUTHENTICATION = 5; 
    public static final Integer PASSWORD_EXPIRED = 6;

    
    private static final long serialVersionUID = 1L;

    public CustomException(Integer type, String message) {
        super();
        this.type = type;
        this.message = message;
    }

    public CustomException(String message) {
        super();
        this.type = BUSINESS;
        this.message = message;
    }

    private Integer type;
    private String message;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
