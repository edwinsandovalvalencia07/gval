package com.gval.gval.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * La utilizaremos cuando no devuelva una entidad en la busqueda.
 */
@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class DroolsException extends RuntimeException {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -6607483487600143865L;

  /**
   * Instantiates a new busqueda not found exception.
   *
   * @param message the message
   */
  public DroolsException(String message) {
    super(message);
  }
}
