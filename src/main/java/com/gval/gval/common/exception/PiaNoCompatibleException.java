package com.gval.gval.common.exception;

/**
 * Excepcion para Pias no complatibles
 */
public class PiaNoCompatibleException extends RuntimeException {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /**
   * Instantiates a new pia no compatible exception.
   *
   * @param message the message
   */
  public PiaNoCompatibleException(final String message) {
    super(message);
  }
}
