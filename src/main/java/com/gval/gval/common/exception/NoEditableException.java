package com.gval.gval.common.exception;

/**
 * Excepcion para recursos no editables.
 */
public class NoEditableException extends RuntimeException {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -1742791952649984237L;

  /**
   * Instantiates a new busqueda not found exception.
   *
   * @param message the message
   */
  public NoEditableException(final String message) {
    super(message);
  }
}
