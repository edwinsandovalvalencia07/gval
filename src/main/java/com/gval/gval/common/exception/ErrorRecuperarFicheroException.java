package com.gval.gval.common.exception;

/**
 * The Class ErrorRecuperarFicheroException.
 */
public class ErrorRecuperarFicheroException extends RuntimeException {


  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 4137191759847825583L;

  /**
   * Instantiates a new error recuperar fichero exception.
   */
  public ErrorRecuperarFicheroException() {
    super();
  }

  /**
   * Instantiates a new error recuperar fichero exception.
   *
   * @param message the message
   */
  public ErrorRecuperarFicheroException(final String message) {
    super(message);
  }

  /**
   * Instantiates a new error recuperar fichero exception.
   *
   * @param message the message
   * @param cause the cause
   */
  public ErrorRecuperarFicheroException(final String message,
      final Throwable cause) {
    super(message, cause);
  }

}
