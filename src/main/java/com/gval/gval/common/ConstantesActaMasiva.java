package com.gval.gval.common;

// TODO: Auto-generated Javadoc
/**
 * The Class ConstantesActaMasiva.
 */
public class ConstantesActaMasiva {

  /** The Constant MIGRADO_SIDEP. */
  public static final String MIGRADO_SIDEP = "S";

  /** The Constant SERVICIO. */
  public static final String SERVICIO = "SERVICIO";
  
  /** The Constant TIPO. */
  public static final String TIPO = "TIPO";
  
  /** The Constant CAMBIOGYN. */
  public static final String CAMBIOGYN = "CAMBIOGYN";

}
