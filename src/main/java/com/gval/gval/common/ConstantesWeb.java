package com.gval.gval.common;

/**
 * Class ConstantesWeb.
 *
 * @author Indra
 */
public final class ConstantesWeb {

  /** The Constant APP_NAME. */
  public static final String APP_NAME = "ADA";

  /** The Constant VALORACION_VERSION_DEFECTO. */
  public static final String VALORACION_VERSION_DEFECTO = "ADA3";

  /**
   * Valor altura de escritorio.
   */
  public static final String DESKTOP_HEIGHT = "DesktopHeight";

  /**
   * Valor por defecto de atlura de escritorio.
   */
  public static final int DESKTOP_HEIGHT_DEFECTO = 570;

  /**
   * Valor anchura de escritorio.
   */
  public static final String DESKTOP_WIDTH = "DesktopWidth";


  /**
   * ESPACIO.
   */
  public static final char ESPACIO = ' ';
  /**
   * Atributo constante IDIOMA_USUARIO de ConstantesWEB.
   */
  public static final String IDIOMA_USUARIO = "idiomaUsiario";

  // --- Parametros entre ventanas

  /**
   * Atributo constante MSGCONFIRMACION de Constantes.
   */
  public static final String MSGCONFIRMACION = "Confirmación";

  /**
   * Atributo constante MSGINFO de Constantes.
   */
  public static final String MSGINFO = "Info";

  /**
   * Atributo constante MSGSEGCONTROL de Constantes.
   */
  public static final String MSGSEGCONTROL = "mensaje.seguridad.control";
  /**
   * Atributo constante MSGSEGMENU de Constantes.
   */
  public static final String MSGSEGMENU = "mensaje.seguridad.menu";
  /**
   * Atributo constante MSGSEGWINDOW de Constantes.
   */
  public static final String MSGSEGWINDOW = "mensaje.seguridad.ventana";
  /**
   * Atributo constante MSGWARNING de Constantes.
   */
  public static final String MSGWARNING = "Warning";

  /** The Constant PANTALLA_BACKGROUND. */
  public static final String PANTALLA_BACKGROUND = "/comun/wBackAplicacion.zul";

  /**
   * Parametro filtro web, que contiene todos los objetos.
   */
  public static final String PARAMETRO_FILTROWEB = "filtroWeb";

  /**
   * Valor por defecto de atlura de pantalla.
   */
  public static final int SCREEN_HEIGHT_DEFECTO = 664;
  /** Parametro para pasar un textbox. */

  /**
   * VACIO.
   */
  public static final String VACIO = "";

  /**
   * Constante para variables de desktop zk.
   */
  public static final String VARIABLE_DESKTOP = "ZK_VBLE_DESKTOP_";

  /** The Constant ZK_ESTILO_DEFECTO. */
  public static final String ZK_ESTILO_DEFECTO = "sapphire";

  /** The Constant CERO. */
  public static final int CERO = 0;

  /** The Constant EXTENSION_PDF. */
  public static final String EXTENSION_PDF = ".pdf";

  /** The Constant BARRA_BAJA. */
  public static final String BARRA_BAJA = "_";

  /** The Constant DEFAULT_VERSION_ISOCIAL. */
  public static final Long DEFAULT_VERSION_INFSOCIAL = 3L;

  /**
   * Instancia un nuevo constantes web de ConstantesWeb.
   */
  private ConstantesWeb() {
    // Empty constructor
  }


}
