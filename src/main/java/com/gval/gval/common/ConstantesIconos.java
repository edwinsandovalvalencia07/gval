package com.gval.gval.common;

/**
 * Class ConstantesWeb.
 *
 * @author Indra
 */
public final class ConstantesIconos {

  /** The ico resoluciones. */
  public static final String ICO_RESOLUCIONES_SVG =
      "/static/public/ico/home/ico_resoluciones.svg";


  /** The ico resoluciones hover svg. */
  public static final String ICO_RESOLUCIONES_HOVER_SVG =
      "/static/public/ico/home/ico_resoluciones_hover.svg";

  /** The Constant ICO_SOLICITUDES_SVG. */
  public static final String ICO_SOLICITUDES_SVG =
      "/static/public/ico/home/ico_buscar_solicitud.svg";


  /** The ico resoluciones hover svg. */
  public static final String ICO_SOLICITUDES_HOVER_SVG =
      "/static/public/ico/home/ico_buscar_solicitud_hover.svg";

  /** The Constant ICO_SOLICITUDES_SVG. */
  public static final String ICO_NUEVA_SOLICITUDES_SVG =
      "/static/public/ico/home/ico_nueva_solicitud.svg";

  /** The ico resoluciones hover svg. */
  public static final String ICO_NUEVA_SOLICITUDES_HOVER_SVG =
      "/static/public/ico/home/ico_nueva_solicitud_hover.svg";

  /** The Constant ICO_SOLICITUDES_SVG. */
  public static final String ICO_ASIGNAR_SOLICITUDES_SVG =
      "/static/public/ico/home/ico_asignar_solicitud.svg";

  /** The ico resoluciones hover svg. */
  public static final String ICO_ASIGNAR_SOLICITUDES_HOVER_SVG =
      "/static/public/ico/home/ico_asignar_solicitud_hover.svg";

  /** The Constant ICO_SOLICITUDES_SVG. */
  public static final String ICO_CITACION_SVG =
      "/static/public/ico/home/ico_citacion.svg";

  /** The ico resoluciones hover svg. */
  public static final String ICO_CITACION_HOVER_SVG =
      "/static/public/ico/home/ico_citacion_hover.svg";

  /** The Constant ICO_SOLICITUDES_SVG. */
  public static final String ICO_BUSCAR_EXPEDIENTE_SVG =
      "/static/public/ico/home/ico_buscar_expediente.svg";

  /** The ico resoluciones hover svg. */
  public static final String ICO_BUSCAR_EXPEDIENTE_HOVER_SVG =
      "/static/public/ico/home/ico_buscar_expediente_hover.svg";

  /**
   * Instancia un nuevo constantes web de ConstantesWeb.
   */
  private ConstantesIconos() {
    // Empty constructor
  }


}
