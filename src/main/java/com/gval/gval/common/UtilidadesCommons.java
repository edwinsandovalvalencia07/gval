package com.gval.gval.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import jakarta.annotation.Nullable;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.poi.util.IOUtils;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import com.codebox.bean.JavaBeanTester;

import es.gva.dependencia.ada.common.exception.CustomException;
import es.gva.dependencia.ada.util.drools.FechasCalculator;



// TODO: Auto-generated Javadoc
/**
 * Utilidades.
 *
 * @author Indra
 */
public final class UtilidadesCommons {

  // TODO revisar todas las contantes de todas las clases. Están duplicadas. Las
  // que exitan en otras clases como StringUtils o CharUtils de lang3
  // utilizaremos esasa

  /** The Constant ERROR_PARSEANDO_ENTERO_MENSAJE. */
  private static final Properties properties = new Properties();

  /** The Constant ERROR_PARSEANDO_ENTERO_MENSAJE. */
  public static final String ERROR_PARSEANDO_ENTERO_MENSAJE =
      "Error parseando entero. Mensaje:";

  /** The Constant FORMAT_CALENDAR_YYYY_MM_DD. */
  public static final String FORMAT_CALENDAR_YYYY_MM_DD = "yyyy\\MM\\dd";

  /**
   * Formato date con fecha y sin hora.
   **/
  public static final String FORMAT_DATE_FECHA = "dd/MM/yyyy";

  /**
   * Formato date con fecha y hora.
   **/
  public static final String FORMAT_DATE_FECHA_HORA = "dd/MM/yyyy HH:mm";

  /** Formato date yyyyMMdd. **/
  public static final String FORMAT_DATE_YYYYMMDD = "yyyyMMdd";

  /** The Constant FORMATO_ANIO. */
  public static final String FORMATO_ANIO = "yyyy";

  /** The Constant FORMATO_HH_MM. */
  public static final String FORMATO_HH_MM = "HH:mm";

  /** The Constant FORMATO_HH_MM_SS. */
  public static final String FORMATO_HH_MM_SS = "hh:mm:ss";


  /** The Constant FORMATO_HORAS. */
  public static final String FORMATO_HORAS = "dd/MM/yyyy HH:mm";

  /** The Constant POSICICION2. */
  public static final int POSICICION2 = 2;

  /** The Constant LAZILY. */
  private static final CharSequence LAZILY = "LAZILY";

  /** The Constant LAZY. */
  private static final CharSequence LAZY = "LAZY";

  /** The Constant MAYOR_EDAD. */
  private static final int MAYOR_EDAD = 18;

  /** Atributo constante LOG. */
  private static final Logger LOG =
      LoggerFactory.getLogger(UtilidadesCommons.class);


  /**
   * Constructor.
   */
  private UtilidadesCommons() {
    super();
  }

  /**
   * Calcular edad.
   *
   * @param nacimiento the nacimiento
   * @return the long
   */
  public static Long calcularEdad(final Date nacimiento) {
    return calcularEdad(nacimiento, new Date());
  }

  /**
   * Calcular edad.
   *
   * @param nacimiento the nacimiento
   * @param diaActual the dia actual
   * @return the long
   */
  public static Long calcularEdad(final Date nacimiento, final Date diaActual) {
    if (nacimiento == null || diaActual == null) {
      throw new IllegalArgumentException();
    }
    return ChronoUnit.YEARS.between(
        nacimiento.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
        diaActual.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
  }

  /**
   * Calcular diferencia anios.
   *
   * @param fechaNacimiento the fecha nacimiento
   * @param fechaValoracion the fecha valoracion
   * @return the int
   */
  public static int calcularDiferenciaAnios(final Date fechaNacimiento, final Date fechaValoracion) {
    final LocalDate localDateNacimiento = convertToLocalDateTime(fechaNacimiento);
    final LocalDate localDateValoracion = convertToLocalDateTime(fechaValoracion);
    return FechasCalculator.calcularAnios(localDateNacimiento, localDateValoracion);
  }

  /**
   * Es mayor de edad.
   *
   * @param nacimiento the nacimiento
   * @param hoy the hoy
   * @return true, if successful
   */
  public static boolean esMayorDeEdad(final Date nacimiento, final Date hoy) {

    final Calendar ci = new GregorianCalendar();
    ci.setTime(nacimiento);
    ci.add(Calendar.YEAR, MAYOR_EDAD);

    final long suma18 = ci.getTime().getTime();
    final long ahora = hoy.getTime();

    return suma18 < ahora;

  }

  /**
   * Cast list.
   *
   * @param <T> the generic type
   * @param clazz the clazz
   * @param c the c
   * @return the list
   */
  public static <T> List<T> castList(final Class<? extends T> clazz,
      final Collection<?> c) {
    final List<T> result = new ArrayList<>(c.size());
    for (final Object o : c) {
      result.add(clazz.cast(o));
    }
    return result;
  }

  /**
   * Clone bytes.
   *
   * @param fichero the fichero
   * @return the byte[]
   */
  public static byte[] cloneBytes(final byte[] fichero) {
    return fichero == null ? null : fichero.clone();
  }

  /**
   * Collectors to list.
   *
   * @param fecha the fecha
   * @return the list
   */
  public static Date cloneDate(final Date fecha) {
    return fecha == null ? null : (Date) fecha.clone();
  }

  /**
   * Clona las propiedades de un objeto a otro de la misma clase y lo devuelve.
   *
   * @param <T> the generic type
   * @param object the object
   * @return the object
   *
   */
  @Nullable
  public static <T extends Serializable> T cloneObject(final T object) {
    return SerializationUtils.clone(object);
  }

  /**
   * Clone time stamp.
   *
   * @param timestampEjecucion the timestamp ejecucion
   * @return the timestamp
   */
  public static Timestamp cloneTimeStamp(final Timestamp timestampEjecucion) {
    return timestampEjecucion == null ? null
        : (Timestamp) timestampEjecucion.clone();
  }

  /**
   * Collectors to list.
   *
   * @param <T> the generic type
   * @param valor the valor
   * @return the list
   */
  @SuppressWarnings("squid:S1452")
  @Nullable
  public static <T> List<T> collectorsToList(final List<T> valor) {
    try {
      if (valor != null) {
        return valor.stream().collect(Collectors.toList());
      }
    } catch (final Exception ex) {
      if (!ex.getMessage().toUpperCase().contains(LAZY)
          && !ex.getMessage().toUpperCase().contains(LAZILY)) {
        LOG.error("Excepcion for en UtilsService.collectorsToList {}", ex);
      }
    }
    return null;
  }

  /**
   * Collectors to set.
   *
   * @param <T> the generic type
   * @param valor the valor
   * @return the sets the
   */
  @SuppressWarnings("squid:S1452")
  @Nullable
  public static <T> Set<T> collectorsToSet(final Set<T> valor) {
    try {
      if (valor != null) {
        return valor.stream().collect(Collectors.toSet());
      }
    } catch (final Exception ex) {
      if (!ex.getMessage().toUpperCase().contains(LAZY)
          && !ex.getMessage().toUpperCase().contains(LAZILY)) {
        LOG.error("Excepcion for en UtilsService.collectorsToSet {}", ex);
      }
    }
    return null;
  }

  /**
   * Concatenar errores con cabecera.
   *
   * @param sb1 the sb 1
   * @param sb2 the sb 2
   * @param cabecera the cabecera
   */
  public static void concatenarErroresConCabecera(final StringBuilder sb1,
      final StringBuilder sb2, final String cabecera) {
    if (sb2.length() > BigDecimal.ZERO.intValue()) {
      sb1.append(cabecera).append(CharUtils.LF).append(CharUtils.LF).append(sb2)
          .append(CharUtils.LF);
    }
  }

  /**
   * Convert big decimal to boolean.
   *
   * @param number the number
   * @return the boolean
   */
  public static Boolean convertBigDecimalToBoolean(final BigDecimal number) {
    return convertLongToBoolean(convertBigDecimalToLong(number));
  }

  /**
   * Convert big decimal to long.
   *
   * @param number the number
   * @return the long
   */
  public static Long convertBigDecimalToLong(final BigDecimal number) {
    return number == null ? null : number.longValue();
  }

  /**
   * Convert long to boolean.
   *
   * @param number the number
   * @return the boolean
   */
  public static Boolean convertLongToBoolean(final Long number) {
    if (number != null && number.equals(BigDecimal.ONE.longValue())) {
      return Boolean.TRUE;
    }
    return Boolean.FALSE;
  }

  /**
   * Convert object to big decimal.
   *
   * @param number the number
   * @return the big decimal
   */
  public static BigDecimal convertObjectToBigDecimal(final Object number) {
    return number == null ? null : (BigDecimal) number;
  }

  /**
   * Object to long.
   *
   * @param obj the obj
   * @return the long
   */
  public static Long convertObjectToLong(final Object obj) {
    return (obj == null) ? null : Long.valueOf(String.valueOf(obj));
  }

  /**
   * Convert object to int.
   *
   * @param obj the obj
   * @return the integer
   */
  public static Integer convertObjectToInt(final Object obj) {
	    return (obj == null) ? null : Long.valueOf(String.valueOf(obj)).intValue();
	  }

  /**
   * Object to string.
   *
   * @param obj the obj
   * @return the string
   */
  public static String convertObjectToString(final Object obj) {
    return (obj == null) ? null : StringUtils.trim(String.valueOf(obj));
  }

  /**
   * Copiar propiedades desde un objeto a otro ignorando nulos.
   *
   * @param source Objeto del cual queremos copiar las propiedades
   * @param target Objeto al que queremos copiar las propiedades
   */
  public static void copyNotNullProperties(final Object source,
      final Object target) {
    BeanUtils.copyProperties(source, target, getNullPropertyNames(source));
  }

  /**
   * Copy properties.
   *
   * @param source the source
   * @param target the target
   */
  public static void copyProperties(final Object source, final Object target) {
    BeanUtils.copyProperties(source, target);
  }

  /**
   * Copy properties.
   *
   * @param source the source
   * @param target the target
   * @param ignoreProperties the ignore properties
   */
  public static void copyProperties(final Object source, final Object target,
      final String... ignoreProperties) {
    BeanUtils.copyProperties(source, target, ignoreProperties);
  }

  /**
   * Copy array list properties.
   *
   * @param <T> the generic type
   * @param source the source
   * @param target the target
   */
  public static <T> void copyArrayListProperties(final List<T> source,
      final List<T> target) {
    BeanUtils.copyProperties(source, target);
    for (int i = 0; i < source.size(); i++) {
      BeanUtils.copyProperties(source.get(i), target.get(i));
    }
  }


  /**
   * Transforma la fecha de un string a un Date con el formato dd/MM/yyyy HH:mm.
   *
   * @param fecha Fecha a transformar
   * @return String representando la fecha
   */
  @Nullable
  public static String dateToString(final Date fecha) {
    return dateToString(fecha, FORMATO_HORAS);
  }

  /**
   * Convierte date en string.
   *
   * @param fecha Parámetro fecha
   * @param pFormato Parámetro formato
   * @return el string
   */
  @Nullable
  public static String dateToString(final Date fecha, final String pFormato) {
    String formato = pFormato;
    if (fecha != null) {
      if (formato == null) {
        formato = FORMAT_DATE_FECHA_HORA;
      }
      final SimpleDateFormat sdf = new SimpleDateFormat(formato, Locale.ITALY);
      return sdf.format(fecha);
    }
    return null;
  }

  /**
   * Dia anterior.
   *
   * @param fecha the fecha
   * @return the date
   */
  public static Date diaAnterior(final Date fecha) {
    final Calendar diaAnterior = Calendar.getInstance();
    diaAnterior.setTime(fecha);
    diaAnterior.add(Calendar.DATE, -1);
    return diaAnterior.getTime();
  }

  /**
   * Dia posterior.
   *
   * @param fecha the fecha
   * @return the date
   */
  public static Date diaPosterior(final Date fecha) {
    final Calendar diaPosterior = Calendar.getInstance();
    diaPosterior.setTime(fecha);
    diaPosterior.add(Calendar.DATE, +1);
    return diaPosterior.getTime();
  }


  /**
   * Distinct by key.
   *
   * @param <T> the generic type
   * @param keyExtractor the key extractor
   * @return the predicate
   */
  public static <T> Predicate<T> distinctByKey(
      final Function<? super T, ?> keyExtractor) {
    final Set<Object> seen = ConcurrentHashMap.newKeySet();
    return t -> seen.add(keyExtractor.apply(t));
  }

  /**
   * Devuelve un array con el nombre de las propiedades nulas de un objeto.
   *
   * @param source Objeto del que queremos extraer las propiedades nulas
   * @return Nombre de las propiedades nulas del objeto
   */
  private static String[] getNullPropertyNames(final Object source) {
    final BeanWrapper src = new BeanWrapperImpl(source);
    final java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();
    final Set<String> emptyNames = new HashSet<>();
    for (final java.beans.PropertyDescriptor pd : pds) {
      // check if value of this property is null then add it to the collection
      final Object srcValue = src.getPropertyValue(pd.getName());
      if (srcValue == null) {
        emptyNames.add(pd.getName());
      }
    }
    final String[] result = new String[emptyNames.size()];
    return emptyNames.toArray(result);
  }

  /**
   * Valida si el objeto pasado no es nulo.
   *
   * @param nulo Objeto a Validar
   * @return si es valido o no
   */
  public static boolean isNotNull(final Object nulo) {
    return !isNull(nulo);
  }

  /**
   * Valida si el objeto pasado es nulo.
   *
   * @param nulo Objeto a Validar
   * @return si es válido o no
   */
  public static boolean isNull(final Object nulo) {
    boolean resultado = false;
    if (nulo == null) {
      resultado = true;
    }
    return resultado;
  }

  /**
   * Valida si el objeto List pasado es valido (No es nulo y tiene contenido).
   *
   * @param token Lista a Validar
   * @return si es valido o no
   */
  public static boolean isValidList(final List<?> token) {
    return CollectionUtils.isNotEmpty(token);
  }

  /**
   * Valida si el parametro String es valido (No es nulo y tiene contenido).
   *
   * @param token String a Validar
   * @return si es valido o no
   */
  public static boolean isValidString(final String token) {
    return StringUtils.isNotEmpty(token);
  }

  /**
   * Leer input stream A byte array.
   *
   * @param inputStream the input stream
   * @return the byte[]
   * @throws IOException Signals that an I/O exception has occurred.
   */
  public static byte[] leerInputStreamAByteArray(
      final Optional<InputStream> inputStream) throws IOException {
    byte[] fichero = new byte[0];

    if (!inputStream.isPresent()) {
      LOG.error("Se ha recibido InputStream vacío");
      return fichero;
    }

    try {
      fichero = IOUtils.toByteArray(inputStream.get());
    } catch (final IOException ioe) {
      LOG.error("Error al convertir los datos en array de bytes", ioe);
      throw ioe;
    }
    return fichero;
  }


  /**
   * Not equals.
   *
   * @param <T> the generic type
   * @param valora the valora
   * @param valorb the valorb
   * @return true si los valores son distintos. Si se comparan dos nulos,
   *         devuelve false
   *
   */
  public static <T extends Comparable<T>> boolean notEquals(final T valora,
      final T valorb) {
    if (isNotNull(valora) && isNotNull(valorb)) {
      return valora.compareTo(valorb) != 0;
    } else if (isNull(valora) && isNull(valorb)) {
      return false;
    }
    return true;
  }

  /**
   * equals.
   *
   * @param <T> the generic type
   * @param valora the valora
   * @param valorb the valorb
   * @return true si los valores son iguales. Si se comparan dos nulos, devuelve
   *         false
   *
   */
  public static <T extends Comparable<T>> boolean equals(final T valora,
      final T valorb) {
    if (isNotNull(valora) && isNotNull(valorb)) {
      return valora.compareTo(valorb) == 0;
    } else if (isNull(valora) || isNull(valorb)) {
      return false;
    }
    return true;
  }



  /**
   * Valida si el objeto es nulo, y en su caso nos devuelve el objeto sustituto.
   *
   * @author jdelgadom - www.minsait.com
   * @param inputToCheck Objeto a valorar si es nulo
   * @param nullSubstition Objeto susutituto en caso que a sea nulo
   * @return El objeto a si no es nulo, b en caso que a sea nulo
   */
  public static String nvl(final String inputToCheck,
      final String nullSubstition) {
    return Optional.ofNullable(inputToCheck).orElse(nullSubstition);
  }

  /**
   * Valida si el objeto es nulo, y en su caso nos devuelve el objeto sustituto.
   *
   * @param <T> the generic type
   * @param <S> the generic type
   * @param value the value
   * @param method Objeto a valorar si es nulo
   * @param nullSubstition Objeto susutituto en caso que a sea nulo
   * @return El objeto a si no es nulo, b en caso que a sea nulo
   */
  public static <T, S> S nvl(final T value, final Function<T, S> method,
      final S nullSubstition) {
    return Optional.ofNullable(value).map(method).orElse(nullSubstition);
  }


  /**
   * Valida si el String es nulo, y en su caso nos devuelve un vacío. Se añade
   * la funcionalidad con los optional de Java.
   *
   * @author jdelgadom - www.minsait.com
   * @param inputToCheck Objeto a valorar si es nulo
   * @return El objeto a si no es nulo, vacío en caso que a sea nulo
   */
  public static String nvlDefect(final String inputToCheck) {
    return Optional.ofNullable(inputToCheck).orElse(StringUtils.EMPTY);
  }

  /**
   * Quitar ultimo salto linea.
   *
   * @param message the message
   * @return the string builder
   */
  public static StringBuilder quitarUltimoSaltoLinea(
      final StringBuilder message) {
    if (message.toString().isEmpty()) {
      return message;
    }
    return new StringBuilder(message.substring(0, message.length() - 1));
  }

  /**
   * Resolve.
   *
   * @param <T> the generic type
   * @param resolver the resolver
   * @return the optional
   */
  @SuppressWarnings("squid:S1166")
  public static <T> Optional<T> resolve(final Supplier<T> resolver) {
    try {
      final T result = resolver.get();
      return Optional.ofNullable(result);
    } catch (final NullPointerException e) {
      return Optional.empty();
    }
  }

  /**
   * Es no nulo. Comprueba si alguno de los objetos que se le pasan en el
   * supplier son no nulos.
   *
   * Ej/ esNoNulo(() -> solicitud.getEstadoActivo().getEstado()) Comprobaria si
   * solicitud, estadoActivo o estado son no nulos.
   *
   * @param <T> the generic type
   * @param resolver the resolver
   * @return true, si no es nulo
   */
  public static <T> boolean esNoNulo(final Supplier<T> resolver) {
    return resolve(resolver).isPresent();
  }

  /**
   * Es nulo.
   *
   * @param <T> the generic type
   * @param resolver the resolver
   * @return true, if successful
   */
  public static <T> boolean esNulo(final Supplier<T> resolver) {
    return BooleanUtils.isFalse(resolve(resolver).isPresent());
  }

  /**
   * Compara si el valor de dos Long son iguales.
   *
   * @param valora Long a Comparar
   * @param valorb Long a Comparar
   * @return true si son iguales false si no son iguales
   */
  public static boolean sonIguales(final Date valora, final Date valorb) {
    boolean salida = false;
    if (isNotNull(valora) && isNotNull(valorb)) {
      salida = (valora.compareTo(valorb) == 0);
    }

    return salida;
  }

  /**
   * No son iguales.
   *
   * @param valora the valora
   * @param valorb the valorb
   * @return true, if successful
   */
  public static boolean noSonIguales(final Date valora, final Date valorb) {
    return !sonIguales(valora, valorb);
  }

  /**
   * Obtener calendar.
   *
   * @param fecha Fecha
   * @param formato the formato
   * @return Fecha establecida a primera hora
   */
  public static Calendar stringToCalendar(final String fecha,
      final String formato) {
    Calendar calendar = null;
    if (isNotNull(fecha) && isNotNull(formato)) {
      calendar = Calendar.getInstance();
      calendar.setTime(stringToDate(fecha, formato));
    }
    return calendar;
  }

  /**
   * Transforma la fecha de un Date a String con el formato dd/MM/yyyy HH:mm.
   *
   * @param fecha Fecha a transformar
   * @return String representando la fecha
   */
  public static Date stringToDate(final String fecha) {
    return stringToDate(fecha, FORMAT_DATE_FECHA);
  }

  /**
   * Transforma la fecha de un String a Date.
   *
   * @param fecha Fecha a transformar
   * @param formato the formato
   * @return Date representando la fecha
   */
  public static Date stringToDate(final String fecha, final String formato) {
    Date resultado = null;
    if (isNotNull(fecha)) {
      final SimpleDateFormat sdf = new SimpleDateFormat(formato, Locale.ITALY);
      try {
        resultado = sdf.parse(fecha);
      } catch (final ParseException e) {
        resultado = null;
      }
    }
    return resultado;
  }

  /**
   * Strip accents.
   *
   * @param cadena the cadena
   * @return the string
   */
  @Nullable
  public static String stripAccents(String cadena) {
    if (cadena == null) {
      return null;
    }

    /* Salvamos las ñ */
    cadena = cadena.replace('ñ', '\001');
    cadena = cadena.replace('Ñ', '\002');
    /* Salvamos las ç */
    cadena = cadena.replace('ç', '\003');
    cadena = cadena.replace('Ç', '\004');
    cadena = StringUtils.stripAccents(cadena);
    /* Volvemos las ñ a la cadena */
    cadena = cadena.replace('\001', 'ñ');
    cadena = cadena.replace('\002', 'Ñ');
    /* Volvemos las ç a la cadena */
    cadena = cadena.replace('\003', 'ç');
    cadena = cadena.replace('\004', 'Ç');

    return cadena;
  }

  /**
   * Contains ignore case and accents.
   *
   * @param str the str
   * @param searchStr the search str
   * @return true, if successful
   */
  public static boolean containsIgnoreCaseAndAccents(final String str,
      final String searchStr) {
    return StringUtils.containsIgnoreCase(UtilidadesCommons.stripAccents(str),
        UtilidadesCommons.stripAccents(searchStr));
  }

  /**
   * Contains ignore case and accents.
   *
   * @param str the str
   * @param searchStr the search str
   * @return true, if successful
   */
  public static boolean equalsIgnoreCaseAndAccents(final String str,
      final String searchStr) {
    return StringUtils.equalsIgnoreCase(UtilidadesCommons.stripAccents(str),
        UtilidadesCommons.stripAccents(searchStr));
  }

  /**
   * Sumar meses A fecha.
   *
   * @param fecha the fecha
   * @param meses the meses
   * @return the date
   */
  public static Date sumarMesesAFecha(final Date fecha, final int meses) {
    final Calendar cal = Calendar.getInstance();
    cal.setTime(fecha);
    cal.add(Calendar.MONTH, +meses);
    return new Date(cal.getTime().getTime());
  }

  /**
   * Clear punctuation upper.
   *
   * @param valor the valor
   * @return the string
   */
  @Nullable
  public static String clearPunctuationUpper(final String valor) {
    if (StringUtils.isBlank(valor)) {
      return null;
    }
    return stripAccents(valor.toUpperCase().trim());
  }

  /**
   * Redondear double.
   *
   * @param puntuacion the puntuacion
   * @return the long
   */
  public static Long redondearDouble(final Double puntuacion) {
    if (puntuacion != null) {
      BigDecimal bd = BigDecimal.valueOf(puntuacion.doubleValue());
      bd = bd.setScale(NumberUtils.INTEGER_ZERO, RoundingMode.HALF_UP);
      return bd.longValue();
    }
    return NumberUtils.LONG_ZERO;
  }

  /**
   * Texto entre parentesis.
   *
   * @param texto the texto
   * @return the string
   */
  @Nullable
  public static String textoEntreParentesis(final String texto) {
    if (StringUtils.isBlank(texto)) {
      return null;
    }
    final StringBuilder sb = new StringBuilder();
    return sb
        .append(CharUtils.toCharacterObject(ConstantesCommons.PARENTESIS_ABRE))
        .append(texto)
        .append(
            CharUtils.toCharacterObject(ConstantesCommons.PARENTESIS_CIERRA))
        .toString();
  }

  /**
   * Es fecha anterior.
   *
   * @param fecha1 the fecha 1
   * @param fecha2 the fecha 2
   * @return true, if successful
   */
  public static boolean esFechaAnterior(final Date fecha1, final Date fecha2) {
    return fecha1 != null && fecha2 != null
        && DateUtils.truncatedCompareTo(fecha1, fecha2, Calendar.DATE) < 0;
  }

  /**
   * Es fecha posterior.
   *
   * @param fecha1 the fecha 1
   * @param fecha2 the fecha 2
   * @return true, if successful
   */
  public static boolean esFechaIgualOPosterior(final Date fecha1,
      final Date fecha2) {
    return fecha1 != null && fecha2 != null
        && DateUtils.truncatedCompareTo(fecha1, fecha2, Calendar.DATE) >= 0;
  }

  /**
   * Test all the classes from the package except the exclude classes.
   *
   * @param excludeClass the exclude class
   * @param thePackage the packages
   */

  /**
   * Test class from package.
   *
   * @param excludeClass the exclude class
   * @param thePackage the the package
   */
  @SuppressWarnings({"squid:S1181"})
  // squid:S1181 se utiliza para poder capturar otra informacion
  public static void testClassFromPackage(
      final Set<Class<? extends Object>> excludeClass,
      final String... thePackage) {

    LOG.info("Testing package {}", Arrays.toString(thePackage));
    // Obten las clases
    final Set<Class<? extends Object>> allTestClass =
        UtilidadesCommons.getAllClassFromPackage(excludeClass, thePackage);

    // ejecuta el test en todas las clases
    final StringBuilder salida = new StringBuilder();
    allTestClass.forEach(clazz -> {
      try {
        salida.append("Testing class ").append(clazz.getName()).append(": ");
        JavaBeanTester.builder(clazz).skipStrictSerializable().test();

        ejecutaOtrosMetodos(clazz);
        salida.append("tested");

        LOG.info(salida.toString());
      } catch (final Throwable e) {
        salida.append(e.getMessage() == null ? e : e.getMessage());
        LOG.error(salida.toString());
        throw e;
      } finally {
        salida.setLength(0);
      }

    });
  }

  /**
   * Ejecuta otros metodos.
   *
   * @param clazz the clazz
   */

  /**
   * Ejecuta otros metodos.
   *
   * @param clazz the clazz
   */
  @SuppressWarnings({"squid:S1166", "squid:S1481",
      "squid:fb-contrib:NPMC_NON_PRODUCTIVE_METHOD_CALL",
      "squid:findbugs:RV_RETURN_VALUE_IGNORED_NO_SIDE_EFFECT"})
  // squid:S106 Solo nos interesa que se ejecute el procedimiento
  // squid:S1481 Solo nos interesa que se ejecute el procedimiento
  // fb-contrib:NPMC_NON_PRODUCTIVE_METHOD_CALL Solo nos interesa que se ejecute
  // el procedimiento
  // findbugs:RV_RETURN_VALUE_IGNORED_NO_SIDE_EFFECT Solo nos interesa que se
  // ejecute el procedimiento
  private static void ejecutaOtrosMetodos(final Class<? extends Object> clazz) {
    try {
      clazz.getDeclaredConstructor().newInstance().toString();
    } catch (final Exception e) {
      LOG.warn("Error ejecutando otros metodos (toString)");
    }
  }

  /**
   * Gets the all class from package, except the set of the exclude Class.
   *
   * @param excludeClass the exclude class
   * @param thePackage the the package
   * @return the all class from package
   */
  public static Set<Class<? extends Object>> getAllClassFromPackage(
      final Set<Class<? extends Object>> excludeClass,
      final String... thePackage) {
    if (StringUtils.isAllBlank(thePackage)) {
      return new LinkedHashSet<>();
    }

    // get all class from the package
    final Reflections reflections =
        new Reflections(thePackage, new SubTypesScanner(false));
    final Set<Class<? extends Object>> allClasses =
        reflections.getSubTypesOf(Object.class);

    // drop excludes
    allClasses.removeAll(excludeClass);
    return allClasses;
  }


  /**
   * Convert to local date time.
   *
   * @param fecha the fecha
   * @return the long
   */
  public static LocalDate convertToLocalDateTime(final Date fecha) {
    if (fecha == null) {
      throw new IllegalArgumentException(
          "Error al convertir a LocalDateTime la fecha no puede ser nula");
    }
    return Instant.ofEpochMilli(fecha.getTime()).atZone(ZoneId.systemDefault())
        .toLocalDate();
  }

  /**
   * Fecha se encuentra rango. Comprueba si la fecha esta entre la fecha de
   * referencia + rango y fecha de referencia - anyos. Incluyendo los días
   * limite.
   *
   * @param fechaComprobacion the fecha
   * @param fechaReferencia the referencia
   * @param rango the rango
   * @return true, if successful
   */
  public static boolean fechaSeEncuentraRango(final Date fechaComprobacion,
      final LocalDate fechaReferencia, final int rango) {
    final LocalDate localDateComprobacion =
        UtilidadesCommons.convertToLocalDateTime(fechaComprobacion);
    final LocalDate fechaLimiteInferior = fechaReferencia.plusYears(-rango);
    final LocalDate fechaLimiteSuperior = fechaReferencia.plusYears(rango);

    return !(localDateComprobacion.isBefore(fechaLimiteInferior)
        || localDateComprobacion.isAfter(fechaLimiteSuperior));
  }


  /**
   * Es fecha posterior A hoy.
   *
   * @param fechaComprobacion the fecha comprobacion
   * @return true, if successful
   */
  public static boolean esFechaPosteriorAHoy(final Date fechaComprobacion) {
    return esFechaPosteriorAFecha(fechaComprobacion, new Date());
  }

  /**
   * Es fecha posterior A fecha.
   *
   * @param fechaComprobacion the fecha comprobacion
   * @param fechaReferencia the fecha referencia
   * @return true, if successful
   */
  protected static boolean esFechaPosteriorAFecha(final Date fechaComprobacion,
      final Date fechaReferencia) {
    final LocalDate localDateComprobacion =
        UtilidadesCommons.convertToLocalDateTime(fechaComprobacion);
    final LocalDate localDateReferencia =
        UtilidadesCommons.convertToLocalDateTime(fechaReferencia);
    return localDateComprobacion.isAfter(localDateReferencia);
  }

  /**
   * Not in.
   *
   * @param valor the valor
   * @param conjunto the conjunto
   * @return true, if successful
   */
  public static boolean notIn(final String valor, final List<String> conjunto) {
    return BooleanUtils.isFalse(conjunto.contains(valor));
  }

  /**
   * All false.
   *
   * @param condiciones the condiciones
   * @return true, if successful
   */
  public static boolean allFalse(final boolean... condiciones) {
    return BooleanUtils.isFalse(BooleanUtils.or(condiciones));
  }

  /**
   * Round.
   *
   * @param number the number
   * @param decimal the decimal
   * @return the float
   */
  public static float round(final float number, final int decimal) {
    BigDecimal bd = new BigDecimal(Float.toString(number));
    bd = bd.setScale(decimal, BigDecimal.ROUND_HALF_UP);
    return bd.floatValue();
  }

  /**
   * Gets the bytes from file.
   *
   * @param file the file
   * @return the bytes from file
   */
  public static byte[] getBytesFromFile(final File file) {
    try {
      final InputStream is = new FileInputStream(file);

      // Get the size of the file
      final long length = file.length();

      // You cannot create an array using a long type.
      // It needs to be an int type.
      // Before converting to an int type, check
      // to ensure that file is not larger than Integer.MAX_VALUE.
      if (length > Integer.MAX_VALUE) {
        // File is too large
      }

      // Create the byte array to hold the data
      final byte[] bytes = new byte[(int) length];

      // Read in the bytes
      int offset = 0;
      int numRead = 0;
      while ((offset < bytes.length) && ((numRead = is.read(bytes, offset, bytes.length - offset)) >= 0)) {
        offset += numRead;
      }

      // Ensure all the bytes have been read in
      if (offset < bytes.length) {
        throw new CustomException("Could not completely read file " + file.getName());
      }

      // Close the input stream and return bytes
      is.close();
      return bytes;
    } catch (final Exception e) {
      LOG.error("Error al obtener contenido de fichero " + e.getMessage(), e);
      throw new CustomException("Error al obtener contenido de fichero " + e.getMessage());
    }
  }

  /**
   * Obtener anio.
   *
   * @param fecha the fecha
   * @return the int
   */
  public static int obtenerAnio(final Date fecha) {
    final Calendar calendario = Calendar.getInstance();
    calendario.setTime(fecha);
    return calendario.get(Calendar.YEAR);
  }

  /**
   * Obtener mes.
   *
   * @param fecha the fecha
   * @return the int
   */
  public static int obtenerMes(final Date fecha) {
    final Calendar calendario = Calendar.getInstance();
    calendario.setTime(fecha);
    return calendario.get(Calendar.MONTH) + 1; // Sumar 1 porque los meses en Calendar van de 0 a 11
  }

  /**
   * Obtener dia.
   *
   * @param fecha the fecha
   * @return the int
   */
  public static int obtenerDia(final Date fecha) {
    final Calendar calendario = Calendar.getInstance();
    calendario.setTime(fecha);
    return calendario.get(Calendar.DAY_OF_MONTH);
  }

  /**
   * Convert long to string.
   *
   * @param value the value
   * @return the string
   */
  public static String convertLongToString(final Long value) {
    return (value == null) ? "" : String.valueOf(value);
  }


  /**
   * Filtrar duplicados.
   *
   * @param arrClave the arr clave
   * @param arrValor the arr valor
   * @param importaOrden the importa orden
   * @return the string[][]
   */
  public static String[][] filtrarDuplicados(final String[] arrClave, final String[] arrValor, final Boolean importaOrden) {
    Validate.isTrue(arrClave.length == arrValor.length, "Los array no tienen la misma longitud");
    final Map<String, String> map = importaOrden ? new LinkedHashMap<String, String>() : new HashMap<String, String>();

    for (int i = 0; i < arrClave.length; i++) {
      map.put(arrClave[i], arrValor[i]);
    }

    final String[] arrClaveFiltrado = new String[map.size()];
    final String[] arrValorFiltrado = new String[map.size()];

    int j = 0;
    for (final Map.Entry<String, String> mapEntry : map.entrySet()) {
      arrClaveFiltrado[j] = mapEntry.getKey();
      arrValorFiltrado[j] = mapEntry.getValue();
      j++;
    }

    return new String[][] {arrClaveFiltrado, arrValorFiltrado};
  }

  /**
   * Recuperar propiedad.
   */
  public static void recuperarPropiedad() {
    try (InputStream input = UtilidadesCommons.class.getResourceAsStream("/application.properties")) {
      if (input == null) {
        System.err.println("No se pudo encontrar el archivo application.properties en el classpath.");
      }
      properties.load(input);

    } catch (IOException e) {
      e.printStackTrace();
      // Manejo de excepción
    }
  }

  /**
   * Proper.
   *
   * @param clavePropiedad the clave propiedad
   * @return the property
   */
  public static String getProperty(final String clavePropiedad) {
    recuperarPropiedad();
    // Obtener un valor específico del archivo de propiedades
    String valorPropiedad = properties.getProperty(clavePropiedad);

    if (valorPropiedad == null) {
      System.out.println("Valor de la propiedad 'clavePropiedad': " + valorPropiedad);
    }
    return valorPropiedad;
  }

  /**
   * Validate is null.
   *
   * @param objeto the objeto
   * @param mensajeError the mensaje error
   */
  public static void validateIsNull(Object objeto, String mensajeError) {
    if (objeto != null) {
      throw new IllegalArgumentException(mensajeError);
    }
  }

  /**
   * Checks if is empty or null.
   *
   * @param texto the texto
   * @return true, if is empty or null
   */
  private static boolean isEmptyOrNull(final String texto) {
    return ((texto != null) && !(texto.trim().length() == 0));
  }

  /**
   * Not is empty or null.
   *
   * @param texto the texto
   * @param msg the msg
   * @throws RuntimeException the runtime exception
   */
  public static void notIsEmptyOrNull(final String texto, final String msg) throws RuntimeException {
    if (!isEmptyOrNull(texto)) {
      throw new IllegalArgumentException(msg);
    }
  }

}
