package com.gval.gval.common;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class ConstantesCommons.
 */
public class ConstantesCommons {

  /** The Constant VALOR_STRING_BBBDD_TRUE. */
  public static final String VALOR_STRING_BBBDD_TRUE = "S";

  /** The Constant VALOR_STRING_BBBDD_FALSE. */
  public static final String VALOR_STRING_BBBDD_FALSE = "N";

  /** The Constant VALORES_POR_DEFECTO. */
  public static final boolean VALORES_POR_DEFECTO = true;

  /** The Constant APLICACION_ADA3. */
  public static final String APLICACION_ADA3 = "ADA3";

  /** The Constant PARENTESIS_ABRE. */
  public static final String PARENTESIS_ABRE = "(";

  /** The Constant PARENTESIS_CIERRE. */
  public static final String PARENTESIS_CIERRA = ")";

  /** The Constant SELECT_COUNT. */
  public static final String SELECT_COUNT_MAYOR_QUE_CERO_AS_RESULTADO =
      "SELECT CASE WHEN COUNT(1) > 0 THEN 'S' ELSE 'N' END AS resultado ";

  /** The Constant VINYETA. */
  public static final String VINYETA = "•";

  /** The Constant LISTA_VINYETA. */
  public static final String LISTA_VINYETA =
      StringUtils.SPACE.concat(VINYETA).concat(StringUtils.SPACE);

  /** The Constant SIN_LONGITUD. */
  public static final int SIN_LONGITUD = 0;

  /** The Constant UN_ELEMENTO. */
  public static final int UN_ELEMENTO = 1;

  /** The Constant VACIO. */
  public static final String VACIO = "";

  /** The Constant FORMATO_PDF. */
  public static final String FORMATO_PDF = "PDF";

  /** The Constant COMA. */
  public static final char COMA = ',';

  /** The Constant DOS_PUNTOS_STR. */
  public static final String DOS_PUNTOS_STR = ":";
  /** The Constant DOS_PUNTOS. */
  public static final char DOS_PUNTOS_CHAR = ':';

  /** The Constant GUION. */
  public static final String GUION = "-";
  /** The Constant ESPACIO_GUION. */
  public static final String ESPACIO_GUION = " - ";

  /** The Constant COMILLAS. */
  public static final String COMILLAS = "\"";

  /** The Constant COMILLA_SIMPLE. */
  public static final String COMILLA_SIMPLE = "'";

  /** The Constant PUNTO. */
  public static final String PUNTO = ".";

  /** The Constant PUNTOYCOMA. */
  public static final String PUNTOYCOMA = ";";

  /** The Constant ESPACIO_CHAR. */
  public static final char ESPACIO_CHAR = ' ';

  /** The Constant VALENCIANO. */
  public static final String VALENCIANO = "ca_ES";

  /** The Constant FORMAT_DATE_FECHA. */
  public static final String FORMAT_DATE_FECHA = "dd/MM/yyyy";

  /** The Constant FORMAT_DATE_FECHA. */
  public static final String INI_CATA_SERV = "-1";

  /** The Constant FECHA_PUBLICACION_NUEVA_SOLICITUD. */
  public static final String FECHA_PUBLICACION_NUEVA_SOLICITUD =
      "FECHA_PUBL_NUEVA_SOLICITUD";

  /** The Constant ESTADO_PROCESO_FINAL. */
  public static final long ESTADO_PROCESO_FINAL = 10L;

  /** Instantiates a new constantes commons. */
  private ConstantesCommons() {
    // Constructor por defecto
  }

}
