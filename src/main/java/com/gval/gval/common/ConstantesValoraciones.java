package com.gval.gval.common;

/**
 * The Class ConstantesValoraciones.
 */
public class ConstantesValoraciones {
  
  /** The Constant GRADO_UNO. */
  public static final int GRADO_UNO = 1;
  
  /** The Constant GRADO_DOS. */
  public static final int GRADO_DOS = 2;
  
  /** The Constant GRADO_TRES. */
  public static final int GRADO_TRES = 3;
  
  /** The Constant NIVEL_UNO. */
  public static final int NIVEL_UNO = 1;
  
  /** The Constant NIVEL_DOS. */
  public static final int NIVEL_DOS = 2;
  
  /** The Constant NUMPREGUNTAS_PESO1_GRADO2. */
  public static final int NUMPREGUNTAS_PESO1_GRADO2 = 4;
  
  /** The Constant NUMPREGUNTAS_PESO2_GRADO3. */
  public static final int NUMPREGUNTAS_PESO2_GRADO3 = 3;
  
  /** The Constant SUMAPREGUNTAS_GRADO3. */
  public static final int SUMAPREGUNTAS_GRADO3 = 5;
  
  /** The Constant SUMAPREGUNTAS_GRADO2. */
  public static final int SUMAPREGUNTAS_GRADO2 = 2;
  
  /** The Constant MAX_DECIMALES_PUNTUACION. */
  public static final int MAX_DECIMALES_PUNTUACION = 2;
  
  /** The Constant LEY_EVE_2007_ID. */
  public static final String LEY_EVE_2007_ID = "3";
  
  /** The Constant LEY_EVE_2011_ID. */
  public static final String LEY_EVE_2011_ID = "4";
  
  /** The Constant BAREMO_GENERAL. */
  public static final String BAREMO_GENERAL = "GN";
  
  /** The Constant BAREMO_ESPECIFICO. */
  public static final String BAREMO_ESPECIFICO = "SP";


  /**
   * Instantiates a new constantes valoraciones.
   */
  private ConstantesValoraciones() {
    // no necesario
  }
}
