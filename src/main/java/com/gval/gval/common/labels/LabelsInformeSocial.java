package com.gval.gval.common.labels;



/**
 * The Class LabelsInformeSocial.
 */
public class LabelsInformeSocial {

  /** The Constant LABEL_INFORMESOCIAL_ASOCIAR_CNP_PREFERENCIAS. */
  public static final String LABEL_INFORMESOCIAL_ASOCIAR_CNP_PREFERENCIAS =
      "label.informesocial.asociar-cnp-preferencias";

  /** The Constant LABEL_INFORMESOCIAL_ASOCIAR_AP_PREFERENCIAS. */
  public static final String LABEL_INFORMESOCIAL_ASOCIAR_AP_PREFERENCIAS =
      "label.informesocial.asociar-ap-preferencias";

  /** The Constant LABEL_INFORMESOCIAL_ASOCIAR_CNP_IS. */
  public static final String LABEL_INFORMESOCIAL_ASOCIAR_CNP_IS =
      "label.informesocial.asociar-cnp-is";

  /** The Constant LABEL_INFORMESOCIAL_ASOCIAR_AP_IS. */
  public static final String LABEL_INFORMESOCIAL_ASOCIAR_AP_IS =
      "label.informesocial.asociar-ap-is";

  /** The Constant ERROR_INFORME_SOCIAL_CAMBIO_CUIDADOR_SIN_CUIDADOR. */
  public static final String ERROR_INFORME_SOCIAL_CAMBIO_CUIDADOR_SIN_CUIDADOR =
      "error.informe-social.cambio-cuidador.sin-cuidador";

  /** The Constant ERROR_INFORME_SOCIAL_CERRAR_SIN_GUARDAR. */
  public static final String ERROR_INFORME_SOCIAL_CERRAR_SIN_GUARDAR =
      "error.informe-social.cerrar-sin-guardar";

  /** The Constant ERROR_INFORME_SOCIAL_SIN_USUARIO_ASIGNADO. */
  public static final String ERROR_INFORME_SOCIAL_SIN_USUARIO_ASIGNADO =
      "error.informe-social.usuario-no-asignado";

  /** The Constant ERROR_INFORME_SOCIAL_COINCIDE_PREFERENCIAS_NULO. */
  public static final String ERROR_INFORME_SOCIAL_COINCIDE_PREFERENCIAS_NULO =
      "error.informe-social.coincide-preferencias-nulo";

  /** The Constant ERROR_INFORME_SOCIAL_CONIVENCIA_NO_INDICADA. */
  public static final String ERROR_INFORME_SOCIAL_CONIVENCIA_NO_INDICADA =
      "error.informe-social.conivencia-no-indicada";

  /** The Constant ERROR_INFORME_SOCIAL_PREFERENCIAS_NO_INDICADAS. */
  public static final String ERROR_INFORME_SOCIAL_PREFERENCIAS_NO_INDICADAS =
      "error.informe-social.sin-preferencias";

  /** The Constant ERROR_INFORME_SOCIAL_CREACION_MOTIVO_GENERACION_NULO. */
  public static final String ERROR_INFORME_SOCIAL_CREACION_MOTIVO_GENERACION_NULO =
      "error.informe-social.creacion.motivo-generacion-nulo";

  /** The Constant ERROR_INFORME_SOCIAL_CREACION_SOLICITUD_NULA. */
  public static final String ERROR_INFORME_SOCIAL_CREACION_SOLICITUD_NULA =
      "error.informe-social.creacion.solicitud-nula";

  /** The Constant ERROR_INFORME_SOCIAL_CUIDADOR_SIN_IDONEIDAD. */
  public static final String ERROR_INFORME_SOCIAL_CUIDADOR_SIN_IDONEIDAD =
      "error.informe-social.cuidador-sin-idoneidad";

  /** The Constant ERROR_INFORME_SOCIAL_DIRECCION_NULA. */
  public static final String ERROR_INFORME_SOCIAL_DIRECCION_NULA =
      "error.informe-social.direccion-nula";

  /** The Constant ERROR_INFORME_SOCIAL_ESTADO_CERRADA. */
  public static final String ERROR_INFORME_SOCIAL_ESTADO_CERRADA =
      "error.informe-social.estado-cerrada";

  /** The Constant ERROR_INFORME_SOCIAL_ESTADO_CERRADO. */
  public static final String ERROR_INFORME_SOCIAL_ESTADO_CERRADO =
      "error.informe-social.no-estado-cerrado";

  /** The Constant ERROR_INFORME_SOCIAL_INFORMACION_ASISTENTE_VACIA. */
  public static final String ERROR_INFORME_SOCIAL_INFORMACION_ASISTENTE_VACIA =
      "error.informe-social.informacion-asistente-vacia";

  /** The Constant ERROR_INFORME_SOCIAL_INIDICACION_COINCIDE_VALORACION. */
  public static final String ERROR_INFORME_SOCIAL_INIDICACION_COINCIDE_VALORACION =
      "error.informe-social.inidicacion-coincide-valoracion";

  /** The Constant ERROR_INFORME_SOCIAL_NO_ESTADO_EN_CURSO. */
  public static final String ERROR_INFORME_SOCIAL_NO_ESTADO_EN_CURSO =
      "error.informe-social.no-estado-en-curso";

  /** The Constant ERROR_INFORME_SOCIAL_NO_SELECCIONADO. */
  public static final String ERROR_INFORME_SOCIAL_NO_SELECCIONADO =
      "error.informe-social.no-seleccionado";

  /** The Constant ERROR_INFORME_SOCIAL_PERSONA_NULA. */
  public static final String ERROR_INFORME_SOCIAL_PERSONA_NULA =
      "error.informe-social.persona-nula";

  /** The Constant ERROR_INFORME_SOCIAL_PREFERENCIA_NO_APORTADAS. */
  public static final String ERROR_INFORME_SOCIAL_PREFERENCIA_NO_APORTADAS =
      "error.informe-social.preferencia-no-aportadas";

  /** The Constant ERROR_INFORME_SOCIAL_PREFERENCIAS_IGUALES. */
  public static final String ERROR_INFORME_SOCIAL_PREFERENCIAS_IGUALES =
      "error.informe-social.preferencias-iguales";

  /** The Constant ERROR_INFORME_SOCIAL_PREFERENCIAS_NULAS. */
  public static final String ERROR_INFORME_SOCIAL_PREFERENCIAS_NULAS =
      "error.informe-social.preferencias-nulas";

  /** The Constant ERROR_INFORME_SOCIAL_PREFERENCIAS_SOLICITUD_NULAS. */
  public static final String ERROR_INFORME_SOCIAL_PREFERENCIAS_SOLICITUD_NULAS =
      "error.informe-social.preferencias-solicitud-nulas";

  /** The Constant ERROR_INFORME_SOCIAL_SEGUIMIENTO_NO_IDONEIDAD. */
  public static final String ERROR_INFORME_SOCIAL_SEGUIMIENTO_NO_IDONEIDAD =
      "error.informe-social.seguimiento.no-idoneidad";

  /** The Constant ERROR_INFORME_SOCIAL_SEGUIMIENTO_SIN_IDONEIDAD. */
  public static final String ERROR_INFORME_SOCIAL_SEGUIMIENTO_SIN_IDONEIDAD =
      "error.informe-social.seguimiento.sin-idoneidad";

  /** The Constant ERROR_INFORME_SOCIAL_SEGUIMIENTO_SIN_CNP_NO_IDONEO. */
  public static final String ERROR_INFORME_SOCIAL_SEGUIMIENTO_SIN_CNP_NO_IDONEO =
      "error.informe-social.seguimiento.cnp.no-idoneos-marcados";

  /** The Constant ERROR_INFORME_SOCIAL_SIN_CUIDADORES. */
  public static final String ERROR_INFORME_SOCIAL_SIN_CUIDADORES =
      "error.informe-social.sin-cuidadores";
  /** The Constant ERROR_INFORME_SOCIAL_SIN_CUIDADORES. */
  public static final String ERROR_INFORME_SOCIAL_SIN_CUIDADORES_ASOCIADOS =
      "error.informe-social.sin-cuidadores-asociados";

  /** The Constant ERROR_INFORME_SOCIAL_CENTRO_TRABAJADOR_SOCIAL_VACIO. */
  public static final String ERROR_INFORME_SOCIAL_NUMERO_COLEGIADO_VACIO =
      "error.informesocial.notnull.numero-colegiado";

  /** The Constant ERROR_INFORME_SOCIAL_CENTRO_TRABAJADOR_SOCIAL_VACIO. */
  public static final String ERROR_INFORME_SOCIAL_CENTRO_TRABAJADOR_SOCIAL_VACIO =
      "error.informesocial.notnull.centro-trabajador-social";

  /** The Constant ERROR_USUARIO_NO_ASIGNADO_O_ADMIN. */
  public static final String ERROR_USUARIO_NO_ASIGNADO_O_ADMIN =
      "error.usuario.no-asignado-o-admin";

  /** The Constant ERROR_USUARIO_NO_ROL_PARA_ACCION. */
  public static final String ERROR_USUARIO_NO_ROL_PARA_ACCION =
      "error.usuario.no-rol-para-accion";

  /** The Constant ERROR_RECURSO_ADECUADO_SIN_JUSTIFICAR. */
  public static final String ERROR_RECURSO_ADECUADO_SIN_JUSTIFICAR =
      "error.informesocial.recurso-adecuado-sin-justificar";

  /** The Constant ERROR_INFORME_SOCIAL_CUIDADORES_DUPLICADOS. */
  public static final String ERROR_INFORME_SOCIAL_CUIDADORES_DUPLICADOS =
      "error.informe-social.cuidadores-valorador-duplicados";

  /** The Constant LABEL_ALERTA. */
  public static final String LABEL_ALERTA = "label.alerta";

  /** The Constant LABEL_INFORME_SOCIAL_ACCION_CERRAR. */
  public static final String LABEL_INFORME_SOCIAL_ACCION_CERRAR =
      "label.informe-social.accion-cerrar";

  /** The Constant LABEL_INFORME_SOCIAL_ALEGACIONES. */
  public static final String LABEL_INFORME_SOCIAL_ALEGACIONES =
      "label.informe_social.alegaciones";

  /** The Constant LABEL_INFORME_SOCIAL_ANTECEDENTES. */
  public static final String LABEL_INFORME_SOCIAL_ANTECEDENTES =
      "label.informe_social.antecedentes";

  /** The Constant LABEL_INFORME_SOCIAL_ASOCIAR_INFORME_ACTIVO. */
  public static final String LABEL_INFORME_SOCIAL_ASOCIAR_INFORME_ACTIVO =
      "label.informe_social.asociar_informe_activo";

  /** The Constant LABEL_INFORME_SOCIAL_CAMBIO_CUIDADOR. */
  public static final String LABEL_INFORME_SOCIAL_CAMBIO_CUIDADOR =
      "label.informe_social.cambio_cuidador";

  /** The Constant LABEL_INFORME_SOCIAL_DOCUMENTO_ASOCIADO. */
  public static final String LABEL_INFORME_SOCIAL_DOCUMENTO_ASOCIADO =
      "label.informe_social.documento_asociado";

  /** The Constant LABEL_INFORME_SOCIAL_PETICION_NUEVAS_PREFERENCIAS. */
  public static final String LABEL_INFORME_SOCIAL_PETICION_NUEVAS_PREFERENCIAS =
      "label.informe_social.peticion_nuevas_preferencias";

  /** The Constant LABEL_INFORME_SOCIAL_SITUACION_CONVIVENCIA. */
  public static final String LABEL_INFORME_SOCIAL_SITUACION_CONVIVENCIA =
      "label.informe_social.situacion_convivencia";

  /** The Constant LABEL_INFORME_SOCIAL_SITUACION_DEPENDENCIA. */
  public static final String LABEL_INFORME_SOCIAL_SITUACION_DEPENDENCIA =
      "label.informe_social.situacion_dependencia";

  /** The Constant LABEL_INFORME_SOCIAL_SOLICITANTE. */
  public static final String LABEL_INFORME_SOCIAL_SOLICITANTE =
      "label.informe_social.solicitante";

  /** The Constant LABEL_INFORME_SOCIAL_VALORACION_PROFESIONAL. */
  public static final String LABEL_INFORME_SOCIAL_VALORACION_PROFESIONAL =
      "label.informe_social.valoracion_profesional";

  /** The Constant LABEL_INFORMESOCIAL_ERROR_MOTIVO_ABIERTO. */
  public static final String LABEL_INFORMESOCIAL_ERROR_MOTIVO_ABIERTO =
      "label.informesocial.error.motivo-abierto";

  /** The Constant LABEL_INFORMESOCIAL_ERROR_NOENCONTRADO. */
  public static final String LABEL_INFORMESOCIAL_ERROR_NOENCONTRADO =
      "label.informesocial.error.noencontrado";

  /** The Constant LABEL_USUARIO_SIN_USUARIO_ASIGNADO. */
  public static final String LABEL_USUARIO_SIN_USUARIO_ASIGNADO =
      "label.usuario.sin-usuario-asignado";
  /** The Constant LABEL_USUARIO_SIN_USUARIO_ASIGNADO. */
  public static final String LABEL_USUARIO_DISTINTO_ASIGNADO =
      "label.usuario.usuario-distinto-asignado";

  /** The Constant MOTIVOS_GENERACION_INFORME_SOCIAL. */
  public static final String MOTIVOS_GENERACION_INFORME_SOCIAL =
      "motivosGeneracionInformeSocial";

  /** The Constant PREFERENCIAS_INFORME_SOCIAL. */
  public static final String PREFERENCIAS_INFORME_SOCIAL =
      "preferenciasInformeSocial";

  /** The Constant BORRAR_CUIDADORES_PREFERENCIAS_AL_GUARDAR. */
  public static final String BORRAR_CUIDADORES_PREFERENCIAS_AL_GUARDAR =
      "borrarCuidadoresPreferenciasAlGuardar";

  /** The Constant CUIDADORES_INFORME_SOCIAL. */
  public static final String CUIDADORES_INFORME_SOCIAL =
      "cuidadoresInformeSocial";

  /** The Constant ASISTENTES_INFORME_SOCIAL. */
  public static final String ASISTENTES_INFORME_SOCIAL =
      "asistentesInformeSocial";

  /** The Constant MARCHA_ATRAS_OK. */
  public static final String MARCHA_ATRAS_OK =
      "label.informesocial.marcha_atras_ok";

  /** The Constant LABEL_SIN_MODALIDAD. */
  public static final String LABEL_SIN_MODALIDAD =
      "error.informe-social.sin-modalidad-teleasistencia";

  /** The Constant LABEL_SIN_MOTIVO_MOVIL. */
  public static final String LABEL_SIN_MOTIVO_MOVIL =
      "error.informe-social.sin-motivo-modalidad-movil";

  /** The Constant LABEL_SIN_RECURSO. */
  public static final String LABEL_SIN_RECURSO =
      "error.informe-social.indicar-recurso-idoneo";

  /** The Constant LABEL_SIN_MOTIVO_IDONEIDAD. */
  public static final String LABEL_SIN_MOTIVO_IDONEIDAD =
      "error.informe-social.sin-motivo-idoneidad";

  /** The Constant ERROR_INFORME_SOCIAL_REGISTRO_NO_SELECCIONADO. */
  public static final String ERROR_INFORME_SOCIAL_REGISTRO_NO_SELECCIONADO =
      "error.listado.validacion.noseleccion";

  /** The Constant ERROR_IS_SOLICANTE_EDAD_ESCOLAR. */
  public static final String ERROR_IS_SOLICANTE_EDAD_ESCOLAR =
      "error.informe-social.solicitante.edad-escolar";

  /** The Constant ERROR_IS_SOLICANTE_TIPO_CENTRO. */
  public static final String ERROR_IS_SOLICANTE_TIPO_CENTRO =
      "error.informe-social.solicitante.tipo-centro";

  /** The Constant ERROR_IS_ANAMNESIS . */
  public static final String ERROR_IS_ANAMNESIS =
      "error.informe-social.anamesis";

  /** The Constant ERROR_IS_PERSONA_SITUACION. */
  public static final String ERROR_IS_PERSONA_SITUACION =
      "error.informe-social.persona-situacion-convivencia";

  /** The Constant ERROR_IT_EXPOSICION_HECHOS. */
  public static final String ERROR_IT_EXPOSICION_HECHOS =
      "error.informe-social.exposicion-hechos";

  /** The Constant ERROR_INFORME_SOCIAL_CONIVENCIA_MODALIDAD. */
  public static final String ERROR_INFORME_SOCIAL_CONIVENCIA_MODALIDAD =
      "error.informe-social.persona-situacion-convivencia-modalidad";

  /** The Constant ERROR_INFORME_SOCIAL_CONIVENCIA_FRAGILIDAD. */
  public static final String ERROR_INFORME_SOCIAL_CONIVENCIA_FRAGILIDAD =
      "error.informe-social.persona-situacion-convivencia-fragilidad";

  /** The Constant ERROR_INFORME_SOCIAL_CONIVENCIA_NIVEL_RELACION . */
  public static final String ERROR_INFORME_SOCIAL_CONIVENCIA_NIVEL_RELACION =
      "error.informe-social.persona-situacion-convivencia-nivel-relacion";

  /** The Constant ERROR_INFORME_SOCIAL_CONIVENCIA_TIENE_VIVIENDA . */
  public static final String ERROR_INFORME_SOCIAL_CONIVENCIA_TIENE_VIVIENDA =
      "error.informe-social.persona-situacion-convivencia-tiene-vivienda";

  /** The Constant ERROR_INFORME_SOCIAL_CONIVENCIA_CONSERVACION . */
  public static final String ERROR_INFORME_SOCIAL_CONIVENCIA_CONSERVACION =
      "error.informe-social.persona-situacion-convivencia-conservacion";

  /** The Constant ERROR_INFORME_SOCIAL_CONIVENCIA_HABITABILIDAD . */
  public static final String ERROR_INFORME_SOCIAL_CONIVENCIA_HABITABILIDAD =
      "error.informe-social.persona-situacion-convivencia-habitabilidad";

  /** The Constant ERROR_INFORME_SOCIAL_CONIVENCIA_SALUBRIDAD . */
  public static final String ERROR_INFORME_SOCIAL_CONIVENCIA_SALUBRIDAD =
      "error.informe-social.persona-situacion-convivencia-salubridad";

  /** The Constant ERROR_INFORME_SOCIAL_CONIVENCIA_ACCESO . */
  public static final String ERROR_INFORME_SOCIAL_CONIVENCIA_ACCESO =
      "error.informe-social.persona-situacion-convivencia-acceso";

  /** The Constant ERROR_INFORME_SOCIAL_CONIVENCIA_ACCESO_DIFICULTADES . */
  public static final String ERROR_INFORME_SOCIAL_CONIVENCIA_ACCESO_DIFICULTADES =
      "error.informe-social.persona-situacion-convivencia-acceso-dificultades";

  /** The Constant ERROR_INFORME_SOCIAL_CONIVENCIA_ADAPTACIONES_EXISTENTES . */
  public static final String ERROR_INFORME_SOCIAL_CONIVENCIA_ADAPTACIONES_EXISTENTES =
      "error.informe-social.persona-situacion-convivencia-adaptaciones-existentes";

  /** The Constant ERROR_INFORME_SOCIAL_CONIVENCIA_ADAPTACIONES_NECESARIAS . */
  public static final String ERROR_INFORME_SOCIAL_CONIVENCIA_ADAPTACIONES_NECESARIAS =
      "error.informe-social.persona-situacion-convivencia-adaptaciones-necesarias";

  /** The Constant LABEL_INFORME_SOCIAL_PERSONA_SITUACION . */
  public static final String LABEL_INFORME_SOCIAL_PERSONA_SITUACION =
      "label.informe_social.persona_situacion_convivencia";

  /** The Constant ERROR_INFORME_SOCIAL_CONIVENCIA_VIVE_LIST. */
  public static final String ERROR_INFORME_SOCIAL_CONIVENCIA_VIVE_LIST =
      "error.informe-social.conivencia-listado-vive-empty";

  /** The Constant ERROR_INFORME_SOCIAL_CONIVENCIA_ALTERNA_LIST. */
  public static final String ERROR_INFORME_SOCIAL_CONIVENCIA_ALTERNA_LIST =
      "error.informe-social.conivencia-listado-alt-empty";

  /** The Constant ERROR_IS_PRESCRIPCION_INF_AUTO_INTERNA. */
  public static final String ERROR_IS_PRESCRIPCION_INF_AUTO_INTERNA =
      "error.informe-social.prescripcion-auto-internamiento";

  /** The Constant ERROR_IS_PRESCRIPCION_INF_FISCALIA. */
  public static final String ERROR_IS_PRESCRIPCION_INF_FISCALIA =
      "error.informe-social.prescripcion-inf-fiscalia";

  /** The Constant ERROR_IS_PRESCRIPCION_INF_VOLUNTARIA. */
  public static final String ERROR_IS_PRESCRIPCION_INF_VOLUNTARIA =
      "error.informe-social.prescripcion-inf-voluntaria";

  /** The Constant ERROR_IS_PRESCRIPCION_SELECT_RECURSO. */
  public static final String ERROR_IS_PRESCRIPCION_SELECT_RECURSO =
      "error.informe-social.prescripcion-seleccionar-recurso";

  /** The Constant ERROR_IS_PRESCRIPCION_SELECT_RECURSO_OTROS. */
  public static final String ERROR_IS_PRESCRIPCION_SELECT_RECURSO_OTROS =
      "error.informe-social.prescripcion-seleccionar-recurso-otros";

  /** The Constant ERROR_IS_PRESCRIPCION_CATALOGO. */
  public static final String ERROR_IS_PRESCRIPCION_CATALOGO =
      "error.informe-social.prescripcion-catalogo-informado";

  /** The Constant ERROR_IS_PRESCRIPCION_INF_CONSIDERA_IDONEO. */
  public static final String ERROR_IS_PRESCRIPCION_INF_CONSIDERA_IDONEO =
      "error.informe-social.prescripcion-considera";

  /** The Constant ERROR_IS_PRESCRIPCION_INF_NO_IDONEO. */
  public static final String ERROR_IS_PRESCRIPCION_INF_NO_IDONEO =
      "error.informe-social.prescripcion-no-idonea";

  /** The Constant LABEL_INFORME_EXPOSICION_HECHOS. */
  public static final String LABEL_INFORME_EXPOSICION_HECHOS =
      "label.informe_social.exposicion-hechos";

  /** The Constant ERROR_IT_OTROS_PROPUESTA. */
  public static final String ERROR_IT_OTROS_PROPUESTA =
      "error.informe-tecnico-otros.propuesta";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PATI_PVI. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PATI_PVI =
      "error.informe-tecnico-ap-pati.pvi";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PATI_ADECUADO. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PATI_ADECUADO =
      "error.informe-tecnico-ap-pati.adecuedo";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PATI_INCUMPLIMIENTO. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PATI_INCUMPLIMIENTO =
      "error.informe-tecnico-ap-pati.incumplimiento";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PATI_INCUMPLIMIENTO_OTROS. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PATI_INCUMPLIMIENTO_OTROS =
      "error.informe-tecnico-ap-pati.incumplimiento-otros";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PAP_ACTIVIDADES. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PAP_ACTIVIDADES =
      "error.informe-tecnico-ap-pap.ayuda-actividades";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PAP_DESPLAZARSE. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PAP_DESPLAZARSE =
      "error.informe-tecnico-ap-pap.ayuda-desplazarse";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PAP_COMUNICACION. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PAP_COMUNICACION =
      "error.informe-tecnico-ap-pap.ayuda-comunicacion";

  /** The Constant LABEL_PERSONAS_CUIDADORAS. */
  public static final String LABEL_PERSONAS_CUIDADORAS =
      "label.informesocial.valoracion_profesional.cuidados.persona_cuidadora";

  /** The Constant ERROR_CUIDADOR_VERIFICADO_EMPTY. */
  public static final String ERROR_CUIDADOR_VERIFICADO_EMPTY =
      "error.listado-cuidador.verificado-empty";

  /** The Constant ERROR_CUIDADOR_IDONEO_EMPTY. */
  public static final String ERROR_CUIDADOR_IDONEO_EMPTY =
      "error.listado-cuidador.idoneo-empty";

  /** The Constant ERROR_CUIDADOR_IDONEO_MOTIVO. */
  public static final String ERROR_CUIDADOR_IDONEO_MOTIVO =
      "error.listado-cuidador.idoneo-motivo";

  /** The Constant ERROR_MOTIVO_NO_IDONEIDAD. */
  public static final String ERROR_MOTIVO_NO_IDONEIDAD =
      "error.motivo-no-idoneidad";

  /** The Constant ERROR_CUIDADOR_NO_IDONEO. */
  public static final String ERROR_CUIDADOR_NO_IDONEO =
      "error.listado-cuidador.idoneo-all-false";

  /** The Constant ERROR_ASISTENTE_CUMPLE_REQUISITOS. */
  public static final String ERROR_ASISTENTE_CUMPLE_REQUISITOS =
      "error.asistentes-personal.propuestas";

  /** The Constant ERROR_ASISTENTE_CUMPLE_PVI. */
  public static final String ERROR_ASISTENTE_CUMPLE_PVI =
      "error.asistentes-personal.cuple.pvi";

  /** The Constant LABEL_ASISTENTES_PERSONALES. */
  public static final String LABEL_ASISTENTES_PERSONALES =
      "label.asistentes-personal.propuestas";

  /** The Constant ERROR_IS_AP_PATI_TIPO_PERSONA. */
  public static final String ERROR_IS_AP_PATI_TIPO_PERSONA =
      "error.informe-ap-pati.tipo-persona";

  /** The Constant ERROR_IS_AP_PATI_OTRO_RECURSO_MOTIVO. */
  public static final String ERROR_IS_AP_PATI_OTRO_RECURSO_MOTIVO =
      "error.informe-ap-pati.otro-recurso-motivo";

  /** The Constant ERROR_IS_AP_PATI_OTRO_RECURSO_SELECT. */
  public static final String ERROR_IS_AP_PATI_OTRO_RECURSO_SELECT =
      "error.informe-ap-pati.otro-recurso-no-seleccionado";

  /** The Constant ERROR_IS_AP_PATI_OTRO_RECURSO. */
  public static final String ERROR_IS_AP_PATI_OTRO_RECURSO =
      "error.informe-ap-pati.otro-recurso";

  /** The Constant ERROR_IS_AP_PATI_HORARIO_LECTIVO. */
  public static final String ERROR_IS_AP_PATI_HORARIO_LECTIVO =
      "error.informe-ap-pati.horario-lectivo";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PAP_FORMACION. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PAP_FORMACION =
      "error.informe-ap-pap.realiza-formacion";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PAP_FORMACION_NOMBRE. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PAP_FORMACION_NOMBRE =
      "error.informe-ap-pap.realiza-formacion-nombre";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PAP_FORMACION_AYUDA. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PAP_FORMACION_AYUDA =
      "error.informe-ap-pap.realiza-formacion-ayuda";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PAP_ACTIVO. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PAP_ACTIVO =
      "error.informe-ap-pap.trabaja";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PAP_ACTIVO_NOMBRE. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PAP_ACTIVO_NOMBRE =
      "error.informe-ap-pap.trabaja-nombre";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PAP_ACTIVO_AYUDA. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PAP_ACTIVO_AYUDA =
      "error.informe-ap-pap.trabaja-ayuda";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PAP_ACTIVIDAD. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PAP_ACTIVIDAD =
      "error.informe-ap-pap.actividad";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PAP_ACTIVIDAD_NOMBRE. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PAP_ACTIVIDAD_NOMBRE =
      "error.informe-ap-pap.actividad-nombre";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PAP_ACTIVIDAD_ENTIDAD. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PAP_ACTIVIDAD_ENTIDAD =
      "error.informe-ap-pap.actividad-entidad";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PAP_ADECUADO. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PAP_ADECUADO =
      "error.informe-ap-pap.adecuado";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PAP_OTRO_RECURSO. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PAP_OTRO_RECURSO =
      "error.informe-ap-pap.otro-recurso";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PAP_OTRO_MOTIVO. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PAP_OTRO_MOTIVO =
      "error.informe-ap-pap.otro-motivo";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PAP_RECURSO_ALT. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PAP_RECURSO_ALT =
      "error.informe-ap-pap.recurso-alternativo";

  /** The Constant ERROR_IS_SEGUIMIENTO_AP_PAP_ACTIVIDAD_AYUDA. */
  public static final String ERROR_IS_SEGUIMIENTO_AP_PAP_ACTIVIDAD_AYUDA =
      "error.informe-ap-pap.actividad-ayuda";

  /** The Constant ERROR_ASISTENTE_LISTADO_VACIO. */
  public static final String ERROR_ASISTENTE_LISTADO_VACIO =
      "error.informe-ap.listado-vacio";

  /** The Constant ERROR_CUIDADOR_EMPTY. */
  public static final String ERROR_CUIDADOR_EMPTY =
      "error.informe-cnp.listado-vacio";

  /** The Constant ERROR_VALORACION_RECURSO_SAD. */
  public static final String ERROR_VALORACION_RECURSO_SAD =
      "error.informe.recurso-sad";

  /** The Constant ERROR_VALORACION_RECURSO_CENTRO_DIA. */
  public static final String ERROR_VALORACION_RECURSO_CENTRO_DIA =
      "error.informe-recurso-centro-dia";

  /** The Constant ERROR_IS_PRESCRIPCION_CNP_CONTINUAR_PRESTACION. */
  public static final String ERROR_IS_PRESCRIPCION_CNP_CONTINUAR_PRESTACION =
      "error.informe-cnp-continuar-prestacion";

  /** The Constant ERROR_IS_PRESCRIPCION_TIPO_PREFERENCIA. */
  public static final String ERROR_IS_PRESCRIPCION_TIPO_PREFERENCIA =
      "error.informe-recurso-centro-dia";

  /** The Constant ERROR_IS_PRESCRIPCION_AUMENTO_HORAS. */
  public static final String ERROR_IS_PRESCRIPCION_AUMENTO_HORAS =
      "error.informe.recurso-sad";

  /** The Constant ERROR_IS_REVISION_HORAS. */
  public static final String ERROR_IS_REVISION_HORAS =
      "error.informe.revision-horas";

  /**
   * Instantiates a new labels informe social.
   */
  private LabelsInformeSocial() {
    // Empty constructor
  }
}
