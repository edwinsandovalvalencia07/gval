package com.gval.gval.common.labels;


/**
 * The Class LabelsCuidador.
 */
public class LabelsCuidador {

  /** The Constant ERROR_CUIDADOR_NUMERO_MAXIMO_CUIDADORES. */
  public static final String ERROR_CUIDADOR_NUMERO_MAXIMO_CUIDADORES =
      "error.cuidador.numero-maximo-cuidadores";

  /** The Constant ERROR_CUIDADOR_ELIMINAR_CUIDADOR_NO_SELECCIONADO. */
  public static final String ERROR_CUIDADOR_ELIMINAR_CUIDADOR_NO_SELECCIONADO =
      "error.cuidador.eliminar-cuidador-no-seleccionado";

  /** The Constant ERROR_ELIMINAR_CUIDADOR_FECHA_DOCU. */
  public static final String ERROR_ELIMINAR_CUIDADOR_FECHA_DOCU =
      "error.catalogoservicio.no-eliminar-cuidador-fecha-docu";

  /** The Constant ERROR_ASOCIAR_CUIDADOR_INFORME_MAXIMO. */
  public static final String ERROR_ASOCIAR_CUIDADOR_INFORME_MAXIMO =
      "error.cuidador.numero-maximo-asociados";

  /**
   * Instantiates a new labels cuidador.
   */
  private LabelsCuidador() {
    // Empty constructor
  }
}
