package com.gval.gval.common.labels;


/**
 * The Class LabelsCommon.
 */
public class LabelsCommon {

  /** The Constant ALERTA. */
  public static final String ALERTA = "label.alerta";

  /** The Constant LABEL_BOTON_CONFIRMAR_BORRADO. */
  public static final String LABEL_BOTON_CONFIRMAR_BORRADO =
      "label.boton.confirmar_borrado";

  /** The Constant LABEL_ERROR. */
  public static final String LABEL_ERROR = "label.error";

  /** The Constant LABEL_SI. */
  public static final String LABEL_SI = "label.si";

  /** The Constant LABEL_NO. */
  public static final String LABEL_NO = "label.no";

  /**
   * Instantiates a new labels common.
   */
  private LabelsCommon() {
    // Empty constructor
  }
}
