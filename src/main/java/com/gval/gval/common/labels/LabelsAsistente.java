package com.gval.gval.common.labels;


/**
 * The Class LabelsAsistente.
 */
public class LabelsAsistente {

  /** The Constant ERROR_ASISTENTE_NUMERO_MAXIMO_ASISTENTES. */
  public static final String ERROR_ASISTENTE_NUMERO_MAXIMO_ASISTENTES =
      "error.asistente.numero-maximo-asistentes";

  /** The Constant ERROR_ASISTENTE_ELIMINAR_ASISTENTE_NO_SELECCIONADO. */
  public static final String ERROR_ASISTENTE_ELIMINAR_ASISTENTE_NO_SELECCIONADO =
      "error.asistente.eliminar-asistente-no-seleccionado";

  /**
   * Instantiates a new labels asistente.
   */
  private LabelsAsistente() {
    // Empty constructor
  }

}
