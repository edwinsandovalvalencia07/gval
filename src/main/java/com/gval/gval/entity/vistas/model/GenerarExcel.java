package com.gval.gval.entity.vistas.model;

import java.io.Serializable;

import jakarta.persistence.*;


// TODO: Auto-generated Javadoc
/**
 * The Class GenerarExcel.
 */
@Entity
@Table(name = "SDV_LD_GENERAR_EXCEL_S")
public class GenerarExcel implements Serializable{

		/** The Constant serialVersionUID. */
		private static final long serialVersionUID = 1L;

        /** The pk resoluci. */
		@EmbeddedId
		private MiClaveCompuesta claveCompuesta;

//		@Column(name = "FILA")
//		private Integer fila;
		// @Id
//		@Column(name = "COMPUESTA")
//		private String pkCompuesta;
        /** The pk resoluci. */
        @Column(name = "PK_RESOLUCI") // Ajusta el nombre de la columna según tu esquema de base de datos
	    private Long pkResoluci;

	    /** The nombre. */
    	@Column(name = "PENOMBRE")
	    private String nombre;

	    /** The primer ape. */
    	@Column(name = "PEPRIMAPEL")
	    private String primerApe;

	    /** The segundo ape. */
    	@Column(name = "PESEGUAPEL")
	    private String segundoApe;

	    /** The residencia. */
    	@Column(name = "ASRESILEGA")
	    private String residencia;

	    /** The direccion. */
    	@Column(name = "TINOMBCORT")
	    private String direccion;

	    /** The direccion 2. */
    	@Column(name = "DINOMBVIA")
	    private String direccion2;

		/** The dinumero. */
		@Column(name = "DINUMERO")
		private String dinumero;

		/** The dipiso. */
		@Column(name = "DIPISO")
		private String dipiso;

		/** The dipuerta. */
		@Column(name = "DIPUERTA")
		private String dipuerta;

		/** The di escalera. */
		@Column(name = "DIESCALERA")
		private String diEscalera;

		/** The di bloque. */
		@Column(name = "DIBLOQUE")
		private String diBloque;

		/** The di portal. */
		@Column(name = "DIPORTAL")
		private String diPortal;
	    /** The cp. */
    	@Column(name = "COCP")
	    private String cp;

	    /** The localidad. */
    	@Column(name = "PONOMBRE")
	    private String localidad;

	    /** The provincia. */
    	@Column(name = "PRNOMBRE")
	    private String provincia;

	    /** The pais. */
    	@Column(name = "NANOMBRE")
	    private String pais;

	    /** The num solicitud. */
//    	@Column(name = "SOLICITUD")
//	    private String numSolicitud;

	    /** The grado. */
    	@Column(name = "REGRADO")
	    private String grado;

	    /** The nivel. */
    	@Column(name = "RENIVEL")
	    private String nivel;

	    /** The dni. */
    	@Column(name = "PENUDOCIDE")
	    private String dni;

	    /** The zona cobe. */
    	@Column(name = "ZODESC")
	    private String zonaCobe;

	    /** The tipo prestacion. */
    	@Column(name = "TIPO_PRESTACION")
	    private String tipoPrestacion;

	    /** The num listado. */
    	@Column(name = "NUM_LISTADO")
	    private String numListado;

	    /** The modalidad. */
    	@Column(name = "MODALIDAD_TELEASISTENCIA")
	    private String modalidad;

	    /** The fecha resolucion. */
    	@Column(name = "REFECHRESO")
	    private String fechaResolucion;

	    /** The tipo documento. */
//    	@Column(name = "TINOMBRE")
//	    private String tipoDocumento;

	    /** The fecha notificacion. */
    	@Column(name = "REFECHNOTI")
	    private String fechaNotificacion;

	    /** The fallecido. */
    	@Column(name = "FALLECIDO")
	    private String fallecido;

	    /** The telefono fijo. */
    	@Column(name = "DITLFNFIJO")
	    private String telefonoFijo;

	    /** The telefono movil. */
    	@Column(name = "DIMOVIL")
	    private String telefonoMovil;

    	/** The telefono movil. */
//    	@Column(name = "PK_TIPODOCU")
//	    private Long pkTipoDocu;
    	@Column(name = "PK_SOLICIT")
    	private Long pkSolicitud;

	    /** The nombre provincia. */
	    @Column(name = "PROVINCIA")
    	private String nombreProvincia;

		/** The nombre H. */
		@Column(name = "PENOMBREH")
		private String nombreH;

		/** The primer ape. */
		@Column(name = "PEPRIMAPELH")
		private String primerApeH;

		/** The segundo ape. */
		@Column(name = "PESEGUAPELH")
		private String segundoApeH;

		/** The dni H. */
		@Column(name = "PENUDOCIDEH")
		private String dniH;

		/** The direccion H. */
		@Column(name = "TINOMBCORTH")
		private String direccionH;

		/** The direccion 2. */
		@Column(name = "DINOMBVIAH")
		private String direccion2H;

		/** The localidad H. */
		@Column(name = "PONOMBREH")
		private String localidadH;

		/** The dinumero H. */
		@Column(name = "DINUMEROH")
		private String dinumeroH;

		/** The dipiso. */
		@Column(name = "DIPISOH")
		private String dipisoH;

		/** The dipuerta. */
		@Column(name = "DIPUERTAH")
		private String dipuertaH;

		/** The row number. */
		@Column(name = "ROWNUMBER")
		private Integer rowNumber;

		/** The di escalera H. */
		@Column(name = "DIESCALERAH")
		private String diEscaleraH;

		/** The di bloque. */
		@Column(name = "DIBLOQUEH")
		private String diBloqueH;

		/** The di portal. */
		@Column(name = "DIPORTALH")
		private String diPortalH;

		/** The cocp H. */
		@Column(name = "COCPH")
		private String cocpH;

		/** The zona cobe H. */
		@Column(name = "ZODESCH")
		private String zonaCobeH;
		/**
		 * Instantiates a new generar excel.
		 *
		 * @return the row number
		 */

		/**
		 * Gets the row number.
		 *
		 * @return the row number
		 */
		public Integer getRowNumber() {
			return rowNumber;
		}

		/**
		 * Instantiates a new generar excel.
		 *
		 * @return the di escalera
		 */

		/**
		 * Gets the di escalera.
		 *
		 * @return the di escalera
		 */
		public String getDiEscalera() {
			return diEscalera;
		}

		/**
		 * Instantiates a new generar excel.
		 *
		 * @param diEscalera the di escalera
		 */

		/**
		 * Sets the di escalera.
		 *
		 * @param diEscalera the new di escalera
		 */
		public void setDiEscalera(String diEscalera) {
			this.diEscalera = diEscalera;
		}

		/**
		 * Instantiates a new generar excel.
		 *
		 * @param claveCompuesta the clave compuesta
		 * @param pkResoluci the pk resoluci
		 * @param nombre the nombre
		 * @param primerApe the primer ape
		 * @param segundoApe the segundo ape
		 * @param residencia the residencia
		 * @param direccion the direccion
		 * @param direccion2 the direccion 2
		 * @param dinumero the dinumero
		 * @param dipiso the dipiso
		 * @param dipuerta the dipuerta
		 * @param diEscalera the di escalera
		 * @param diBloque the di bloque
		 * @param diPortal the di portal
		 * @param cp the cp
		 * @param localidad the localidad
		 * @param provincia the provincia
		 * @param pais the pais
		 * @param grado the grado
		 * @param nivel the nivel
		 * @param dni the dni
		 * @param zonaCobe the zona cobe
		 * @param tipoPrestacion the tipo prestacion
		 * @param numListado the num listado
		 * @param modalidad the modalidad
		 * @param fechaResolucion the fecha resolucion
		 * @param fechaNotificacion the fecha notificacion
		 * @param fallecido the fallecido
		 * @param telefonoFijo the telefono fijo
		 * @param telefonoMovil the telefono movil
		 * @param pkSolicitud the pk solicitud
		 * @param nombreProvincia the nombre provincia
		 * @param nombreH the nombre H
		 * @param primerApeH the primer ape H
		 * @param segundoApeH the segundo ape H
		 * @param dniH the dni H
		 * @param direccionH the direccion H
		 * @param direccion2h the direccion 2 h
		 * @param localidadH the localidad H
		 * @param dinumeroH the dinumero H
		 * @param dipisoH the dipiso H
		 * @param dipuertaH the dipuerta H
		 * @param rowNumber the row number
		 * @param diEscaleraH the di escalera H
		 * @param diBloqueH the di bloque H
		 * @param diPortalH the di portal H
		 * @param cocpH the cocp H
		 * @param zonaCobeH the zona cobe H
		 */
		public GenerarExcel(
				// Integer fila,
				MiClaveCompuesta claveCompuesta, Long pkResoluci, String nombre, String primerApe, String segundoApe,
				String residencia,
				String direccion, String direccion2, String dinumero, String dipiso, String dipuerta, String diEscalera,
				String diBloque, String diPortal, String cp, String localidad, String provincia, String pais,
				// String numSolicitud,
				String grado, String nivel, String dni, String zonaCobe, String tipoPrestacion, String numListado,
				String modalidad, String fechaResolucion, // String tipoDocumento,
				String fechaNotificacion, String fallecido, String telefonoFijo, String telefonoMovil, Long pkSolicitud,
				String nombreProvincia, String nombreH, String primerApeH, String segundoApeH, String dniH,
				String direccionH, String direccion2h, String localidadH, String dinumeroH, String dipisoH,
				String dipuertaH, Integer rowNumber, String diEscaleraH, String diBloqueH, String diPortalH,
				String cocpH, String zonaCobeH) {
			super();
			// this.fila = fila;
			this.claveCompuesta = claveCompuesta;
			this.pkResoluci = pkResoluci;
			this.nombre = nombre;
			this.primerApe = primerApe;
			this.segundoApe = segundoApe;
			this.residencia = residencia;
			this.direccion = direccion;
			this.direccion2 = direccion2;
			this.dinumero = dinumero;
			this.dipiso = dipiso;
			this.dipuerta = dipuerta;
			this.diEscalera = diEscalera;
			this.diBloque = diBloque;
			this.diPortal = diPortal;
			this.cp = cp;
			this.localidad = localidad;
			this.provincia = provincia;
			this.pais = pais;
			// this.numSolicitud = numSolicitud;
			this.grado = grado;
			this.nivel = nivel;
			this.dni = dni;
			this.zonaCobe = zonaCobe;
			this.tipoPrestacion = tipoPrestacion;
			this.numListado = numListado;
			this.modalidad = modalidad;
			this.fechaResolucion = fechaResolucion;
			// this.tipoDocumento = tipoDocumento;
			this.fechaNotificacion = fechaNotificacion;
			this.fallecido = fallecido;
			this.telefonoFijo = telefonoFijo;
			this.telefonoMovil = telefonoMovil;
			this.pkSolicitud = pkSolicitud;
			this.nombreProvincia = nombreProvincia;
			this.nombreH = nombreH;
			this.primerApeH = primerApeH;
			this.segundoApeH = segundoApeH;
			this.dniH = dniH;
			this.direccionH = direccionH;
			direccion2H = direccion2h;
			this.localidadH = localidadH;
			this.dinumeroH = dinumeroH;
			this.dipisoH = dipisoH;
			this.dipuertaH = dipuertaH;
			this.rowNumber = rowNumber;
			this.diEscaleraH = diEscaleraH;
			this.diBloqueH = diBloqueH;
			this.diPortalH = diPortalH;
			this.cocpH = cocpH;
			this.zonaCobeH = zonaCobeH;
		}

		/**
		 * Gets the di bloque.
		 *
		 * @return the di bloque
		 */
		public String getDiBloque() {
			return diBloque;
		}

		/**
		 * Sets the di bloque.
		 *
		 * @param diBloque the new di bloque
		 */
		public void setDiBloque(String diBloque) {
			this.diBloque = diBloque;
		}

		/**
		 * Gets the di portal.
		 *
		 * @return the di portal
		 */
		public String getDiPortal() {
			return diPortal;
		}

		/**
		 * Sets the di portal.
		 *
		 * @param diPortal the new di portal
		 */
		public void setDiPortal(String diPortal) {
			this.diPortal = diPortal;
		}

		/**
		 * Gets the di escalera H.
		 *
		 * @return the di escalera H
		 */
		public String getDiEscaleraH() {
			return diEscaleraH;
		}

		/**
		 * Sets the di escalera H.
		 *
		 * @param diEscaleraH the new di escalera H
		 */
		public void setDiEscaleraH(String diEscaleraH) {
			this.diEscaleraH = diEscaleraH;
		}

		/**
		 * Gets the di bloque H.
		 *
		 * @return the di bloque H
		 */
		public String getDiBloqueH() {
			return diBloqueH;
		}

		/**
		 * Sets the di bloque H.
		 *
		 * @param diBloqueH the new di bloque H
		 */
		public void setDiBloqueH(String diBloqueH) {
			this.diBloqueH = diBloqueH;
		}

		/**
		 * Gets the di portal H.
		 *
		 * @return the di portal H
		 */
		public String getDiPortalH() {
			return diPortalH;
		}

		/**
		 * Sets the di portal H.
		 *
		 * @param diPortalH the new di portal H
		 */
		public void setDiPortalH(String diPortalH) {
			this.diPortalH = diPortalH;
		}

		/**
		 * Sets the row number.
		 *
		 * @param rowNumber the new row number
		 */
		public void setRowNumber(Integer rowNumber) {
			this.rowNumber = rowNumber;
		}

		/**
		 * Gets the pk solicitud.
		 *
		 * @return the pk solicitud
		 */
		public Long getPkSolicitud() {
			return pkSolicitud;
		}
		/**
		 * Sets the pk solicitud.
		 *
		 * @param pkSolicitud the new pk solicitud
		 */
		public void setPkSolicitud(Long pkSolicitud) {
			this.pkSolicitud = pkSolicitud;
		}


		/**
		 * Gets the nombre provincia.
		 *
		 * @return the nombre provincia
		 */
		public String getNombreProvincia() {
			return nombreProvincia;
		}


		/**
		 * Sets the nombre provincia.
		 *
		 * @param nombreProvincia the new nombre provincia
		 */
		public void setNombreProvincia(String nombreProvincia) {
			this.nombreProvincia = nombreProvincia;
		}


		/**
		 * Instantiates a new generar excel.
		 */
		public GenerarExcel() {
			super();
			// TODO Auto-generated constructor stub
		}


		/**
		 * Gets the num solicitud.
		 *
		 * @return the num solicitud
		 */
//		public String getNumSolicitud() {
//			return numSolicitud;
//		}
//
//
//		/**
//		 * Sets the num solicitud.
//		 *
//		 * @param numSolicitud the new num solicitud
//		 */
//		public void setNumSolicitud(final String numSolicitud) {
//			this.numSolicitud = numSolicitud;
//		}


		/**
		 * Gets the tipo prestacion.
		 *
		 * @return the tipo prestacion
		 */
		public String getTipoPrestacion() {
			return tipoPrestacion;
		}


		/**
		 * Sets the tipo prestacion.
		 *
		 * @param tipoPrestacion the new tipo prestacion
		 */
		public void setTipoPrestacion(final String tipoPrestacion) {
			this.tipoPrestacion = tipoPrestacion;
		}


		/**
		 * Gets the num listado.
		 *
		 * @return the num listado
		 */
		public String getNumListado() {
			return numListado;
		}


		/**
		 * Sets the num listado.
		 *
		 * @param numListado the new num listado
		 */
		public void setNumListado(final String numListado) {
			this.numListado = numListado;
		}


		/**
		 * Gets the modalidad.
		 *
		 * @return the modalidad
		 */
		public String getModalidad() {
			return modalidad;
		}


		/**
		 * Sets the modalidad.
		 *
		 * @param modalidad the new modalidad
		 */
		public void setModalidad(final String modalidad) {
			this.modalidad = modalidad;
		}


		/**
		 * Gets the tipo documento.
		 *
		 * @return the tipo documento
		 */
//		public String getTipoDocumento() {
//			return tipoDocumento;
//		}
//
//
//		/**
//		 * Sets the tipo documento.
//		 *
//		 * @param tipoDocumento the new tipo documento
//		 */
//		public void setTipoDocumento(final String tipoDocumento) {
//			this.tipoDocumento = tipoDocumento;
//		}


		/**
		 * Gets the fecha notificacion.
		 *
		 * @return the fecha notificacion
		 */
		public String getFechaNotificacion() {
			return fechaNotificacion;
		}


		/**
		 * Sets the fecha notificacion.
		 *
		 * @param fechaNotificacion the new fecha notificacion
		 */
		public void setFechaNotificacion(final String fechaNotificacion) {
			this.fechaNotificacion = fechaNotificacion;
		}


		/**
		 * Gets the fallecido.
		 *
		 * @return the fallecido
		 */
		public String getFallecido() {
			return fallecido;
		}


		/**
		 * Sets the fallecido.
		 *
		 * @param fallecido the new fallecido
		 */
		public void setFallecido(final String fallecido) {
			this.fallecido = fallecido;
		}


		/**
		 * Gets the residencia.
		 *
		 * @return the residencia
		 */
		public String getResidencia() {
			return residencia;
		}



		/**
		 * Sets the residencia.
		 *
		 * @param residencia the new residencia
		 */
		public void setResidencia(final String residencia) {
			this.residencia = residencia;
		}



		/**
		 * Gets the pk resoluci.
		 *
		 * @return the pk resoluci
		 */
		public Long getPkResoluci() {
			return pkResoluci;
		}

		/**
		 * Sets the pk resoluci.
		 *
		 * @param pkResoluci the new pk resoluci
		 */
		public void setPkResoluci(final Long pkResoluci) {
			this.pkResoluci = pkResoluci;
		}

		/**
		 * Gets the nombre.
		 *
		 * @return the nombre
		 */
		public String getNombre() {
			return nombre;
		}

		/**
		 * Sets the nombre.
		 *
		 * @param nombre the new nombre
		 */
		public void setNombre(final String nombre) {
			this.nombre = nombre;
		}

		/**
		 * Gets the primer ape.
		 *
		 * @return the primer ape
		 */
		public String getPrimerApe() {
			return primerApe;
		}

		/**
		 * Sets the primer ape.
		 *
		 * @param primerApe the new primer ape
		 */
		public void setPrimerApe(final String primerApe) {
			this.primerApe = primerApe;
		}

		/**
		 * Gets the segundo ape.
		 *
		 * @return the segundo ape
		 */
		public String getSegundoApe() {
			return segundoApe;
		}

		/**
		 * Sets the segundo ape.
		 *
		 * @param segundoApe the new segundo ape
		 */
		public void setSegundoApe(final String segundoApe) {
			this.segundoApe = segundoApe;
		}

		/**
		 * Gets the direccion.
		 *
		 * @return the direccion
		 */
		public String getDireccion() {
			return direccion;
		}

		/**
		 * Sets the direccion.
		 *
		 * @param direccion the new direccion
		 */
		public void setDireccion(final String direccion) {
			this.direccion = direccion;
		}

		/**
		 * Gets the direccion 2.
		 *
		 * @return the direccion 2
		 */
		public String getDireccion2() {
			return direccion2;
		}

		/**
		 * Sets the direccion 2.
		 *
		 * @param direccion2 the new direccion 2
		 */
		public void setDireccion2(final String direccion2) {
			this.direccion2 = direccion2;
		}

		/**
		 * Gets the cp.
		 *
		 * @return the cp
		 */
		public String getCp() {
			return cp;
		}

		/**
		 * Sets the cp.
		 *
		 * @param cp the new cp
		 */
		public void setCp(final String cp) {
			this.cp = cp;
		}

		/**
		 * Gets the localidad.
		 *
		 * @return the localidad
		 */
		public String getLocalidad() {
			return localidad;
		}

		/**
		 * Sets the localidad.
		 *
		 * @param localidad the new localidad
		 */
		public void setLocalidad(final String localidad) {
			this.localidad = localidad;
		}

		/**
		 * Gets the provincia.
		 *
		 * @return the provincia
		 */
		public String getProvincia() {
			return provincia;
		}

		/**
		 * Sets the provincia.
		 *
		 * @param provincia the new provincia
		 */
		public void setProvincia(final String provincia) {
			this.provincia = provincia;
		}

		/**
		 * Gets the pais.
		 *
		 * @return the pais
		 */
		public String getPais() {
			return pais;
		}

		/**
		 * Sets the pais.
		 *
		 * @param pais the new pais
		 */
		public void setPais(final String pais) {
			this.pais = pais;
		}

		/**
		 * Gets the grado.
		 *
		 * @return the grado
		 */
		public String getGrado() {
			return grado;
		}

		/**
		 * Sets the grado.
		 *
		 * @param grado the new grado
		 */
		public void setGrado(final String grado) {
			this.grado = grado;
		}

		/**
		 * Gets the nivel.
		 *
		 * @return the nivel
		 */
		public String getNivel() {
			return nivel;
		}

		/**
		 * Sets the nivel.
		 *
		 * @param nivel the new nivel
		 */
		public void setNivel(final String nivel) {
			this.nivel = nivel;
		}

		/**
		 * Gets the dni.
		 *
		 * @return the dni
		 */
		public String getDni() {
			return dni;
		}

		/**
		 * Sets the dni.
		 *
		 * @param dni the new dni
		 */
		public void setDni(final String dni) {
			this.dni = dni;
		}

		/**
		 * Gets the zona cobe.
		 *
		 * @return the zona cobe
		 */
		public String getZonaCobe() {
			return zonaCobe;
		}

		/**
		 * Sets the zona cobe.
		 *
		 * @param zonaCobe the new zona cobe
		 */
		public void setZonaCobe(final String zonaCobe) {
			this.zonaCobe = zonaCobe;
		}

		/**
		 * Gets the fecha resolucion.
		 *
		 * @return the fecha resolucion
		 */
		public String getFechaResolucion() {
			return fechaResolucion;
		}

		/**
		 * Sets the fecha resolucion.
		 *
		 * @param fechaResolucion the new fecha resolucion
		 */
		public void setFechaResolucion(final String fechaResolucion) {
			this.fechaResolucion = fechaResolucion;
		}

		/**
		 * Gets the telefono fijo.
		 *
		 * @return the telefono fijo
		 */
		public String getTelefonoFijo() {
			return telefonoFijo;
		}

		/**
		 * Sets the telefono fijo.
		 *
		 * @param telefonoFijo the new telefono fijo
		 */
		public void setTelefonoFijo(final String telefonoFijo) {
			this.telefonoFijo = telefonoFijo;
		}

		/**
		 * Gets the telefono movil.
		 *
		 * @return the telefono movil
		 */
		public String getTelefonoMovil() {
			return telefonoMovil;
		}

		/**
		 * Sets the telefono movil.
		 *
		 * @param telefonoMovil the new telefono movil
		 */
		public void setTelefonoMovil(final String telefonoMovil) {
			this.telefonoMovil = telefonoMovil;
		}

		/**
		 * Gets the serialversionuid.
		 *
		 * @return the serialversionuid
		 */
		public static long getSerialversionuid() {
			return serialVersionUID;
		}

		/**
		 * Gets the dinumero.
		 *
		 * @return the dinumero
		 */
		public String getDinumero() {
			return dinumero;
		}

		/**
		 * Sets the dinumero.
		 *
		 * @param dinumero the new dinumero
		 */
		public void setDinumero(String dinumero) {
			this.dinumero = dinumero;
		}

		/**
		 * Gets the dipiso.
		 *
		 * @return the dipiso
		 */
		public String getDipiso() {
			return dipiso;
		}

		/**
		 * Sets the dipiso.
		 *
		 * @param dipiso the new dipiso
		 */
		public void setDipiso(String dipiso) {
			this.dipiso = dipiso;
		}

		/**
		 * Gets the dipuerta.
		 *
		 * @return the dipuerta
		 */
		public String getDipuerta() {
			return dipuerta;
		}

		/**
		 * Sets the dipuerta.
		 *
		 * @param dipuerta the new dipuerta
		 */
		public void setDipuerta(String dipuerta) {
			this.dipuerta = dipuerta;
		}

		/**
		 * Gets the nombre H.
		 *
		 * @return the nombre H
		 */
		public String getNombreH() {
			return nombreH;
		}

		/**
		 * Sets the nombre H.
		 *
		 * @param nombreH the new nombre H
		 */
		public void setNombreH(String nombreH) {
			this.nombreH = nombreH;
		}

		/**
		 * Gets the primer ape H.
		 *
		 * @return the primer ape H
		 */
		public String getPrimerApeH() {
			return primerApeH;
		}

		/**
		 * Sets the primer ape H.
		 *
		 * @param primerApeH the new primer ape H
		 */
		public void setPrimerApeH(String primerApeH) {
			this.primerApeH = primerApeH;
		}

		/**
		 * Gets the segundo ape H.
		 *
		 * @return the segundo ape H
		 */
		public String getSegundoApeH() {
			return segundoApeH;
		}

		/**
		 * Sets the segundo ape H.
		 *
		 * @param segundoApeH the new segundo ape H
		 */
		public void setSegundoApeH(String segundoApeH) {
			this.segundoApeH = segundoApeH;
		}

		/**
		 * Gets the dni H.
		 *
		 * @return the dni H
		 */
		public String getDniH() {
			return dniH;
		}

		/**
		 * Sets the dni H.
		 *
		 * @param dniH the new dni H
		 */
		public void setDniH(String dniH) {
			this.dniH = dniH;
		}

		/**
		 * Gets the direccion H.
		 *
		 * @return the direccion H
		 */
		public String getDireccionH() {
			return direccionH;
		}

		/**
		 * Sets the direccion H.
		 *
		 * @param direccionH the new direccion H
		 */
		public void setDireccionH(String direccionH) {
			this.direccionH = direccionH;
		}

		/**
		 * Gets the direccion 2 H.
		 *
		 * @return the direccion 2 H
		 */
		public String getDireccion2H() {
			return direccion2H;
		}

		/**
		 * Sets the direccion 2 H.
		 *
		 * @param direccion2h the new direccion 2 H
		 */
		public void setDireccion2H(String direccion2h) {
			direccion2H = direccion2h;
		}

		/**
		 * Gets the localidad H.
		 *
		 * @return the localidad H
		 */
		public String getLocalidadH() {
			return localidadH;
		}

		/**
		 * Sets the localidad H.
		 *
		 * @param localidadH the new localidad H
		 */
		public void setLocalidadH(String localidadH) {
			this.localidadH = localidadH;
		}

		/**
		 * Gets the dinumero H.
		 *
		 * @return the dinumero H
		 */
		public String getDinumeroH() {
			return dinumeroH;
		}

		/**
		 * Sets the dinumero H.
		 *
		 * @param dinumeroH the new dinumero H
		 */
		public void setDinumeroH(String dinumeroH) {
			this.dinumeroH = dinumeroH;
		}

		/**
		 * Gets the dipiso H.
		 *
		 * @return the dipiso H
		 */
		public String getDipisoH() {
			return dipisoH;
		}

		/**
		 * Sets the dipiso H.
		 *
		 * @param dipisoH the new dipiso H
		 */
		public void setDipisoH(String dipisoH) {
			this.dipisoH = dipisoH;
		}

		/**
		 * Gets the dipuerta H.
		 *
		 * @return the dipuerta H
		 */
		public String getDipuertaH() {
			return dipuertaH;
		}

		/**
		 * Sets the dipuerta H.
		 *
		 * @param dipuertaH the new dipuerta H
		 */
		public void setDipuertaH(String dipuertaH) {
			this.dipuertaH = dipuertaH;
		}

		/**
		 * Gets the cocp H.
		 *
		 * @return the cocp H
		 */
		public String getCocpH() {
			return cocpH;
		}

		/**
		 * Sets the cocp H.
		 *
		 * @param cocpH the new cocp H
		 */
		public void setCocpH(String cocpH) {
			this.cocpH = cocpH;
		}

		/**
		 * Gets the zona cobe H.
		 *
		 * @return the zona cobe H
		 */
		public String getZonaCobeH() {
			return zonaCobeH;
		}

		/**
		 * Sets the zona cobe H.
		 *
		 * @param zonaCobeH the new zona cobe H
		 */
		public void setZonaCobeH(String zonaCobeH) {
			this.zonaCobeH = zonaCobeH;
		}

		/**
		 * Gets the clave compuesta.
		 *
		 * @return the clave compuesta
		 */
		public MiClaveCompuesta getClaveCompuesta() {
			return claveCompuesta;
		}

		/**
		 * Sets the clave compuesta.
		 *
		 * @param claveCompuesta the new clave compuesta
		 */
		public void setClaveCompuesta(MiClaveCompuesta claveCompuesta) {
			this.claveCompuesta = claveCompuesta;
		}

		/**
		 * Gets the fila.
		 *
		 * @return the fila
		 */
//		public Integer getFila() {
//			return fila;
//		}
//
//		/**
//		 * Sets the fila.
//		 *
//		 * @param fila the new fila
//		 */
//		public void setFila(Integer fila) {
//			this.fila = fila;
//		}

//		public String getPkCompuesta() {
//			return pkCompuesta;
//		}
//
//		/**
//		 * Sets the pk compuesta.
//		 *
//		 * @param pkCompuesta the new pk compuesta
//		 */
//		public void setPkCompuesta(String pkCompuesta) {
//			this.pkCompuesta = pkCompuesta;
//		}





}
