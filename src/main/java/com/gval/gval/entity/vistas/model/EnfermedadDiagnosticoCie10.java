package com.gval.gval.entity.vistas.model;

import java.io.Serializable;

import jakarta.persistence.*;

/**
 * The Class EnfermedadDiagnosticoCie10.
 */
@Entity
@Table(name = "SDV_LD_ENFERMEDADES")
public class EnfermedadDiagnosticoCie10 implements Serializable {

  /** The codigo var enfermedad. */
  @Id
  @Column(name = "COD_VAR_ENFERMEDAD")
  private String codVarEnfermedad;

  /** The descripcion var enfermedad. */
  @Column(name = "DESC_VAR_ENFERMEDAD")
  private String descVarEnfermedad;

  /**
   * Instantiates a new enfermedad diagnostico cie 10.
   */
  public EnfermedadDiagnosticoCie10() {
    super();
  }

  /**
   * Gets the cod var enfermedad.
   *
   * @return the cod var enfermedad
   */
  public String getCodVarEnfermedad() {
    return codVarEnfermedad;
  }

  /**
   * Sets the cod var enfermedad.
   *
   * @param codVarEnfermedad the new cod var enfermedad
   */
  public void setCodVarEnfermedad(final String codVarEnfermedad) {
    this.codVarEnfermedad = codVarEnfermedad;
  }

  /**
   * Gets the desc var enfermedad.
   *
   * @return the desc var enfermedad
   */
  public String getDescVarEnfermedad() {
    return descVarEnfermedad;
  }

  /**
   * Sets the desc var enfermedad.
   *
   * @param descVarEnfermedad the new desc var enfermedad
   */
  public void setDescVarEnfermedad(final String descVarEnfermedad) {
    this.descVarEnfermedad = descVarEnfermedad;
  }


}
