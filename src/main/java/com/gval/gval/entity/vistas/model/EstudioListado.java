package com.gval.gval.entity.vistas.model;

import java.util.Date;

import jakarta.persistence.*;

@Entity
@Table(name = "SDV_ESTUDIOS_EXPEDIENTE")
public class EstudioListado {

	private static final long serialVersionUID = 5488094284496709824L;
	
	@Id
	  @Column(name = "PK_SOLICITUD")
	  private Long pkSolicit;

	  /** The retipo. */
	  @Column(name = "SOLICITUD")
	  private String codigo;

	  /** The pk resolucion caducada. */
	  @Column(name = "ESTUDIADO_POR")
	  private String nombre;

	  /** The pk expedien. */
	  @Column(name = "TIPO")
	  private String tipoSolicitud;

	  /** The pk solicitud. */
	  @Column(name = "FECHA_APROBACION")
	  private Date fechaAprobacion;
	  
	  public EstudioListado() {
			super();
			// TODO Auto-generated constructor stub
		}

		

		public EstudioListado(Long pkSolicit, String codigo, String nombre, String tipoSolicitud, Date fechaAprobacion) {
			super();
			this.pkSolicit = pkSolicit;
			this.codigo = codigo;
			this.nombre = nombre;
			this.tipoSolicitud = tipoSolicitud;
			this.fechaAprobacion = fechaAprobacion;
		}



		public Long getPkSolicit() {
			return pkSolicit;
		}



		public void setPkSolicit(Long pkSolicit) {
			this.pkSolicit = pkSolicit;
		}



		public String getCodigo() {
			return codigo;
		}



		public void setCodigo(String codigo) {
			this.codigo = codigo;
		}



		public String getNombre() {
			return nombre;
		}



		public void setNombre(String nombre) {
			this.nombre = nombre;
		}



		public String getTipoSolicitud() {
			return tipoSolicitud;
		}



		public void setTipoSolicitud(String tipoSolicitud) {
			this.tipoSolicitud = tipoSolicitud;
		}



		public Date getFechaAprobacion() {
			return fechaAprobacion;
		}



		public void setFechaAprobacion(Date fechaAprobacion) {
			this.fechaAprobacion = fechaAprobacion;
		}



		public static long getSerialversionuid() {
			return serialVersionUID;
		}
}
