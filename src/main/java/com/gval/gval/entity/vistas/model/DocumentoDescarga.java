package com.gval.gval.entity.vistas.model;

import java.io.Serializable;

import jakarta.persistence.*;

/**
 * The Class DocumentoDescarga.
 */
@Entity
@Table(name = "SDV_LD_DOC_DESCARGAS")
@NamedStoredProcedureQuery(
		name = "DESCARGAR.DOCUMENTOS",
procedureName = "DESCARGAR_DOCUMENTOS_V2",
parameters = {
		@StoredProcedureParameter(mode = ParameterMode.IN,name = "P_COD_RESOLUCI", type = String.class),
		@StoredProcedureParameter(mode = ParameterMode.IN,name = "P_COD_TIPODOCU", type = String.class),
		@StoredProcedureParameter(mode = ParameterMode.IN,name = "P_ORDEN", type = String.class)}
)
public class DocumentoDescarga implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5295071877737432565L;

	/** The pk docu. */
	@Id
	@Column(name = "PK_DOCU")
	private Integer pkDocumento;

	/** The pk tipo docu. */
	@Column(name = "PK_TIPODOCU")
	private Integer pkTipoDocumento;

	/** The pk resoluci. */
	@Column(name = "PK_RESOLUCI")
	private Integer pkResolucion;

	/** The pk solicit. */
	@Column(name = "PK_SOLICIT")
	private Long pkSolicitud;

	@Column(name = "PEPRIMAPEL")
	private String primerApellido;

	@Column(name = "PESEGUAPEL")
	private String segundoApellido;

	@Column(name = "PENOMBRE")
	private String nombre;

	@Column(name = "TINOMBRE")
	private String tipoDocumentoNombre;

	@Column(name = "PROVINCIA")
	private String provincia;

	/**
	 * Instantiates a new documento descarga.
	 */
	public DocumentoDescarga() {
		//super();
	}

	public DocumentoDescarga(Integer pkDocumento, Integer pkTipoDocumento, Integer pkResolucion, Long pkSolicitud,
			String primerApellido, String segundoApellido, String nombre, String tipoDocumentoNombre,
			String provincia) {
		super();
		this.pkDocumento = pkDocumento;
		this.pkTipoDocumento = pkTipoDocumento;
		this.pkResolucion = pkResolucion;
		this.pkSolicitud = pkSolicitud;
		this.primerApellido = primerApellido;
		this.segundoApellido = segundoApellido;
		this.nombre = nombre;
		this.tipoDocumentoNombre = tipoDocumentoNombre;
		this.provincia = provincia;
	}

	public Integer getPkDocumento() {
		return pkDocumento;
	}

	public void setPkDocumento(Integer pkDocumento) {
		this.pkDocumento = pkDocumento;
	}

	public Integer getPkTipoDocumento() {
		return pkTipoDocumento;
	}

	public void setPkTipoDocumento(Integer pkTipoDocumento) {
		this.pkTipoDocumento = pkTipoDocumento;
	}

	public Integer getPkResolucion() {
		return pkResolucion;
	}

	public void setPkResolucion(Integer pkResolucion) {
		this.pkResolucion = pkResolucion;
	}

	public Long getPkSolicitud() {
		return pkSolicitud;
	}

	public void setPkSolicitud(Long pkSolicitud) {
		this.pkSolicitud = pkSolicitud;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipoDocumentoNombre() {
		return tipoDocumentoNombre;
	}

	public void setTipoDocumentoNombre(String tipoDocumentoNombre) {
		this.tipoDocumentoNombre = tipoDocumentoNombre;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
