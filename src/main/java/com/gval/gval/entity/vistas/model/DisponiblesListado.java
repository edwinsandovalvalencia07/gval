package com.gval.gval.entity.vistas.model;

import java.io.Serializable;

import jakarta.persistence.*;

/**
 * The Class DisponiblesListado.
 */
@Entity
@Table(name = "SDV_LD_DOC_DISPONIBLES_S")
public class DisponiblesListado implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5488094284496709824L;

	  /** The pk tipodocu. */
  	@Id
	  @Column(name = "PK_TIPODOCU")
	  private Long pkTipodocu;

	  @Column(name = "PK_RESOLUCI")
	  private String pKResolucion;
	  
	  @Column(name = "TINOMBRE")
	  private String nombre;

	/**
	 * Instantiates a new disponibles listado.
	 */
	public DisponiblesListado() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the pk tipodocu.
	 *
	 * @return the pk tipodocu
	 */
	public Long getPkTipodocu() {
		return pkTipodocu;
	}

	/**
	 * Sets the pk tipodocu.
	 *
	 * @param pkTipodocu the new pk tipodocu
	 */
	public void setPkTipodocu(final Long pkTipodocu) {
		this.pkTipodocu = pkTipodocu;
	}

	/**
	 * Gets the nombre.
	 *
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Sets the nombre.
	 *
	 * @param nombre the new nombre
	 */
	public void setNombre(final String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public String getpKResolucion() {
		return pKResolucion;
	}

	public void setpKResolucion(String pKResolucion) {
		this.pKResolucion = pKResolucion;
	}

	public DisponiblesListado(Long pkTipodocu, String pKResolucion, String nombre) {

		super();
		this.pkTipodocu = pkTipodocu;
		this.pKResolucion = pKResolucion;
		this.nombre = nombre;
	}


}
