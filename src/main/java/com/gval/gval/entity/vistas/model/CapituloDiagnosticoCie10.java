package com.gval.gval.entity.vistas.model;

import java.io.Serializable;

import jakarta.persistence.*;

/**
 * The Class DiagnosticoCie10.
 */
@Entity
@Table(name = "SDV_LD_CAPITULOS")
public class CapituloDiagnosticoCie10 implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1212401766362003208L;

  /** The codigo capitulo. */
  @Id
  @Column(name = "COD_CAPITULO")
  private Long codCapitulo;

  /** The descripcion capitulo. */
  @Column(name = "DESC_CAPITULO")
  private String descCapitulo;


  /**
   * Instantiates a new diagnostico cie 10.
   */
  public CapituloDiagnosticoCie10() {
    super();
  }

  /**
   * Gets the cod capitulo.
   *
   * @return the cod capitulo
   */
  public Long getCodCapitulo() {
    return codCapitulo;
  }

  /**
   * Sets the cod capitulo.
   *
   * @param codCapitulo the new cod capitulo
   */
  public void setCodCapitulo(final Long codCapitulo) {
    this.codCapitulo = codCapitulo;
  }

  /**
   * Gets the desc capitulo.
   *
   * @return the desc capitulo
   */
  public String getDescCapitulo() {
    return descCapitulo;
  }

  /**
   * Sets the desc capitulo.
   *
   * @param descCapitulo the new desc capitulo
   */
  public void setDescCapitulo(final String descCapitulo) {
    this.descCapitulo = descCapitulo;
  }

}