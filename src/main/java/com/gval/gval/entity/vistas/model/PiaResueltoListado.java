package com.gval.gval.entity.vistas.model;


import java.io.Serializable;
import java.util.Date;

import com.gval.gval.common.UtilidadesCommons;
import jakarta.persistence.*;


/**
 * The Class PiaResueltoListado.
 */
@Entity
@Table(name = "SDV_PIA_RESOLUCION_PRESTACION")
public class PiaResueltoListado implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 5488094284496709824L;

  /** The pk pia. */
  @Id
  @Column(name = "PK_PIA")
  private Long pkPia;

  /** The pk expedien. */
  @Column(name = "PK_EXPEDIEN")
  private Long pkExpedien;

  /** The pk solicit. */
  @Column(name = "PK_SOLICIT")
  private Long pkSolicitud;

  /** The codigo solicitud. */
  @Column(name = "SOLICITUD")
  private String codigoSolicitud;

  /** The tipo pia. */
  @Column(name = "TIPO_RECURSO_PIA")
  private String tipoPia;

  /** The tipo solicit pia. */
  @Column(name = "TIPO_PIA")
  private String tipoSolicitPia;

  /** The fecha resolucion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_RESOLUCI")
  private Date fechaResolucion;

  /** The fecha efecto. */
  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_EFECTO")
  private Date fechaEfecto;

  /** The fecha baja. */
  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_BAJA")
  private Date fechaBaja;

  /** The fecha inicio pago. */
  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_INI_PAGO")
  private Date fechaInicioPago;

  /** The terceros. */
  @Column(name = "TERCEROS")
  private Boolean terceros;

  /** The fiscalizacion previa. */
  @Column(name = "FISCALIZACION_PREVIA")
  private Boolean fiscalizacionPrevia;

  /** The contabilizacion. */
  @Column(name = "CONTABILIZACION")
  private Boolean contabilizacion;

  /** The resolucion economica. */
  @Column(name = "RESOL_ECONOMICA")
  private Boolean resolucionEconomica;

  /** The pipago. */
  @Column(name = "PIPAGO")
  private Boolean pipago;

  /**
   * Instantiates a new pia resuelto listado.
   */
  public PiaResueltoListado() {
    super();
  }

  /**
   * Gets the pk pia.
   *
   * @return the pk pia
   */
  public Long getPkPia() {
    return pkPia;
  }

  /**
   * Sets the pk pia.
   *
   * @param pkPia the new pk pia
   */
  public void setPkPia(final Long pkPia) {
    this.pkPia = pkPia;
  }

  /**
   * Gets the pk expedien.
   *
   * @return the pk expedien
   */
  public Long getPkExpedien() {
    return pkExpedien;
  }

  /**
   * Sets the pk expedien.
   *
   * @param pkExpedien the new pk expedien
   */
  public void setPkExpedien(final Long pkExpedien) {
    this.pkExpedien = pkExpedien;
  }

  /**
   * Gets the pk solicit.
   *
   * @return the pk solicit
   */
  public Long getPkSolicitud() {
    return pkSolicitud;
  }

  /**
   * Sets the pk solicit.
   *
   * @param pkSolicitud the new pk solicitud
   */
  public void setPkSolicitud(final Long pkSolicitud) {
    this.pkSolicitud = pkSolicitud;
  }

  /**
   * Gets the codigo solicitud.
   *
   * @return the codigo solicitud
   */
  public String getCodigoSolicitud() {
    return codigoSolicitud;
  }

  /**
   * Sets the codigo solicitud.
   *
   * @param codigoSolicitud the new codigo solicitud
   */
  public void setCodigoSolicitud(final String codigoSolicitud) {
    this.codigoSolicitud = codigoSolicitud;
  }

  /**
   * Gets the tipo pia.
   *
   * @return the tipo pia
   */
  public String getTipoPia() {
    return tipoPia;
  }

  /**
   * Sets the tipo pia.
   *
   * @param tipoPia the new tipo pia
   */
  public void setTipoPia(final String tipoPia) {
    this.tipoPia = tipoPia;
  }

  /**
   * Gets the fecha resolucion.
   *
   * @return the fecha resolucion
   */
  public Date getFechaResolucion() {
    return UtilidadesCommons.cloneDate(fechaResolucion);
  }

  /**
   * Sets the fecha resolucion.
   *
   * @param fechaResolucion the new fecha resolucion
   */
  public void setFechaResolucion(final Date fechaResolucion) {
    this.fechaResolucion = UtilidadesCommons.cloneDate(fechaResolucion);
  }

  /**
   * Gets the fecha efecto.
   *
   * @return the fecha efecto
   */
  public Date getFechaEfecto() {
    return UtilidadesCommons.cloneDate(fechaEfecto);
  }

  /**
   * Sets the fecha efecto.
   *
   * @param fechaEfecto the new fecha efecto
   */
  public void setFechaEfecto(final Date fechaEfecto) {
    this.fechaEfecto = UtilidadesCommons.cloneDate(fechaEfecto);
  }

  /**
   * Gets the fecha baja.
   *
   * @return the fecha baja
   */
  public Date getFechaBaja() {
    return UtilidadesCommons.cloneDate(fechaBaja);
  }

  /**
   * Sets the fecha baja.
   *
   * @param fechaBaja the new fecha baja
   */
  public void setFechaBaja(final Date fechaBaja) {
    this.fechaBaja = UtilidadesCommons.cloneDate(fechaBaja);
  }

  /**
   * Gets the fecha inicio pago.
   *
   * @return the fecha inicio pago
   */
  public Date getFechaInicioPago() {
    return UtilidadesCommons.cloneDate(fechaInicioPago);
  }

  /**
   * Sets the fecha inicio pago.
   *
   * @param fechaInicioPago the new fecha inicio pago
   */
  public void setFechaInicioPago(final Date fechaInicioPago) {
    this.fechaInicioPago = UtilidadesCommons.cloneDate(fechaInicioPago);
  }

  /**
   * Gets the terceros.
   *
   * @return the terceros
   */
  public Boolean getTerceros() {
    return terceros;
  }

  /**
   * Sets the terceros.
   *
   * @param terceros the new terceros
   */
  public void setTerceros(final Boolean terceros) {
    this.terceros = terceros;
  }

  /**
   * Gets the fiscalizacion previa.
   *
   * @return the fiscalizacion previa
   */
  public Boolean getFiscalizacionPrevia() {
    return fiscalizacionPrevia;
  }

  /**
   * Sets the fiscalizacion previa.
   *
   * @param fiscalizacionPrevia the new fiscalizacion previa
   */
  public void setFiscalizacionPrevia(final Boolean fiscalizacionPrevia) {
    this.fiscalizacionPrevia = fiscalizacionPrevia;
  }

  /**
   * Gets the contabilizacion.
   *
   * @return the contabilizacion
   */
  public Boolean getContabilizacion() {
    return contabilizacion;
  }

  /**
   * Sets the contabilizacion.
   *
   * @param contabilizacion the new contabilizacion
   */
  public void setContabilizacion(final Boolean contabilizacion) {
    this.contabilizacion = contabilizacion;
  }

  /**
   * Gets the resolucion economica.
   *
   * @return the resolucion economica
   */
  public Boolean getResolucionEconomica() {
    return resolucionEconomica;
  }

  /**
   * Sets the resolucion economica.
   *
   * @param resolucionEconomica the new resolucion economica
   */
  public void setResolucionEconomica(final Boolean resolucionEconomica) {
    this.resolucionEconomica = resolucionEconomica;
  }



  /**
   * Gets the tipo solicit pia.
   *
   * @return the tipo solicit pia
   */
  public String getTipoSolicitPia() {
    return tipoSolicitPia;
  }



  /**
   * Sets the tipo solicit pia.
   *
   * @param tipoSolicitPia the new tipo solicit pia
   */
  public void setTipoSolicitPia(final String tipoSolicitPia) {
    this.tipoSolicitPia = tipoSolicitPia;
  }

  /**
   * Gets the pipago.
   *
   * @return the pipago
   */
  public Boolean getPipago() {
    return pipago;
  }

  /**
   * Sets the pipago.
   *
   * @param pipago the new pipago
   */
  public void setPipago(final Boolean pipago) {
    this.pipago = pipago;
  }



}
