package com.gval.gval.entity.vistas.model;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;

/**
 * The Class JavapsaSolicitante.
 */
@Entity
@Table(name = "SDV_JAVAPSA_SOLICITANTES")
public class JavapsaSolicitante implements Serializable{

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 6130018611983090457L;

  /** The dni. */
  @Id
  @Column(name = "DNI", length = 100)
  private String dni;

  /** The codigo solicitud. */
  @Column(name = "CODSOLICITUD", length = 100)
  private String codigoSolicitud;

  /** The nombre. */
  @Column(name = "NOMBRE", length = 500)
  private String nombre;

  /** The primer apellido. */
  @Column(name = "APE1", length = 500)
  private String primerApellido;

  /** The segundo apellido. */
  @Column(name = "APE2", length = 500)
  private String segundoApellido;

  /** The tipo documento. */
  @Column(name = "TIPODNI", length = 3)
  private String tipoDocumento;

  /** The letra. */
  @Column(name = "LETRA", length = 1)
  private String letra;

  /** The nif. */
  @Column(name = "NIF", length = 101)
  private String nif;

  /** The fecha nacimiento. */
  @Column(name = "FENAC")
  private Date fechaNacimiento;

  /** The sexo. */
  @Column(name = "SEXO", length = 100)
  private String sexo;

  /** The nacionalidad. */
  @Column(name = "NACIONALIDAD", length = 100)
  private String nacionalidad;

  /** The estado civil. */
  @Column(name = "estado", length = 100)
  private String estadoCivil;

  /** The tipo via. */
  @Column(name = "TIPVIA", length = 40)
  private String tipoVia;

  /** The domicilio. */
  @Column(name = "DOMICILIO", length = 100)
  private String domicilio;

  /** The numero. */
  @Column(name = "NUM", length = 100)
  private String numero;

  /** The piso. */
  @Column(name = "PISO", length = 100)
  private String piso;

  /** The codigo postal. */
  @Column(name = "CP", length = 10)
  private String codigoPostal;

  /** The localidad. */
  @Column(name = "LOCALIDAD", length = 200)
  private String localidad;

  /** The provincia. */
  @Column(name = "PROVINCIA", length = 200)
  private String provincia;

  /** The zona cobertura. */
  @Column(name = "ZCOVER", length = 500)
  private String zonaCobertura;

  /** The telefono fijo. */
  @Column(name = "TFIJO", length = 100)
  private String telefonoFijo;

  /** The telefono movil. */
  @Column(name = "TMOVIL", length = 100)
  private String telefonoMovil;

  /** The numero seguridad social. */
  @Column(name = "NUMSEGSOCIAL", length = 10)
  private String numeroSeguridadSocial;

  /** The tipo seguridad social. */
  @Column(name = "tiposegsocial", length = 100)
  private String tipoSeguridadSocial;

  /** The tutelado. */
  @Column(name = "TUTELADO", length = 1)
  private String tutelado;

  /**
   * Instantiates a new javapsa solicitante.
   */
  public JavapsaSolicitante() {
    super();
  }

  /**
   * Gets the dni.
   *
   * @return the dni
   */
  public String getDni() {
    return dni;
  }

  /**
   * Sets the dni.
   *
   * @param dni the new dni
   */
  public void setDni(final String dni) {
    this.dni = dni;
  }

  /**
   * Gets the codigo solicitud.
   *
   * @return the codigo solicitud
   */
  public String getCodigoSolicitud() {
    return codigoSolicitud;
  }

  /**
   * Sets the codigo solicitud.
   *
   * @param codigoSolicitud the new codigo solicitud
   */
  public void setCodigoSolicitud(final String codigoSolicitud) {
    this.codigoSolicitud = codigoSolicitud;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the primer apellido.
   *
   * @return the primer apellido
   */
  public String getPrimerApellido() {
    return primerApellido;
  }

  /**
   * Sets the primer apellido.
   *
   * @param primerApellido the new primer apellido
   */
  public void setPrimerApellido(final String primerApellido) {
    this.primerApellido = primerApellido;
  }

  /**
   * Gets the segundo apellido.
   *
   * @return the segundo apellido
   */
  public String getSegundoApellido() {
    return segundoApellido;
  }

  /**
   * Sets the segundo apellido.
   *
   * @param segundoApellido the new segundo apellido
   */
  public void setSegundoApellido(final String segundoApellido) {
    this.segundoApellido = segundoApellido;
  }

  /**
   * Gets the tipo documento.
   *
   * @return the tipo documento
   */
  public String getTipoDocumento() {
    return tipoDocumento;
  }

  /**
   * Sets the tipo documento.
   *
   * @param tipoDocumento the new tipo documento
   */
  public void setTipoDocumento(final String tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }

  /**
   * Gets the letra.
   *
   * @return the letra
   */
  public String getLetra() {
    return letra;
  }

  /**
   * Sets the letra.
   *
   * @param letra the new letra
   */
  public void setLetra(final String letra) {
    this.letra = letra;
  }

  /**
   * Gets the nif.
   *
   * @return the nif
   */
  public String getNif() {
    return nif;
  }

  /**
   * Sets the nif.
   *
   * @param nif the new nif
   */
  public void setNif(final String nif) {
    this.nif = nif;
  }

  /**
   * Gets the fecha nacimiento.
   *
   * @return the fecha nacimiento
   */
  public Date getFechaNacimiento() {
    return fechaNacimiento;
  }

  /**
   * Sets the fecha nacimiento.
   *
   * @param fechaNacimiento the new fecha nacimiento
   */
  public void setFechaNacimiento(final Date fechaNacimiento) {
    this.fechaNacimiento = fechaNacimiento;
  }

  /**
   * Gets the sexo.
   *
   * @return the sexo
   */
  public String getSexo() {
    return sexo;
  }

  /**
   * Sets the sexo.
   *
   * @param sexo the new sexo
   */
  public void setSexo(final String sexo) {
    this.sexo = sexo;
  }

  /**
   * Gets the nacionalidad.
   *
   * @return the nacionalidad
   */
  public String getNacionalidad() {
    return nacionalidad;
  }

  /**
   * Sets the nacionalidad.
   *
   * @param nacionalidad the new nacionalidad
   */
  public void setNacionalidad(final String nacionalidad) {
    this.nacionalidad = nacionalidad;
  }

  /**
   * Gets the estado civil.
   *
   * @return the estado civil
   */
  public String getEstadoCivil() {
    return estadoCivil;
  }

  /**
   * Sets the estado civil.
   *
   * @param estadoCivil the new estado civil
   */
  public void setEstadoCivil(final String estadoCivil) {
    this.estadoCivil = estadoCivil;
  }

  /**
   * Gets the tipo via.
   *
   * @return the tipo via
   */
  public String getTipoVia() {
    return tipoVia;
  }

  /**
   * Sets the tipo via.
   *
   * @param tipoVia the new tipo via
   */
  public void setTipoVia(final String tipoVia) {
    this.tipoVia = tipoVia;
  }

  /**
   * Gets the domicilio.
   *
   * @return the domicilio
   */
  public String getDomicilio() {
    return domicilio;
  }

  /**
   * Sets the domicilio.
   *
   * @param domicilio the new domicilio
   */
  public void setDomicilio(final String domicilio) {
    this.domicilio = domicilio;
  }

  /**
   * Gets the numero.
   *
   * @return the numero
   */
  public String getNumero() {
    return numero;
  }

  /**
   * Sets the numero.
   *
   * @param numero the new numero
   */
  public void setNumero(final String numero) {
    this.numero = numero;
  }

  /**
   * Gets the piso.
   *
   * @return the piso
   */
  public String getPiso() {
    return piso;
  }

  /**
   * Sets the piso.
   *
   * @param piso the new piso
   */
  public void setPiso(final String piso) {
    this.piso = piso;
  }

  /**
   * Gets the codigo postal.
   *
   * @return the codigo postal
   */
  public String getCodigoPostal() {
    return codigoPostal;
  }

  /**
   * Sets the codigo postal.
   *
   * @param codigoPostal the new codigo postal
   */
  public void setCodigoPostal(final String codigoPostal) {
    this.codigoPostal = codigoPostal;
  }

  /**
   * Gets the localidad.
   *
   * @return the localidad
   */
  public String getLocalidad() {
    return localidad;
  }

  /**
   * Sets the localidad.
   *
   * @param localidad the new localidad
   */
  public void setLocalidad(final String localidad) {
    this.localidad = localidad;
  }

  /**
   * Gets the provincia.
   *
   * @return the provincia
   */
  public String getProvincia() {
    return provincia;
  }

  /**
   * Sets the provincia.
   *
   * @param provincia the new provincia
   */
  public void setProvincia(final String provincia) {
    this.provincia = provincia;
  }

  /**
   * Gets the zona cobertura.
   *
   * @return the zona cobertura
   */
  public String getZonaCobertura() {
    return zonaCobertura;
  }

  /**
   * Sets the zona cobertura.
   *
   * @param zonaCobertura the new zona cobertura
   */
  public void setZonaCobertura(final String zonaCobertura) {
    this.zonaCobertura = zonaCobertura;
  }

  /**
   * Gets the telefono fijo.
   *
   * @return the telefono fijo
   */
  public String getTelefonoFijo() {
    return telefonoFijo;
  }

  /**
   * Sets the telefono fijo.
   *
   * @param telefonoFijo the new telefono fijo
   */
  public void setTelefonoFijo(final String telefonoFijo) {
    this.telefonoFijo = telefonoFijo;
  }

  /**
   * Gets the telefono movil.
   *
   * @return the telefono movil
   */
  public String getTelefonoMovil() {
    return telefonoMovil;
  }

  /**
   * Sets the telefono movil.
   *
   * @param telefonoMovil the new telefono movil
   */
  public void setTelefonoMovil(final String telefonoMovil) {
    this.telefonoMovil = telefonoMovil;
  }

  /**
   * Gets the numero seguridad social.
   *
   * @return the numero seguridad social
   */
  public String getNumeroSeguridadSocial() {
    return numeroSeguridadSocial;
  }

  /**
   * Sets the numero seguridad social.
   *
   * @param numeroSeguridadSocial the new numero seguridad social
   */
  public void setNumeroSeguridadSocial(final String numeroSeguridadSocial) {
    this.numeroSeguridadSocial = numeroSeguridadSocial;
  }

  /**
   * Gets the tipo seguridad social.
   *
   * @return the tipo seguridad social
   */
  public String getTipoSeguridadSocial() {
    return tipoSeguridadSocial;
  }

  /**
   * Sets the tipo seguridad social.
   *
   * @param tipoSeguridadSocial the new tipo seguridad social
   */
  public void setTipoSeguridadSocial(final String tipoSeguridadSocial) {
    this.tipoSeguridadSocial = tipoSeguridadSocial;
  }

  /**
   * Gets the tutelado.
   *
   * @return the tutelado
   */
  public String getTutelado() {
    return tutelado;
  }

  /**
   * Sets the tutelado.
   *
   * @param tutelado the new tutelado
   */
  public void setTutelado(final String tutelado) {
    this.tutelado = tutelado;
  }


}
