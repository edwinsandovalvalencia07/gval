package com.gval.gval.entity.vistas.model;

import java.io.Serializable;
import java.util.Objects;
import jakarta.persistence.*;

// TODO: Auto-generated Javadoc
/**
 * The Class MiClaveCompuesta.
 */
@Embeddable
public class MiClaveCompuesta implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The num solicitud. */
	@Column(name = "SOLICITUD")
	private String numSolicitud;

	/** The tipo documento. */
	@Column(name = "TINOMBRE")
	private String tipoDocumento;

	/**
	 * Instantiates a new mi clave compuesta.
	 */
	// Constructor vacío requerido por JPA
	public MiClaveCompuesta() {
	}

	/**
	 * Instantiates a new mi clave compuesta.
	 *
	 * @param numSolicitud the num solicitud
	 * @param tipoDocumento the tipo documento
	 */
	public MiClaveCompuesta(String numSolicitud, String tipoDocumento) {
		this.numSolicitud = numSolicitud;
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * Gets the num solicitud.
	 *
	 * @return the num solicitud
	 */
	// Getters y Setters
	public String getNumSolicitud() {
		return numSolicitud;
	}

	/**
	 * Sets the num solicitud.
	 *
	 * @param numSolicitud the new num solicitud
	 */
	public void setNumSolicitud(String numSolicitud) {
		this.numSolicitud = numSolicitud;
	}

	/**
	 * Gets the tipo documento.
	 *
	 * @return the tipo documento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * Sets the tipo documento.
	 *
	 * @param tipoDocumento the new tipo documento
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * Equals.
	 *
	 * @param o the o
	 * @return true, if successful
	 */
	// Métodos equals() y hashCode() para la comparación y el uso en colecciones
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		MiClaveCompuesta that = (MiClaveCompuesta) o;
		return Objects.equals(numSolicitud, that.numSolicitud) && Objects.equals(tipoDocumento, that.tipoDocumento);
	}

	/**
	 * Hash code.
	 *
	 * @return the int
	 */
	@Override
	public int hashCode() {
		return Objects.hash(numSolicitud, tipoDocumento);
	}
}
