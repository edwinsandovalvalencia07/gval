package com.gval.gval.entity.vistas.model;


import java.io.Serializable;

import com.gval.gval.entity.model.ZonaCobertura;
import jakarta.persistence.*;


/**
 * The persistent class for the SDV_CENTROS database table.
 *
 */
@Entity
@Table(name = "SDV_CENTROS")
public class Centro implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -2601752090224831116L;

  /** The numcen. */
  @Id
  @Column(name = "NUMCEN", length = 41)
  private String numcen;

  /** The nombre. */
  @Column(name = "NOMBRE", length = 100)
  private String nombre;

  /** The codigo poblacion. */
  @Column(name = "COD_PROVINCI")
  private String codigoProvincia;

  /** The provincia. */
  @Column(name = "PROVINCIA", length = 30)
  private String provincia;

  /** The codigo poblacion. */
  @Column(name = "COD_MUNICIPI")
  private String codigoMunicipio;

  /** The municipio. */
  @Column(name = "MUNICIPIO", length = 30)
  private String municipio;

  /** The codigo poblacion. */
  @Column(name = "COD_POBLAC")
  private String codigoPoblacion;

  /** The codigo postal. */
  @Column(name = "CP", length = 5)
  private String codigoPostal;

  /** The domicilio. */
  @Column(name = "DOMICI", length = 40)
  private String domicilio;

  /** The telefono. */
  @Column(name = "TELEF", length = 9)
  private String telefono;

  /** The tipo. */
  @Column(name = "TIPO", length = 60)
  private String tipo;

  /** The num tipo. */
  @Column(name = "NUM_TIPO", length = 4)
  private String numTipo;

  /** The descripcion. */
  @Column(name = "DESCTIPOCENTRO", length = 18)
  private String descripcion;

  /** The zona cobertura. */
  // bi-directional many-to-one association to CodigoPostal
  @ManyToOne
  @JoinColumn(name = "PK_ZONACOBE")
  private ZonaCobertura zonaCobertura;

  /**
   * Instantiates a new centro.
   */
  public Centro() {
    // Empty constructor
  }



  /**
   * Instantiates a new centro.
   *
   * @param numcen the numcen
   * @param nombre the nombre
   * @param provincia the provincia
   * @param municipio the municipio
   * @param codigoPostal the codigo postal
   * @param domicilio the domicilio
   * @param telefono the telefono
   * @param tipo the tipo
   * @param numTipo the num tipo
   * @param descripcion the descripcion
   * @param zonaCobertura the zona cobertura
   */
  public Centro(final String numcen, final String nombre,
      final String provincia, final String municipio, final String codigoPostal,
      final String domicilio, final String telefono, final String tipo,
      final String numTipo, final String descripcion,
      final ZonaCobertura zonaCobertura) {
    super();
    this.numcen = numcen;
    this.nombre = nombre;
    this.provincia = provincia;
    this.municipio = municipio;
    this.codigoPostal = codigoPostal;
    this.domicilio = domicilio;
    this.telefono = telefono;
    this.tipo = tipo;
    this.numTipo = numTipo;
    this.descripcion = descripcion;
    this.zonaCobertura = zonaCobertura;
  }



  /**
   * Gets the numcen.
   *
   * @return the numcen
   */
  public String getNumcen() {
    return numcen;
  }

  /**
   * Sets the numcen.
   *
   * @param numcen the new numcen
   */
  public void setNumcen(final String numcen) {
    this.numcen = numcen;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the provincia.
   *
   * @return the provincia
   */
  public String getProvincia() {
    return provincia;
  }

  /**
   * Sets the provincia.
   *
   * @param provincia the new provincia
   */
  public void setProvincia(final String provincia) {
    this.provincia = provincia;
  }

  /**
   * Gets the municipio.
   *
   * @return the municipio
   */
  public String getMunicipio() {
    return municipio;
  }

  /**
   * Sets the municipio.
   *
   * @param municipio the new municipio
   */
  public void setMunicipio(final String municipio) {
    this.municipio = municipio;
  }



  /**
   * Gets the codigo poblacion.
   *
   * @return the codigo poblacion
   */
  public String getCodigoPoblacion() {
    return codigoPoblacion;
  }

  /**
   * Sets the codigo poblacion.
   *
   * @param codigoPoblacion the new codigo poblacion
   */
  public void setCodigoPoblacion(final String codigoPoblacion) {
    this.codigoPoblacion = codigoPoblacion;
  }


  /**
   * Gets the codigo postal.
   *
   * @return the codigo postal
   */
  public String getCodigoPostal() {
    return codigoPostal;
  }

  /**
   * Sets the codigo postal.
   *
   * @param codigoPostal the new codigo postal
   */
  public void setCodigoPostal(final String codigoPostal) {
    this.codigoPostal = codigoPostal;
  }

  /**
   * Gets the domicilio.
   *
   * @return the domicilio
   */
  public String getDomicilio() {
    return domicilio;
  }

  /**
   * Sets the domicilio.
   *
   * @param domicilio the new domicilio
   */
  public void setDomicilio(final String domicilio) {
    this.domicilio = domicilio;
  }

  /**
   * Gets the telefono.
   *
   * @return the telefono
   */
  public String getTelefono() {
    return telefono;
  }

  /**
   * Sets the telefono.
   *
   * @param telefono the new telefono
   */
  public void setTelefono(final String telefono) {
    this.telefono = telefono;
  }

  /**
   * Gets the tipo.
   *
   * @return the tipo
   */
  public String getTipo() {
    return tipo;
  }

  /**
   * Sets the tipo.
   *
   * @param tipo the new tipo
   */
  public void setTipo(final String tipo) {
    this.tipo = tipo;
  }



  /**
   * Gets the num tipo.
   *
   * @return the num tipo
   */
  public String getNumTipo() {
    return numTipo;
  }

  /**
   * Sets the num tipo.
   *
   * @param numTipo the new num tipo
   */
  public void setNumTipo(final String numTipo) {
    this.numTipo = numTipo;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the zona cobertura.
   *
   * @return the zona cobertura
   */
  public ZonaCobertura getZonaCobertura() {
    return zonaCobertura;
  }

  /**
   * Sets the zona cobertura.
   *
   * @param zonaCobertura the new zona cobertura
   */
  public void setZonaCobertura(final ZonaCobertura zonaCobertura) {
    this.zonaCobertura = zonaCobertura;
  }



  /**
   * Gets the codigo provincia.
   *
   * @return the codigo provincia
   */
  public String getCodigoProvincia() {
    return codigoProvincia;
  }



  /**
   * Sets the codigo provincia.
   *
   * @param codigoProvincia the new codigo provincia
   */
  public void setCodigoProvincia(final String codigoProvincia) {
    this.codigoProvincia = codigoProvincia;
  }



  /**
   * Gets the codigo municipio.
   *
   * @return the codigo municipio
   */
  public String getCodigoMunicipio() {
    return codigoMunicipio;
  }



  /**
   * Sets the codigo municipio.
   *
   * @param codigoMunicipio the new codigo municipio
   */
  public void setCodigoMunicipio(final String codigoMunicipio) {
    this.codigoMunicipio = codigoMunicipio;
  }
}
