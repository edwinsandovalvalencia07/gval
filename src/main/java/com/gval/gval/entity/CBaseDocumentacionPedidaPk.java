package com.gval.gval.entity;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

import javax.persistence.Column;

/**
 * The Class CBaseDocumentacionPedidaPk.
 */
public class CBaseDocumentacionPedidaPk implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -7019649705011974094L;

  /** The pex maquina. */
  @Column(name = "PEX_MAQUINA", length = 1)
  private String pexMaquina;

  /** The pex numero reset. */
  @Column(name = "PEX_NUMERO_RESET")
  private Long pexNumeroReset;

  /** The pex cod prueba. */
  @Column(name = "PEX_CODPRUEBA", length = 3)
  private Long pexCodPrueba;

  /**
   * Instantiates a new c base documentacion pedida pk.
   */
  public CBaseDocumentacionPedidaPk() {}

  /**
   * Gets the pex maquina.
   *
   * @return the pex maquina
   */
  public String getPexMaquina() {
    return pexMaquina;
  }

  /**
   * Sets the pex maquina.
   *
   * @param pexMaquina the new pex maquina
   */
  public void setPexMaquina(final String pexMaquina) {
    this.pexMaquina = pexMaquina;
  }

  /**
   * Gets the pex numero reset.
   *
   * @return the pex numero reset
   */
  public Long getPexNumeroReset() {
    return pexNumeroReset;
  }

  /**
   * Sets the pex numero reset.
   *
   * @param pexNumeroReset the new pex numero reset
   */
  public void setPexNumeroReset(final Long pexNumeroReset) {
    this.pexNumeroReset = pexNumeroReset;
  }

  /**
   * Gets the pex cod prueba.
   *
   * @return the pex cod prueba
   */
  public Long getPexCodPrueba() {
    return pexCodPrueba;
  }

  /**
   * Sets the pex cod prueba.
   *
   * @param pexCodPrueba the new pex cod prueba
   */
  public void setPexCodPrueba(final Long pexCodPrueba) {
    this.pexCodPrueba = pexCodPrueba;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
