package com.gval.gval.entity;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The Class CBaseDocumentacionPedida.
 */
@Entity
@Table(name = "SDV_CBASE_DOCPEDIDA")
public class CBaseDocumentacionPedida implements Serializable{

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 163145042236498338L;

  /** The id. */
  @EmbeddedId
  private CBaseDocumentacionPedidaPk id;

  /** The soldni solicitante. */
  @Column(name = "SOL_DNI_SOLICITANTE")
  private String soldniSolicitante;

  /** The doc pedida cbase. */
  @Column(name = "DOCPEDIDACBASE")
  private String docPedidaCbase;

  /**
   * Instantiates a new c base documentacion pedida.
   */
  public CBaseDocumentacionPedida() {

  }

  /**
   * Gets the id.
   *
   * @return the id
   */
  public CBaseDocumentacionPedidaPk getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(final CBaseDocumentacionPedidaPk id) {
    this.id = id;
  }

  /**
   * Gets the soldni solicitante.
   *
   * @return the soldni solicitante
   */
  public String getSoldniSolicitante() {
    return soldniSolicitante;
  }

  /**
   * Sets the soldni solicitante.
   *
   * @param soldniSolicitante the new soldni solicitante
   */
  public void setSoldniSolicitante(final String soldniSolicitante) {
    this.soldniSolicitante = soldniSolicitante;
  }

  /**
   * Gets the doc pedida cbase.
   *
   * @return the doc pedida cbase
   */
  public String getDocPedidaCbase() {
    return docPedidaCbase;
  }

  /**
   * Sets the doc pedida cbase.
   *
   * @param docPedidaCbase the new doc pedida cbase
   */
  public void setDocPedidaCbase(final String docPedidaCbase) {
    this.docPedidaCbase = docPedidaCbase;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
