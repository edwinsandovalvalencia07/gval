package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The persistent class for the SDM_DOCUSUBS database table.
 *
 */
@Entity
@Table(name = "SDM_DOCUSUBS")
@GenericGenerator(name = "SDM_DOCUSUBS_PKDOCUSUBS_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_DOCUSUBS"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})

public final class DocumentoSubsanacion implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -7250658433412860543L;

  /** The pk docusubs. */
  @Id
  @Column(name = "PK_DOCUSUBS", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_DOCUSUBS_PKDOCUSUBS_GENERATOR")
  private Long pkDocusubs;

  /** The fecha entregado. */
  @Temporal(TemporalType.DATE)
  @Column(name = "DOENTREGAD")
  private Date fechaEntregado;

  /** The observaciones. */
  @Column(name = "DOOBSE", length = 4000)
  private String observaciones;

  /** The activo. */
  @Column(name = "DOSACTIVO", nullable = false)
  private Boolean activo;

  /** The persona. */
  // bi-directional many-to-one association to SdmPersona
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA2")
  private Persona persona;

  /** The subsanacion. */
  // bi-directional many-to-one association to Subsanacion
  @ManyToOne
  @JoinColumn(name = "PK_SUBSANAC", nullable = false)
  private Subsanacion subsanacion;

  /** The tipo documento. */
  // bi-directional many-to-one association to SdxTipodocu
  @ManyToOne
  @JoinColumn(name = "PK_TIPODOCU", nullable = false)
  private TipoDocumento tipoDocumento;

  /** The usuario. */
  // bi-directional many-to-one association to SdxUsuario
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA")
  private Usuario usuario;

  /**
   * Instantiates a new documento subsanacion.
   */
  public DocumentoSubsanacion() {
    // Constructor vacío
  }

  /**
   * Gets the pk docusubs.
   *
   * @return the pkDocusubs
   */
  public Long getPkDocusubs() {
    return pkDocusubs;
  }

  /**
   * Sets the pk docusubs.
   *
   * @param pkDocusubs the pkDocusubs to set
   */
  public void setPkDocusubs(final Long pkDocusubs) {
    this.pkDocusubs = pkDocusubs;
  }

  /**
   * Gets the fecha entregado.
   *
   * @return the fechaEntregado
   */
  public Date getFechaEntregado() {
    return UtilidadesCommons.cloneDate(fechaEntregado);
  }

  /**
   * Sets the fecha entregado.
   *
   * @param fechaEntregado the fechaEntregado to set
   */
  public void setFechaEntregado(final Date fechaEntregado) {
    this.fechaEntregado = UtilidadesCommons.cloneDate(fechaEntregado);
  }

  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return observaciones;
  }

  /**
   * Sets the observaciones.
   *
   * @param observaciones the observaciones to set
   */
  public void setObservaciones(final String observaciones) {
    this.observaciones = observaciones;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the persona.
   *
   * @return the persona
   */
  public Persona getPersona() {
    return persona;
  }

  /**
   * Sets the persona.
   *
   * @param persona the persona to set
   */
  public void setPersona(final Persona persona) {
    this.persona = persona;
  }

  /**
   * Gets the subsanacion.
   *
   * @return the subsanacion
   */
  public Subsanacion getSubsanacion() {
    return subsanacion;
  }

  /**
   * Sets the subsanacion.
   *
   * @param subsanacion the subsanacion to set
   */
  public void setSubsanacion(final Subsanacion subsanacion) {
    this.subsanacion = subsanacion;
  }

  /**
   * Gets the tipo documento.
   *
   * @return the tipoDocumento
   */
  public TipoDocumento getTipoDocumento() {
    return tipoDocumento;
  }

  /**
   * Sets the tipo documento.
   *
   * @param tipoDocumento the tipoDocumento to set
   */
  public void setTipoDocumento(final TipoDocumento tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the usuario to set
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
