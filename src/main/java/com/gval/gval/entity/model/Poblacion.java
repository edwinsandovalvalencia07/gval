package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;

/**
 * The persistent class for the SDX_POBLAC database table.
 *
 */
@Entity
@Table(name = "SDX_POBLAC")
public class Poblacion implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 312566498753321272L;


  /** The pk poblacion. */
  @Id
  @Column(name = "PK_POBLAC", unique = true, nullable = false)
  private Long pkPoblacion;

  /** The activo. */
  @Column(name = "POACTIVO", nullable = false, precision = 38)
  private Boolean activo;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "POFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The imserso. */
  @Column(name = "POIMSERSO", length = 15)
  private String imserso;

  /** The nombre. */
  @Column(name = "PONOMBRE", nullable = false, length = 50)
  private String nombre;

  /** The codigo provincia. */
  @Column(name = "COD_PROV", nullable = false, length = 2)
  private String codigoProvincia;

  /** The codigo municipio. */
  @Column(name = "COD_MUNI", nullable = false, length = 3)
  private String codigoMunicipio;

  /** The codigo poblacion. */
  @Column(name = "COD_POBLAC", nullable = false, length = 6)
  private String codigoPoblacion;

  /** The municipio. */
  // bi-directional many-to-one association to SdxMunicipi
  @ManyToOne
  @JoinColumn(name = "PK_MUNICIPI", nullable = false)
  private Municipio municipio;

  /**
   * Instantiates a new poblacion.
   */
  public Poblacion() {
    // Empty constructor
  }

  /**
   * Gets the pk poblacion.
   *
   * @return the pk poblacion
   */
  public Long getPkPoblacion() {
    return pkPoblacion;
  }

  /**
   * Sets the pk poblacion.
   *
   * @param pkPoblacion the new pk poblacion
   */
  public void setPkPoblacion(final Long pkPoblacion) {
    this.pkPoblacion = pkPoblacion;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the imserso.
   *
   * @return the imserso
   */
  public String getImserso() {
    return imserso;
  }

  /**
   * Sets the imserso.
   *
   * @param imserso the new imserso
   */
  public void setImserso(final String imserso) {
    this.imserso = imserso;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the codigo provincia.
   *
   * @return the codigoProvincia
   */
  public String getCodigoProvincia() {
    return codigoProvincia;
  }

  /**
   * Sets the codigo provincia.
   *
   * @param codigoProvincia the codigoProvincia to set
   */
  public void setCodigoProvincia(final String codigoProvincia) {
    this.codigoProvincia = codigoProvincia;
  }

  /**
   * Gets the codigo municipio.
   *
   * @return the codigoMunicipio
   */
  public String getCodigoMunicipio() {
    return codigoMunicipio;
  }

  /**
   * Sets the codigo municipio.
   *
   * @param codigoMunicipio the codigoMunicipio to set
   */
  public void setCodigoMunicipio(final String codigoMunicipio) {
    this.codigoMunicipio = codigoMunicipio;
  }

  /**
   * Gets the codigo poblacion.
   *
   * @return the codigoPoblacion
   */
  public String getCodigoPoblacion() {
    return codigoPoblacion;
  }

  /**
   * Sets the codigo poblacion.
   *
   * @param codigoPoblacion the codigoPoblacion to set
   */
  public void setCodigoPoblacion(final String codigoPoblacion) {
    this.codigoPoblacion = codigoPoblacion;
  }

  /**
   * Gets the municipio.
   *
   * @return the municipio
   */
  public Municipio getMunicipio() {
    return municipio;
  }

  /**
   * Sets the municipio.
   *
   * @param municipio the new municipio
   */
  public void setMunicipio(final Municipio municipio) {
    this.municipio = municipio;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
