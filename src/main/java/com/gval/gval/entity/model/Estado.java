package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;


import jakarta.persistence.*;


/**
 * The persistent class for the SDX_ESTADO database table.
 *
 */
@Entity
@Table(name = "SDX_ESTADO")
@NamedQuery(name = "SdxEstado.findAll", query = "SELECT s FROM Estado s")
@GenericGenerator(name = "SDX_ESTADO_PKESTADO_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {@Parameter(name = "sequence_name", value = "SEC_SDX_ESTADO"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class Estado implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk estado. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDX_ESTADO_PKESTADO_GENERATOR")
  @Column(name = "PK_ESTADO", unique = true, nullable = false)
  private Long pkEstado;

  /** The codigo. */
  @Column(name = "ESCODIGO", nullable = false, length = 4)
  private String codigo;

  /** The descripcion. */
  @Column(name = "ESDESC", nullable = false, length = 250)
  private String descripcion;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "ESFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The es estado final. */
  @Column(name = "ESFINAL", nullable = false, precision = 38)
  private Boolean esEstadoFinal;

  /** The nombre. */
  @Column(name = "ESNOMBRE", length = 50)
  private String nombre;

  /** The proceso. */
  @Column(name = "ESPROCESO", nullable = false, precision = 38)
  private Long proceso;

  /** The activo. */
  @Column(name = "ESSACTIVO", nullable = false, precision = 38)
  private Boolean activo;

  /** The nsisaad terminado. */
  @Column(name = "NSISAAD_TERMINADO", precision = 1)
  private Boolean nsisaadTerminado;

  /**
   * Instantiates a new estado.
   */
  public Estado() {
    super();
  }

  /**
   * Gets the pk estado.
   *
   * @return the pkEstado
   */
  public Long getPkEstado() {
    return pkEstado;
  }

  /**
   * Sets the pk estado.
   *
   * @param pkEstado the pkEstado to set
   */
  public void setPkEstado(final Long pkEstado) {
    this.pkEstado = pkEstado;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the codigo to set
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the descripcion to set
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return fechaCreacion;
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

  /**
   * Gets the es estado final.
   *
   * @return the esEstadoFinal
   */
  public Boolean getEsEstadoFinal() {
    return esEstadoFinal;
  }

  /**
   * Sets the es estado final.
   *
   * @param esEstadoFinal the esEstadoFinal to set
   */
  public void setEsEstadoFinal(final Boolean esEstadoFinal) {
    this.esEstadoFinal = esEstadoFinal;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the nombre to set
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the proceso.
   *
   * @return the proceso
   */
  public Long getProceso() {
    return proceso;
  }

  /**
   * Sets the proceso.
   *
   * @param proceso the proceso to set
   */
  public void setProceso(final Long proceso) {
    this.proceso = proceso;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the nsisaad terminado.
   *
   * @return the nsisaadTerminado
   */
  public Boolean getNsisaadTerminado() {
    return nsisaadTerminado;
  }

  /**
   * Sets the nsisaad terminado.
   *
   * @param nsisaadTerminado the nsisaadTerminado to set
   */
  public void setNsisaadTerminado(final Boolean nsisaadTerminado) {
    this.nsisaadTerminado = nsisaadTerminado;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
