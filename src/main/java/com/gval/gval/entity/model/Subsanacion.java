package com.gval.gval.entity.model;

import com.gval.gval.common.ConstantesCommons;
import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


import jakarta.persistence.*;


/**
 * The persistent class for the SDM_SUBSANAC database table.
 *
 */
@Entity
@Table(name = "SDM_SUBSANAC")
@GenericGenerator(name = "SDM_SUBSANAC_PKSUBSANAC_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_SUBSANAC"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public final class Subsanacion implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 7614894262704410008L;

  /** The pk subsanacion. */
  @Id
  @Column(name = "PK_SUBSANAC", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_SUBSANAC_PKSUBSANAC_GENERATOR")
  private Long pkSubsanacion;

  /** The activo. */
  @Column(name = "SUACTIVO", nullable = false)
  private Boolean activo;

  /** The derecho pendiente. */
  @Column(name = "SUDERECHOPENDIENTE", nullable = false)
  private Boolean derechoPendiente;

  /** The fase. */
  @Column(name = "SUFASE", length = 1)
  private String fase;

  /** The fecha completado. */
  @Temporal(TemporalType.DATE)
  @Column(name = "SUFECHCOMP")
  private Date fechaCompletado;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "SUFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The observaciones. */
  @Column(name = "SUOBSE", length = 4000)
  private String observaciones;

  /** The perfil subsanador. */
  @Column(name = "SUPERFILSUBSANADOR", nullable = false, precision = 10)
  private Long perfilSubsanador;

  /** The app origen. */
  @Column(name = "APP_ORIGEN")
  private String appOrigen;

  /** The documentos subsanacion. */
  @OneToMany(mappedBy = "subsanacion", cascade = CascadeType.ALL)
  private List<DocumentoSubsanacion> documentosSubsanacion;

  /** The documento generado. */
  // bi-directional many-to-one association to SdmDocugene
  @ManyToOne
  @JoinColumn(name = "PK_DOCU")
  private DocumentoGenerado documentoGenerado;

  /** The solicitud. */
  // bi-directional many-to-one association to SdmSolicit
  @ManyToOne
  @JoinColumn(name = "PK_SOLICIT", nullable = false)
  private Solicitud solicitud;

  /** The usuario. */
  // bi-directional many-to-one association to SdxUsuario
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA", nullable = false)
  private Usuario usuario;

  /** The documento A subsanar. */
  @ManyToOne
  @JoinColumn(name = "PK_DOCU2")
  private DocumentoAportado documentoASubsanar;

  /**
   * Instantiates a new subsanacion.
   */
  public Subsanacion() {
    // Constructor vacío
  }

  /**
   * On pre persist.
   */
  @PrePersist
  public void onPrePersist() {
    appOrigen = ConstantesCommons.APLICACION_ADA3;
  }

  /**
   * Gets the pk subsanacion.
   *
   * @return the pkSubsanacion
   */
  public Long getPkSubsanacion() {
    return pkSubsanacion;
  }

  /**
   * Sets the pk subsanacion.
   *
   * @param pkSubsanacion the pkSubsanacion to set
   */
  public void setPkSubsanacion(final Long pkSubsanacion) {
    this.pkSubsanacion = pkSubsanacion;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the derecho pendiente.
   *
   * @return the derechoPendiente
   */
  public Boolean getDerechoPendiente() {
    return derechoPendiente;
  }

  /**
   * Sets the derecho pendiente.
   *
   * @param derechoPendiente the derechoPendiente to set
   */
  public void setDerechoPendiente(final Boolean derechoPendiente) {
    this.derechoPendiente = derechoPendiente;
  }

  /**
   * Gets the fase.
   *
   * @return the fase
   */
  public String getFase() {
    return fase;
  }

  /**
   * Sets the fase.
   *
   * @param fase the fase to set
   */
  public void setFase(final String fase) {
    this.fase = fase;
  }

  /**
   * Gets the fecha comprobante.
   *
   * @return the fechaCompletado
   */
  public Date getFechaCompletado() {
    return UtilidadesCommons.cloneDate(fechaCompletado);
  }

  /**
   * Sets the fecha comprobante.
   *
   * @param fechaComprobante the new fecha completado
   */
  public void setFechaCompletado(final Date fechaComprobante) {
    this.fechaCompletado = UtilidadesCommons.cloneDate(fechaComprobante);
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return observaciones;
  }

  /**
   * Sets the observaciones.
   *
   * @param observaciones the observaciones to set
   */
  public void setObservaciones(final String observaciones) {
    this.observaciones = observaciones;
  }

  /**
   * Gets the perfil subsanador.
   *
   * @return the perfilSubsanador
   */
  public Long getPerfilSubsanador() {
    return perfilSubsanador;
  }

  /**
   * Sets the perfil subsanador.
   *
   * @param perfilSubsanador the perfilSubsanador to set
   */
  public void setPerfilSubsanador(final Long perfilSubsanador) {
    this.perfilSubsanador = perfilSubsanador;
  }

  /**
   * Gets the documento generado.
   *
   * @return the documentoGenerado
   */
  public DocumentoGenerado getDocumentoGenerado() {
    return documentoGenerado;
  }

  /**
   * Sets the documento generado.
   *
   * @param documentoGenerado the documentoGenerado to set
   */
  public void setDocumentoGenerado(final DocumentoGenerado documentoGenerado) {
    this.documentoGenerado = documentoGenerado;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public Solicitud getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the solicitud to set
   */
  public void setSolicitud(final Solicitud solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the usuario to set
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the documentos subsanacion.
   *
   * @return the documentos subsanacion
   */
  public List<DocumentoSubsanacion> getDocumentosSubsanacion() {
    return UtilidadesCommons.collectorsToList(documentosSubsanacion);
  }

  /**
   * Sets the documentos subsanacion.
   *
   * @param documentosSubsanacion the new documentos subsanacion
   */
  public void setDocumentosSubsanacion(
      final List<DocumentoSubsanacion> documentosSubsanacion) {
    this.documentosSubsanacion =
        UtilidadesCommons.collectorsToList(documentosSubsanacion);
  }

  /**
   * Gets the app origen.
   *
   * @return the app origen
   */
  public String getAppOrigen() {
    return appOrigen;
  }

  /**
   * Sets the app origen.
   *
   * @param appOrigen the new app origen
   */
  public void setAppOrigen(final String appOrigen) {
    this.appOrigen = appOrigen;
  }


  /**
   * Gets the documento A subsanar.
   *
   * @return the documento A subsanar
   */
  public DocumentoAportado getDocumentoASubsanar() {
    return documentoASubsanar;
  }

  /**
   * Sets the documento A subsanar.
   *
   * @param documentoASubsanar the new documento A subsanar
   */
  public void setDocumentoASubsanar(
      final DocumentoAportado documentoASubsanar) {
    this.documentoASubsanar = documentoASubsanar;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
