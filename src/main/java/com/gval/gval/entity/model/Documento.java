package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


import jakarta.persistence.*;


/**
 * The Class Documento.
 */
@Entity
@Table(name = "SDM_DOCU")
@GenericGenerator(name = "SDM_DOCU_PKDOCU_GENERATOR",
strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
parameters = {@Parameter(name = "sequence_name", value = "SEC_SDM_DOCU"),
    @Parameter(name = "initial_value", value = "1"),
    @Parameter(name = "increment_size", value = "1")})
public class Documento implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 250519533922785234L;

  /** The pk documento. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
  generator = "SDM_DOCU_PKDOCU_GENERATOR")
  @Column(name = "PK_DOCU", unique = true, nullable = false)
  private Long pkDocumento;

  /** The csv. */
  @Column(name = "CSV", length = 1000)
  private String csv;

  /** The activo. */
  @Column(name = "DOCACTIVO", nullable = false)
  private Boolean activo;

  /** The correos acuse 1. */
  @Column(name = "DOCORREOS_ACUSE1", length = 50)
  private String correosAcuse1;

  /** The correos acuse 2. */
  @Column(name = "DOCORREOS_ACUSE2", length = 50)
  private String correosAcuse2;

  /** The descripcion registro. */
  @Column(name = "DODESCREGI", length = 100)
  private String descripcionRegistro;

  /** The enviado acuse 1. */
  @Column(name = "DOENVIADO_ACUSE1", length = 1)
  private String enviadoAcuse1;

  /** The enviado acuse 2. */
  @Column(name = "DOENVIADO_ACUSE2", length = 1)
  private String enviadoAcuse2;

  /** The fecha acuse 1. */
  @Temporal(TemporalType.DATE)
  @Column(name = "DOFECHACUS_ACUSE1")
  private Date fechaAcuse1;

  /** The fecha acuse 2. */
  @Temporal(TemporalType.DATE)
  @Column(name = "DOFECHACUS_ACUSE2")
  private Date fechaAcuse2;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "DOFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The fecha registro. */
  @Temporal(TemporalType.DATE)
  @Column(name = "DOFECHREGI")
  private Date fechaRegistro;

  /** The fichero. */
  @Lob
  @Column(name = "DOFICHERO")
  private byte[] fichero;

  /** The nombre. */
  @Column(name = "DONOMBRE", length = 250)
  private String nombre;

  /** The nuacre acuse 1. */
  @Column(name = "DONUACRE_ACUSE1", length = 50)
  private String nuacreAcuse1;

  /** The nuacre acuse 2. */
  @Column(name = "DONUACRE_ACUSE2", length = 50)
  private String nuacreAcuse2;

  /** The numero registro. */
  @Column(name = "DONUMREGI", length = 20)
  private String numeroRegistro;

  /** The tipo acuse 1. */
  @Column(name = "DOTIPOACUS_ACUSE1", length = 2)
  private String tipoAcuse1;

  /** The tipo acuse 2. */
  @Column(name = "DOTIPOACUS_ACUSE2", length = 2)
  private String tipoAcuse2;

  /** The uuid. */
  @Column(name = "UUID", length = 100)
  private String uuid;

  /** The clave consulta. */
  @Column(name = "DOCLAVE_CONSULTA", length = 4000)
  private String claveConsulta;

  /** The error sincronizacion. */
  @Column(name = "DOERROR_SINCRO", nullable = false)
  private Boolean errorSincronizacion;

  /** The expediente. */
  // bi-directional many-to-one association to SdmExpedien
  @ManyToOne
  @JoinColumn(name = "PK_EXPEDIEN")
  private Expediente expediente;

  /** The solicitud. */
  // bi-directional many-to-one association to SdmSolicit
  @ManyToOne
  @JoinColumn(name = "PK_SOLICIT")
  private Solicitud solicitud;

  /** The tipo documento. */
  // bi-directional many-to-one association to SdxTipodocu
  @ManyToOne
  @JoinColumn(name = "PK_TIPODOCU", nullable = false)
  private TipoDocumento tipoDocumento;

  /** The unidad. */
  // bi-directional many-to-one association to SdxUnidad
  @ManyToOne
  @JoinColumn(name = "PK_UNIDAD")
  private Unidad unidad;

  /** The fecha publicacion BOE. */
  @Temporal(TemporalType.DATE)
  @Column(name = "DOFECHBOE")
  private Date fechaPublicacionBOE;

  /** The numero BOE. */
  @Column(name = "DONUMBOE")
  private String numeroBOE;

  /** The documento aportado. */
  // bi-directional one-to-one association to SdmDocuapor
  @OneToOne(mappedBy = "documento", cascade = CascadeType.ALL)
  private DocumentoAportado documentoAportado;

  /** The documento generado. */
  // bi-directional one-to-one association to SdmDocugene
  @OneToOne(mappedBy = "documento", cascade = CascadeType.ALL)
  private DocumentoGenerado documentoGenerado;

  /** The estado activo. */
  @OneToMany(mappedBy = "documento")
  private List<HistoricoEstadoDocumento> estados;

  /** The dofechimp. */
  @Temporal(TemporalType.DATE)
  @Column(name = "dofechimp")
  private Date fechaImpresion;

  /** The usuario impresion. */
  // bi-directional many-to-one association to SdxUsuario
  @ManyToOne
  @JoinColumn(name = "pk_persona_imp",
  referencedColumnName = "PK_PERSONA")
  private Usuario usuarioImpresion;

  /**
   * Instantiates a new documento.
   */
  public Documento() {
    super();
  }

  /**
   * Gets the pk documento.
   *
   * @return the pkDocumento
   */
  public Long getPkDocumento() {
    return pkDocumento;
  }

  /**
   * Sets the pk documento.
   *
   * @param pkDocumento the pkDocumento to set
   */
  public void setPkDocumento(final Long pkDocumento) {
    this.pkDocumento = pkDocumento;
  }

  /**
   * Gets the csv.
   *
   * @return the csv
   */
  public String getCsv() {
    return csv;
  }

  /**
   * Sets the csv.
   *
   * @param csv the csv to set
   */
  public void setCsv(final String csv) {
    this.csv = csv;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the correos acuse 1.
   *
   * @return the correosAcuse1
   */
  public String getCorreosAcuse1() {
    return correosAcuse1;
  }

  /**
   * Sets the correos acuse 1.
   *
   * @param correosAcuse1 the correosAcuse1 to set
   */
  public void setCorreosAcuse1(final String correosAcuse1) {
    this.correosAcuse1 = correosAcuse1;
  }

  /**
   * Gets the correos acuse 2.
   *
   * @return the correosAcuse2
   */
  public String getCorreosAcuse2() {
    return correosAcuse2;
  }

  /**
   * Sets the correos acuse 2.
   *
   * @param correosAcuse2 the correosAcuse2 to set
   */
  public void setCorreosAcuse2(final String correosAcuse2) {
    this.correosAcuse2 = correosAcuse2;
  }

  /**
   * Gets the descripcion registro.
   *
   * @return the descripcionRegistro
   */
  public String getDescripcionRegistro() {
    return descripcionRegistro;
  }

  /**
   * Sets the descripcion registro.
   *
   * @param descripcionRegistro the descripcionRegistro to set
   */
  public void setDescripcionRegistro(final String descripcionRegistro) {
    this.descripcionRegistro = descripcionRegistro;
  }

  /**
   * Gets the enviado acuse 1.
   *
   * @return the enviadoAcuse1
   */
  public String getEnviadoAcuse1() {
    return enviadoAcuse1;
  }

  /**
   * Sets the enviado acuse 1.
   *
   * @param enviadoAcuse1 the enviadoAcuse1 to set
   */
  public void setEnviadoAcuse1(final String enviadoAcuse1) {
    this.enviadoAcuse1 = enviadoAcuse1;
  }

  /**
   * Gets the enviado acuse 2.
   *
   * @return the enviadoAcuse2
   */
  public String getEnviadoAcuse2() {
    return enviadoAcuse2;
  }

  /**
   * Sets the enviado acuse 2.
   *
   * @param enviadoAcuse2 the enviadoAcuse2 to set
   */
  public void setEnviadoAcuse2(final String enviadoAcuse2) {
    this.enviadoAcuse2 = enviadoAcuse2;
  }

  /**
   * Gets the fecha acuse 1.
   *
   * @return the fechaAcuse1
   */
  public Date getFechaAcuse1() {
    return UtilidadesCommons.cloneDate(fechaAcuse1);
  }

  /**
   * Sets the fecha acuse 1.
   *
   * @param fechaAcuse1 the fechaAcuse1 to set
   */
  public void setFechaAcuse1(final Date fechaAcuse1) {
    this.fechaAcuse1 = UtilidadesCommons.cloneDate(fechaAcuse1);
  }

  /**
   * Gets the fecha acuse 2.
   *
   * @return the fechaAcuse2
   */
  public Date getFechaAcuse2() {
    return UtilidadesCommons.cloneDate(fechaAcuse2);
  }

  /**
   * Sets the fecha acuse 2.
   *
   * @param fechaAcuse2 the fechaAcuse2 to set
   */
  public void setFechaAcuse2(final Date fechaAcuse2) {
    this.fechaAcuse2 = UtilidadesCommons.cloneDate(fechaAcuse2);
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha registro.
   *
   * @return the fechaRegistro
   */
  public Date getFechaRegistro() {
    return UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Sets the fecha registro.
   *
   * @param fechaRegistro the fechaRegistro to set
   */
  public void setFechaRegistro(final Date fechaRegistro) {
    this.fechaRegistro = UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Gets the fichero.
   *
   * @return the fichero
   */
  public byte[] getFichero() {
    return UtilidadesCommons.cloneBytes(fichero);
  }

  /**
   * Sets the fichero.
   *
   * @param fichero the fichero to set
   */
  public void setFichero(final byte[] fichero) {
    this.fichero = UtilidadesCommons.cloneBytes(fichero);
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the nombre to set
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the nuacre acuse 1.
   *
   * @return the nuacreAcuse1
   */
  public String getNuacreAcuse1() {
    return nuacreAcuse1;
  }

  /**
   * Sets the nuacre acuse 1.
   *
   * @param nuacreAcuse1 the nuacreAcuse1 to set
   */
  public void setNuacreAcuse1(final String nuacreAcuse1) {
    this.nuacreAcuse1 = nuacreAcuse1;
  }

  /**
   * Gets the nuacre acuse 2.
   *
   * @return the nuacreAcuse2
   */
  public String getNuacreAcuse2() {
    return nuacreAcuse2;
  }

  /**
   * Sets the nuacre acuse 2.
   *
   * @param nuacreAcuse2 the nuacreAcuse2 to set
   */
  public void setNuacreAcuse2(final String nuacreAcuse2) {
    this.nuacreAcuse2 = nuacreAcuse2;
  }

  /**
   * Gets the numero registro.
   *
   * @return the numeroRegistro
   */
  public String getNumeroRegistro() {
    return numeroRegistro;
  }

  /**
   * Sets the numero registro.
   *
   * @param numeroRegistro the numeroRegistro to set
   */
  public void setNumeroRegistro(final String numeroRegistro) {
    this.numeroRegistro = numeroRegistro;
  }

  /**
   * Gets the tipo acuse 1.
   *
   * @return the tipoAcuse1
   */
  public String getTipoAcuse1() {
    return tipoAcuse1;
  }

  /**
   * Sets the tipo acuse 1.
   *
   * @param tipoAcuse1 the tipoAcuse1 to set
   */
  public void setTipoAcuse1(final String tipoAcuse1) {
    this.tipoAcuse1 = tipoAcuse1;
  }

  /**
   * Gets the tipo acuse 2.
   *
   * @return the tipoAcuse2
   */
  public String getTipoAcuse2() {
    return tipoAcuse2;
  }

  /**
   * Sets the tipo acuse 2.
   *
   * @param tipoAcuse2 the tipoAcuse2 to set
   */
  public void setTipoAcuse2(final String tipoAcuse2) {
    this.tipoAcuse2 = tipoAcuse2;
  }

  /**
   * Gets the expediente.
   *
   * @return the expediente
   */
  public Expediente getExpediente() {
    return expediente;
  }

  /**
   * Sets the expediente.
   *
   * @param expediente the expediente to set
   */
  public void setExpediente(final Expediente expediente) {
    this.expediente = expediente;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public Solicitud getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the solicitud to set
   */
  public void setSolicitud(final Solicitud solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the tipo documento.
   *
   * @return the tipoDocumento
   */
  public TipoDocumento getTipoDocumento() {
    return tipoDocumento;
  }

  /**
   * Sets the tipo documento.
   *
   * @param tipoDocumento the tipoDocumento to set
   */
  public void setTipoDocumento(final TipoDocumento tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }

  /**
   * Gets the unidad.
   *
   * @return the unidad
   */
  public Unidad getUnidad() {
    return unidad;
  }

  /**
   * Sets the unidad.
   *
   * @param unidad the unidad to set
   */
  public void setUnidad(final Unidad unidad) {
    this.unidad = unidad;
  }

  /**
   * Gets the documento aportado.
   *
   * @return the documentoAportado
   */
  public DocumentoAportado getDocumentoAportado() {
    return documentoAportado;
  }

  /**
   * Sets the documento aportado.
   *
   * @param documentoAportado the documentoAportado to set
   */
  public void setDocumentoAportado(final DocumentoAportado documentoAportado) {
    this.documentoAportado = documentoAportado;
  }

  /**
   * Gets the documento generado.
   *
   * @return the documentoGenerado
   */
  public DocumentoGenerado getDocumentoGenerado() {
    return documentoGenerado;
  }

  /**
   * Sets the documento generado.
   *
   * @param documentoGenerado the documentoGenerado to set
   */
  public void setDocumentoGenerado(final DocumentoGenerado documentoGenerado) {
    this.documentoGenerado = documentoGenerado;
  }

  /**
   * Gets the uuid.
   *
   * @return the uuid
   */
  public String getUuid() {
    return uuid;
  }

  /**
   * Sets the uuid.
   *
   * @param uuid the new uuid
   */
  public void setUuid(final String uuid) {
    this.uuid = uuid;
  }

  /**
   * Gets the clave consulta.
   *
   * @return the clave consulta
   */
  public String getClaveConsulta() {
    return claveConsulta;
  }

  /**
   * Sets the clave consulta.
   *
   * @param claveConsulta the new clave consulta
   */
  public void setClaveConsulta(final String claveConsulta) {
    this.claveConsulta = claveConsulta;
  }

  /**
   * Gets the error sincronizacion.
   *
   * @return the error sincronizacion
   */
  public Boolean getErrorSincronizacion() {
    return errorSincronizacion;
  }

  /**
   * Sets the error sincronizacion.
   *
   * @param errorSincronizacion the new error sincronizacion
   */
  public void setErrorSincronizacion(final Boolean errorSincronizacion) {
    this.errorSincronizacion = errorSincronizacion;
  }

  /**
   * Gets the estados.
   *
   * @return the estados
   */
  public List<HistoricoEstadoDocumento> getEstados() {
    return UtilidadesCommons.collectorsToList(estados);
  }

  /**
   * Sets the estados.
   *
   * @param estados the new estados
   */
  public void setEstados(final List<HistoricoEstadoDocumento> estados) {
    this.estados = UtilidadesCommons.collectorsToList(estados);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }


  /**
   * Gets the fecha publicacion BOE.
   *
   * @return the fecha publicacion BOE
   */
  public Date getFechaPublicacionBOE() {
    return UtilidadesCommons.cloneDate(fechaPublicacionBOE);
  }

  /**
   * Sets the fecha publicacion BOE.
   *
   * @param fechaPublicacionBOE the new fecha publicacion BOE
   */
  public void setFechaPublicacionBOE(final Date fechaPublicacionBOE) {
    this.fechaPublicacionBOE = UtilidadesCommons.cloneDate(fechaPublicacionBOE);
  }

  /**
   * Gets the numero BOE.
   *
   * @return the numero BOE
   */
  public String getNumeroBOE() {
    return numeroBOE;
  }

  /**
   * Sets the numero BOE.
   *
   * @param numeroBOE the new numero BOE
   */
  public void setNumeroBOE(final String numeroBOE) {
    this.numeroBOE = numeroBOE;
  }

  /**
   * Gets the fecha impresion.
   *
   * @return the fecha impresion
   */
  public Date getFechaImpresion() {
    return UtilidadesCommons.cloneDate(fechaImpresion);
  }

  /**
   * Sets the fecha impresion.
   *
   * @param fechaImpresion the new fecha impresion
   */
  public void setFechaImpresion(final Date fechaImpresion) {
    this.fechaImpresion = UtilidadesCommons.cloneDate(fechaImpresion);
  }

  /**
   * Gets the usuario impresion.
   *
   * @return the usuario impresion
   */
  public Usuario getUsuarioImpresion() {
    return usuarioImpresion;
  }

  /**
   * Sets the usuario impresion.
   *
   * @param usuarioImpresion the new usuario impresion
   */
  public void setUsuarioImpresion(final Usuario usuarioImpresion) {
    this.usuarioImpresion = usuarioImpresion;
  }

}
