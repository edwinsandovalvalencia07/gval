package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The Class CambiosVersion.
 */
@Entity
@Table(name = "SDM_CAMBIOS_VERSION")
public class CambioVersion implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 5756022428327460188L;

  /** The pk version. */
  @Id
  @Column(name = "PK_CAMBIO_VERSION", unique = true, nullable = false)
  private Long pkCambioVersion;

  /** The entrada. */
  @Column(name = "CVENT")
  private String entrada;

  /** The titulo. */
  @Column(name = "CVTITULO", nullable = false)
  private String titulo;

  /** The descripcion. */
  @Column(name = "CVDESCRIPCION")
  private String descripcion;

  /** The fecha crea. */
  @Temporal(TemporalType.DATE)
  @Column(name = "CVFECHCREA", nullable = false)
  private Date fechaCrea;

  /** The activo. */
  @Column(name = "CVACTIVO", nullable = false)
  private Boolean activo;

  /** The es correccion. */
  @Column(name = "CVTIPO", nullable = false)
  private String tipo;

  /** The version ada. */
  @ManyToOne
  @JoinColumn(name = "PK_VERSION_ADA", nullable = false)
  private VersionAda versionAda;


  /**
   * Instantiates a new version ada.
   */
  public CambioVersion() {
    // Empty constructor
  }

  /**
   * Gets the pk mejora version.
   *
   * @return the pk mejora version
   */
  public Long getPkMejoraVersion() {
    return this.pkCambioVersion;
  }

  /**
   * Sets the pk mejora version.
   *
   * @param pkMejoraVersion the new pk mejora version
   */
  public void setPkMejoraVersion(final Long pkMejoraVersion) {
    this.pkCambioVersion = pkMejoraVersion;
  }

  /**
   * Gets the entrada.
   *
   * @return the entrada
   */
  public String getEntrada() {
    return this.entrada;
  }

  /**
   * Sets the entrada.
   *
   * @param entrada the new entrada
   */
  public void setEntrada(final String entrada) {
    this.entrada = entrada;
  }

  /**
   * Gets the titulo.
   *
   * @return the titulo
   */
  public String getTitulo() {
    return this.titulo;
  }

  /**
   * Sets the titulo.
   *
   * @param titulo the new titulo
   */
  public void setTitulo(final String titulo) {
    this.titulo = titulo;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return this.descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the fecha crea.
   *
   * @return the fecha crea
   */
  public Date getFechaCrea() {
    return UtilidadesCommons.cloneDate(this.fechaCrea);
  }

  /**
   * Sets the fecha crea.
   *
   * @param fechaCrea the new fecha crea
   */
  public void setFechaCrea(final Date fechaCrea) {
    this.fechaCrea = UtilidadesCommons.cloneDate(fechaCrea);
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return this.activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the tipo.
   *
   * @return the tipo
   */
  public String getTipo() {
    return tipo;
  }

  /**
   * Sets the tipo.
   *
   * @param tipo the new tipo
   */
  public void setTipo(final String tipo) {
    this.tipo = tipo;
  }

  /**
   * Gets the version ada.
   *
   * @return the version ada
   */
  public VersionAda getVersionAda() {
    return this.versionAda;
  }

  /**
   * Sets the version ada.
   *
   * @param versionAda the new version ada
   */
  public void setVersionAda(final VersionAda versionAda) {
    this.versionAda = versionAda;
  }

}
