package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

import jakarta.persistence.*;


/**
 * The persistent class for the SDX_SITLABORA database table.
 *
 */
@Entity
@Table(name = "SDX_SITLABORA")
public class SituacionLaboral implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 8402296599638457807L;

  /** The pk situacionlaboral. */
  @Id
  @Column(name = "PK_SITLABORA", unique = true, nullable = false)
  private long pkSituacionlaboral;

  /** The descripcion. */
  @Column(name = "SIDESC", nullable = false, length = 40)
  private String descripcion;



  /**
   * Instantiates a new situacion laboral.
   */
  public SituacionLaboral() {
    // Empty constructor
  }



  /**
   * Gets the pk situacionlaboral.
   *
   * @return the pk situacionlaboral
   */
  public long getPkSituacionlaboral() {
    return pkSituacionlaboral;
  }



  /**
   * Sets the pk situacionlaboral.
   *
   * @param pkSituacionlaboral the new pk situacionlaboral
   */
  public void setPkSituacionlaboral(final long pkSituacionlaboral) {
    this.pkSituacionlaboral = pkSituacionlaboral;
  }



  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }



  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
