package com.gval.gval.entity.model;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "SDX_DIAGNOST")
@GenericGenerator(name = "SDX_DIAGNOST_PKDIAGNOST_GENERATOR", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
		@Parameter(name = "sequence_name", value = "SEC_SDX_DIAGNOST"), @Parameter(name = "initial_value", value = "1"),
		@Parameter(name = "increment_size", value = "1") })
public class Diagnostico implements Serializable {

	private static final long serialVersionUID = -4825223146071638877L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SDX_DIAGNOST_PKDIAGNOST_GENERATOR")
	@Column(name = "PK_DIAGNOST", unique = true, nullable = false)
	private Long pkDiagnost;

	@Column(name = "DIACTIVO", length = 22)
	private Long diactivo;

	@Column(name = "DICODIAPAR", length = 3)
	private String dicodiapar;

	@Column(name = "DICODIBLOQ", length = 7)
	private String dicodibloq;

	@Column(name = "DICODICAPI", length = 22)
	private Long dicodicapi;

	@Column(name = "DICODIDIAG", length = 5)
	private String dicodidiag;

	@Column(name = "DIDESCAPAR", length = 250)
	private String didescapar;

	@Column(name = "DIDESCBLOQ", length = 250)
	private String didescbloq;

	@Column(name = "DIDESCCAPI", length = 250)
	private String didesccapi;

	@Column(name = "DIDESCDIAG", length = 250)
	private String didescdiag;

	/** The fecha revisable. */
	@Temporal(TemporalType.DATE)
	@Column(name = "DIFECHCREA")
	private Date difechcrea;

	public Diagnostico() {
		super();
	}

	public Long getPkDiagnost() {
		return pkDiagnost;
	}

	public void setPkDiagnost(Long pkDiagnost) {
		this.pkDiagnost = pkDiagnost;
	}

	public Long getDiactivo() {
		return diactivo;
	}

	public void setDiactivo(Long diactivo) {
		this.diactivo = diactivo;
	}

	public String getDicodiapar() {
		return dicodiapar;
	}

	public void setDicodiapar(String dicodiapar) {
		this.dicodiapar = dicodiapar;
	}

	public String getDicodibloq() {
		return dicodibloq;
	}

	public void setDicodibloq(String dicodibloq) {
		this.dicodibloq = dicodibloq;
	}

	public Long getDicodicapi() {
		return dicodicapi;
	}

	public void setDicodicapi(Long dicodicapi) {
		this.dicodicapi = dicodicapi;
	}

	public String getDicodidiag() {
		return dicodidiag;
	}

	public void setDicodidiag(String dicodidiag) {
		this.dicodidiag = dicodidiag;
	}

	public String getDidescapar() {
		return didescapar;
	}

	public void setDidescapar(String didescapar) {
		this.didescapar = didescapar;
	}

	public String getDidescbloq() {
		return didescbloq;
	}

	public void setDidescbloq(String didescbloq) {
		this.didescbloq = didescbloq;
	}

	public String getDidesccapi() {
		return didesccapi;
	}

	public void setDidesccapi(String didesccapi) {
		this.didesccapi = didesccapi;
	}

	public String getDidescdiag() {
		return didescdiag;
	}

	public void setDidescdiag(String didescdiag) {
		this.didescdiag = didescdiag;
	}

	public Date getDifechcrea() {
		return difechcrea;
	}

	public void setDifechcrea(Date difechcrea) {
		this.difechcrea = difechcrea;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

}
