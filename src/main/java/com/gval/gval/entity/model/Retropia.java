package com.gval.gval.entity.model;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;

import jakarta.persistence.*;

// TODO: Auto-generated Javadoc
/**
 * The Class Retropia.
 */
@Entity
@Table(name = "SDM_RETROPIA")
@GenericGenerator(name = "SDM_RETROPIA_PK_GENERATOR", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {@Parameter(name = "sequence_name", value = "SEC_SDM_RETROPIA"), @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1") })
public class Retropia  implements Serializable{

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 8762961499254128399L;

    /** The pk retropia. */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SDM_RETROPIA_PK_GENERATOR")
    @Column(name = "PK_RETROPIA", nullable = false)
    private Long pkRetropia;

    /** The sdm comision. */
    @ManyToOne
    @JoinColumn(name = "PK_PIA", nullable = false)
    private Pia pia;

    /**
     * Instantiates a new acta.
     */
    public Retropia() {}

    /**
     * Gets the pk retropia.
     *
     * @return the pk retropia
     */
    public Long getPkRetropia() {
      return pkRetropia;
    }

    /**
     * Sets the pk retropia.
     *
     * @param pkRetropia the new pk retropia
     */
    public void setPkRetropia(final Long pkRetropia) {
      this.pkRetropia = pkRetropia;
    }

    /**
     * Gets the pia.
     *
     * @return the pia
     */
    public Pia getPia() {
      return pia;
    }

    /**
     * Sets the pia.
     *
     * @param pia the new pia
     */
    public void setPia(final Pia pia) {
      this.pia = pia;
    }



}
