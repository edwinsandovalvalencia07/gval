package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.math.BigDecimal;

import jakarta.persistence.*;


/**
 * The persistent class for the SDM_PRESCOMP database table.
 *
 */
@Entity
@Table(name = "SDM_PRESCOMP")
@GenericGenerator(name = "SDM_PRESCOMP_PKSDM_CONVIVEN_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_PRESCOMP"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public final class PrestacionInformeSocial implements Serializable {


  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -2557131330626318897L;

  /** The pk prescomp. */
  @Id
  @Column(name = "PK_PRESCOMP", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_PRESCOMP_PKSDM_CONVIVEN_GENERATOR")
  private Long pkPrescomp;

  /** The aportacion. */
  @Column(name = "PRAPORTACI", precision = 8, scale = 2)
  private BigDecimal aportacion;

  /** The descripcion detallada. */
  @Column(name = "PRDESCDETA", length = 4000)
  private String descripcionDetallada;

  /** The descripcion. */
  @Column(name = "PRDESCRIP", length = 250)
  private String descripcion;

  /** The horas semanales. */
  @Column(name = "PRHORASSEM", length = 250)
  private String horasSemanales;

  /** The titularidad. */
  @Column(name = "PRTITULARI", length = 250)
  private String titularidad;

  /** The informe social. */
  // bi-directional many-to-one association to SdmInfosoci
  @ManyToOne
  @JoinColumn(name = "PK_INFOSOCI", nullable = false)
  private InformeSocial informeSocial;

  /**
   * Instantiates a new prestacion informe social.
   */
  public PrestacionInformeSocial() {
    // Constructor vacío
  }

  /**
   * Gets the pk prescomp.
   *
   * @return the pkPrescomp
   */
  public Long getPkPrescomp() {
    return pkPrescomp;
  }

  /**
   * Sets the pk prescomp.
   *
   * @param pkPrescomp the pkPrescomp to set
   */
  public void setPkPrescomp(final Long pkPrescomp) {
    this.pkPrescomp = pkPrescomp;
  }

  /**
   * Gets the aportacion.
   *
   * @return the aportacion
   */
  public BigDecimal getAportacion() {
    return aportacion;
  }

  /**
   * Sets the aportacion.
   *
   * @param aportacion the aportacion to set
   */
  public void setAportacion(final BigDecimal aportacion) {
    this.aportacion = aportacion;
  }

  /**
   * Gets the descripcion detallada.
   *
   * @return the descripcionDetallada
   */
  public String getDescripcionDetallada() {
    return descripcionDetallada;
  }

  /**
   * Sets the descripcion detallada.
   *
   * @param descripcionDetallada the descripcionDetallada to set
   */
  public void setDescripcionDetallada(final String descripcionDetallada) {
    this.descripcionDetallada = descripcionDetallada;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the descripcion to set
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the horas semanales.
   *
   * @return the horasSemanales
   */
  public String getHorasSemanales() {
    return horasSemanales;
  }

  /**
   * Sets the horas semanales.
   *
   * @param horasSemanales the horasSemanales to set
   */
  public void setHorasSemanales(final String horasSemanales) {
    this.horasSemanales = horasSemanales;
  }

  /**
   * Gets the titularidad.
   *
   * @return the titularidad
   */
  public String getTitularidad() {
    return titularidad;
  }

  /**
   * Sets the titularidad.
   *
   * @param titularidad the titularidad to set
   */
  public void setTitularidad(final String titularidad) {
    this.titularidad = titularidad;
  }

  /**
   * Gets the informe social.
   *
   * @return the informeSocial
   */
  public InformeSocial getInformeSocial() {
    return informeSocial;
  }

  /**
   * Sets the informe social.
   *
   * @param informeSocial the informeSocial to set
   */
  public void setInformeSocial(final InformeSocial informeSocial) {
    this.informeSocial = informeSocial;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
