package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;



/**
 * The persistent class for the SDM_CNP_INFOSOCI database table.
 *
 */
@Entity
@Table(name = "SDM_CNP_INFOSOCI")
@GenericGenerator(name = "SDM_CNP_INFOSOCI_PKCNP_INFOSOCI_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_CNP_INFOSOCI"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public final class CuidadorInformeSocial implements Serializable {


  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -3078741686182604131L;

  /** The pk cnp infosoci. */
  @Id
  @Column(name = "PK_CNP_INFOSOCI", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_CNP_INFOSOCI_PKCNP_INFOSOCI_GENERATOR")
  private Long pkCnpInfosoci;

  /** The activo. */
  @Column(name = "CNACTIVO", nullable = false)
  private Boolean activo;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "CNFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The idoneo. */
  @Column(name = "CNIDONEO", nullable = false)
  private Boolean idoneo;

  /** The motivo. */
  @Column(name = "CNMOTIVO", length = 250, nullable = false)
  private String motivo;

  /** The verificado. */
  @Column(name = "CNVERIFICADO", nullable = false)
  private Boolean verificado;

  /** The origen. */
  @Column(name = "CNORIGEN")
  private String origen;

  /** The cuidador. */
  // bi-directional many-to-one association to SdmCnp
  @ManyToOne
  @JoinColumn(name = "PK_CNP", nullable = false)
  private Cuidador cuidador;

  /** The informe social. */
  // bi-directional many-to-one association to SdmInfosoci
  @ManyToOne
  @JoinColumn(name = "PK_INFOSOCI", nullable = false)
  private InformeSocial informeSocial;

  /** The verificado. */
  @Column(name = "CNASOCIADOIS", nullable = false)
  private Boolean asociadoIS;

  /**
   * Instantiates a new cuidador informe social.
   */
  public CuidadorInformeSocial() {
    // Constructor vacío
  }

  /**
   * Gets the pk cnp infosoci.
   *
   * @return the pkCnpInfosoci
   */
  public Long getPkCnpInfosoci() {
    return pkCnpInfosoci;
  }

  /**
   * Sets the pk cnp infosoci.
   *
   * @param pkCnpInfosoci the pkCnpInfosoci to set
   */
  public void setPkCnpInfosoci(final Long pkCnpInfosoci) {
    this.pkCnpInfosoci = pkCnpInfosoci;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the idoneo.
   *
   * @return the idoneo
   */
  public Boolean getIdoneo() {
    return idoneo;
  }

  /**
   * Sets the idoneo.
   *
   * @param idoneo the idoneo to set
   */
  public void setIdoneo(final Boolean idoneo) {
    this.idoneo = idoneo;
  }

  /**
   * Gets the motivo.
   *
   * @return the motivo
   */
  public String getMotivo() {
    return motivo;
  }

  /**
   * Sets the motivo.
   *
   * @param motivo the new motivo
   */
  public void setMotivo(final String motivo) {
    this.motivo = motivo;
  }

  /**
   * Gets the verificado.
   *
   * @return the verificado
   */
  public Boolean getVerificado() {
    return verificado;
  }

  /**
   * Sets the verificado.
   *
   * @param verificado the new verificado
   */
  public void setVerificado(final Boolean verificado) {
    this.verificado = verificado;
  }

  /**
   * Gets the cuidador.
   *
   * @return the cuidador
   */
  public Cuidador getCuidador() {
    return cuidador;
  }

  /**
   * Sets the cuidador.
   *
   * @param cuidador the cuidador to set
   */
  public void setCuidador(final Cuidador cuidador) {
    this.cuidador = cuidador;
  }

  /**
   * Gets the informe social.
   *
   * @return the informeSocial
   */
  public InformeSocial getInformeSocial() {
    return informeSocial;
  }

  /**
   * Sets the informe social.
   *
   * @param informeSocial the informeSocial to set
   */
  public void setInformeSocial(final InformeSocial informeSocial) {
    this.informeSocial = informeSocial;
  }

  /**
   * Gets the origen.
   *
   * @return the origen
   */
  public String getOrigen() {
    return origen;
  }

  /**
   * Sets the origen.
   *
   * @param origen the new origen
   */
  public void setOrigen(final String origen) {
    this.origen = origen;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }


  /**
   * Gets the asociado IS.
   *
   * @return the asociado IS
   */
  public Boolean getAsociadoIS() {
    return asociadoIS;
  }

  /**
   * Sets the asociado IS.
   *
   * @param asociadoIS the new asociado IS
   */
  public void setAsociadoIS(final Boolean asociadoIS) {
    this.asociadoIS = asociadoIS;
  }
}
