package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Where;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import jakarta.persistence.*;

/**
 * The Class VersionAda.
 */
@Entity
@Table(name = "SDM_VERSION_ADA")
@GenericGenerator(name = "SDM_VERSION_ADA_PKVERSIONADA_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_VERSION_ADA"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class VersionAda implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 2289904273369602776L;

  /** The pk version. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_VERSION_ADA_PKVERSIONADA_GENERATOR")
  @Column(name = "PK_VERSION_ADA", unique = true, nullable = false)
  private Long pkVersion;

  /** The fecha version. */
  @Temporal(TemporalType.DATE)
  @Column(name = "VAFECHVERSION", nullable = false)
  private Date fechaVersion;

  /** The fecha crea. */
  @Temporal(TemporalType.DATE)
  @Column(name = "VAFECHCREA", nullable = false)
  private Date fechaCrea;

  /** The num version. */
  @Column(name = "VANUMVERSION", length = 20, nullable = false)
  private String numVersion;

  /** The activo. */
  @Column(name = "VAACTIVO", nullable = false)
  private Boolean activo;

  /** The versiones ada. */
  @OneToMany(mappedBy = "versionAda")
  @Where(clause = "CVACTIVO = 1")
  private List<CambioVersion> cambiosVersion;

  /**
   * Instantiates a new version ada.
   */
  public VersionAda() {
    // Empty constructor
  }

  /**
   * Gets the pk version.
   *
   * @return the pk version
   */
  public Long getPkVersion() {
    return this.pkVersion;
  }

  /**
   * Sets the pk version.
   *
   * @param pkVersion the new pk version
   */
  public void setPkVersion(final Long pkVersion) {
    this.pkVersion = pkVersion;
  }

  /**
   * Gets the fecha version.
   *
   * @return the fecha version
   */
  public Date getFechaVersion() {
    return UtilidadesCommons.cloneDate(this.fechaVersion);
  }

  /**
   * Sets the fecha version.
   *
   * @param fechaVersion the new fecha version
   */
  public void setFechaVersion(final Date fechaVersion) {
    this.fechaVersion = UtilidadesCommons.cloneDate(fechaVersion);
  }

  /**
   * Gets the fecha crea.
   *
   * @return the fecha crea
   */
  public Date getFechaCrea() {
    return UtilidadesCommons.cloneDate(this.fechaCrea);
  }

  /**
   * Sets the fecha crea.
   *
   * @param fechaCrea the new fecha crea
   */
  public void setFechaCrea(final Date fechaCrea) {
    this.fechaCrea = UtilidadesCommons.cloneDate(fechaCrea);
  }

  /**
   * Gets the num version.
   *
   * @return the num version
   */
  public String getNumVersion() {
    return this.numVersion;
  }

  /**
   * Sets the num version.
   *
   * @param numVersion the new num version
   */
  public void setNumVersion(final String numVersion) {
    this.numVersion = numVersion;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return this.activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }


  /**
   * Gets the cambios version.
   *
   * @return the cambios version
   */
  public List<CambioVersion> getCambiosVersion() {
    return UtilidadesCommons.collectorsToList(this.cambiosVersion);
  }

  /**
   * Sets the cambios version.
   *
   * @param cambiosVersion the new cambios version
   */
  public void setCambiosVersion(final List<CambioVersion> cambiosVersion) {
    this.cambiosVersion = UtilidadesCommons.collectorsToList(cambiosVersion);
  }
}
