package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.math.BigDecimal;

import jakarta.persistence.*;


/**
 * The persistent class for the SDX_VARIECON database table.
 *
 */
@Entity
@Table(name = "SDX_VARIECON")
@GenericGenerator(name = "SDX_VARIECON_PKVARIECON_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDX_VARIECON"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class VariableEconomica implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -4796707753241706022L;

  /** The pk variecon. */
  @Id
  @Column(name = "PK_VARIECON", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDX_VARIECON_PKVARIECON_GENERATOR")
  private Long pkVariecon;

  /** The codigo. */
  @Column(name = "VACODIGO", length = 15)
  private String codigo;

  /** The descripcion. */
  @Column(name = "VADESC", length = 100)
  private String descripcion;

  /** The valor. */
  @Column(name = "VAVALO", precision = 12, scale = 4)
  private BigDecimal valor;

  /** The ejercicio. */
  // bi-directional many-to-one association to Ejercicio
  @ManyToOne
  @JoinColumn(name = "CLAVE")
  private Ejercicio ejercicio;

  /**
   * Instantiates a new variable economica.
   */
  public VariableEconomica() {
    // Constructor
  }

  /**
   * Gets the pk variecon.
   *
   * @return the pkVariecon
   */
  public Long getPkVariecon() {
    return pkVariecon;
  }

  /**
   * Sets the pk variecon.
   *
   * @param pkVariecon the pkVariecon to set
   */
  public void setPkVariecon(final Long pkVariecon) {
    this.pkVariecon = pkVariecon;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the codigo to set
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the descripcion to set
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public BigDecimal getValor() {
    return valor;
  }

  /**
   * Sets the valor.
   *
   * @param valor the valor to set
   */
  public void setValor(final BigDecimal valor) {
    this.valor = valor;
  }

  /**
   * Gets the ejercicio.
   *
   * @return the ejercicio
   */
  public Ejercicio getEjercicio() {
    return ejercicio;
  }

  /**
   * Sets the ejercicio.
   *
   * @param ejercicio the ejercicio to set
   */
  public void setEjercicio(final Ejercicio ejercicio) {
    this.ejercicio = ejercicio;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
