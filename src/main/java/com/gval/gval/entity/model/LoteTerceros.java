package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


import jakarta.persistence.*;

/**
 * The persistent class for the SDM_LOTE database table.
 *
 */

@Entity
@Table(name = "SDM_LOTE")
@GenericGenerator(name = "SDM_LOTE_PKLOTE_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {@Parameter(name = "sequence_name", value = "SEC_SDM_LOTE"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})

public class LoteTerceros implements Serializable {

  /** serial version UID. */
  private static final long serialVersionUID = -9066520218383265129L;

  /** the pk lote. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_LOTE_PKLOTE_GENERATOR")
  /**
   *
   */
  @Column(name = "PK_LOTE", unique = true, nullable = false)
  private long pkLote;

  /** The codigo. */
  @Column(name = "LOCODIGO", nullable = false, length = 20)
  private String codigo;

  /** The fecha envio. */
  @Column(name = "lofechenvi", nullable = false, length = 20)
  @Temporal(TemporalType.DATE)
  private Date fechaEnvio;

  /** The fecha respuesta. */
  @Column(name = "lofechresp", nullable = false, length = 20)
  @Temporal(TemporalType.DATE)
  private Date fechaRespuesta;

  /** The terceros. */
  // bi-directional many-to-one association to SdmTercero
  @OneToMany(mappedBy = "loteTerceros")
  private List<Tercero> terceros;

  /**
   * Gets the pk lote.
   *
   * @return the pkLote
   */
  public long getPkLote() {
    return pkLote;
  }

  /**
   * Sets the pk lote.
   *
   * @param pkLote the pkLote to set
   */
  public void setPkLote(long pkLote) {
    this.pkLote = pkLote;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the codigo to set
   */
  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the fecha envio.
   *
   * @return the fechaEnvio
   */
  public Date getFechaEnvio() {
    return UtilidadesCommons.cloneDate(fechaEnvio);
  }

  /**
   * Sets the fecha envio.
   *
   * @param fechaEnvio the fechaEnvio to set
   */
  public void setFechaEnvio(Date fechaEnvio) {
    this.fechaEnvio = UtilidadesCommons.cloneDate(fechaEnvio);
  }

  /**
   * Gets the fecha respuesta.
   *
   * @return the fechaRespuesta
   */
  public Date getFechaRespuesta() {
    return UtilidadesCommons.cloneDate(fechaRespuesta);
  }

  /**
   * Sets the fecha respuesta.
   *
   * @param fechaRespuesta the fechaRespuesta to set
   */
  public void setFechaRespuesta(Date fechaRespuesta) {
    this.fechaRespuesta = UtilidadesCommons.cloneDate(fechaRespuesta);
  }

  /**
   * Gets the terceros.
   *
   * @return the terceros
   */
  public List<Tercero> getTerceros() {
    return UtilidadesCommons.collectorsToList(terceros);
  }

  /**
   * Sets the terceros.
   *
   * @param terceros the terceros to set
   */
  public void setTerceros(List<Tercero> terceros) {
    this.terceros = UtilidadesCommons.collectorsToList(terceros);
  }

}
