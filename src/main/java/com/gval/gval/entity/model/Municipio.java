package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The persistent class for the SDX_MUNICIPI database table.
 *
 */
@Entity
@Table(name = "SDX_MUNICIPI")
public class Municipio implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -7700670371812031492L;


  /** The pk municipio. */
  @Id
  @Column(name = "PK_MUNICIPI", unique = true, nullable = false)
  private Long pkMunicipio;

  /** The activo. */
  @Column(name = "MUACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "MUFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The nombre. */
  @Column(name = "MUNOMBRE", nullable = false, length = 50)
  private String nombre;

  /** The codigo municipio. */
  @Column(name = "COD_MUNI")
  private Long codigoMunicipio;

  /** The codigo provincia. */
  @Column(name = "COD_PROV")
  private Long codigoProvincia;


  /** The provincia. */
  // bi-directional many-to-one association to SdxProvinci
  @ManyToOne
  @JoinColumn(name = "PK_PROVINCI", nullable = false)
  private Provincia provincia;

  /** The zona cobertura. */
  // bi-directional many-to-one association to SdxZonacobe
  @ManyToOne
  @JoinColumn(name = "PK_ZONACOBE", nullable = false)
  private ZonaCobertura zonaCobertura;

  /** The comarca. */
  // bi-directional many-to-one association to SdxComarca
  @ManyToOne
  @JoinColumn(name = "PK_COMARCA")
  private Comarca comarca;

  /**
   * Instantiates a new municipio.
   */
  public Municipio() {
    // Empty constructor
  }


  /**
   * Instantiates a new municipio.
   *
   * @param pkMunicipio the pk municipio
   * @param activo the activo
   * @param fechaCreacion the fecha creacion
   * @param nombre the nombre
   * @param codigoMunicipio the codigo municipio
   * @param codigoProvincia the codigo provincia
   * @param provincia the provincia
   * @param zonaCobertura the zona cobertura
   * @param comarca the comarca
   */
  public Municipio(final Long pkMunicipio, final Boolean activo,
      final Date fechaCreacion, final String nombre, final Long codigoMunicipio,
      final Long codigoProvincia, final Provincia provincia,
      final ZonaCobertura zonaCobertura, final Comarca comarca) {
    super();
    this.pkMunicipio = pkMunicipio;
    this.activo = activo;
    this.fechaCreacion = fechaCreacion;
    this.nombre = nombre;
    this.codigoMunicipio = codigoMunicipio;
    this.codigoProvincia = codigoProvincia;
    this.provincia = provincia;
    this.zonaCobertura = zonaCobertura;
    this.comarca = comarca;
  }

  /**
   * Gets the pk municipio.
   *
   * @return the pk municipio
   */
  public Long getPkMunicipio() {
    return pkMunicipio;
  }

  /**
   * Sets the pk municipio.
   *
   * @param pkMunicipio the new pk municipio
   */
  public void setPkMunicipio(final Long pkMunicipio) {
    this.pkMunicipio = pkMunicipio;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return fechaCreacion;
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the provincia.
   *
   * @return the provincia
   */
  public Provincia getProvincia() {
    return provincia;
  }

  /**
   * Sets the provincia.
   *
   * @param provincia the new provincia
   */
  public void setProvincia(final Provincia provincia) {
    this.provincia = provincia;
  }

  /**
   * Gets the zona cobertura.
   *
   * @return the zona cobertura
   */
  public ZonaCobertura getZonaCobertura() {
    return zonaCobertura;
  }

  /**
   * Sets the zona cobertura.
   *
   * @param zonaCobertura the new zona cobertura
   */
  public void setZonaCobertura(final ZonaCobertura zonaCobertura) {
    this.zonaCobertura = zonaCobertura;
  }

  /**
   * Gets the codigo municipio.
   *
   * @return the codigo municipio
   */
  public Long getCodigoMunicipio() {
    return codigoMunicipio;
  }

  /**
   * Sets the codigo municipio.
   *
   * @param codigoMunicipio the new codigo municipio
   */
  public void setCodigoMunicipio(final Long codigoMunicipio) {
    this.codigoMunicipio = codigoMunicipio;
  }

  /**
   * Gets the codigo provincia.
   *
   * @return the codigo provincia
   */
  public Long getCodigoProvincia() {
    return codigoProvincia;
  }

  /**
   * Sets the codigo provincia.
   *
   * @param codigoProvincia the new codigo provincia
   */
  public void setCodigoProvincia(final Long codigoProvincia) {
    this.codigoProvincia = codigoProvincia;
  }

  /**
   * Gets the comarca.
   *
   * @return the comarca
   */
  public Comarca getComarca() {
    return comarca;
  }

  /**
   * Sets the comarca.
   *
   * @param comarca the new comarca
   */
  public void setComarca(final Comarca comarca) {
    this.comarca = comarca;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
