package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The persistent class for the SDM_MOTGENERACION_IS database table.
 *
 */
@Entity
@Table(name = "SDM_MOTGENERACION_IS")
@GenericGenerator(name = "SDM_MOTGENERACION_IS_PKMOTGENERACION_IS_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_MOTGENERACION_IS"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public final class MotivoGeneracionInformeSocial implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -8372358734177631063L;

  /** The pk motgeneracion is. */
  @Id
  @Column(name = "PK_MOTGENERACION_IS", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_MOTGENERACION_IS_PKMOTGENERACION_IS_GENERATOR")
  private Long pkMotgeneracionIs;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "MOTFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The observaciones. */
  @Column(name = "MOTOBSERVACIONES", length = 300)
  private String observaciones;

  /** The informe social. */
  // bi-directional many-to-one association to SdmInfosoci
  @ManyToOne
  @JoinColumn(name = "PK_INFOSOCI", nullable = false)
  private InformeSocial informeSocial;

  /** The motivo generacion. */
  // bi-directional many-to-one association to MotivoGeneracion
  @ManyToOne
  @JoinColumn(name = "PK_MOTGENERACION", nullable = false)
  private MotivoGeneracion motivoGeneracion;

  /** The usuario. */
  // bi-directional many-to-one association to SdxUsuario
  @ManyToOne
  @JoinColumn(name = "PK_USUARIO", nullable = false)
  private Usuario usuario;

  /**
   * On pre persist.
   */
  @PrePersist
  public void onPrePersist() {
    fechaCreacion = new Date();
  }

  /**
   * Instantiates a new motivo generacion informe social.
   */
  public MotivoGeneracionInformeSocial() {
    // Constructor vacío
  }

  /**
   * Gets the pk motgeneracion is.
   *
   * @return the pkMotgeneracionIs
   */
  public Long getPkMotgeneracionIs() {
    return pkMotgeneracionIs;
  }

  /**
   * Sets the pk motgeneracion is.
   *
   * @param pkMotgeneracionIs the pkMotgeneracionIs to set
   */
  public void setPkMotgeneracionIs(final Long pkMotgeneracionIs) {
    this.pkMotgeneracionIs = pkMotgeneracionIs;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return observaciones;
  }

  /**
   * Sets the observaciones.
   *
   * @param observaciones the observaciones to set
   */
  public void setObservaciones(final String observaciones) {
    this.observaciones = observaciones;
  }

  /**
   * Gets the informe social.
   *
   * @return the informeSocial
   */
  public InformeSocial getInformeSocial() {
    return informeSocial;
  }

  /**
   * Sets the informe social.
   *
   * @param informeSocial the informeSocial to set
   */
  public void setInformeSocial(final InformeSocial informeSocial) {
    this.informeSocial = informeSocial;
  }

  /**
   * Gets the motivo generacion.
   *
   * @return the motivoGeneracion
   */
  public MotivoGeneracion getMotivoGeneracion() {
    return motivoGeneracion;
  }

  /**
   * Sets the motivo generacion.
   *
   * @param motivoGeneracion the motivoGeneracion to set
   */
  public void setMotivoGeneracion(final MotivoGeneracion motivoGeneracion) {
    this.motivoGeneracion = motivoGeneracion;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the usuario to set
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
