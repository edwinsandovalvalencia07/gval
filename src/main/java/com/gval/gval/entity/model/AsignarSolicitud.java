package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;

/**
 * The Class AsignacionSolicitud.
 */
@Entity
@Table(name = "SDM_ASIGSOLI")
@GenericGenerator(name = "SDM_ASIGSOLI_PKASIGSOLI_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_ASIGSOLI"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class AsignarSolicitud implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 8269919326829881576L;

  /** The pk asigsoli. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_ASIGSOLI_PKASIGSOLI_GENERATOR")
  @Column(name = "PK_ASIGSOLI", unique = true, nullable = false)
  private Long pkAsigsoli;

  /** The activo. */
  @Column(name = "ASACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /** The fecha aplazamiento. */
  @Temporal(TemporalType.DATE)
  @Column(name = "ASFECHAPLA")
  private Date fechaAplazamiento;

  /** The fecha asignacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "ASFECHASIG")
  private Date fechaAsignacion;

  /** The fecha cita. */
  @Temporal(TemporalType.DATE)
  @Column(name = "ASFECHCITA")
  private Date fechaCita;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "ASFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The hora desde. */
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "ASHORADESD")
  private Date horaDesde;

  /** The hora hasta. */
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "ASHORAHAST")
  private Date horaHasta;

  /** The motivo. */
  @Column(name = "ASMOTIVO", length = 4000)
  private String motivo;

  /** The direccion. */
  // bi-directional many-to-one association to SdmDireccio
  @ManyToOne
  @JoinColumn(name = "PK_DIRECCIO")
  private DireccionAda direccion;

  /** The solicitud. */
  // bi-directional many-to-one association to SdmSolicit
  @ManyToOne
  @JoinColumn(name = "PK_SOLICIT", nullable = false)
  private Solicitud solicitud;

  /** The usuario . */
  // bi-directional many-to-one association to SdxUsuario
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA2")
  private Usuario usuario;

  /** The valoracion. */
  @OneToOne(mappedBy = "asignarSolicitud")
  private Valoracion valoracion;


  /**
   * On pre persist.
   */
  @PrePersist
  public void onPrePersist() {
    this.activo = Boolean.TRUE;
    this.fechaCreacion = new Date();
  }

  /**
   * Instantiates a new asignacion solicitud.
   */
  public AsignarSolicitud() {
    super();
  }

  /**
   * Gets the pk asigsoli.
   *
   * @return the pk asigsoli
   */
  public Long getPkAsigsoli() {
    return pkAsigsoli;
  }

  /**
   * Sets the pk asigsoli.
   *
   * @param pkAsigsoli the new pk asigsoli
   */
  public void setPkAsigsoli(final Long pkAsigsoli) {
    this.pkAsigsoli = pkAsigsoli;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha aplazamiento.
   *
   * @return the fecha aplazamiento
   */
  public Date getFechaAplazamiento() {
    return UtilidadesCommons.cloneDate(fechaAplazamiento);
  }

  /**
   * Sets the fecha aplazamiento.
   *
   * @param fechaAplazamiento the new fecha aplazamiento
   */
  public void setFechaAplazamiento(final Date fechaAplazamiento) {
    this.fechaAplazamiento = UtilidadesCommons.cloneDate(fechaAplazamiento);
  }

  /**
   * Gets the fecha asignacion.
   *
   * @return the fecha asignacion
   */
  public Date getFechaAsignacion() {
    return UtilidadesCommons.cloneDate(fechaAsignacion);
  }

  /**
   * Sets the fecha asignacion.
   *
   * @param fechaAsignacion the new fecha asignacion
   */
  public void setFechaAsignacion(final Date fechaAsignacion) {
    this.fechaAsignacion = UtilidadesCommons.cloneDate(fechaAsignacion);
  }

  /**
   * Gets the fecha cita.
   *
   * @return the fecha cita
   */
  public Date getFechaCita() {
    return UtilidadesCommons.cloneDate(fechaCita);
  }

  /**
   * Sets the fecha cita.
   *
   * @param fechaCita the new fecha cita
   */
  public void setFechaCita(final Date fechaCita) {
    this.fechaCita = UtilidadesCommons.cloneDate(fechaCita);
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the hora desde.
   *
   * @return the hora desde
   */
  public Date getHoraDesde() {
    return UtilidadesCommons.cloneDate(horaDesde);
  }

  /**
   * Sets the hora desde.
   *
   * @param horaDesde the new hora desde
   */
  public void setHoraDesde(final Date horaDesde) {
    this.horaDesde = UtilidadesCommons.cloneDate(horaDesde);
  }

  /**
   * Gets the hora hasta.
   *
   * @return the hora hasta
   */
  public Date getHoraHasta() {
    return UtilidadesCommons.cloneDate(horaHasta);
  }

  /**
   * Sets the hora hasta.
   *
   * @param horaHasta the new hora hasta
   */
  public void setHoraHasta(final Date horaHasta) {
    this.horaHasta = UtilidadesCommons.cloneDate(horaHasta);
  }

  /**
   * Gets the motivo.
   *
   * @return the motivo
   */
  public String getMotivo() {
    return motivo;
  }

  /**
   * Sets the motivo.
   *
   * @param motivo the new motivo
   */
  public void setMotivo(final String motivo) {
    this.motivo = motivo;
  }

  /**
   * Gets the direccion.
   *
   * @return the direccion
   */
  public DireccionAda getDireccion() {
    return direccion;
  }

  /**
   * Sets the direccion.
   *
   * @param direccion the new direccion
   */
  public void setDireccion(final DireccionAda direccion) {
    this.direccion = direccion;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public Solicitud getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the new solicitud
   */
  public void setSolicitud(final Solicitud solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the valoracion.
   *
   * @return the valoracion
   */
  public Valoracion getValoracion() {
    return valoracion;
  }

  /**
   * Sets the valoracion.
   *
   * @param valoracion the new valoracion
   */
  public void setValoracion(final Valoracion valoracion) {
    this.valoracion = valoracion;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
