package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;
import jakarta.persistence.*;


/**
 * The persistent class for the SDX_COMAUTO database table.
 *
 */
@Entity
@Table(name = "SDX_COMAUTO")
public class ComunidadAutonoma implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 3156437960088426494L;

  /** The pk comauto. */
  @Id
  @Column(name = "PK_COMAUTO", unique = true, nullable = false)
  private Long pkComauto;

  /** The activo. */
  @Column(name = "COACTIVO", nullable = false, precision = 38)
  private Boolean activo;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "COFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The imserso. */
  @Column(name = "COIMSERSO", length = 2)
  private String imserso;

  /** The nombre. */
  @Column(name = "CONOMBRE", nullable = false, length = 50)
  private String nombre;

  /**
   * Instantiates a new comunidad autonoma.
   */
  public ComunidadAutonoma() {
    // Empty constructor
  }

  /**
   * Instantiates a new comunidad autonoma.
   *
   * @param pkComauto the pk comauto
   * @param activo the activo
   * @param fechaCreacion the fecha creacion
   * @param imserso the imserso
   * @param nombre the nombre
   */
  public ComunidadAutonoma(final Long pkComauto, final Boolean activo,
      final Date fechaCreacion, final String imserso, final String nombre) {
    super();
    this.pkComauto = pkComauto;
    this.activo = activo;
    this.fechaCreacion = fechaCreacion;
    this.imserso = imserso;
    this.nombre = nombre;
  }

  /**
   * Gets the pk comauto.
   *
   * @return the pk comauto
   */
  public Long getPkComauto() {
    return pkComauto;
  }

  /**
   * Sets the pk comauto.
   *
   * @param pkComauto the new pk comauto
   */
  public void setPkComauto(final Long pkComauto) {
    this.pkComauto = pkComauto;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return fechaCreacion;
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

  /**
   * Gets the imserso.
   *
   * @return the imserso
   */
  public String getImserso() {
    return imserso;
  }

  /**
   * Sets the imserso.
   *
   * @param imserso the new imserso
   */
  public void setImserso(final String imserso) {
    this.imserso = imserso;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
