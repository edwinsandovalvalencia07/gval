package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import jakarta.persistence.*;


/**
 * The type Contador.
 */
@Entity
@Table(name = "SDM_CONTADOR_SAD", schema = "ADA")
@NamedQuery(name = "Contador.findAll", query = "SELECT c FROM Contador c")
@GenericGenerator(name = "SDM_CONTADOR_PKCONTADOR_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_CONTADOR_SAD"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class Contador implements Serializable {

  /** Serial ID. */
  private static final long serialVersionUID = -1086979375177333243L;

  /** The id contador. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_CONTADOR_PKCONTADOR_GENERATOR")
  @Column(name = "PK_CONTADOR_SAD", nullable = false)
  private Long idContador;

  /** The fk zona cobertura. */
  @Column(name = "PK_ZONACOBE", nullable = false)
  private Long fkZonaCobertura;
  /** The clave. */
  @Column(name = "CLAVE", nullable = false)
  private Long clave;

  /** The estado. */
  @Column(name = "COESTADO", nullable = true, length = 1)
  private String estado;

  /** The activo. */
  @Column(name = "COACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /** The fecha inicio. */
  @Temporal(TemporalType.DATE)
  @Column(name = "COFECHINI", nullable = true)
  private Date fechaInicio;

  /** The fecha fin. */
  @Temporal(TemporalType.DATE)
  @Column(name = "COFECHFIN", nullable = true)
  private Date fechaFin;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "COFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The presupuesto anual. */
  @Column(name = "COPPTO_ANUAL", nullable = true, precision = 9, scale = 2)
  private Double presupuestoAnual;

  /** The presupuesto disponible. */
  @Column(name = "COPPTO_DISPONIBLE", nullable = true, precision = 9, scale = 2)
  private Double presupuestoDisponible;

  /** The precio hora. */
  @Column(name = "COPRECIO_HORA", nullable = true, precision = 5, scale = 2)
  private Double precioHora;

  /** The horas asignadas. */
  @Column(name = "COHORAS_ASIGNADAS", nullable = true, precision = 9, scale = 2)
  private Double horasAsignadas;

  /** The horas disponibles. */
  @Column(name = "COHORAS_DISPONIBLES", nullable = true, precision = 9,
      scale = 2)
  private Double horasDisponibles;

  /** The zona cobertura. */
  @ManyToOne
  @JoinColumn(name = "PK_ZONACOBE", referencedColumnName = "PK_ZONACOBE",
      nullable = false, updatable = false, insertable = false)
  private ZonaCobertura zonaCobertura;

  /** The ejercicio. */
  @ManyToOne
  @JoinColumn(name = "CLAVE", referencedColumnName = "CLAVE", nullable = false,
      updatable = false, insertable = false)
  private Ejercicio ejercicio;


  /**
   * Constructor de contador.
   */
  public Contador() {
    // constructor stub Contador
  }

  /**
   * Gets id contador.
   *
   * @return the id contador
   */

  public Long getIdContador() {
    return idContador;
  }

  /**
   * Sets id contador.
   *
   * @param pkContadorSad the pk contador sad
   */
  public void setIdContador(final Long pkContadorSad) {
    this.idContador = pkContadorSad;
  }

  /**
   * Gets fk zona cobertura.
   *
   * @return the fk zona cobertura
   */

  public Long getFkZonaCobertura() {
    return fkZonaCobertura;
  }

  /**
   * Sets fk zona cobertura.
   *
   * @param pkZonacobe the pk zonacobe
   */
  public void setFkZonaCobertura(final Long pkZonacobe) {
    this.fkZonaCobertura = pkZonacobe;
  }

  /**
   * Gets clave.
   *
   * @return the clave
   */

  public Long getClave() {
    return clave;
  }

  /**
   * Sets clave.
   *
   * @param clave the clave
   */
  public void setClave(final Long clave) {
    this.clave = clave;
  }

  /**
   * Gets estado.
   *
   * @return the estado
   */

  public String getEstado() {
    return estado;
  }

  /**
   * Sets estado.
   *
   * @param estado the estado
   */
  public void setEstado(final String estado) {
    this.estado = estado;
  }

  /**
   * Gets activo.
   *
   * @return the activo
   */

  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets activo.
   *
   * @param activo the activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets fecha inicio.
   *
   * @return the fecha inicio
   */
  public Date getFechaInicio() {
    return UtilidadesCommons.cloneDate(fechaInicio);
  }

  /**
   * Sets fecha inicio.
   *
   * @param fechaInicio the fecha inicio
   */
  public void setFechaInicio(final Date fechaInicio) {
    this.fechaInicio = UtilidadesCommons.cloneDate(fechaInicio);
  }

  /**
   * Gets fecha fin.
   *
   * @return the fecha fin
   */

  public Date getFechaFin() {
    return UtilidadesCommons.cloneDate(fechaFin);
  }

  /**
   * Sets fecha fin.
   *
   * @param fechaFin the fecha fin
   */
  public void setFechaFin(final Date fechaFin) {
    this.fechaFin = UtilidadesCommons.cloneDate(fechaFin);
  }

  /**
   * Gets fecha creacion.
   *
   * @return the fecha creacion
   */

  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets fecha creacion.
   *
   * @param fechaCreacion the fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets presupuesto anual.
   *
   * @return the presupuesto anual
   */
  public Double getPresupuestoAnual() {
    return presupuestoAnual;
  }

  /**
   * Sets presupuesto anual.
   *
   * @param presupuestoAnual the presupuesto anual
   */
  public void setPresupuestoAnual(final Double presupuestoAnual) {
    this.presupuestoAnual = presupuestoAnual;
  }

  /**
   * Gets presupuesto disponible.
   *
   * @return the presupuesto disponible
   */

  public Double getPresupuestoDisponible() {
    return presupuestoDisponible;
  }

  /**
   * Sets presupuesto disponible.
   *
   * @param presupuestoDisponible the presupuesto disponible
   */
  public void setPresupuestoDisponible(final Double presupuestoDisponible) {
    this.presupuestoDisponible = presupuestoDisponible;
  }

  /**
   * Gets precio hora.
   *
   * @return the precio hora
   */
  public Double getPrecioHora() {
    return precioHora;
  }

  /**
   * Sets precio hora.
   *
   * @param precioHora the precio hora
   */
  public void setPrecioHora(final Double precioHora) {
    this.precioHora = precioHora;
  }

  /**
   * Gets horas asignadas.
   *
   * @return the horas asignadas
   */
  public Double getHorasAsignadas() {
    return horasAsignadas;
  }

  /**
   * Sets horas asignadas.
   *
   * @param horasAsignadas the horas asignadas
   */
  public void setHorasAsignadas(final Double horasAsignadas) {
    this.horasAsignadas = horasAsignadas;
  }

  /**
   * Gets horas disponibles.
   *
   * @return the horas disponibles
   */
  public Double getHorasDisponibles() {
    return horasDisponibles;
  }

  /**
   * Sets horas disponibles.
   *
   * @param horasDisponibles the horas disponibles
   */
  public void setHorasDisponibles(final Double horasDisponibles) {
    this.horasDisponibles = horasDisponibles;
  }


  /**
   * Gets zona cobertura.
   *
   * @return the zona cobertura
   */

  public ZonaCobertura getZonaCobertura() {
    return zonaCobertura;
  }

  /**
   * Sets zona cobertura.
   *
   * @param zonaCobertura the zona cobertura
   */
  public void setZonaCobertura(final ZonaCobertura zonaCobertura) {
    this.zonaCobertura = zonaCobertura;
  }

  /**
   * Gets ejercicio.
   *
   * @return the ejercicio
   */

  public Ejercicio getEjercicio() {
    return ejercicio;
  }

  /**
   * Sets ejercicio.
   *
   * @param ejercicio the ejercicio
   */
  public void setEjercicio(final Ejercicio ejercicio) {
    this.ejercicio = ejercicio;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
