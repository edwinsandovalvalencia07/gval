package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

import jakarta.persistence.*;


/**
 * The persistent class for the SDM_DOCUAPOR database table.
 *
 */
@Entity
@Table(name = "SDM_DOCUAPOR")
public class DocumentoAportado implements Serializable {


  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -530317362189496019L;

  /** The pk documento. */
  @Id
  @Column(name = "PK_DOCU", unique = true, nullable = false)
  private Long pkDocumento;

  /** The observaciones. */
  @Column(name = "DOOBSERVAC", length = 250)
  private String observaciones;

  /** The visto administrador. */
  @Column(name = "DOVISTOADM", nullable = false)
  private Boolean vistoAdministrador;

  /** The documento. */
  // bi-directional one-to-one association to SdmDocu
  @OneToOne
  @JoinColumn(name = "PK_DOCU", nullable = false, insertable = false,
      updatable = true, referencedColumnName = "pkDocumento")
  @MapsId
  private Documento documento;

  /** The documento generado. */
  // bi-directional many-to-one association to SdmDocugene
  @ManyToOne
  @JoinColumn(name = "PK_DOCU2")
  private DocumentoGenerado documentoGenerado;

  /** The usuario. */
  // bi-directional many-to-one association to SdxUsuario
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA")
  private Usuario usuario;



  /**
   * Instantiates a new documento aportado.
   */
  public DocumentoAportado() {
    // Constructor vacio
  }

  /**
   * Gets the pk documento.
   *
   * @return the pkDocumento
   */
  public Long getPkDocumento() {
    return pkDocumento;
  }

  /**
   * Sets the pk documento.
   *
   * @param pkDocumento the pkDocumento to set
   */
  public void setPkDocumento(final Long pkDocumento) {
    this.pkDocumento = pkDocumento;
  }

  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return observaciones;
  }

  /**
   * Sets the observaciones.
   *
   * @param observaciones the observaciones to set
   */
  public void setObservaciones(final String observaciones) {
    this.observaciones = observaciones;
  }

  /**
   * Gets the visto administrador.
   *
   * @return the vistoAdministrador
   */
  public Boolean getVistoAdministrador() {
    return vistoAdministrador;
  }

  /**
   * Sets the visto administrador.
   *
   * @param vistoAdministrador the vistoAdministrador to set
   */
  public void setVistoAdministrador(final Boolean vistoAdministrador) {
    this.vistoAdministrador = vistoAdministrador;
  }

  /**
   * Gets the documento.
   *
   * @return the documento
   */
  public Documento getDocumento() {
    return documento;
  }

  /**
   * Sets the documento.
   *
   * @param documento the documento to set
   */
  public void setDocumento(final Documento documento) {
    this.documento = documento;
  }

  /**
   * Gets the documento generado.
   *
   * @return the documentoGenerado
   */
  public DocumentoGenerado getDocumentoGenerado() {
    return documentoGenerado;
  }

  /**
   * Sets the documento generado.
   *
   * @param documentoGenerado the documentoGenerado to set
   */
  public void setDocumentoGenerado(final DocumentoGenerado documentoGenerado) {
    this.documentoGenerado = documentoGenerado;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the usuario to set
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
