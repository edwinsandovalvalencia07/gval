package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The persistent class for the SDM_ESTDOCU database table.
 *
 */
@Entity
@Table(name = "SDM_ESTDOCU")
@GenericGenerator(name = "SDM_ESTDOCU_PKESTDOCU_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {@Parameter(name = "sequence_name", value = "SEC_SDM_ESTDOCU"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class HistoricoEstadoDocumento implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 193624777339955404L;

  /** The pk historico estado documento. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_ESTDOCU_PKESTDOCU_GENERATOR")
  @Column(name = "PK_ESTDOCU", unique = true, nullable = false)
  private Long pkHistoricoEstadoDocumento;

  /** The activo. */
  @Column(name = "ESACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "ESFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The fecha desde. */
  @Temporal(TemporalType.DATE)
  @Column(name = "ESFECHDESD", nullable = false)
  private Date fechaDesde;

  /** The fecha hasta. */
  @Temporal(TemporalType.DATE)
  @Column(name = "ESFECHHASTA")
  private Date fechaHasta;

  /** The documento. */
  // bi-directional many-to-one association to SdmDocu
  @ManyToOne
  @JoinColumn(name = "PK_DOCU", nullable = false)
  private Documento documento;

  /** The usuario. */
  // bi-directional many-to-one association to SdmPersona
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA", nullable = false)
  private Usuario usuario;

  /** The estado documento. */
  // bi-directional many-to-one association to SdxEstadoDocu
  @ManyToOne
  @JoinColumn(name = "PK_ESTADO_DOCU", nullable = false)
  private EstadoDocumento estadoDocumento;

  /** The observaciones. */
  @Column(name = "OBSERVACIONES")
  private String observaciones;

  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return observaciones;
  }

  /**
   * Sets the observaciones.
   *
   * @param observaciones the new observaciones
   */
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }

  /**
   * Instantiates a new historico estado documento.
   */
  public HistoricoEstadoDocumento() {
    // Empty constructor
  }

  /**
   * Gets the pk historico estado documento.
   *
   * @return the pk historico estado documento
   */
  public Long getPkHistoricoEstadoDocumento() {
    return pkHistoricoEstadoDocumento;
  }

  /**
   * Sets the pk historico estado documento.
   *
   * @param pkHistoricoEstadoDocumento the new pk historico estado documento
   */
  public void setPkHistoricoEstadoDocumento(
      final Long pkHistoricoEstadoDocumento) {
    this.pkHistoricoEstadoDocumento = pkHistoricoEstadoDocumento;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha desde.
   *
   * @return the fecha desde
   */
  public Date getFechaDesde() {
    return UtilidadesCommons.cloneDate(fechaDesde);
  }

  /**
   * Sets the fecha desde.
   *
   * @param fechaDesde the new fecha desde
   */
  public void setFechaDesde(final Date fechaDesde) {
    this.fechaDesde = UtilidadesCommons.cloneDate(fechaDesde);
  }

  /**
   * Gets the fecha hasta.
   *
   * @return the fecha hasta
   */
  public Date getFechaHasta() {
    return UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Sets the fecha hasta.
   *
   * @param fechaHasta the new fecha hasta
   */
  public void setFechaHasta(final Date fechaHasta) {
    this.fechaHasta = UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Gets the documento.
   *
   * @return the documento
   */
  public Documento getDocumento() {
    return documento;
  }

  /**
   * Sets the documento.
   *
   * @param documento the new documento
   */
  public void setDocumento(final Documento documento) {
    this.documento = documento;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the estado documento.
   *
   * @return the estado documento
   */
  public EstadoDocumento getEstadoDocumento() {
    return estadoDocumento;
  }

  /**
   * Sets the estado documento.
   *
   * @param estadoDocumento the new estado documento
   */
  public void setEstadoDocumento(final EstadoDocumento estadoDocumento) {
    this.estadoDocumento = estadoDocumento;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
