package com.gval.gval.entity.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import jakarta.persistence.*;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.gval.gval.common.UtilidadesCommons;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the SDM_DICTAMEN database table.
 *
 */
@Entity
@Table(name = "SDM_DICTAMEN")
@GenericGenerator(name = "SDM_DICTAMEN_PKDICTAMEN_GENERATOR", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
		@Parameter(name = "sequence_name", value = "SEC_SDM_DICTAMEN"), @Parameter(name = "initial_value", value = "1"),
		@Parameter(name = "increment_size", value = "1") })
public class Dictamen implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2778482234495470921L;

	/** The pk dictamen. */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SDM_DICTAMEN_PKDICTAMEN_GENERATOR")
	@Column(name = "PK_DICTAMEN", unique = true, nullable = false)
	private long pkDictamen;

	/** The activo. */
	@Column(name = "DIACTIVO", nullable = false)
	private Boolean activo;

	/** The discapacidad fisica. */
	@Column(name = "DIDISCFISI")
	private Boolean discapacidadFisica;

	/** The discapacidad psiquica. */
	@Column(name = "DIDISCPSIQ")
	private Boolean discapacidadPsiquica;

	/** The discapacidad sensorial. */
	@Column(name = "DIDISCSENS")
	private Boolean discapacidadSensorial;

	/** The enfermedad mental. */
	@Column(name = "DIENFEMENT")
	private Boolean enfermedadMental;

	/** The fecha revisable. */
	@Temporal(TemporalType.DATE)
	@Column(name = "DIFECHAREV")
	private Date fechaRevisable;

	/** The grado. */
	@Column(name = "DIGRADO", length = 1)
	private String grado;

	/** The nivel. */
	@Column(name = "DINIVEL", length = 1)
	private String nivel;

	/** The otros cuidados. */
	@Column(name = "DIOTROCUID", length = 500)
	private String otrosCuidados;

	/** The puntuacion. */
	@Column(name = "DIPUNT")
	private Long puntuacion;

	/** The pluripatologia. */
	@Column(name = "PLURIPATOLOGIA")
	private Boolean pluripatologia;

	/** The estudio. */
	@OneToOne(mappedBy = "dictamen")
	private Estudio estudio;

    /** The diagnosticoDictamenList. */
	@OneToMany(mappedBy = "dictamen")
	private List<Diagdict> diagnosticoDictamenList;

	/**
	 * Instantiates a new dictamen.
	 */
	public Dictamen() {
		super();
	}

	/**
	 * Gets the pk dictamen.
	 *
	 * @return the pkDictamen
	 */
	public long getPkDictamen() {
		return pkDictamen;
	}

	/**
	 * Sets the pk dictamen.
	 *
	 * @param pkDictamen the pkDictamen to set
	 */
	public void setPkDictamen(final long pkDictamen) {
		this.pkDictamen = pkDictamen;
	}

	/**
	 * Gets the activo.
	 *
	 * @return the activo
	 */
	public Boolean getActivo() {
		return activo;
	}

	/**
	 * Sets the activo.
	 *
	 * @param activo the activo to set
	 */
	public void setActivo(final Boolean activo) {
		this.activo = activo;
	}

	/**
	 * Gets the discapacidad fisica.
	 *
	 * @return the discapacidadFisica
	 */
	public Boolean getDiscapacidadFisica() {
		return discapacidadFisica;
	}

	/**
	 * Sets the discapacidad fisica.
	 *
	 * @param discapacidadFisica the discapacidadFisica to set
	 */
	public void setDiscapacidadFisica(final Boolean discapacidadFisica) {
		this.discapacidadFisica = discapacidadFisica;
	}

	/**
	 * Gets the discapacidad psiquica.
	 *
	 * @return the discapacidadPsiquica
	 */
	public Boolean getDiscapacidadPsiquica() {
		return discapacidadPsiquica;
	}

	/**
	 * Sets the discapacidad psiquica.
	 *
	 * @param discapacidadPsiquica the discapacidadPsiquica to set
	 */
	public void setDiscapacidadPsiquica(final Boolean discapacidadPsiquica) {
		this.discapacidadPsiquica = discapacidadPsiquica;
	}

	/**
	 * Gets the discapacidad sensorial.
	 *
	 * @return the discapacidadSensorial
	 */
	public Boolean getDiscapacidadSensorial() {
		return discapacidadSensorial;
	}

	/**
	 * Sets the discapacidad sensorial.
	 *
	 * @param discapacidadSensorial the discapacidadSensorial to set
	 */
	public void setDiscapacidadSensorial(final Boolean discapacidadSensorial) {
		this.discapacidadSensorial = discapacidadSensorial;
	}

	/**
	 * Gets the enfermedad mental.
	 *
	 * @return the enfermedadMental
	 */
	public Boolean getEnfermedadMental() {
		return enfermedadMental;
	}

	/**
	 * Sets the enfermedad mental.
	 *
	 * @param enfermedadMental the enfermedadMental to set
	 */
	public void setEnfermedadMental(final Boolean enfermedadMental) {
		this.enfermedadMental = enfermedadMental;
	}

	/**
	 * Gets the fecha revisable.
	 *
	 * @return the fechaRevisable
	 */
	public Date getFechaRevisable() {
		return UtilidadesCommons.cloneDate(fechaRevisable);
	}

	/**
	 * Sets the fecha revisable.
	 *
	 * @param fechaRevisable the fechaRevisable to set
	 */
	public void setFechaRevisable(final Date fechaRevisable) {
		this.fechaRevisable = UtilidadesCommons.cloneDate(fechaRevisable);
	}

	/**
	 * Gets the grado.
	 *
	 * @return the grado
	 */
	public String getGrado() {
		return grado;
	}

	/**
	 * Sets the grado.
	 *
	 * @param grado the grado to set
	 */
	public void setGrado(final String grado) {
		this.grado = grado;
	}

	/**
	 * Gets the nivel.
	 *
	 * @return the nivel
	 */
	public String getNivel() {
		return nivel;
	}

	/**
	 * Sets the nivel.
	 *
	 * @param nivel the nivel to set
	 */
	public void setNivel(final String nivel) {
		this.nivel = nivel;
	}

	/**
	 * Gets the otros cuidados.
	 *
	 * @return the otrosCuidados
	 */
	public String getOtrosCuidados() {
		return otrosCuidados;
	}

	/**
	 * Sets the otros cuidados.
	 *
	 * @param otrosCuidados the otrosCuidados to set
	 */
	public void setOtrosCuidados(final String otrosCuidados) {
		this.otrosCuidados = otrosCuidados;
	}

	/**
	 * Gets the puntuacion.
	 *
	 * @return the puntuacion
	 */
	public Long getPuntuacion() {
		return puntuacion;
	}

	/**
	 * Sets the puntuacion.
	 *
	 * @param puntuacion the puntuacion to set
	 */
	public void setPuntuacion(final Long puntuacion) {
		this.puntuacion = puntuacion;
	}

	/**
	 * Gets the pluripatologia.
	 *
	 * @return the pluripatologia
	 */
	public Boolean getPluripatologia() {
		return pluripatologia;
	}

	/**
	 * Sets the pluripatologia.
	 *
	 * @param pluripatologia the pluripatologia to set
	 */
	public void setPluripatologia(final Boolean pluripatologia) {
		this.pluripatologia = pluripatologia;
	}

	/**
	 * Gets the estudio.
	 *
	 * @return the estudio
	 */
	public Estudio getEstudio() {
		return estudio;
	}

	/**
	 * Sets the estudio.
	 *
	 * @param estudio the new estudio
	 */
	public void setEstudio(final Estudio estudio) {
		this.estudio = estudio;
	}

	/**
	 * Gets the diagnostico dictamen list.
	 *
	 * @return the diagnostico dictamen list
	 */
	public List<Diagdict> getDiagnosticoDictamenList() {
		return UtilidadesCommons.collectorsToList(diagnosticoDictamenList);
	}

	/**
	 * Sets the diagnostico dictamen list.
	 *
	 * @param diagnosticoDictamenList the new diagnostico dictamen list
	 */
	public void setDiagnosticoDictamenList(final List<Diagdict> diagnosticoDictamenList) {
		this.diagnosticoDictamenList = UtilidadesCommons.collectorsToList(diagnosticoDictamenList);
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

}
