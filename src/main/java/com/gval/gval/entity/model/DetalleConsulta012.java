package com.gval.gval.entity.model;

import java.io.Serializable;

import jakarta.persistence.*;

/**
 * The Class DetalleConsulta012.
 */
@Entity
@Table(name = "SDV_DS_CONSULTA012")
public class DetalleConsulta012 implements Serializable {
  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk expedien. */
  @Id
  @Column(name = "PK_EXPEDIEN", unique = true, nullable = false)
  private Long pkExpedien;

  /** The dni. */
  @Column(name = "DNI", unique = true, nullable = false)
  private String dni;

  /** The nombre. */
  @Column(name = "NOMBRE", unique = true, nullable = false)
  private String nombre;

  /** The apellido 1. */
  @Column(name = "APELLIDO1", unique = true, nullable = false)
  private String primerApellido;

  /** The apellido 2. */
  @Column(name = "APELLIDO2", unique = true, nullable = false)
  private String segundoApellido;

  /** The expediente. */
  @Column(name = "EXPEDIENTE", unique = true, nullable = false)
  private String expediente;

  /** The poblacion. */
  @Column(name = "POBLACION", unique = true, nullable = false)
  private String poblacion;

  /** The entidad local referencia. */
  @Column(name = "ENTIDAD_LOCAL_REFERENCIA", unique = true, nullable = false)
  private String entidadLocalReferencia;

  /** The telefono entidad local. */
  @Column(name = "TELEFONO_ENTIDAD_LOCAL", unique = true, nullable = false)
  private String telefonoEntidadLocal;

  /** The estado expediente. */
  @Column(name = "ESTADO_EXPEDIENTE", unique = true, nullable = false)
  private String estadoExpediente;

  /** The grabada. */
  @Column(name = "GRABADA", unique = true, nullable = false)
  private Long grabada;

  /** The valorada. */
  @Column(name = "VALORADA", unique = true, nullable = false)
  private Long valorada;

  /** The requerimientos. */
  @Column(name = "REQUERIMIENTOS", unique = true, nullable = false)
  private Long requerimientos;

  /** The res gn notificada. */
  @Column(name = "RES_GN_NOTIFICADA", unique = true, nullable = false)
  private String resGnNotificada;

  /** The res PIA notificada. */
  @Column(name = "RES_PIA_NOTIFICADA", unique = true, nullable = false)
  private String resPIANotificada;

  /** The estado desc. */
  @Column(name = "ESTADO_DESC", unique = true, nullable = false)
  private String estadoDesc;

  /** The res pia desc. */
  @Column(name = "RES_PIA_DESC", unique = true, nullable = false)
  private String resPiaDesc;

  /** The ultimo grado. */
  @Column(name = "ULTIMO_GRADO", unique = true, nullable = false)
  private String ultimoGrado;

  /** The pia 1. */
  @Column(name = "PIA_1", unique = true, nullable = false)
  private String primerPia;

  /** The pia 2. */
  @Column(name = "PIA_2", unique = true, nullable = false)
  private String segundoPia;

  /** The teleasistencia. */
  @Column(name = "TELEASISTENCIA", unique = true, nullable = false)
  private String teleasistencia;

  /**
   * Gets the pk expedien.
   *
   * @return the pk expedien
   */
  public Long getPkExpedien() {
    return pkExpedien;
  }

  /**
   * Sets the pk expedien.
   *
   * @param pkExpedien the new pk expedien
   */
  public void setPkExpedien(final Long pkExpedien) {
    this.pkExpedien = pkExpedien;
  }

  /**
   * Gets the dni.
   *
   * @return the dni
   */
  public String getDni() {
    return dni;
  }

  /**
   * Sets the dni.
   *
   * @param dni the new dni
   */
  public void setDni(final String dni) {
    this.dni = dni;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the primer apellido.
   *
   * @return the primer apellido
   */
  public String getPrimerApellido() {
    return primerApellido;
  }

  /**
   * Sets the primer apellido.
   *
   * @param primerApellido the new primer apellido
   */
  public void setPrimerApellido(final String primerApellido) {
    this.primerApellido = primerApellido;
  }

  /**
   * Gets the segundo apellido.
   *
   * @return the segundo apellido
   */
  public String getSegundoApellido() {
    return segundoApellido;
  }

  /**
   * Sets the segundo apellido.
   *
   * @param segundoApellido the new segundo apellido
   */
  public void setSegundoApellido(final String segundoApellido) {
    this.segundoApellido = segundoApellido;
  }

  /**
   * Gets the expediente.
   *
   * @return the expediente
   */
  public String getExpediente() {
    return expediente;
  }

  /**
   * Sets the expediente.
   *
   * @param expediente the new expediente
   */
  public void setExpediente(final String expediente) {
    this.expediente = expediente;
  }

  /**
   * Gets the poblacion.
   *
   * @return the poblacion
   */
  public String getPoblacion() {
    return poblacion;
  }

  /**
   * Sets the poblacion.
   *
   * @param poblacion the new poblacion
   */
  public void setPoblacion(final String poblacion) {
    this.poblacion = poblacion;
  }

  /**
   * Gets the entidad local referencia.
   *
   * @return the entidad local referencia
   */
  public String getEntidadLocalReferencia() {
    return entidadLocalReferencia;
  }

  /**
   * Sets the entidad local referencia.
   *
   * @param entidadLocalReferencia the new entidad local referencia
   */
  public void setEntidadLocalReferencia(final String entidadLocalReferencia) {
    this.entidadLocalReferencia = entidadLocalReferencia;
  }

  /**
   * Gets the telefono entidad local.
   *
   * @return the telefono entidad local
   */
  public String getTelefonoEntidadLocal() {
    return telefonoEntidadLocal;
  }

  /**
   * Sets the telefono entidad local.
   *
   * @param telefonoEntidadLocal the new telefono entidad local
   */
  public void setTelefonoEntidadLocal(final String telefonoEntidadLocal) {
    this.telefonoEntidadLocal = telefonoEntidadLocal;
  }

  /**
   * Gets the estado expediente.
   *
   * @return the estado expediente
   */
  public String getEstadoExpediente() {
    return estadoExpediente;
  }

  /**
   * Sets the estado expediente.
   *
   * @param estadoExpediente the new estado expediente
   */
  public void setEstadoExpediente(final String estadoExpediente) {
    this.estadoExpediente = estadoExpediente;
  }

  /**
   * Gets the grabada.
   *
   * @return the grabada
   */
  public Long getGrabada() {
    return grabada;
  }

  /**
   * Sets the grabada.
   *
   * @param grabada the new grabada
   */
  public void setGrabada(final Long grabada) {
    this.grabada = grabada;
  }

  /**
   * Gets the valorada.
   *
   * @return the valorada
   */
  public Long getValorada() {
    return valorada;
  }

  /**
   * Sets the valorada.
   *
   * @param valorada the new valorada
   */
  public void setValorada(final Long valorada) {
    this.valorada = valorada;
  }

  /**
   * Gets the requerimientos.
   *
   * @return the requerimientos
   */
  public Long getRequerimientos() {
    return requerimientos;
  }

  /**
   * Sets the requerimientos.
   *
   * @param requerimientos the new requerimientos
   */
  public void setRequerimientos(final Long requerimientos) {
    this.requerimientos = requerimientos;
  }

  /**
   * Gets the res gn notificada.
   *
   * @return the res gn notificada
   */
  public String getResGnNotificada() {
    return resGnNotificada;
  }

  /**
   * Sets the res gn notificada.
   *
   * @param resGnNotificada the new res gn notificada
   */
  public void setResGnNotificada(final String resGnNotificada) {
    this.resGnNotificada = resGnNotificada;
  }

  /**
   * Gets the res PIA notificada.
   *
   * @return the res PIA notificada
   */
  public String getResPIANotificada() {
    return resPIANotificada;
  }

  /**
   * Sets the res PIA notificada.
   *
   * @param resPIANotificada the new res PIA notificada
   */
  public void setResPIANotificada(final String resPIANotificada) {
    this.resPIANotificada = resPIANotificada;
  }

  /**
   * Gets the estado desc.
   *
   * @return the estado desc
   */
  public String getEstadoDesc() {
    return estadoDesc;
  }

  /**
   * Sets the estado desc.
   *
   * @param estadoDesc the new estado desc
   */
  public void setEstadoDesc(final String estadoDesc) {
    this.estadoDesc = estadoDesc;
  }

  /**
   * Gets the res pia desc.
   *
   * @return the res pia desc
   */
  public String getResPiaDesc() {
    return resPiaDesc;
  }

  /**
   * Sets the res pia desc.
   *
   * @param resPiaDesc the new res pia desc
   */
  public void setResPiaDesc(final String resPiaDesc) {
    this.resPiaDesc = resPiaDesc;
  }

  /**
   * Gets the ultimo grado.
   *
   * @return the ultimo grado
   */
  public String getUltimoGrado() {
    return ultimoGrado;
  }

  /**
   * Sets the ultimo grado.
   *
   * @param ultimoGrado the new ultimo grado
   */
  public void setUltimoGrado(final String ultimoGrado) {
    this.ultimoGrado = ultimoGrado;
  }

  /**
   * Gets the primer pia.
   *
   * @return the primer pia
   */
  public String getPrimerPia() {
    return primerPia;
  }

  /**
   * Sets the primer pia.
   *
   * @param primerPia the new primer pia
   */
  public void setPrimerPia(final String primerPia) {
    this.primerPia = primerPia;
  }

  /**
   * Gets the segundo pia.
   *
   * @return the segundo pia
   */
  public String getSegundoPia() {
    return segundoPia;
  }

  /**
   * Sets the segundo pia.
   *
   * @param segundoPia the new segundo pia
   */
  public void setSegundoPia(final String segundoPia) {
    this.segundoPia = segundoPia;
  }

  /**
   * Gets the teleasistencia.
   *
   * @return the teleasistencia
   */
  public String getTeleasistencia() {
    return teleasistencia;
  }

  /**
   * Sets the teleasistencia.
   *
   * @param teleasistencia the new teleasistencia
   */
  public void setTeleasistencia(final String teleasistencia) {
    this.teleasistencia = teleasistencia;
  }
}
