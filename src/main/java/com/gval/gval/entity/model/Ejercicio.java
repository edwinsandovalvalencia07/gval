package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;

import jakarta.persistence.*;


/**
 * The persistent class for the SDX_EJERCICI database table.
 *
 */
@Entity
@Table(name = "SDX_EJERCICI")
@GenericGenerator(name = "SDX_EJERCICI_PKEJERCICI_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDX_EJERCICI"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class Ejercicio implements Serializable {


  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 2816519479388916459L;

  /** The clave. */
  @Id
  @Column(name = "CLAVE", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDX_EJERCICI_PKEJERCICI_GENERATOR")
  private Long clave;

  /** The anyo. */
  @Column(name = "EJANYO", nullable = false)
  private Integer anyo;

  /** The cerrado. */
  @Column(name = "EJCERRADO", nullable = false)
  private Boolean cerrado;

  /** The descripcion. */
  @Column(name = "EJDESC", length = 50)
  private String descripcion;


  /**
   * Instantiates a new ejercicio.
   */
  public Ejercicio() {
    // Constructor
  }


  /**
   * Gets the clave.
   *
   * @return the clave
   */
  public Long getClave() {
    return clave;
  }


  /**
   * Sets the clave.
   *
   * @param clave the clave to set
   */
  public void setClave(final Long clave) {
    this.clave = clave;
  }


  /**
   * Gets the anyo.
   *
   * @return the anyo
   */
  public Integer getAnyo() {
    return anyo;
  }


  /**
   * Sets the anyo.
   *
   * @param anyo the anyo to set
   */
  public void setAnyo(final Integer anyo) {
    this.anyo = anyo;
  }


  /**
   * Gets the cerrado.
   *
   * @return the cerrado
   */
  public Boolean getCerrado() {
    return cerrado;
  }


  /**
   * Sets the cerrado.
   *
   * @param ejcerrado the new cerrado
   */
  public void setCerrado(final Boolean ejcerrado) {
    this.cerrado = ejcerrado;
  }


  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }


  /**
   * Sets the descripcion.
   *
   * @param ejdesc the new descripcion
   */
  public void setDescripcion(final String ejdesc) {
    this.descripcion = ejdesc;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
