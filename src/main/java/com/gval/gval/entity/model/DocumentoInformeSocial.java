package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The persistent class for the SDM_DOCU_INFOSOCI database table.
 *
 */
@Entity
@Table(name = "SDM_DOCU_INFOSOCI")
@GenericGenerator(name = "SDM_DOCU_INFOSOCI_DOCU_INFOSOCI_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_DOCU_INFOSOCI"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public final class DocumentoInformeSocial implements Serializable {


  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -8712160978988638747L;

  /** The pk docu infosoci. */
  @Id
  @Column(name = "PK_DOCU_INFOSOCI", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_DOCU_INFOSOCI_DOCU_INFOSOCI_GENERATOR")
  private Long pkDocuInfosoci;

  /** The fech creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "DIFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The documento. */
  // bi-directional many-to-one association to SdmDocu
  @ManyToOne
  @JoinColumn(name = "PK_DOCU", nullable = false)
  private Documento documento;

  /** The informe social. */
  // bi-directional many-to-one association to SdmInfosoci
  @ManyToOne
  @JoinColumn(name = "PK_INFOSOCI", nullable = false)
  private InformeSocial informeSocial;

  /**
   * Instantiates a new documento informe social.
   */
  public DocumentoInformeSocial() {
    // Constructor vacío
  }

  /**
   * Gets the pk docu infosoci.
   *
   * @return the pkDocuInfosoci
   */
  public Long getPkDocuInfosoci() {
    return pkDocuInfosoci;
  }

  /**
   * Sets the pk docu infosoci.
   *
   * @param pkDocuInfosoci the pkDocuInfosoci to set
   */
  public void setPkDocuInfosoci(final Long pkDocuInfosoci) {
    this.pkDocuInfosoci = pkDocuInfosoci;
  }

  /**
   * Gets the fech creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fech creacion.
   *
   * @param fechCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechCreacion);
  }

  /**
   * Gets the documento.
   *
   * @return the documento
   */
  public Documento getDocumento() {
    return documento;
  }

  /**
   * Sets the documento.
   *
   * @param documento the documento to set
   */
  public void setDocumento(final Documento documento) {
    this.documento = documento;
  }

  /**
   * Gets the informe social.
   *
   * @return the informeSocial
   */
  public InformeSocial getInformeSocial() {
    return informeSocial;
  }

  /**
   * Sets the informe social.
   *
   * @param informeSocial the informeSocial to set
   */
  public void setInformeSocial(final InformeSocial informeSocial) {
    this.informeSocial = informeSocial;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
