package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import java.util.Date;

import jakarta.persistence.*;

/**
 * The Class DetalleTramiteSolicitud.
 */
@Entity
@Table(name = "SDV_LD_LISTADO_TRAMITES")
public class DetalleTramiteSolicitud {

  /** The pk tramite solicitud. */
  @Id
  @Column(name = "PK_TRAMITE_SOLICITUD", unique = true, nullable = false)
  private Long pkTramiteSolicitud;

  /** The nombre. */
  @Column(name = "NOMBRE", nullable = false)
  private String nombre;

  /** The apellidos. */
  @Column(name = "APELLIDOS", nullable = false)
  private String apellidos;

  /** The nif. */
  @Column(name = "NIF", unique = true, nullable = false)
  private String nif;

  /** The telefono. */
  @Column(name = "TELEFONO", unique = true, nullable = false)
  private String telefono;

  /** The cod expediente. */
  @Column(name = "CODIGO_EXPEDIENTE", unique = true, nullable = false)
  private String codExpediente;

  /** The estado. */
  @Column(name = "Estado", nullable = false)
  private String estado;

  /** The fecha registro. */
  @Column(name = "FECHA_REGISTRO", nullable = false)
  private Date fechaRegistro;

  /** The info general. */
  @Column(name = "INFORMACION_GENERAL")
  private int infoGeneral;

  /** The pagos pendientes. */
  @Column(name = "PAGOS_PENDIENTES")
  private int pagosPendientes;

  /** The res patrimonial. */
  @Column(name = "RESPONSABILIDAD_PATRIMONIAL")
  private int resPatrimonial;

  /** The pagos. */
  @Column(name = "PAGOS")
  private int pagos;

  /** The otros. */
  @Column(name = "OTROS")
  private int otros;

  /** The otros desc. */
  @Column(name = "OTROS_DESC")
  private String otrosDesc;

  /** The fecha llamada. */
  @Column(name = "FECHA_LLAMADA")
  private Date fechaLlamada;

  /** The observaciones. */
  @Column(name = "OBSERVACIONES")
  private String observaciones;

  /**
   * Gets the pk tramite solicitud.
   *
   * @return the pk tramite solicitud
   */
  public Long getPkTramiteSolicitud() {
    return pkTramiteSolicitud;
  }

  /**
   * Sets the pk tramite solicitud.
   *
   * @param pkTramiteSolicitud the new pk tramite solicitud
   */
  public void setPkTramiteSolicitud(Long pkTramiteSolicitud) {
    this.pkTramiteSolicitud = pkTramiteSolicitud;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the otros desc.
   *
   * @return the otros desc
   */
  public String getOtrosDesc() {
    return otrosDesc;
  }

  /**
   * Gets the apellidos.
   *
   * @return the apellidos
   */
  public String getApellidos() {
    return apellidos;
  }

  /**
   * Sets the apellidos.
   *
   * @param apellidos the new apellidos
   */
  public void setApellidos(String apellidos) {
    this.apellidos = apellidos;
  }

  /**
   * Gets the nif.
   *
   * @return the nif
   */
  public String getNif() {
    return nif;
  }

  /**
   * Sets the nif.
   *
   * @param nif the new nif
   */
  public void setNif(String nif) {
    this.nif = nif;
  }

  /**
   * Gets the telefono.
   *
   * @return the telefono
   */
  public String getTelefono() {
    return telefono;
  }

  /**
   * Sets the telefono.
   *
   * @param telefono the new telefono
   */
  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }

  /**
   * Gets the cod expediente.
   *
   * @return the cod expediente
   */
  public String getCodExpediente() {
    return codExpediente;
  }

  /**
   * Sets the cod expediente.
   *
   * @param codExpediente the new cod expediente
   */
  public void setCodExpediente(String codExpediente) {
    this.codExpediente = codExpediente;
  }

  /**
   * Gets the estado.
   *
   * @return the estado
   */
  public String getEstado() {
    return estado;
  }

  /**
   * Sets the estado.
   *
   * @param estado the new estado
   */
  public void setEstado(String estado) {
    this.estado = estado;
  }

  /**
   * Gets the fecha registro.
   *
   * @return the fecha registro
   */
  public Date getFechaRegistro() {
    return UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Sets the fecha registro.
   *
   * @param fechaRegistro the new fecha registro
   */
  public void setFechaRegistro(Date fechaRegistro) {
    this.fechaRegistro = UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Gets the info general.
   *
   * @return the info general
   */
  public int getInfoGeneral() {
    return infoGeneral;
  }

  /**
   * Sets the info general.
   *
   * @param infoGeneral the new info general
   */
  public void setInfoGeneral(int infoGeneral) {
    this.infoGeneral = infoGeneral;
  }

  /**
   * Gets the pagos pendientes.
   *
   * @return the pagos pendientes
   */
  public int getPagosPendientes() {
    return pagosPendientes;
  }

  /**
   * Sets the pagos pendientes.
   *
   * @param pagosPendientes the new pagos pendientes
   */
  public void setPagosPendientes(int pagosPendientes) {
    this.pagosPendientes = pagosPendientes;
  }

  /**
   * Gets the res patrimonial.
   *
   * @return the res patrimonial
   */
  public int getResPatrimonial() {
    return resPatrimonial;
  }

  /**
   * Sets the res patrimonial.
   *
   * @param resPatrimonial the new res patrimonial
   */
  public void setResPatrimonial(int resPatrimonial) {
    this.resPatrimonial = resPatrimonial;
  }

  /**
   * Gets the pagos.
   *
   * @return the pagos
   */
  public int getPagos() {
    return pagos;
  }

  /**
   * Sets the pagos.
   *
   * @param pagos the new pagos
   */
  public void setPagos(int pagos) {
    this.pagos = pagos;
  }

  /**
   * Gets the otros.
   *
   * @return the otros
   */
  public int getOtros() {
    return otros;
  }

  /**
   * Sets the otros.
   *
   * @param otros the new otros
   */
  public void setOtros(int otros) {
    this.otros = otros;
  }

  /**
   * Gets the fecha llamada.
   *
   * @return the fecha llamada
   */
  public Date getFechaLlamada() {
    return UtilidadesCommons.cloneDate(fechaLlamada);
  }

  /**
   * Sets the fecha llamada.
   *
   * @param fechaLlamada the new fecha llamada
   */
  public void setFechaLlamada(Date fechaLlamada) {
    this.fechaLlamada = UtilidadesCommons.cloneDate(fechaLlamada);
  }

  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return observaciones;
  }

  /**
   * Sets the observaciones.
   *
   * @param observaciones the new observaciones
   */
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }
}
