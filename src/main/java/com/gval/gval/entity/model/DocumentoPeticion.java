package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import jakarta.persistence.*;

/**
 * The Class DocumentoPeticion.
 */
@Entity
@Table(name = "SDM_DOCUPETICION")
public class DocumentoPeticion implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 7270751505127034133L;

  /** The pk docupeticion. */
  @Id
  @Column(name = "PK_DOCUPETICION", unique = true, nullable = false)
  private long pkDocupeticion;

  /** The activo. */
  @Column(name = "DOACTIVO")
  private Boolean activo;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "DOFECHACREA", nullable = false)
  private Date fechaCreacion;

  /** The fecha completo. */
  @Temporal(TemporalType.DATE)
  @Column(name = "DOFECHCOMP")
  private Date fechaCompleto;

  /** The documento peticion aportados. */
  // bi-directional many-to-one association to SdmDocupedidoapor
  @OneToMany(mappedBy = "documentoPeticion")
  private List<DocumentoPeticionAportado> documentoPeticionAportados;


  /** The documento. */
  // bi-directional many-to-one association to documento
  @ManyToOne
  @JoinColumn(name = "PK_DOCU", nullable = false)
  private Documento documento;

  /** The usuario. */
  // bi-directional many-to-one association usuario
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA", nullable = false)
  private Usuario usuario;

  /**
   * Instantiates a new documento peticion.
   */
  public DocumentoPeticion() {
    super();
  }

  /**
   * Gets the pk docupeticion.
   *
   * @return the pk docupeticion
   */
  public long getPkDocupeticion() {
    return pkDocupeticion;
  }

  /**
   * Sets the pk docupeticion.
   *
   * @param pkDocupeticion the new pk docupeticion
   */
  public void setPkDocupeticion(final long pkDocupeticion) {
    this.pkDocupeticion = pkDocupeticion;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return fechaCreacion;
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

  /**
   * Gets the fecha completo.
   *
   * @return the fecha completo
   */
  public Date getFechaCompleto() {
    return fechaCompleto;
  }

  /**
   * Sets the fecha completo.
   *
   * @param fechaCompleto the new fecha completo
   */
  public void setFechaCompleto(final Date fechaCompleto) {
    this.fechaCompleto = fechaCompleto;
  }

  /**
   * Gets the documento.
   *
   * @return the documento
   */
  public Documento getDocumento() {
    return documento;
  }

  /**
   * Sets the documento.
   *
   * @param documento the new documento
   */
  public void setDocumento(final Documento documento) {
    this.documento = documento;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the documento peticion aportados.
   *
   * @return the documento peticion aportados
   */
  public List<DocumentoPeticionAportado> getDocumentoPeticionAportados() {
    return documentoPeticionAportados;
  }

  /**
   * Sets the documento peticion aportados.
   *
   * @param documentoPeticionAportados the new documento peticion aportados
   */
  public void setDocumentoPeticionAportados(
      final List<DocumentoPeticionAportado> documentoPeticionAportados) {
    this.documentoPeticionAportados = documentoPeticionAportados;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
