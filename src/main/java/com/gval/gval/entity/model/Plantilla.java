package com.gval.gval.entity.model;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.gval.gval.common.UtilidadesCommons;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the SDM_PLANTILL database table.
 *
 */
@Entity
@Table(name = "SDM_PLANTILL")
public class Plantilla implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk plantilla. */
  @Id
  @Column(name = "PK_PLANTILL", unique = true, nullable = false)
  private Long pkPlantilla;

  /** The activo. */
  @Column(name = "PLACTIVO", nullable = false)
  private Boolean activo;

  /** The codigo. */
  @Column(name = "PLCODIGO", nullable = false, length = 15)
  private String codigo;

  /** The fecha hasta. */
  @Temporal(TemporalType.DATE)
  @Column(name = "PLFECHASTA")
  private Date fechaHasta;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "PLFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The fecha desde. */
  @Temporal(TemporalType.DATE)
  @Column(name = "PLFECHDESD", nullable = false)
  private Date fechaDesde;

  /** The fichero. */
  @Lob
  @Column(name = "PLFICH")
  private byte[] fichero;

  /** The nombre. */
  @Column(name = "PLNOMBRE", length = 80)
  private String nombre;

  /** The documento plantilla. */
  @Column(name = "PLPLANTILL", length = 100)
  private String documentoPlantilla;

  /** The tipo documento. */
  // bi-directional many-to-one association to SdxTipodocu
  @ManyToOne
  @JoinColumn(name = "PK_TIPODOCU", nullable = false)
  private TipoDocumento tipoDocumento;


  /**
   * Instantiates a new plantilla.
   */
  public Plantilla() {
    // Empty method
  }

  /**
   * Instantiates a new plantilla.
   *
   * @param pkPlantilla the pk plantilla
   * @param activo the activo
   * @param codigo the codigo
   * @param fechaHasta the fecha hasta
   * @param fechaCreacion the fecha creacion
   * @param fechaDesde the fecha desde
   * @param fichero the fichero
   * @param nombre the nombre
   * @param documentoPlantilla the documento plantilla
   * @param tipoDocumento the tipo documento
   */
  public Plantilla(final Long pkPlantilla, final Boolean activo,
      final String codigo, final Date fechaHasta, final Date fechaCreacion,
      final Date fechaDesde, final byte[] fichero, final String nombre,
      final String documentoPlantilla, final TipoDocumento tipoDocumento) {
    super();
    this.pkPlantilla = pkPlantilla;
    this.activo = activo;
    this.codigo = codigo;
    this.fechaHasta = fechaHasta;
    this.fechaCreacion = fechaCreacion;
    this.fechaDesde = fechaDesde;
    this.fichero = fichero;
    this.nombre = nombre;
    this.documentoPlantilla = documentoPlantilla;
    this.tipoDocumento = tipoDocumento;
  }

  /**
   * Gets the pk plantilla.
   *
   * @return the pk plantilla
   */
  public Long getPkPlantilla() {
    return pkPlantilla;
  }


  /**
   * Sets the pk plantilla.
   *
   * @param pkPlantilla the new pk plantilla
   */
  public void setPkPlantilla(final Long pkPlantilla) {
    this.pkPlantilla = pkPlantilla;
  }


  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }


  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }


  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the fecha hasta.
   *
   * @return the fecha hasta
   */
  public Date getFechaHasta() {
    return UtilidadesCommons.cloneDate(fechaHasta);
  }


  /**
   * Sets the fecha hasta.
   *
   * @param fechaHasta the new fecha hasta
   */
  public void setFechaHasta(final Date fechaHasta) {
    this.fechaHasta = UtilidadesCommons.cloneDate(fechaHasta);
  }


  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }


  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }


  /**
   * Gets the fecha desde.
   *
   * @return the fecha desde
   */
  public Date getFechaDesde() {
    return UtilidadesCommons.cloneDate(fechaDesde);
  }


  /**
   * Sets the fecha desde.
   *
   * @param fechaDesde the new fecha desde
   */
  public void setFechaDesde(final Date fechaDesde) {
    this.fechaDesde = UtilidadesCommons.cloneDate(fechaDesde);
  }


  /**
   * Gets the fichero.
   *
   * @return the fichero
   */
  public byte[] getFichero() {
    return UtilidadesCommons.cloneBytes(fichero);
  }


  /**
   * Sets the fichero.
   *
   * @param fichero the new fichero
   */
  public void setFichero(final byte[] fichero) {
    this.fichero = UtilidadesCommons.cloneBytes(fichero);
  }


  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }


  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }


  /**
   * Gets the documento plantilla.
   *
   * @return the documento plantilla
   */
  public String getDocumentoPlantilla() {
    return documentoPlantilla;
  }


  /**
   * Sets the documento plantilla.
   *
   * @param documentoPlantilla the new documento plantilla
   */
  public void setDocumentoPlantilla(final String documentoPlantilla) {
    this.documentoPlantilla = documentoPlantilla;
  }


  /**
   * Gets the tipo documento.
   *
   * @return the tipo documento
   */
  public TipoDocumento getTipoDocumento() {
    return tipoDocumento;
  }


  /**
   * Sets the tipo documento.
   *
   * @param tipoDocumento the new tipo documento
   */
  public void setTipoDocumento(final TipoDocumento tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
