package com.gval.gval.entity.model;


import com.gval.gval.common.UtilidadesCommons;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;
import jakarta.persistence.*;
import jakarta.persistence.*;


/**
 * The persistent class for the SDM_FIRMA database table.
 *
 */
@Entity
@Table(name = "SDM_FIRMA")
public class Firma implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk firma. */
  @Id
  @Column(name = "PK_FIRMA", unique = true, nullable = false)
  private Long pkFirma;

  /** The activo. */
  @Column(name = "FIACTIVO", nullable = false)
  private Boolean activo;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "FIFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The fecha desde. */
  @Temporal(TemporalType.DATE)
  @Column(name = "FIFECHDESD", nullable = false)
  private Date fechaDesde;

  /** The fecha hasta. */
  @Temporal(TemporalType.DATE)
  @Column(name = "FIFECHHAST")
  private Date fechaHasta;

  /** The imagen firma. */
  @Lob
  @Column(name = "FIIMAGFIRM")
  private byte[] imagenFirma;

  /** The nombre. */
  @Column(name = "FINOMBRE", nullable = false, length = 150)
  private String nombre;

  /** The firmante. */
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA")
  private Usuario firmante;


  /**
   * Instantiates a new firma.
   */
  public Firma() {
    // Empty constructor
  }


  /**
   * Gets the pk firma.
   *
   * @return the pk firma
   */
  public Long getPkFirma() {
    return pkFirma;
  }


  /**
   * Sets the pk firma.
   *
   * @param pkFirma the new pk firma
   */
  public void setPkFirma(final Long pkFirma) {
    this.pkFirma = pkFirma;
  }


  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }


  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }


  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }


  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }


  /**
   * Gets the fecha desde.
   *
   * @return the fecha desde
   */
  public Date getFechaDesde() {
    return UtilidadesCommons.cloneDate(fechaDesde);
  }


  /**
   * Sets the fecha desde.
   *
   * @param fechaDesde the new fecha desde
   */
  public void setFechaDesde(final Date fechaDesde) {
    this.fechaDesde = UtilidadesCommons.cloneDate(fechaDesde);
  }


  /**
   * Gets the fecha hasta.
   *
   * @return the fecha hasta
   */
  public Date getFechaHasta() {
    return UtilidadesCommons.cloneDate(fechaHasta);
  }


  /**
   * Sets the fecha hasta.
   *
   * @param fechaHasta the new fecha hasta
   */
  public void setFechaHasta(final Date fechaHasta) {
    this.fechaHasta = UtilidadesCommons.cloneDate(fechaHasta);
  }


  /**
   * Gets the imagen firma.
   *
   * @return the imagen firma
   */
  public byte[] getImagenFirma() {
    return UtilidadesCommons.cloneBytes(imagenFirma);
  }


  /**
   * Sets the imagen firma.
   *
   * @param imagenFirma the new imagen firma
   */
  public void setImagenFirma(final byte[] imagenFirma) {
    this.imagenFirma = UtilidadesCommons.cloneBytes(imagenFirma);
  }


  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }


  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }


  /**
   * Gets the firmante.
   *
   * @return the firmante
   */
  public Usuario getFirmante() {
    return firmante;
  }


  /**
   * Sets the firmante.
   *
   * @param firmante the new firmante
   */
  public void setFirmante(final Usuario firmante) {
    this.firmante = firmante;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
