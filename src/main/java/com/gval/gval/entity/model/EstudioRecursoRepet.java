package com.gval.gval.entity.model;

import java.io.Serializable;

import jakarta.persistence.*;


/**
 * The persistent class for the SDM_ESTUDIORECURSOREPET database table.
 *
 */
@Entity
@Table(name = "SDM_ESTUDIORECURSOREPET")
public class EstudioRecursoRepet implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk estudio. */
  @Id
  @Column(name = "PK_ESTUDIO", unique = true, nullable = false)
  private Long pkEstudio;

  /** The esactivo. */
  @Column(name = "ESACTIVO", nullable = false)
  private Boolean activo;

  /** The estudio 1. */
  // bi-directional many-to-one association to SdmEstudio
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_ESTUDIO_ANTERIOR", nullable = false)
  private Estudio estudioAnterior;

  /** The estudio 2. */
  // bi-directional many-to-one association to SdmEstudio
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_ESTUDIO_INICIAL", nullable = false)
  private Estudio estudioInicial;

  /**
   * Instantiates a new estudio recurso repet.
   */
  public EstudioRecursoRepet() {
    // Empty constructor
  }

  /**
   * Gets the pk estudio.
   *
   * @return the pk estudio
   */
  public Long getPkEstudio() {
    return this.pkEstudio;
  }

  /**
   * Sets the pk estudio.
   *
   * @param pkEstudio the new pk estudio
   */
  public void setPkEstudio(final Long pkEstudio) {
    this.pkEstudio = pkEstudio;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return this.activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the estudio anterior.
   *
   * @return the estudio anterior
   */
  public Estudio getEstudioAnterior() {
    return this.estudioAnterior;
  }

  /**
   * Sets the estudio anterior.
   *
   * @param estudioAnterior the new estudio anterior
   */
  public void setEstudioAnterior(final Estudio estudioAnterior) {
    this.estudioAnterior = estudioAnterior;
  }

  /**
   * Gets the estudio inicial.
   *
   * @return the estudio inicial
   */
  public Estudio getEstudioInicial() {
    return this.estudioInicial;
  }

  /**
   * Sets the estudio inicial.
   *
   * @param estudioInicial the new estudio inicial
   */
  public void setEstudioInicial(final Estudio estudioInicial) {
    this.estudioInicial = estudioInicial;
  }
}
