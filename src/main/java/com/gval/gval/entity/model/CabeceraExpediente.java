package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;

/**
 * The Class ExpedienteDTO.
 */
@Entity
@Table(name = "SDV_CABECERA_EXPEDIENTE")
public class CabeceraExpediente implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1175256989101541094L;

  /** The pk expediente. */
  @Id
  @Column(name = "PK_EXPEDIEN", unique = true, nullable = false)
  private Long pkExpediente;

  /** The codigo expediente. */
  @Column(name = "EXPEDIENTE")
  private String codigoExpediente;

  /** The identificador solicitante. */
  @Column(name = "IDENTIFICADOR")
  private String identificadorSolicitante;

  /** The nombre solicitante. */
  @Column(name = "SOLICITANTE")
  private String nombreCompletoSolicitante;

  /** The fecha registro. */
  @Column(name = "FECHA_REGISTRO")
  private Date fechaRegistro;

  /** The ultimo grado. */
  @Column(name = "ULTIMO_GRADO")
  private String ultimoGrado;

  /** The fallecido. */
  @Column(name = "FALLECIDO")
  private String fallecido;

  /** The activo. */
  @Column(name = "ACTIVO")
  private String activo;

  /** The pia 1. */
  @Column(name = "PIA_1")
  private String pia1;

  /** The pia 2. */
  @Column(name = "PIA_2")
  private String pia2;

  /** The tiene teleasistencia. */
  @Column(name = "TELEASISTENCIA")
  private String tieneTeleasistencia;

  /** The traslado. */
  @Column(name = "TRASLADO")
  private String traslado;

  /** The ingresado. */
  @Column(name = "INGRESADO")
  private String ingresado;

  /** The urgencia. */
  @Column(name = "URGENCIA")
  private String urgencia;

  /** The responsabilidad patrimonial. */
  @Column(name = "RESP_PATRIMONIAL")
  private String responsabilidadPatrimonial;

  /** The archivado. */
  @Column(name = "ARCHIVADO")
  private String archivado;

  /** The codigo solicitante sidep. */
  @Column(name = "ID_STE_SIDEP")
  private String codigoSolicitanteSidep;

  /** The atendido. */
  @Column(name = "ATENDIDO")
  private String atendido;

  /** The exclusion imserso. */
  @Column(name = "LISTA_EXCLUSION")
  private String exclusionImserso;


  /**
   * Instantiates a new cabecera expediente DTO.
   */
  public CabeceraExpediente() {
    super();
  }

  /**
   * Gets the pk expediente.
   *
   * @return the pk expediente
   */
  public Long getPkExpediente() {
    return pkExpediente;
  }

  /**
   * Sets the pk expediente.
   *
   * @param pkExpediente the new pk expediente
   */
  public void setPkExpediente(final Long pkExpediente) {
    this.pkExpediente = pkExpediente;
  }

  /**
   * Gets the codigo expediente.
   *
   * @return the codigo expediente
   */
  public String getCodigoExpediente() {
    return codigoExpediente;
  }

  /**
   * Sets the codigo expediente.
   *
   * @param codigoExpediente the new codigo expediente
   */
  public void setCodigoExpediente(final String codigoExpediente) {
    this.codigoExpediente = codigoExpediente;
  }

  /**
   * Gets the identificador solicitante.
   *
   * @return the identificador solicitante
   */
  public String getIdentificadorSolicitante() {
    return identificadorSolicitante;
  }

  /**
   * Sets the identificador solicitante.
   *
   * @param identificadorSolicitante the new identificador solicitante
   */
  public void setIdentificadorSolicitante(
      final String identificadorSolicitante) {
    this.identificadorSolicitante = identificadorSolicitante;
  }

  /**
   * Gets the nombre completo solicitante.
   *
   * @return the nombre completo solicitante
   */
  public String getNombreCompletoSolicitante() {
    return nombreCompletoSolicitante;
  }

  /**
   * Sets the nombre completo solicitante.
   *
   * @param nombreCompletoSolicitante the new nombre completo solicitante
   */
  public void setNombreCompletoSolicitante(
      final String nombreCompletoSolicitante) {
    this.nombreCompletoSolicitante = nombreCompletoSolicitante;
  }

  /**
   * Gets the fecha registro.
   *
   * @return the fecha registro
   */
  public Date getFechaRegistro() {
    return UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Sets the fecha registro.
   *
   * @param fechaRegistro the new fecha registro
   */
  public void setFechaRegistro(final Date fechaRegistro) {
    this.fechaRegistro = UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Gets the ultimo grado.
   *
   * @return the ultimo grado
   */
  public String getUltimoGrado() {
    return ultimoGrado;
  }

  /**
   * Sets the ultimo grado.
   *
   * @param ultimoGrado the new ultimo grado
   */
  public void setUltimoGrado(final String ultimoGrado) {
    this.ultimoGrado = ultimoGrado;
  }

  /**
   * Gets the fallecido.
   *
   * @return the fallecido
   */
  public String getFallecido() {
    return fallecido;
  }

  /**
   * Sets the fallecido.
   *
   * @param fallecido the new fallecido
   */
  public void setFallecido(final String fallecido) {
    this.fallecido = fallecido;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public String getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final String activo) {
    this.activo = activo;
  }

  /**
   * Gets the pia 1.
   *
   * @return the pia 1
   */
  public String getPia1() {
    return pia1;
  }

  /**
   * Sets the pia 1.
   *
   * @param pia1 the new pia 1
   */
  public void setPia1(final String pia1) {
    this.pia1 = pia1;
  }

  /**
   * Gets the pia 2.
   *
   * @return the pia 2
   */
  public String getPia2() {
    return pia2;
  }

  /**
   * Sets the pia 2.
   *
   * @param pia2 the new pia 2
   */
  public void setPia2(final String pia2) {
    this.pia2 = pia2;
  }

  /**
   * Gets the tiene teleasistencia.
   *
   * @return the tiene teleasistencia
   */
  public String getTieneTeleasistencia() {
    return tieneTeleasistencia;
  }

  /**
   * Sets the tiene teleasistencia.
   *
   * @param tieneTeleasistencia the new tiene teleasistencia
   */
  public void setTieneTeleasistencia(final String tieneTeleasistencia) {
    this.tieneTeleasistencia = tieneTeleasistencia;
  }

  /**
   * Gets the traslado.
   *
   * @return the traslado
   */
  public String getTraslado() {
    return traslado;
  }

  /**
   * Sets the traslado.
   *
   * @param traslado the new traslado
   */
  public void setTraslado(final String traslado) {
    this.traslado = traslado;
  }

  /**
   * Gets the ingresado.
   *
   * @return the ingresado
   */
  public String getIngresado() {
    return ingresado;
  }

  /**
   * Sets the ingresado.
   *
   * @param ingresado the new ingresado
   */
  public void setIngresado(final String ingresado) {
    this.ingresado = ingresado;
  }

  /**
   * Gets the urgencia.
   *
   * @return the urgencia
   */
  public String getUrgencia() {
    return urgencia;
  }

  /**
   * Sets the urgencia.
   *
   * @param urgencia the new urgencia
   */
  public void setUrgencia(final String urgencia) {
    this.urgencia = urgencia;
  }

  /**
   * Gets the responsabilidad patrimonial.
   *
   * @return the responsabilidad patrimonial
   */
  public String getResponsabilidadPatrimonial() {
    return responsabilidadPatrimonial;
  }

  /**
   * Sets the responsabilidad patrimonial.
   *
   * @param responsabilidadPatrimonial the new responsabilidad patrimonial
   */
  public void setResponsabilidadPatrimonial(
      final String responsabilidadPatrimonial) {
    this.responsabilidadPatrimonial = responsabilidadPatrimonial;
  }

  /**
   * Gets the archivado.
   *
   * @return the archivado
   */
  public String getArchivado() {
    return archivado;
  }

  /**
   * Sets the archivado.
   *
   * @param archivado the new archivado
   */
  public void setArchivado(final String archivado) {
    this.archivado = archivado;
  }

  /**
   * Gets the codigo solicitante sidep.
   *
   * @return the codigo solicitante sidep
   */
  public String getCodigoSolicitanteSidep() {
    return codigoSolicitanteSidep;
  }

  /**
   * Sets the codigo solicitante sidep.
   *
   * @param codigoSolicitanteSidep the new codigo solicitante sidep
   */
  public void setCodigoSolicitanteSidep(final String codigoSolicitanteSidep) {
    this.codigoSolicitanteSidep = codigoSolicitanteSidep;
  }

  /**
   * Gets the atendido.
   *
   * @return the atendido
   */
  public String getAtendido() {
    return atendido;
  }

  /**
   * Sets the atendido.
   *
   * @param atendido the new atendido
   */
  public void setAtendido(final String atendido) {
    this.atendido = atendido;
  }



  /**
   * Gets the exclusion imserso.
   *
   * @return the exclusion imserso
   */
  public String getExclusionImserso() {
    return exclusionImserso;
  }

  /**
   * Sets the exclusion imserso.
   *
   * @param exclusionImserso the new exclusion imserso
   */
  public void setExclusionImserso(String exclusionImserso) {
    this.exclusionImserso = exclusionImserso;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
