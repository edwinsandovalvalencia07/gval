package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;

import jakarta.persistence.*;


/**
 * The persistent class for the SDM_CONVIVEN database table.
 *
 */
@Entity
@Table(name = "SDM_CONVIVEN")
@GenericGenerator(name = "SDM_SDM_CONVIVEN_PKSDM_CONVIVEN_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_CONVIVEN"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public final class ConvivenciaInformeSocial implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 7978156874497162535L;

  /** The pk conviven. */
  @Id
  @Column(name = "PK_CONVIVEN", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_SDM_CONVIVEN_PKSDM_CONVIVEN_GENERATOR")
  private Long pkConviven;

  /** The ayuda administracion. */
  @Column(name = "COAYBIEPAT")
  private Boolean ayudaAdministracion;

  /** The ayuda comunicacion. */
  @Column(name = "COAYCOMENT")
  private Boolean ayudaComunicacion;

  /** The ayuda decisiones. */
  @Column(name = "COAYDECCNP")
  private Boolean ayudaDecisiones;

  /** The ayuda desplazamiento. */
  @Column(name = "COAYDESP")
  private Boolean ayudaDesplazamiento;

  /** The ayuda higiene. */
  @Column(name = "COAYHIASEO")
  private Boolean ayudaHigiene;

  /** The ayuda conflictos. */
  @Column(name = "COAYRESSIT")
  private Boolean ayudaConflictos;

  /** The tipo cuidador. */
  @Column(name = "COCUIDADOR", nullable = false)
  private Integer tipoCuidador;

  /** The cuidados hogar. */
  @Column(name = "COCUIDHOGA")
  private Boolean cuidadosHogar;

  /** The cuidados otros. */
  @Column(name = "COCUIDOTRO")
  private Boolean cuidadosOtros;

  /** The edad. */
  @Column(name = "COEDAD")
  private Integer edad;

  /** The intensidad. */
  @Column(name = "COINTENSID", length = 1)
  private String intensidad;

  /** The nombre. */
  @Column(name = "CONOMBRE", length = 50)
  private String nombre;

  /** The nombre otros dependientes. */
  @Column(name = "CONOOTDE", length = 200)
  private String nombreOtrosDependientes;

  /** The cuida otros dependientes. */
  @Column(name = "COOTRODEP")
  private Boolean cuidaOtrosDependientes;

  /** The principal. */
  @Column(name = "COPRINCIPA")
  private Boolean principal;

  /** The tipo jornada. */
  @Column(name = "COTIPJORN")
  private Integer tipoJornada;

  /** The tipo trabajo. */
  @Column(name = "COTIPTRAB")
  private Integer tipoTrabajo;

  /** The trabajo fuera hogar. */
  @Column(name = "COTRABFUE")
  private Boolean trabajoFueraHogar;

  /** The informe social. */
  // bi-directional many-to-one association to SdmInfosoci
  @ManyToOne
  @JoinColumn(name = "PK_INFOSOCI", nullable = false)
  private InformeSocial informeSocial;

  /** The cuidador relacion otros dependientes. */
  @ManyToOne
  @JoinColumn(name = "SDX_PK_TIPOFAMI")
  private TipoFamiliar cuidadorRelacionOtrosDependientes;

  /** The cuidador relacion principal. */
  @ManyToOne
  @JoinColumn(name = "PK_TIPOFAMI")
  private TipoFamiliar cuidadorRelacionPrincipal;

  /**
   * Instantiates a new convivencia informe social.
   */
  public ConvivenciaInformeSocial() {
    // Constructor vacío
  }

  /**
   * Gets the pk conviven.
   *
   * @return the pkConviven
   */
  public Long getPkConviven() {
    return pkConviven;
  }

  /**
   * Sets the pk conviven.
   *
   * @param pkConviven the pkConviven to set
   */
  public void setPkConviven(final Long pkConviven) {
    this.pkConviven = pkConviven;
  }

  /**
   * Gets the ayuda administracion.
   *
   * @return the ayudaAdministracion
   */
  public Boolean getAyudaAdministracion() {
    return ayudaAdministracion;
  }

  /**
   * Sets the ayuda administracion.
   *
   * @param ayudaAdministracion the ayudaAdministracion to set
   */
  public void setAyudaAdministracion(final Boolean ayudaAdministracion) {
    this.ayudaAdministracion = ayudaAdministracion;
  }

  /**
   * Gets the ayuda comunicacion.
   *
   * @return the ayudaComunicacion
   */
  public Boolean getAyudaComunicacion() {
    return ayudaComunicacion;
  }

  /**
   * Sets the ayuda comunicacion.
   *
   * @param ayudaComunicacion the ayudaComunicacion to set
   */
  public void setAyudaComunicacion(final Boolean ayudaComunicacion) {
    this.ayudaComunicacion = ayudaComunicacion;
  }

  /**
   * Gets the ayuda decisiones.
   *
   * @return the ayudaDecisiones
   */
  public Boolean getAyudaDecisiones() {
    return ayudaDecisiones;
  }

  /**
   * Sets the ayuda decisiones.
   *
   * @param ayudaDecisiones the ayudaDecisiones to set
   */
  public void setAyudaDecisiones(final Boolean ayudaDecisiones) {
    this.ayudaDecisiones = ayudaDecisiones;
  }

  /**
   * Gets the ayuda desplazamiento.
   *
   * @return the ayudaDesplazamiento
   */
  public Boolean getAyudaDesplazamiento() {
    return ayudaDesplazamiento;
  }

  /**
   * Sets the ayuda desplazamiento.
   *
   * @param ayudaDesplazamiento the ayudaDesplazamiento to set
   */
  public void setAyudaDesplazamiento(final Boolean ayudaDesplazamiento) {
    this.ayudaDesplazamiento = ayudaDesplazamiento;
  }

  /**
   * Gets the ayuda higiene.
   *
   * @return the ayudaHigiene
   */
  public Boolean getAyudaHigiene() {
    return ayudaHigiene;
  }

  /**
   * Sets the ayuda higiene.
   *
   * @param ayudaHigiene the ayudaHigiene to set
   */
  public void setAyudaHigiene(final Boolean ayudaHigiene) {
    this.ayudaHigiene = ayudaHigiene;
  }

  /**
   * Gets the ayuda conflictos.
   *
   * @return the ayudaConflictos
   */
  public Boolean getAyudaConflictos() {
    return ayudaConflictos;
  }

  /**
   * Sets the ayuda conflictos.
   *
   * @param ayudaConflictos the ayudaConflictos to set
   */
  public void setAyudaConflictos(final Boolean ayudaConflictos) {
    this.ayudaConflictos = ayudaConflictos;
  }

  /**
   * Gets the tipo cuidador.
   *
   * @return the tipoCuidador
   */
  public Integer getTipoCuidador() {
    return tipoCuidador;
  }

  /**
   * Sets the tipo cuidador.
   *
   * @param tipoCuidador the tipoCuidador to set
   */
  public void setTipoCuidador(final Integer tipoCuidador) {
    this.tipoCuidador = tipoCuidador;
  }

  /**
   * Gets the cuidados hogar.
   *
   * @return the cuidadosHogar
   */
  public Boolean getCuidadosHogar() {
    return cuidadosHogar;
  }

  /**
   * Sets the cuidados hogar.
   *
   * @param cuidadosHogar the cuidadosHogar to set
   */
  public void setCuidadosHogar(final Boolean cuidadosHogar) {
    this.cuidadosHogar = cuidadosHogar;
  }

  /**
   * Gets the cuidados otros.
   *
   * @return the cuidadosOtros
   */
  public Boolean getCuidadosOtros() {
    return cuidadosOtros;
  }

  /**
   * Sets the cuidados otros.
   *
   * @param cuidadosOtros the cuidadosOtros to set
   */
  public void setCuidadosOtros(final Boolean cuidadosOtros) {
    this.cuidadosOtros = cuidadosOtros;
  }

  /**
   * Gets the edad.
   *
   * @return the edad
   */
  public Integer getEdad() {
    return edad;
  }

  /**
   * Sets the edad.
   *
   * @param edad the edad to set
   */
  public void setEdad(final Integer edad) {
    this.edad = edad;
  }

  /**
   * Gets the intensidad.
   *
   * @return the intensidad
   */
  public String getIntensidad() {
    return intensidad;
  }

  /**
   * Sets the intensidad.
   *
   * @param intensidad the intensidad to set
   */
  public void setIntensidad(final String intensidad) {
    this.intensidad = intensidad;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the nombre to set
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the nombre otros dependientes.
   *
   * @return the nombreOtrosDependientes
   */
  public String getNombreOtrosDependientes() {
    return nombreOtrosDependientes;
  }

  /**
   * Sets the nombre otros dependientes.
   *
   * @param numeroOtroDeootde the new nombre otros dependientes
   */
  public void setNombreOtrosDependientes(final String numeroOtroDeootde) {
    this.nombreOtrosDependientes = numeroOtroDeootde;
  }



  /**
   * Gets the principal.
   *
   * @return the principal
   */
  public Boolean getPrincipal() {
    return principal;
  }

  /**
   * Sets the principal.
   *
   * @param principal the principal to set
   */
  public void setPrincipal(final Boolean principal) {
    this.principal = principal;
  }

  /**
   * Gets the tipo jornada.
   *
   * @return the tipoJornada
   */
  public Integer getTipoJornada() {
    return tipoJornada;
  }

  /**
   * Sets the tipo jornada.
   *
   * @param tipoJornada the tipoJornada to set
   */
  public void setTipoJornada(final Integer tipoJornada) {
    this.tipoJornada = tipoJornada;
  }

  /**
   * Gets the tipo trabajo.
   *
   * @return the tipoTrabajo
   */
  public Integer getTipoTrabajo() {
    return tipoTrabajo;
  }

  /**
   * Sets the tipo trabajo.
   *
   * @param tipoTrabajo the tipoTrabajo to set
   */
  public void setTipoTrabajo(final Integer tipoTrabajo) {
    this.tipoTrabajo = tipoTrabajo;
  }

  /**
   * Gets the trabajo fuera hogar.
   *
   * @return the trabajoFueraHogar
   */
  public Boolean getTrabajoFueraHogar() {
    return trabajoFueraHogar;
  }

  /**
   * Sets the trabajo fuera hogar.
   *
   * @param trabajoFueraHogar the trabajoFueraHogar to set
   */
  public void setTrabajoFueraHogar(final Boolean trabajoFueraHogar) {
    this.trabajoFueraHogar = trabajoFueraHogar;
  }

  /**
   * Gets the informe social.
   *
   * @return the informeSocial
   */
  public InformeSocial getInformeSocial() {
    return informeSocial;
  }

  /**
   * Sets the informe social.
   *
   * @param informeSocial the informeSocial to set
   */
  public void setInformeSocial(final InformeSocial informeSocial) {
    this.informeSocial = informeSocial;
  }

  /**
   * Gets the cuida otros dependientes.
   *
   * @return the cuidaOtrosDependientes
   */
  public Boolean getCuidaOtrosDependientes() {
    return cuidaOtrosDependientes;
  }

  /**
   * Sets the cuida otros dependientes.
   *
   * @param cuidaOtrosDependientes the cuidaOtrosDependientes to set
   */
  public void setCuidaOtrosDependientes(final Boolean cuidaOtrosDependientes) {
    this.cuidaOtrosDependientes = cuidaOtrosDependientes;
  }

  /**
   * Gets the cuidador relacion otros dependientes.
   *
   * @return the cuidadorRelacionOtrosDependientes
   */
  public TipoFamiliar getCuidadorRelacionOtrosDependientes() {
    return cuidadorRelacionOtrosDependientes;
  }

  /**
   * Sets the cuidador relacion otros dependientes.
   *
   * @param cuidadorRelacionOtrosDependientes the
   *        cuidadorRelacionOtrosDependientes to set
   */
  public void setCuidadorRelacionOtrosDependientes(
      final TipoFamiliar cuidadorRelacionOtrosDependientes) {
    this.cuidadorRelacionOtrosDependientes = cuidadorRelacionOtrosDependientes;
  }

  /**
   * Gets the cuidador relacion principal.
   *
   * @return the cuidadorRelacionPrincipal
   */
  public TipoFamiliar getCuidadorRelacionPrincipal() {
    return cuidadorRelacionPrincipal;
  }

  /**
   * Sets the cuidador relacion principal.
   *
   * @param cuidadorRelacionPrincipals the new cuidador relacion principal
   */
  public void setCuidadorRelacionPrincipal(
      final TipoFamiliar cuidadorRelacionPrincipals) {
    this.cuidadorRelacionPrincipal = cuidadorRelacionPrincipals;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
