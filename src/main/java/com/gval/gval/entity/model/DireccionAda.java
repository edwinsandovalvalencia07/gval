package com.gval.gval.entity.model;

import com.gval.gval.common.ConstantesCommons;
import com.gval.gval.common.UtilidadesCommons;

import com.gval.gval.entity.vistas.model.Centro;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


import jakarta.persistence.*;



/**
 * The Class DireccionAda.
 */
@Entity
@Table(name = "SDM_DIRECCIO")
@GenericGenerator(name = "SDM_DIRECCIO_PKDIRECCIO_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_DIRECCIO"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class DireccionAda implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -3137431290196352955L;

  /** The pk direccion. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_DIRECCIO_PKDIRECCIO_GENERATOR")
  @Column(name = "PK_DIRECCIO", unique = true, nullable = false)
  private Long pkDireccion;

  /** The activo. */
  @Column(name = "DIACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /** The bloque. */
  @Column(name = "DIBLOQUE", length = 5)
  private String bloque;

  /** The centro servicios sociales. */
  @Column(name = "DICENTROSERVSOC", length = 200)
  private String nombreCentroServiciosSociales;

  /** The direccion extranjera. */
  @Column(name = "DIDIREEXTR", length = 500)
  private String direccionExtranjera;

  /** The email. */
  @Column(name = "DIEMAIL", length = 200)
  private String email;

  /** The escalera. */
  @Column(name = "DIESCALERA", length = 5)
  private String escalera;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "DIFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The habitual. */
  @Column(name = "DIHABITUAL", nullable = false, precision = 1)
  private Boolean habitual;

  /** The habitual anterior. */
  @Column(name = "DIHABITUALANTERIOR", precision = 1)
  private Boolean habitualAnterior;

  /** The movil. */
  @Column(name = "DIMOVIL", length = 100)
  private String movil;

  /** The nombre via. */
  @Column(name = "DINOMBVIA", length = 100)
  private String nombreVia;

  /** The notificacion. */
  @Column(name = "DINOTIFI", nullable = false, precision = 1)
  private Boolean notificacion;

  /** The numero. */
  @Column(name = "DINUMERO", length = 100)
  private String numero;

  /** The observaciones. */
  @Column(name = "DIOVSERVACIONES", length = 255)
  private String observaciones;

  /** The persona contacto. */
  @Column(name = "DIPERSCONT", length = 150)
  private String personaContacto;

  /** The piso. */
  @Column(name = "DIPISO", precision = 38)
  private BigDecimal piso;

  /** The puerta. */
  @Column(name = "DIPUERTA", length = 100)
  private String puerta;

  /** The tipo contacto. */
  @Column(name = "DITIPOCONTACTO", length = 1)
  private String tipoContacto;

  /** The tipo direccion. */
  @Column(name = "DITIPODIR", length = 50)
  private String tipoDireccion;

  /** The telefono fijo. */
  @Column(name = "DITLFNFIJO", length = 100)
  private String telefonoFijo;

  /** The telefono fijo. */
  @Column(name = "DITLFN3", length = 100)
  private String telefonoOtro;

  /** The zaguan. */
  @Column(name = "DIZAGUAN", length = 5)
  private String zaguan;

  /** The id api. */
  @Column(name = "DIR_ID")
  private Long idApi;

  /** The app origen. */
  @Column(name = "APP_ORIGEN")
  private String appOrigen;

  /** The app modifica. */
  @Column(name = "APP_MODIFICA")
  private String appModifica;


  /** The persona ada. */
  // bi-directional many-to-one association to SdmPersona
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA")
  private Persona personaAda;

  /** The codigo postal ada. */
  // bi-directional many-to-one association to CodigoPostal
  @ManyToOne
  @JoinColumn(name = "PK_CODPOS")
  private CodigoPostal codigoPostalAda;

  /** The poblacion. */
  // bi-directional many-to-one association to SdxPoblac
  @ManyToOne
  @JoinColumn(name = "PK_POBLAC")
  private Poblacion poblacion;

  /** The provincia. */
  // bi-directional many-to-one association to SdxProvinci
  @ManyToOne
  @JoinColumn(name = "PK_PROVINCI")
  private Provincia provincia;

  /** The tipo via. */
  // bi-directional many-to-one association to SdxTipovia
  @ManyToOne
  @JoinColumn(name = "PK_TIPOVIA")
  private TipoVia tipoVia;

  /** The centro. */
  // bi-directional many-to-one association to SdvCentro
  @ManyToOne
  @JoinColumn(name = "PK_CENTRO")
  @NotFound(action = NotFoundAction.IGNORE)
  private Centro centro;

  /** The unidades. */
  // bi-directional many-to-one association to SdxUnidad
  @OneToMany(mappedBy = "direccion")
  private List<Unidad> unidades;


  /** The centro servicios sociales. */
  @ManyToOne
  @JoinColumn(name = "PK_CENTRO_SERV_SOCIALES")
  private CentroServiciosSociales centroServiciosSociales;

  /** The habitual. */
  @Column(name = "diotraccaa", nullable = false, precision = 1)
  private Boolean esOtraCCAA;

  /** The portal. */
  @Column(name = "DIPORTAL", length = 5)
  private String portal;

  /**
   * On pre persist.
   */
  @PrePersist
  public void onPrePersist() {
    fechaCreacion = new Date();
    appOrigen = ConstantesCommons.APLICACION_ADA3;
    appModifica = ConstantesCommons.APLICACION_ADA3;
  }

  /**
   * On pre update.
   */
  @PreUpdate
  public void onPreUpdate() {
    appModifica = ConstantesCommons.APLICACION_ADA3;
  }

  /**
   * Instantiates a new direccion ada.
   */
  public DireccionAda() {
    // Empty constructor
  }

  /**
   * Gets the pk direccion.
   *
   * @return the pk direccion
   */
  public Long getPkDireccion() {
    return pkDireccion;
  }

  /**
   * Sets the pk direccion.
   *
   * @param pkDireccion the new pk direccion
   */
  public void setPkDireccion(final Long pkDireccion) {
    this.pkDireccion = pkDireccion;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the bloque.
   *
   * @return the bloque
   */
  public String getBloque() {
    return bloque;
  }

  /**
   * Sets the bloque.
   *
   * @param bloque the new bloque
   */
  public void setBloque(final String bloque) {
    this.bloque = bloque;
  }

  /**
   * Gets the nombre centro servicios sociales.
   *
   * @return the nombre centro servicios sociales
   */
  public String getNombreCentroServiciosSociales() {
    return nombreCentroServiciosSociales;
  }

  /**
   * Sets the nombre centro servicios sociales.
   *
   * @param nombreCentroServiciosSociales the new nombre centro servicios
   *        sociales
   */
  public void setNombreCentroServiciosSociales(
      final String nombreCentroServiciosSociales) {
    this.nombreCentroServiciosSociales = nombreCentroServiciosSociales;
  }

  /**
   * Gets the centro servicios sociales.
   *
   * @return the centro servicios sociales
   */
  public CentroServiciosSociales getCentroServiciosSociales() {
    return centroServiciosSociales;
  }

  /**
   * Sets the centro servicios sociales.
   *
   * @param centroServiciosSociales the new centro servicios sociales
   */
  public void setCentroServiciosSociales(
      final CentroServiciosSociales centroServiciosSociales) {
    this.centroServiciosSociales = centroServiciosSociales;
  }

  /**
   * Gets the direccion extranjera.
   *
   * @return the direccion extranjera
   */
  public String getDireccionExtranjera() {
    return direccionExtranjera;
  }

  /**
   * Sets the direccion extranjera.
   *
   * @param direccionExtranjera the new direccion extranjera
   */
  public void setDireccionExtranjera(final String direccionExtranjera) {
    this.direccionExtranjera = direccionExtranjera;
  }

  /**
   * Gets the email.
   *
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * Sets the email.
   *
   * @param email the new email
   */
  public void setEmail(final String email) {
    this.email = email;
  }

  /**
   * Gets the escalera.
   *
   * @return the escalera
   */
  public String getEscalera() {
    return escalera;
  }

  /**
   * Sets the escalera.
   *
   * @param escalera the new escalera
   */
  public void setEscalera(final String escalera) {
    this.escalera = escalera;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the habitual.
   *
   * @return the habitual
   */
  public Boolean getHabitual() {
    return habitual;
  }

  /**
   * Sets the habitual.
   *
   * @param habitual the new habitual
   */
  public void setHabitual(final Boolean habitual) {
    this.habitual = habitual;
  }

  /**
   * Gets the habitual anterior.
   *
   * @return the habitual anterior
   */
  public Boolean getHabitualAnterior() {
    return habitualAnterior;
  }

  /**
   * Sets the habitual anterior.
   *
   * @param habitualAnterior the new habitual anterior
   */
  public void setHabitualAnterior(final Boolean habitualAnterior) {
    this.habitualAnterior = habitualAnterior;
  }

  /**
   * Gets the es otra CCAA.
   *
   * @return the es otra CCAA
   */
  public Boolean getEsOtraCCAA() {
    return esOtraCCAA;
  }

  /**
   * Sets the es otra CCAA.
   *
   * @param esOtraCCAA the new es otra CCAA
   */
  public void setEsOtraCCAA(final Boolean esOtraCCAA) {
    this.esOtraCCAA = esOtraCCAA;
  }

  /**
   * Gets the movil.
   *
   * @return the movil
   */
  public String getMovil() {
    return movil;
  }

  /**
   * Sets the movil.
   *
   * @param movil the new movil
   */
  public void setMovil(final String movil) {
    this.movil = movil;
  }

  /**
   * Gets the nombre via.
   *
   * @return the nombre via
   */
  public String getNombreVia() {
    return nombreVia;
  }

  /**
   * Sets the nombre via.
   *
   * @param nombreVia the new nombre via
   */
  public void setNombreVia(final String nombreVia) {
    this.nombreVia = nombreVia;
  }

  /**
   * Gets the notificacion.
   *
   * @return the notificacion
   */
  public Boolean getNotificacion() {
    return notificacion;
  }

  /**
   * Sets the notificacion.
   *
   * @param notificacion the new notificacion
   */
  public void setNotificacion(final Boolean notificacion) {
    this.notificacion = notificacion;
  }

  /**
   * Gets the numero.
   *
   * @return the numero
   */
  public String getNumero() {
    return numero;
  }

  /**
   * Sets the numero.
   *
   * @param numero the new numero
   */
  public void setNumero(final String numero) {
    this.numero = numero;
  }

  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return observaciones;
  }

  /**
   * Sets the observaciones.
   *
   * @param observaciones the new observaciones
   */
  public void setObservaciones(final String observaciones) {
    this.observaciones = observaciones;
  }

  /**
   * Gets the persona contacto.
   *
   * @return the persona contacto
   */
  public String getPersonaContacto() {
    return personaContacto;
  }

  /**
   * Sets the persona contacto.
   *
   * @param personaContacto the new persona contacto
   */
  public void setPersonaContacto(final String personaContacto) {
    this.personaContacto = personaContacto;
  }

  /**
   * Gets the piso.
   *
   * @return the piso
   */
  public BigDecimal getPiso() {
    return piso;
  }

  /**
   * Sets the piso.
   *
   * @param piso the new piso
   */
  public void setPiso(final BigDecimal piso) {
    this.piso = piso;
  }

  /**
   * Gets the puerta.
   *
   * @return the puerta
   */
  public String getPuerta() {
    return puerta;
  }

  /**
   * Sets the puerta.
   *
   * @param puerta the new puerta
   */
  public void setPuerta(final String puerta) {
    this.puerta = puerta;
  }

  /**
   * Gets the tipo contacto.
   *
   * @return the tipo contacto
   */
  public String getTipoContacto() {
    return tipoContacto;
  }

  /**
   * Sets the tipo contacto.
   *
   * @param tipoContacto the new tipo contacto
   */
  public void setTipoContacto(final String tipoContacto) {
    this.tipoContacto = tipoContacto;
  }

  /**
   * Gets the tipo direccion.
   *
   * @return the tipo direccion
   */
  public String getTipoDireccion() {
    return tipoDireccion;
  }

  /**
   * Sets the tipo direccion.
   *
   * @param tipoDireccion the new tipo direccion
   */
  public void setTipoDireccion(final String tipoDireccion) {
    this.tipoDireccion = tipoDireccion;
  }

  /**
   * Gets the telefono fijo.
   *
   * @return the telefono fijo
   */
  public String getTelefonoFijo() {
    return telefonoFijo;
  }

  /**
   * Sets the telefono fijo.
   *
   * @param telefonoFijo the new telefono fijo
   */
  public void setTelefonoFijo(final String telefonoFijo) {
    this.telefonoFijo = telefonoFijo;
  }

  /**
   * Gets the zaguan.
   *
   * @return the zaguan
   */
  public String getZaguan() {
    return zaguan;
  }

  /**
   * Sets the zaguan.
   *
   * @param zaguan the new zaguan
   */
  public void setZaguan(final String zaguan) {
    this.zaguan = zaguan;
  }

  /**
   * Gets the centro.
   *
   * @return the centro
   */
  public Centro getCentro() {
    return centro;
  }

  /**
   * Sets the centro.
   *
   * @param centro the new centro
   */
  public void setCentro(final Centro centro) {
    this.centro = centro;
  }

  /**
   * Gets the persona ada.
   *
   * @return the persona ada
   */
  public Persona getPersonaAda() {
    return personaAda;
  }

  /**
   * Sets the persona ada.
   *
   * @param personaAda the new persona ada
   */
  public void setPersonaAda(final Persona personaAda) {
    this.personaAda = personaAda;
  }

  /**
   * Gets the codigo postal ada.
   *
   * @return the codigo postal ada
   */
  public CodigoPostal getCodigoPostalAda() {
    return codigoPostalAda;
  }

  /**
   * Sets the codigo postal ada.
   *
   * @param codigoPostalAda the new codigo postal ada
   */
  public void setCodigoPostalAda(final CodigoPostal codigoPostalAda) {
    this.codigoPostalAda = codigoPostalAda;
  }

  /**
   * Gets the poblacion.
   *
   * @return the poblacion
   */
  public Poblacion getPoblacion() {
    return poblacion;
  }

  /**
   * Sets the poblacion.
   *
   * @param poblacion the new poblacion
   */
  public void setPoblacion(final Poblacion poblacion) {
    this.poblacion = poblacion;
  }

  /**
   * Gets the provincia.
   *
   * @return the provincia
   */
  public Provincia getProvincia() {
    return provincia;
  }

  /**
   * Sets the provincia.
   *
   * @param provincia the new provincia
   */
  public void setProvincia(final Provincia provincia) {
    this.provincia = provincia;
  }

  /**
   * Gets the tipo via.
   *
   * @return the tipo via
   */
  public TipoVia getTipoVia() {
    return tipoVia;
  }

  /**
   * Sets the tipo via.
   *
   * @param tipoVia the new tipo via
   */
  public void setTipoVia(final TipoVia tipoVia) {
    this.tipoVia = tipoVia;
  }

  /**
   * Gets the unidades.
   *
   * @return the unidades
   */
  public List<Unidad> getUnidades() {
    return UtilidadesCommons.collectorsToList(unidades);
  }

  /**
   * Sets the unidades.
   *
   * @param unidades the new unidades
   */
  public void setUnidades(final List<Unidad> unidades) {
    this.unidades = UtilidadesCommons.collectorsToList(unidades);
  }

  /**
   * Gets the id api.
   *
   * @return the id api
   */
  public Long getIdApi() {
    return idApi;
  }

  /**
   * Sets the id api.
   *
   * @param idApi the new id api
   */
  public void setIdApi(final Long idApi) {
    this.idApi = idApi;
  }

  /**
   * Gets the app origen.
   *
   * @return the app origen
   */
  public String getAppOrigen() {
    return appOrigen;
  }

  /**
   * Sets the app origen.
   *
   * @param appOrigen the new app origen
   */
  public void setAppOrigen(final String appOrigen) {
    this.appOrigen = appOrigen;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

  /**
   * Gets the telefono otro.
   *
   * @return the telefonoOtro
   */
  public String getTelefonoOtro() {
    return telefonoOtro;
  }

  /**
   * Sets the telefono otro.
   *
   * @param telefonoOtro the telefonoOtro to set
   */
  public void setTelefonoOtro(final String telefonoOtro) {
    this.telefonoOtro = telefonoOtro;
  }

  /**
   * Gets the portal.
   *
   * @return the portal
   */
  public String getPortal() {
    return portal;
  }

  /**
   * Sets the portal.
   *
   * @param portal the new portal
   */
  public void setPortal(final String portal) {
    this.portal = portal;
  }

}
