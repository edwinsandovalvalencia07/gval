package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

import jakarta.persistence.*;


/**
 * The persistent class for the SDM_COMPATIBILIDADES_PIA database table.
 *
 */
@Entity
@Table(name = "SDM_COMPATIBILIDADES_PIA")
public class CompatibilidadesPia implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk com pia. */
  @Id
  @Column(name = "PK_COM_PIA", unique = true, nullable = false)
  private Long pkComPia;

  /** The activo. */
  @Column(nullable = false, precision = 1)
  private Boolean activo;

  /** The check grado 32. */
  @Column(name = "CHECK_GRADO_3_2", nullable = false, precision = 1)
  private Boolean checkGrado32;

  /** The pk cataserv 1. */
  // bi-directional many-to-one association to SdxCataserv
  @ManyToOne
  @JoinColumn(name = "PK_CATASERV2", nullable = false)
  private CatalogoServicio servicio2;

  /** The pk cataserv 2. */
  // bi-directional many-to-one association to SdxCataserv
  @ManyToOne
  @JoinColumn(name = "PK_CATASERV1", nullable = false)
  private CatalogoServicio servicio1;

  /**
   * Instantiates a new compatibilidades pia.
   */
  public CompatibilidadesPia() {
    super();
  }


  /**
   * Gets the pk com pia.
   *
   * @return the pk com pia
   */
  public Long getPkComPia() {
    return pkComPia;
  }

  /**
   * Sets the pk com pia.
   *
   * @param pkComPia the new pk com pia
   */
  public void setPkComPia(final Long pkComPia) {
    this.pkComPia = pkComPia;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the check grado 32.
   *
   * @return the check grado 32
   */
  public Boolean getCheckGrado32() {
    return checkGrado32;
  }

  /**
   * Sets the check grado 32.
   *
   * @param checkGrado32 the new check grado 32
   */
  public void setCheckGrado32(final Boolean checkGrado32) {
    this.checkGrado32 = checkGrado32;
  }


  /**
   * Gets the servicio 2.
   *
   * @return the servicio2
   */
  public CatalogoServicio getServicio2() {
    return servicio2;
  }


  /**
   * Sets the servicio 2.
   *
   * @param servicio2 the servicio2 to set
   */
  public void setServicio2(final CatalogoServicio servicio2) {
    this.servicio2 = servicio2;
  }


  /**
   * Gets the servicio 1.
   *
   * @return the servicio1
   */
  public CatalogoServicio getServicio1() {
    return servicio1;
  }


  /**
   * Sets the servicio 1.
   *
   * @param servicio1 the servicio1 to set
   */
  public void setServicio1(final CatalogoServicio servicio1) {
    this.servicio1 = servicio1;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
