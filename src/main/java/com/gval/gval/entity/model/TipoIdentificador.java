package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;
import jakarta.persistence.*;


/**
 * The persistent class for the SDX_TIPOIDEN database table.
 *
 */
@Entity
@Table(name = "SDX_TIPOIDEN")
public class TipoIdentificador implements Serializable {

  /** Serial version UID. */
  private static final long serialVersionUID = -1455747683717271608L;

  /** The pk tipoiden. */
  @Id
  @Column(name = "PK_TIPOIDEN", unique = true, nullable = false)
  private Long pkTipoiden;

  /** The codinss. */
  @Column(name = "CODINSS", precision = 38)
  private Long codinss;

  /** The activo. */
  @Column(name = "TIEACTIVO", nullable = false, precision = 38)
  private boolean activo;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "TIFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The codigo imserso. */
  @Column(name = "TIIMSERSO", length = 1)
  private String codigoImserso;

  /** The nombre. */
  @Column(name = "TINOMBRE", nullable = false, length = 50)
  private String nombre;

  /**
   * Instantiates a new tipo identificador.
   */
  public TipoIdentificador() {
    // Empty constructor
  }

  /**
   * Gets the pk tipoiden.
   *
   * @return the pk tipoiden
   */
  public Long getPkTipoiden() {
    return pkTipoiden;
  }

  /**
   * Sets the pk tipoiden.
   *
   * @param pkTipoiden the new pk tipoiden
   */
  public void setPkTipoiden(final Long pkTipoiden) {
    this.pkTipoiden = pkTipoiden;
  }

  /**
   * Gets the codinss.
   *
   * @return the codinss
   */
  public Long getCodinss() {
    return codinss;
  }

  /**
   * Sets the codinss.
   *
   * @param codinss the new codinss
   */
  public void setCodinss(final Long codinss) {
    this.codinss = codinss;
  }

  /**
   * Checks if is activo.
   *
   * @return true, if is activo
   */
  public boolean isActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the codigo imserso.
   *
   * @return the codigo imserso
   */
  public String getCodigoImserso() {
    return codigoImserso;
  }

  /**
   * Sets the codigo imserso.
   *
   * @param codigoImserso the new codigo imserso
   */
  public void setCodigoImserso(final String codigoImserso) {
    this.codigoImserso = codigoImserso;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
