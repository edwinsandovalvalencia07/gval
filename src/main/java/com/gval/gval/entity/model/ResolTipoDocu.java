package com.gval.gval.entity.model;

import java.io.Serializable;

import jakarta.persistence.*;

/**
 * The Class ResolTipoDocu.
 */
@Entity
@Table(name = "SDM_RESOL_TIPODOC")
public class ResolTipoDocu implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 5902146420056939686L;

  /** The pk resoluci. */
  @Id
  @Column(name = "PK_RESOLUCI", unique = true, nullable = false)
  private Long pkResoluci;

  /** The activo. */
  @Column(name = "PK_TIPODOCU", nullable = false)
  private Long pkTipodocu;

  /** The activo. */
  @Column(name = "RRT_ACTIVO", nullable = false)
  private Boolean activo;

  /**
   * Gets the pk resoluci.
   *
   * @return the pk resoluci
   */
  public Long getPkResoluci() {
    return pkResoluci;
  }

  /**
   * Sets the pk resoluci.
   *
   * @param pkResoluci the new pk resoluci
   */
  public void setPkResoluci(final Long pkResoluci) {
    this.pkResoluci = pkResoluci;
  }

  /**
   * Gets the pk tipodocu.
   *
   * @return the pk tipodocu
   */
  public Long getPkTipodocu() {
    return pkTipodocu;
  }

  /**
   * Sets the pk tipodocu.
   *
   * @param pkTipodocu the new pk tipodocu
   */
  public void setPkTipodocu(final Long pkTipodocu) {
    this.pkTipodocu = pkTipodocu;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

}
