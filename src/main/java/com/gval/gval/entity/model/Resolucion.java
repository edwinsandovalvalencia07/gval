package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;

/**
 * The Class Resolucion.
 */
@Entity
@Table(name = "SDM_RESOLUCI")
@GenericGenerator(name = "SDM_RESOLUCI_PKRESOLUCI_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_RESOLUCI"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class Resolucion implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 5493990036126923724L;

  /** The pk resoluci. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_RESOLUCI_PKRESOLUCI_GENERATOR")
  @Column(name = "PK_RESOLUCI", unique = true, nullable = false)
  private Long pkResoluci;

  /** The solicitud. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_SOLICIT")
  private Solicitud solicitud;

  /** The usuario. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_PERSONA")
  private Usuario usuario;

  /** The reestado. */
  @Column(name = "REESTADO", length = 1, nullable = false)
  private String estado;

  /** The retipo. */
  @Column(name = "RETIPO", length = 3, nullable = false)
  private String tipo;

  /** The remotivo. */
  @Column(name = "REMOTIVO", length = 250)
  private String motivo;

  /** The refechreso. */
  @Column(name = "REFECHRESO")
  private Date fechaResolucion;

  /** The refechvigo. */
  @Column(name = "REFECHVIGO")
  private Date fechaEfecto;

  /** The rerecurrid. */
  @Column(name = "RERECURRID", nullable = false)
  private Boolean recurrida;

  /** The refechrecu. */
  @Column(name = "REFECHRECU")
  private Date fechaRecuperacion;

  /** The rerevocada. */
  @Column(name = "REREVOCADA", nullable = false)
  private Boolean revocada;

  /** The regrado. */
  @Column(name = "REGRADO", length = 1)
  private String grado;

  /** The renivel. */
  @Column(name = "RENIVEL", length = 1)
  private String nivel;

  /** The reactivo. */
  @Column(name = "REACTIVO", nullable = false)
  private boolean activo;

  /** The refechcrea. */
  @Column(name = "REFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The refechnoti. */
  @Column(name = "REFECHNOTI")
  private Date fechaNotificacion;

  /** The resolucion recorrida. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_RESOLUCI2")
  private Resolucion resolucionRecorrida;

  /** The resolucion correccion errores GYN. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_RESOLUCI3")
  private Resolucion resolucionCorreccionErroresGYN;

  /** The reacuerdoi. */
  @Column(name = "REACUERDOI")
  private Long acuerdoi;

  /** The remodificagrado. */
  @Column(name = "REMODIFICAGRADO", length = 25)
  private String modificagrado;

  /** The refechfirmaresol. */
  @Column(name = "REFECHFIRMARESOL")
  private Date fechaFirmaResolucion;

  /** The resolucion revocada. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_RESOLUCI4")
  private Resolucion resolucionRevocada;

  /** The recentro. */
  @Column(name = "RECENTRO", length = 250)
  private String centro;

  /** The renotasmigracion. */
  @Column(name = "RENOTASMIGRACION", length = 100)
  private String notasMigracion;

  /** The resolucion pia extingue. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_RESOLUCI5")
  private Resolucion resolucionPiaExtingue;

  /** The rensisaad migracion. */
  @Column(name = "RENSISAAD_MIGRACION", length = 20)
  private String nsisaadMigracion;

  /** The a enviar nsisaad. */
  @Column(name = "A_ENVIAR_NSISAAD", nullable = false)
  private boolean aEnviarNsisaad;

  /** The representa escrito. */
  @Column(name = "REPRESENTA_ESCRITO", length = 1)
  private String representaEscrito;

  /** The rejustificaciondoc. */
  @Column(name = "REJUSTIFICACIONDOC", length = 1)
  private String justificacionDocu;

  /** The reestado nsisaad. */
  @Column(name = "REESTADO_NSISAAD", length = 2)
  private String estadoNsisaad;

  /** The recaducada. */
  @Column(name = "RECADUCADA", nullable = false)
  private Boolean caducada;

  /** The revalido. */
  @Column(name = "REVALIDO", nullable = false)
  private Boolean valido;

  /**
   * On pre persist.
   */
  @PrePersist
  public void onPrePersist() {
    recurrida = Boolean.FALSE;
    revocada = Boolean.FALSE;
    activo = true;
    aEnviarNsisaad = true;
    caducada = Boolean.FALSE;
    valido = Boolean.TRUE;
    fechaCreacion = new Date();
  }

  /**
   * Gets the pk resoluci.
   *
   * @return the pk resoluci
   */
  public Long getPkResoluci() {
    return pkResoluci;
  }

  /**
   * Sets the pk resoluci.
   *
   * @param pkResoluci the new pk resoluci
   */
  public void setPkResoluci(final Long pkResoluci) {
    this.pkResoluci = pkResoluci;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public Solicitud getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the new solicitud
   */
  public void setSolicitud(final Solicitud solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the estado.
   *
   * @return the estado
   */
  public String getEstado() {
    return estado;
  }

  /**
   * Sets the estado.
   *
   * @param estado the new estado
   */
  public void setEstado(final String estado) {
    this.estado = estado;
  }

  /**
   * Gets the tipo.
   *
   * @return the tipo
   */
  public String getTipo() {
    return tipo;
  }

  /**
   * Sets the tipo.
   *
   * @param tipo the new tipo
   */
  public void setTipo(final String tipo) {
    this.tipo = tipo;
  }

  /**
   * Gets the motivo.
   *
   * @return the motivo
   */
  public String getMotivo() {
    return motivo;
  }

  /**
   * Sets the motivo.
   *
   * @param motivo the new motivo
   */
  public void setMotivo(final String motivo) {
    this.motivo = motivo;
  }

  /**
   * Gets the fecha resolucion.
   *
   * @return the fecha resolucion
   */
  public Date getFechaResolucion() {
    return UtilidadesCommons.cloneDate(fechaResolucion);
  }

  /**
   * Sets the fecha resolucion.
   *
   * @param fechaResolucion the new fecha resolucion
   */
  public void setFechaResolucion(final Date fechaResolucion) {
    this.fechaResolucion = UtilidadesCommons.cloneDate(fechaResolucion);
  }

  /**
   * Gets the fecha efecto.
   *
   * @return the fecha efecto
   */
  public Date getFechaEfecto() {
    return UtilidadesCommons.cloneDate(fechaEfecto);
  }

  /**
   * Sets the fecha efecto.
   *
   * @param fechaEfecto the new fecha efecto
   */
  public void setFechaEfecto(final Date fechaEfecto) {
    this.fechaEfecto = UtilidadesCommons.cloneDate(fechaEfecto);
  }

  /**
   * Gets the recurrida.
   *
   * @return the recurrida
   */
  public Boolean getRecurrida() {
    return recurrida;
  }

  /**
   * Sets the recurrida.
   *
   * @param recurrida the new recurrida
   */
  public void setRecurrida(final Boolean recurrida) {
    this.recurrida = recurrida;
  }

  /**
   * Gets the fecha recuperacion.
   *
   * @return the fecha recuperacion
   */
  public Date getFechaRecuperacion() {
    return UtilidadesCommons.cloneDate(fechaRecuperacion);
  }

  /**
   * Sets the fecha recuperacion.
   *
   * @param fechaRecuperacion the new fecha recuperacion
   */
  public void setFechaRecuperacion(final Date fechaRecuperacion) {
    this.fechaRecuperacion = UtilidadesCommons.cloneDate(fechaRecuperacion);
  }

  /**
   * Gets the revocada.
   *
   * @return the revocada
   */
  public Boolean getRevocada() {
    return revocada;
  }

  /**
   * Sets the revocada.
   *
   * @param revocada the new revocada
   */
  public void setRevocada(final Boolean revocada) {
    this.revocada = revocada;
  }

  /**
   * Gets the grado.
   *
   * @return the grado
   */
  public String getGrado() {
    return grado;
  }

  /**
   * Sets the grado.
   *
   * @param grado the new grado
   */
  public void setGrado(final String grado) {
    this.grado = grado;
  }

  /**
   * Gets the nivel.
   *
   * @return the nivel
   */
  public String getNivel() {
    return nivel;
  }

  /**
   * Sets the nivel.
   *
   * @param nivel the new nivel
   */
  public void setNivel(final String nivel) {
    this.nivel = nivel;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha notificacion.
   *
   * @return the fecha notificacion
   */
  public Date getFechaNotificacion() {
    return UtilidadesCommons.cloneDate(fechaNotificacion);
  }

  /**
   * Sets the fecha notificacion.
   *
   * @param fechaNotificacion the new fecha notificacion
   */
  public void setFechaNotificacion(final Date fechaNotificacion) {
    this.fechaNotificacion = UtilidadesCommons.cloneDate(fechaNotificacion);
  }

  /**
   * Gets the resolucion recorrida.
   *
   * @return the resolucion recorrida
   */
  public Resolucion getResolucionRecorrida() {
    return resolucionRecorrida;
  }

  /**
   * Sets the resolucion recorrida.
   *
   * @param resolucionRecorrida the new resolucion recorrida
   */
  public void setResolucionRecorrida(Resolucion resolucionRecorrida) {
    this.resolucionRecorrida = resolucionRecorrida;
  }

  /**
   * Gets the resolucion correccion errores GYN.
   *
   * @return the resolucion correccion errores GYN
   */
  public Resolucion getResolucionCorreccionErroresGYN() {
    return resolucionCorreccionErroresGYN;
  }

  /**
   * Sets the resolucion correccion errores GYN.
   *
   * @param resolucionCorreccionErroresGYN the new resolucion correccion errores
   *        GYN
   */
  public void setResolucionCorreccionErroresGYN(
      final Resolucion resolucionCorreccionErroresGYN) {
    this.resolucionCorreccionErroresGYN = resolucionCorreccionErroresGYN;
  }

  /**
   * Gets the acuerdoi.
   *
   * @return the acuerdoi
   */
  public Long getAcuerdoi() {
    return acuerdoi;
  }

  /**
   * Sets the acuerdoi.
   *
   * @param acuerdoi the new acuerdoi
   */
  public void setAcuerdoi(final Long acuerdoi) {
    this.acuerdoi = acuerdoi;
  }

  /**
   * Gets the modificagrado.
   *
   * @return the modificagrado
   */
  public String getModificagrado() {
    return modificagrado;
  }

  /**
   * Sets the modificagrado.
   *
   * @param modificagrado the new modificagrado
   */
  public void setModificagrado(final String modificagrado) {
    this.modificagrado = modificagrado;
  }

  /**
   * Gets the fecha firma resolucion.
   *
   * @return the fecha firma resolucion
   */
  public Date getFechaFirmaResolucion() {
    return UtilidadesCommons.cloneDate(fechaFirmaResolucion);
  }

  /**
   * Sets the fecha firma resolucion.
   *
   * @param fechaFirmaResolucion the new fecha firma resolucion
   */
  public void setFechaFirmaResolucion(final Date fechaFirmaResolucion) {
    this.fechaFirmaResolucion =
        UtilidadesCommons.cloneDate(fechaFirmaResolucion);
  }

  /**
   * Gets the resolucion revocada.
   *
   * @return the resolucion revocada
   */
  public Resolucion getResolucionRevocada() {
    return resolucionRevocada;
  }

  /**
   * Sets the resolucion revocada.
   *
   * @param resolucionRevocada the new resolucion revocada
   */
  public void setResolucionRevocada(final Resolucion resolucionRevocada) {
    this.resolucionRevocada = resolucionRevocada;
  }

  /**
   * Gets the centro.
   *
   * @return the centro
   */
  public String getCentro() {
    return centro;
  }

  /**
   * Sets the centro.
   *
   * @param centro the new centro
   */
  public void setCentro(final String centro) {
    this.centro = centro;
  }

  /**
   * Gets the notas migracion.
   *
   * @return the notas migracion
   */
  public String getNotasMigracion() {
    return notasMigracion;
  }

  /**
   * Sets the notas migracion.
   *
   * @param notasMigracion the new notas migracion
   */
  public void setNotasMigracion(final String notasMigracion) {
    this.notasMigracion = notasMigracion;
  }

  /**
   * Gets the resolucion pia extingue.
   *
   * @return the resolucion pia extingue
   */
  public Resolucion getResolucionPiaExtingue() {
    return resolucionPiaExtingue;
  }

  /**
   * Sets the resolucion pia extingue.
   *
   * @param resolucionPiaExtingue the new resolucion pia extingue
   */
  public void setResolucionPiaExtingue(final Resolucion resolucionPiaExtingue) {
    this.resolucionPiaExtingue = resolucionPiaExtingue;
  }

  /**
   * Gets the nsisaad migracion.
   *
   * @return the nsisaad migracion
   */
  public String getNsisaadMigracion() {
    return nsisaadMigracion;
  }

  /**
   * Sets the nsisaad migracion.
   *
   * @param nsisaadMigracion the new nsisaad migracion
   */
  public void setNsisaadMigracion(final String nsisaadMigracion) {
    this.nsisaadMigracion = nsisaadMigracion;
  }

  /**
   * Gets the a enviar nsisaad.
   *
   * @return the a enviar nsisaad
   */
  public boolean getaEnviarNsisaad() {
    return aEnviarNsisaad;
  }

  /**
   * Sets the a enviar nsisaad.
   *
   * @param aEnviarNsisaad the new a enviar nsisaad
   */
  public void setaEnviarNsisaad(final boolean aEnviarNsisaad) {
    this.aEnviarNsisaad = aEnviarNsisaad;
  }

  /**
   * Gets the representa escrito.
   *
   * @return the representa escrito
   */
  public String getRepresentaEscrito() {
    return representaEscrito;
  }

  /**
   * Sets the representa escrito.
   *
   * @param representaEscrito the new representa escrito
   */
  public void setRepresentaEscrito(final String representaEscrito) {
    this.representaEscrito = representaEscrito;
  }

  /**
   * Gets the justificacion docu.
   *
   * @return the justificacion docu
   */
  public String getJustificacionDocu() {
    return justificacionDocu;
  }

  /**
   * Sets the justificacion docu.
   *
   * @param justificacionDocu the new justificacion docu
   */
  public void setJustificacionDocu(final String justificacionDocu) {
    this.justificacionDocu = justificacionDocu;
  }

  /**
   * Gets the estado nsisaad.
   *
   * @return the estado nsisaad
   */
  public String getEstadoNsisaad() {
    return estadoNsisaad;
  }

  /**
   * Sets the estado nsisaad.
   *
   * @param estadoNsisaad the new estado nsisaad
   */
  public void setEstadoNsisaad(final String estadoNsisaad) {
    this.estadoNsisaad = estadoNsisaad;
  }

  /**
   * Gets the caducada.
   *
   * @return the caducada
   */
  public Boolean getCaducada() {
    return caducada;
  }

  /**
   * Sets the caducada.
   *
   * @param caducada the new caducada
   */
  public void setCaducada(final Boolean caducada) {
    this.caducada = caducada;
  }

  /**
   * Gets the valido.
   *
   * @return the valido
   */
  public Boolean getValido() {
    return valido;
  }

  /**
   * Sets the valido.
   *
   * @param valido the new valido
   */
  public void setValido(final Boolean valido) {
    this.valido = valido;
  }

}
