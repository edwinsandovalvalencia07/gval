package com.gval.gval.entity.model;

import com.gval.gval.common.ConstantesCommons;
import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.entity.vistas.model.Centro;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import jakarta.persistence.*;



/**
 * The persistent class for the SDM_INFOSOCI database table.
 *
 */
@Entity
@Table(name = "SDM_INFOSOCI")
@GenericGenerator(name = "SDM_INFOSOCI_PKINFOSOCI_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_INFOSOCI"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public final class InformeSocial implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -1322720071856572473L;

  /** The pk informe social. */
  @Id
  @Column(name = "PK_INFOSOCI", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_INFOSOCI_PKINFOSOCI_GENERATOR")
  private Long pkInformeSocial;

  /** The sexo. */
  @Column(name = "PK_SEXO")
  private Long sexo;

  /** The direccion ada. */
  @ManyToOne
  @JoinColumn(name = "PK_DIRECCIO")
  private DireccionAda direccionAda;

  /** The nacionalidad. */
  @Column(name = "PK_NACIONA")
  private Long nacionalidad;

  /** The tipo identificador. */
  @ManyToOne
  @JoinColumn(name = "PK_TIPOIDEN")
  private TipoIdentificador tipoIdentificador;

  /** The solicitud. */
  @ManyToOne
  @JoinColumn(name = "PK_SOLICIT")
  private Solicitud solicitud;

  /** The usuario empieza. */
  // bi-directional many-to-one association to SdxUsuario
  @ManyToOne
  @JoinColumn(name = "PK_USUARIO", nullable = false)
  private Usuario usuarioCreador;


  /** The usuario asignado. */
  // bi-directional many-to-one association to SdxUsuario
  @ManyToOne
  @JoinColumn(name = "PK_USUARIO2")
  private Usuario usuarioAsignado;

  /** The documento generado. */
  // bi-directional many-to-one association to SdmDocugene
  @ManyToOne
  @JoinColumn(name = "PK_DOCU")
  private DocumentoGenerado documentoGenerado;

  /** The estado peticion. */
  @Column(name = "INESTADO", nullable = false, length = 1)
  private String estadoPeticion;

  /** The fecha peticion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "INFPETICION", nullable = false)
  private Date fechaPeticion;

  /**
   * The fecha peticion. Momento en el que pasa de pendiente a encurso.
   * establecido mediante trigger
   */
  @Temporal(TemporalType.DATE)
  @Column(name = "INFENCURSO")
  private Date fechaGrabacion;

  /** The motivo peticion. */
  @Column(name = "INMOTIVO", length = 4000)
  private String motivoPeticion;

  /** The fecha inicio. */
  @Temporal(TemporalType.DATE)
  @Column(name = "INFINICIO")
  private Date fechaInicio;

  /** The fecha fin. */
  @Temporal(TemporalType.DATE)
  @Column(name = "INFFIN")
  private Date fechaFin;

  /** The ultimo informe realizado. */
  @Column(name = "INULTIMO", nullable = false)
  private Boolean esUltimoInforme;

  /** The nombre solicitante. */
  @Column(name = "INNOMBRE", length = 50)
  private String nombreSolicitante;

  /** The primer apellido solicitante. */
  @Column(name = "INAPELLIDO1", length = 50)
  private String primerApellidoSolicitante;

  /** The segundo apellido solicitante. */
  @Column(name = "INAPELLIDO2", length = 50)
  private String segundoApellidoSolicitante;

  /** The numero identificacion. */
  @Column(name = "INDNINIE", length = 50)
  private String numeroIdentificacion;

  /** The fecha nacimiento solicitante. */
  @Temporal(TemporalType.DATE)
  @Column(name = "INFECHNACI")
  private Date fechaNacimientoSolicitante;

  /** The tiempo residencia. */
  @Column(name = "INTIMPRESD")
  private Long tiempoResidencia;

  /** The situacion capacidad. */
  @Column(name = "INSITUCAPA", length = 1)
  private String situacionCapacidad;

  /** The tiene edad escolar. */
  @Column(name = "INEDADESCO")
  private Boolean tieneEdadEscolar;

  /** The centro estudios. */
  @Column(name = "INCENTESTU", length = 250)
  private String centroEstudios;

  /** The tipo plaza residencia. */
  @Column(name = "INTIPOPLAZ", length = 250)
  private String tipoPlazaResidencia;

  /** The centro aportacion usuario. */
  @Column(name = "INCEAPOUSU", precision = 8, scale = 2)
  private BigDecimal centroAportacionUsuario;

  /** The tipo centro especializado. */
  @Column(name = "INTIPPLCD", length = 250)
  private String tipoCentroEspecializado;

  /** The aportacion usuario en centro especializado. */
  @Column(name = "INAPUSUCD", precision = 8, scale = 2)
  private BigDecimal aportacionUsuarioCentroEspecializado;

  /** The apoyo domicilio horas semana. */
  @Column(name = "INCDHORSEM", nullable = false, precision = 4, scale = 2)
  private Long apoyoDomicilioHorasSemana;

  /** The opinion persona situacion centro. */
  @Column(name = "INOPSITUCT", length = 1)
  private String opinionPersonaSituacionCentro;

  /** The opinion familia cren. */
  @Column(name = "INFMATPERS", length = 1)
  private String opinionFamiliaCren;

  /** The valoracion tecnica. */
  @Column(name = "INVALOTECN", length = 4000)
  private String valoracionTecnica;

  /** The tipo convivencia. */
  @Column(name = "INTIPOCONV", length = 1)
  private String tipoConvivencia;

  /** The apoyo familiar diario. */
  @Column(name = "INAPFMDIAR")
  private Boolean apoyoFamiliarDiario;

  /** The apoyo familiar esporadico. */
  @Column(name = "INAPFMESPO")
  private Boolean apoyoFamiliarEsporadico;

  /** The apoyo social-comunitario diario. */
  @Column(name = "INAPSODIAR")
  private Boolean apoyoSocialDiario;

  /** The apoyo social-comunitario esporadico. */
  @Column(name = "INAPSOESPO")
  private Boolean apoyoSocialEsporadico;

  /** The sin apoyo familiar. */
  @Column(name = "INSINAPOYO")
  private Boolean sinApoyo;

  /** The sin apoyo social-comunitario. */
  @Column(name = "INSINAPSO")
  private Boolean sinApoyoSocial;

  /** The nivel relacion. */
  @Column(name = "INNIVREL", length = 1)
  private String nivelRelacion;

  /** The mayor edad. */
  @Column(name = "INCUMAYEDA")
  private Boolean mayorEdad;

  /** The cuidador predependiente O dependiencia acusada. */
  @Column(name = "INCUPRDEAC")
  private Boolean cuidadorPredependiente;

  /** The concurrencia enfermedad. */
  @Column(name = "INCONENF")
  private Boolean concurrenciaEnfermedad;

  /** The seguridad economica. */
  @Column(name = "INSEGECO")
  private Boolean seguridadEconomica;

  /** The conocimientos suficientes. */
  @Column(name = "INCONSUF")
  private Boolean conocimientosSuficientes;

  /** The conocimientos escasos. */
  @Column(name = "INCONESC")
  private Boolean conocimientosEscasos;

  /** The dificultad comprension enfermedad. */
  @Column(name = "INDIFCOM")
  private Boolean dificultadComprensionEnfermedad;

  /** The disponibilidad apoyos profesionales. */
  @Column(name = "INDISPPROF")
  private Boolean disponibilidadApoyosProfesionales;

  /** The disponibilidad tiempo. */
  @Column(name = "INDISPTIEM")
  private Boolean disponibilidadTiempo;

  /** The periodos descanso. */
  @Column(name = "INPERIDESC")
  private Boolean periodosDescanso;

  /** The signos agotamiento. */
  @Column(name = "INSIGNAGOT")
  private Boolean signosAgotamiento;

  /** The sin signos fragilidad. */
  @Column(name = "INNOSIGNFRAG")
  private Boolean sinSignosFragilidad;

  /** The ausencia compromiso estable. */
  @Column(name = "INAUSCOMP")
  private Boolean ausenciaCompromiso;

  /** The dificultados conexion. */
  @Column(name = "INDIFCON")
  private Boolean dificultadosConexion;

  /** The riesgo claudicacion. */
  @Column(name = "INRIESCLAU", length = 1)
  private String riesgoClaudicacion;

  /** The propiedad vivienda habitual. */
  @Column(name = "INVIVIHABI", length = 1)
  private String propiedadViviendaHabitual;

  /** The condiciones vivienda. */
  @Column(name = "INCONDVIVI", length = 1)
  private String condicionesVivienda;

  /** The adaptacion hogar. */
  @Column(name = "INADTAHABI", length = 1000)
  private String adaptacionHogar;

  /** The facilita acceso. */
  @Column(name = "INACFACI")
  private Long facilitaAcceso;

  /** The valoracion social. */
  @Column(name = "INVALOSOCI", length = 4000)
  private String valoracionSocial;

  /** The coinciden valores preferencias. */
  @Column(name = "INCOINVALO")
  private Boolean coincidenValoresPreferencias;

  /** The motivos priorizacion profesional. */
  @Column(name = "INMOTIVOS", length = 1000)
  private String motivosPriorizacionProfesional;

  /** The identificador trabajador social. */
  @Column(name = "INIDENTS", length = 250)
  private String identificadorTrabajadorSocial;

  /** The numero colegiado. */
  @Column(name = "INNCOLEG", length = 250)
  private String numeroColegiado;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "INFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The nivel cultural. */
  @Column(name = "INNIVECULT")
  private Long nivelCultural;

  /** The activo. */
  @Column(name = "ISACTIVO", nullable = false)
  private Boolean activo;

  /** The tipo centro. */
  @Column(name = "INTIPOCENTRO")
  private Long tipoCentro;

  /** The tiene edad laboral. */
  @Column(name = "INEDADLABO")
  private Boolean tieneEdadLaboral;

  /** The activo laboralmente. */
  @Column(name = "INLABORACT")
  private Boolean activoLaboralmente;

  /** The situacion inactividad. */
  @Column(name = "INSITINACT")
  private Long situacionInactividad;

  /** The tipo jornada. */
  @Column(name = "INTIPOJORN")
  private Long tipoJornada;

  /** The profesion solicitante. */
  @Column(name = "INPROFESIO", length = 250)
  private String profesionSolicitante;

  /** The es centro residencial. */
  @Column(name = "INCENTRESI")
  private Boolean tieneCentroResidencial;

  /** The codigo centro residencial. */
  @ManyToOne
  @JoinColumn(name = "INCOCENRE")
  @NotFound(action = NotFoundAction.IGNORE)
  private Centro centroAtencionResidencial;

  /** The centro atencion diurna. */
  @Column(name = "INCENTDIUR")
  private Boolean tieneCentroAtencionDiurna;

  /** The codigo centro atencion diurna. */
  @ManyToOne
  @JoinColumn(name = "INCOCENDI")
  @NotFound(action = NotFoundAction.IGNORE)
  private Centro centroAtencionDiurna;

  /** The soporte social Y familiar. */
  @Column(name = "INSOPSYF")
  private Long soporteSocialYFamiliar;

  /** The ayuda movilidad baston muleta. */
  @Column(name = "INAYUMOV1")
  private Boolean ayudaMovilidadBastonMuleta;

  /** The ayuda movilidad silla ruedas. */
  @Column(name = "INAYUMOV2")
  private Boolean ayudaMovilidadSillaRuedas;

  /** The ayuda movilidad grua transferencia. */
  @Column(name = "INAYUMOV3")
  private Boolean ayudaMovilidadGruaTransferencia;

  /** The ayuda movilidad otros. */
  @Column(name = "INAYUMOV4")
  private Boolean ayudaMovilidadOtros;

  /** The otras ayudas movilidad. */
  @Column(name = "INOTROMOVI", length = 1000)
  private String otrasAyudasMovilidad;

  /** The ayuda aseo personal. */
  @Column(name = "INAYUASEO1")
  private Boolean ayudaAseoPersonal;

  /** The ayuda aseo banio Y ducha. */
  @Column(name = "INAYUASEO2")
  private Boolean ayudaAseoBanioYDucha;

  /** The ayuda aseo otros. */
  @Column(name = "INAYUASEO3")
  private Boolean ayudaAseoOtros;

  /** The otras ayudas aseo. */
  @Column(name = "INOTROASEO", length = 1000)
  private String otrasAyudasAseo;

  /** The ayuda alimentacion para preparar comida. */
  @Column(name = "INAYUALIM1")
  private Boolean ayudaAlimentacionParaPrepararComida;

  /** The ayuda alimentacion para comer Y beber. */
  @Column(name = "INAYUALIM2")
  private Boolean ayudaAlimentacionParaComerYBeber;

  /** The ayuda alimentacion otros. */
  @Column(name = "INAYUALIM3")
  private Boolean ayudaAlimentacionOtros;

  /** The otras ayudas alimentacion. */
  @Column(name = "INOTROALIM", length = 1000)
  private String otrasAyudasAlimentacion;

  /** The ayudas tecnicas tic. */
  @Column(name = "INAYUTIC", length = 1000)
  private String ayudasTecnicasTic;

  /** The ancho puertas. */
  @Column(name = "INANCPUE")
  private Boolean anchoPuertas;

  /** The escaleras interiores. */
  @Column(name = "INESCINT")
  private Boolean escalerasInteriores;

  /** The adecuacion sanitarios. */
  @Column(name = "INADESAN")
  private Boolean adecuacionSanitarios;

  /** The pasamanos. */
  @Column(name = "INPASMAN")
  private Boolean pasamanos;

  /** The adecuacion cocina. */
  @Column(name = "INADECOC")
  private Boolean adecuacionCocina;

  /** The instalacion gas luz. */
  @Column(name = "INGASLUZ")
  private Boolean instalacionGasLuz;

  /** The suelo antideslizante. */
  @Column(name = "INSUELANT")
  private Boolean sueloAntideslizante;

  /** The otros dentro vivienda. */
  @Column(name = "INOTDENTVI")
  private Boolean otrosDentroVivienda;

  /** The rampas. */
  @Column(name = "INRAMPAS")
  private Boolean rampas;

  /** The ascensor. */
  @Column(name = "INASCENS")
  private Boolean ascensor;

  /** The silla salvaescalera. */
  @Column(name = "INSILLSALV")
  private Boolean sillaSalvaescalera;

  /** The otrosAccesoHogar. */
  @Column(name = "INOTACCHG")
  private Boolean otrosAccesoHogar;

  /** The otros entorno. */
  @Column(name = "INOTROENT", length = 1000)
  private String otrosEntorno;

  /** The dificultades servicio. */
  @Column(name = "INDIFSERV", length = 4000)
  private String dificultadesServicio;

  /** The info cuidador. */
  @Column(name = "INCUIDONE", length = 4000)
  private String infoCuidador;

  /** The info asistente. */
  @Column(name = "INAPIDONE", length = 4000)
  private String infoAsistente;

  /** The pedido. */
  @Column(name = "INPEDIDO")
  private Long pedido;

  /** The tipo informe social. */
  @Column(name = "INTIPOIS")
  private Long tipoInformeSocial;

  /** The situacion capacidad laboral. */
  @Column(name = "INSITUACIO")
  private Long situacionCapacidadLaboral;

  /** The estado civil. */
  @Column(name = "PK_ESTACIVI")
  private Long estadoCivil;

  /** The modalidad. */
  @Column(name = "INAPMODALI", length = 1)
  private String modalidad;

  /** The intensidad asistencia. */
  @Column(name = "INAPINTENS", length = 1)
  private String intensidadAsistencia;

  /** The fecha cuidador. */
  @Temporal(TemporalType.DATE)
  @Column(name = "INCUFECHA")
  private Date fechaCuidador;

  /** The motivoPersonal. */
  @Column(name = "INMOTIVOPERSONAL")
  private Boolean motivoPersonal;

  /** The preferencias revisadas. */
  @Column(name = "INPREFERENCIASREVISADAS", nullable = false)
  private Boolean preferenciasRevisadas;

  /** The cambio revisado. */
  @Column(name = "INCAMBIOREVISADO", nullable = false)
  private Boolean cambioRevisado;

  /** The coincide cuidador. */
  @Column(name = "INCOINCUID")
  private Boolean coincideCuidador;

  /** The documento nuevas preferencias. */
  @Column(name = "PK_DOCU_PREF")
  private Long pkDocumentoPreferencias;

  /** The pk pia. */
  @Column(name = "PK_PIA")
  private Long pkPia;

  /** The app origen. */
  @Column(name = "APP_ORIGEN")
  private String appOrigen;

  /** The motivos generacion informe social. */
  @OneToMany(mappedBy = "informeSocial")
  private List<MotivoGeneracionInformeSocial> motivosGeneracionInformeSocial;

  /** The preferencia informe social. */
  @OneToMany(mappedBy = "informeSocial")
  private List<PreferenciaInformeSocial> preferenciasInformeSocial;

  /** The cuidadores informe social. */
  @OneToMany(mappedBy = "informeSocial", cascade = CascadeType.ALL)
  private List<CuidadorInformeSocial> cuidadoresInformeSocial;

  /** The asistentes informe social. */
  @OneToMany(mappedBy = "informeSocial", cascade = CascadeType.ALL)
  private List<AsistenteInformeSocial> asistentesInformeSocial;

  /** The documentos informe social. */
  @OneToMany(mappedBy = "informeSocial")
  private List<DocumentoInformeSocial> documentosInformeSocial;


  /**
   * The nueva version informes sociales. el informe social tiene el nuevo
   * formato valor 3.
   */
  @Column(name = "INVERSION", nullable = false)
  private Long version;

  /** The anamnesis. */
  @Column(name = "INANAMNESIS", length = 1000)
  private String anamnesis;

  /** The riesgo claudica. */
  @Column(name = "INRIESCLAUDIC")
  private Boolean riesgoClaudica;

  /** The abandono. */
  @Column(name = "INABANDONO")
  private Boolean abandono;

  /** The tiene vivienda habitual. */
  @Column(name = "INVIVIENDAHAB")
  private Boolean tieneVivienda;

  /** The conservacion. */
  @Column(name = "INCONDCONS")
  private Boolean conservacion;

  /** The habitabilidad. */
  @Column(name = "INCONDHAB")
  private Boolean habitabilidad;

  /** The salubridad. */
  @Column(name = "INCONDSALU")
  private Boolean salubridad;

  /** The adaptacion hogar actual. */
  @Column(name = "INADTAHABIACTUAL", length = 4000)
  private String adaptacionHogarActual;

  /** The situacion socio familiar. */
  @Column(name = "INSITUASOCFAM", length = 4000)
  private String situacionSocioFamiliar;

  /** The mantener primera pref. */
  @Column(name = "INMANTPRIMERAPREF")
  private Boolean mantenerPreferencia1;

  /** The mantener segunda pref. */
  @Column(name = "INMANTSEGUNDAPREF")
  private Boolean mantenerPreferencia2;

  /** The centro trabajador social. */
  @Column(name = "INCENTROTRABSOCIAL", length = 250, nullable = false)
  private String centroTrabajadorSocial;

  // IS nuevas preferencias AP:
  /** The ayuda comunicacion. */
  @Column(name = "INAYUDACOMUNICA")
  private Boolean ayudaComunicacion;

  /** The ayuda desplazamiento. */
  @Column(name = "INAYUDADESPLAZA")
  private Boolean ayudaDesplazamiento;

  /** The ayuda acceso recursos. */
  @Column(name = "INAYUDARECURSOS")
  private Boolean ayudaAccesoRecursos;

  /** The realiza formacion. */
  @Column(name = "INREALIZAFORMAC")
  private Boolean realizaFormacion;

  /** The nombre formacion. */
  @Column(name = "INNOMFORMACION", length = 250)
  private String nombreFormacion;

  /** The formacion ayuda AP. */
  @Column(name = "INFORMACAYUDAAP")
  private Boolean formacionAyudaAP;

  /** The trabajo ayuda AP. */
  @Column(name = "INTRABAJOAYUDAAP")
  private Boolean trabajoAyudaAP;

  /** The realiza actividad. */
  @Column(name = "INREALIZAACTIVIDAD")
  private Boolean realizaActividad;

  /** The tipo actividad. */
  @Column(name = "INTIPOACTIVIDAD", length = 250)
  private String tipoActividad;

  /** The nombre entidad actividades. */
  @Column(name = "INENTIACTIVIDAD", length = 250)
  private String nombreEntidadActividad;

  /** The actividad ayuda AP. */
  @Column(name = "INACTIVAYUDAAP")
  private Boolean actividadAyudaAP;

  /** The proyecto adecuado. */
  @Column(name = "INPROYECTOADEC")
  private Boolean proyectoAdecuado;

  /** The otro recurso. */
  @Column(name = "INOTRORECURSO")
  private Boolean otroRecurso;

  /** The recurso alternativo prestacion. */
  @ManyToOne
  @JoinColumn(name = "INRECURSOALT")
  private CatalogoServicio otroRecursoSeleccionado;

  /** The otro recurso motivos. */
  @Column(name = "INOTRORECURSOMOTIV", length = 1000)
  private String otroRecursoMotivos;

  /** The tipo persona AP fisica. */
  @Column(name = "INTIPOAPFISICA")
  private Boolean tipoPersonaAPFisica;

  /** The tipo persona AP juridica. */
  @Column(name = "INTIPOAPJURIDICA")
  private Boolean tipoPersonaAPJuridica;

  /** The horario lectivo AP. */
  @Column(name = "INHORARIOLECTIVOAP")
  private Boolean horarioLectivoAP;

  /** The mantener teleasistencia. */
  @Column(name = "INMANTENERTELEASIST")
  private Boolean mantenerTeleasistencia;

  /**
   * The tipo asispers. Tipo de persona de asistencia personal: PATI - menores
   * edad (0) / PAP - mayores edad (1)
   */
  @Column(name = "INTIPOASISPERS")
  private Boolean tipoAsispers;

  /** The aumento horas. */
  @Column(name = "INAUMENTOHORAS")
  private Boolean aumentoHoras;

  /**
   * Instantiates a new informe social.
   */
  public InformeSocial() {
    super();
  }


  /**
   * On pre persist.
   */
  @PrePersist
  public void onPrePersist() {
    fechaCreacion = new Date();
    appOrigen = ConstantesCommons.APLICACION_ADA3;
  }


  /**
   * Gets the pk informe social.
   *
   * @return the pkInformeSocial
   */
  public Long getPkInformeSocial() {
    return pkInformeSocial;
  }



  /**
   * Sets the pk informe social.
   *
   * @param pkInformeSocial the pkInformeSocial to set
   */
  public void setPkInformeSocial(final Long pkInformeSocial) {
    this.pkInformeSocial = pkInformeSocial;
  }



  /**
   * Gets the sexo.
   *
   * @return the sexo
   */
  public Long getSexo() {
    return sexo;
  }



  /**
   * Sets the sexo.
   *
   * @param sexo the sexo to set
   */
  public void setSexo(final Long sexo) {
    this.sexo = sexo;
  }



  /**
   * Gets the direccion ada.
   *
   * @return the direccionAda
   */
  public DireccionAda getDireccionAda() {
    return direccionAda;
  }



  /**
   * Sets the direccion ada.
   *
   * @param direccionAda the direccionAda to set
   */
  public void setDireccionAda(final DireccionAda direccionAda) {
    this.direccionAda = direccionAda;
  }



  /**
   * Gets the nacionalidad.
   *
   * @return the nacionalidad
   */
  public Long getNacionalidad() {
    return nacionalidad;
  }



  /**
   * Sets the nacionalidad.
   *
   * @param nacionalidad the nacionalidad to set
   */
  public void setNacionalidad(final Long nacionalidad) {
    this.nacionalidad = nacionalidad;
  }



  /**
   * Gets the tipo identificador.
   *
   * @return the tipoIdentificador
   */
  public TipoIdentificador getTipoIdentificador() {
    return tipoIdentificador;
  }



  /**
   * Sets the tipo identificador.
   *
   * @param tipoIdentificador the tipoIdentificador to set
   */
  public void setTipoIdentificador(final TipoIdentificador tipoIdentificador) {
    this.tipoIdentificador = tipoIdentificador;
  }



  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public Solicitud getSolicitud() {
    return solicitud;
  }



  /**
   * Sets the solicitud.
   *
   * @param solicitud the solicitud to set
   */
  public void setSolicitud(final Solicitud solicitud) {
    this.solicitud = solicitud;
  }



  /**
   * Gets the usuario empieza.
   *
   * @return the usuarioCreador
   */
  public Usuario getUsuarioCreador() {
    return usuarioCreador;
  }



  /**
   * Sets the usuario empieza.
   *
   * @param usuarioCreador the usuarioCreador to set
   */
  public void setUsuarioCreador(final Usuario usuarioCreador) {
    this.usuarioCreador = usuarioCreador;
  }

  /**
   * Gets the usuario asignado.
   *
   * @return the usuario asignado
   */
  public Usuario getUsuarioAsignado() {
    return usuarioAsignado;
  }

  /**
   * Sets the usuario asignado.
   *
   * @param usuarioAsignado the new usuario asignado
   */
  public void setUsuarioAsignado(final Usuario usuarioAsignado) {
    this.usuarioAsignado = usuarioAsignado;
  }

  /**
   * Gets the documento generado.
   *
   * @return the documentoGenerado
   */
  public DocumentoGenerado getDocumentoGenerado() {
    return documentoGenerado;
  }



  /**
   * Sets the documento generado.
   *
   * @param documentoGenerado the documentoGenerado to set
   */
  public void setDocumentoGenerado(final DocumentoGenerado documentoGenerado) {
    this.documentoGenerado = documentoGenerado;
  }



  /**
   * Gets the estado peticion.
   *
   * @return the estadoPeticion
   */
  public String getEstadoPeticion() {
    return estadoPeticion;
  }



  /**
   * Sets the estado peticion.
   *
   * @param estadoPeticion the estadoPeticion to set
   */
  public void setEstadoPeticion(final String estadoPeticion) {
    this.estadoPeticion = estadoPeticion;
  }



  /**
   * Gets the fecha peticion.
   *
   * @return the fechaPeticion
   */
  public Date getFechaPeticion() {
    return UtilidadesCommons.cloneDate(fechaPeticion);
  }



  /**
   * Sets the fecha peticion.
   *
   * @param fechaPeticion the fechaPeticion to set
   */
  public void setFechaPeticion(final Date fechaPeticion) {
    this.fechaPeticion = UtilidadesCommons.cloneDate(fechaPeticion);
  }



  /**
   * Gets the motivo peticion.
   *
   * @return the motivoPeticion
   */
  public String getMotivoPeticion() {
    return motivoPeticion;
  }



  /**
   * Sets the motivo peticion.
   *
   * @param motivoPeticion the motivoPeticion to set
   */
  public void setMotivoPeticion(final String motivoPeticion) {
    this.motivoPeticion = motivoPeticion;
  }



  /**
   * Gets the fecha inicio.
   *
   * @return the fechaInicio
   */
  public Date getFechaInicio() {
    return UtilidadesCommons.cloneDate(fechaInicio);
  }



  /**
   * Sets the fecha inicio.
   *
   * @param fechaInicio the fechaInicio to set
   */
  public void setFechaInicio(final Date fechaInicio) {
    this.fechaInicio = UtilidadesCommons.cloneDate(fechaInicio);
  }



  /**
   * Gets the fecha fin.
   *
   * @return the fechaFin
   */
  public Date getFechaFin() {
    return UtilidadesCommons.cloneDate(fechaFin);
  }



  /**
   * Sets the fecha fin.
   *
   * @param fechaFin the fechaFin to set
   */
  public void setFechaFin(final Date fechaFin) {
    this.fechaFin = UtilidadesCommons.cloneDate(fechaFin);
  }



  /**
   * Gets the ultimo informe realizado.
   *
   * @return the esUltimoInforme
   */
  public Boolean getEsUltimoInforme() {
    return esUltimoInforme;
  }

  /**
   * Sets the ultimo informe realizado.
   *
   * @param ultimoInformeRealizado the new ultimo informe realizado
   */
  public void setEsUltimoInforme(final Boolean ultimoInformeRealizado) {
    this.esUltimoInforme = ultimoInformeRealizado;
  }



  /**
   * Gets the nombre solicitante.
   *
   * @return the nombreSolicitante
   */
  public String getNombreSolicitante() {
    return nombreSolicitante;
  }



  /**
   * Sets the nombre solicitante.
   *
   * @param nombreSolicitante the nombreSolicitante to set
   */
  public void setNombreSolicitante(final String nombreSolicitante) {
    this.nombreSolicitante = nombreSolicitante;
  }



  /**
   * Gets the primer apellido solicitante.
   *
   * @return the primerApellidoSolicitante
   */
  public String getPrimerApellidoSolicitante() {
    return primerApellidoSolicitante;
  }



  /**
   * Sets the primer apellido solicitante.
   *
   * @param primerApellidoSolicitante the primerApellidoSolicitante to set
   */
  public void setPrimerApellidoSolicitante(
      final String primerApellidoSolicitante) {
    this.primerApellidoSolicitante = primerApellidoSolicitante;
  }



  /**
   * Gets the segundo apellido solicitante.
   *
   * @return the segundoApellidoSolicitante
   */
  public String getSegundoApellidoSolicitante() {
    return segundoApellidoSolicitante;
  }



  /**
   * Sets the segundo apellido solicitante.
   *
   * @param segundoApellidoSolicitante the segundoApellidoSolicitante to set
   */
  public void setSegundoApellidoSolicitante(
      final String segundoApellidoSolicitante) {
    this.segundoApellidoSolicitante = segundoApellidoSolicitante;
  }



  /**
   * Gets the numero identificacion.
   *
   * @return the numeroIdentificacion
   */
  public String getNumeroIdentificacion() {
    return numeroIdentificacion;
  }



  /**
   * Sets the numero identificacion.
   *
   * @param numeroIdentificacion the numeroIdentificacion to set
   */
  public void setNumeroIdentificacion(final String numeroIdentificacion) {
    this.numeroIdentificacion = numeroIdentificacion;
  }



  /**
   * Gets the fecha nacimiento solicitante.
   *
   * @return the fechaNacimientoSolicitante
   */
  public Date getFechaNacimientoSolicitante() {
    return UtilidadesCommons.cloneDate(fechaNacimientoSolicitante);
  }



  /**
   * Sets the fecha nacimiento solicitante.
   *
   * @param fechaNacimientoSolicitante the fechaNacimientoSolicitante to set
   */
  public void setFechaNacimientoSolicitante(
      final Date fechaNacimientoSolicitante) {
    this.fechaNacimientoSolicitante =
        UtilidadesCommons.cloneDate(fechaNacimientoSolicitante);
  }



  /**
   * Gets the tiempo residencia.
   *
   * @return the tiempoResidencia
   */
  public Long getTiempoResidencia() {
    return tiempoResidencia;
  }



  /**
   * Sets the tiempo residencia.
   *
   * @param tiempoResidencia the tiempoResidencia to set
   */
  public void setTiempoResidencia(final Long tiempoResidencia) {
    this.tiempoResidencia = tiempoResidencia;
  }



  /**
   * Gets the situacion capacidad.
   *
   * @return the situacionCapacidad
   */
  public String getSituacionCapacidad() {
    return situacionCapacidad;
  }



  /**
   * Sets the situacion capacidad.
   *
   * @param situacionCapacidad the situacionCapacidad to set
   */
  public void setSituacionCapacidad(final String situacionCapacidad) {
    this.situacionCapacidad = situacionCapacidad;
  }



  /**
   * Gets the tiene edad escolar.
   *
   * @return the tieneEdadEscolar
   */
  public Boolean getTieneEdadEscolar() {
    return tieneEdadEscolar;
  }



  /**
   * Sets the tiene edad escolar.
   *
   * @param tieneEdadEscolar the tieneEdadEscolar to set
   */
  public void setTieneEdadEscolar(final Boolean tieneEdadEscolar) {
    this.tieneEdadEscolar = tieneEdadEscolar;
  }



  /**
   * Gets the centro estudios.
   *
   * @return the centroEstudios
   */
  public String getCentroEstudios() {
    return centroEstudios;
  }



  /**
   * Sets the centro estudios.
   *
   * @param centroEstudios the centroEstudios to set
   */
  public void setCentroEstudios(final String centroEstudios) {
    this.centroEstudios = centroEstudios;
  }



  /**
   * Gets the tipo plaza residencia.
   *
   * @return the tipoPlazaResidencia
   */
  public String getTipoPlazaResidencia() {
    return tipoPlazaResidencia;
  }



  /**
   * Sets the tipo plaza residencia.
   *
   * @param tipoPlazaResidencia the tipoPlazaResidencia to set
   */
  public void setTipoPlazaResidencia(final String tipoPlazaResidencia) {
    this.tipoPlazaResidencia = tipoPlazaResidencia;
  }



  /**
   * Gets the centro aportacion usuario.
   *
   * @return the centroAportacionUsuario
   */
  public BigDecimal getCentroAportacionUsuario() {
    return centroAportacionUsuario;
  }



  /**
   * Sets the centro aportacion usuario.
   *
   * @param centroAportacionUsuario the centroAportacionUsuario to set
   */
  public void setCentroAportacionUsuario(
      final BigDecimal centroAportacionUsuario) {
    this.centroAportacionUsuario = centroAportacionUsuario;
  }



  /**
   * Gets the tipo centro especializado.
   *
   * @return the tipoCentroEspecializado
   */
  public String getTipoCentroEspecializado() {
    return tipoCentroEspecializado;
  }



  /**
   * Sets the tipo centro especializado.
   *
   * @param tipoCentroEspecializado the tipoCentroEspecializado to set
   */
  public void setTipoCentroEspecializado(final String tipoCentroEspecializado) {
    this.tipoCentroEspecializado = tipoCentroEspecializado;
  }



  /**
   * Gets the aportacion usuario en centro especializado.
   *
   * @return the aportacionUsuarioCentroEspecializado
   */
  public BigDecimal getAportacionUsuarioCentroEspecializado() {
    return aportacionUsuarioCentroEspecializado;
  }

  /**
   * Sets the aportacion usuario en centro especializado.
   *
   * @param aportacionUsuarioEnCentroEspecializado the new aportacion usuario en
   *        centro especializado
   */
  public void setAportacionUsuarioCentroEspecializado(
      final BigDecimal aportacionUsuarioEnCentroEspecializado) {
    this.aportacionUsuarioCentroEspecializado =
        aportacionUsuarioEnCentroEspecializado;
  }



  /**
   * Gets the apoyo domicilio horas semana.
   *
   * @return the apoyoDomicilioHorasSemana
   */
  public Long getApoyoDomicilioHorasSemana() {
    return apoyoDomicilioHorasSemana;
  }



  /**
   * Sets the apoyo domicilio horas semana.
   *
   * @param apoyoDomicilioHorasSemana the apoyoDomicilioHorasSemana to set
   */
  public void setApoyoDomicilioHorasSemana(
      final Long apoyoDomicilioHorasSemana) {
    this.apoyoDomicilioHorasSemana = apoyoDomicilioHorasSemana;
  }



  /**
   * Gets the opinion persona situacion centro.
   *
   * @return the opinionPersonaSituacionCentro
   */
  public String getOpinionPersonaSituacionCentro() {
    return opinionPersonaSituacionCentro;
  }



  /**
   * Sets the opinion persona situacion centro.
   *
   * @param opinionPersonaSituacionCentro the opinionPersonaSituacionCentro to
   *        set
   */
  public void setOpinionPersonaSituacionCentro(
      final String opinionPersonaSituacionCentro) {
    this.opinionPersonaSituacionCentro = opinionPersonaSituacionCentro;
  }



  /**
   * Gets the opinion familia cren.
   *
   * @return the opinionFamiliaCren
   */
  public String getOpinionFamiliaCren() {
    return opinionFamiliaCren;
  }



  /**
   * Sets the opinion familia cren.
   *
   * @param opinionFamiliaCren the opinionFamiliaCren to set
   */
  public void setOpinionFamiliaCren(final String opinionFamiliaCren) {
    this.opinionFamiliaCren = opinionFamiliaCren;
  }



  /**
   * Gets the valoracion tecnica.
   *
   * @return the valoracionTecnica
   */
  public String getValoracionTecnica() {
    return valoracionTecnica;
  }



  /**
   * Sets the valoracion tecnica.
   *
   * @param valoracionTecnica the valoracionTecnica to set
   */
  public void setValoracionTecnica(final String valoracionTecnica) {
    this.valoracionTecnica = valoracionTecnica;
  }

  /**
   * Gets the tipo convivencia.
   *
   * @return the tipo convivencia
   */
  public String getTipoConvivencia() {
    return tipoConvivencia;
  }

  /**
   * Sets the tipo convivencia.
   *
   * @param tipoConvivencia the new tipo convivencia
   */
  public void setTipoConvivencia(final String tipoConvivencia) {
    this.tipoConvivencia = tipoConvivencia;
  }



  /**
   * Gets the apoyo familiar diario.
   *
   * @return the apoyoFamiliarDiario
   */
  public Boolean getApoyoFamiliarDiario() {
    return apoyoFamiliarDiario;
  }



  /**
   * Sets the apoyo familiar diario.
   *
   * @param apoyoFamiliarDiario the apoyoFamiliarDiario to set
   */
  public void setApoyoFamiliarDiario(final Boolean apoyoFamiliarDiario) {
    this.apoyoFamiliarDiario = apoyoFamiliarDiario;
  }



  /**
   * Gets the apoyo familiar esporadico.
   *
   * @return the apoyoFamiliarEsporadico
   */
  public Boolean getApoyoFamiliarEsporadico() {
    return apoyoFamiliarEsporadico;
  }



  /**
   * Sets the apoyo familiar esporadico.
   *
   * @param apoyoFamiliarEsporadico the apoyoFamiliarEsporadico to set
   */
  public void setApoyoFamiliarEsporadico(
      final Boolean apoyoFamiliarEsporadico) {
    this.apoyoFamiliarEsporadico = apoyoFamiliarEsporadico;
  }



  /**
   * Gets the apoyo social diario.
   *
   * @return the apoyoSocialDiario
   */
  public Boolean getApoyoSocialDiario() {
    return apoyoSocialDiario;
  }



  /**
   * Sets the apoyo social diario.
   *
   * @param apoyoSocialDiario the apoyoSocialDiario to set
   */
  public void setApoyoSocialDiario(final Boolean apoyoSocialDiario) {
    this.apoyoSocialDiario = apoyoSocialDiario;
  }



  /**
   * Gets the apoyo social esporadico.
   *
   * @return the apoyoSocialEsporadico
   */
  public Boolean getApoyoSocialEsporadico() {
    return apoyoSocialEsporadico;
  }



  /**
   * Sets the apoyo social esporadico.
   *
   * @param apoyoSocialEsporadico the apoyoSocialEsporadico to set
   */
  public void setApoyoSocialEsporadico(final Boolean apoyoSocialEsporadico) {
    this.apoyoSocialEsporadico = apoyoSocialEsporadico;
  }



  /**
   * Gets the sin apoyo.
   *
   * @return the sinApoyo
   */
  public Boolean getSinApoyo() {
    return sinApoyo;
  }



  /**
   * Sets the sin apoyo.
   *
   * @param sinApoyo the sinApoyo to set
   */
  public void setSinApoyo(final Boolean sinApoyo) {
    this.sinApoyo = sinApoyo;
  }

  /**
   * Gets the sin apoyo social.
   *
   * @return the sin apoyo social
   */
  public Boolean getSinApoyoSocial() {
    return sinApoyoSocial;
  }

  /**
   * Sets the sin apoyo social.
   *
   * @param sinApoyoSocial the new sin apoyo social
   */
  public void setSinApoyoSocial(final Boolean sinApoyoSocial) {
    this.sinApoyoSocial = sinApoyoSocial;
  }

  /**
   * Gets the nivel relacion.
   *
   * @return the nivelRelacion
   */
  public String getNivelRelacion() {
    return nivelRelacion;
  }



  /**
   * Sets the nivel relacion.
   *
   * @param nivelRelacion the nivelRelacion to set
   */
  public void setNivelRelacion(final String nivelRelacion) {
    this.nivelRelacion = nivelRelacion;
  }



  /**
   * Gets the mayor edad.
   *
   * @return the mayorEdad
   */
  public Boolean getMayorEdad() {
    return mayorEdad;
  }



  /**
   * Sets the mayor edad.
   *
   * @param mayorEdad the mayorEdad to set
   */
  public void setMayorEdad(final Boolean mayorEdad) {
    this.mayorEdad = mayorEdad;
  }



  /**
   * Gets the cuidador predependiente.
   *
   * @return the cuidador predependiente
   */
  public Boolean getCuidadorPredependiente() {
    return cuidadorPredependiente;
  }



  /**
   * Sets the cuidador predependiente.
   *
   * @param cuidadorPredependiente the new cuidador predependiente
   */
  public void setCuidadorPredependiente(final Boolean cuidadorPredependiente) {
    this.cuidadorPredependiente = cuidadorPredependiente;
  }



  /**
   * Gets the concurrencia enfermedad.
   *
   * @return the concurrenciaEnfermedad
   */
  public Boolean getConcurrenciaEnfermedad() {
    return concurrenciaEnfermedad;
  }



  /**
   * Sets the concurrencia enfermedad.
   *
   * @param concurrenciaEnfermedad the concurrenciaEnfermedad to set
   */
  public void setConcurrenciaEnfermedad(final Boolean concurrenciaEnfermedad) {
    this.concurrenciaEnfermedad = concurrenciaEnfermedad;
  }



  /**
   * Gets the seguridad economica.
   *
   * @return the seguridadEconomica
   */
  public Boolean getSeguridadEconomica() {
    return seguridadEconomica;
  }



  /**
   * Sets the seguridad economica.
   *
   * @param seguridadEconomica the seguridadEconomica to set
   */
  public void setSeguridadEconomica(final Boolean seguridadEconomica) {
    this.seguridadEconomica = seguridadEconomica;
  }



  /**
   * Gets the conocimientos suficientes.
   *
   * @return the conocimientosSuficientes
   */
  public Boolean getConocimientosSuficientes() {
    return conocimientosSuficientes;
  }



  /**
   * Sets the conocimientos suficientes.
   *
   * @param conocimientosSuficientes the conocimientosSuficientes to set
   */
  public void setConocimientosSuficientes(
      final Boolean conocimientosSuficientes) {
    this.conocimientosSuficientes = conocimientosSuficientes;
  }



  /**
   * Gets the conocimientos escasos.
   *
   * @return the conocimientosEscasos
   */
  public Boolean getConocimientosEscasos() {
    return conocimientosEscasos;
  }



  /**
   * Sets the conocimientos escasos.
   *
   * @param conocimientosEscasos the conocimientosEscasos to set
   */
  public void setConocimientosEscasos(final Boolean conocimientosEscasos) {
    this.conocimientosEscasos = conocimientosEscasos;
  }



  /**
   * Gets the dificultad comprension enfermedad.
   *
   * @return the dificultadComprensionEnfermedad
   */
  public Boolean getDificultadComprensionEnfermedad() {
    return dificultadComprensionEnfermedad;
  }



  /**
   * Sets the dificultad comprension enfermedad.
   *
   * @param dificultadComprensionEnfermedad the dificultadComprensionEnfermedad
   *        to set
   */
  public void setDificultadComprensionEnfermedad(
      final Boolean dificultadComprensionEnfermedad) {
    this.dificultadComprensionEnfermedad = dificultadComprensionEnfermedad;
  }



  /**
   * Gets the disponibilidad apoyos profesionales.
   *
   * @return the disponibilidadApoyosProfesionales
   */
  public Boolean getDisponibilidadApoyosProfesionales() {
    return disponibilidadApoyosProfesionales;
  }



  /**
   * Sets the disponibilidad apoyos profesionales.
   *
   * @param disponibilidadApoyosProfesionales the
   *        disponibilidadApoyosProfesionales to set
   */
  public void setDisponibilidadApoyosProfesionales(
      final Boolean disponibilidadApoyosProfesionales) {
    this.disponibilidadApoyosProfesionales = disponibilidadApoyosProfesionales;
  }



  /**
   * Gets the disponibilidad tiempo.
   *
   * @return the disponibilidadTiempo
   */
  public Boolean getDisponibilidadTiempo() {
    return disponibilidadTiempo;
  }



  /**
   * Sets the disponibilidad tiempo.
   *
   * @param disponibilidadTiempo the disponibilidadTiempo to set
   */
  public void setDisponibilidadTiempo(final Boolean disponibilidadTiempo) {
    this.disponibilidadTiempo = disponibilidadTiempo;
  }



  /**
   * Gets the periodos descanso.
   *
   * @return the periodosDescanso
   */
  public Boolean getPeriodosDescanso() {
    return periodosDescanso;
  }



  /**
   * Sets the periodos descanso.
   *
   * @param periodosDescanso the periodosDescanso to set
   */
  public void setPeriodosDescanso(final Boolean periodosDescanso) {
    this.periodosDescanso = periodosDescanso;
  }



  /**
   * Gets the signos agotamiento.
   *
   * @return the signosAgotamiento
   */
  public Boolean getSignosAgotamiento() {
    return signosAgotamiento;
  }



  /**
   * Sets the signos agotamiento.
   *
   * @param signosAgotamiento the signosAgotamiento to set
   */
  public void setSignosAgotamiento(final Boolean signosAgotamiento) {
    this.signosAgotamiento = signosAgotamiento;
  }

  /**
   * Gets the sin signos fragilidad.
   *
   * @return the sinSignosFragilidad
   */
  public Boolean getSinSignosFragilidad() {
    return sinSignosFragilidad;
  }


  /**
   * Sets the sin signos fragilidad.
   *
   * @param sinSignosFragilidad the sinSignosFragilidad to set
   */
  public void setSinSignosFragilidad(final Boolean sinSignosFragilidad) {
    this.sinSignosFragilidad = sinSignosFragilidad;
  }


  /**
   * Gets the ausencia compromiso.
   *
   * @return the ausencia compromiso
   */
  public Boolean getAusenciaCompromiso() {
    return ausenciaCompromiso;
  }

  /**
   * Sets the ausencia compromiso.
   *
   * @param ausenciaCompromiso the new ausencia compromiso
   */
  public void setAusenciaCompromiso(final Boolean ausenciaCompromiso) {
    this.ausenciaCompromiso = ausenciaCompromiso;
  }

  /**
   * Gets the dificultados conexion.
   *
   * @return the dificultadosConexion
   */
  public Boolean getDificultadosConexion() {
    return dificultadosConexion;
  }



  /**
   * Sets the dificultados conexion.
   *
   * @param dificultadosConexion the dificultadosConexion to set
   */
  public void setDificultadosConexion(final Boolean dificultadosConexion) {
    this.dificultadosConexion = dificultadosConexion;
  }



  /**
   * Gets the riesgo claudicacion.
   *
   * @return the riesgoClaudicacion
   */
  public String getRiesgoClaudicacion() {
    return riesgoClaudicacion;
  }



  /**
   * Sets the riesgo claudicacion.
   *
   * @param riesgoClaudicacion the riesgoClaudicacion to set
   */
  public void setRiesgoClaudicacion(final String riesgoClaudicacion) {
    this.riesgoClaudicacion = riesgoClaudicacion;
  }


  /**
   * Gets the abandono.
   *
   * @return the abandono
   */
  public Boolean getAbandono() {
    return abandono;
  }


  /**
   * Sets the abandono.
   *
   * @param abandono the new abandono
   */
  public void setAbandono(final Boolean abandono) {
    this.abandono = abandono;
  }


  /**
   * Gets the propiedad vivienda habitual.
   *
   * @return the propiedadViviendaHabitual
   */
  public String getPropiedadViviendaHabitual() {
    return propiedadViviendaHabitual;
  }



  /**
   * Sets the propiedad vivienda habitual.
   *
   * @param propiedadViviendaHabitual the propiedadViviendaHabitual to set
   */
  public void setPropiedadViviendaHabitual(
      final String propiedadViviendaHabitual) {
    this.propiedadViviendaHabitual = propiedadViviendaHabitual;
  }



  /**
   * Gets the condiciones vivienda.
   *
   * @return the condicionesVivienda
   */
  public String getCondicionesVivienda() {
    return condicionesVivienda;
  }



  /**
   * Sets the condiciones vivienda.
   *
   * @param condicionesVivienda the condicionesVivienda to set
   */
  public void setCondicionesVivienda(final String condicionesVivienda) {
    this.condicionesVivienda = condicionesVivienda;
  }



  /**
   * Gets the adaptacion hogar.
   *
   * @return the adaptacionHogar
   */
  public String getAdaptacionHogar() {
    return adaptacionHogar;
  }



  /**
   * Sets the adaptacion hogar.
   *
   * @param adaptacionHogar the adaptacionHogar to set
   */
  public void setAdaptacionHogar(final String adaptacionHogar) {
    this.adaptacionHogar = adaptacionHogar;
  }



  /**
   * Gets the facilita acceso.
   *
   * @return the facilitaAcceso
   */
  public Long getFacilitaAcceso() {
    return facilitaAcceso;
  }



  /**
   * Sets the facilita acceso.
   *
   * @param facilitaAcceso the facilitaAcceso to set
   */
  public void setFacilitaAcceso(final Long facilitaAcceso) {
    this.facilitaAcceso = facilitaAcceso;
  }



  /**
   * Gets the valoracion social.
   *
   * @return the valoracionSocial
   */
  public String getValoracionSocial() {
    return valoracionSocial;
  }



  /**
   * Sets the valoracion social.
   *
   * @param valoracionSocial the valoracionSocial to set
   */
  public void setValoracionSocial(final String valoracionSocial) {
    this.valoracionSocial = valoracionSocial;
  }



  /**
   * Gets the coinciden valores preferencias.
   *
   * @return the coincidenValoresPreferencias
   */
  public Boolean getCoincidenValoresPreferencias() {
    return coincidenValoresPreferencias;
  }



  /**
   * Sets the coinciden valores preferencias.
   *
   * @param coincidenValoresPreferencias the coincidenValoresPreferencias to set
   */
  public void setCoincidenValoresPreferencias(
      final Boolean coincidenValoresPreferencias) {
    this.coincidenValoresPreferencias = coincidenValoresPreferencias;
  }



  /**
   * Gets the motivos priorizacion profesional.
   *
   * @return the motivosPriorizacionProfesional
   */
  public String getMotivosPriorizacionProfesional() {
    return motivosPriorizacionProfesional;
  }



  /**
   * Sets the motivos priorizacion profesional.
   *
   * @param motivosPriorizacionProfesional the motivosPriorizacionProfesional to
   *        set
   */
  public void setMotivosPriorizacionProfesional(
      final String motivosPriorizacionProfesional) {
    this.motivosPriorizacionProfesional = motivosPriorizacionProfesional;
  }



  /**
   * Gets the identificador trabajador social.
   *
   * @return the identificadorTrabajadorSocial
   */
  public String getIdentificadorTrabajadorSocial() {
    return identificadorTrabajadorSocial;
  }



  /**
   * Sets the identificador trabajador social.
   *
   * @param identificadorTrabajadorSocial the identificadorTrabajadorSocial to
   *        set
   */
  public void setIdentificadorTrabajadorSocial(
      final String identificadorTrabajadorSocial) {
    this.identificadorTrabajadorSocial = identificadorTrabajadorSocial;
  }



  /**
   * Gets the numero colegiado.
   *
   * @return the numeroColegiado
   */
  public String getNumeroColegiado() {
    return numeroColegiado;
  }



  /**
   * Sets the numero colegiado.
   *
   * @param numeroColegiado the numeroColegiado to set
   */
  public void setNumeroColegiado(final String numeroColegiado) {
    this.numeroColegiado = numeroColegiado;
  }



  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }



  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }



  /**
   * Gets the nivel cultural.
   *
   * @return the nivelCultural
   */
  public Long getNivelCultural() {
    return nivelCultural;
  }



  /**
   * Sets the nivel cultural.
   *
   * @param nivelCultural the nivelCultural to set
   */
  public void setNivelCultural(final Long nivelCultural) {
    this.nivelCultural = nivelCultural;
  }



  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }



  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }



  /**
   * Gets the tipo centro.
   *
   * @return the tipoCentro
   */
  public Long getTipoCentro() {
    return tipoCentro;
  }



  /**
   * Sets the tipo centro.
   *
   * @param tipoCentro the tipoCentro to set
   */
  public void setTipoCentro(final Long tipoCentro) {
    this.tipoCentro = tipoCentro;
  }



  /**
   * Gets the tiene edad laboral.
   *
   * @return the tieneEdadLaboral
   */
  public Boolean getTieneEdadLaboral() {
    return tieneEdadLaboral;
  }



  /**
   * Sets the tiene edad laboral.
   *
   * @param tieneEdadLaboral the tieneEdadLaboral to set
   */
  public void setTieneEdadLaboral(final Boolean tieneEdadLaboral) {
    this.tieneEdadLaboral = tieneEdadLaboral;
  }



  /**
   * Gets the activo laboralmente.
   *
   * @return the activoLaboralmente
   */
  public Boolean getActivoLaboralmente() {
    return activoLaboralmente;
  }



  /**
   * Sets the activo laboralmente.
   *
   * @param activoLaboralmente the activoLaboralmente to set
   */
  public void setActivoLaboralmente(final Boolean activoLaboralmente) {
    this.activoLaboralmente = activoLaboralmente;
  }



  /**
   * Gets the situacion inactividad.
   *
   * @return the situacionInactividad
   */
  public Long getSituacionInactividad() {
    return situacionInactividad;
  }



  /**
   * Sets the situacion inactividad.
   *
   * @param situacionInactividad the situacionInactividad to set
   */
  public void setSituacionInactividad(final Long situacionInactividad) {
    this.situacionInactividad = situacionInactividad;
  }



  /**
   * Gets the tipo jornada.
   *
   * @return the tipoJornada
   */
  public Long getTipoJornada() {
    return tipoJornada;
  }



  /**
   * Sets the tipo jornada.
   *
   * @param tipoJornada the tipoJornada to set
   */
  public void setTipoJornada(final Long tipoJornada) {
    this.tipoJornada = tipoJornada;
  }



  /**
   * Gets the profesion solicitante.
   *
   * @return the profesionSolicitante
   */
  public String getProfesionSolicitante() {
    return profesionSolicitante;
  }



  /**
   * Sets the profesion solicitante.
   *
   * @param profesionSolicitante the profesionSolicitante to set
   */
  public void setProfesionSolicitante(final String profesionSolicitante) {
    this.profesionSolicitante = profesionSolicitante;
  }


  /**
   * Gets the soporte social Y familiar.
   *
   * @return the soporteSocialYFamiliar
   */
  public Long getSoporteSocialYFamiliar() {
    return soporteSocialYFamiliar;
  }



  /**
   * Sets the soporte social Y familiar.
   *
   * @param soporteSocialYFamiliar the soporteSocialYFamiliar to set
   */
  public void setSoporteSocialYFamiliar(final Long soporteSocialYFamiliar) {
    this.soporteSocialYFamiliar = soporteSocialYFamiliar;
  }



  /**
   * Gets the ayuda movilidad baston muleta.
   *
   * @return the ayudaMovilidadBastonMuleta
   */
  public Boolean getAyudaMovilidadBastonMuleta() {
    return ayudaMovilidadBastonMuleta;
  }



  /**
   * Sets the ayuda movilidad baston muleta.
   *
   * @param ayudaMovilidadBastonMuleta the ayudaMovilidadBastonMuleta to set
   */
  public void setAyudaMovilidadBastonMuleta(
      final Boolean ayudaMovilidadBastonMuleta) {
    this.ayudaMovilidadBastonMuleta = ayudaMovilidadBastonMuleta;
  }



  /**
   * Gets the ayuda movilidad silla ruedas.
   *
   * @return the ayudaMovilidadSillaRuedas
   */
  public Boolean getAyudaMovilidadSillaRuedas() {
    return ayudaMovilidadSillaRuedas;
  }



  /**
   * Sets the ayuda movilidad silla ruedas.
   *
   * @param ayudaMovilidadSillaRuedas the ayudaMovilidadSillaRuedas to set
   */
  public void setAyudaMovilidadSillaRuedas(
      final Boolean ayudaMovilidadSillaRuedas) {
    this.ayudaMovilidadSillaRuedas = ayudaMovilidadSillaRuedas;
  }



  /**
   * Gets the ayuda movilidad grua transferencia.
   *
   * @return the ayudaMovilidadGruaTransferencia
   */
  public Boolean getAyudaMovilidadGruaTransferencia() {
    return ayudaMovilidadGruaTransferencia;
  }



  /**
   * Sets the ayuda movilidad grua transferencia.
   *
   * @param ayudaMovilidadGruaTransferencia the ayudaMovilidadGruaTransferencia
   *        to set
   */
  public void setAyudaMovilidadGruaTransferencia(
      final Boolean ayudaMovilidadGruaTransferencia) {
    this.ayudaMovilidadGruaTransferencia = ayudaMovilidadGruaTransferencia;
  }



  /**
   * Gets the ayuda movilidad otros.
   *
   * @return the ayudaMovilidadOtros
   */
  public Boolean getAyudaMovilidadOtros() {
    return ayudaMovilidadOtros;
  }



  /**
   * Sets the ayuda movilidad otros.
   *
   * @param ayudaMovilidadOtros the ayudaMovilidadOtros to set
   */
  public void setAyudaMovilidadOtros(final Boolean ayudaMovilidadOtros) {
    this.ayudaMovilidadOtros = ayudaMovilidadOtros;
  }



  /**
   * Gets the otras ayudas movilidad.
   *
   * @return the otrasAyudasMovilidad
   */
  public String getOtrasAyudasMovilidad() {
    return otrasAyudasMovilidad;
  }



  /**
   * Sets the otras ayudas movilidad.
   *
   * @param otrasAyudasMovilidad the otrasAyudasMovilidad to set
   */
  public void setOtrasAyudasMovilidad(final String otrasAyudasMovilidad) {
    this.otrasAyudasMovilidad = otrasAyudasMovilidad;
  }



  /**
   * Gets the ayuda aseo personal.
   *
   * @return the ayudaAseoPersonal
   */
  public Boolean getAyudaAseoPersonal() {
    return ayudaAseoPersonal;
  }



  /**
   * Sets the ayuda aseo personal.
   *
   * @param ayudaAseoPersonal the ayudaAseoPersonal to set
   */
  public void setAyudaAseoPersonal(final Boolean ayudaAseoPersonal) {
    this.ayudaAseoPersonal = ayudaAseoPersonal;
  }



  /**
   * Gets the ayuda aseo banio Y ducha.
   *
   * @return the ayudaAseoBanioYDucha
   */
  public Boolean getAyudaAseoBanioYDucha() {
    return ayudaAseoBanioYDucha;
  }



  /**
   * Sets the ayuda aseo banio Y ducha.
   *
   * @param ayudaAseoBanioYDucha the ayudaAseoBanioYDucha to set
   */
  public void setAyudaAseoBanioYDucha(final Boolean ayudaAseoBanioYDucha) {
    this.ayudaAseoBanioYDucha = ayudaAseoBanioYDucha;
  }



  /**
   * Gets the ayuda aseo otros.
   *
   * @return the ayudaAseoOtros
   */
  public Boolean getAyudaAseoOtros() {
    return ayudaAseoOtros;
  }



  /**
   * Sets the ayuda aseo otros.
   *
   * @param ayudaAseoOtros the ayudaAseoOtros to set
   */
  public void setAyudaAseoOtros(final Boolean ayudaAseoOtros) {
    this.ayudaAseoOtros = ayudaAseoOtros;
  }



  /**
   * Gets the otras ayudas aseo.
   *
   * @return the otrasAyudasAseo
   */
  public String getOtrasAyudasAseo() {
    return otrasAyudasAseo;
  }



  /**
   * Sets the otras ayudas aseo.
   *
   * @param otrasAyudasAseo the otrasAyudasAseo to set
   */
  public void setOtrasAyudasAseo(final String otrasAyudasAseo) {
    this.otrasAyudasAseo = otrasAyudasAseo;
  }



  /**
   * Gets the ayuda alimentacion para preparar comida.
   *
   * @return the ayudaAlimentacionParaPrepararComida
   */
  public Boolean getAyudaAlimentacionParaPrepararComida() {
    return ayudaAlimentacionParaPrepararComida;
  }



  /**
   * Sets the ayuda alimentacion para preparar comida.
   *
   * @param ayudaAlimentacionParaPrepararComida the
   *        ayudaAlimentacionParaPrepararComida to set
   */
  public void setAyudaAlimentacionParaPrepararComida(
      final Boolean ayudaAlimentacionParaPrepararComida) {
    this.ayudaAlimentacionParaPrepararComida =
        ayudaAlimentacionParaPrepararComida;
  }



  /**
   * Gets the ayuda alimentacion para comer Y beber.
   *
   * @return the ayudaAlimentacionParaComerYBeber
   */
  public Boolean getAyudaAlimentacionParaComerYBeber() {
    return ayudaAlimentacionParaComerYBeber;
  }



  /**
   * Sets the ayuda alimentacion para comer Y beber.
   *
   * @param ayudaAlimentacionParaComerYBeber the
   *        ayudaAlimentacionParaComerYBeber to set
   */
  public void setAyudaAlimentacionParaComerYBeber(
      final Boolean ayudaAlimentacionParaComerYBeber) {
    this.ayudaAlimentacionParaComerYBeber = ayudaAlimentacionParaComerYBeber;
  }



  /**
   * Gets the ayuda alimentacion otros.
   *
   * @return the ayudaAlimentacionOtros
   */
  public Boolean getAyudaAlimentacionOtros() {
    return ayudaAlimentacionOtros;
  }



  /**
   * Sets the ayuda alimentacion otros.
   *
   * @param ayudaAlimentacionOtros the ayudaAlimentacionOtros to set
   */
  public void setAyudaAlimentacionOtros(final Boolean ayudaAlimentacionOtros) {
    this.ayudaAlimentacionOtros = ayudaAlimentacionOtros;
  }



  /**
   * Gets the otras ayudas alimentacion.
   *
   * @return the otrasAyudasAlimentacion
   */
  public String getOtrasAyudasAlimentacion() {
    return otrasAyudasAlimentacion;
  }



  /**
   * Sets the otras ayudas alimentacion.
   *
   * @param otrasAyudasAlimentacion the otrasAyudasAlimentacion to set
   */
  public void setOtrasAyudasAlimentacion(final String otrasAyudasAlimentacion) {
    this.otrasAyudasAlimentacion = otrasAyudasAlimentacion;
  }



  /**
   * Gets the ayudas tecnicas tic.
   *
   * @return the ayudasTecnicasTic
   */
  public String getAyudasTecnicasTic() {
    return ayudasTecnicasTic;
  }



  /**
   * Sets the ayudas tecnicas tic.
   *
   * @param ayudasTecnicasTic the ayudasTecnicasTic to set
   */
  public void setAyudasTecnicasTic(final String ayudasTecnicasTic) {
    this.ayudasTecnicasTic = ayudasTecnicasTic;
  }



  /**
   * Gets the ancho puertas.
   *
   * @return the anchoPuertas
   */
  public Boolean getAnchoPuertas() {
    return anchoPuertas;
  }



  /**
   * Sets the ancho puertas.
   *
   * @param anchoPuertas the anchoPuertas to set
   */
  public void setAnchoPuertas(final Boolean anchoPuertas) {
    this.anchoPuertas = anchoPuertas;
  }



  /**
   * Gets the escaleras interiores.
   *
   * @return the escalerasInteriores
   */
  public Boolean getEscalerasInteriores() {
    return escalerasInteriores;
  }



  /**
   * Sets the escaleras interiores.
   *
   * @param escalerasInteriores the escalerasInteriores to set
   */
  public void setEscalerasInteriores(final Boolean escalerasInteriores) {
    this.escalerasInteriores = escalerasInteriores;
  }



  /**
   * Gets the adecuacion sanitarios.
   *
   * @return the adecuacionSanitarios
   */
  public Boolean getAdecuacionSanitarios() {
    return adecuacionSanitarios;
  }



  /**
   * Sets the adecuacion sanitarios.
   *
   * @param adecuacionSanitarios the adecuacionSanitarios to set
   */
  public void setAdecuacionSanitarios(final Boolean adecuacionSanitarios) {
    this.adecuacionSanitarios = adecuacionSanitarios;
  }



  /**
   * Gets the pasamanos.
   *
   * @return the pasamanos
   */
  public Boolean getPasamanos() {
    return pasamanos;
  }



  /**
   * Sets the pasamanos.
   *
   * @param pasamanos the pasamanos to set
   */
  public void setPasamanos(final Boolean pasamanos) {
    this.pasamanos = pasamanos;
  }



  /**
   * Gets the adecuacion cocina.
   *
   * @return the adecuacionCocina
   */
  public Boolean getAdecuacionCocina() {
    return adecuacionCocina;
  }



  /**
   * Sets the adecuacion cocina.
   *
   * @param adecuacionCocina the adecuacionCocina to set
   */
  public void setAdecuacionCocina(final Boolean adecuacionCocina) {
    this.adecuacionCocina = adecuacionCocina;
  }



  /**
   * Gets the instalacion gas luz.
   *
   * @return the instalacionGasLuz
   */
  public Boolean getInstalacionGasLuz() {
    return instalacionGasLuz;
  }



  /**
   * Sets the instalacion gas luz.
   *
   * @param instalacionGasLuz the instalacionGasLuz to set
   */
  public void setInstalacionGasLuz(final Boolean instalacionGasLuz) {
    this.instalacionGasLuz = instalacionGasLuz;
  }



  /**
   * Gets the suelo antideslizante.
   *
   * @return the sueloAntideslizante
   */
  public Boolean getSueloAntideslizante() {
    return sueloAntideslizante;
  }



  /**
   * Sets the suelo antideslizante.
   *
   * @param sueloAntideslizante the sueloAntideslizante to set
   */
  public void setSueloAntideslizante(final Boolean sueloAntideslizante) {
    this.sueloAntideslizante = sueloAntideslizante;
  }



  /**
   * Gets the otros dentro vivienda.
   *
   * @return the otrosDentroVivienda
   */
  public Boolean getOtrosDentroVivienda() {
    return otrosDentroVivienda;
  }



  /**
   * Sets the otros dentro vivienda.
   *
   * @param otrosDentroVivienda the otrosDentroVivienda to set
   */
  public void setOtrosDentroVivienda(final Boolean otrosDentroVivienda) {
    this.otrosDentroVivienda = otrosDentroVivienda;
  }



  /**
   * Gets the rampas.
   *
   * @return the rampas
   */
  public Boolean getRampas() {
    return rampas;
  }



  /**
   * Sets the rampas.
   *
   * @param rampas the rampas to set
   */
  public void setRampas(final Boolean rampas) {
    this.rampas = rampas;
  }



  /**
   * Gets the ascensor.
   *
   * @return the ascensor
   */
  public Boolean getAscensor() {
    return ascensor;
  }



  /**
   * Sets the ascensor.
   *
   * @param ascensor the ascensor to set
   */
  public void setAscensor(final Boolean ascensor) {
    this.ascensor = ascensor;
  }



  /**
   * Gets the silla salvaescalera.
   *
   * @return the sillaSalvaescalera
   */
  public Boolean getSillaSalvaescalera() {
    return sillaSalvaescalera;
  }



  /**
   * Sets the silla salvaescalera.
   *
   * @param sillaSalvaescalera the sillaSalvaescalera to set
   */
  public void setSillaSalvaescalera(final Boolean sillaSalvaescalera) {
    this.sillaSalvaescalera = sillaSalvaescalera;
  }



  /**
   * Gets the otrosAccesoHogar.
   *
   * @return the otrosAccesoHogar
   */
  public Boolean getOtrosAccesoHogar() {
    return otrosAccesoHogar;
  }


  /**
   * Sets the otrosAccesoHogar.
   *
   * @param inotacchg the new otrosAccesoHogar
   */
  public void setOtrosAccesoHogar(final Boolean inotacchg) {
    this.otrosAccesoHogar = inotacchg;
  }

  /**
   * Gets the otros entorno.
   *
   * @return the otrosEntorno
   */
  public String getOtrosEntorno() {
    return otrosEntorno;
  }



  /**
   * Sets the otros entorno.
   *
   * @param otrosEntorno the otrosEntorno to set
   */
  public void setOtrosEntorno(final String otrosEntorno) {
    this.otrosEntorno = otrosEntorno;
  }



  /**
   * Gets the dificultades servicio.
   *
   * @return the dificultadesServicio
   */
  public String getDificultadesServicio() {
    return dificultadesServicio;
  }



  /**
   * Sets the dificultades servicio.
   *
   * @param dificultadesServicio the dificultadesServicio to set
   */
  public void setDificultadesServicio(final String dificultadesServicio) {
    this.dificultadesServicio = dificultadesServicio;
  }



  /**
   * Gets the info cuidador.
   *
   * @return the infoCuidador
   */
  public String getInfoCuidador() {
    return infoCuidador;
  }



  /**
   * Sets the info cuidador.
   *
   * @param infoCuidador the infoCuidador to set
   */
  public void setInfoCuidador(final String infoCuidador) {
    this.infoCuidador = infoCuidador;
  }



  /**
   * Gets the info asistente.
   *
   * @return the infoAsistente
   */
  public String getInfoAsistente() {
    return infoAsistente;
  }



  /**
   * Sets the info asistente.
   *
   * @param infoAsistente the infoAsistente to set
   */
  public void setInfoAsistente(final String infoAsistente) {
    this.infoAsistente = infoAsistente;
  }



  /**
   * Gets the pedido.
   *
   * @return the pedido
   */
  public Long getPedido() {
    return pedido;
  }



  /**
   * Sets the pedido.
   *
   * @param pedido the pedido to set
   */
  public void setPedido(final Long pedido) {
    this.pedido = pedido;
  }



  /**
   * Gets the tipo informe social.
   *
   * @return the tipoInformeSocial
   */
  public Long getTipoInformeSocial() {
    return tipoInformeSocial;
  }



  /**
   * Sets the tipo informe social.
   *
   * @param tipoInformeSocial the tipoInformeSocial to set
   */
  public void setTipoInformeSocial(final Long tipoInformeSocial) {
    this.tipoInformeSocial = tipoInformeSocial;
  }



  /**
   * Gets the situacion capacidad laboral.
   *
   * @return the situacionCapacidadLaboral
   */
  public Long getSituacionCapacidadLaboral() {
    return situacionCapacidadLaboral;
  }



  /**
   * Sets the situacion capacidad laboral.
   *
   * @param situacionCapacidadLaboral the situacionCapacidadLaboral to set
   */
  public void setSituacionCapacidadLaboral(
      final Long situacionCapacidadLaboral) {
    this.situacionCapacidadLaboral = situacionCapacidadLaboral;
  }



  /**
   * Gets the estado civil.
   *
   * @return the estadoCivil
   */
  public Long getEstadoCivil() {
    return estadoCivil;
  }



  /**
   * Sets the estado civil.
   *
   * @param estadoCivil the estadoCivil to set
   */
  public void setEstadoCivil(final Long estadoCivil) {
    this.estadoCivil = estadoCivil;
  }



  /**
   * Gets the modalidad.
   *
   * @return the modalidad
   */
  public String getModalidad() {
    return modalidad;
  }



  /**
   * Sets the modalidad.
   *
   * @param modalidad the modalidad to set
   */
  public void setModalidad(final String modalidad) {
    this.modalidad = modalidad;
  }



  /**
   * Gets the intensidad asistencia.
   *
   * @return the intensidadAsistencia
   */
  public String getIntensidadAsistencia() {
    return intensidadAsistencia;
  }



  /**
   * Sets the intensidad asistencia.
   *
   * @param intensidadAsistencia the intensidadAsistencia to set
   */
  public void setIntensidadAsistencia(final String intensidadAsistencia) {
    this.intensidadAsistencia = intensidadAsistencia;
  }



  /**
   * Gets the fecha cuidador.
   *
   * @return the fechaCuidador
   */
  public Date getFechaCuidador() {
    return UtilidadesCommons.cloneDate(fechaCuidador);
  }



  /**
   * Sets the fecha cuidador.
   *
   * @param fechaCuidador the fechaCuidador to set
   */
  public void setFechaCuidador(final Date fechaCuidador) {
    this.fechaCuidador = UtilidadesCommons.cloneDate(fechaCuidador);
  }



  /**
   * Gets the motivoPersonal.
   *
   * @return the motivoPersonal
   */
  public Boolean getMotivoPersonal() {
    return motivoPersonal;
  }

  /**
   * Sets the motivoPersonal.
   *
   * @param motivopersonal the new motivoPersonal
   */
  public void setMotivoPersonal(final Boolean motivopersonal) {
    this.motivoPersonal = motivopersonal;
  }

  /**
   * Gets the preferencias revisadas.
   *
   * @return the preferenciasRevisadas
   */
  public Boolean getPreferenciasRevisadas() {
    return preferenciasRevisadas;
  }



  /**
   * Sets the preferencias revisadas.
   *
   * @param preferenciasRevisadas the preferenciasRevisadas to set
   */
  public void setPreferenciasRevisadas(final Boolean preferenciasRevisadas) {
    this.preferenciasRevisadas = preferenciasRevisadas;
  }



  /**
   * Gets the cambio revisado.
   *
   * @return the cambioRevisado
   */
  public Boolean getCambioRevisado() {
    return cambioRevisado;
  }



  /**
   * Sets the cambio revisado.
   *
   * @param cambioRevisado the cambioRevisado to set
   */
  public void setCambioRevisado(final Boolean cambioRevisado) {
    this.cambioRevisado = cambioRevisado;
  }



  /**
   * Gets the documento nuevas preferencias.
   *
   * @return the pkDocumentoPreferencias
   */
  public Long getPkDocumentoPreferencias() {
    return pkDocumentoPreferencias;
  }

  /**
   * Sets the documento nuevas preferencias.
   *
   * @param documentoNuevasPreferencias the new documento nuevas preferencias
   */
  public void setPkDocumentoPreferencias(
      final Long documentoNuevasPreferencias) {
    this.pkDocumentoPreferencias = documentoNuevasPreferencias;
  }

  /**
   * Gets the pk pia.
   *
   * @return the pk pia
   */
  public Long getPkPia() {
    return pkPia;
  }

  /**
   * Sets the pk pia.
   *
   * @param pkPia the new pk pia
   */
  public void setPkPia(final Long pkPia) {
    this.pkPia = pkPia;
  }

  /**
   * Gets the motivos generacion informe social.
   *
   * @return the motivos generacion informe social
   */
  public List<MotivoGeneracionInformeSocial> getMotivosGeneracionInformeSocial() {
    return UtilidadesCommons.collectorsToList(motivosGeneracionInformeSocial);
  }

  /**
   * Sets the motivos generacion informe social.
   *
   * @param motivosGeneracionInformeSocial the new motivos generacion informe
   *        social
   */
  public void setMotivosGeneracionInformeSocial(
      final List<MotivoGeneracionInformeSocial> motivosGeneracionInformeSocial) {
    this.motivosGeneracionInformeSocial =
        UtilidadesCommons.collectorsToList(motivosGeneracionInformeSocial);
  }



  /**
   * Gets the preferencias informe social.
   *
   * @return the preferencias informe social
   */
  public List<PreferenciaInformeSocial> getPreferenciasInformeSocial() {
    return UtilidadesCommons.collectorsToList(preferenciasInformeSocial);
  }

  /**
   * Sets the preferencias informe social.
   *
   * @param preferenciasInformeSocial the new preferencias informe social
   */
  public void setPreferenciasInformeSocial(
      final List<PreferenciaInformeSocial> preferenciasInformeSocial) {
    this.preferenciasInformeSocial =
        UtilidadesCommons.collectorsToList(preferenciasInformeSocial);
  }

  /**
   * Gets the centro atencion residencial.
   *
   * @return the centro atencion residencial
   */
  public Centro getCentroAtencionResidencial() {
    return centroAtencionResidencial;
  }



  /**
   * Sets the centro atencion residencial.
   *
   * @param centroAtencionResidencial the new centro atencion residencial
   */
  public void setCentroAtencionResidencial(
      final Centro centroAtencionResidencial) {
    this.centroAtencionResidencial = centroAtencionResidencial;
  }



  /**
   * Gets the tiene centro atencion diurna.
   *
   * @return the tiene centro atencion diurna
   */
  public Boolean getTieneCentroAtencionDiurna() {
    return tieneCentroAtencionDiurna;
  }



  /**
   * Sets the tiene centro atencion diurna.
   *
   * @param tieneCentroAtencionDiurna the new tiene centro atencion diurna
   */
  public void setTieneCentroAtencionDiurna(
      final Boolean tieneCentroAtencionDiurna) {
    this.tieneCentroAtencionDiurna = tieneCentroAtencionDiurna;
  }



  /**
   * Gets the centro atencion diurna.
   *
   * @return the centro atencion diurna
   */
  public Centro getCentroAtencionDiurna() {
    return centroAtencionDiurna;
  }



  /**
   * Sets the centro atencion diurna.
   *
   * @param centroAtencionDiurna the new centro atencion diurna
   */
  public void setCentroAtencionDiurna(final Centro centroAtencionDiurna) {
    this.centroAtencionDiurna = centroAtencionDiurna;
  }



  /**
   * Gets the tiene centro residencial.
   *
   * @return the tiene centro residencial
   */
  public Boolean getTieneCentroResidencial() {
    return tieneCentroResidencial;
  }



  /**
   * Sets the tiene centro residencial.
   *
   * @param tieneCentroResidencial the new tiene centro residencial
   */
  public void setTieneCentroResidencial(final Boolean tieneCentroResidencial) {
    this.tieneCentroResidencial = tieneCentroResidencial;
  }

  /**
   * Gets the coincide cuidador.
   *
   * @return the coincide cuidador
   */
  public Boolean getCoincideCuidador() {
    return coincideCuidador;
  }

  /**
   * Gets the documentos informe social.
   *
   * @return the documentos informe social
   */
  public List<DocumentoInformeSocial> getDocumentosInformeSocial() {
    return UtilidadesCommons.collectorsToList(documentosInformeSocial);
  }

  /**
   * Sets the coincide cuidador.
   *
   * @param coincideCuidador the new coincide cuidador
   */
  public void setCoincideCuidador(final Boolean coincideCuidador) {
    this.coincideCuidador = coincideCuidador;
  }

  /**
   * Gets the cuidadores informe social.
   *
   * @return the cuidadores informe social
   */
  public List<CuidadorInformeSocial> getCuidadoresInformeSocial() {
    return UtilidadesCommons.collectorsToList(cuidadoresInformeSocial);
  }

  /**
   * Sets the cuidadores informe social.
   *
   * @param cuidadoresInformeSocial the new cuidadores informe social
   */
  public void setCuidadoresInformeSocial(
      final List<CuidadorInformeSocial> cuidadoresInformeSocial) {
    this.cuidadoresInformeSocial =
        UtilidadesCommons.collectorsToList(cuidadoresInformeSocial);
  }

  /**
   * Gets the asistentes informe social.
   *
   * @return the asistentes informe social
   */
  public List<AsistenteInformeSocial> getAsistentesInformeSocial() {
    return UtilidadesCommons.collectorsToList(asistentesInformeSocial);
  }


  /**
   * Sets the asistentes informe social.
   *
   * @param asistentesInformeSocial the new asistentes informe social
   */
  public void setAsistentesInformeSocial(
      final List<AsistenteInformeSocial> asistentesInformeSocial) {
    this.asistentesInformeSocial =
        UtilidadesCommons.collectorsToList(asistentesInformeSocial);
  }


  /**
   * Sets the documentos informe social.
   *
   * @param documentosInformeSocial the new documentos informe social
   */
  public void setDocumentosInformeSocial(
      final List<DocumentoInformeSocial> documentosInformeSocial) {
    this.documentosInformeSocial =
        UtilidadesCommons.collectorsToList(documentosInformeSocial);
  }

  /**
   * Gets the anamnesis.
   *
   * @return the anamnesis
   */
  public String getAnamnesis() {
    return anamnesis;
  }


  /**
   * Sets the anamnesis.
   *
   * @param anamnesis the new anamnesis
   */
  public void setAnamnesis(final String anamnesis) {
    this.anamnesis = anamnesis;
  }

  /**
   * Gets the tiene vivienda.
   *
   * @return the tiene vivienda
   */
  public Boolean getTieneVivienda() {
    return tieneVivienda;
  }

  /**
   * Sets the tiene vivienda.
   *
   * @param tieneVivienda the new tiene vivienda
   */
  public void setTieneVivienda(final Boolean tieneVivienda) {
    this.tieneVivienda = tieneVivienda;
  }


  /**
   * Gets the conservacion.
   *
   * @return the conservacion
   */
  public Boolean getConservacion() {
    return conservacion;
  }


  /**
   * Sets the conservacion.
   *
   * @param conservacion the new conservacion
   */
  public void setConservacion(final Boolean conservacion) {
    this.conservacion = conservacion;
  }


  /**
   * Gets the habitabilidad.
   *
   * @return the habitabilidad
   */
  public Boolean getHabitabilidad() {
    return habitabilidad;
  }


  /**
   * Sets the habitabilidad.
   *
   * @param habitabilidad the new habitabilidad
   */
  public void setHabitabilidad(final Boolean habitabilidad) {
    this.habitabilidad = habitabilidad;
  }


  /**
   * Gets the salubridad.
   *
   * @return the salubridad
   */
  public Boolean getSalubridad() {
    return salubridad;
  }


  /**
   * Sets the salubridad.
   *
   * @param salubridad the new salubridad
   */
  public void setSalubridad(final Boolean salubridad) {
    this.salubridad = salubridad;
  }


  /**
   * Gets the adaptacion hogar actual.
   *
   * @return the adaptacion hogar actual
   */
  public String getAdaptacionHogarActual() {
    return adaptacionHogarActual;
  }


  /**
   * Sets the adaptacion hogar actual.
   *
   * @param adaptacionHogarActual the new adaptacion hogar actual
   */
  public void setAdaptacionHogarActual(final String adaptacionHogarActual) {
    this.adaptacionHogarActual = adaptacionHogarActual;
  }

  /**
   * Gets the situacion socio familiar.
   *
   * @return the situacion socio familiar
   */
  public String getSituacionSocioFamiliar() {
    return situacionSocioFamiliar;
  }


  /**
   * Sets the situacion socio familiar.
   *
   * @param situacionSocioFamiliar the new situacion socio familiar
   */
  public void setSituacionSocioFamiliar(final String situacionSocioFamiliar) {
    this.situacionSocioFamiliar = situacionSocioFamiliar;
  }

  /**
   * Gets the riesgo claudica.
   *
   * @return the riesgo claudica
   */
  public Boolean getRiesgoClaudica() {
    return riesgoClaudica;
  }


  /**
   * Sets the riesgo claudica.
   *
   * @param riesgoClaudica the new riesgo claudica
   */
  public void setRiesgoClaudica(final Boolean riesgoClaudica) {
    this.riesgoClaudica = riesgoClaudica;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }


  /**
   * Gets the version.
   *
   * @return the version
   */
  public Long getVersion() {
    return version;
  }


  /**
   * Sets the version.
   *
   * @param version the new version
   */
  public void setVersion(final Long version) {
    this.version = version;
  }


  /**
   * Gets the mantener preferencia 1.
   *
   * @return the mantener preferencia 1
   */
  public Boolean getMantenerPreferencia1() {
    return mantenerPreferencia1;
  }


  /**
   * Sets the mantener preferencia 1.
   *
   * @param mantenerPreferencia1 the new mantener preferencia 1
   */
  public void setMantenerPreferencia1(final Boolean mantenerPreferencia1) {
    this.mantenerPreferencia1 = mantenerPreferencia1;
  }


  /**
   * Gets the mantener preferencia 2.
   *
   * @return the mantener preferencia 2
   */
  public Boolean getMantenerPreferencia2() {
    return mantenerPreferencia2;
  }


  /**
   * Sets the mantener preferencia 2.
   *
   * @param mantenerPreferencia2 the new mantener preferencia 2
   */
  public void setMantenerPreferencia2(final Boolean mantenerPreferencia2) {
    this.mantenerPreferencia2 = mantenerPreferencia2;
  }


  /**
   * Gets the centro trabajador social.
   *
   * @return the centro trabajador social
   */
  public String getCentroTrabajadorSocial() {
    return centroTrabajadorSocial;
  }


  /**
   * Sets the centro trabajador social.
   *
   * @param centroTrabajadorSocial the new centro trabajador social
   */
  public void setCentroTrabajadorSocial(final String centroTrabajadorSocial) {
    this.centroTrabajadorSocial = centroTrabajadorSocial;
  }


  /**
   * Gets the ayuda comunicacion.
   *
   * @return the ayuda comunicacion
   */
  public Boolean getAyudaComunicacion() {
    return ayudaComunicacion;
  }


  /**
   * Sets the ayuda comunicacion.
   *
   * @param ayudaComunicacion the new ayuda comunicacion
   */
  public void setAyudaComunicacion(final Boolean ayudaComunicacion) {
    this.ayudaComunicacion = ayudaComunicacion;
  }


  /**
   * Gets the ayuda desplazamiento.
   *
   * @return the ayuda desplazamiento
   */
  public Boolean getAyudaDesplazamiento() {
    return ayudaDesplazamiento;
  }


  /**
   * Sets the ayuda desplazamiento.
   *
   * @param ayudaDesplazamiento the new ayuda desplazamiento
   */
  public void setAyudaDesplazamiento(final Boolean ayudaDesplazamiento) {
    this.ayudaDesplazamiento = ayudaDesplazamiento;
  }


  /**
   * Gets the ayuda acceso recursos.
   *
   * @return the ayuda acceso recursos
   */
  public Boolean getAyudaAccesoRecursos() {
    return ayudaAccesoRecursos;
  }


  /**
   * Sets the ayuda acceso recursos.
   *
   * @param ayudaAccesoRecursos the new ayuda acceso recursos
   */
  public void setAyudaAccesoRecursos(final Boolean ayudaAccesoRecursos) {
    this.ayudaAccesoRecursos = ayudaAccesoRecursos;
  }


  /**
   * Gets the realiza formacion.
   *
   * @return the realiza formacion
   */
  public Boolean getRealizaFormacion() {
    return realizaFormacion;
  }


  /**
   * Sets the realiza formacion.
   *
   * @param realizaFormacion the new realiza formacion
   */
  public void setRealizaFormacion(final Boolean realizaFormacion) {
    this.realizaFormacion = realizaFormacion;
  }


  /**
   * Gets the nombre formacion.
   *
   * @return the nombre formacion
   */
  public String getNombreFormacion() {
    return nombreFormacion;
  }


  /**
   * Sets the nombre formacion.
   *
   * @param nombreFormacion the new nombre formacion
   */
  public void setNombreFormacion(final String nombreFormacion) {
    this.nombreFormacion = nombreFormacion;
  }


  /**
   * Gets the formacion ayuda AP.
   *
   * @return the formacion ayuda AP
   */
  public Boolean getFormacionAyudaAP() {
    return formacionAyudaAP;
  }


  /**
   * Sets the formacion ayuda AP.
   *
   * @param formacionAyudaAP the new formacion ayuda AP
   */
  public void setFormacionAyudaAP(final Boolean formacionAyudaAP) {
    this.formacionAyudaAP = formacionAyudaAP;
  }


  /**
   * Gets the realiza actividad.
   *
   * @return the realiza actividad
   */
  public Boolean getRealizaActividad() {
    return realizaActividad;
  }


  /**
   * Sets the realiza actividad.
   *
   * @param realizaActividad the new realiza actividad
   */
  public void setRealizaActividad(final Boolean realizaActividad) {
    this.realizaActividad = realizaActividad;
  }


  /**
   * Gets the tipo actividad.
   *
   * @return the tipo actividad
   */
  public String getTipoActividad() {
    return tipoActividad;
  }


  /**
   * Sets the tipo actividad.
   *
   * @param tipoActividad the new tipo actividad
   */
  public void setTipoActividad(final String tipoActividad) {
    this.tipoActividad = tipoActividad;
  }


  /**
   * Gets the nombre entidad actividad.
   *
   * @return the nombre entidad actividad
   */
  public String getNombreEntidadActividad() {
    return nombreEntidadActividad;
  }


  /**
   * Sets the nombre entidad actividad.
   *
   * @param nombreEntidadActividad the new nombre entidad actividad
   */
  public void setNombreEntidadActividad(final String nombreEntidadActividad) {
    this.nombreEntidadActividad = nombreEntidadActividad;
  }


  /**
   * Gets the actividad ayuda AP.
   *
   * @return the actividad ayuda AP
   */
  public Boolean getActividadAyudaAP() {
    return actividadAyudaAP;
  }


  /**
   * Sets the actividad ayuda AP.
   *
   * @param actividadAyudaAP the new actividad ayuda AP
   */
  public void setActividadAyudaAP(final Boolean actividadAyudaAP) {
    this.actividadAyudaAP = actividadAyudaAP;
  }


  /**
   * Gets the proyecto adecuado.
   *
   * @return the proyecto adecuado
   */
  public Boolean getProyectoAdecuado() {
    return proyectoAdecuado;
  }


  /**
   * Sets the proyecto adecuado.
   *
   * @param proyectoAdecuado the new proyecto adecuado
   */
  public void setProyectoAdecuado(final Boolean proyectoAdecuado) {
    this.proyectoAdecuado = proyectoAdecuado;
  }


  /**
   * Gets the otro recurso.
   *
   * @return the otro recurso
   */
  public Boolean getOtroRecurso() {
    return otroRecurso;
  }


  /**
   * Sets the otro recurso.
   *
   * @param otroRecurso the new otro recurso
   */
  public void setOtroRecurso(final Boolean otroRecurso) {
    this.otroRecurso = otroRecurso;
  }

  /**
   * Gets the otro recurso seleccionado.
   *
   * @return the otro recurso seleccionado
   */
  public CatalogoServicio getOtroRecursoSeleccionado() {
    return otroRecursoSeleccionado;
  }


  /**
   * Sets the otro recurso seleccionado.
   *
   * @param otroRecursoSeleccionado the new otro recurso seleccionado
   */
  public void setOtroRecursoSeleccionado(
      final CatalogoServicio otroRecursoSeleccionado) {
    this.otroRecursoSeleccionado = otroRecursoSeleccionado;
  }


  /**
   * Gets the otro recurso motivos.
   *
   * @return the otro recurso motivos
   */
  public String getOtroRecursoMotivos() {
    return otroRecursoMotivos;
  }


  /**
   * Sets the otro recurso motivos.
   *
   * @param otroRecursoMotivos the new otro recurso motivos
   */
  public void setOtroRecursoMotivos(final String otroRecursoMotivos) {
    this.otroRecursoMotivos = otroRecursoMotivos;
  }


  /**
   * Gets the tipo persona AP fisica.
   *
   * @return the tipo persona AP fisica
   */
  public Boolean getTipoPersonaAPFisica() {
    return tipoPersonaAPFisica;
  }


  /**
   * Sets the tipo persona AP fisica.
   *
   * @param tipoPersonaAPFisica the new tipo persona AP fisica
   */
  public void setTipoPersonaAPFisica(final Boolean tipoPersonaAPFisica) {
    this.tipoPersonaAPFisica = tipoPersonaAPFisica;
  }


  /**
   * Gets the tipo persona AP juridica.
   *
   * @return the tipo persona AP juridica
   */
  public Boolean getTipoPersonaAPJuridica() {
    return tipoPersonaAPJuridica;
  }


  /**
   * Sets the tipo persona AP juridica.
   *
   * @param tipoPersonaAPJuridica the new tipo persona AP juridica
   */
  public void setTipoPersonaAPJuridica(final Boolean tipoPersonaAPJuridica) {
    this.tipoPersonaAPJuridica = tipoPersonaAPJuridica;
  }


  /**
   * Gets the horario lectivo AP.
   *
   * @return the horario lectivo AP
   */
  public Boolean getHorarioLectivoAP() {
    return horarioLectivoAP;
  }


  /**
   * Sets the horario lectivo AP.
   *
   * @param horarioLectivoAP the new horario lectivo AP
   */
  public void setHorarioLectivoAP(final Boolean horarioLectivoAP) {
    this.horarioLectivoAP = horarioLectivoAP;
  }


  /**
   * Gets the trabajo ayuda AP.
   *
   * @return the trabajo ayuda AP
   */
  public Boolean getTrabajoAyudaAP() {
    return trabajoAyudaAP;
  }


  /**
   * Sets the trabajo ayuda AP.
   *
   * @param trabajoAyudaAP the new trabajo ayuda AP
   */
  public void setTrabajoAyudaAP(final Boolean trabajoAyudaAP) {
    this.trabajoAyudaAP = trabajoAyudaAP;
  }


  /**
   * Gets the mantener teleasistencia.
   *
   * @return the mantener teleasistencia
   */
  public Boolean getMantenerTeleasistencia() {
    return mantenerTeleasistencia;
  }


  /**
   * Sets the mantener teleasistencia.
   *
   * @param mantenerTeleasistencia the new mantener teleasistencia
   */
  public void setMantenerTeleasistencia(final Boolean mantenerTeleasistencia) {
    this.mantenerTeleasistencia = mantenerTeleasistencia;
  }


  /**
   * Gets the tipo asispers.
   *
   * @return the tipo asispers
   */
  public Boolean getTipoAsispers() {
    return tipoAsispers;
  }


  /**
   * Sets the tipo asispers.
   *
   * @param tipoAsispers the new tipo asispers
   */
  public void setTipoAsispers(final Boolean tipoAsispers) {
    this.tipoAsispers = tipoAsispers;
  }


  /**
   * Gets the fecha grabacion.
   *
   * @return the fecha grabacion
   */
  public Date getFechaGrabacion() {
    return fechaGrabacion;
  }


  /**
   * Sets the fecha grabacion.
   *
   * @param fechaGrabacion the new fecha grabacion
   */
  public void setFechaGrabacion(final Date fechaGrabacion) {
    this.fechaGrabacion = fechaGrabacion;
  }

  /**
   * Gets the aumento horas.
   *
   * @return the aumento horas
   */
  public Boolean getAumentoHoras() {
    return aumentoHoras;
  }


  /**
   * Sets the aumento horas.
   *
   * @param aumentoHoras the new aumento horas
   */
  public void setAumentoHoras(final Boolean aumentoHoras) {
    this.aumentoHoras = aumentoHoras;
  }


}
