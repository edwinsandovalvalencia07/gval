package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The Class TipoPerfil.
 */
@Entity
@Table(name = "SDX_TIPOPERF_CLAU")
@GenericGenerator(name = "SDX_TIPOPERF_PKTIPOPERF_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDX_TIPOPERF_CLAU"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class TipoPerfil implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -3185084499670422284L;

  /** The pk tipoperf. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDX_TIPOPERF_PKTIPOPERF_GENERATOR")
  @Column(name = "PK_TIPOPERF", unique = true, nullable = false)
  private Long pkTipoperf;

  /** The codigo. */
  @Column(name = "TICODIGO", nullable = false, length = 2)
  private String codigo;

  /** The descripcion. */
  @Column(name = "TIDESC", length = 100)
  private String descripcion;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "TIFECHCREA", nullable = false)
  private Date fechaCreacion;


  /** The activo. */
  @Column(name = "TIPACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /** The grupo clau. */
  @Column(name = "TIGRUPO_CLAU")
  private String grupoClau;

  /** The valorador. */
  @Column(name = "TIVALORADOR")
  private Boolean valorador;

  /** The social. */
  @Column(name = "TISOCIAL")
  private Boolean social;

  /**
   * Instantiates a new tipo perfil.
   */
  public TipoPerfil() {
    // Constructor
  }

  /**
   * Instantiates a new tipo perfil.
   *
   * @param pkTipoperf the pk tipoperf
   * @param codigo the codigo
   * @param descripcion the descripcion
   * @param fechaCreacion the fecha creacion
   * @param activo the activo
   * @param grupoClau the grupo clau
   * @param valorador the valorador
   * @param social the social
   */
  public TipoPerfil(final Long pkTipoperf, final String codigo,
      final String descripcion, final Date fechaCreacion, final Boolean activo,
      final String grupoClau, final Boolean valorador, final Boolean social) {
    super();
    this.pkTipoperf = pkTipoperf;
    this.codigo = codigo;
    this.descripcion = descripcion;
    this.fechaCreacion = fechaCreacion;
    this.activo = activo;
    this.grupoClau = grupoClau;
    this.valorador = valorador;
    this.social = social;
  }



  /**
   * Gets the pk tipoperf.
   *
   * @return the pk tipoperf
   */
  public Long getPkTipoperf() {
    return pkTipoperf;
  }

  /**
   * Sets the pk tipoperf.
   *
   * @param pkTipoperf the new pk tipoperf
   */
  public void setPkTipoperf(final Long pkTipoperf) {
    this.pkTipoperf = pkTipoperf;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return (fechaCreacion == null) ? null : (Date) fechaCreacion.clone();
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion =
        (fechaCreacion == null) ? null : (Date) fechaCreacion.clone();
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the grupo clau.
   *
   * @return the grupo clau
   */
  public String getGrupoClau() {
    return grupoClau;
  }

  /**
   * Sets the grupo clau.
   *
   * @param grupoClau the new grupo clau
   */
  public void setGrupoClau(final String grupoClau) {
    this.grupoClau = grupoClau;
  }

  /**
   * Gets the prioridad.
   *
   * @return the prioridad
   */
  public Boolean getValorador() {
    return valorador;
  }

  /**
   * Sets the prioridad.
   *
   * @param valorador the new prioridad
   */
  public void setValorador(final Boolean valorador) {
    this.valorador = valorador;
  }

  /**
   * Gets the prioridad.
   *
   * @return the prioridad
   */
  public Boolean getSocial() {
    return social;
  }

  /**
   * Sets the prioridad.
   *
   * @param social the new prioridad
   */
  public void setSocial(final Boolean social) {
    this.social = social;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
