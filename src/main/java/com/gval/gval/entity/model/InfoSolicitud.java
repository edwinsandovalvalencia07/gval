package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import jakarta.persistence.*;


/**
 * The persistent class for the SDM_INFOSOLI database table.
 *
 */
@Entity
@Table(name = "SDM_INFOSOLI")
@GenericGenerator(name = "SDM_INFOSOLI_PKINFOSOLI_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_INFOSOLI"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class InfoSolicitud implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -7537159029546286364L;

  /** The pk infosoli. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_INFOSOLI_PKINFOSOLI_GENERATOR")
  @Column(name = "PK_INFOSOLI", unique = true, nullable = false)
  private Long pkInfosoli;

  /** The activo. */
  @Column(name = "INACTIVO", nullable = false, length = 1)
  private Boolean activo;

  /** The anyo gran invalidez. */
  @Column(name = "INANOGINVA", precision = 4)
  private Long anyoGranInvalidez;

  /** The anyo grado minusvalia. */
  @Column(name = "INANOMINU", precision = 4)
  private Long anyoGradoMinusvalia;

  /** The anyo reconocimiento ATP. */
  @Column(name = "INANOREATP", precision = 4)
  private Long anyoReconocimientoATP;

  /** The anyo solicitud anterior. */
  @Column(name = "INANOSOLI", precision = 4)
  private Long anyoSolicitudAnterior;

  /** The fecha caducidad minusvalia. */
  @Temporal(TemporalType.DATE)
  @Column(name = "INCADUMINU")
  private Date fechaCaducidadMinusvalia;

  /** The fecha caducidad ATP. */
  @Temporal(TemporalType.DATE)
  @Column(name = "INCADURATP")
  private Date fechaCaducidadATP;

  /** The caracter plaza. */
  @Column(name = "INCARAPLAZ", length = 1)
  private String caracterPlaza;

  /** The indica gran dependencia. */
  @Column(name = "INDATOSALU", precision = 8, scale = 2)
  private Long indicaGranDependencia;

  /** The cuantia anual. */
  @Column(name = "INDECUANTI", precision = 38)
  private Long cuantiaAnual;

  /** The declaracion impuesto. */
  @Column(name = "INDEDECLAR", precision = 38)
  private Long declaracionImpuesto;

  /** The nombre perceptor. */
  @Column(name = "INDENOMBRE", length = 50)
  private String nombrePerceptor;

  /** The documento identificador. */
  @Column(name = "INDENUDOID", length = 15)
  private String documentoIdentificador;

  /** The primer apelido perceptor. */
  @Column(name = "INDEPRAPEL", length = 50)
  private String primerApelidoPerceptor;

  /** The desea permanecer centro. */
  @Column(name = "INDESCONCE", length = 1)
  private Boolean deseaPermanecerCentro;

  /** The segundo apelido perceptor. */
  @Column(name = "INDESEAPEL", length = 50)
  private String segundoApelidoPerceptor;

  /** The emigrante retornado. */
  @Column(name = "INDREMRE", length = 1)
  private Boolean emigranteRetornado;

  /** The fecha retorno emigrante. */
  @Temporal(TemporalType.DATE)
  @Column(name = "INDRFERE")
  private Date fechaRetornoEmigrante;

  /** The residente legal. */
  @Column(name = "INDRRELE", length = 1)
  private Boolean residenteLegal;

  /** The residente legal 2 anyos. */
  @Column(name = "INDRRL2", length = 1)
  private Boolean residenteLegal2anyos;

  /** The residente legal 5 anyos. */
  @Column(name = "INDRRL5", length = 1)
  private Boolean residenteLegal5anyos;

  /** The entidad publica. */
  @Column(name = "INENTIDAD", length = 38)
  private Long entidadPublica;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "INFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The tiene gran invalidez. */
  @Column(name = "INGRANINVA", length = 1)
  private Boolean tieneGranInvalidez;

  /** The tiene minusvalia. */
  @Column(name = "INMINUSVA", length = 1)
  private Boolean tieneMinusvalia;

  /** The puntuacion adicional. */
  @Column(name = "INMOREPUNT", length = 4)
  private Long puntuacionAdicional;

  /** The movilidad reducida. */
  @Column(name = "INMOVIREDU", length = 4)
  private Long movilidadReducida;

  /** The reconocimiento ATP. */
  @Column(name = "INRECONATP", length = 1)
  private Boolean reconocimientoATP;

  /** The admun. */
  @Column(name = "INSADMUN", length = 38)
  private Long admun;

  /** The porcentaje minusvalia. */
  @Column(name = "INSDPORCEN")
  private Long porcentajeMinusvalia;

  /** The puntuacion ATP. */
  @Column(name = "INSDPUNT", precision = 38)
  private Long puntuacionATP;

  /** The solicitado anteriormente. */
  @Column(name = "INSOLIANTE", length = 1)
  private Boolean solicitadoAnteriormente;

  /** The tele publico. */
  @Column(name = "INTELEPUB", length = 38)
  private Long telePublico;

  /** The prestacion recibida. */
  @Column(name = "INTIPOPREST", length = 250)
  private String prestacionRecibida;

  /** The tipo residencia. */
  @Column(name = "INTIPORESI", length = 1)
  private String tipoResidencia;

  /** The cata serv. */
  @Column(name = "PK_CATASERV")
  private Long cataServ;

  /** The solicitud. */
  // bi-directional one-to-one association to SdmSolicit
  @ManyToOne(cascade = CascadeType.MERGE)
  @JoinColumn(name = "PK_SOLICIT", nullable = false)
  private Solicitud solicitud;

  /** The provincia situacion dependencia. */
  // bi-directional many-to-one association to SdxProvinci
  @ManyToOne
  @JoinColumn(name = "PK_PROVINCI")
  private Provincia provinciaSituacionDependencia;

  /** The provincia minusvalia. */
  // bi-directional many-to-one association to SdxProvinci
  @ManyToOne
  @JoinColumn(name = "PK_PROVINCI2")
  private Provincia provinciaMinusvalia;

  /** The provincia asistencia tercera persona. */
  // bi-directional many-to-one association to SdxProvinci
  @ManyToOne
  @JoinColumn(name = "PK_PROVINCI3")
  private Provincia provinciaAsistenciaTerceraPersona;

  /** The provincia gran invalidez. */
  // bi-directional many-to-one association to SdxProvinci
  @ManyToOne
  @JoinColumn(name = "PK_PROVINCI4")
  private Provincia provinciaGranInvalidez;

  /** The provincia emigrante retornado. */
  // bi-directional many-to-one association to SdxProvinci
  @ManyToOne
  @JoinColumn(name = "PK_PROVINCI5")
  private Provincia provinciaEmigranteRetornado;

  /** The cuantia. */
  @Column(name = "INCUANTIA")
  private Double cuantiaPension;

  /** The paises pension. */
  @Column(name = "INPAISES")
  private String paisesPension;

  /**
   * Instantiates a new info solicitud.
   */
  public InfoSolicitud() {
    // Constructor
  }

  /**
   * Gets the pk infosoli.
   *
   * @return the pk infosoli
   */
  public Long getPkInfosoli() {
    return pkInfosoli;
  }

  /**
   * Sets the pk infosoli.
   *
   * @param pkInfosoli the new pk infosoli
   */
  public void setPkInfosoli(final Long pkInfosoli) {
    this.pkInfosoli = pkInfosoli;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the anyo gran invalidez.
   *
   * @return the anyo gran invalidez
   */
  public Long getAnyoGranInvalidez() {
    return anyoGranInvalidez;
  }

  /**
   * Sets the anyo gran invalidez.
   *
   * @param anyoGranInvalidez the new anyo gran invalidez
   */
  public void setAnyoGranInvalidez(final Long anyoGranInvalidez) {
    this.anyoGranInvalidez = anyoGranInvalidez;
  }

  /**
   * Gets the anyo grado minusvalia.
   *
   * @return the anyo grado minusvalia
   */
  public Long getAnyoGradoMinusvalia() {
    return anyoGradoMinusvalia;
  }

  /**
   * Sets the anyo grado minusvalia.
   *
   * @param anyoGradoMinusvalia the new anyo grado minusvalia
   */
  public void setAnyoGradoMinusvalia(final Long anyoGradoMinusvalia) {
    this.anyoGradoMinusvalia = anyoGradoMinusvalia;
  }

  /**
   * Gets the anyo reconocimiento ATP.
   *
   * @return the anyo reconocimiento ATP
   */
  public Long getAnyoReconocimientoATP() {
    return anyoReconocimientoATP;
  }

  /**
   * Sets the anyo reconocimiento ATP.
   *
   * @param anyoReconocimientoATP the new anyo reconocimiento ATP
   */
  public void setAnyoReconocimientoATP(final Long anyoReconocimientoATP) {
    this.anyoReconocimientoATP = anyoReconocimientoATP;
  }

  /**
   * Gets the anyo solicitud anterior.
   *
   * @return the anyo solicitud anterior
   */
  public Long getAnyoSolicitudAnterior() {
    return anyoSolicitudAnterior;
  }

  /**
   * Sets the anyo solicitud anterior.
   *
   * @param anyoSolicitudAnterior the new anyo solicitud anterior
   */
  public void setAnyoSolicitudAnterior(final Long anyoSolicitudAnterior) {
    this.anyoSolicitudAnterior = anyoSolicitudAnterior;
  }

  /**
   * Gets the fecha caducidad minusvalia.
   *
   * @return the fecha caducidad minusvalia
   */
  public Date getFechaCaducidadMinusvalia() {
    return UtilidadesCommons.cloneDate(fechaCaducidadMinusvalia);
  }

  /**
   * Sets the fecha caducidad minusvalia.
   *
   * @param fechaCaducidadMinusvalia the new fecha caducidad minusvalia
   */
  public void setFechaCaducidadMinusvalia(final Date fechaCaducidadMinusvalia) {
    this.fechaCaducidadMinusvalia =
        UtilidadesCommons.cloneDate(fechaCaducidadMinusvalia);
  }

  /**
   * Gets the fecha caducidad ATP.
   *
   * @return the fecha caducidad ATP
   */
  public Date getFechaCaducidadATP() {
    return UtilidadesCommons.cloneDate(fechaCaducidadATP);
  }

  /**
   * Sets the fecha caducidad ATP.
   *
   * @param fechaCaducidadATP the new fecha caducidad ATP
   */
  public void setFechaCaducidadATP(final Date fechaCaducidadATP) {
    this.fechaCaducidadATP = UtilidadesCommons.cloneDate(fechaCaducidadATP);
  }

  /**
   * Gets the desea permanecer centro.
   *
   * @return the desea permanecer centro
   */
  public Boolean getDeseaPermanecerCentro() {
    return deseaPermanecerCentro;
  }

  /**
   * Sets the desea permanecer centro.
   *
   * @param deseaPermanecerCentro the new desea permanecer centro
   */
  public void setDeseaPermanecerCentro(final Boolean deseaPermanecerCentro) {
    this.deseaPermanecerCentro = deseaPermanecerCentro;
  }

  /**
   * Gets the emigrante retornado.
   *
   * @return the emigrante retornado
   */
  public Boolean getEmigranteRetornado() {
    return emigranteRetornado;
  }

  /**
   * Sets the emigrante retornado.
   *
   * @param emigranteRetornado the new emigrante retornado
   */
  public void setEmigranteRetornado(final Boolean emigranteRetornado) {
    this.emigranteRetornado = emigranteRetornado;
  }

  /**
   * Gets the fecha retorno emigrante.
   *
   * @return the fecha retorno emigrante
   */
  public Date getFechaRetornoEmigrante() {
    return UtilidadesCommons.cloneDate(fechaRetornoEmigrante);
  }

  /**
   * Sets the fecha retorno emigrante.
   *
   * @param fechaRetornoEmigrante the new fecha retorno emigrante
   */
  public void setFechaRetornoEmigrante(final Date fechaRetornoEmigrante) {
    this.fechaRetornoEmigrante =
        UtilidadesCommons.cloneDate(fechaRetornoEmigrante);
  }

  /**
   * Gets the residente legal.
   *
   * @return the residente legal
   */
  public Boolean getResidenteLegal() {
    return residenteLegal;
  }

  /**
   * Sets the residente legal.
   *
   * @param residenteLegal the new residente legal
   */
  public void setResidenteLegal(final Boolean residenteLegal) {
    this.residenteLegal = residenteLegal;
  }

  /**
   * Gets the residente legal 2 anyos.
   *
   * @return the residente legal 2 anyos
   */
  public Boolean getResidenteLegal2anyos() {
    return residenteLegal2anyos;
  }

  /**
   * Sets the residente legal 2 anyos.
   *
   * @param residenteLegal2anyos the new residente legal 2 anyos
   */
  public void setResidenteLegal2anyos(final Boolean residenteLegal2anyos) {
    this.residenteLegal2anyos = residenteLegal2anyos;
  }

  /**
   * Gets the residente legal 5 anyos.
   *
   * @return the residente legal 5 anyos
   */
  public Boolean getResidenteLegal5anyos() {
    return residenteLegal5anyos;
  }

  /**
   * Sets the residente legal 5 anyos.
   *
   * @param residenteLegal5anyos the new residente legal 5 anyos
   */
  public void setResidenteLegal5anyos(final Boolean residenteLegal5anyos) {
    this.residenteLegal5anyos = residenteLegal5anyos;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the tiene gran invalidez.
   *
   * @return the tiene gran invalidez
   */
  public Boolean getTieneGranInvalidez() {
    return tieneGranInvalidez;
  }

  /**
   * Sets the tiene gran invalidez.
   *
   * @param tieneGranInvalidez the new tiene gran invalidez
   */
  public void setTieneGranInvalidez(final Boolean tieneGranInvalidez) {
    this.tieneGranInvalidez = tieneGranInvalidez;
  }

  /**
   * Gets the tiene minusvalia.
   *
   * @return the tiene minusvalia
   */
  public Boolean getTieneMinusvalia() {
    return tieneMinusvalia;
  }

  /**
   * Sets the tiene minusvalia.
   *
   * @param tieneMinusvalia the new tiene minusvalia
   */
  public void setTieneMinusvalia(final Boolean tieneMinusvalia) {
    this.tieneMinusvalia = tieneMinusvalia;
  }

  /**
   * Gets the reconocimiento ATP.
   *
   * @return the reconocimiento ATP
   */
  public Boolean getReconocimientoATP() {
    return reconocimientoATP;
  }

  /**
   * Sets the reconocimiento ATP.
   *
   * @param reconocimientoATP the new reconocimiento ATP
   */
  public void setReconocimientoATP(final Boolean reconocimientoATP) {
    this.reconocimientoATP = reconocimientoATP;
  }

  /**
   * Gets the porcentaje minusvalia.
   *
   * @return the porcentaje minusvalia
   */
  public Long getPorcentajeMinusvalia() {
    return porcentajeMinusvalia;
  }

  /**
   * Sets the porcentaje minusvalia.
   *
   * @param porcentajeMinusvalia the new porcentaje minusvalia
   */
  public void setPorcentajeMinusvalia(final Long porcentajeMinusvalia) {
    this.porcentajeMinusvalia = porcentajeMinusvalia;
  }

  /**
   * Gets the puntuacion ATP.
   *
   * @return the puntuacion ATP
   */
  public Long getPuntuacionATP() {
    return puntuacionATP;
  }

  /**
   * Sets the puntuacion ATP.
   *
   * @param puntuacionATP the new puntuacion ATP
   */
  public void setPuntuacionATP(final Long puntuacionATP) {
    this.puntuacionATP = puntuacionATP;
  }

  /**
   * Gets the solicitado anteriormente.
   *
   * @return the solicitado anteriormente
   */
  public Boolean getSolicitadoAnteriormente() {
    return solicitadoAnteriormente;
  }

  /**
   * Sets the solicitado anteriormente.
   *
   * @param solicitadoAnteriormente the new solicitado anteriormente
   */
  public void setSolicitadoAnteriormente(
      final Boolean solicitadoAnteriormente) {
    this.solicitadoAnteriormente = solicitadoAnteriormente;
  }

  /**
   * Gets the tipo residencia.
   *
   * @return the tipo residencia
   */
  public String getTipoResidencia() {
    return tipoResidencia;
  }

  /**
   * Sets the tipo residencia.
   *
   * @param tipoResidencia the new tipo residencia
   */
  public void setTipoResidencia(final String tipoResidencia) {
    this.tipoResidencia = tipoResidencia;
  }

  /**
   * Gets the caracter plaza.
   *
   * @return the caracter plaza
   */
  public String getCaracterPlaza() {
    return caracterPlaza;
  }

  /**
   * Sets the caracter plaza.
   *
   * @param caracterPlaza the new caracter plaza
   */
  public void setCaracterPlaza(final String caracterPlaza) {
    this.caracterPlaza = caracterPlaza;
  }

  /**
   * Gets the indica gran dependencia.
   *
   * @return the indica gran dependencia
   */
  public Long getIndicaGranDependencia() {
    return indicaGranDependencia;
  }

  /**
   * Sets the indica gran dependencia.
   *
   * @param indicaGranDependencia the new indica gran dependencia
   */
  public void setIndicaGranDependencia(final Long indicaGranDependencia) {
    this.indicaGranDependencia = indicaGranDependencia;
  }

  /**
   * Gets the cuantia anual.
   *
   * @return the cuantia anual
   */
  public Long getCuantiaAnual() {
    return cuantiaAnual;
  }

  /**
   * Sets the cuantia anual.
   *
   * @param cuantiaAnual the new cuantia anual
   */
  public void setCuantiaAnual(final Long cuantiaAnual) {
    this.cuantiaAnual = cuantiaAnual;
  }

  /**
   * Gets the declaracion impuesto.
   *
   * @return the declaracion impuesto
   */
  public Long getDeclaracionImpuesto() {
    return declaracionImpuesto;
  }

  /**
   * Sets the declaracion impuesto.
   *
   * @param declaracionImpuesto the new declaracion impuesto
   */
  public void setDeclaracionImpuesto(final Long declaracionImpuesto) {
    this.declaracionImpuesto = declaracionImpuesto;
  }

  /**
   * Gets the nombre perceptor.
   *
   * @return the nombre perceptor
   */
  public String getNombrePerceptor() {
    return nombrePerceptor;
  }

  /**
   * Sets the nombre perceptor.
   *
   * @param nombrePerceptor the new nombre perceptor
   */
  public void setNombrePerceptor(final String nombrePerceptor) {
    this.nombrePerceptor = nombrePerceptor;
  }

  /**
   * Gets the documento identificador.
   *
   * @return the documento identificador
   */
  public String getDocumentoIdentificador() {
    return documentoIdentificador;
  }

  /**
   * Sets the documento identificador.
   *
   * @param documentoIdentificador the new documento identificador
   */
  public void setDocumentoIdentificador(final String documentoIdentificador) {
    this.documentoIdentificador = documentoIdentificador;
  }

  /**
   * Gets the primer apelido perceptor.
   *
   * @return the primer apelido perceptor
   */
  public String getPrimerApelidoPerceptor() {
    return primerApelidoPerceptor;
  }

  /**
   * Sets the primer apelido perceptor.
   *
   * @param primerApelidoPerceptor the new primer apelido perceptor
   */
  public void setPrimerApelidoPerceptor(final String primerApelidoPerceptor) {
    this.primerApelidoPerceptor = primerApelidoPerceptor;
  }

  /**
   * Gets the segundo apelido perceptor.
   *
   * @return the segundo apelido perceptor
   */
  public String getSegundoApelidoPerceptor() {
    return segundoApelidoPerceptor;
  }

  /**
   * Sets the segundo apelido perceptor.
   *
   * @param segundoApelidoPerceptor the new segundo apelido perceptor
   */
  public void setSegundoApelidoPerceptor(final String segundoApelidoPerceptor) {
    this.segundoApelidoPerceptor = segundoApelidoPerceptor;
  }

  /**
   * Gets the entidad publica.
   *
   * @return the entidad publica
   */
  public Long getEntidadPublica() {
    return entidadPublica;
  }

  /**
   * Sets the entidad publica.
   *
   * @param entidadPublica the new entidad publica
   */
  public void setEntidadPublica(final Long entidadPublica) {
    this.entidadPublica = entidadPublica;
  }

  /**
   * Gets the puntuacion adicional.
   *
   * @return the puntuacion adicional
   */
  public Long getPuntuacionAdicional() {
    return puntuacionAdicional;
  }

  /**
   * Sets the puntuacion adicional.
   *
   * @param puntuacionAdicional the new puntuacion adicional
   */
  public void setPuntuacionAdicional(final Long puntuacionAdicional) {
    this.puntuacionAdicional = puntuacionAdicional;
  }

  /**
   * Gets the movilidad reducida.
   *
   * @return the movilidad reducida
   */
  public Long getMovilidadReducida() {
    return movilidadReducida;
  }

  /**
   * Sets the movilidad reducida.
   *
   * @param movilidadReducida the new movilidad reducida
   */
  public void setMovilidadReducida(final Long movilidadReducida) {
    this.movilidadReducida = movilidadReducida;
  }

  /**
   * Gets the admun.
   *
   * @return the admun
   */
  public Long getAdmun() {
    return admun;
  }

  /**
   * Sets the admun.
   *
   * @param admun the new admun
   */
  public void setAdmun(final Long admun) {
    this.admun = admun;
  }

  /**
   * Gets the tele publico.
   *
   * @return the tele publico
   */
  public Long getTelePublico() {
    return telePublico;
  }

  /**
   * Sets the tele publico.
   *
   * @param telePublico the new tele publico
   */
  public void setTelePublico(final Long telePublico) {
    this.telePublico = telePublico;
  }

  /**
   * Gets the prestacion recibida.
   *
   * @return the prestacion recibida
   */
  public String getPrestacionRecibida() {
    return prestacionRecibida;
  }

  /**
   * Sets the prestacion recibida.
   *
   * @param prestacionRecibida the new prestacion recibida
   */
  public void setPrestacionRecibida(final String prestacionRecibida) {
    this.prestacionRecibida = prestacionRecibida;
  }

  /**
   * Gets the cata serv.
   *
   * @return the cata serv
   */
  public Long getCataServ() {
    return cataServ;
  }

  /**
   * Sets the cata serv.
   *
   * @param cataServ the new cata serv
   */
  public void setCataServ(final Long cataServ) {
    this.cataServ = cataServ;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public Solicitud getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the new solicitud
   */
  public void setSolicitud(final Solicitud solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the provincia situacion dependencia.
   *
   * @return the provincia situacion dependencia
   */
  public Provincia getProvinciaSituacionDependencia() {
    return provinciaSituacionDependencia;
  }

  /**
   * Sets the provincia situacion dependencia.
   *
   * @param provinciaSituacionDependencia the new provincia situacion
   *        dependencia
   */
  public void setProvinciaSituacionDependencia(
      final Provincia provinciaSituacionDependencia) {
    this.provinciaSituacionDependencia = provinciaSituacionDependencia;
  }

  /**
   * Gets the provincia minusvalia.
   *
   * @return the provincia minusvalia
   */
  public Provincia getProvinciaMinusvalia() {
    return provinciaMinusvalia;
  }

  /**
   * Sets the provincia minusvalia.
   *
   * @param provinciaMinusvalia the new provincia minusvalia
   */
  public void setProvinciaMinusvalia(final Provincia provinciaMinusvalia) {
    this.provinciaMinusvalia = provinciaMinusvalia;
  }

  /**
   * Gets the provincia asistencia tercera persona.
   *
   * @return the provincia asistencia tercera persona
   */
  public Provincia getProvinciaAsistenciaTerceraPersona() {
    return provinciaAsistenciaTerceraPersona;
  }

  /**
   * Sets the provincia asistencia tercera persona.
   *
   * @param provinciaVidaDiaria the new provincia asistencia tercera persona
   */
  public void setProvinciaAsistenciaTerceraPersona(
      final Provincia provinciaVidaDiaria) {
    this.provinciaAsistenciaTerceraPersona = provinciaVidaDiaria;
  }

  /**
   * Gets the provincia gran invalidez.
   *
   * @return the provincia gran invalidez
   */
  public Provincia getProvinciaGranInvalidez() {
    return provinciaGranInvalidez;
  }

  /**
   * Sets the provincia gran invalidez.
   *
   * @param provinciaGranInvalidez the new provincia gran invalidez
   */
  public void setProvinciaGranInvalidez(
      final Provincia provinciaGranInvalidez) {
    this.provinciaGranInvalidez = provinciaGranInvalidez;
  }

  /**
   * Gets the provincia emigrante retornado.
   *
   * @return the provincia emigrante retornado
   */
  public Provincia getProvinciaEmigranteRetornado() {
    return provinciaEmigranteRetornado;
  }

  /**
   * Sets the provincia emigrante retornado.
   *
   * @param provinciaEmigranteRetornado the new provincia emigrante retornado
   */
  public void setProvinciaEmigranteRetornado(
      final Provincia provinciaEmigranteRetornado) {
    this.provinciaEmigranteRetornado = provinciaEmigranteRetornado;
  }


  /**
   * Gets the cuantia pension.
   *
   * @return the cuantia pension
   */
  public Double getCuantiaPension() {
    return cuantiaPension;
  }

  /**
   * Sets the cuantia pension.
   *
   * @param cuantiaPension the new cuantia pension
   */
  public void setCuantiaPension(final Double cuantiaPension) {
    this.cuantiaPension = cuantiaPension;
  }

  /**
   * Gets the paises pension.
   *
   * @return the paises pension
   */
  public String getPaisesPension() {
    return paisesPension;
  }

  /**
   * Sets the paises pension.
   *
   * @param paisesPension the new paises pension
   */
  public void setPaisesPension(final String paisesPension) {
    this.paisesPension = paisesPension;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
