package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;
import jakarta.persistence.*;


/**
 * The persistent class for the SDM_ESTSOL database table.
 *
 */
@Entity
@Table(name = "SDM_ESTSOL")
@GenericGenerator(name = "SDM_ESTSOL_PKESTSOL_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {@Parameter(name = "sequence_name", value = "SEC_SDM_ESTSOL"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class EstadoSolicitud implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk estsol. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_ESTSOL_PKESTSOL_GENERATOR")
  @Column(name = "PK_ESTSOL", unique = true, nullable = false)
  private Long pkEstsol;

  /** The activo. */
  @Column(name = "ESACTIVO", nullable = false)
  private Boolean activo;

  /** The fecha hasta. */
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "ESFECHASTA")
  private Date fechaHasta;

  /** The fecha creacion. */
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "ESFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The fecha desde. */
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "ESFECHDESD", nullable = false)
  private Date fechaDesde;

  /** The observacion. */
  @Column(name = "ESOBSERV", length = 1000)
  private String observacion;

  /**  The usuario asociated to EstadoSolicitud. */
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "PK_PERSONA", nullable = false)
  private Usuario usuario;

  /** The estado. */
  // bi-directional many-to-one association to Estado
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "PK_ESTADO", nullable = false)
  private Estado estado;

  /** The subestado. */
  // bi-directional many-to-one association to Subestado
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "PK_SUBESTADO_SOLICITUD")
  private Subestado subestado;

  /** The solicitud. */
  // bi-directional many-to-one association to SdmSolicit
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_SOLICIT", nullable = false)
  private Solicitud solicitud;

  /**
   * Instantiates a new estado solicitud.
   */
  public EstadoSolicitud() {
    // Constructor
  }

  /**
   * Gets the pk estsol.
   *
   * @return the pk estsol
   */
  public Long getPkEstsol() {
    return pkEstsol;
  }

  /**
   * Sets the pk estsol.
   *
   * @param pkEstsol the new pk estsol
   */
  public void setPkEstsol(final Long pkEstsol) {
    this.pkEstsol = pkEstsol;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha hasta.
   *
   * @return the fecha hasta
   */
  public Date getFechaHasta() {
    return UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Sets the fecha hasta.
   *
   * @param fechaHasta the new fecha hasta
   */
  public void setFechaHasta(final Date fechaHasta) {
    this.fechaHasta = UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha desde.
   *
   * @return the fecha desde
   */
  public Date getFechaDesde() {
    return UtilidadesCommons.cloneDate(fechaDesde);
  }

  /**
   * Sets the fecha desde.
   *
   * @param fechaDesde the new fecha desde
   */
  public void setFechaDesde(final Date fechaDesde) {
    this.fechaDesde = UtilidadesCommons.cloneDate(fechaDesde);
  }

  /**
   * Gets the observacion.
   *
   * @return the observacion
   */
  public String getObservacion() {
    return observacion;
  }

  /**
   * Sets the observacion.
   *
   * @param observacion the new observacion
   */
  public void setObservacion(final String observacion) {
    this.observacion = observacion;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public Solicitud getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the new solicitud
   */
  public void setSolicitud(final Solicitud solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the estado.
   *
   * @return the estado
   */
  public Estado getEstado() {
    return estado;
  }

  /**
   * Sets the estado.
   *
   * @param estado the new estado
   */
  public void setEstado(final Estado estado) {
    this.estado = estado;
  }

  /**
   * Gets the subestado.
   *
   * @return the subestado
   */
  public Subestado getSubestado() {
    return subestado;
  }

  /**
   * Sets the subestado.
   *
   * @param subestado the new subestado
   */
  public void setSubestado(final Subestado subestado) {
    this.subestado = subestado;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the usuario to set
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
