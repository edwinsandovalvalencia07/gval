package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;
import jakarta.persistence.*;


/**
 * The persistent class for the SDX_TIPOFAMI database table.
 *
 */
@Entity
@Table(name = "SDX_TIPOFAMI")
public class TipoFamiliar implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -6413116749235440965L;

  /** The pk tipofami. */
  @Id
  @Column(name = "PK_TIPOFAMI", unique = true, nullable = false)
  private Long pkTipofami;

  /** The activo. */
  @Column(name = "TIFACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "TIFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The imserso. */
  @Column(name = "TIIMSERSO", precision = 2)
  private Long imserso;

  /** The parentesco. */
  @Column(name = "TIPARENTES", nullable = false, length = 30)
  private String parentesco;

  /** The relacion familiar. */
  @Column(name = "TIRELFAM", precision = 1)
  private Long relacionFamiliar;

  /**
   * Instantiates a new tipo familiar.
   */
  public TipoFamiliar() {
    // Empty constructor
  }

  /**
   * Instantiates a new tipo familiar.
   *
   * @param pkTipofami the pk tipofami
   * @param activo the activo
   * @param fechaCreacion the fecha creacion
   * @param imserso the imserso
   * @param parentesco the parentesco
   * @param relacionFamiliar the relacion familiar
   */
  public TipoFamiliar(final Long pkTipofami, final Boolean activo,
      final Date fechaCreacion, final Long imserso, final String parentesco,
      final Long relacionFamiliar) {
    super();
    this.activo = activo;
    this.fechaCreacion = fechaCreacion;
    this.imserso = imserso;
    this.parentesco = parentesco;
    this.pkTipofami = pkTipofami;
    this.relacionFamiliar = relacionFamiliar;
  }

  /**
   * Gets the pk tipofami.
   *
   * @return the pk tipofami
   */
  public Long getPkTipofami() {
    return pkTipofami;
  }

  /**
   * Sets the pk tipofami.
   *
   * @param pkTipofami the new pk tipofami
   */
  public void setPkTipofami(final Long pkTipofami) {
    this.pkTipofami = pkTipofami;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return fechaCreacion;
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

  /**
   * Gets the imserso.
   *
   * @return the imserso
   */
  public Long getImserso() {
    return imserso;
  }

  /**
   * Sets the imserso.
   *
   * @param imserso the new imserso
   */
  public void setImserso(final Long imserso) {
    this.imserso = imserso;
  }

  /**
   * Gets the parentesco.
   *
   * @return the parentesco
   */
  public String getParentesco() {
    return parentesco;
  }

  /**
   * Sets the parentesco.
   *
   * @param parentesco the new parentesco
   */
  public void setParentesco(final String parentesco) {
    this.parentesco = parentesco;
  }

  /**
   * Gets the relacion familiar.
   *
   * @return the relacion familiar
   */
  public Long getRelacionFamiliar() {
    return relacionFamiliar;
  }

  /**
   * Sets the relacion familiar.
   *
   * @param relacionFamiliar the new relacion familiar
   */
  public void setRelacionFamiliar(final Long relacionFamiliar) {
    this.relacionFamiliar = relacionFamiliar;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
