package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

import jakarta.persistence.*;


/**
 * The persistent class for the SDX_ESTADO_DOCU database table.
 *
 */
@Entity
@Table(name = "SDX_ESTADO_DOCU")
public class EstadoDocumento implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 2000521426238573131L;

  /** The pk estado documento. */
  @Id
  @Column(name = "PK_ESTADO_DOCU", unique = true, nullable = false)
  private Long pkEstadoDocumento;

  /** The activo. */
  @Column(name = "ESACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /** The codigo. */
  @Column(name = "ESCODIGO", nullable = false, length = 3)
  private String codigo;

  /** The nombre. */
  @Column(name = "ESNOMBRE", nullable = false, length = 50)
  private String nombre;


  /**
   * Instantiates a new estado documento.
   */
  public EstadoDocumento() {
    // Empty constructor
  }


  /**
   * Gets the pk estado documento.
   *
   * @return the pk estado documento
   */
  public Long getPkEstadoDocumento() {
    return pkEstadoDocumento;
  }


  /**
   * Sets the pk estado documento.
   *
   * @param pkEstadoDocumento the new pk estado documento
   */
  public void setPkEstadoDocumento(final Long pkEstadoDocumento) {
    this.pkEstadoDocumento = pkEstadoDocumento;
  }


  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }


  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }


  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }


  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }


  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }


  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
