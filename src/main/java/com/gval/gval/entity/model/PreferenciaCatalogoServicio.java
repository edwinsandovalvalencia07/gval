package com.gval.gval.entity.model;


import com.gval.gval.entity.vistas.model.Centro;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;

import jakarta.persistence.*;

/**
 * The Class PrefCatalogoServicio.
 */
@Entity
@Table(name = "SDM_PREF_CATASERV")
@GenericGenerator(name = "SDM_PREF_CATASERV_PKPREF_CATASERV_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_PREF_CATASERV"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class PreferenciaCatalogoServicio implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 6339139670966464286L;

  /** The pk pref cataserv. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_PREF_CATASERV_PKPREF_CATASERV_GENERATOR")
  @Column(name = "PK_PREF_CATASERV", unique = true, nullable = false)
  private Long pkPrefCataserv;

  /** The orden. */
  @Column(name = "PCORDEN", precision = 1)
  private Long orden;

  /** The centro primero. */
  @ManyToOne
  @JoinColumn(name = "PCCOPRCENTRO")
  @NotFound(action = NotFoundAction.IGNORE)
  private Centro centroPrimero;

  /** The centro segundo. */
  @ManyToOne
  @JoinColumn(name = "PCCOSECENTRO")
  @NotFound(action = NotFoundAction.IGNORE)
  private Centro centroSegundo;

  /** The centro tercero. */
  @ManyToOne
  @JoinColumn(name = "PCCOTECENTRO")
  @NotFound(action = NotFoundAction.IGNORE)
  private Centro centroTercero;
  
  /** The provincia CV. */
  @ManyToOne
  @JoinColumn(name = "PCPROVINCIACV")
  @NotFound(action = NotFoundAction.IGNORE)
  private Provincia provincia;

  /** The satisfecho. */
  @Column(name = "PCSATISFECHO", precision = 1)
  private Boolean satisfecho;

  /** The denegado. */
  @Column(name = "PCDENEGADO", precision = 1)
  private Boolean denegado;

  /** The observaciones. */
  @Column(name = "PCOBSERVOS", length = 500)
  private String observaciones;

  /** The preferencia solicitud. */
  @ManyToOne
  @JoinColumn(name = "PK_PREFSOL", nullable = false)
  private Preferencia preferencia;

  /** The catalogo servicio. */
  // bi-directional many-to-one association to SdxCataserv
  @ManyToOne
  @JoinColumn(name = "PK_CATASERV", nullable = false)
  private CatalogoServicio catalogoServicio;

  /**
   * Instantiates a new pref catalogo servicio.
   */
  public PreferenciaCatalogoServicio() {
    super();
  }

  /**
   * Gets the pk pref cataserv.
   *
   * @return the pk pref cataserv
   */
  public Long getPkPrefCataserv() {
    return pkPrefCataserv;
  }

  /**
   * Gets the orden.
   *
   * @return the orden
   */
  public Long getOrden() {
    return orden;
  }

  /**
   * Gets the satisfecho.
   *
   * @return the satisfecho
   */
  public Boolean getSatisfecho() {
    return satisfecho;
  }

  /**
   * Gets the denegado.
   *
   * @return the denegado
   */
  public Boolean getDenegado() {
    return denegado;
  }

  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return observaciones;
  }

  /**
   * Gets the preferencia.
   *
   * @return the preferencia
   */
  public Preferencia getPreferencia() {
    return preferencia;
  }

  /**
   * Gets the catalogo servicio.
   *
   * @return the catalogo servicio
   */
  public CatalogoServicio getCatalogoServicio() {
    return catalogoServicio;
  }

  /**
   * Sets the pk pref cataserv.
   *
   * @param pkPrefCataserv the new pk pref cataserv
   */
  public void setPkPrefCataserv(final Long pkPrefCataserv) {
    this.pkPrefCataserv = pkPrefCataserv;
  }

  /**
   * Sets the orden.
   *
   * @param orden the new orden
   */
  public void setOrden(final Long orden) {
    this.orden = orden;
  }

  /**
   * Sets the satisfecho.
   *
   * @param satisfecho the new satisfecho
   */
  public void setSatisfecho(final Boolean satisfecho) {
    this.satisfecho = satisfecho;
  }

  /**
   * Sets the denegado.
   *
   * @param denegado the new denegado
   */
  public void setDenegado(final Boolean denegado) {
    this.denegado = denegado;
  }

  /**
   * Sets the observaciones.
   *
   * @param observaciones the new observaciones
   */
  public void setObservaciones(final String observaciones) {
    this.observaciones = observaciones;
  }

  /**
   * Sets the preferencia.
   *
   * @param preferencia the new preferencia
   */
  public void setPreferencia(final Preferencia preferencia) {
    this.preferencia = preferencia;
  }

  /**
   * Sets the catalogo servicio.
   *
   * @param catalogoServicio the new catalogo servicio
   */
  public void setCatalogoServicio(final CatalogoServicio catalogoServicio) {
    this.catalogoServicio = catalogoServicio;
  }

  /**
   * Gets the centro primero.
   *
   * @return the centro primero
   */
  public Centro getCentroPrimero() {
    return centroPrimero;
  }

  /**
   * Sets the centro primero.
   *
   * @param centroPrimero the new centro primero
   */
  public void setCentroPrimero(final Centro centroPrimero) {
    this.centroPrimero = centroPrimero;
  }

  /**
   * Gets the centro segundo.
   *
   * @return the centro segundo
   */
  public Centro getCentroSegundo() {
    return centroSegundo;
  }

  /**
   * Sets the centro segundo.
   *
   * @param centroSegundo the new centro segundo
   */
  public void setCentroSegundo(final Centro centroSegundo) {
    this.centroSegundo = centroSegundo;
  }

  /**
   * Gets the centro tercero.
   *
   * @return the centro tercero
   */
  public Centro getCentroTercero() {
    return centroTercero;
  }

  /**
   * Sets the centro tercero.
   *
   * @param centroTercero the new centro tercero
   */
  public void setCentroTercero(final Centro centroTercero) {
    this.centroTercero = centroTercero;
  }

  /**
   * Gets the provincia CV.
   *
   * @return the provincia CV
   */
  public Provincia getProvincia() {
    return provincia;
  }
  /**
   * Sets the provincia CV.
   *
   * @param provincia the new provincia CV
   */
  public void setProvincia(Provincia provincia) {
    this.provincia = provincia;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
