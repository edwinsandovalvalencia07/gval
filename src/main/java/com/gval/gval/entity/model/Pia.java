package com.gval.gval.entity.model;

import java.io.Serializable;

import jakarta.persistence.*;


// TODO: Auto-generated Javadoc
/**
 * The Class Pia.
 */
@Entity
@Table(name = "SDM_PIA")
public class Pia implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pkpia. */
  @Id
  @Column(name = "PK_PIA", unique = true, nullable = false)
  private Long pkpia;

  /** The pivalido. */
  @Column(name = "PIVALIDO")
  private Boolean pivalido;

  /** The pk resoluci 2. */
  @ManyToOne
  @JoinColumn(name = "PK_RESOLUCI2", nullable = false)
  private Resolucion resoluci2;

  /** The catalogo servicio. */
  @ManyToOne
  @JoinColumn(name = "PK_CATASERV")
  private CatalogoServicio catalogoServicio;

  /** The pia tipo. */
  @Column(name = "PITIPO", length = 1)
  private String piaTipo;

  /**
   * Gets the pivalido.
   *
   * @return the pivalido
   */
  public Boolean getPivalido() {
    return this.pivalido;
  }

  /**
   * Sets the pivalido.
   *
   * @param pivalido the new pivalido
   */
  public void setPivalido(final Boolean pivalido) {
    this.pivalido = pivalido;
  }

  /**
   * Gets the pkpia.
   *
   * @return the pkpia
   */
  public Long getPkpia() {
    return this.pkpia;
  }

  /**
   * Sets the pkpia.
   *
   * @param pkpia the new pkpia
   */
  public void setPkpia(final Long pkpia) {
    this.pkpia = pkpia;
  }

  /**
   * Gets the resoluci 2.
   *
   * @return the resoluci 2
   */
  public Resolucion getResoluci2() {
    return resoluci2;
  }

  /**
   * Sets the resoluci 2.
   *
   * @param resoluci2 the new resoluci 2
   */
  public void setResoluci2(final Resolucion resoluci2) {
    this.resoluci2 = resoluci2;
  }

  /**
   * Gets the catalogo servicio.
   *
   * @return the catalogo servicio
   */
  public CatalogoServicio getCatalogoServicio() {
    return catalogoServicio;
  }

  /**
   * Sets the catalogo servicio.
   *
   * @param catalogoServicio the new catalogo servicio
   */
  public void setCatalogoServicio(CatalogoServicio catalogoServicio) {
    this.catalogoServicio = catalogoServicio;
  }

  /**
   * Gets the pia tipo.
   *
   * @return the pia tipo
   */
  public String getPiaTipo() {
    return piaTipo;
  }

  /**
   * Sets the pia tipo.
   *
   * @param piaTipo the new pia tipo
   */
  public void setPiaTipo(String piaTipo) {
    this.piaTipo = piaTipo;
  }

}
