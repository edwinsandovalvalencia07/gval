package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;
import jakarta.persistence.*;


/**
 * The Class EstadoTramiteSolicitud.
 */
@Entity
@Table(name = "sdm_est_tramite_solicitud")
@GenericGenerator(name = "SDM_EST_TRAMITE_PK_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name",
            value = "SEC_SDM_EST_TRAMITE_SOLICITUD"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class EstadoTramiteSolicitud implements Serializable {
  /** The pk est tramite solicitud. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_EST_TRAMITE_PK_GENERATOR")
  @Column(name = "PK_EST_TRAMITE_SOLICITUD", unique = true, nullable = false,
      updatable = false)
  private Long pkEstTramiteSolicitud;

  /** The tramite solicitud. */
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "PK_TRAMITE_SOLICITUD",
      referencedColumnName = "pk_tramite_solicitud")
  private TramiteSolicitud tramiteSolicitud;

  /** The pk persona. */
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "pk_persona", referencedColumnName = "pk_persona")
  private Persona persona;

  /** The estado. */
  @Column(name = "estado")
  private String estado;

  /** The esfechdesde. */
  @Temporal(TemporalType.DATE)
  @Column(name = "esfechdesde", updatable = false)
  private Date fechaDesde;

  /** The esfechasta. */
  @Temporal(TemporalType.DATE)
  @Column(name = "esfechasta")
  private Date fechaHasta;

  /** The esactivo. */
  @Column(name = "esactivo")
  private int activo;

  /** The esfechcrea. */
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "esfechcrea", updatable = false)
  private Date fechaCreacion;

  /**
   * Gets the pk est tramite solicitud.
   *
   * @return the pk est tramite solicitud
   */
  public Long getPkEstTramiteSolicitud() {
    return pkEstTramiteSolicitud;
  }

  /**
   * Sets the pk est tramite solicitud.
   *
   * @param pkEstTramiteSolicitud the new pk est tramite solicitud
   */
  public void setPkEstTramiteSolicitud(final Long pkEstTramiteSolicitud) {
    this.pkEstTramiteSolicitud = pkEstTramiteSolicitud;
  }

  /**
   * Gets the pk tramite solicitud.
   *
   * @return the pk tramite solicitud
   */
  public TramiteSolicitud getTramiteSolicitud() {
    return tramiteSolicitud;
  }

  /**
   * Sets the pk tramite solicitud.
   *
   * @param tramiteSolicitud the new pk tramite solicitud
   */
  public void setTramiteSolicitud(final TramiteSolicitud tramiteSolicitud) {
    this.tramiteSolicitud = tramiteSolicitud;
  }

  /**
   * Gets the pk persona.
   *
   * @return the pk persona
   */
  public Persona getPersona() {
    return persona;
  }

  /**
   * Sets the pk persona.
   *
   * @param persona the new pk persona
   */
  public void setPersona(final Persona persona) {
    this.persona = persona;
  }

  /**
   * Gets the estado.
   *
   * @return the estado
   */
  public String getEstado() {
    return estado;
  }

  /**
   * Sets the estado.
   *
   * @param estado the new estado
   */
  public void setEstado(final String estado) {
    this.estado = estado;
  }

  /**
   * Gets the esfechdesde.
   *
   * @return the esfechdesde
   */
  public Date getFechaDesde() {
    return UtilidadesCommons.cloneDate(fechaDesde);
  }

  /**
   * Sets the esfechdesde.
   *
   * @param fechaDesde the new esfechdesde
   */
  public void setFechaDesde(final Date fechaDesde) {
    this.fechaDesde = UtilidadesCommons.cloneDate(fechaDesde);
  }

  /**
   * Gets the esfechasta.
   *
   * @return the esfechasta
   */
  public Date getFechaHasta() {
    return UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Sets the esfechasta.
   *
   * @param fechaHasta the new esfechasta
   */
  public void setFechaHasta(final Date fechaHasta) {
    this.fechaHasta = UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Gets the esactivo.
   *
   * @return the esactivo
   */
  public int getActivo() {
    return activo;
  }

  /**
   * Sets the esactivo.
   *
   * @param activo the new esactivo
   */
  public void setActivo(final int activo) {
    this.activo = activo;
  }

  /**
   * Gets the esfechcrea.
   *
   * @return the esfechcrea
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the esfechcrea.
   *
   * @param fechaCreacion the new esfechcrea
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }



}

