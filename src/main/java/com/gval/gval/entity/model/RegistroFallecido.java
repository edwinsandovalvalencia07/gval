package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;

/**
 * The Class RegistroFallecido.
 */
@Entity
@Table(name = "SDM_REGFALLEC")
@GenericGenerator(name = "SDM_REGFALLEC_PKREGFALLEC_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_REGFALLEC"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class RegistroFallecido implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;


  /** The pk registro fallecido. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_REGFALLEC_PKREGFALLEC_GENERATOR")
  @Column(name = "PK_REGFALLEC", unique = true, nullable = false)
  private Long pkRegistroFallecido;

  /** The expediente. */
  @ManyToOne
  @JoinColumn(name = "PK_EXPEDIEN", nullable = false)
  private Expediente expediente;

  /** The usuario. */
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA", nullable = false)
  private Usuario usuario;

  /** The fecha registro. */
  @Temporal(TemporalType.DATE)
  @Column(name = "REGFECHA")
  private Date fechaRegistro;

  /** The fecha fallecimiento. */
  @Temporal(TemporalType.DATE)
  @Column(name = "REGFECHFALLEC")
  private Date fechaFallecimiento;

  /** The indica fallecimiento. */
  @Column(name = "REGFALLEC")
  private Boolean indicaFallecimiento;

  /**
   * Instantiates a new registro fallecido.
   */
  public RegistroFallecido() {
    // Constructor vacio
  }

  /**
   * Gets the pk registro fallecido.
   *
   * @return the pk registro fallecido
   */
  public Long getPkRegistroFallecido() {
    return pkRegistroFallecido;
  }

  /**
   * Sets the pk registro fallecido.
   *
   * @param pkRegistroFallecido the new pk registro fallecido
   */
  public void setPkRegistroFallecido(final Long pkRegistroFallecido) {
    this.pkRegistroFallecido = pkRegistroFallecido;
  }

  /**
   * Gets the expediente.
   *
   * @return the expediente
   */
  public Expediente getExpediente() {
    return expediente;
  }

  /**
   * Sets the expediente.
   *
   * @param expediente the new expediente
   */
  public void setExpediente(final Expediente expediente) {
    this.expediente = expediente;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the fecha registro.
   *
   * @return the fecha registro
   */
  public Date getFechaRegistro() {
    return UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Sets the fecha registro.
   *
   * @param fechaRegistro the new fecha registro
   */
  public void setFechaRegistro(final Date fechaRegistro) {
    this.fechaRegistro = UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Gets the fecha fallecimiento.
   *
   * @return the fecha fallecimiento
   */
  public Date getFechaFallecimiento() {
    return UtilidadesCommons.cloneDate(fechaFallecimiento);
  }

  /**
   * Sets the fecha fallecimiento.
   *
   * @param fechaFallecimiento the new fecha fallecimiento
   */
  public void setFechaFallecimiento(final Date fechaFallecimiento) {
    this.fechaFallecimiento = UtilidadesCommons.cloneDate(fechaFallecimiento);
  }

  /**
   * Gets the indica fallecimiento.
   *
   * @return the indica fallecimiento
   */
  public Boolean getIndicaFallecimiento() {
    return indicaFallecimiento;
  }

  /**
   * Sets the indica fallecimiento.
   *
   * @param indicaFallecimiento the new indica fallecimiento
   */
  public void setIndicaFallecimiento(final Boolean indicaFallecimiento) {
    this.indicaFallecimiento = indicaFallecimiento;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
