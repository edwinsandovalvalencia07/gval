package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;

/**
 * The persistent class for the SDX_MOTGENERACION database table.
 *
 */
@Entity
@Table(name = "SDX_MOTGENERACION")
@GenericGenerator(name = "SDX_MOTGENERACION_PKMOTGENERACION_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDX_MOTGENERACION"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public final class MotivoGeneracion implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -3083448576248819148L;

  /** The pk motgeneracion. */
  @Id
  @Column(name = "PK_MOTGENERACION", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDX_MOTGENERACION_PKMOTGENERACION_GENERATOR")
  private Long pkMotgeneracion;

  /** The activo. */
  @Column(name = "MOTACTIVO", nullable = false)
  private Boolean activo;

  /** The auto. */
  @Column(name = "MOTAUTO", nullable = false)
  private Boolean auto;

  /** The codigo. */
  @Column(name = "MOTCODIGO", nullable = false, length = 3)
  private String codigo;

  /** The descripcion. */
  @Column(name = "MOTDESCRIPCION", nullable = false, length = 100)
  private String descripcion;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "MOTFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The manual. */
  @Column(name = "MOTMANUAL", nullable = false)
  private Boolean manual;

  /** The nombre. */
  @Column(name = "MOTNOMBRE", nullable = false, length = 30)
  private String nombre;


  /**
   * Instantiates a new motivo generacion.
   */
  public MotivoGeneracion() {
    // Constructor Vacío
  }


  /**
   * Gets the pk motgeneracion.
   *
   * @return the pkMotgeneracion
   */
  public Long getPkMotgeneracion() {
    return pkMotgeneracion;
  }


  /**
   * Sets the pk motgeneracion.
   *
   * @param pkMotgeneracion the pkMotgeneracion to set
   */
  public void setPkMotgeneracion(final Long pkMotgeneracion) {
    this.pkMotgeneracion = pkMotgeneracion;
  }


  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }


  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }


  /**
   * Gets the auto.
   *
   * @return the auto
   */
  public Boolean getAuto() {
    return auto;
  }


  /**
   * Sets the auto.
   *
   * @param auto the auto to set
   */
  public void setAuto(final Boolean auto) {
    this.auto = auto;
  }


  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }


  /**
   * Sets the codigo.
   *
   * @param codigo the codigo to set
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }


  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }


  /**
   * Sets the descripcion.
   *
   * @param descripcion the descripcion to set
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }


  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }


  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }


  /**
   * Gets the manual.
   *
   * @return the manual
   */
  public Boolean getManual() {
    return manual;
  }


  /**
   * Sets the manual.
   *
   * @param manual the manual to set
   */
  public void setManual(final Boolean manual) {
    this.manual = manual;
  }


  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }


  /**
   * Sets the nombre.
   *
   * @param nombre the nombre to set
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
