package com.gval.gval.entity.model;

//import com.gval.gval.common.ConstantesCommons;

import com.gval.gval.common.ConstantesCommons;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The persistent class for the SDM_PERSONA database table.
 *
 */
@Entity
@Table(name = "SDM_PERSONA")
@NamedQuery(name = "Persona.findAll", query = "SELECT s FROM Persona s")
@GenericGenerator(name = "SDM_PERSONA_PKPERSONA_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {@Parameter(name = "sequence_name", value = "SEC_SDM_PERSONA"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class Persona implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk persona. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_PERSONA_PKPERSONA_GENERATOR")
  @Column(name = "PK_PERSONA", unique = true, nullable = false)
  private Long pkPersona;

  /** The id intregracion imserso. */
  @Column(name = "IMSERSOINTEGRATIONID", length = 50)
  private String idIntregracionImserso;

  /** The pendiente envio imserso. */
  @Column(name = "PE_PENDIENTE_ENVIO")
  private Boolean pendienteEnvioImserso;

  /** The activo. */
  @Column(name = "PEACTIVO")
  private Boolean activo;

  /** The nif. */
  @Column(name = "PENUDOCIDE", length = 50)
  private String nif;

  /** The nombre. */
  @Column(name = "PENOMBRE", length = 50)
  private String nombre;

  /** The primer apellido. */
  @Column(name = "PEPRIMAPEL", length = 50)
  private String primerApellido;

  /** The segundo apellido. */
  @Column(name = "PESEGUAPEL", length = 50)
  private String segundoApellido;

  /** The fecha nacimiento. */
  @Temporal(TemporalType.DATE)
  @Column(name = "PEFECHNACI")
  private Date fechaNacimiento;

  /** The edad. */
  @Column(name = "PEEDAD")
  private Long edad;

  /** The fecha fallecimiento. */
  @Temporal(TemporalType.DATE)
  @Column(name = "PEFECHA_FALL")
  private Date fechaFallecimiento;

  /** The numero SIP. */
  @Column(name = "PENSIP", length = 50)
  private String numeroSIP;

  /** The nuss. */
  @Column(name = "PENUSS", length = 50)
  private String nuss;

  /** The centro salud. */
  @Column(name = "PECENTSALU", length = 250)
  private String centroSalud;

  /** The grupo. */
  @Column(name = "PEGRUPO")
  private Long grupo;

  /** The sector. */
  @Column(name = "PESECTOR", length = 20)
  private String sector;

  /** The autoriza acceso cbase. */
  @Column(name = "PECONCBASE")
  private Boolean autorizaAccesoCbase;

  /** The autoriza acceso identidad. */
  @Column(name = "PECONSVDI")
  private Boolean autorizaAccesoIdentidad;

  /** The autoriza acceso residencia. */
  @Column(name = "PECONSVDR")
  private Boolean autorizaAccesoResidencia;

  /** The autoriza acceso otras administraciones. */
  @Column(name = "PECONSVDO")
  private Boolean autorizaAccesoOtras;

  /** The autoriza acceso economicos. */
  @Column(name = "PEPEACDAFI")
  private Boolean autorizaAccesoEconomicos;

  /** The autoriza acceso sanitarios. */
  @Column(name = "PEPEACDASA")
  private Boolean autorizaAccesoSanitarios;

  /** The autoriza acceso nacimiento y filiacion. */
  @Column(name = "PECONSVDN")
  private Boolean autorizaAccesoNacimiento;

  /** The autoriza acceso pensiones INSS. */
  @Column(name = "PECONSVDP")
  private Boolean autorizaAccesoPensiones;

  /** The autoriza acceso registro de representantes. */
  @Column(name = "PECONSVDRR")
  private Boolean autorizaAccesoRepresentantes;

  /** The seguridad social. */
  @Column(name = "PETIENESS")
  private Boolean seguridadSocial;

  /** The servicio privado. */
  @Column(name = "PESERVPRIVAD")
  private Boolean servicioPrivado;

  /** The servicio publico. */
  @Column(name = "PESERVPUBLIC")
  private Boolean servicioPublico;

  /** The titular. */
  @Column(name = "PETITULASS")
  private Boolean titular;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "PEFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The id api. */
  @Column(name = "PER_ID")
  private Long idApi;

  /** The ind dup api. */
  @Column(name = "PER_INDDUP")
  private Long indDupApi;

  /** The sexo. */
  @Column(name = "PK_SEXO")
  private Long sexo;

  /** The nacionalidad. */
  @Column(name = "PK_NACIONA")
  private Long nacionalidad;

  /** The tipo identificador. */
  @Column(name = "PK_TIPOIDEN")
  private Long tipoIdentificador;

  /** The estado civil. */
  @Column(name = "PK_ESTACIVI")
  private Long estadoCivil;

  /** The app origen. */
  @Column(name = "APP_ORIGEN")
  private String appOrigen;

  /** The app modifica. */
  @Column(name = "APP_MODIFICA")
  private String appModifica;

  // TODO: Recuperar las relaciones de sdm_persona con las demas tablas, ya que
  // se eliminan temporalmente.

  /** The usuario. */
  // bi-directional one-to-one association to Usuario
  @OneToOne(mappedBy = "persona", cascade = CascadeType.ALL)
  private Usuario usuario;

  /**
   * On pre persist.
   */
  @PrePersist
  public void onPrePersist() {
    appOrigen = ConstantesCommons.APLICACION_ADA3;
    this.activo = Boolean.TRUE;
    this.fechaCreacion = new Date();
    appModifica = ConstantesCommons.APLICACION_ADA3;
  }

  /**
   * On pre update.
   */
  @PreUpdate
  public void onPreUpdate() {
    appModifica = ConstantesCommons.APLICACION_ADA3;
  }

  /**
   * Instantiates a new persona.
   */
  public Persona() {
    // Empty constructor
  }


  /**
   * Gets the pk persona.
   *
   * @return the pk persona
   */
  public Long getPkPersona() {
    return pkPersona;
  }

  /**
   * Sets the pk persona.
   *
   * @param pkPersona the new pk persona
   */
  public void setPkPersona(final Long pkPersona) {
    this.pkPersona = pkPersona;
  }

  /**
   * Gets the id intregracion imserso.
   *
   * @return the id intregracion imserso
   */
  public String getIdIntregracionImserso() {
    return idIntregracionImserso;
  }

  /**
   * Sets the id intregracion imserso.
   *
   * @param idIntregracionImserso the new id intregracion imserso
   */
  public void setIdIntregracionImserso(final String idIntregracionImserso) {
    this.idIntregracionImserso = idIntregracionImserso;
  }

  /**
   * Gets the pendiente envio imserso.
   *
   * @return the pendiente envio imserso
   */
  public Boolean getPendienteEnvioImserso() {
    return pendienteEnvioImserso;
  }

  /**
   * Sets the pendiente envio imserso.
   *
   * @param pendienteEnvioImserso the new pendiente envio imserso
   */
  public void setPendienteEnvioImserso(final Boolean pendienteEnvioImserso) {
    this.pendienteEnvioImserso = pendienteEnvioImserso;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the nif.
   *
   * @return the nif
   */
  public String getNif() {
    return nif;
  }

  /**
   * Sets the nif.
   *
   * @param nif the new nif
   */
  public void setNif(final String nif) {
    this.nif = nif;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the primer apellido.
   *
   * @return the primer apellido
   */
  public String getPrimerApellido() {
    return primerApellido;
  }

  /**
   * Sets the primer apellido.
   *
   * @param primerApellido the new primer apellido
   */
  public void setPrimerApellido(final String primerApellido) {
    this.primerApellido = primerApellido;
  }

  /**
   * Gets the segundo apellido.
   *
   * @return the segundo apellido
   */
  public String getSegundoApellido() {
    return segundoApellido;
  }

  /**
   * Sets the segundo apellido.
   *
   * @param segundoApellido the new segundo apellido
   */
  public void setSegundoApellido(final String segundoApellido) {
    this.segundoApellido = segundoApellido;
  }

  /**
   * Gets the fecha nacimiento.
   *
   * @return the fecha nacimiento
   */
  public Date getFechaNacimiento() {
    return (fechaNacimiento == null) ? null : (Date) fechaNacimiento.clone();
  }

  /**
   * Sets the fecha nacimiento.
   *
   * @param fechaNacimiento the new fecha nacimiento
   */
  public void setFechaNacimiento(final Date fechaNacimiento) {
    this.fechaNacimiento =
        (fechaNacimiento == null) ? null : (Date) fechaNacimiento.clone();
  }

  /**
   * Gets the edad.
   *
   * @return the edad
   */
  public Long getEdad() {
    return edad;
  }

  /**
   * Sets the edad.
   *
   * @param edad the new edad
   */
  public void setEdad(final Long edad) {
    this.edad = edad;
  }

  /**
   * Gets the fecha fallecimiento.
   *
   * @return the fecha fallecimiento
   */
  public Date getFechaFallecimiento() {
    return fechaFallecimiento;
  }

  /**
   * Sets the fecha fallecimiento.
   *
   * @param fechaFallecimiento the new fecha fallecimiento
   */
  public void setFechaFallecimiento(final Date fechaFallecimiento) {
    this.fechaFallecimiento = fechaFallecimiento;
  }

  /**
   * Gets the numero SIP.
   *
   * @return the numero SIP
   */
  public String getNumeroSIP() {
    return numeroSIP;
  }

  /**
   * Sets the numero SIP.
   *
   * @param numeroSIP the new numero SIP
   */
  public void setNumeroSIP(final String numeroSIP) {
    this.numeroSIP = numeroSIP;
  }

  /**
   * Gets the nuss.
   *
   * @return the nuss
   */
  public String getNuss() {
    return nuss;
  }

  /**
   * Sets the nuss.
   *
   * @param nuss the new nuss
   */
  public void setNuss(final String nuss) {
    this.nuss = nuss;
  }

  /**
   * Gets the centro salud.
   *
   * @return the centro salud
   */
  public String getCentroSalud() {
    return centroSalud;
  }

  /**
   * Sets the centro salud.
   *
   * @param centroSalud the new centro salud
   */
  public void setCentroSalud(final String centroSalud) {
    this.centroSalud = centroSalud;
  }

  /**
   * Gets the grupo.
   *
   * @return the grupo
   */
  public Long getGrupo() {
    return grupo;
  }

  /**
   * Sets the grupo.
   *
   * @param grupo the new grupo
   */
  public void setGrupo(final Long grupo) {
    this.grupo = grupo;
  }

  /**
   * Gets the sector.
   *
   * @return the sector
   */
  public String getSector() {
    return sector;
  }

  /**
   * Sets the sector.
   *
   * @param sector the new sector
   */
  public void setSector(final String sector) {
    this.sector = sector;
  }

  /**
   * Gets the autoriza acceso cbase.
   *
   * @return the autoriza acceso cbase
   */
  public Boolean getAutorizaAccesoCbase() {
    return autorizaAccesoCbase;
  }

  /**
   * Sets the autoriza acceso cbase.
   *
   * @param autorizaAccesoCbase the new autoriza acceso cbase
   */
  public void setAutorizaAccesoCbase(final Boolean autorizaAccesoCbase) {
    this.autorizaAccesoCbase = autorizaAccesoCbase;
  }

  /**
   * Gets the autoriza acceso identidad.
   *
   * @return the autoriza acceso identidad
   */
  public Boolean getAutorizaAccesoIdentidad() {
    return autorizaAccesoIdentidad;
  }

  /**
   * Sets the autoriza acceso identidad.
   *
   * @param autorizaAccesoIdentidad the new autoriza acceso identidad
   */
  public void setAutorizaAccesoIdentidad(
      final Boolean autorizaAccesoIdentidad) {
    this.autorizaAccesoIdentidad = autorizaAccesoIdentidad;
  }

  /**
   * Gets the autoriza acceso residencia.
   *
   * @return the autoriza acceso residencia
   */
  public Boolean getAutorizaAccesoResidencia() {
    return autorizaAccesoResidencia;
  }

  /**
   * Sets the autoriza acceso residencia.
   *
   * @param autorizaAccesoResidencia the new autoriza acceso residencia
   */
  public void setAutorizaAccesoResidencia(
      final Boolean autorizaAccesoResidencia) {
    this.autorizaAccesoResidencia = autorizaAccesoResidencia;
  }


  /**
   * Gets the autoriza acceso otras.
   *
   * @return the autoriza acceso otras
   */
  public Boolean getAutorizaAccesoOtras() {
    return autorizaAccesoOtras;
  }


  /**
   * Sets the autoriza acceso otras.
   *
   * @param autorizaAccesoOtras the new autoriza acceso otras
   */
  public void setAutorizaAccesoOtras(final Boolean autorizaAccesoOtras) {
    this.autorizaAccesoOtras = autorizaAccesoOtras;
  }


  /**
   * Gets the autoriza acceso economicos.
   *
   * @return the autoriza acceso economicos
   */
  public Boolean getAutorizaAccesoEconomicos() {
    return autorizaAccesoEconomicos;
  }

  /**
   * Sets the autoriza acceso economicos.
   *
   * @param autorizaAccesoEconomicos the new autoriza acceso economicos
   */
  public void setAutorizaAccesoEconomicos(
      final Boolean autorizaAccesoEconomicos) {
    this.autorizaAccesoEconomicos = autorizaAccesoEconomicos;
  }

  /**
   * Gets the autoriza acceso nacimiento.
   *
   * @return the autorizaAccesoNacimiento
   */
  public Boolean getAutorizaAccesoNacimiento() {
    return autorizaAccesoNacimiento;
  }

  /**
   * Sets the autoriza acceso nacimiento.
   *
   * @param autorizaAccesoNacimiento the autorizaAccesoNacimiento to set
   */
  public void setAutorizaAccesoNacimiento(
      final Boolean autorizaAccesoNacimiento) {
    this.autorizaAccesoNacimiento = autorizaAccesoNacimiento;
  }

  /**
   * Gets the autoriza acceso pensiones.
   *
   * @return the autorizaAccesoPensiones
   */
  public Boolean getAutorizaAccesoPensiones() {
    return autorizaAccesoPensiones;
  }

  /**
   * Sets the autoriza acceso pensiones.
   *
   * @param autorizaAccesoPensiones the autorizaAccesoPensiones to set
   */
  public void setAutorizaAccesoPensiones(
      final Boolean autorizaAccesoPensiones) {
    this.autorizaAccesoPensiones = autorizaAccesoPensiones;
  }

  /**
   * Gets the autoriza acceso representantes.
   *
   * @return the autorizaAccesoRepresentantes
   */
  public Boolean getAutorizaAccesoRepresentantes() {
    return autorizaAccesoRepresentantes;
  }

  /**
   * Sets the autoriza acceso representantes.
   *
   * @param autorizaAccesoRepresentantes the autorizaAccesoRepresentantes to set
   */
  public void setAutorizaAccesoRepresentantes(
      final Boolean autorizaAccesoRepresentantes) {
    this.autorizaAccesoRepresentantes = autorizaAccesoRepresentantes;
  }

  /**
   * Gets the autoriza acceso sanitarios.
   *
   * @return the autoriza acceso sanitarios
   */
  public Boolean getAutorizaAccesoSanitarios() {
    return autorizaAccesoSanitarios;
  }

  /**
   * Sets the autoriza acceso sanitarios.
   *
   * @param autorizaAccesoSanitarios the new autoriza acceso sanitarios
   */
  public void setAutorizaAccesoSanitarios(
      final Boolean autorizaAccesoSanitarios) {
    this.autorizaAccesoSanitarios = autorizaAccesoSanitarios;
  }

  /**
   * Gets the seguridad social.
   *
   * @return the seguridad social
   */
  public Boolean getSeguridadSocial() {
    return seguridadSocial;
  }

  /**
   * Sets the seguridad social.
   *
   * @param seguridadSocial the new seguridad social
   */
  public void setSeguridadSocial(final Boolean seguridadSocial) {
    this.seguridadSocial = seguridadSocial;
  }

  /**
   * Gets the servicio privado.
   *
   * @return the servicio privado
   */
  public Boolean getServicioPrivado() {
    return servicioPrivado;
  }

  /**
   * Sets the servicio privado.
   *
   * @param servicioPrivado the new servicio privado
   */
  public void setServicioPrivado(final Boolean servicioPrivado) {
    this.servicioPrivado = servicioPrivado;
  }

  /**
   * Gets the servicio publico.
   *
   * @return the servicio publico
   */
  public Boolean getServicioPublico() {
    return servicioPublico;
  }

  /**
   * Sets the servicio publico.
   *
   * @param servicioPublico the new servicio publico
   */
  public void setServicioPublico(final Boolean servicioPublico) {
    this.servicioPublico = servicioPublico;
  }

  /**
   * Gets the titular.
   *
   * @return the titular
   */
  public Boolean getTitular() {
    return titular;
  }

  /**
   * Sets the titular.
   *
   * @param titular the new titular
   */
  public void setTitular(final Boolean titular) {
    this.titular = titular;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return (fechaCreacion == null) ? null : (Date) fechaCreacion.clone();
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion =
        (fechaCreacion == null) ? null : (Date) fechaCreacion.clone();
  }

  /**
   * Gets the id api.
   *
   * @return the id api
   */
  public Long getIdApi() {
    return idApi;
  }

  /**
   * Sets the id api.
   *
   * @param idApi the new id api
   */
  public void setIdApi(final Long idApi) {
    this.idApi = idApi;
  }

  /**
   * Gets the ind dup api.
   *
   * @return the ind dup api
   */
  public Long getIndDupApi() {
    return indDupApi;
  }

  /**
   * Sets the ind dup api.
   *
   * @param indDupApi the new ind dup api
   */
  public void setIndDupApi(final Long indDupApi) {
    this.indDupApi = indDupApi;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }


  /**
   * Sets the usuario.
   *
   * @param usuario the usuario to set
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }


  /**
   * Gets the sexo.
   *
   * @return the sexo
   */
  public Long getSexo() {
    return sexo;
  }

  /**
   * Sets the sexo.
   *
   * @param sexo the new sexo
   */
  public void setSexo(final Long sexo) {
    this.sexo = sexo;
  }

  /**
   * Gets the nacionalidad.
   *
   * @return the nacionalidad
   */
  public Long getNacionalidad() {
    return nacionalidad;
  }

  /**
   * Sets the nacionalidad.
   *
   * @param nacionalidad the new nacionalidad
   */
  public void setNacionalidad(final Long nacionalidad) {
    this.nacionalidad = nacionalidad;
  }

  /**
   * Gets the tipo identificador.
   *
   * @return the tipo identificador
   */
  public Long getTipoIdentificador() {
    return tipoIdentificador;
  }

  /**
   * Sets the tipo identificador.
   *
   * @param tipoIdentificador the new tipo identificador
   */
  public void setTipoIdentificador(final Long tipoIdentificador) {
    this.tipoIdentificador = tipoIdentificador;
  }


  /**
   * Gets the estado civil.
   *
   * @return the estado civil
   */
  public Long getEstadoCivil() {
    return estadoCivil;
  }


  /**
   * Sets the estado civil.
   *
   * @param estadoCivil the new estado civil
   */
  public void setEstadoCivil(final Long estadoCivil) {
    this.estadoCivil = estadoCivil;
  }

  /**
   * Gets the app origen.
   *
   * @return the app origen
   */
  public String getAppOrigen() {
    return appOrigen;
  }

  /**
   * Sets the app origen.
   *
   * @param appOrigen the new app origen
   */
  public void setAppOrigen(final String appOrigen) {
    this.appOrigen = appOrigen;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
