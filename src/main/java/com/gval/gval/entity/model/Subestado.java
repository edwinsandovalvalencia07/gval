package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

import jakarta.persistence.*;


/**
 * The persistent class for the SDX_SUBESTADO_SOLICITUD database table.
 *
 */
@Entity
@Table(name = "SDX_SUBESTADO_SOLICITUD")
public class Subestado implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -6090370353224950625L;


  /** The pk subestado. */
  @Id
  @Column(name = "PK_SUBESTADO_SOLICITUD", unique = true, nullable = false)
  private Long pkSubestado;

  /** The activo. */
  @Column(name = "SUSACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /** The codigo. */
  @Column(name = "SUSCODIGO", nullable = false, length = 3)
  private String codigo;

  /** The descripcion. */
  @Column(name = "SUSDESCRIPCION", nullable = false, length = 200)
  private String descripcion;

  /**
   * Instantiates a new subestado.
   */
  public Subestado() {
    // Empty constructor
  }

  /**
   * Gets the pk subestado.
   *
   * @return the pk subestado
   */
  public Long getPkSubestado() {
    return pkSubestado;
  }

  /**
   * Sets the pk subestado.
   *
   * @param pkSubestado the new pk subestado
   */
  public void setPkSubestado(final Long pkSubestado) {
    this.pkSubestado = pkSubestado;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
