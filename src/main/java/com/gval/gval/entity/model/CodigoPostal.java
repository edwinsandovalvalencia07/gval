package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;
import jakarta.persistence.*;


/**
 * The persistent class for the SDX_CODPOS database table.
 *
 */
@Entity
@Table(name = "SDX_CODPOS")
public class CodigoPostal implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 2743518373650048285L;


  /** The pk codigo postal. */
  @Id
  @Column(name = "PK_CODPOS", unique = true, nullable = false)
  private Long pkCodigoPostal;

  /** The activo. */
  @Column(name = "COACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /** The codigo postal. */
  @Column(name = "COCP", nullable = false, length = 5)
  private String codigoPostal;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "COFECHCREA", nullable = false)
  private Date fechaCreacion;

  /**
   * Instantiates a new codigo postal.
   */
  public CodigoPostal() {
    // Empty constructor
  }

  /**
   * Instantiates a new codigo postal.
   *
   * @param pkCodigoPostal the pk codigo postal
   * @param activo the activo
   * @param codigoPostal the codigo postal
   * @param fechaCreacion the fecha creacion
   */
  public CodigoPostal(final Long pkCodigoPostal, final Boolean activo,
      final String codigoPostal, final Date fechaCreacion) {
    super();
    this.pkCodigoPostal = pkCodigoPostal;
    this.activo = activo;
    this.codigoPostal = codigoPostal;
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the pk codigo postal.
   *
   * @return the pk codigo postal
   */
  public long getPkCodigoPostal() {
    return pkCodigoPostal;
  }

  /**
   * Sets the pk codigo postal.
   *
   * @param pkCodigoPostal the new pk codigo postal
   */
  public void setPkCodigoPostal(final Long pkCodigoPostal) {
    this.pkCodigoPostal = pkCodigoPostal;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the codigo postal.
   *
   * @return the codigo postal
   */
  public String getCodigoPostal() {
    return codigoPostal;
  }

  /**
   * Sets the codigo postal.
   *
   * @param codigoPostal the new codigo postal
   */
  public void setCodigoPostal(final String codigoPostal) {
    this.codigoPostal = codigoPostal;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
