package com.gval.gval.entity.model;

import java.io.Serializable;
import java.util.Date;



import jakarta.persistence.*;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Parameter;

// TODO: Auto-generated Javadoc
/**
 * The Class Acta.
 */
@Entity
@Table(name = "SDM_ACTA")
@GenericGenerator(name = "SDM_ACTA_PKACTA_GENERATOR", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
		@Parameter(name = "sequence_name", value = "SEC_SDM_ACTA"), @Parameter(name = "initial_value", value = "1"),
		@Parameter(name = "increment_size", value = "1") })
public class Acta  implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 294925675088934497L;

	/** The Constant serialVersionUID. */


	/** The pk acta. */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SDM_ACTA_PKACTA_GENERATOR")
	@Column(name = "PK_ACTA", nullable = false)
	private Long pkActa;

	/** The comision. */
	@ManyToOne
	@JoinColumn(name = "PK_COMISION", nullable = false)
	private Comision sdmComision;


	/** The firma 1. */
	@ManyToOne
	@JoinColumn(name = "PK_FIRMA", nullable = false)
	private Firma sdmFirma1;

	/** The firma 2. */
	@ManyToOne
	@JoinColumn(name = "PK_FIRMA2")
	private Firma sdmFirma2;

	/** The firma 3. */
	@ManyToOne
	@JoinColumn(name = "PK_FIRMA3")
	private Firma sdmFirma3;

	/** The firma 4. */
	@ManyToOne
	@JoinColumn(name = "PK_FIRMA4")
	private Firma sdmFirma4;

	/** The firma 5. */
	@ManyToOne
	@JoinColumn(name = "PK_FIRMA5")
	private Firma sdmFirma5;

	/** The docu. */
	@ManyToOne
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PK_DOCU")
	private DocumentoGenerado sdmDocugene;

	/** The fecha. */
	@Temporal(TemporalType.DATE)
	@Column(name = "ACFECHA")
	private Date acfecha;

	/** The activo. */
	@Column(name = "ACACTIVO")
	private Boolean acactivo;

	/** The fecha. */
	@Temporal(TemporalType.DATE)
	@Column(name = "ACFECHCREA")
	private Date acfechcrea;


	/**
	 * Instantiates a new acta.
	 */
	public Acta (){}




	/**
	 * Gets the pk acta.
	 *
	 * @return the pk acta
	 */
	public Long getPkActa() {
		return pkActa;
	}




	/**
	 * Sets the pk acta.
	 *
	 * @param pkActa the new pk acta
	 */
	public void setPkActa(final Long pkActa) {
		this.pkActa = pkActa;
	}




	/**
	 * Gets the sdm comision.
	 *
	 * @return the sdm comision
	 */
	public Comision getSdmComision() {
		return sdmComision;
	}




	/**
	 * Sets the sdm comision.
	 *
	 * @param sdmComision the new sdm comision
	 */
	public void setSdmComision(final Comision sdmComision) {
		this.sdmComision = sdmComision;
	}




	/**
	 * Gets the sdm firma 1.
	 *
	 * @return the sdm firma 1
	 */
	public Firma getSdmFirma1() {
		return sdmFirma1;
	}




	/**
	 * Sets the sdm firma 1.
	 *
	 * @param sdmFirma1 the new sdm firma 1
	 */
	public void setSdmFirma1(final Firma sdmFirma1) {
		this.sdmFirma1 = sdmFirma1;
	}




	/**
	 * Gets the sdm firma 2.
	 *
	 * @return the sdm firma 2
	 */
	public Firma getSdmFirma2() {
		return sdmFirma2;
	}




	/**
	 * Sets the sdm firma 2.
	 *
	 * @param sdmFirma2 the new sdm firma 2
	 */
	public void setSdmFirma2(final Firma sdmFirma2) {
		this.sdmFirma2 = sdmFirma2;
	}




	/**
	 * Gets the sdm firma 3.
	 *
	 * @return the sdm firma 3
	 */
	public Firma getSdmFirma3() {
		return sdmFirma3;
	}




	/**
	 * Sets the sdm firma 3.
	 *
	 * @param sdmFirma3 the new sdm firma 3
	 */
	public void setSdmFirma3(final Firma sdmFirma3) {
		this.sdmFirma3 = sdmFirma3;
	}




	/**
	 * Gets the sdm firma 4.
	 *
	 * @return the sdm firma 4
	 */
	public Firma getSdmFirma4() {
		return sdmFirma4;
	}




	/**
	 * Sets the sdm firma 4.
	 *
	 * @param sdmFirma4 the new sdm firma 4
	 */
	public void setSdmFirma4(final Firma sdmFirma4) {
		this.sdmFirma4 = sdmFirma4;
	}




	/**
	 * Gets the sdm firma 5.
	 *
	 * @return the sdm firma 5
	 */
	public Firma getSdmFirma5() {
		return sdmFirma5;
	}




	/**
	 * Sets the sdm firma 5.
	 *
	 * @param sdmFirma5 the new sdm firma 5
	 */
	public void setSdmFirma5(final Firma sdmFirma5) {
		this.sdmFirma5 = sdmFirma5;
	}




	/**
	 * Gets the sdm docugene.
	 *
	 * @return the sdm docugene
	 */
	public DocumentoGenerado getSdmDocugene() {
		return sdmDocugene;
	}

	/**
	 * Sets the sdm docugene.
	 *
	 * @param sdmDocugene the new sdm docugene
	 */
	public void setSdmDocugene(final DocumentoGenerado sdmDocugene) {
		this.sdmDocugene = sdmDocugene;
	}



	/**
 * Gets the acfecha.
 *
 * @return the acfecha
 */
public Date getAcfecha() {
		return acfecha;
	}




	/**
	 * Sets the acfecha.
	 *
	 * @param acfecha the new acfecha
	 */
	public void setAcfecha(final Date acfecha) {
		this.acfecha = acfecha;
	}




	/**
	 * Gets the acactivo.
	 *
	 * @return the acactivo
	 */
	public Boolean getAcactivo() {
		return acactivo;
	}




	/**
	 * Sets the acactivo.
	 *
	 * @param acactivo the new acactivo
	 */
	public void setAcactivo(final Boolean acactivo) {
		this.acactivo = acactivo;
	}




	/**
	 * Gets the acfechcrea.
	 *
	 * @return the acfechcrea
	 */
	public Date getAcfechcrea() {
		return acfechcrea;
	}




	/**
	 * Sets the acfechcrea.
	 *
	 * @param acfechcrea the new acfechcrea
	 */
	public void setAcfechcrea(final Date acfechcrea) {
		this.acfechcrea = acfechcrea;
	}




	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}


}
