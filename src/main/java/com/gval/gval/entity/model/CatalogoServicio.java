package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The Class CatalogoServicio.
 */
@Entity
@Table(name = "SDX_CATASERV")
public class CatalogoServicio implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 2361447038085554847L;

  /** The pk cataserv. */
  @Id
  @Column(name = "PK_CATASERV", unique = true, nullable = false)
  private Long pkCataserv;

  /** The codigo. */
  @Column(name = "CACODIGO", nullable = false, length = 2)
  private String codigo;

  /** The nombre. */
  @Column(name = "CANOMBRE", nullable = false, length = 100)
  private String nombre;

  /** The orden. */
  @Column(name = "CAORDEN", precision = 2)
  private Long orden;

  /** The activo. */
  @Column(name = "CAACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "CAFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The intensidad fija. */
  @Column(name = "INTENSIDADFIJA", length = 1)
  private String intensidadFija;

  /** The empresas. */
  @Column(name = "EMPRESASADSN", length = 1)
  private String empresas;

  /** The transportes. */
  @Column(name = "TRANSPORTESN", length = 1)
  private String transportes;

  /** The tipo. */
  @Column(name = "CATIPO", nullable = false, length = 1)
  private String tipo;

  /** The nombre abreviado. */
  @Column(name = "CANOMBRE_ABREV", length = 60)
  private String nombreAbreviado;

  /** The imserso. */
  @Column(name = "CAIMSERSO", length = 5)
  private String imserso;

  /** The imserso superior. */
  @Column(name = "CAIMSERSO_SUPERIOR", length = 5)
  private String imsersoSuperior;

  /** The pagos. */
  @Column(name = "CAPAGOS", precision = 1)
  private Boolean pagos;

  /** The combo preferencias. */
  @Column(name = "COMBOPREFERENCIAS", precision = 1)
  private Boolean comboPreferencias;

  /** The combo prestaciones PIA. */
  @Column(name = "COMBOPRESTACIONESPIA", precision = 1)
  private Boolean comboPrestacionesPIA;

  /** The prestaciones servicios. */
  @Column(name = "PVSSERVPROMO", precision = 1)
  private Boolean prestacionesServicios;

  /** The necesita contrato. */
  @Column(name = "CANECESITA_CONTRATO", precision = 1)
  private Boolean necesitaContrato;

  /** The id servicio equivalente. */
  @Column(name = "PK_CATASERV_EQUIVALE", precision = 1, nullable = false)
  private Long idServicioEquivalente;
  
  /** The indica si el servicio tiene centros asociados */
  @Column(name = "CATIENECENTROS", nullable = false, precision = 1)
  private boolean tieneCentros;
  
  /**
   * Instantiates a new catalogo servicio.
   */
  public CatalogoServicio() {
    // Constructor
  }

  /**
   * Gets the pk cataserv.
   *
   * @return the pk cataserv
   */
  public Long getPkCataserv() {
    return pkCataserv;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Gets the orden.
   *
   * @return the orden
   */
  public Long getOrden() {
    return orden;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return fechaCreacion;
  }

  /**
   * Gets the intensidad fija.
   *
   * @return the intensidad fija
   */
  public String getIntensidadFija() {
    return intensidadFija;
  }

  /**
   * Gets the empresas.
   *
   * @return the empresas
   */
  public String getEmpresas() {
    return empresas;
  }

  /**
   * Gets the transportes.
   *
   * @return the transportes
   */
  public String getTransportes() {
    return transportes;
  }

  /**
   * Gets the tipo.
   *
   * @return the tipo
   */
  public String getTipo() {
    return tipo;
  }

  /**
   * Gets the nombre abreviado.
   *
   * @return the nombre abreviado
   */
  public String getNombreAbreviado() {
    return nombreAbreviado;
  }

  /**
   * Gets the imserso.
   *
   * @return the imserso
   */
  public String getImserso() {
    return imserso;
  }

  /**
   * Gets the imserso superior.
   *
   * @return the imserso superior
   */
  public String getImsersoSuperior() {
    return imsersoSuperior;
  }

  /**
   * Gets the pagos.
   *
   * @return the pagos
   */
  public Boolean getPagos() {
    return pagos;
  }

  /**
   * Gets the combo preferencias.
   *
   * @return the combo preferencias
   */
  public Boolean getComboPreferencias() {
    return comboPreferencias;
  }

  /**
   * Gets the combo prestaciones PIA.
   *
   * @return the combo prestaciones PIA
   */
  public Boolean getComboPrestacionesPIA() {
    return comboPrestacionesPIA;
  }

  /**
   * Gets the prestaciones servicios.
   *
   * @return the prestaciones servicios
   */
  public Boolean getPrestacionesServicios() {
    return prestacionesServicios;
  }

  /**
   * Gets the necesita contrato.
   *
   * @return the necesita contrato
   */
  public Boolean getNecesitaContrato() {
    return necesitaContrato;
  }

  /**
   * Sets the pk cataserv.
   *
   * @param pkCataserv the new pk cataserv
   */
  public void setPkCataserv(final Long pkCataserv) {
    this.pkCataserv = pkCataserv;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Sets the orden.
   *
   * @param orden the new orden
   */
  public void setOrden(final Long orden) {
    this.orden = orden;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

  /**
   * Sets the intensidad fija.
   *
   * @param intensidadFija the new intensidad fija
   */
  public void setIntensidadFija(final String intensidadFija) {
    this.intensidadFija = intensidadFija;
  }

  /**
   * Sets the empresas.
   *
   * @param empresas the new empresas
   */
  public void setEmpresas(final String empresas) {
    this.empresas = empresas;
  }

  /**
   * Sets the transportes.
   *
   * @param transportes the new transportes
   */
  public void setTransportes(final String transportes) {
    this.transportes = transportes;
  }

  /**
   * Sets the tipo.
   *
   * @param tipo the new tipo
   */
  public void setTipo(final String tipo) {
    this.tipo = tipo;
  }

  /**
   * Sets the nombre abreviado.
   *
   * @param nombreAbreviado the new nombre abreviado
   */
  public void setNombreAbreviado(final String nombreAbreviado) {
    this.nombreAbreviado = nombreAbreviado;
  }

  /**
   * Sets the imserso.
   *
   * @param imserso the new imserso
   */
  public void setImserso(final String imserso) {
    this.imserso = imserso;
  }

  /**
   * Sets the imserso superior.
   *
   * @param imsersoSuperior the new imserso superior
   */
  public void setImsersoSuperior(final String imsersoSuperior) {
    this.imsersoSuperior = imsersoSuperior;
  }

  /**
   * Sets the pagos.
   *
   * @param pagos the new pagos
   */
  public void setPagos(final Boolean pagos) {
    this.pagos = pagos;
  }

  /**
   * Sets the combo preferencias.
   *
   * @param comboPreferencias the new combo preferencias
   */
  public void setComboPreferencias(final Boolean comboPreferencias) {
    this.comboPreferencias = comboPreferencias;
  }

  /**
   * Sets the combo prestaciones PIA.
   *
   * @param comboPrestacionesPIA the new combo prestaciones PIA
   */
  public void setComboPrestacionesPIA(final Boolean comboPrestacionesPIA) {
    this.comboPrestacionesPIA = comboPrestacionesPIA;
  }

  /**
   * Sets the prestaciones servicios.
   *
   * @param prestacionesServicios the new prestaciones servicios
   */
  public void setPrestacionesServicios(final Boolean prestacionesServicios) {
    this.prestacionesServicios = prestacionesServicios;
  }

  /**
   * Sets the necesita contrato.
   *
   * @param necesitaContrato the new necesita contrato
   */
  public void setNecesitaContrato(final Boolean necesitaContrato) {
    this.necesitaContrato = necesitaContrato;
  }

  /**
   * Gets the id servicio equivalente.
   *
   * @return the id servicio equivalente
   */
  public Long getIdServicioEquivalente() {
    return idServicioEquivalente;
  }

  /**
   * Sets the id servicio equivalente.
   *
   * @param idServicioEquivalente the new id servicio equivalente
   */
  public void setIdServicioEquivalente(final Long idServicioEquivalente) {
    this.idServicioEquivalente = idServicioEquivalente;
  }
  
  /**
   * Gets the tiene centros asociados.
   *
   * @return the tiene centros asociados
   */
  public boolean getTieneCentros() {
    return tieneCentros;
  }

  /**
   * Sets the tiene centros asociados.
   *
   * @param tieneCentros the new  tiene centros asociados
   */
  public void setTieneCentros(boolean tieneCentros) {
    this.tieneCentros = tieneCentros;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
