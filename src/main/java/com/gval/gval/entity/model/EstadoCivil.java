package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The persistent class for the SDX_ESTACIVI database table.
 *
 */
@Entity
@Table(name = "SDX_ESTACIVI")
@GenericGenerator(name = "SDM_ESTADOCIVIL_PKESTACIVI_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name",
            value = "SDM_ESTADOCIVIL_PKESTACIVI_GENERATOR"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public final class EstadoCivil implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -6089355878333423003L;

  /** Clave Primaria. **/
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_ESTADOCIVIL_PKESTACIVI_GENERATOR")
  @Column(name = "PK_ESTACIVI", unique = true, nullable = false)
  private Long pkEstadoCivil;

  /** Indica un borrado logico si no esta activo, en caso contrario esta vigente. */
  @Column(name = "ESCACTIVO", nullable = false)
  private Boolean activo;

  /**  Fecha de creación del registro *. */
  @Temporal(TemporalType.DATE)
  @Column(name = "ESFECHCREA", nullable = false)
  private Date fechaCreacion;

  /**  Codigo del estado civil en el catalogo del IMSERSO *. */
  @Column(name = "ESIMSERSO", length = 1)
  private String imserso;

  /**  Nombre del Estado Civil *. */
  @Column(name = "ESNOMBRE", nullable = false, length = 15)
  private String nombre;

  /**
   * Gets the pk estado civil.
   *
   * @return the pkEstadoCivil
   */
  public Long getPkEstadoCivil() {
    return pkEstadoCivil;
  }

  /**
   * Sets the pk estado civil.
   *
   * @param pkEstadoCivil the pkEstadoCivil to set
   */
  public void setPkEstadoCivil(final Long pkEstadoCivil) {
    this.pkEstadoCivil = pkEstadoCivil;
  }

  /**
   * Gets the activo.
   *
   * @return the escactivo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    if (fechaCreacion != null) {
      return (Date) fechaCreacion.clone();
    }
    return fechaCreacion;
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = (Date) fechaCreacion.clone();
  }

  /**
   * Gets the imserso.
   *
   * @return the imserso
   */
  public String getImserso() {
    return imserso;
  }

  /**
   * Sets the imserso.
   *
   * @param imserso the imserso to set
   */
  public void setImserso(final String imserso) {
    this.imserso = imserso;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the nombre to set
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
