package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;

import jakarta.persistence.*;

/**
 * The Class GrupoValoracion.
 */
@Entity
@Table(name = "SDX_GRUPOVAL")
@GenericGenerator(name = "SDX_GRUPOVAL_PKGRUPOVAL_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDX_GRUPOVAL"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class GrupoValoracion implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 5243098169080152201L;

  /** The pk grupo valoracion. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDX_GRUPOVAL_PKGRUPOVAL_GENERATOR")
  @Column(name = "PK_GRUPOVAL", unique = true, nullable = false)
  private Long pkGrupoValoracion;

  /** The codigo. */
  @Column(name = "GVCOD", nullable = false, length = 2)
  private String codigo;

  /** The descripcion. */
  @Column(name = "GVDESC", length = 30)
  private String descripcion;

  /** The activo. */
  @Column(name = "GVACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /**
   * Instantiates a new grupo valoracion.
   */
  public GrupoValoracion() {
    super();
  }

  /**
   * Gets the pk grupo valoracion.
   *
   * @return the pkGrupoValoracion
   */
  public Long getPkGrupoValoracion() {
    return pkGrupoValoracion;
  }

  /**
   * Sets the pk grupo valoracion.
   *
   * @param pkGrupoValoracion the pkGrupoValoracion to set
   */
  public void setPkGrupoValoracion(final Long pkGrupoValoracion) {
    this.pkGrupoValoracion = pkGrupoValoracion;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the codigo to set
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the descripcion to set
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
