package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;



/**
 * The persistent class for the SDM_DOCUSUBS database table.
 *
 */
@Entity
@Table(name = "SDX_CATEGORIA_DOCSUB")
@GenericGenerator(name = "SDX_CATEGORIA_PK_CATEGORIA_DOCSUB_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDX_CATEGORIA_DOCSUB"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})

public final class CategoriaDocSubsanacion implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 7986859777475110841L;


  /** The pk categoria doc sub. */
  @Id
  @Column(name = "PK_CATEGORIA_DOCSUB", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDX_CATEGORIA_PK_CATEGORIA_DOCSUB_GENERATOR")
  private Long pkCategoriaDocSub;


  /** The fecha creado. */
  @Temporal(TemporalType.DATE)
  @Column(name = "CDFECHACRE")
  private Date fechaCreado;


  /** The categoria. */
  @Column(name = "CDCATEGORIA", length = 2)
  private String categoria;

  /** The relativo. */
  @Column(name = "CDRELATIVO", nullable = false)
  private Boolean relativo;

  /** The activo. */
  @Column(name = "CDACTIVO", nullable = false)
  private Boolean activo;

  /** The tipo documento. */
  // bi-directional many-to-one association to SdxTipodocu
  @ManyToOne
  @JoinColumn(name = "PK_TIPODOCU", nullable = false)
  private TipoDocumento tipoDocumento;


  /**
   * Instantiates a new categoria doc subsanacion.
   */
  public CategoriaDocSubsanacion() {
    // Constructor vacío
  }

  /**
   * Gets the fecha creado.
   *
   * @return the fecha creado
   */
  public Date getFechaCreado() {
    return UtilidadesCommons.cloneDate(fechaCreado);
  }

  /**
   * Sets the fecha Creado.
   *
   * @param fechaCreado the fechaCreado to set
   */
  public void setFechaCreado(final Date fechaCreado) {
    this.fechaCreado = UtilidadesCommons.cloneDate(fechaCreado);
  }

  /**
   * Gets the pk categoria doc sub.
   *
   * @return the pk categoria doc sub
   */
  public Long getPkCategoriaDocSub() {
    return pkCategoriaDocSub;
  }

  /**
   * Sets the pk categoria doc sub.
   *
   * @param pkCategoriaDocSub the new pk categoria doc sub
   */
  public void setPkCategoriaDocSub(final Long pkCategoriaDocSub) {
    this.pkCategoriaDocSub = pkCategoriaDocSub;
  }

  /**
   * Gets the categoria.
   *
   * @return the categoria
   */
  public String getCategoria() {
    return categoria;
  }

  /**
   * Sets the categoria.
   *
   * @param categoria the new categoria
   */
  public void setCategoria(final String categoria) {
    this.categoria = categoria;
  }

  /**
   * Gets the relativo.
   *
   * @return the relativo
   */
  public Boolean getRelativo() {
    return relativo;
  }

  /**
   * Sets the relativo.
   *
   * @param relativo the new relativo
   */
  public void setRelativo(final Boolean relativo) {
    this.relativo = relativo;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the tipo documento.
   *
   * @return the tipo documento
   */
  public TipoDocumento getTipoDocumento() {
    return tipoDocumento;
  }

  /**
   * Sets the tipo documento.
   *
   * @param tipoDocumento the new tipo documento
   */
  public void setTipoDocumento(final TipoDocumento tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
