package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.sql.Timestamp;

import jakarta.persistence.*;


/**
 * The Class Trazabilidad.
 */
@Entity
@Table(name = "SDM_TRAZABILIDAD")
public class Trazabilidad {

  /** The timestamp ejecucion. */
  @Id
  @Column(name = "TRZ_TMSTMP")
  private Timestamp timestampEjecucion;

  /** The duracion ejecucion. */
  @Column(name = "TRZ_TMP_EJECUCION")
  private Long duracionEjecucion;

  /** The id usuario. */
  @Column(name = "TRZ_ID_USUARIO", length = 20)
  private String idUsuario;

  /** The objeto. */
  @Column(name = "TRZ_OBJETO", length = 1000)
  private String objeto;

  /** The accion. */
  @Column(name = "TRZ_ACCION", length = 255)
  private String accion;

  /** The parametros. */
  @Column(name = "TRZ_PARAMETROS", length = 4000)
  private String parametros;

  /** The texto auxiliar. */
  @Column(name = "TRZ_TEXTO_AUXILIAR", length = 4000)
  private String textoAuxiliar;

  /** The comentario administrador. */
  @Column(name = "TRZ_COMENTARIO_ADMINISTRADOR", length = 4000)
  private String comentarioAdministrador;

  /**
   * Gets the timestamp ejecucion.
   *
   * @return timestampEjecucion
   */
  public Timestamp getTimestampEjecucion() {
    return timestampEjecucion;
  }

  /**
   * Sets the timestamp ejecucion.
   *
   * @param timestampEjecucion the new timestamp ejecucion
   */
  public void setTimestampEjecucion(final Timestamp timestampEjecucion) {
    this.timestampEjecucion = timestampEjecucion;
  }

  /**
   * Gets the duracion ejecucion.
   *
   * @return duracionEjecucion
   */
  public Long getDuracionEjecucion() {
    return duracionEjecucion;
  }

  /**
   * Sets the duracion ejecucion.
   *
   * @param duracionEjecucion the new duracion ejecucion
   */
  public void setDuracionEjecucion(final Long duracionEjecucion) {
    this.duracionEjecucion = duracionEjecucion;
  }

  /**
   * Gets the id usuario.
   *
   * @return idUsuario
   */
  public String getIdUsuario() {
    return idUsuario;
  }

  /**
   * Sets the id usuario.
   *
   * @param idUsuario the new id usuario
   */
  public void setIdUsuario(final String idUsuario) {
    this.idUsuario = idUsuario;
  }

  /**
   * Gets the objeto.
   *
   * @return objeto
   */
  public String getObjeto() {
    return objeto;
  }

  /**
   * Sets the objeto.
   *
   * @param objeto the new objeto
   */
  public void setObjeto(final String objeto) {
    this.objeto = objeto;
  }

  /**
   * Gets the accion.
   *
   * @return accion
   */
  public String getAccion() {
    return accion;
  }

  /**
   * Sets the accion.
   *
   * @param accion the new accion
   */
  public void setAccion(final String accion) {
    this.accion = accion;
  }

  /**
   * Gets the parametros.
   *
   * @return parametros
   */
  public String getParametros() {
    return parametros;
  }

  /**
   * Sets the parametros.
   *
   * @param parametros the new parametros
   */
  public void setParametros(final String parametros) {
    this.parametros = parametros;
  }

  /**
   * Gets the texto auxiliar.
   *
   * @return textoAuxiliar
   */
  public String getTextoAuxiliar() {
    return textoAuxiliar;
  }

  /**
   * Sets the texto auxiliar.
   *
   * @param textoAuxiliar the new texto auxiliar
   */
  public void setTextoAuxiliar(final String textoAuxiliar) {
    this.textoAuxiliar = textoAuxiliar;
  }

  /**
   * Gets the comentario administrador.
   *
   * @return comentarioAdministrador
   */
  public String getComentarioAdministrador() {
    return comentarioAdministrador;
  }

  /**
   * Sets the comentario administrador.
   *
   * @param comentarioAdministrador the new comentario administrador
   */
  public void setComentarioAdministrador(final String comentarioAdministrador) {
    this.comentarioAdministrador = comentarioAdministrador;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
