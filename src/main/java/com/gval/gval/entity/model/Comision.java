package com.gval.gval.entity.model;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

// TODO: Auto-generated Javadoc
/**
 * The Class Comision.
 */
@Entity
@Table(name = "SDM_COMISION")
@GenericGenerator(name = "SDM_COMISION_PKCOMISION_GENERATOR", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
		@Parameter(name = "sequence_name", value = "SEC_SDM_COMISION"), @Parameter(name = "initial_value", value = "1"),
		@Parameter(name = "increment_size", value = "1") })
public class Comision implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4584654863118357337L;
	/** The pk acta. */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SDM_COMISION_PKCOMISION_GENERATOR")
	@Column(name = "PK_COMISION", nullable = false)
	private Long pkComision;

	/** The firma 1. */
	@ManyToOne
	@JoinColumn(name = "PK_FIRMA", nullable = false)
	private Firma sdmFirma1;

	/** The firma 2. */
	@ManyToOne
	@JoinColumn(name = "PK_FIRMA2")
	private Firma sdmFirma2;
	/** The firma 3. */
	@ManyToOne
	@JoinColumn(name = "PK_FIRMA3")
	private Firma sdmFirma3;

	/** The firma 4. */
	@ManyToOne
	@JoinColumn(name = "PK_FIRMA4")
	private Firma sdmFirma4;

	/** The firma 5. */
	@ManyToOne
	@JoinColumn(name = "PK_FIRMA5")
	private Firma sdmFirma5;

	/** The conombre. */
	@Column(name = "CONOMBRE", length = 15)
	private String conombre;

	/** The codescripc. */
	@Column(name = "CODESCRIPC", length = 50)
	private String codescripc;

	/** The coactivo. */
	@Column(name = "COACTIVO", precision = 22)
	private Long coactivo;

	/** The cofechcrea. */
	@Temporal(TemporalType.DATE)
	@Column(name = "COFECHCREA")
	private Date cofechcrea;

	/**
	 * Instantiates a new comision.
	 */
	public Comision() {
	}

	/**
	 * Gets the pk comision.
	 *
	 * @return the pk comision
	 */
	public Long getPkComision() {
		return pkComision;
	}

	/**
	 * Sets the pk comision.
	 *
	 * @param pkComision the new pk comision
	 */
	public void setPkComision(final Long pkComision) {
		this.pkComision = pkComision;
	}

	/**
	 * Gets the sdm firma 1.
	 *
	 * @return the sdm firma 1
	 */
	public Firma getSdmFirma1() {
		return sdmFirma1;
	}

	/**
	 * Sets the sdm firma 1.
	 *
	 * @param sdmFirma1 the new sdm firma 1
	 */
	public void setSdmFirma1(final Firma sdmFirma1) {
		this.sdmFirma1 = sdmFirma1;
	}

	/**
	 * Gets the sdm firma 2.
	 *
	 * @return the sdm firma 2
	 */
	public Firma getSdmFirma2() {
		return sdmFirma2;
	}

	/**
	 * Sets the sdm firma 2.
	 *
	 * @param sdmFirma2 the new sdm firma 2
	 */
	public void setSdmFirma2(final Firma sdmFirma2) {
		this.sdmFirma2 = sdmFirma2;
	}

	/**
	 * Gets the sdm firma 3.
	 *
	 * @return the sdm firma 3
	 */
	public Firma getSdmFirma3() {
		return sdmFirma3;
	}

	/**
	 * Sets the sdm firma 3.
	 *
	 * @param sdmFirma3 the new sdm firma 3
	 */
	public void setSdmFirma3(final Firma sdmFirma3) {
		this.sdmFirma3 = sdmFirma3;
	}

	/**
	 * Gets the sdm firma 4.
	 *
	 * @return the sdm firma 4
	 */
	public Firma getSdmFirma4() {
		return sdmFirma4;
	}

	/**
	 * Sets the sdm firma 4.
	 *
	 * @param sdmFirma4 the new sdm firma 4
	 */
	public void setSdmFirma4(final Firma sdmFirma4) {
		this.sdmFirma4 = sdmFirma4;
	}

	/**
	 * Gets the sdm firma 5.
	 *
	 * @return the sdm firma 5
	 */
	public Firma getSdmFirma5() {
		return sdmFirma5;
	}

	/**
	 * Sets the sdm firma 5.
	 *
	 * @param sdmFirma5 the new sdm firma 5
	 */
	public void setSdmFirma5(final Firma sdmFirma5) {
		this.sdmFirma5 = sdmFirma5;
	}

	/**
	 * Gets the conombre.
	 *
	 * @return the conombre
	 */
	public String getConombre() {
		return conombre;
	}

	/**
	 * Sets the conombre.
	 *
	 * @param conombre the new conombre
	 */
	public void setConombre(final String conombre) {
		this.conombre = conombre;
	}

	/**
	 * Gets the codescripc.
	 *
	 * @return the codescripc
	 */
	public String getCodescripc() {
		return codescripc;
	}

	/**
	 * Sets the codescripc.
	 *
	 * @param codescripc the new codescripc
	 */
	public void setCodescripc(final String codescripc) {
		this.codescripc = codescripc;
	}

	/**
	 * Gets the coactivo.
	 *
	 * @return the coactivo
	 */
	public Long getCoactivo() {
		return coactivo;
	}

	/**
	 * Sets the coactivo.
	 *
	 * @param coactivo the new coactivo
	 */
	public void setCoactivo(final Long coactivo) {
		this.coactivo = coactivo;
	}

	/**
	 * Gets the cofechcrea.
	 *
	 * @return the cofechcrea
	 */
	public Date getCofechcrea() {
		return cofechcrea;
	}

	/**
	 * Sets the cofechcrea.
	 *
	 * @param cofechcrea the new cofechcrea
	 */
	public void setCofechcrea(final Date cofechcrea) {
		this.cofechcrea = cofechcrea;
	}




	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

}
