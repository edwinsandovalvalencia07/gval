package com.gval.gval.entity.model;

import com.gval.gval.common.ConstantesCommons;
import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


import jakarta.persistence.*;

/**
 * The persistent class for the SDM_EXPEDIEN database table.
 *
 */
@Entity
@Table(name = "SDM_EXPEDIEN")
@GenericGenerator(name = "SDM_EXPEDIEN_PKEXPEDIEN_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_EXPEDIEN"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class Expediente implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk expedien. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_EXPEDIEN_PKEXPEDIEN_GENERATOR")
  @Column(name = "PK_EXPEDIEN", unique = true, nullable = false)
  private Long pkExpedien;

  /** The activo. */
  @Column(name = "EXACTIVO", nullable = false)
  private Boolean activo;

  /** The ambito. */
  @Column(name = "EXAMBITO", length = 1)
  private String ambito;

  /** The caja. */
  @Column(name = "EXCAJA", length = 15)
  private String caja;

  /** The fallecimiento persona. */
  @Column(name = "EXFALLCONF", nullable = false)
  private Boolean fallecimientoPersona;

  /** The indica fallecimiento. */
  @Column(name = "EXFALLEC", nullable = false)
  private Boolean indicaFallecimiento;

  /** The fecha sale. */
  @Temporal(TemporalType.DATE)
  @Column(name = "EXFECHASALE")
  private Date fechaSale;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "EXFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The fecha fallecimiento. */
  @Temporal(TemporalType.DATE)
  @Column(name = "EXFECHFALL")
  private Date fechaFallecimiento;

  /** The grado nivel vigente. */
  @Column(name = "EXGYNVIGENTE", precision = 38)
  private BigDecimal gradoNivelVigente;

  /** The motivo cancelacion archivo. */
  @Column(name = "EXMOTCANCELACION_ARCH")
  private Long motivoCancelacionArchivo;

  /** The numero expediente. */
  @Column(name = "EXNUMEXP", nullable = false, length = 15)
  private String numeroExpediente;

  /** The resolucion PIA. */
  @Column(name = "EXPIAVIGENTE", precision = 38)
  private BigDecimal resolucionPIA;

  /** The traslado entrantre. */
  @Column(name = "EXTRASLADO", nullable = false, precision = 38)
  private Boolean trasladoEntrante;

  /** The traslado saliente. */
  @Column(name = "EXTRASSALE", nullable = false, precision = 38)
  private Boolean trasladoSaliente;

  /** The fecha traslado saliente. */
  @Temporal(TemporalType.DATE)
  @Column(name = "FECHAEFECTRASLADOSALIENTE")
  private Date fechaTrasladoSaliente;

  /** The fecha solicitud traslado saliente. */
  @Temporal(TemporalType.DATE)
  @Column(name = "FECHASOLICITUDTRASLADOSALIENTE")
  private Date fechaSolicitudTrasladoSaliente;

  /** The solicitante sidep. */
  @Column(name = "IDSOLICITANTE_SIDEP", precision = 10)
  private BigDecimal solicitanteSidep;

  /** The padron verificado. */
  @Column(name = "EXPADRON_VERIFICADO", length = 1)
  private Boolean padronVerificado;

  /** The entidad dependencia saliente. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_ENTIDEPE")
  private EntidadDependencia entidadDependenciaSaliente;

  /** The comunidad autonoma entrante. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_COMAUTO")
  private ComunidadAutonoma comunidadAutonomaEntrante;

  /** The comunidad autonoma saliente. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_COMAUTO2")
  private ComunidadAutonoma comunidadAutonomaSaliente;

  /** The usuario. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_PERSONA2", nullable = false)
  private Usuario usuario;

  /** The solicitante. */
  // bi-directional one-to-one association to SdmPersona
  @OneToOne
  @JoinColumn(name = "PK_PERSONA", nullable = false)
  private Persona solicitante;

  /** The app origen. */
  @Column(name = "APP_ORIGEN")
  private String appOrigen;

  /** The grado. */
  @Formula(value = "ULTIMO_GRADO_EXPEDIENTE(PK_EXPEDIEN)")
  private Integer grado;

  /** The unidad. */
  @Formula(value = "UNIDAD_EXPEDIENTE(PK_EXPEDIEN)")
  private Long unidad;

  /** The servicio identidad pai consultado. */
  @Column(name = "EXINCIIDENTIDADPAI", precision = 1)
  private Boolean servicioIdentidadPaiConsultado;

  /** The servicio identidad imserso consultado. */
  @Column(name = "EXINCIIDENTIDADIMSERSO", precision = 1)
  private Boolean servicioIdentidadImsersoConsultado;

  /** The servicio residencia pai consultado. */
  @Column(name = "EXINCIRESIDENCIAPAI", precision = 1)
  private Boolean servicioResidenciaPaiConsultado;

  /** The servicio prestacion pai consultado. */
  @Column(name = "EXINCIPRESTACIPAI", precision = 1)
  private Boolean servicioPrestacionPaiConsultado;

  /**
   * Instantiates a new expediente.
   */

  public Expediente() {
    super();
  }

  /**
   * On pre persist.
   */
  @PrePersist
  public void onPrePersist() {
    appOrigen = ConstantesCommons.APLICACION_ADA3;
  }

  /**
   * Gets the pk expedien.
   *
   * @return the pk expedien
   */
  public Long getPkExpedien() {
    return pkExpedien;
  }

  /**
   * Sets the pk expedien.
   *
   * @param pkExpedien the new pk expedien
   */
  public void setPkExpedien(final Long pkExpedien) {
    this.pkExpedien = pkExpedien;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the ambito.
   *
   * @return the ambito
   */
  public String getAmbito() {
    return ambito;
  }

  /**
   * Sets the ambito.
   *
   * @param ambito the new ambito
   */
  public void setAmbito(final String ambito) {
    this.ambito = ambito;
  }

  /**
   * Gets the caja.
   *
   * @return the caja
   */
  public String getCaja() {
    return caja;
  }

  /**
   * Sets the caja.
   *
   * @param caja the new caja
   */
  public void setCaja(final String caja) {
    this.caja = caja;
  }

  /**
   * Gets the fallecimiento persona.
   *
   * @return the fallecimiento persona
   */
  public Boolean getFallecimientoPersona() {
    return fallecimientoPersona;
  }

  /**
   * Sets the fallecimiento persona.
   *
   * @param fallecimientoPersona the new fallecimiento persona
   */
  public void setFallecimientoPersona(final Boolean fallecimientoPersona) {
    this.fallecimientoPersona = fallecimientoPersona;
  }

  /**
   * Gets the indica fallecimiento.
   *
   * @return the indica fallecimiento
   */
  public Boolean getIndicaFallecimiento() {
    return indicaFallecimiento;
  }

  /**
   * Sets the indica fallecimiento.
   *
   * @param indicaFallecimiento the new indica fallecimiento
   */
  public void setIndicaFallecimiento(final Boolean indicaFallecimiento) {
    this.indicaFallecimiento = indicaFallecimiento;
  }

  /**
   * Gets the fecha sale.
   *
   * @return the fecha sale
   */
  public Date getFechaSale() {
    return UtilidadesCommons.cloneDate(fechaSale);
  }

  /**
   * Sets the fecha sale.
   *
   * @param fechaSale the new fecha sale
   */
  public void setFechaSale(final Date fechaSale) {
    this.fechaSale = UtilidadesCommons.cloneDate(fechaSale);
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha fallecimiento.
   *
   * @return the fecha fallecimiento
   */
  public Date getFechaFallecimiento() {
    return UtilidadesCommons.cloneDate(fechaFallecimiento);
  }

  /**
   * Sets the fecha fallecimiento.
   *
   * @param fechaFallecimiento the new fecha fallecimiento
   */
  public void setFechaFallecimiento(final Date fechaFallecimiento) {
    this.fechaFallecimiento = UtilidadesCommons.cloneDate(fechaFallecimiento);
  }

  /**
   * Gets the grado nivel vigente.
   *
   * @return the grado nivel vigente
   */
  public BigDecimal getGradoNivelVigente() {
    return gradoNivelVigente;
  }

  /**
   * Sets the grado nivel vigente.
   *
   * @param gradoNivelVigente the new grado nivel vigente
   */
  public void setGradoNivelVigente(final BigDecimal gradoNivelVigente) {
    this.gradoNivelVigente = gradoNivelVigente;
  }

  /**
   * Gets the motivo cancelacion archivo.
   *
   * @return the motivo cancelacion archivo
   */
  public Long getMotivoCancelacionArchivo() {
    return this.motivoCancelacionArchivo;
  }

  /**
   * Sets the motivo cancelacion archivo.
   *
   * @param motivoCancelacionArchivo the new motivo cancelacion archivo
   */
  public void setMotivoCancelacionArchivo(final Long motivoCancelacionArchivo) {
    this.motivoCancelacionArchivo = motivoCancelacionArchivo;
  }

  /**
   * Gets the numero expediente.
   *
   * @return the numero expediente
   */
  public String getNumeroExpediente() {
    return numeroExpediente;
  }

  /**
   * Sets the numero expediente.
   *
   * @param numeroExpediente the new numero expediente
   */
  public void setNumeroExpediente(final String numeroExpediente) {
    this.numeroExpediente = numeroExpediente;
  }

  /**
   * Gets the resolucion PIA.
   *
   * @return the resolucion PIA
   */
  public BigDecimal getResolucionPIA() {
    return resolucionPIA;
  }

  /**
   * Sets the resolucion PIA.
   *
   * @param resolucionPIA the new resolucion PIA
   */
  public void setResolucionPIA(final BigDecimal resolucionPIA) {
    this.resolucionPIA = resolucionPIA;
  }

  /**
   * Gets the traslado entrante.
   *
   * @return the traslado entrante
   */
  public Boolean getTrasladoEntrante() {
    return trasladoEntrante;
  }

  /**
   * Sets the traslado entrante.
   *
   * @param trasladoEntrante the new traslado entrante
   */
  public void setTrasladoEntrante(final Boolean trasladoEntrante) {
    this.trasladoEntrante = trasladoEntrante;
  }

  /**
   * Gets the traslado saliente.
   *
   * @return the traslado saliente
   */
  public Boolean getTrasladoSaliente() {
    return trasladoSaliente;
  }

  /**
   * Sets the traslado saliente.
   *
   * @param trasladoSaliente the new traslado saliente
   */
  public void setTrasladoSaliente(final Boolean trasladoSaliente) {
    this.trasladoSaliente = trasladoSaliente;
  }

  /**
   * Gets the fecha traslado saliente.
   *
   * @return the fecha traslado saliente
   */
  public Date getFechaTrasladoSaliente() {
    return UtilidadesCommons.cloneDate(fechaTrasladoSaliente);
  }

  /**
   * Sets the fecha traslado saliente.
   *
   * @param fechaTrasladoSaliente the new fecha traslado saliente
   */
  public void setFechaTrasladoSaliente(final Date fechaTrasladoSaliente) {
    this.fechaTrasladoSaliente =
        UtilidadesCommons.cloneDate(fechaTrasladoSaliente);
  }

  /**
   * Gets the fecha solicitud traslado saliente.
   *
   * @return the fecha solicitud traslado saliente
   */
  public Date getFechaSolicitudTrasladoSaliente() {
    return UtilidadesCommons.cloneDate(fechaSolicitudTrasladoSaliente);
  }

  /**
   * Sets the fecha solicitud traslado saliente.
   *
   * @param fechaSolicitudTrasladoSaliente the new fecha solicitud traslado
   *        saliente
   */
  public void setFechaSolicitudTrasladoSaliente(
      final Date fechaSolicitudTrasladoSaliente) {
    this.fechaSolicitudTrasladoSaliente =
        UtilidadesCommons.cloneDate(fechaSolicitudTrasladoSaliente);
  }

  /**
   * Gets the solicitante sidep.
   *
   * @return the solicitante sidep
   */
  public BigDecimal getSolicitanteSidep() {
    return solicitanteSidep;
  }

  /**
   * Sets the solicitante sidep.
   *
   * @param solicitanteSidep the new solicitante sidep
   */
  public void setSolicitanteSidep(final BigDecimal solicitanteSidep) {
    this.solicitanteSidep = solicitanteSidep;
  }

  /**
   * Gets the entidad dependencia saliente.
   *
   * @return the entidad dependencia saliente
   */
  public EntidadDependencia getEntidadDependenciaSaliente() {
    return entidadDependenciaSaliente;
  }

  /**
   * Sets the entidad dependencia saliente.
   *
   * @param entidadDependenciaSaliente the new entidad dependencia saliente
   */
  public void setEntidadDependenciaSaliente(
      final EntidadDependencia entidadDependenciaSaliente) {
    this.entidadDependenciaSaliente = entidadDependenciaSaliente;
  }

  /**
   * Gets the comunidad autonoma entrante.
   *
   * @return the comunidadAutonomaEntrante
   */
  public ComunidadAutonoma getComunidadAutonomaEntrante() {
    return comunidadAutonomaEntrante;
  }

  /**
   * Sets the comunidad autonoma entrante.
   *
   * @param comunidadAutonomaEntrante the comunidadAutonomaEntrante to set
   */
  public void setComunidadAutonomaEntrante(
      final ComunidadAutonoma comunidadAutonomaEntrante) {
    this.comunidadAutonomaEntrante = comunidadAutonomaEntrante;
  }

  /**
   * Gets the comunidad autonoma saliente.
   *
   * @return the comunidadAutonomaSaliente
   */
  public ComunidadAutonoma getComunidadAutonomaSaliente() {
    return comunidadAutonomaSaliente;
  }

  /**
   * Sets the comunidad autonoma saliente.
   *
   * @param comunidadAutonomaSaliente the comunidadAutonomaSaliente to set
   */
  public void setComunidadAutonomaSaliente(
      final ComunidadAutonoma comunidadAutonomaSaliente) {
    this.comunidadAutonomaSaliente = comunidadAutonomaSaliente;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the solicitante.
   *
   * @return the solicitante
   */
  public Persona getSolicitante() {
    return solicitante;
  }

  /**
   * Sets the solicitante.
   *
   * @param solicitante the new solicitante
   */
  public void setSolicitante(final Persona solicitante) {
    this.solicitante = solicitante;
  }

  /**
   * Gets the padron verificado.
   *
   * @return the padron verificado
   */
  public Boolean getPadronVerificado() {
    return padronVerificado;
  }

  /**
   * Sets the padron verificado.
   *
   * @param padronVerificado the new padron verificado
   */
  public void setPadronVerificado(final Boolean padronVerificado) {
    this.padronVerificado = padronVerificado;
  }

  /**
   * Gets the grado.
   *
   * @return the grado
   */
  public Integer getGrado() {
    return grado;
  }

  /**
   * Sets the grado.
   *
   * @param grado the grado to set
   */
  public void setGrado(final Integer grado) {
    this.grado = grado;
  }

  /**
   * Gets the unidad.
   *
   * @return the unidad
   */
  public Long getUnidad() {
    return unidad;
  }

  /**
   * Sets the unidad.
   *
   * @param unidad the unidad to set
   */
  public void setUnidad(final Long unidad) {
    this.unidad = unidad;
  }

  /**
   * Gets the app origen.
   *
   * @return the app origen
   */
  public String getAppOrigen() {
    return appOrigen;
  }

  /**
   * Sets the app origen.
   *
   * @param appOrigen the new app origen
   */
  public void setAppOrigen(final String appOrigen) {
    this.appOrigen = appOrigen;
  }

  /**
   * Gets the servicio identidad pai consultado.
   *
   * @return the servicio identidad pai consultado
   */
  public Boolean getServicioIdentidadPaiConsultado() {
    return servicioIdentidadPaiConsultado;
  }


  /**
   * Sets the servicio identidad pai consultado.
   *
   * @param servicioIdentidadPaiConsultado the new servicio identidad pai
   *        consultado
   */
  public void setServicioIdentidadPaiConsultado(
      final Boolean servicioIdentidadPaiConsultado) {
    this.servicioIdentidadPaiConsultado = servicioIdentidadPaiConsultado;
  }



  /**
   * Gets the servicio identidad imserso consultado.
   *
   * @return the servicio identidad imserso consultado
   */
  public Boolean getServicioIdentidadImsersoConsultado() {
    return servicioIdentidadImsersoConsultado;
  }


  /**
   * Sets the servicio identidad imserso consultado.
   *
   * @param servicioIdentidadImsersoConsultado the new servicio identidad
   *        imserso consultado
   */
  public void setServicioIdentidadImsersoConsultado(
      final Boolean servicioIdentidadImsersoConsultado) {
    this.servicioIdentidadImsersoConsultado =
        servicioIdentidadImsersoConsultado;
  }

  /**
   * Gets the servicio residencia pai consultado.
   *
   * @return the servicio residencia pai consultado
   */
  public Boolean getServicioResidenciaPaiConsultado() {
    return servicioResidenciaPaiConsultado;
  }

  /**
   * Sets the servicio residencia pai consultado.
   *
   * @param servicioResidenciaPaiConsultado the new servicio residencia pai
   *        consultado
   */
  public void setServicioResidenciaPaiConsultado(
      final Boolean servicioResidenciaPaiConsultado) {
    this.servicioResidenciaPaiConsultado = servicioResidenciaPaiConsultado;
  }


  /**
   * Gets the servicio prestacion pai consultado.
   *
   * @return the servicio prestacion pai consultado
   */
  public Boolean getServicioPrestacionPaiConsultado() {
    return servicioPrestacionPaiConsultado;
  }


  /**
   * Sets the servicio prestacion pai consultado.
   *
   * @param servicioPrestacionPaiConsultado the new servicio prestacion pai
   *        consultado
   */
  public void setServicioPrestacionPaiConsultado(
      final Boolean servicioPrestacionPaiConsultado) {
    this.servicioPrestacionPaiConsultado = servicioPrestacionPaiConsultado;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
