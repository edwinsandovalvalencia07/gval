package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;

/**
 * The persistent class for the SDM_TIPORESO database table.
 *
 */
@Entity
@Table(name = "SDM_TIPORESO")
@GenericGenerator(name = "SDM_TIPORESO_PKTIPORESO_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_TIPORESO"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class TipoResolucion implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 8534896320070637218L;

  /** The pk tipo resolución. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_TIPORESO_PKTIPORESO_GENERATOR")
  @Column(name = "PK_TIPORESO", unique = true, nullable = false)
  private String pkTipoResolucion;

  /** The tipo descripción. */
  @Column(name = "TIDESC", nullable = false, length = 400)
  private String descripcion;

  /** The tipo manual. */
  @Column(name = "TIMANUAL", nullable = true, length = 1)
  private Boolean manual;

  /** The tipo activo. */
  @Column(name = "TIACTIVO", nullable = true, length = 1)
  private Boolean activo;

  /** The tipo aporta PDF. */
  @Column(name = "TIAPORTAPDF", nullable = true, length = 1)
  private Boolean aportaPdf;

  /** The codigo. */
  @Column(name = "TICODIGO", nullable = false, length = 15)
  private String codigo;

  /** The tiene PIA. */
  @Column(name = "TIENEPPIA", nullable = true, length = 1)
  private Boolean tienePIA;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "TIFECHCREA", nullable = true, length = 7)
  private Date fechaCreacion;

  /** The grado y nivel. */
  @Column(name = "TIESDEGRADOYNIVEL", length = 1)
  private Boolean esdeGradoyNivel;

  /** The tipo es de PIA. */
  @Column(name = "TIESDEPIA", length = 1)
  private Boolean esdePIA;

  /** The envio BOE. */
  @Column(name = "TIENVIOBOE", length = 1)
  private Boolean envioBOE;

  /** The tipo eliminar. */
  @Column(name = "TIELIMINAR", length = 1)
  private Boolean eliminar;

  /** The tipo nsisaad grado. */
  @Column(name = "NSISAAD_GRADO", length = 1)
  private Boolean nsisaadGrado;

  /** The tipo nsisaad archivado. */
  @Column(name = "NSISAAD_ARCHIVADO", length = 1)
  private Boolean nsisaadArchivado;

  /** The tipo nsisaad cierreg. */
  @Column(name = "NSISAAD_CIERREG", length = 1)
  private Boolean nsisaadCierreg;

  /** The tipo nsisaad pia. */
  @Column(name = "NSISAAD_PIA", length = 1)
  private Boolean nsisaadPIA;

  /** The tipo nsisaad pendiente arc. */
  @Column(name = "NSISAAD_PENDIENTE_ARC", length = 1)
  private Boolean nsisaadPendiente;

  /** The tipo nsisaad comher. */
  @Column(name = "NSISAAD_COMHER", length = 1)
  private Boolean nsisaadComher;

  /** The tipo resoexitus. */
  @Column(name = "TIRESOEXITUS", length = 1)
  private Boolean resoExitus;

  /** The tipo retro. */
  @Column(name = "TIRETRO", length = 1)
  private Boolean retro;

  /** The sec sdv bi tipo tramite. */
  @Column(name = "SEC_SDV_BI_TIPO_TRAMITE", length = 5)
  private Long tramite;

  /** The tipo reso minorada. */
  @Column(name = "TIRESOMINORADA", length = 1)
  private Boolean resoMinorada;

  /** The tipo es ADA3. */
  @Column(name = "TIESADA3", length = 1)
  private Boolean esADA;

  /**
   * Instantiates a new tipo documento.
   */
  public TipoResolucion() {
    super();
  }

  /**
   * Gets the pk tipo resolucion.
   *
   * @return the pk tipo resolucion
   */
  public String getPkTipoResolucion() {
    return pkTipoResolucion;
  }

  /**
   * Sets the pk tipo resolucion.
   *
   * @param pkTipoResolucion the new pk tipo resolucion
   */
  public void setPkTipoResolucion(final String pkTipoResolucion) {
    this.pkTipoResolucion = pkTipoResolucion;
  }


  /**
   * Gets the tidescripcion.
   *
   * @return the tidescripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the tidescripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the timanual.
   *
   * @return the timanual
   */
  public Boolean getManual() {
    return manual;
  }

  /**
   * Sets the timanual.
   *
   * @param manual the new timanual
   */
  public void setManual(final Boolean manual) {
    this.manual = manual;
  }

  /**
   * Gets the tiactivo.
   *
   * @return the tiactivo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the tiactivo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the tiaportapdf.
   *
   * @return the tiaportapdf
   */
  public Boolean getAportaPdf() {
    return aportaPdf;
  }

  /**
   * Sets the tiaportapdf.
   *
   * @param aportapdf the new aporta pdf
   */
  public void setAportaPdf(final Boolean aportapdf) {
    this.aportaPdf = aportapdf;
  }

  /**
   * Gets the ticodigo.
   *
   * @return the ticodigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the ticodigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the nsisaad grado.
   *
   * @return the nsisaad grado
   */
  public Boolean getNsisaadGrado() {
    return nsisaadGrado;
  }

  /**
   * Sets the nsisaad grado.
   *
   * @param nsisaadGrado the new nsisaad grado
   */
  public void setNsisaadGrado(final Boolean nsisaadGrado) {
    this.nsisaadGrado = nsisaadGrado;
  }

  /**
   * Gets the nsisaad archivado.
   *
   * @return the nsisaad archivado
   */
  public Boolean getNsisaadArchivado() {
    return nsisaadArchivado;
  }

  /**
   * Sets the nsisaad archivado.
   *
   * @param nsisaadArchivado the new nsisaad archivado
   */
  public void setNsisaadArchivado(final Boolean nsisaadArchivado) {
    this.nsisaadArchivado = nsisaadArchivado;
  }

  /**
   * Gets the nsisaad cierreg.
   *
   * @return the nsisaad cierreg
   */
  public Boolean getNsisaadCierreg() {
    return nsisaadCierreg;
  }

  /**
   * Sets the nsisaad cierreg.
   *
   * @param nsisaadCierreg the new nsisaad cierreg
   */
  public void setNsisaadCierreg(final Boolean nsisaadCierreg) {
    this.nsisaadCierreg = nsisaadCierreg;
  }

  /**
   * Gets the nsisaad PIA.
   *
   * @return the nsisaad PIA
   */
  public Boolean getNsisaadPIA() {
    return nsisaadPIA;
  }

  /**
   * Sets the nsisaad PIA.
   *
   * @param nsisaadPIA the new nsisaad PIA
   */
  public void setNsisaadPIA(final Boolean nsisaadPIA) {
    this.nsisaadPIA = nsisaadPIA;
  }

  /**
   * Gets the nsisaad pendiente.
   *
   * @return the nsisaad pendiente
   */
  public Boolean getNsisaadPendiente() {
    return nsisaadPendiente;
  }

  /**
   * Sets the nsisaad pendiente.
   *
   * @param nsisaadPendiente the new nsisaad pendiente
   */
  public void setNsisaadPendiente(final Boolean nsisaadPendiente) {
    this.nsisaadPendiente = nsisaadPendiente;
  }

  /**
   * Gets the nsisaad comher.
   *
   * @return the nsisaad comher
   */
  public Boolean getNsisaadComher() {
    return nsisaadComher;
  }

  /**
   * Sets the nsisaad comher.
   *
   * @param nsisaadComher the new nsisaad comher
   */
  public void setNsisaadComher(final Boolean nsisaadComher) {
    this.nsisaadComher = nsisaadComher;
  }

  /**
   * Gets the reso exitus.
   *
   * @return the reso exitus
   */
  public Boolean getResoExitus() {
    return resoExitus;
  }

  /**
   * Sets the reso exitus.
   *
   * @param resoExitus the new reso exitus
   */
  public void setResoExitus(final Boolean resoExitus) {
    this.resoExitus = resoExitus;
  }

  /**
   * Gets the retro.
   *
   * @return the retro
   */
  public Boolean getRetro() {
    return retro;
  }

  /**
   * Sets the retro.
   *
   * @param retro the new retro
   */
  public void setRetro(final Boolean retro) {
    this.retro = retro;
  }

  /**
   * Gets the titramite.
   *
   * @return the titramite
   */
  public Long getTramite() {
    return tramite;
  }

  /**
   * Sets the titramite.
   *
   * @param tramite the new tramite
   */
  public void setTramite(final Long tramite) {
    this.tramite = tramite;
  }

  /**
   * Gets the reso minorada.
   *
   * @return the reso minorada
   */
  public Boolean getResoMinorada() {
    return resoMinorada;
  }

  /**
   * Sets the reso minorada.
   *
   * @param resoMinorada the new reso minorada
   */
  public void setResoMinorada(final Boolean resoMinorada) {
    this.resoMinorada = resoMinorada;
  }

  /**
   * Gets the es ADA.
   *
   * @return the es ADA
   */
  public Boolean getEsADA() {
    return esADA;
  }

  /**
   * Sets the es ADA.
   *
   * @param esADA the new es ADA
   */
  public void setEsADA(final Boolean esADA) {
    this.esADA = esADA;
  }

  /**
   * Gets the tiene PIA.
   *
   * @return the tiene PIA
   */
  public Boolean getTienePIA() {
    return tienePIA;
  }

  /**
   * Sets the tiene PIA.
   *
   * @param tienePIA the new tiene PIA
   */
  public void setTienePIA(final Boolean tienePIA) {
    this.tienePIA = tienePIA;
  }

  /**
   * Gets the tifechcrea.
   *
   * @return the tifechcrea
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the tifechcrea.
   *
   * @param fechcrea the new fecha creacion
   */
  public void setFechaCreacion(final Date fechcrea) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechcrea);
  }

  /**
   * Gets the tiesde gradoy nivel.
   *
   * @return the tiesde gradoy nivel
   */
  public Boolean getEsdeGradoyNivel() {
    return esdeGradoyNivel;
  }

  /**
   * Sets the tiesde gradoy nivel.
   *
   * @param esdeGradoyNivel the new esde gradoy nivel
   */
  public void setEsdeGradoyNivel(final Boolean esdeGradoyNivel) {
    this.esdeGradoyNivel = esdeGradoyNivel;
  }

  /**
   * Gets the tiesde PIA.
   *
   * @return the tiesde PIA
   */
  public Boolean getEsDePIA() {
    return esdePIA;
  }

  /**
   * Sets the tiesde PIA.
   *
   * @param esdePIA the new es de PIA
   */
  public void setEsDePIA(final Boolean esdePIA) {
    this.esdePIA = esdePIA;
  }

  /**
   * Gets the tienvio BOE.
   *
   * @return the tienvio BOE
   */
  public Boolean getEnvioBOE() {
    return envioBOE;
  }

  /**
   * Sets the tienvio BOE.
   *
   * @param envioBOE the new envio BOE
   */
  public void setEnvioBOE(final Boolean envioBOE) {
    this.envioBOE = envioBOE;
  }

  /**
   * Gets the ti eliminar.
   *
   * @return the ti eliminar
   */
  public Boolean getEliminar() {
    return eliminar;
  }

  /**
   * Sets the ti eliminar.
   *
   * @param Eliminar the new eliminar
   */
  public void setEliminar(final Boolean Eliminar) {
    this.eliminar = Eliminar;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
