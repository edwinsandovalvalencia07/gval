package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The Class Perfil.
 */
@Entity
@Table(name = "SDM_PERFIL_CLAU")
@GenericGenerator(name = "SDM_PERFIL_PKPERFIL_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_PERFIL_CLAU"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class Perfil implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1257727334308849931L;

  /** The pk perfil. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_PERFIL_PKPERFIL_GENERATOR")
  @Column(name = "PK_PERFIL", unique = true, nullable = false)
  private Long pkPerfil;

  /** The activo. */
  @Column(name = "PEACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /** The fecha hasta. */
  @Temporal(TemporalType.DATE)
  @Column(name = "PEFECHASTA", nullable = false)
  private Date fechaHasta;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "PEFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The fecha desde. */
  @Temporal(TemporalType.DATE)
  @Column(name = "PEFECHDESD", nullable = false)
  private Date fechaDesde;

  /** The tipo perfil. */
  // bi-directional many-to-one association to SdxTipoperf
  @ManyToOne
  @JoinColumn(name = "PK_TIPOPERF", nullable = false)
  private TipoPerfil tipoPerfil;

  /** The usuario. */
  // bi-directional many-to-one association to SdxUsuario
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA", nullable = false)
  private Usuario usuario;

  /** The pk clau. */
  @Column(name = "PK_CLAU")
  private Long pkClau;

  /**
   * Instantiates a new perfil.
   */
  public Perfil() {
    // Constructor
  }

  /**
   * Instantiates a new perfil.
   *
   * @param pkPerfil the pk perfil
   * @param activo the activo
   * @param fechaHasta the fecha hasta
   * @param fechaCreacion the fecha creacion
   * @param fechaDesde the fecha desde
   * @param tipoPerfil the tipo perfil
   * @param usuario the usuario
   */
  public Perfil(final Long pkPerfil, final Boolean activo,
      final Date fechaHasta, final Date fechaCreacion, final Date fechaDesde,
      final TipoPerfil tipoPerfil, final Usuario usuario) {
    super();
    this.pkPerfil = pkPerfil;
    this.activo = activo;
    this.fechaHasta = fechaHasta;
    this.fechaCreacion = fechaCreacion;
    this.fechaDesde = fechaDesde;
    this.tipoPerfil = tipoPerfil;
    this.usuario = usuario;
  }


  /**
   * Gets the pk perfil.
   *
   * @return the pk perfil
   */
  public Long getPkPerfil() {
    return pkPerfil;
  }

  /**
   * Sets the pk perfil.
   *
   * @param pkPerfil the new pk perfil
   */
  public void setPkPerfil(final Long pkPerfil) {
    this.pkPerfil = pkPerfil;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha hasta.
   *
   * @return the fecha hasta
   */
  public Date getFechaHasta() {
    return (fechaHasta == null) ? null : (Date) fechaHasta.clone();
  }

  /**
   * Sets the fecha hasta.
   *
   * @param fechaHasta the new fecha hasta
   */
  public void setFechaHasta(final Date fechaHasta) {
    this.fechaHasta = (fechaHasta == null) ? null : (Date) fechaHasta.clone();
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return (fechaCreacion == null) ? null : (Date) fechaCreacion.clone();
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion =
        (fechaCreacion == null) ? null : (Date) fechaCreacion.clone();
  }

  /**
   * Gets the fecha desde.
   *
   * @return the fecha desde
   */
  public Date getFechaDesde() {
    return (fechaDesde == null) ? null : (Date) fechaDesde.clone();
  }

  /**
   * Sets the fecha desde.
   *
   * @param fechaDesde the new fecha desde
   */
  public void setFechaDesde(final Date fechaDesde) {
    this.fechaDesde = (fechaDesde == null) ? null : (Date) fechaDesde.clone();
  }

  /**
   * Gets the tipo perfil.
   *
   * @return the tipo perfil
   */
  public TipoPerfil getTipoPerfil() {
    return tipoPerfil;
  }

  /**
   * Sets the tipo perfil.
   *
   * @param tipoPerfil the new tipo perfil
   */
  public void setTipoPerfil(final TipoPerfil tipoPerfil) {
    this.tipoPerfil = tipoPerfil;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the pk clau.
   *
   * @return the pk clau
   */
  public Long getPkClau() {
    return pkClau;
  }

  /**
   * Sets the pk clau.
   *
   * @param pkClau the new pk clau
   */
  public void setPkClau(final Long pkClau) {
    this.pkClau = pkClau;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
