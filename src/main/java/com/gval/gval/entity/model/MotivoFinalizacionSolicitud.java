package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The persistent class for the SDM_MOTFINALIZSOLICIT database table.
 * 
 */
@Entity
@Table(name = "SDM_MOTFINALIZSOLICIT")
@GenericGenerator(name = "SDM_MOTFINALIZSOLICIT_PK_MOTFINALIZSOLICIT_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_MOTFINALIZSOLICIT"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class MotivoFinalizacionSolicitud implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk motivo finalizacion solicitud. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_MOTFINALIZSOLICIT_PK_MOTFINALIZSOLICIT_GENERATOR")
  @Column(name = "PK_MOTFINALIZSOLICIT", unique = true, nullable = false)
  private Long pkMotivoFinalizacionSolicitud;

  /** The activo. */
  @Column(name = "MOACTIVO", nullable = false)
  private Boolean activo;

  /** The fecha finalizacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "MOFECHA", nullable = false)
  private Date fechaFinalizacion;

  /** The observaciones. */
  @Column(name = "MOOBSERVACIONES", length = 2000)
  private String observaciones;

  /** The solicitud. */
  // bi-directional many-to-one association to SdmSolicit
  @ManyToOne
  @JoinColumn(name = "PK_SOLICIT", nullable = false)
  private Solicitud solicitud;

  /** The motivoFinalizacion. */
  // bi-directional many-to-one association to SdxMotivosfinalizacion
  @ManyToOne
  @JoinColumn(name = "PK_MOTIVOSFINALIZACION", nullable = false)
  private MotivoFinalizacion motivoFinalizacion;

  /**
   * Gets the pk motivo finalizacion solicitud.
   *
   * @return the pk motivo finalizacion solicitud
   */
  public Long getPkMotivoFinalizacionSolicitud() {
    return pkMotivoFinalizacionSolicitud;
  }

  /**
   * Sets the pk motivo finalizacion solicitud.
   *
   * @param pkMotivoFinalizacionSolicitud the new pk motivo finalizacion
   *        solicitud
   */
  public void setPkMotivoFinalizacionSolicitud(
      final Long pkMotivoFinalizacionSolicitud) {
    this.pkMotivoFinalizacionSolicitud = pkMotivoFinalizacionSolicitud;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha finalizacion.
   *
   * @return the fecha finalizacion
   */
  public Date getFechaFinalizacion() {
    return UtilidadesCommons.cloneDate(fechaFinalizacion);
  }

  /**
   * Sets the fecha finalizacion.
   *
   * @param fechaFinalizacion the new fecha finalizacion
   */
  public void setFechaFinalizacion(final Date fechaFinalizacion) {
    this.fechaFinalizacion = UtilidadesCommons.cloneDate(fechaFinalizacion);
  }

  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return observaciones;
  }

  /**
   * Sets the observaciones.
   *
   * @param observaciones the new observaciones
   */
  public void setObservaciones(final String observaciones) {
    this.observaciones = observaciones;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public Solicitud getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the new solicitud
   */
  public void setSolicitud(final Solicitud solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the motivo finalizacion.
   *
   * @return the motivo finalizacion
   */
  public MotivoFinalizacion getMotivoFinalizacion() {
    return motivoFinalizacion;
  }

  /**
   * Sets the motivo finalizacion.
   *
   * @param motivoFinalizacion the new motivo finalizacion
   */
  public void setMotivoFinalizacion(
      final MotivoFinalizacion motivoFinalizacion) {
    this.motivoFinalizacion = motivoFinalizacion;
  }

}
