package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The persistent class for the SDM_CAPAECON database table.
 *
 */
@Entity
@Table(name = "SDM_CAPAECON")
@GenericGenerator(name = "SDM_CAPAECON_PKCAPAECON_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_CAPAECON"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public final class CapacidadEconomica implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 6417699529725157442L;

  /** The pk capaecon. */
  @Id
  @Column(name = "PK_CAPAECON", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_CAPAECON_PKCAPAECON_GENERATOR")
  private Long pkCapaecon;

  /** The capacidad economica familiar. */
  @Column(name = "CACAECUF", precision = 15, scale = 2)
  private BigDecimal capacidadEconomicaFamiliar;

  /** The capacidad economica. */
  @Column(name = "CACAPAECON", precision = 15, scale = 2)
  private BigDecimal capacidadEconomica;

  /** The coodigo identificacion. */
  @Column(name = "CACODIGOID", length = 1)
  private String coodigoIdentificacion;

  /** The codigo respuesta. */
  @Column(name = "CACODIRESP", length = 4)
  private String codigoRespuesta;

  /** The extras. */
  @Column(name = "CAEXTRAS", precision = 15, scale = 2)
  private BigDecimal extras;

  /** The fecha peticion aeat. */
  @Temporal(TemporalType.DATE)
  @Column(name = "CAFEPEAEAT")
  private Date fechaPeticionAeat;

  /** The fecha peticion inss. */
  @Temporal(TemporalType.DATE)
  @Column(name = "CAFEPEINSS")
  private Date fechaPeticionInss;

  /** The fecha importacion fichero intercambio aeat. */
  @Temporal(TemporalType.DATE)
  @Column(name = "CAFEREAEAT")
  private Date fechaImportacionFicheroIntercambioAeat;

  /** The fecha importacion fichero inss. */
  @Temporal(TemporalType.DATE)
  @Column(name = "CAFEREINSS")
  private Date fechaImportacionFicheroInss;

  /** The migrada. */
  @Column(name = "CAMIGRADA")
  private Boolean migrada;

  /** The nifpeticionario. */
  @Column(name = "CANIFPETICIONARIO", length = 15)
  private String nifpeticionario;

  /** The origen datos. */
  @Column(name = "CAORIDATOS", length = 1)
  private String origenDatos;

  /** The nombre peticionario. */
  @Column(name = "CAPETICIONARIO", length = 100)
  private String nombrePeticionario;

  /** The problemas. */
  @Column(name = "CAPROBLEMAS", nullable = false)
  private BigDecimal problemas;

  /** The referencia identificador capacidad economica. */
  @Column(name = "CAREFECAEC")
  private BigDecimal referenciaIdentificadorCapacidadEconomica;

  /** The suma complementos. */
  @Column(name = "CASUMACOMP", precision = 15, scale = 2)
  private BigDecimal sumaComplementos;

  /** The catipo capacidad. */
  @Column(name = "CATIPO_CAPACIDAD", nullable = false, length = 1)
  private String tipoCapacidad;

  /** The tipo contribuyente. */
  @Column(name = "CATIPOCONT", length = 3)
  private String tipoContribuyente;

  /** The tipo declaracion. */
  @Column(name = "CATIPODECL", length = 1)
  private String tipoDeclaracion;

  /** The validada. */
  @Column(name = "CAVALIDADA")
  private Boolean validada;

  /** The validado unidad familiar. */
  @Column(name = "CAVALIUF")
  private Boolean validadoUnidadFamiliar;

  /** The persona. */
  // bi-directional many-to-one association to SdmPersona
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA", nullable = false)
  private Persona persona;

  /** The ejercicio. */
  // bi-directional many-to-one association to SdxEjercici
  @ManyToOne
  @JoinColumn(name = "CLAVE")
  private Ejercicio ejercicio;

  /**
   * Instantiates a new capacidad economica.
   */
  public CapacidadEconomica() {
    // Constructor
  }

  /**
   * Gets the pk capaecon.
   *
   * @return the pkCapaecon
   */
  public Long getPkCapaecon() {
    return pkCapaecon;
  }

  /**
   * Sets the pk capaecon.
   *
   * @param pkCapaecon the pkCapaecon to set
   */
  public void setPkCapaecon(final Long pkCapaecon) {
    this.pkCapaecon = pkCapaecon;
  }

  /**
   * Gets the capacidad economica familiar.
   *
   * @return the capacidadEconomicaFamiliar
   */
  public BigDecimal getCapacidadEconomicaFamiliar() {
    return capacidadEconomicaFamiliar;
  }

  /**
   * Sets the capacidad economica familiar.
   *
   * @param capacidadEconomicaFamiliar the capacidadEconomicaFamiliar to set
   */
  public void setCapacidadEconomicaFamiliar(
      final BigDecimal capacidadEconomicaFamiliar) {
    this.capacidadEconomicaFamiliar = capacidadEconomicaFamiliar;
  }

  /**
   * Gets the capacidad economica.
   *
   * @return the capacidadEconomica
   */
  public BigDecimal getCapacidadEconomica() {
    return capacidadEconomica;
  }

  /**
   * Sets the capacidad economica.
   *
   * @param capacidadEconomica the capacidadEconomica to set
   */
  public void setCapacidadEconomica(final BigDecimal capacidadEconomica) {
    this.capacidadEconomica = capacidadEconomica;
  }

  /**
   * Gets the coodigo identificacion.
   *
   * @return the coodigoIdentificacion
   */
  public String getCoodigoIdentificacion() {
    return coodigoIdentificacion;
  }

  /**
   * Sets the coodigo identificacion.
   *
   * @param coodigoIdentificacion the coodigoIdentificacion to set
   */
  public void setCoodigoIdentificacion(final String coodigoIdentificacion) {
    this.coodigoIdentificacion = coodigoIdentificacion;
  }

  /**
   * Gets the codigo respuesta.
   *
   * @return the codigoRespuesta
   */
  public String getCodigoRespuesta() {
    return codigoRespuesta;
  }

  /**
   * Sets the codigo respuesta.
   *
   * @param codigoRespuesta the codigoRespuesta to set
   */
  public void setCodigoRespuesta(final String codigoRespuesta) {
    this.codigoRespuesta = codigoRespuesta;
  }

  /**
   * Gets the extras.
   *
   * @return the extras
   */
  public BigDecimal getExtras() {
    return extras;
  }

  /**
   * Sets the extras.
   *
   * @param extras the extras to set
   */
  public void setExtras(final BigDecimal extras) {
    this.extras = extras;
  }

  /**
   * Gets the fecha peticion aeat.
   *
   * @return the fechaPeticionAeat
   */
  public Date getFechaPeticionAeat() {
    return fechaPeticionAeat;
  }

  /**
   * Sets the fecha peticion aeat.
   *
   * @param fechaPeticionAeat the fechaPeticionAeat to set
   */
  public void setFechaPeticionAeat(final Date fechaPeticionAeat) {
    this.fechaPeticionAeat = fechaPeticionAeat;
  }

  /**
   * Gets the fecha peticion inss.
   *
   * @return the fechaPeticionInss
   */
  public Date getFechaPeticionInss() {
    return fechaPeticionInss;
  }

  /**
   * Sets the fecha peticion inss.
   *
   * @param fechaPeticionInss the fechaPeticionInss to set
   */
  public void setFechaPeticionInss(final Date fechaPeticionInss) {
    this.fechaPeticionInss = fechaPeticionInss;
  }

  /**
   * Gets the fecha importacion fichero intercambio aeat.
   *
   * @return the fechaImportacionFicheroIntercambioAeat
   */
  public Date getFechaImportacionFicheroIntercambioAeat() {
    return fechaImportacionFicheroIntercambioAeat;
  }

  /**
   * Sets the fecha importacion fichero intercambio aeat.
   *
   * @param fechaImportacionFicheroIntercambioAeat the
   *        fechaImportacionFicheroIntercambioAeat to set
   */
  public void setFechaImportacionFicheroIntercambioAeat(
      final Date fechaImportacionFicheroIntercambioAeat) {
    this.fechaImportacionFicheroIntercambioAeat =
        fechaImportacionFicheroIntercambioAeat;
  }

  /**
   * Gets the fecha importacion fichero inss.
   *
   * @return the fechaImportacionFicheroInss
   */
  public Date getFechaImportacionFicheroInss() {
    return fechaImportacionFicheroInss;
  }

  /**
   * Sets the fecha importacion fichero inss.
   *
   * @param fechaImportacionFicheroInss the fechaImportacionFicheroInss to set
   */
  public void setFechaImportacionFicheroInss(
      final Date fechaImportacionFicheroInss) {
    this.fechaImportacionFicheroInss = fechaImportacionFicheroInss;
  }

  /**
   * Gets the migrada.
   *
   * @return the migrada
   */
  public Boolean getMigrada() {
    return migrada;
  }

  /**
   * Sets the migrada.
   *
   * @param migrada the migrada to set
   */
  public void setMigrada(final Boolean migrada) {
    this.migrada = migrada;
  }

  /**
   * Gets the nifpeticionario.
   *
   * @return the nifpeticionario
   */
  public String getNifpeticionario() {
    return nifpeticionario;
  }

  /**
   * Sets the nifpeticionario.
   *
   * @param nifpeticionario the nifpeticionario to set
   */
  public void setNifpeticionario(final String nifpeticionario) {
    this.nifpeticionario = nifpeticionario;
  }

  /**
   * Gets the origen datos.
   *
   * @return the origenDatos
   */
  public String getOrigenDatos() {
    return origenDatos;
  }

  /**
   * Sets the origen datos.
   *
   * @param origenDatos the origenDatos to set
   */
  public void setOrigenDatos(final String origenDatos) {
    this.origenDatos = origenDatos;
  }

  /**
   * Gets the nombre peticionario.
   *
   * @return the nombrePeticionario
   */
  public String getNombrePeticionario() {
    return nombrePeticionario;
  }

  /**
   * Sets the nombre peticionario.
   *
   * @param nombrePeticionario the nombrePeticionario to set
   */
  public void setNombrePeticionario(final String nombrePeticionario) {
    this.nombrePeticionario = nombrePeticionario;
  }

  /**
   * Gets the problemas.
   *
   * @return the problemas
   */
  public BigDecimal getProblemas() {
    return problemas;
  }

  /**
   * Sets the problemas.
   *
   * @param problemas the problemas to set
   */
  public void setProblemas(final BigDecimal problemas) {
    this.problemas = problemas;
  }

  /**
   * Gets the referencia identificador capacidad economica.
   *
   * @return the referenciaIdentificadorCapacidadEconomica
   */
  public BigDecimal getReferenciaIdentificadorCapacidadEconomica() {
    return referenciaIdentificadorCapacidadEconomica;
  }

  /**
   * Sets the referencia identificador capacidad economica.
   *
   * @param referenciaIdentificadorCapacidadEconomica the
   *        referenciaIdentificadorCapacidadEconomica to set
   */
  public void setReferenciaIdentificadorCapacidadEconomica(
      final BigDecimal referenciaIdentificadorCapacidadEconomica) {
    this.referenciaIdentificadorCapacidadEconomica =
        referenciaIdentificadorCapacidadEconomica;
  }

  /**
   * Gets the suma complementos.
   *
   * @return the sumaComplementos
   */
  public BigDecimal getSumaComplementos() {
    return sumaComplementos;
  }

  /**
   * Sets the suma complementos.
   *
   * @param sumaComplementos the sumaComplementos to set
   */
  public void setSumaComplementos(final BigDecimal sumaComplementos) {
    this.sumaComplementos = sumaComplementos;
  }

  /**
   * Gets the catipo capacidad.
   *
   * @return the tipoCapacidad
   */
  public String getTipoCapacidad() {
    return tipoCapacidad;
  }

  /**
   * Sets the catipo capacidad.
   *
   * @param catipoCapacidad the new tipo capacidad
   */
  public void setTipoCapacidad(final String catipoCapacidad) {
    this.tipoCapacidad = catipoCapacidad;
  }

  /**
   * Gets the tipo contribuyente.
   *
   * @return the tipoContribuyente
   */
  public String getTipoContribuyente() {
    return tipoContribuyente;
  }

  /**
   * Sets the tipo contribuyente.
   *
   * @param tipoContribuyente the tipoContribuyente to set
   */
  public void setTipoContribuyente(final String tipoContribuyente) {
    this.tipoContribuyente = tipoContribuyente;
  }

  /**
   * Gets the tipo declaracion.
   *
   * @return the tipoDeclaracion
   */
  public String getTipoDeclaracion() {
    return tipoDeclaracion;
  }

  /**
   * Sets the tipo declaracion.
   *
   * @param tipoDeclaracion the tipoDeclaracion to set
   */
  public void setTipoDeclaracion(final String tipoDeclaracion) {
    this.tipoDeclaracion = tipoDeclaracion;
  }

  /**
   * Gets the validada.
   *
   * @return the validada
   */
  public Boolean getValidada() {
    return validada;
  }

  /**
   * Sets the validada.
   *
   * @param validada the validada to set
   */
  public void setValidada(final Boolean validada) {
    this.validada = validada;
  }

  /**
   * Gets the validado unidad familiar.
   *
   * @return the validadoUnidadFamiliar
   */
  public Boolean getValidadoUnidadFamiliar() {
    return validadoUnidadFamiliar;
  }

  /**
   * Sets the validado unidad familiar.
   *
   * @param validadoUnidadFamiliar the validadoUnidadFamiliar to set
   */
  public void setValidadoUnidadFamiliar(final Boolean validadoUnidadFamiliar) {
    this.validadoUnidadFamiliar = validadoUnidadFamiliar;
  }

  /**
   * Gets the persona.
   *
   * @return the persona
   */
  public Persona getPersona() {
    return persona;
  }

  /**
   * Sets the persona.
   *
   * @param persona the persona to set
   */
  public void setPersona(final Persona persona) {
    this.persona = persona;
  }

  /**
   * Gets the ejercicio.
   *
   * @return the ejercicio
   */
  public Ejercicio getEjercicio() {
    return ejercicio;
  }

  /**
   * Sets the ejercicio.
   *
   * @param ejercicio the ejercicio to set
   */
  public void setEjercicio(final Ejercicio ejercicio) {
    this.ejercicio = ejercicio;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
