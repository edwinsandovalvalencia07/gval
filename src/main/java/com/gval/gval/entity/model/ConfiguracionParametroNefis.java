package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

import jakarta.persistence.*;

/**
 * The Class ConfiguracionParametrosNefis.
 */
@Entity
@Table(name = "COM_PARAM_NEFIS")
public class ConfiguracionParametroNefis implements Serializable {


  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -2552777996364623999L;

  /** The id. */
  @Id
  @Column(name = "CPAN_ID_PARAM", unique = true, nullable = false)
  private Long idParam;

  /** The codigo Padre. */
  @Column(name = "CPAN_PARAMETRO", nullable = false, length = 30)
  private String parametro;

  /** The codigo. */
  @Column(name = "CPAN_VALOR", nullable = false, length = 100)
  private String valor;

  /** The descripcion. */
  @Column(name = "CPAN_DESCRIPCION", nullable = false, length = 100)
  private String descripcion;

  /** The valor caracter. */
  @Column(name = "CPAN_APLICACION", nullable = false)
  private String aplicacion;

  /** The valor caracter. */
  @Column(name = "CPAN_EDITABLE")
  private String editable;

  /**
   * Gets the id.
   *
   * @return the id
   */
  public Long getIdParam() {
    return idParam;
  }

  /**
   * Sets the id.
   *
   * @param idParam the new id param
   */
  public void setIdParam(final Long idParam) {
    this.idParam = idParam;
  }

  /**
   * Gets the parametro.
   *
   * @return the parametro
   */
  public String getParametro() {
    return parametro;
  }

  /**
   * Sets the parametro.
   *
   * @param parametro the parametro to set
   */
  public void setParametro(final String parametro) {
    this.parametro = parametro;
  }

  /**
   * Gets the valor.
   *
   * @return the valor
   */
  public String getValor() {
    return valor;
  }

  /**
   * Sets the valor.
   *
   * @param valor the valor to set
   */
  public void setValor(final String valor) {
    this.valor = valor;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the descripcion to set
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the aplicacion.
   *
   * @return the aplicacion
   */
  public String getAplicacion() {
    return aplicacion;
  }

  /**
   * Sets the aplicacion.
   *
   * @param aplicacion the aplicacion to set
   */
  public void setAplicacion(final String aplicacion) {
    this.aplicacion = aplicacion;
  }

  /**
   * Gets the editable.
   *
   * @return the editable
   */
  public String getEditable() {
    return editable;
  }

  /**
   * Sets the editable.
   *
   * @param editable the editable to set
   */
  public void setEditable(final String editable) {
    this.editable = editable;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
