package com.gval.gval.entity.model;

import java.io.Serializable;

import jakarta.persistence.*;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "SDM_DIAGDICT")
@GenericGenerator(name = "SDM_DIAGDICT_PKDIAGDICT_GENERATOR", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
		@Parameter(name = "sequence_name", value = "SEC_SDM_DIAGDICT"), @Parameter(name = "initial_value", value = "1"),
		@Parameter(name = "increment_size", value = "1") })
public class Diagdict implements Serializable {

	private static final long serialVersionUID = 3508615926696091499L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SDM_DIAGDICT_PKDIAGDICT_GENERATOR")
	@Column(name = "PK_DIAGDICT", unique = true, nullable = false)
	private Long pkDiagdict;

	/** The nivel. */
	@Column(name = "DINIVEL", length = 1)
	private String nivel;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PK_DICTAMEN")
	private Dictamen dictamen;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PK_DIAGNOST_CIE10")
	private DiagnosticoCie10 diagnosticoCie10;

	public Diagdict() {
		super();
	}

	public Long getPkDiagdict() {
		return pkDiagdict;
	}

	public void setPkDiagdict(Long pkDiagdict) {
		this.pkDiagdict = pkDiagdict;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public Dictamen getDictamen() {
		return dictamen;
	}

	public void setDictamen(Dictamen dictamen) {
		this.dictamen = dictamen;
	}

	public DiagnosticoCie10 getDiagnosticoCie10() {
		return diagnosticoCie10;
	}

	public void setDiagnosticoCie10(DiagnosticoCie10 diagnosticoCie10) {
		this.diagnosticoCie10 = diagnosticoCie10;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

}
