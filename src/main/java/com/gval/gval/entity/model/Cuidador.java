package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;
import jakarta.persistence.*;

/**
 * The persistent class for the SDM_CNP database table.
 *
 */
@Entity
@Table(name = "SDM_CNP")
@GenericGenerator(name = "SDM_CNP_PKCNP_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {@Parameter(name = "sequence_name", value = "SEC_SDM_CNP"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class Cuidador implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -1685604553595396498L;

  /** The pk cuidador. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_CNP_PKCNP_GENERATOR")
  @Column(name = "PK_CNP", unique = true, nullable = false)
  private Long pkCuidador;

  /** The activo. */
  @Column(name = "CNACTIVO", nullable = false)
  private Boolean activo;

  /** The comprom perma. */
  @Column(name = "CNCOMPPERM")
  private Boolean compromisoPermanencia;

  /** The situ convivencia sol. */
  @Column(name = "CNCONVIVE", nullable = false)
  private Boolean conviveConSolicitante;

  /** The tipo denegacion. */
  @Column(name = "CNDENEGA", precision = 38)
  private Long tipoDenegacion;

  /** The fecha denegacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "CNFECHA")
  private Date fechaDenegacion;

  /** The fecha alta conv SS. */
  @Temporal(TemporalType.DATE)
  @Column(name = "CNFECHALSS")
  private Date fechaAltaConvenioSS;

  /** The fecha baja cuidador. */
  @Temporal(TemporalType.DATE)
  @Column(name = "CNFECHBAJA")
  private Date fechaBajaPia;

  /** The fecha baja SS. */
  @Temporal(TemporalType.DATE)
  @Column(name = "CNFECHBASS")
  private Date fechaBajaConvenioSS;

  /** The fecha crea registro. */
  @Temporal(TemporalType.DATE)
  @Column(name = "CNFECHCREA")
  private Date fechaCreacion;

  /** The fecha efecto cuidador. */
  @Temporal(TemporalType.DATE)
  @Column(name = "CNFECHEFECTO")
  private Date fechaEfecto;

  /** The fecha fin. */
  @Temporal(TemporalType.DATE)
  @Column(name = "CNFECHFIN")
  private Date fechaFinCuidados;

  /** The fecha inicio. */
  @Temporal(TemporalType.DATE)
  @Column(name = "CNFECHINIC")
  private Date fechaInicioCuidados;

  /** The comprom formacion. */
  @Column(name = "CNFORMACI")
  private Boolean compromisoFormacion;

  /** The num horas mes. */
  @Column(name = "CNHORASMES")
  private Long horasMesDedicacion;

  /** The intensidad. */
  @Column(name = "CNINTENSID", length = 1)
  private String intensidad;

  /**
   * 1 si jornada completa 0 si es jornada media null si situacion laboral no es
   * cuenta ajena.
   */
  @Column(name = "CNJORNADA")
  private Boolean tipoJornada;

  /** The enero. */
  @Column(name = "CNMES1")
  private Boolean enero;

  /** The octubre. */
  @Column(name = "CNMES10")
  private Boolean octubre;

  /** The noviembre. */
  @Column(name = "CNMES11")
  private Boolean noviembre;

  /** The diciembre. */
  @Column(name = "CNMES12")
  private Boolean diciembre;

  /** The febrero. */
  @Column(name = "CNMES2")
  private Boolean febrero;

  /** The marzo. */
  @Column(name = "CNMES3")
  private Boolean marzo;

  /** The abril. */
  @Column(name = "CNMES4")
  private Boolean abril;

  /** The mayo. */
  @Column(name = "CNMES5")
  private Boolean mayo;

  /** The junio. */
  @Column(name = "CNMES6")
  private Boolean junio;

  /** The julio. */
  @Column(name = "CNMES7")
  private Boolean julio;

  /** The agosto. */
  @Column(name = "CNMES8")
  private Boolean agosto;

  /** The septiembre. */
  @Column(name = "CNMES9")
  private Boolean septiembre;

  /** The Desc motivo denegacion. */
  @Column(name = "CNMOTIVO", length = 250)
  private String motivoDenegacion;

  /** The nuss cuidador. */
  @Column(name = "CNNUSS", length = 50)
  private String nussCuidador;

  /** The suscripcion oblig conv. */
  @Column(name = "CNOBLICONVE")
  private Boolean suscripcionObligatoriaConvenio;

  /** The parientes. */
  @Column(name = "CNPARENTES", nullable = false)
  private Boolean tienenParentesco;

  /** The periodo cuidados. */
  @Column(name = "CNPERIODO")
  /* Indica si el periodo de cuidados es por años (0) o por meses (1) */
  private Long tipoPeriodoCuidados;

  /** The prestacion desempleo. */
  @Column(name = "CNPRESTACI")
  private Boolean prestacionDesempleo;

  /** The otros laboral. */
  @Column(name = "CNSITOTROS", length = 100)
  private String descripcionOtraSituacionLaboral;

  /** The subsidio desempleo. */
  @Column(name = "CNSUBSIDIO")
  private Long tipoSubsidioDesempleo;

  /** The tipo cuidador. */
  @Column(name = "CNTIPOCNP")
  private Long tipoCuidador;

  /** The direccion. */
  // bi-directional many-to-one association to DireccionAda
  @ManyToOne
  @JoinColumn(name = "PK_DIRECCIO")
  private DireccionAda direccion;

  /** The persona. */
  // bi-directional many-to-one association to Persona
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA", nullable = false)
  private Persona persona;

  // bi-directional many-to-one association to SdmPia
  // @ManyToOne
  // @JoinColumn(name = "PK_PIA")
  /** The pk pia. */
  // private SdmPia sdmPia;
  @Column(name = "PK_PIA")
  private Long pkPia;

  /** The Pref catalogo servicio. */
  // bi-directional many-to-one association to PrefCatalogoServicio
  @ManyToOne
  @JoinColumn(name = "PK_PREF_CATASERV")
  private PreferenciaCatalogoServicio preferenciaCatalogoServicio;

  /** The situacion laboral. */
  // bi-directional many-to-one association to SdxSitlabora
  @ManyToOne
  @JoinColumn(name = "PK_SITLABORA")
  private SituacionLaboral situacionLaboral;

  /** The tipo familiar. */
  // bi-directional many-to-one association to SdxTipofami
  @ManyToOne
  @JoinColumn(name = "PK_TIPOFAMI")
  private TipoFamiliar tipoFamiliar;

  /** The vigente. */
  @Formula(value = "esCNPVigente(PK_PERSONA, PK_PREF_CATASERV)")
  private boolean vigente;

  /** The fecha crea registro. */
  @Temporal(TemporalType.DATE)
  @Column(name = "CNFECHMODIFI")
  private Date fechaModificacion;


  /**
   * On pre persist.
   */
  @PrePersist
  public void onPrePersist() {
    fechaCreacion = new Date();
    fechaModificacion = new Date();
  }


  /**
   * On pre update.
   */
  @PreUpdate
  public void onPreUpdate() {
    fechaModificacion = new Date();
  }

  /**
   * Instantiates a new cuidador.
   */
  public Cuidador() {
    // Contructor vacio
  }


  /**
   * Gets the pk cuidador.
   *
   * @return the pk cuidador
   */
  public Long getPkCuidador() {
    return pkCuidador;
  }



  /**
   * Sets the pk cuidador.
   *
   * @param pkCuidador the new pk cuidador
   */
  public void setPkCuidador(final Long pkCuidador) {
    this.pkCuidador = pkCuidador;
  }



  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }



  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }



  /**
   * Gets the compromiso permanencia.
   *
   * @return the compromiso permanencia
   */
  public Boolean getCompromisoPermanencia() {
    return compromisoPermanencia;
  }



  /**
   * Sets the compromiso permanencia.
   *
   * @param compromisoPermanencia the new compromiso permanencia
   */
  public void setCompromisoPermanencia(final Boolean compromisoPermanencia) {
    this.compromisoPermanencia = compromisoPermanencia;
  }



  /**
   * Gets the convive con solicitante.
   *
   * @return the convive con solicitante
   */
  public Boolean getConviveConSolicitante() {
    return conviveConSolicitante;
  }



  /**
   * Sets the convive con solicitante.
   *
   * @param conviveConSolicitante the new convive con solicitante
   */
  public void setConviveConSolicitante(final Boolean conviveConSolicitante) {
    this.conviveConSolicitante = conviveConSolicitante;
  }



  /**
   * Gets the tipo denegacion.
   *
   * @return the tipo denegacion
   */
  public Long getTipoDenegacion() {
    return tipoDenegacion;
  }



  /**
   * Sets the tipo denegacion.
   *
   * @param tipoDenegacion the new tipo denegacion
   */
  public void setTipoDenegacion(final Long tipoDenegacion) {
    this.tipoDenegacion = tipoDenegacion;
  }



  /**
   * Gets the fecha denegacion.
   *
   * @return the fecha denegacion
   */
  public Date getFechaDenegacion() {
    return UtilidadesCommons.cloneDate(fechaDenegacion);
  }



  /**
   * Sets the fecha denegacion.
   *
   * @param fechaDenegacion the new fecha denegacion
   */
  public void setFechaDenegacion(final Date fechaDenegacion) {
    this.fechaDenegacion = UtilidadesCommons.cloneDate(fechaDenegacion);
  }



  /**
   * Gets the fecha alta convenio SS.
   *
   * @return the fecha alta convenio SS
   */
  public Date getFechaAltaConvenioSS() {
    return UtilidadesCommons.cloneDate(fechaAltaConvenioSS);
  }



  /**
   * Sets the fecha alta convenio SS.
   *
   * @param fechaAltaConvenioSS the new fecha alta convenio SS
   */
  public void setFechaAltaConvenioSS(final Date fechaAltaConvenioSS) {
    this.fechaAltaConvenioSS = UtilidadesCommons.cloneDate(fechaAltaConvenioSS);
  }



  /**
   * Gets the fecha baja pia.
   *
   * @return the fecha baja pia
   */
  public Date getFechaBajaPia() {
    return UtilidadesCommons.cloneDate(fechaBajaPia);
  }



  /**
   * Sets the fecha baja pia.
   *
   * @param fechaBajaPia the new fecha baja pia
   */
  public void setFechaBajaPia(final Date fechaBajaPia) {
    this.fechaBajaPia = UtilidadesCommons.cloneDate(fechaBajaPia);
  }



  /**
   * Gets the fecha baja convenio SS.
   *
   * @return the fecha baja convenio SS
   */
  public Date getFechaBajaConvenioSS() {
    return UtilidadesCommons.cloneDate(fechaBajaConvenioSS);
  }



  /**
   * Sets the fecha baja convenio SS.
   *
   * @param fechaBajaConvenioSS the new fecha baja convenio SS
   */
  public void setFechaBajaConvenioSS(final Date fechaBajaConvenioSS) {
    this.fechaBajaConvenioSS = UtilidadesCommons.cloneDate(fechaBajaConvenioSS);
  }



  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }



  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }



  /**
   * Gets the fecha efecto.
   *
   * @return the fecha efecto
   */
  public Date getFechaEfecto() {
    return UtilidadesCommons.cloneDate(fechaEfecto);
  }



  /**
   * Sets the fecha efecto.
   *
   * @param fechaEfecto the new fecha efecto
   */
  public void setFechaEfecto(final Date fechaEfecto) {
    this.fechaEfecto = UtilidadesCommons.cloneDate(fechaEfecto);
  }



  /**
   * Gets the fecha fin cuidados.
   *
   * @return the fecha fin cuidados
   */
  public Date getFechaFinCuidados() {
    return UtilidadesCommons.cloneDate(fechaFinCuidados);
  }



  /**
   * Sets the fecha fin cuidados.
   *
   * @param fechaFinCuidados the new fecha fin cuidados
   */
  public void setFechaFinCuidados(final Date fechaFinCuidados) {
    this.fechaFinCuidados = UtilidadesCommons.cloneDate(fechaFinCuidados);
  }



  /**
   * Gets the fecha inicio cuidados.
   *
   * @return the fecha inicio cuidados
   */
  public Date getFechaInicioCuidados() {
    return UtilidadesCommons.cloneDate(fechaInicioCuidados);
  }



  /**
   * Sets the fecha inicio cuidados.
   *
   * @param fechaInicioCuidados the new fecha inicio cuidados
   */
  public void setFechaInicioCuidados(final Date fechaInicioCuidados) {
    this.fechaInicioCuidados = UtilidadesCommons.cloneDate(fechaInicioCuidados);
  }



  /**
   * Gets the compromiso formacion.
   *
   * @return the compromiso formacion
   */
  public Boolean getCompromisoFormacion() {
    return compromisoFormacion;
  }



  /**
   * Sets the compromiso formacion.
   *
   * @param compromisoFormacion the new compromiso formacion
   */
  public void setCompromisoFormacion(final Boolean compromisoFormacion) {
    this.compromisoFormacion = compromisoFormacion;
  }



  /**
   * Gets the horas mes dedicacion.
   *
   * @return the horas mes dedicacion
   */
  public Long getHorasMesDedicacion() {
    return horasMesDedicacion;
  }



  /**
   * Sets the horas mes dedicacion.
   *
   * @param horasMesDedicacion the new horas mes dedicacion
   */
  public void setHorasMesDedicacion(final Long horasMesDedicacion) {
    this.horasMesDedicacion = horasMesDedicacion;
  }



  /**
   * Gets the intensidad.
   *
   * @return the intensidad
   */
  public String getIntensidad() {
    return intensidad;
  }



  /**
   * Sets the intensidad.
   *
   * @param intensidad the new intensidad
   */
  public void setIntensidad(final String intensidad) {
    this.intensidad = intensidad;
  }



  /**
   * Gets the tipo jornada.
   *
   * @return the tipo jornada
   */
  public Boolean getTipoJornada() {
    return tipoJornada;
  }



  /**
   * Sets the tipo jornada.
   *
   * @param tipoJornada the new tipo jornada
   */
  public void setTipoJornada(final Boolean tipoJornada) {
    this.tipoJornada = tipoJornada;
  }



  /**
   * Gets the enero.
   *
   * @return the enero
   */
  public Boolean getEnero() {
    return enero;
  }



  /**
   * Sets the enero.
   *
   * @param enero the new enero
   */
  public void setEnero(final Boolean enero) {
    this.enero = enero;
  }



  /**
   * Gets the octubre.
   *
   * @return the octubre
   */
  public Boolean getOctubre() {
    return octubre;
  }



  /**
   * Sets the octubre.
   *
   * @param octubre the new octubre
   */
  public void setOctubre(final Boolean octubre) {
    this.octubre = octubre;
  }



  /**
   * Gets the noviembre.
   *
   * @return the noviembre
   */
  public Boolean getNoviembre() {
    return noviembre;
  }



  /**
   * Sets the noviembre.
   *
   * @param noviembre the new noviembre
   */
  public void setNoviembre(final Boolean noviembre) {
    this.noviembre = noviembre;
  }



  /**
   * Gets the diciembre.
   *
   * @return the diciembre
   */
  public Boolean getDiciembre() {
    return diciembre;
  }



  /**
   * Sets the diciembre.
   *
   * @param diciembre the new diciembre
   */
  public void setDiciembre(final Boolean diciembre) {
    this.diciembre = diciembre;
  }



  /**
   * Gets the febrero.
   *
   * @return the febrero
   */
  public Boolean getFebrero() {
    return febrero;
  }



  /**
   * Sets the febrero.
   *
   * @param febrero the new febrero
   */
  public void setFebrero(final Boolean febrero) {
    this.febrero = febrero;
  }



  /**
   * Gets the marzo.
   *
   * @return the marzo
   */
  public Boolean getMarzo() {
    return marzo;
  }



  /**
   * Sets the marzo.
   *
   * @param marzo the new marzo
   */
  public void setMarzo(final Boolean marzo) {
    this.marzo = marzo;
  }



  /**
   * Gets the abril.
   *
   * @return the abril
   */
  public Boolean getAbril() {
    return abril;
  }



  /**
   * Sets the abril.
   *
   * @param abril the new abril
   */
  public void setAbril(final Boolean abril) {
    this.abril = abril;
  }



  /**
   * Gets the mayo.
   *
   * @return the mayo
   */
  public Boolean getMayo() {
    return mayo;
  }



  /**
   * Sets the mayo.
   *
   * @param mayo the new mayo
   */
  public void setMayo(final Boolean mayo) {
    this.mayo = mayo;
  }



  /**
   * Gets the junio.
   *
   * @return the junio
   */
  public Boolean getJunio() {
    return junio;
  }



  /**
   * Sets the junio.
   *
   * @param junio the new junio
   */
  public void setJunio(final Boolean junio) {
    this.junio = junio;
  }



  /**
   * Gets the julio.
   *
   * @return the julio
   */
  public Boolean getJulio() {
    return julio;
  }



  /**
   * Sets the julio.
   *
   * @param julio the new julio
   */
  public void setJulio(final Boolean julio) {
    this.julio = julio;
  }



  /**
   * Gets the agosto.
   *
   * @return the agosto
   */
  public Boolean getAgosto() {
    return agosto;
  }



  /**
   * Sets the agosto.
   *
   * @param agosto the new agosto
   */
  public void setAgosto(final Boolean agosto) {
    this.agosto = agosto;
  }



  /**
   * Gets the septiembre.
   *
   * @return the septiembre
   */
  public Boolean getSeptiembre() {
    return septiembre;
  }



  /**
   * Sets the septiembre.
   *
   * @param septiembre the new septiembre
   */
  public void setSeptiembre(final Boolean septiembre) {
    this.septiembre = septiembre;
  }



  /**
   * Gets the motivo denegacion.
   *
   * @return the motivo denegacion
   */
  public String getMotivoDenegacion() {
    return motivoDenegacion;
  }



  /**
   * Sets the motivo denegacion.
   *
   * @param motivoDenegacion the new motivo denegacion
   */
  public void setMotivoDenegacion(final String motivoDenegacion) {
    this.motivoDenegacion = motivoDenegacion;
  }



  /**
   * Gets the nuss cuidador.
   *
   * @return the nuss cuidador
   */
  public String getNussCuidador() {
    return nussCuidador;
  }



  /**
   * Sets the nuss cuidador.
   *
   * @param nussCuidador the new nuss cuidador
   */
  public void setNussCuidador(final String nussCuidador) {
    this.nussCuidador = nussCuidador;
  }



  /**
   * Gets the suscripcion obligatoria convenio.
   *
   * @return the suscripcion obligatoria convenio
   */
  public Boolean getSuscripcionObligatoriaConvenio() {
    return suscripcionObligatoriaConvenio;
  }



  /**
   * Sets the suscripcion obligatoria convenio.
   *
   * @param suscripcionObligatoriaConvenio the new suscripcion obligatoria
   *        convenio
   */
  public void setSuscripcionObligatoriaConvenio(
      final Boolean suscripcionObligatoriaConvenio) {
    this.suscripcionObligatoriaConvenio = suscripcionObligatoriaConvenio;
  }



  /**
   * Gets the tienen parentesco.
   *
   * @return the tienen parentesco
   */
  public Boolean getTienenParentesco() {
    return tienenParentesco;
  }



  /**
   * Sets the tienen parentesco.
   *
   * @param tienenParentesco the new tienen parentesco
   */
  public void setTienenParentesco(final Boolean tienenParentesco) {
    this.tienenParentesco = tienenParentesco;
  }



  /**
   * Gets the tipo periodo cuidados.
   *
   * @return the tipo periodo cuidados
   */
  public Long getTipoPeriodoCuidados() {
    return tipoPeriodoCuidados;
  }



  /**
   * Sets the tipo periodo cuidados.
   *
   * @param tipoPeriodoCuidados the new tipo periodo cuidados
   */
  public void setTipoPeriodoCuidados(final Long tipoPeriodoCuidados) {
    this.tipoPeriodoCuidados = tipoPeriodoCuidados;
  }



  /**
   * Gets the prestacion desempleo.
   *
   * @return the prestacion desempleo
   */
  public Boolean getPrestacionDesempleo() {
    return prestacionDesempleo;
  }



  /**
   * Sets the prestacion desempleo.
   *
   * @param prestacionDesempleo the new prestacion desempleo
   */
  public void setPrestacionDesempleo(final Boolean prestacionDesempleo) {
    this.prestacionDesempleo = prestacionDesempleo;
  }



  /**
   * Gets the descripcion otra situacion laboral.
   *
   * @return the descripcion otra situacion laboral
   */
  public String getDescripcionOtraSituacionLaboral() {
    return descripcionOtraSituacionLaboral;
  }



  /**
   * Sets the descripcion otra situacion laboral.
   *
   * @param descripcionOtraSituacionLaboral the new descripcion otra situacion
   *        laboral
   */
  public void setDescripcionOtraSituacionLaboral(
      final String descripcionOtraSituacionLaboral) {
    this.descripcionOtraSituacionLaboral = descripcionOtraSituacionLaboral;
  }



  /**
   * Gets the tipo subsidio desempleo.
   *
   * @return the tipo subsidio desempleo
   */
  public Long getTipoSubsidioDesempleo() {
    return tipoSubsidioDesempleo;
  }



  /**
   * Sets the tipo subsidio desempleo.
   *
   * @param tipoSubsidioDesempleo the new tipo subsidio desempleo
   */
  public void setTipoSubsidioDesempleo(final Long tipoSubsidioDesempleo) {
    this.tipoSubsidioDesempleo = tipoSubsidioDesempleo;
  }



  /**
   * Gets the tipo cuidador.
   *
   * @return the tipo cuidador
   */
  public Long getTipoCuidador() {
    return tipoCuidador;
  }



  /**
   * Sets the tipo cuidador.
   *
   * @param tipoCuidador the new tipo cuidador
   */
  public void setTipoCuidador(final Long tipoCuidador) {
    this.tipoCuidador = tipoCuidador;
  }



  /**
   * Gets the direccion.
   *
   * @return the direccion
   */
  public DireccionAda getDireccion() {
    return direccion;
  }



  /**
   * Sets the direccion.
   *
   * @param direccion the new direccion
   */
  public void setDireccion(final DireccionAda direccion) {
    this.direccion = direccion;
  }



  /**
   * Gets the persona.
   *
   * @return the persona
   */
  public Persona getPersona() {
    return persona;
  }



  /**
   * Sets the persona.
   *
   * @param persona the new persona
   */
  public void setPersona(final Persona persona) {
    this.persona = persona;
  }



  /**
   * Gets the preferencia catalogo servicio.
   *
   * @return the preferencia catalogo servicio
   */
  public PreferenciaCatalogoServicio getPreferenciaCatalogoServicio() {
    return preferenciaCatalogoServicio;
  }



  /**
   * Sets the preferencia catalogo servicio.
   *
   * @param preferenciaCatalogoServicio the new preferencia catalogo servicio
   */
  public void setPreferenciaCatalogoServicio(
      final PreferenciaCatalogoServicio preferenciaCatalogoServicio) {
    this.preferenciaCatalogoServicio = preferenciaCatalogoServicio;
  }



  /**
   * Gets the situacion laboral.
   *
   * @return the situacion laboral
   */
  public SituacionLaboral getSituacionLaboral() {
    return situacionLaboral;
  }



  /**
   * Sets the situacion laboral.
   *
   * @param situacionLaboral the new situacion laboral
   */
  public void setSituacionLaboral(final SituacionLaboral situacionLaboral) {
    this.situacionLaboral = situacionLaboral;
  }



  /**
   * Gets the tipo familiar.
   *
   * @return the tipo familiar
   */
  public TipoFamiliar getTipoFamiliar() {
    return tipoFamiliar;
  }



  /**
   * Sets the tipo familiar.
   *
   * @param tipoFamiliar the new tipo familiar
   */
  public void setTipoFamiliar(final TipoFamiliar tipoFamiliar) {
    this.tipoFamiliar = tipoFamiliar;
  }


  /**
   * Checks if is vigente.
   *
   * @return true, if is vigente
   */
  public boolean isVigente() {
    return vigente;
  }


  /**
   * Sets the vigente. No se utiliza ya que es un campo calculado
   *
   * @param vigente the new vigente
   */
  public void setVigente(final boolean vigente) {
    this.vigente = vigente;
  }

  /**
   * Gets the pk pia.
   *
   * @return the pk pia
   */
  public Long getPkPia() {
    return pkPia;
  }

  /**
   * Sets the pk pia.
   *
   * @param pkPia the new pk pia
   */
  public void setPkPia(final Long pkPia) {
    this.pkPia = pkPia;
  }


  /**
   * Gets the fecha modificacion.
   *
   * @return the fecha modificacion
   */
  public Date getFechaModificacion() {
    return UtilidadesCommons.cloneDate(fechaModificacion);
  }

  /**
   * Sets the fecha modificacion.
   *
   * @param fechaModificacion the new fecha modificacion
   */
  public void setFechaModificacion(final Date fechaModificacion) {
    this.fechaModificacion = UtilidadesCommons.cloneDate(fechaModificacion);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
