package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The persistent class for the SDX_PROVINCI database table.
 *
 */
@Entity
@Table(name = "SDX_PROVINCI")
@GenericGenerator(name = "SDM_PROVINCI_PKPROVINCI_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDX_PROVINCI"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class Provincia implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1937990515154095794L;

  /** The pk provincia. */
  @Id
  @Column(name = "PK_PROVINCI", unique = true, nullable = false)
  private Long pkProvincia;

  /** The activo. */
  @Column(name = "PRACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /** Indica la fecha de creacion del registro. */
  @Temporal(TemporalType.DATE)
  @Column(name = "PRFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** Codigo de la provincia en SISAAD. */
  @Column(name = "PRIMSERSO", length = 2)
  private String imserso;

  /** Nombre de la provincia. */
  @Column(name = "PRNOMBRE", length = 50, nullable = false)
  private String nombre;

  /** The comunidad autonoma. */
  // bi-directional many-to-one association to SdxComauto
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_COMAUTO", nullable = false)
  private ComunidadAutonoma comunidadAutonoma;

  /**
   * Instantiates a new provincia.
   */
  public Provincia() {
    // Constructor
  }

  /**
   * Gets the pk provincia.
   *
   * @return the pk provincia
   */
  public Long getPkProvincia() {
    return pkProvincia;
  }

  /**
   * Sets the pk provincia.
   *
   * @param pkProvincia the new pk provincia
   */
  public void setPkProvincia(final Long pkProvincia) {
    this.pkProvincia = pkProvincia;
  }


  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the indica la fecha de creacion del registro.
   *
   * @return the indica la fecha de creacion del registro
   */
  public Date getFechaCreacion() {
    return fechaCreacion;
  }

  /**
   * Sets the indica la fecha de creacion del registro.
   *
   * @param fechaCreacion the new indica la fecha de creacion del registro
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }


  /**
   * Gets the nombre de la provincia.
   *
   * @return the nombre de la provincia
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre de la provincia.
   *
   * @param nombre the new nombre de la provincia
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }


  /**
   * Gets the codigo de la provincia en SISAAD.
   *
   * @return the codigo de la provincia en SISAAD
   */
  public String getImserso() {
    return imserso;
  }

  /**
   * Sets the codigo de la provincia en SISAAD.
   *
   * @param imserso the new codigo de la provincia en SISAAD
   */
  public void setImserso(final String imserso) {
    this.imserso = imserso;
  }

  /**
   * Gets the comunidad autonoma.
   *
   * @return the comunidad autonoma
   */
  public ComunidadAutonoma getComunidadAutonoma() {
    return comunidadAutonoma;
  }

  /**
   * Sets the comunidad autonoma.
   *
   * @param comunidadAutonoma the new comunidad autonoma
   */
  public void setComunidadAutonoma(final ComunidadAutonoma comunidadAutonoma) {
    this.comunidadAutonoma = comunidadAutonoma;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
