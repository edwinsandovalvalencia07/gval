package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The persistent class for the SDX_UNIDAD database table.
 *
 */
@Entity
@Table(name = "SDX_UNIDAD")
public class Unidad implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 3901211856922256835L;

  /** The pk unidad. */
  @Id
  @Column(name = "PK_UNIDAD", unique = true, nullable = false)
  private Long pkUnidad;

  /** The activo. */
  @Column(name = "UNACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /** The descripcion. */
  @Column(name = "UNDESC", length = 100)
  private String descripcion;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "UNFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The smad. */
  @Column(name = "UNSMAD", nullable = false, precision = 1)
  private Boolean smad;

  /** The direccion. */
  // bi-directional many-to-one association to SdmDireccio
  @ManyToOne
  @JoinColumn(name = "PK_DIRECCIO", nullable = false)
  private DireccionAda direccion;

  /** The zona cobertura. */
  // bi-directional many-to-one association to SdxZonacobe
  @ManyToOne
  @JoinColumn(name = "PK_ZONACOBE")
  private ZonaCobertura zonaCobertura;

  /**
   * Instantiates a new unidad.
   */
  public Unidad() {
    // Empty constructor
  }

  /**
   * Gets the pk unidad.
   *
   * @return the pkUnidad
   */
  public Long getPkUnidad() {
    return pkUnidad;
  }

  /**
   * Sets the pk unidad.
   *
   * @param pkUnidad the pkUnidad to set
   */
  public void setPkUnidad(final Long pkUnidad) {
    this.pkUnidad = pkUnidad;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the descripcion to set
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the smad.
   *
   * @return the smad
   */
  public Boolean getSmad() {
    return smad;
  }

  /**
   * Sets the smad.
   *
   * @param smad the smad to set
   */
  public void setSmad(final Boolean smad) {
    this.smad = smad;
  }

  /**
   * Gets the direccion.
   *
   * @return the direccion
   */
  public DireccionAda getDireccion() {
    return direccion;
  }

  /**
   * Sets the direccion.
   *
   * @param direccion the direccion to set
   */
  public void setDireccion(final DireccionAda direccion) {
    this.direccion = direccion;
  }

  /**
   * Gets the zona cobertura.
   *
   * @return the zonaCobertura
   */
  public ZonaCobertura getZonaCobertura() {
    return zonaCobertura;
  }

  /**
   * Sets the zona cobertura.
   *
   * @param zonaCobertura the zonaCobertura to set
   */
  public void setZonaCobertura(final ZonaCobertura zonaCobertura) {
    this.zonaCobertura = zonaCobertura;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
