package com.gval.gval.entity.model;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import jakarta.persistence.*;



/**
 * The Class AsistentePersonal.
 *
 */
@Entity
@Table(name = "SDM_ASISPERS")
@GenericGenerator(name = "SDM_ASISPERS_PKASISPERS_GENERATOR",
strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
parameters = {
    @Parameter(name = "sequence_name", value = "SEC_SDM_ASISPERS"),
    @Parameter(name = "initial_value", value = "1"),
    @Parameter(name = "increment_size", value = "1")})
public class AsistentePersonal implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk asistente personal. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
  generator = "SDM_ASISPERS_PKASISPERS_GENERATOR")
  @Column(name = "PK_ASISPERS", unique = true, nullable = false)
  private Long pkAsispers;

  /** The activo. */
  @Column(name = "ASACTIVO", nullable = false)
  private Boolean activo;

  /** The cambio situacion dependiente. */
  @Column(name = "ASCAMBSITU", precision = 38)
  private BigDecimal cambioSituacion;

  /** The compromiso formacion. */
  @Column(name = "ASCOMPFORM")
  private Boolean compromisoFormacion;

  /** The tiempo dedicacion. */
  @Column(name = "ASDEDICACI", length = 1)
  private String tiempoDedicacion;

  /** The fecha creacion registro. */
  @Temporal(TemporalType.DATE)
  @Column(name = "ASFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The fecha fin servicio. */
  @Temporal(TemporalType.DATE)
  @Column(name = "ASFECHFIN")
  private Date fechaFin;

  /** The fecha inicio servicio. */
  @Temporal(TemporalType.DATE)
  @Column(name = "ASFECHINIC")
  private Date fechaInicio;

  /** The residencia legal. */
  @Column(name = "ASRESILEGA", precision = 38)
  private BigDecimal residenciaLegal;

  /** The servicio prestado por. */
  @Column(name = "ASTIPOPRES", length = 1)
  private String servicioPrestadoPor;

  /** The direccion. */
  // bi-directional many-to-one association to DireccionAda
  @ManyToOne
  @JoinColumn(name = "PK_DIRECCIO")
  private DireccionAda direccion;

  /** The persona. */
  // bi-directional many-to-one association to Persona
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA", nullable = false)
  private Persona persona;

  /** The pk pia. */
  @Column(name = "PK_PROPPIA")
  private Long pkPia;

  /** The Pref catalogo servicio. */
  // bi-directional many-to-one association to PrefCatalogoServicio
  @ManyToOne
  @JoinColumn(name = "PK_PREF_CATASERV")
  private PreferenciaCatalogoServicio preferenciaCatalogoServicio;

  /** The propuesta pia. */
  @ManyToOne
  @JoinColumn(name = "PK_PROPPIA", insertable = false, updatable = false)
  private PropuestaPia propuestaPia;

  /**
   * The tipoPersona. 1 si es persona juridica/profesional prestadora de
   * servicio 0 si es persona fisica.
   */
  @Column(name = "TIPO_PERSONA")
  private Boolean tipoPersona;

  /** The aporta contrato. */
  @Column(name = "APORTA_CONTRATO")
  private Boolean aportaContrato;

  /** The residente cva. */
  @Column(name = "RESIDENTE_CVA")
  private Boolean residenteCva;

  /** The tiene parentesco. */
  @Column(name = "PARENTESCO")
  private Boolean tieneParentesco;

  /** The tiene certificado. */
  @Column(name = "CERTIFICADO")
  private Boolean tieneCertificado;

  /** The tipo asistencia. */
  @Column(name = "TIPO_ASISTENCIA")
  private Boolean tipoAsistencia;

  /** The tipo asistencia. */
  @Column(name = "TIENE_FORMACION")
  private Boolean tieneFormacion;

  /** The tiene formacion. */
  @ManyToOne
  @JoinColumn(name = "PK_CAT_FORMACION")
  private CatalogoFormacion formacion;

  @Formula(value = "esAPVigente(PK_PERSONA, PK_PREF_CATASERV)")
  private Boolean activoPia;

  /**
   * Instantiates a new asistente personal.
   */
  public AsistentePersonal() {
    // Empty constructor
  }

  /**
   * Gets the pk asistente personal.
   *
   * @return the pk asispers
   */
  public Long getPkAsispers() {
    return this.pkAsispers;
  }

  /**
   * Sets the pk asistente personal.
   *
   * @param pkAsispers the new pk asispers
   */
  public void setPkAsispers(final Long pkAsispers) {
    this.pkAsispers = pkAsispers;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the cambio situacion dependiente.
   *
   * @return the cambioSituacion
   */
  public BigDecimal getCambioSituacion() {
    return this.cambioSituacion;
  }

  /**
   * Sets the cambio situacion dependiente.
   *
   * @param cambioSituacion the new cambio situacion
   */
  public void setCambioSituacion(final BigDecimal cambioSituacion) {
    this.cambioSituacion = cambioSituacion;
  }

  /**
   * Gets the compromiso formacion.
   *
   * @return the compromiso formacion
   */
  public Boolean getCompromisoFormacion() {
    return compromisoFormacion;
  }

  /**
   * Sets the compromiso formacion.
   *
   * @param compromisoFormacion the new compromiso formacion
   */
  public void setCompromisoFormacion(final Boolean compromisoFormacion) {
    this.compromisoFormacion = compromisoFormacion;
  }

  /**
   * Gets the tiempo dedicacion.
   *
   * @return the tiempo dedicacion
   */
  public String getTiempoDedicacion() {
    return this.tiempoDedicacion;
  }

  /**
   * Sets the tiempo dedicacion.
   *
   * @param tiempoDedicacion the new tiempo dedicacion
   */
  public void setTiempoDedicacion(final String tiempoDedicacion) {
    this.tiempoDedicacion = tiempoDedicacion;
  }

  /**
   * Gets the fecha creacion registro.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return this.fechaCreacion;
  }

  /**
   * Sets the fecha creacion registro.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

  /**
   * Gets the fecha fin servicio.
   *
   * @return the fecha fin
   */
  public Date getFechaFin() {
    return this.fechaFin;
  }

  /**
   * Sets the fecha fin servicio.
   *
   * @param fechaFin the new fecha fin
   */
  public void setFechaFin(final Date fechaFin) {
    this.fechaFin = fechaFin;
  }

  /**
   * Gets the fecha inicio servicio.
   *
   * @return the fechaInicio
   */
  public Date getFechaInicio() {
    return this.fechaInicio;
  }

  /**
   * Sets the fecha inicio servicio.
   *
   * @param fechaInicio the new fecha inicio
   */
  public void setFechaInicio(final Date fechaInicio) {
    this.fechaInicio = fechaInicio;
  }

  /**
   * Gets the residencia legal.
   *
   * @return the residencia legal
   */
  public BigDecimal getResidenciaLegal() {
    return this.residenciaLegal;
  }

  /**
   * Sets the residencia legal.
   *
   * @param residenciaLegal the new residencia legal
   */
  public void setResidenciaLegal(final BigDecimal residenciaLegal) {
    this.residenciaLegal = residenciaLegal;
  }

  /**
   * Gets the servicio prestado por.
   *
   * @return the servicio prestado por
   */
  public String getServicioPrestadoPor() {
    return this.servicioPrestadoPor;
  }

  /**
   * Sets the servicio prestado por.
   *
   * @param servicioPrestadoPor the new servicio prestado por
   */
  public void setServicioPrestadoPor(final String servicioPrestadoPor) {
    this.servicioPrestadoPor = servicioPrestadoPor;
  }

  /**
   * Gets the direccion ada.
   *
   * @return the direccion ada
   */
  public DireccionAda getDireccion() {
    return this.direccion;
  }

  /**
   * Sets the direccion ada.
   *
   * @param direccion the new direccion
   */
  public void setDireccion(final DireccionAda direccion) {
    this.direccion = direccion;
  }

  /**
   * Gets the persona ada.
   *
   * @return the persona ada
   */
  public Persona getPersona() {
    return this.persona;
  }

  /**
   * Sets the persona ada.
   *
   * @param persona the new persona
   */
  public void setPersona(final Persona persona) {
    this.persona = persona;
  }

  /**
   * Gets the pk pia.
   *
   * @return the pk pia
   */
  public Long getPkPia() {
    return pkPia;
  }

  /**
   * Sets the pk pia.
   *
   * @param pkPia the new pk pia
   */
  public void setPkPia(final Long pkPia) {
    this.pkPia = pkPia;
  }

  /**
   * Gets the preferencia catalogo servicio.
   *
   * @return the preferencia catalogo servicio
   */
  public PreferenciaCatalogoServicio getPreferenciaCatalogoServicio() {
    return preferenciaCatalogoServicio;
  }

  /**
   * Sets the preferencia catalogo servicio.
   *
   * @param preferenciaCatalogoServicio the new preferencia catalogo servicio
   */
  public void setPreferenciaCatalogoServicio(
      final PreferenciaCatalogoServicio preferenciaCatalogoServicio) {
    this.preferenciaCatalogoServicio = preferenciaCatalogoServicio;
  }

  /**
   * Gets the tipo persona.
   *
   * @return the tipo persona
   */
  public Boolean getTipoPersona() {
    return tipoPersona;
  }

  /**
   * Sets the tipo persona.
   *
   * @param tipoPersona the new tipo persona
   */
  public void setTipoPersona(final Boolean tipoPersona) {
    this.tipoPersona = tipoPersona;
  }

  /**
   * Gets the aporta contrato.
   *
   * @return the aporta contrato
   */
  public Boolean getAportaContrato() {
    return aportaContrato;
  }

  /**
   * Sets the aporta contrato.
   *
   * @param aportaContrato the new aporta contrato
   */
  public void setAportaContrato(final Boolean aportaContrato) {
    this.aportaContrato = aportaContrato;
  }

  /**
   * Gets the residente cva.
   *
   * @return the residente cva
   */
  public Boolean getResidenteCva() {
    return residenteCva;
  }

  /**
   * Sets the residente cva.
   *
   * @param residenteCva the new residente cva
   */
  public void setResidenteCva(final Boolean residenteCva) {
    this.residenteCva = residenteCva;
  }

  /**
   * Gets the tiene parentesco.
   *
   * @return the tiene parentesco
   */
  public Boolean getTieneParentesco() {
    return tieneParentesco;
  }

  /**
   * Sets the tiene parentesco.
   *
   * @param tieneParentesco the new tiene parentesco
   */
  public void setTieneParentesco(final Boolean tieneParentesco) {
    this.tieneParentesco = tieneParentesco;
  }

  /**
   * Gets the tiene certificado.
   *
   * @return the tiene certificado
   */
  public Boolean getTieneCertificado() {
    return tieneCertificado;
  }

  /**
   * Sets the tiene certificado.
   *
   * @param tieneCertificado the new tiene certificado
   */
  public void setTieneCertificado(final Boolean tieneCertificado) {
    this.tieneCertificado = tieneCertificado;
  }

  /**
   * Gets the tipo asistencia.
   *
   * @return the tipo asistencia
   */
  public Boolean getTipoAsistencia() {
    return tipoAsistencia;
  }

  /**
   * Sets the tipo asistencia.
   *
   * @param tipoAsistencia the new tipo asistencia
   */
  public void setTipoAsistencia(final Boolean tipoAsistencia) {
    this.tipoAsistencia = tipoAsistencia;
  }

  /**
   * Gets the tiene formacion.
   *
   * @return the tiene formacion
   */
  public Boolean getTieneFormacion() {
    return tieneFormacion;
  }

  /**
   * Sets the tiene formacion.
   *
   * @param tieneFormacion the new tiene formacion
   */
  public void setTieneFormacion(final Boolean tieneFormacion) {
    this.tieneFormacion = tieneFormacion;
  }

  /**
   * Gets the formacion.
   *
   * @return the formacion
   */
  public CatalogoFormacion getFormacion() {
    return formacion;
  }

  /**
   * Sets the formacion.
   *
   * @param formacion the new formacion
   */
  public void setFormacion(final CatalogoFormacion formacion) {
    this.formacion = formacion;
  }

  /**
   * Gets the propuesta pia.
   *
   * @return the propuesta pia
   */
  public PropuestaPia getPropuestaPia() {
    return propuestaPia;

  }

  /**
   * Sets the propuesta pia.
   *
   * @param propuestaPia the new propuesta pia
   */
  public void setPropuestaPia(final PropuestaPia propuestaPia) {
    this.propuestaPia = propuestaPia;

  }

  public Boolean isActivoPia() {
    return activoPia;
  }

  public void setActivoPia(final Boolean activoPia) {
    this.activoPia = activoPia;
  }

}
