package com.gval.gval.entity.model;


import com.gval.gval.common.ConstantesCommons;
import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


import jakarta.persistence.*;

/**
 * The persistent class for the SDM_SOLICIT database table.
 *
 */
@Entity
@Table(name = "SDM_SOLICIT")
@GenericGenerator(name = "SDM_SOLICIT_PKSOLICIT_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {@Parameter(name = "sequence_name", value = "SEC_SDM_SOLICIT"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class Solicitud implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk solicitud. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_SOLICIT_PKSOLICIT_GENERATOR")
  @Column(name = "PK_SOLICIT", unique = true, nullable = false)
  private Long pkSolicitud;

  /** The codigo solicitud sidep. */
  @Column(name = "CODSOLICITUD_SIDEP", length = 100)
  private String codigoSolicitudSidep;

  /** The estado traslado. */
  @Column(name = "ESTADOTRAS")
  private Long estadoTraslado;

  /** The fecha efectos traslado saliente. */
  @Temporal(TemporalType.DATE)
  @Column(name = "FECHAEFECTRASLADOSALIENTE")
  private Date fechaEfectosTrasladoSaliente;

  /** The fecha revisable. */
  @Temporal(TemporalType.DATE)
  @Column(name = "FECHAREVISABLE")
  private Date fechaRevisable;

  /** The fecha solicitud traslado entrante. */
  @Temporal(TemporalType.DATE)
  @Column(name = "FECHASOLICITUDTRASLADOENTRANTE")
  private Date fechaSolicitudTrasladoEntrante;

  /** The fecha solicitud traslado saliente. */
  @Temporal(TemporalType.DATE)
  @Column(name = "FECHASOLICITUDTRASLADOSALIENTE")
  private Date fechaSolicitudTrasladoSaliente;

  /** The migrada documento sidep. */
  @Column(name = "MIGRADA_DOC_SIDEP", length = 1)
  private String migradaDocumentoSidep;

  /** The grado. */
  @Column(name = "GRADO", length = 1)
  private String grado;

  /** The nivel. */
  @Column(name = "NIVEL", length = 1)
  private String nivel;

  /** The comunidad autonoma entrante. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_COMAUTO")
  private ComunidadAutonoma comunidadAutonomaEntrante;

  /** The comunidad autonoma saliente. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_COMAUTO2")
  private ComunidadAutonoma comunidadAutonomaSaliente;

  /** The entidad dependencia entrante. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_ENTIDEPE_ENT")
  private EntidadDependencia entidadDependenciaEntrante;

  /** The entidad dependencia saliente. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_ENTIDEPE_SAL")
  private EntidadDependencia entidadDependenciaSaliente;

  // TODO: Cambiar a objetos cuando se creen
  /** The pk estado civil. */
  @Column(name = "PK_ESTACIVI")
  private Long pkEstadoCivil;

  /** The pk motivo archivo. */
  @Column(name = "PK_MOTARCHIVO")
  private Long pkMotivoArchivo;


  /** The pk unidad. */
  @Column(name = "PK_UNIDAD")
  private Long pkUnidad;

  /** The activo. */
  @Column(name = "SOACTIVO", nullable = false)
  private Boolean activo;

  /** The ambito solicitud. */
  @Column(name = "SOAMBITO", length = 1)
  private String ambitoSolicitud;

  /** The bloqueada. */
  @Column(name = "SOBLOQUEAD", nullable = false)
  private Boolean bloqueada;

  /** The bloqueo discrepancia. */
  @Column(name = "SOBLOQUEO_DISCREPANCIA", nullable = false)
  private Boolean bloqueoDiscrepancia;

  /** The capacidad economica. */
  @Column(name = "SOCAPAECON", precision = 8, scale = 2)
  private BigDecimal capacidadEconomica;

  /** The codigo. */
  @Column(name = "SOCODIGO", length = 15)
  private String codigo;

  /** The fecha copia. */
  @Temporal(TemporalType.DATE)
  @Column(name = "SOFECCOPIA")
  private Date fechaEfectosTrasladoEntrante;

  /** The fecha anterior reactivacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "SOFECH_ANT_REACT")
  private Date fechaAnteriorReactivacion;

  /** The fecha traslado saliente. */
  @Temporal(TemporalType.DATE)
  @Column(name = "SOFECHASALE")
  private Date fechaTrasladoSaliente;

  /** The fecha creacion. */
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "SOFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The fecha entrega. */
  @Temporal(TemporalType.DATE)
  @Column(name = "SOFECHENTR")
  private Date fechaEntrega;

  /** The fecha registro. */
  @Temporal(TemporalType.DATE)
  @Column(name = "SOFECHREGI")
  private Date fechaRegistro;

  /** The fecha entrada comunidad. */
  @Temporal(TemporalType.DATE)
  @Column(name = "SOFENTCOM")
  private Date fechaEntradaComunidad;

  /** The finalizada. */
  @Column(name = "SOFINALIZADA", nullable = false)
  private Boolean finalizada;

  /** The fecha notificacion GYN. */
  @Temporal(TemporalType.DATE)
  @Column(name = "SOFNOTIGYN")
  private Date fechaNotificacionGYN;

  /** The fecha notificacion PIA. */
  @Temporal(TemporalType.DATE)
  @Column(name = "SOFNOTIPIA")
  private Date fechaNotificacionPIA;

  /** The fecha resolucion GYN. */
  @Temporal(TemporalType.DATE)
  @Column(name = "SOFRESOGYN")
  private Date fechaResolucionGYN;

  /** The fecha resolucion PIA. */
  @Temporal(TemporalType.DATE)
  @Column(name = "SOFRESOPIA")
  private Date fechaResolucionPIA;

  /** The fecha revision GYN. */
  @Temporal(TemporalType.DATE)
  @Column(name = "SOFREVISAGYN")
  private Date fechaRevisionGYN;

  /** The grado traslado. */
  @Column(name = "SOGRADTRAS", length = 1)
  private String gradoTraslado;

  /** The pendiente informe social. */
  @Column(name = "SOINFOSOCI", length = 1, nullable = false)
  private Boolean pendienteInformeSocial;

  /** The pendiente informe salud. */
  @Column(name = "SOINFSALUD", length = 1, nullable = false)
  private Boolean pendienteInformeSalud;

  /** The pendiente informe extendido fisico. */
  @Column(name = "SOINSAEXFI", length = 1, nullable = false)
  private Boolean pendienteInformeExtendidoFisico;

  /** The pendiente informe extendido mental. */
  @Column(name = "SOINSAEXME", length = 1, nullable = false)
  private Boolean pendienteInformeExtendidoMental;

  /** The motivo borrado. */
  @Column(name = "SOMOTIVOIN", length = 250)
  private String motivoBorrado;

  /** The motivo revision. */
  @Column(name = "SOMOTIVORE", length = 500)
  private String motivoRevision;

  /** The nivel traslado. */
  @Column(name = "SONIVETRAS", length = 1)
  private String nivelTraslado;

  /** The numero entidad local. */
  @Column(name = "SONUMEROENTIDADLOCAL", length = 25)
  private String numeroEntidadLocal;

  /** The numero registro. */
  @Column(name = "SONUMREG", length = 20)
  private String numeroRegistro;

  /** The revision oficio. */
  @Column(name = "SOOFICIO", length = 1)
  private Boolean revisionOficio;

  /** The revision oficio es menor. */
  @Column(name = "SOOFICIOESMENOR", length = 1)
  private Boolean revisionOficioEsMenor;

  /** The pendiente documentacion. */
  @Column(name = "SOPENDDOCU", length = 1, nullable = false)
  private Boolean pendienteDocumentacion;

  /** The tipo perfil apertura. */
  @Column(name = "SOPERFILAPERTURA", nullable = false, length = 10)
  private Long tipoPerfilApertura;

  /** The traslado permanente. */
  @Column(name = "SOPERMANENTE", length = 1)
  private Boolean trasladoPermanente;

  /** The reactivacion. */
  @Column(name = "SOREACTIVACION", length = 1, nullable = false)
  private Boolean reactivacion;

  /** The revision. */
  @Column(name = "SOREVISION", length = 1, nullable = false)
  private Boolean revision;

  /** The servicio. */
  @Column(name = "SOSERVICIO", length = 1)
  private Boolean servicio;

  /** The tiene nivel. */
  @Column(name = "SOTIENENIVEL", length = 1, nullable = false)
  private Boolean tieneNivel;

  /** The traslado. */
  @Column(name = "SOTRASLADO", length = 1, nullable = false)
  private Boolean traslado;

  /** The traslado saliente. */
  @Column(name = "SOTRASLADOSALIENTE", length = 1)
  private Boolean trasladoSaliente;

  /** The proceso urgencia validacion. */
  @Column(name = "SOURGEVALI", length = 1, nullable = false)
  private Boolean procesoUrgenciaValidacion;

  /** The pendiente valoracion. */
  @Column(name = "SOVALORACI", length = 1, nullable = false)
  private Boolean pendienteValoracion;

  /** The vive solo. */
  @Column(name = "SOVIVESOLO", length = 1)
  private Boolean viveSolo;

  /** The pendiente envio. */
  @Column(name = "SO_PENDIENTE_ENVIO", length = 1)
  private Boolean pendienteEnvio;

  /** Usuario asociado a la Solicitud *. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_PERSONA")
  private Usuario usuario;

  /** The tipo solicitud. */
  // bi-directional many-to-one association to SdxTiposoli
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_TIPOSOLI")
  private TipoSolicitud tipoSolicitud;

  /** The expediente. */
  // bi-directional many-to-one association to SdmExpedien
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_EXPEDIEN", nullable = false)
  private Expediente expediente;

  /** The provincia 1. */
  // bi-directional many-to-one association to SdxProvinci
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_PROVINCI_ENT")
  private Provincia provinciaEntrante;

  /** The provincia 2. */
  // bi-directional many-to-one association to SdxProvinci
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_PROVINCI_SAL")
  private Provincia provinciaSaliente;

  /** The estado activo. */
  // bi-directional many-to-many association to EstadoSolicitud
  @OneToMany(mappedBy = "solicitud")
  private List<EstadoSolicitud> estados;

  /** The solicitud revisada. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_SOLICIT2")
  private Solicitud solicitudRevisada;

  /** The app origen. */
  @Column(name = "APP_ORIGEN")
  private String appOrigen;


  /** The motivo finalizada. */
  @Formula(value = "obtenerMotivoFinalizada(PK_SOLICIT)")
  private String motivoFinalizada;

  /**
   * Instantiates a new solicitud.
   */
  public Solicitud() {
    // Constructor
  }

  /**
   * On pre persist.
   */
  @PrePersist
  public void onPrePersist() {
    appOrigen = ConstantesCommons.APLICACION_ADA3;
  }

  /**
   * Gets the pk solicitud.
   *
   * @return the pk solicitud
   */
  public Long getPkSolicitud() {
    return pkSolicitud;
  }

  /**
   * Sets the pk solicitud.
   *
   * @param pkSolicitud the new pk solicitud
   */
  public void setPkSolicitud(final Long pkSolicitud) {
    this.pkSolicitud = pkSolicitud;
  }

  /**
   * Gets the codigo solicitud sidep.
   *
   * @return the codigo solicitud sidep
   */
  public String getCodigoSolicitudSidep() {
    return codigoSolicitudSidep;
  }

  /**
   * Sets the codigo solicitud sidep.
   *
   * @param codigoSolicitudSidep the new codigo solicitud sidep
   */
  public void setCodigoSolicitudSidep(final String codigoSolicitudSidep) {
    this.codigoSolicitudSidep = codigoSolicitudSidep;
  }

  /**
   * Gets the estado traslado.
   *
   * @return the estado traslado
   */
  public Long getEstadoTraslado() {
    return estadoTraslado;
  }

  /**
   * Sets the estado traslado.
   *
   * @param estadoTraslado the new estado traslado
   */
  public void setEstadoTraslado(final Long estadoTraslado) {
    this.estadoTraslado = estadoTraslado;
  }

  /**
   * Gets the fecha efectos traslado saliente.
   *
   * @return the fecha efectos traslado saliente
   */
  public Date getFechaEfectosTrasladoSaliente() {
    return UtilidadesCommons.cloneDate(fechaEfectosTrasladoSaliente);
  }

  /**
   * Sets the fecha efectos traslado saliente.
   *
   * @param fechaEfectosTrasladoSaliente the new fecha efectos traslado saliente
   */
  public void setFechaEfectosTrasladoSaliente(
      final Date fechaEfectosTrasladoSaliente) {
    this.fechaEfectosTrasladoSaliente =
        UtilidadesCommons.cloneDate(fechaEfectosTrasladoSaliente);
  }

  /**
   * Gets the fecha revisable.
   *
   * @return the fecha revisable
   */
  public Date getFechaRevisable() {
    return UtilidadesCommons.cloneDate(fechaRevisable);
  }

  /**
   * Sets the fecha revisable.
   *
   * @param fechaRevisable the new fecha revisable
   */
  public void setFechaRevisable(final Date fechaRevisable) {
    this.fechaRevisable = UtilidadesCommons.cloneDate(fechaRevisable);
  }

  /**
   * Gets the fecha solicitud traslado entrante.
   *
   * @return the fecha solicitud traslado entrante
   */
  public Date getFechaSolicitudTrasladoEntrante() {
    return UtilidadesCommons.cloneDate(fechaSolicitudTrasladoEntrante);
  }

  /**
   * Sets the fecha solicitud traslado entrante.
   *
   * @param fechaSolicitudTrasladoEntrante the new fecha solicitud traslado
   *        entrante
   */
  public void setFechaSolicitudTrasladoEntrante(
      final Date fechaSolicitudTrasladoEntrante) {
    this.fechaSolicitudTrasladoEntrante =
        UtilidadesCommons.cloneDate(fechaSolicitudTrasladoEntrante);
  }

  /**
   * Gets the fecha solicitud traslado saliente.
   *
   * @return the fecha solicitud traslado saliente
   */
  public Date getFechaSolicitudTrasladoSaliente() {
    return UtilidadesCommons.cloneDate(fechaSolicitudTrasladoSaliente);
  }

  /**
   * Sets the fecha solicitud traslado saliente.
   *
   * @param fechaSolicitudTrasladoSaliente the new fecha solicitud traslado
   *        saliente
   */
  public void setFechaSolicitudTrasladoSaliente(
      final Date fechaSolicitudTrasladoSaliente) {
    this.fechaSolicitudTrasladoSaliente =
        UtilidadesCommons.cloneDate(fechaSolicitudTrasladoSaliente);
  }

  /**
   * Gets the grado.
   *
   * @return the grado
   */
  public String getGrado() {
    return grado;
  }

  /**
   * Sets the grado.
   *
   * @param grado the new grado
   */
  public void setGrado(final String grado) {
    this.grado = grado;
  }

  /**
   * Gets the nivel.
   *
   * @return the nivel
   */
  public String getNivel() {
    return nivel;
  }

  /**
   * Sets the nivel.
   *
   * @param nivel the new nivel
   */
  public void setNivel(final String nivel) {
    this.nivel = nivel;
  }

  /**
   * Gets the pk comunidad autonoma entrante.
   *
   * @return the pk comunidad autonoma entrante
   */
  public ComunidadAutonoma getComunidadAutonomaEntrante() {
    return comunidadAutonomaEntrante;
  }

  /**
   * Sets the pk comunidad autonoma entrante.
   *
   * @param comunidadAutonomaEntrante the new comunidad autonoma entrante
   */
  public void setComunidadAutonomaEntrante(
      final ComunidadAutonoma comunidadAutonomaEntrante) {
    this.comunidadAutonomaEntrante = comunidadAutonomaEntrante;
  }

  /**
   * Gets the pk comunidad autonoma saliente.
   *
   * @return the pk comunidad autonoma saliente
   */
  public ComunidadAutonoma getComunidadAutonomaSaliente() {
    return comunidadAutonomaSaliente;
  }

  /**
   * Sets the pk comunidad autonoma saliente.
   *
   * @param comunidadAutonomaSaliente the new comunidad autonoma saliente
   */
  public void setComunidadAutonomaSaliente(
      final ComunidadAutonoma comunidadAutonomaSaliente) {
    this.comunidadAutonomaSaliente = comunidadAutonomaSaliente;
  }

  /**
   * Gets the pk entidad dependencia entrante.
   *
   * @return the pk entidad dependencia entrante
   */
  public EntidadDependencia getEntidadDependenciaEntrante() {
    return entidadDependenciaEntrante;
  }

  /**
   * Sets the pk entidad dependencia entrante.
   *
   * @param entidadDependenciaEntrante the new entidad dependencia entrante
   */
  public void setEntidadDependenciaEntrante(
      final EntidadDependencia entidadDependenciaEntrante) {
    this.entidadDependenciaEntrante = entidadDependenciaEntrante;
  }

  /**
   * Gets the pk entidad dependencia saliente.
   *
   * @return the pk entidad dependencia saliente
   */
  public EntidadDependencia getEntidadDependenciaSaliente() {
    return entidadDependenciaSaliente;
  }

  /**
   * Sets the pk entidad dependencia saliente.
   *
   * @param entidadDependenciaSaliente the new entidad dependencia saliente
   */
  public void setEntidadDependenciaSaliente(
      final EntidadDependencia entidadDependenciaSaliente) {
    this.entidadDependenciaSaliente = entidadDependenciaSaliente;
  }

  /**
   * Gets the pk estado civil.
   *
   * @return the pk estado civil
   */
  public Long getPkEstadoCivil() {
    return pkEstadoCivil;
  }

  /**
   * Sets the pk estado civil.
   *
   * @param pkEstadoCivil the new pk estado civil
   */
  public void setPkEstadoCivil(final Long pkEstadoCivil) {
    this.pkEstadoCivil = pkEstadoCivil;
  }

  /**
   * Gets the pk motivo archivo.
   *
   * @return the pk motivo archivo
   */
  public Long getPkMotivoArchivo() {
    return pkMotivoArchivo;
  }

  /**
   * Sets the pk motivo archivo.
   *
   * @param pkMotivoArchivo the new pk motivo archivo
   */
  public void setPkMotivoArchivo(final Long pkMotivoArchivo) {
    this.pkMotivoArchivo = pkMotivoArchivo;
  }

  /**
   * Gets the pk unidad.
   *
   * @return the pk unidad
   */
  public Long getPkUnidad() {
    return pkUnidad;
  }

  /**
   * Sets the pk unidad.
   *
   * @param pkUnidad the new pk unidad
   */
  public void setPkUnidad(final Long pkUnidad) {
    this.pkUnidad = pkUnidad;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the ambito solicitud.
   *
   * @return the ambito solicitud
   */
  public String getAmbitoSolicitud() {
    return ambitoSolicitud;
  }

  /**
   * Sets the ambito solicitud.
   *
   * @param ambitoSolicitud the new ambito solicitud
   */
  public void setAmbitoSolicitud(final String ambitoSolicitud) {
    this.ambitoSolicitud = ambitoSolicitud;
  }

  /**
   * Gets the bloqueada.
   *
   * @return the bloqueada
   */
  public Boolean getBloqueada() {
    return bloqueada;
  }

  /**
   * Sets the bloqueada.
   *
   * @param bloqueada the new bloqueada
   */
  public void setBloqueada(final Boolean bloqueada) {
    this.bloqueada = bloqueada;
  }

  /**
   * Gets the bloqueo discrepancia.
   *
   * @return the bloqueo discrepancia
   */
  public Boolean getBloqueoDiscrepancia() {
    return bloqueoDiscrepancia;
  }

  /**
   * Sets the bloqueo discrepancia.
   *
   * @param bloqueoDiscrepancia the new bloqueo discrepancia
   */
  public void setBloqueoDiscrepancia(final Boolean bloqueoDiscrepancia) {
    this.bloqueoDiscrepancia = bloqueoDiscrepancia;
  }

  /**
   * Gets the capacidad economica.
   *
   * @return the capacidad economica
   */
  public BigDecimal getCapacidadEconomica() {
    return capacidadEconomica;
  }

  /**
   * Sets the capacidad economica.
   *
   * @param capacidadEconomica the new capacidad economica
   */
  public void setCapacidadEconomica(final BigDecimal capacidadEconomica) {
    this.capacidadEconomica = capacidadEconomica;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the fecha efecto traslado entrante.
   *
   * @return the fecha efecto traslado entrante
   */
  public Date getFechaEfectosTrasladoEntrante() {
    return UtilidadesCommons.cloneDate(fechaEfectosTrasladoEntrante);
  }

  /**
   * Sets the fecha efecto traslado entrante.
   *
   * @param fechaEfectosTrasladoEntrante the new fecha efectos traslado entrante
   */
  public void setFechaEfectosTrasladoEntrante(
      final Date fechaEfectosTrasladoEntrante) {
    this.fechaEfectosTrasladoEntrante =
        UtilidadesCommons.cloneDate(fechaEfectosTrasladoEntrante);
  }

  /**
   * Gets the fecha anterior reactivacion.
   *
   * @return the fecha anterior reactivacion
   */
  public Date getFechaAnteriorReactivacion() {
    return UtilidadesCommons.cloneDate(fechaAnteriorReactivacion);
  }

  /**
   * Sets the fecha anterior reactivacion.
   *
   * @param fechaAnteriorReactivacion the new fecha anterior reactivacion
   */
  public void setFechaAnteriorReactivacion(
      final Date fechaAnteriorReactivacion) {
    this.fechaAnteriorReactivacion =
        UtilidadesCommons.cloneDate(fechaAnteriorReactivacion);
  }

  /**
   * Gets the fecha traslado saliente.
   *
   * @return the fecha traslado saliente
   */
  public Date getFechaTrasladoSaliente() {
    return UtilidadesCommons.cloneDate(fechaTrasladoSaliente);
  }

  /**
   * Sets the fecha traslado saliente.
   *
   * @param fechaTrasladoSaliente the new fecha traslado saliente
   */
  public void setFechaTrasladoSaliente(final Date fechaTrasladoSaliente) {
    this.fechaTrasladoSaliente =
        UtilidadesCommons.cloneDate(fechaTrasladoSaliente);
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha entrega.
   *
   * @return the fecha entrega
   */
  public Date getFechaEntrega() {
    return UtilidadesCommons.cloneDate(fechaEntrega);
  }

  /**
   * Sets the fecha entrega.
   *
   * @param fechaEntrega the new fecha entrega
   */
  public void setFechaEntrega(final Date fechaEntrega) {
    this.fechaEntrega = UtilidadesCommons.cloneDate(fechaEntrega);
  }

  /**
   * Gets the fecha registro.
   *
   * @return the fecha registro
   */
  public Date getFechaRegistro() {
    return UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Sets the fecha registro.
   *
   * @param fechaRegistro the new fecha registro
   */
  public void setFechaRegistro(final Date fechaRegistro) {
    this.fechaRegistro = UtilidadesCommons.cloneDate(fechaRegistro);
  }

  /**
   * Gets the fecha entrada comunidad.
   *
   * @return the fecha entrada comunidad
   */
  public Date getFechaEntradaComunidad() {
    return UtilidadesCommons.cloneDate(fechaEntradaComunidad);
  }

  /**
   * Sets the fecha entrada comunidad.
   *
   * @param fechaEntradaComunidad the new fecha entrada comunidad
   */
  public void setFechaEntradaComunidad(final Date fechaEntradaComunidad) {
    this.fechaEntradaComunidad =
        UtilidadesCommons.cloneDate(fechaEntradaComunidad);
  }

  /**
   * Gets the finalizada.
   *
   * @return the finalizada
   */
  public Boolean getFinalizada() {
    return finalizada;
  }

  /**
   * Sets the finalizada.
   *
   * @param finalizada the new finalizada
   */
  public void setFinalizada(final Boolean finalizada) {
    this.finalizada = finalizada;
  }

  /**
   * Gets the fecha notificacion GYN.
   *
   * @return the fecha notificacion GYN
   */
  public Date getFechaNotificacionGYN() {
    return UtilidadesCommons.cloneDate(fechaNotificacionGYN);
  }

  /**
   * Sets the fecha notificacion GYN.
   *
   * @param fechaNotificacionGYN the new fecha notificacion GYN
   */
  public void setFechaNotificacionGYN(final Date fechaNotificacionGYN) {
    this.fechaNotificacionGYN =
        UtilidadesCommons.cloneDate(fechaNotificacionGYN);
  }

  /**
   * Gets the fecha notificacion PIA.
   *
   * @return the fecha notificacion PIA
   */
  public Date getFechaNotificacionPIA() {
    return UtilidadesCommons.cloneDate(fechaNotificacionPIA);
  }

  /**
   * Sets the fecha notificacion PIA.
   *
   * @param fechaNotificacionPIA the new fecha notificacion PIA
   */
  public void setFechaNotificacionPIA(final Date fechaNotificacionPIA) {
    this.fechaNotificacionPIA =
        UtilidadesCommons.cloneDate(fechaNotificacionPIA);
  }

  /**
   * Gets the fecha resolucion GYN.
   *
   * @return the fecha resolucion GYN
   */
  public Date getFechaResolucionGYN() {
    return UtilidadesCommons.cloneDate(fechaResolucionGYN);
  }

  /**
   * Sets the fecha resolucion GYN.
   *
   * @param fechaResolucionGYN the new fecha resolucion GYN
   */
  public void setFechaResolucionGYN(final Date fechaResolucionGYN) {
    this.fechaResolucionGYN = UtilidadesCommons.cloneDate(fechaResolucionGYN);
  }

  /**
   * Gets the fecha resolucion PIA.
   *
   * @return the fecha resolucion PIA
   */
  public Date getFechaResolucionPIA() {
    return UtilidadesCommons.cloneDate(fechaResolucionPIA);
  }

  /**
   * Sets the fecha resolucion PIA.
   *
   * @param fechaResolucionPIA the new fecha resolucion PIA
   */
  public void setFechaResolucionPIA(final Date fechaResolucionPIA) {
    this.fechaResolucionPIA = UtilidadesCommons.cloneDate(fechaResolucionPIA);
  }

  /**
   * Gets the fecha revision GYN.
   *
   * @return the fecha revision GYN
   */
  public Date getFechaRevisionGYN() {
    return UtilidadesCommons.cloneDate(fechaRevisionGYN);
  }

  /**
   * Sets the fecha revision GYN.
   *
   * @param fechaRevisionGYN the new fecha revision GYN
   */
  public void setFechaRevisionGYN(final Date fechaRevisionGYN) {
    this.fechaRevisionGYN = UtilidadesCommons.cloneDate(fechaRevisionGYN);
  }

  /**
   * Gets the grado traslado.
   *
   * @return the grado traslado
   */
  public String getGradoTraslado() {
    return gradoTraslado;
  }

  /**
   * Sets the grado traslado.
   *
   * @param gradoTraslado the new grado traslado
   */
  public void setGradoTraslado(final String gradoTraslado) {
    this.gradoTraslado = gradoTraslado;
  }

  /**
   * Gets the pendiente informe social.
   *
   * @return the pendienteInformeSocial
   */
  public Boolean getPendienteInformeSocial() {
    return pendienteInformeSocial;
  }

  /**
   * Sets the pendiente informe social.
   *
   * @param pendienteInformeSocial the pendienteInformeSocial to set
   */
  public void setPendienteInformeSocial(final Boolean pendienteInformeSocial) {
    this.pendienteInformeSocial = pendienteInformeSocial;
  }

  /**
   * Gets the pendiente informe salud.
   *
   * @return the pendiente informe salud
   */
  public Boolean getPendienteInformeSalud() {
    return pendienteInformeSalud;
  }

  /**
   * Sets the pendiente informe salud.
   *
   * @param pendienteInformeSalud the new pendiente informe salud
   */
  public void setPendienteInformeSalud(final Boolean pendienteInformeSalud) {
    this.pendienteInformeSalud = pendienteInformeSalud;
  }

  /**
   * Gets the pendiente informe extendido fisico.
   *
   * @return the pendiente informe extendido fisico
   */
  public Boolean getPendienteInformeExtendidoFisico() {
    return pendienteInformeExtendidoFisico;
  }

  /**
   * Sets the pendiente informe extendido fisico.
   *
   * @param pendienteInformeExtendidoFisico the new pendiente informe extendido
   *        fisico
   */
  public void setPendienteInformeExtendidoFisico(
      final Boolean pendienteInformeExtendidoFisico) {
    this.pendienteInformeExtendidoFisico = pendienteInformeExtendidoFisico;
  }

  /**
   * Gets the pendiente informe extendido mental.
   *
   * @return the pendiente informe extendido mental
   */
  public Boolean getPendienteInformeExtendidoMental() {
    return pendienteInformeExtendidoMental;
  }

  /**
   * Sets the pendiente informe extendido mental.
   *
   * @param pendienteInformeExtendidoMental the new pendiente informe extendido
   *        mental
   */
  public void setPendienteInformeExtendidoMental(
      final Boolean pendienteInformeExtendidoMental) {
    this.pendienteInformeExtendidoMental = pendienteInformeExtendidoMental;
  }

  /**
   * Gets the motivo revision.
   *
   * @return the motivo revision
   */
  public String getMotivoRevision() {
    return motivoRevision;
  }

  /**
   * Sets the motivo revision.
   *
   * @param motivoRevision the new motivo revision
   */
  public void setMotivoRevision(final String motivoRevision) {
    this.motivoRevision = motivoRevision;
  }

  /**
   * Gets the nivel traslado.
   *
   * @return the nivel traslado
   */
  public String getNivelTraslado() {
    return nivelTraslado;
  }

  /**
   * Sets the nivel traslado.
   *
   * @param nivelTraslado the new nivel traslado
   */
  public void setNivelTraslado(final String nivelTraslado) {
    this.nivelTraslado = nivelTraslado;
  }

  /**
   * Gets the numero entidad local.
   *
   * @return the numero entidad local
   */
  public String getNumeroEntidadLocal() {
    return numeroEntidadLocal;
  }

  /**
   * Sets the numero entidad local.
   *
   * @param numeroEntidadLocal the new numero entidad local
   */
  public void setNumeroEntidadLocal(final String numeroEntidadLocal) {
    this.numeroEntidadLocal = numeroEntidadLocal;
  }

  /**
   * Gets the numero registro.
   *
   * @return the numero registro
   */
  public String getNumeroRegistro() {
    return numeroRegistro;
  }

  /**
   * Sets the numero registro.
   *
   * @param numeroRegistro the new numero registro
   */
  public void setNumeroRegistro(final String numeroRegistro) {
    this.numeroRegistro = numeroRegistro;
  }

  /**
   * Gets the revision oficio.
   *
   * @return the revision oficio
   */
  public Boolean getRevisionOficio() {
    return revisionOficio;
  }

  /**
   * Sets the revision oficio.
   *
   * @param revisionOficio the new revision oficio
   */
  public void setRevisionOficio(final Boolean revisionOficio) {
    this.revisionOficio = revisionOficio;
  }

  /**
   * Gets the revision oficio es menor.
   *
   * @return the revision oficio es menor
   */
  public Boolean getRevisionOficioEsMenor() {
    return revisionOficioEsMenor;
  }

  /**
   * Sets the revision oficio es menor.
   *
   * @param revisionOficioEsMenor the new revision oficio es menor
   */
  public void setRevisionOficioEsMenor(final Boolean revisionOficioEsMenor) {
    this.revisionOficioEsMenor = revisionOficioEsMenor;
  }

  /**
   * Gets the pendiente documentacion.
   *
   * @return the pendiente documentacion
   */
  public Boolean getPendienteDocumentacion() {
    return pendienteDocumentacion;
  }

  /**
   * Sets the pendiente documentacion.
   *
   * @param pendienteDocumentacion the new pendiente documentacion
   */
  public void setPendienteDocumentacion(final Boolean pendienteDocumentacion) {
    this.pendienteDocumentacion = pendienteDocumentacion;
  }

  /**
   * Gets the tipo perfil apertura.
   *
   * @return the tipo perfil apertura
   */
  public Long getTipoPerfilApertura() {
    return tipoPerfilApertura;
  }

  /**
   * Sets the tipo perfil apertura.
   *
   * @param tipoPerfilApertura the new tipo perfil apertura
   */
  public void setTipoPerfilApertura(final Long tipoPerfilApertura) {
    this.tipoPerfilApertura = tipoPerfilApertura;
  }

  /**
   * Gets the traslado permanente.
   *
   * @return the traslado permanente
   */
  public Boolean getTrasladoPermanente() {
    return trasladoPermanente;
  }

  /**
   * Sets the traslado permanente.
   *
   * @param trasladoPermanente the new traslado permanente
   */
  public void setTrasladoPermanente(final Boolean trasladoPermanente) {
    this.trasladoPermanente = trasladoPermanente;
  }

  /**
   * Gets the reactivacion.
   *
   * @return the reactivacion
   */
  public Boolean getReactivacion() {
    return reactivacion;
  }

  /**
   * Sets the reactivacion.
   *
   * @param reactivacion the new reactivacion
   */
  public void setReactivacion(final Boolean reactivacion) {
    this.reactivacion = reactivacion;
  }

  /**
   * Gets the revision.
   *
   * @return the revision
   */
  public Boolean getRevision() {
    return revision;
  }

  /**
   * Sets the revision.
   *
   * @param revision the new revision
   */
  public void setRevision(final Boolean revision) {
    this.revision = revision;
  }

  /**
   * Gets the servicio.
   *
   * @return the servicio
   */
  public Boolean getServicio() {
    return servicio;
  }

  /**
   * Sets the servicio.
   *
   * @param servicio the new servicio
   */
  public void setServicio(final Boolean servicio) {
    this.servicio = servicio;
  }

  /**
   * Gets the tiene nivel.
   *
   * @return the tiene nivel
   */
  public Boolean getTieneNivel() {
    return tieneNivel;
  }

  /**
   * Sets the tiene nivel.
   *
   * @param tieneNivel the new tiene nivel
   */
  public void setTieneNivel(final Boolean tieneNivel) {
    this.tieneNivel = tieneNivel;
  }

  /**
   * Gets the traslado.
   *
   * @return the traslado
   */
  public Boolean getTraslado() {
    return traslado;
  }

  /**
   * Sets the traslado.
   *
   * @param traslado the new traslado
   */
  public void setTraslado(final Boolean traslado) {
    this.traslado = traslado;
  }

  /**
   * Gets the traslado saliente.
   *
   * @return the traslado saliente
   */
  public Boolean getTrasladoSaliente() {
    return trasladoSaliente;
  }

  /**
   * Sets the traslado saliente.
   *
   * @param trasladoSaliente the new traslado saliente
   */
  public void setTrasladoSaliente(final Boolean trasladoSaliente) {
    this.trasladoSaliente = trasladoSaliente;
  }

  /**
   * Gets the proceso urgencia validacion.
   *
   * @return the proceso urgencia validacion
   */
  public Boolean getProcesoUrgenciaValidacion() {
    return procesoUrgenciaValidacion;
  }

  /**
   * Sets the proceso urgencia validacion.
   *
   * @param procesoUrgenciaValidacion the new proceso urgencia validacion
   */
  public void setProcesoUrgenciaValidacion(
      final Boolean procesoUrgenciaValidacion) {
    this.procesoUrgenciaValidacion = procesoUrgenciaValidacion;
  }

  /**
   * Gets the pendiente valoracion.
   *
   * @return the pendienteValoracion
   */
  public Boolean getPendienteValoracion() {
    return pendienteValoracion;
  }

  /**
   * Sets the pendiente valoracion.
   *
   * @param pendienteValoracion the pendienteValoracion to set
   */
  public void setPendienteValoracion(final Boolean pendienteValoracion) {
    this.pendienteValoracion = pendienteValoracion;
  }

  /**
   * Gets the vive solo.
   *
   * @return the vive solo
   */
  public Boolean getViveSolo() {
    return viveSolo;
  }

  /**
   * Sets the vive solo.
   *
   * @param viveSolo the new vive solo
   */
  public void setViveSolo(final Boolean viveSolo) {
    this.viveSolo = viveSolo;
  }



  /**
   * Gets the pendiente envio.
   *
   * @return the pendiente envio
   */
  public Boolean getPendienteEnvio() {
    return pendienteEnvio;
  }

  /**
   * Sets the pendiente envio.
   *
   * @param pendienteEnvio the new pendiente envio
   */
  public void setPendienteEnvio(final Boolean pendienteEnvio) {
    this.pendienteEnvio = pendienteEnvio;
  }

  /**
   * Gets the tipo solicitud.
   *
   * @return the tipo solicitud
   */
  public TipoSolicitud getTipoSolicitud() {
    return tipoSolicitud;
  }

  /**
   * Sets the tipo solicitud.
   *
   * @param tipoSolicitud the new tipo solicitud
   */
  public void setTipoSolicitud(final TipoSolicitud tipoSolicitud) {
    this.tipoSolicitud = tipoSolicitud;
  }

  /**
   * Gets the motivo borrado.
   *
   * @return the motivo borrado
   */
  public String getMotivoBorrado() {
    return motivoBorrado;
  }

  /**
   * Sets the motivo borrado.
   *
   * @param motivoBorrado the new motivo borrado
   */
  public void setMotivoBorrado(final String motivoBorrado) {
    this.motivoBorrado = motivoBorrado;
  }

  /**
   * Gets the migrada documento sidep.
   *
   * @return the migrada documento sidep
   */
  public String getMigradaDocumentoSidep() {
    return migradaDocumentoSidep;
  }

  /**
   * Sets the migrada documento sidep.
   *
   * @param migradaDocumentoSidep the new migrada documento sidep
   */
  public void setMigradaDocumentoSidep(final String migradaDocumentoSidep) {
    this.migradaDocumentoSidep = migradaDocumentoSidep;
  }

  /**
   * Gets the expediente.
   *
   * @return the expediente
   */
  public Expediente getExpediente() {
    return expediente;
  }

  /**
   * Sets the expediente.
   *
   * @param expediente the new expediente
   */
  public void setExpediente(final Expediente expediente) {
    this.expediente = expediente;
  }

  /**
   * Gets the provincia 1.
   *
   * @return the provincia 1
   */
  public Provincia getProvinciaEntrante() {
    return provinciaEntrante;
  }

  /**
   * Sets the provincia entrante.
   *
   * @param provinciaEntrante the new provincia entrante
   */
  public void setProvinciaEntrante(final Provincia provinciaEntrante) {
    this.provinciaEntrante = provinciaEntrante;
  }

  /**
   * Gets the provincia saliente.
   *
   * @return the provincia saliente
   */
  public Provincia getProvinciaSaliente() {
    return provinciaSaliente;
  }

  /**
   * Sets the provincia saliente.
   *
   * @param provinciaSaliente the new provincia saliente
   */
  public void setProvinciaSaliente(final Provincia provinciaSaliente) {
    this.provinciaSaliente = provinciaSaliente;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the usuario to set
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the solicitud revisada.
   *
   * @return the solicitud revisada
   */
  public Solicitud getSolicitudRevisada() {
    return solicitudRevisada;
  }

  /**
   * Sets the solicitud revisada.
   *
   * @param solicitudRevisada the new solicitud revisada
   */
  public void setSolicitudRevisada(final Solicitud solicitudRevisada) {
    this.solicitudRevisada = solicitudRevisada;
  }

  /**
   * Gets the estado activo.
   *
   * @return the estado activo
   */
  public List<EstadoSolicitud> getEstados() {
    return UtilidadesCommons.collectorsToList(estados);
  }

  /**
   * Sets the estado activo.
   *
   * @param estados the new estado activo
   */
  public void setEstados(final List<EstadoSolicitud> estados) {
    this.estados = UtilidadesCommons.collectorsToList(estados);
  }

  /**
   * Gets the motivo finalizada.
   *
   * @return the motivo finalizada
   */
  public String getMotivoFinalizada() {
    return motivoFinalizada;
  }

  /**
   * Sets the motivo finalizada.
   *
   * @param motivoFinalizada the new motivo finalizada
   */
  public void setMotivoFinalizada(final String motivoFinalizada) {
    this.motivoFinalizada = motivoFinalizada;
  }

  /**
   * Gets the app origen.
   *
   * @return the app origen
   */
  public String getAppOrigen() {
    return appOrigen;
  }

  /**
   * Sets the app origen.
   *
   * @param appOrigen the new app origen
   */
  public void setAppOrigen(final String appOrigen) {
    this.appOrigen = appOrigen;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
