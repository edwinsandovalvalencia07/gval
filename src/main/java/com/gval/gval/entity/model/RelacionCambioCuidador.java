package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;
import jakarta.persistence.*;

/**
 * The persistent class for the SDM_REL_CAMBIO_CNP database table.
 *
 */
@Entity
@Table(name = "SDM_REL_CAMBIO_CNP")
@GenericGenerator(name = "SDM_REL_CAMBIO_CNP_PKREL_CAMBIO_CNP_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_REL_RESOLUCI"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class RelacionCambioCuidador implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk cuidador. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_REL_CAMBIO_CNP_PKREL_CAMBIO_CNP_GENERATOR")
  @Column(name = "PK_REL_CAMBIO_CNP", unique = true, nullable = false)
  private Long pkRelacionCambioCuidador;

  /** The cuidador. */
  // bi-directional many-to-one association to SdmCnp
  @ManyToOne
  @JoinColumn(name = "PK_CNP", nullable = false)
  private Cuidador cuidador;

  /** The activo. */
  @Column(name = "RCACTIVO", nullable = false)
  private Boolean activo;

  /** The fecha crea registro. */
  @Temporal(TemporalType.DATE)
  @Column(name = "RCFECHCREA")
  private Date fechaCreacion;

  /** Usuario asociado. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_PERSONA")
  private Usuario usuario;


  // TODO:Completar cuando se desarrolle PIA
  // bi-directional many-to-one association to SdmPia
  // @ManyToOne
  // @JoinColumn(name = "PK_PIA")
  // private SdmPia sdmPia;

  // TODO:Completar cuando se desarrolle RESOLUCIONES
  // bi-directional many-to-one association to SdmResoluci
  // @ManyToOne
  // @JoinColumn(name = "PK_RESOLUCION")
  // private SdmResoluci sdmResoluci;

  /**
   * Instantiates a new relacion cambio cuidador.
   */
  public RelacionCambioCuidador() {
    // Constructor
  }


  /**
   * Gets the pk relacion cambio cuidador.
   *
   * @return the pk relacion cambio cuidador
   */
  public Long getPkRelacionCambioCuidador() {
    return pkRelacionCambioCuidador;
  }


  /**
   * Sets the pk relacion cambio cuidador.
   *
   * @param pkRelacionCambioCuidador the new pk relacion cambio cuidador
   */
  public void setPkRelacionCambioCuidador(final Long pkRelacionCambioCuidador) {
    this.pkRelacionCambioCuidador = pkRelacionCambioCuidador;
  }


  /**
   * Gets the cuidador.
   *
   * @return the cuidador
   */
  public Cuidador getCuidador() {
    return cuidador;
  }


  /**
   * Sets the cuidador.
   *
   * @param cuidador the new cuidador
   */
  public void setCuidador(final Cuidador cuidador) {
    this.cuidador = cuidador;
  }


  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }


  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }


  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }


  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }


  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }


  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }



}
