package com.gval.gval.entity.model;

import java.io.Serializable;
import java.sql.Date;

import jakarta.persistence.*;


  /**
   * The Class PiaLlamadas.
   */
  @Entity
  @Table(name = "SDM_PIALLAMADAS")
  public class PiaLlamadas implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The pkpiallamada. */
    @Id
    @Column(name = "PK_PIALLAMADA", unique = true, nullable = false)
    private Long pkPiaLlamada;

    /** The pkpia. */
    @Column(name = "PK_PIA", nullable = false)
    private Long pkPia;


    /** The persona contacto. */
    @Column(name = "PERSONACONTACTO", length = 100)
    private String personaContacto;


    /** The acepta centro. */
    @Column(name = "ACEPTACENTRO")
    private Long aceptaCentro;


    /** The fecha hora. */
    @Column(name = "FECHAHORA")
    private Date fechaHora;


    /** The observaciones. */
    @Column(name = "OBSERVACIONES", length = 100)
    private String observaciones;

    /** The pk tipo llamada. */
    @Column(name = "PK_TIPO_LLAMADA")
    private Long pkTipoLlamada;


    /** The pk docu. */
    @Column(name = "PK_DOCU")
    private Long pkDocu;


    /**
     * Gets the serialversionuid.
     *
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
      return serialVersionUID;
    }


    /**
     * Gets the pkpiallamada.
     *
     * @return the pkpiallamada
     */
    public Long getPkPiaLlamada() {
      return pkPiaLlamada;
    }


    /**
     * Gets the pkpia.
     *
     * @return the pkpia
     */
    public Long getPkpia() {
      return pkPia;
    }


    /**
     * Gets the persona contacto.
     *
     * @return the persona contacto
     */
    public String getPersonaContacto() {
      return personaContacto;
    }


    /**
     * Gets the acepta centro.
     *
     * @return the acepta centro
     */
    public Long getAceptaCentro() {
      return aceptaCentro;
    }


    /**
     * Gets the fecha hora.
     *
     * @return the fecha hora
     */
    public Date getFechaHora() {
      return fechaHora;
    }


    /**
     * Gets the observaciones.
     *
     * @return the observaciones
     */
    public String getObservaciones() {
      return observaciones;
    }


    /**
     * Gets the pk tipo llamada.
     *
     * @return the pk tipo llamada
     */
    public Long getPkTipoLlamada() {
      return pkTipoLlamada;
    }


    /**
     * Gets the pk docu.
     *
     * @return the pk docu
     */
    public Long getPkDocu() {
      return pkDocu;
    }


    /**
     * Sets the pkpiallamada.
     *
     * @param pkpiallamada the new pkpiallamada
     */
    public void setPiaLlamada(final Long pkpiallamada) {
      this.pkPiaLlamada = pkpiallamada;
    }


    /**
     * Sets the pkpia.
     *
     * @param pkpia the new pkpia
     */
    public void setPkpia(final Long pkpia) {
      this.pkPia = pkpia;
    }


    /**
     * Sets the persona contacto.
     *
     * @param personaContacto the new persona contacto
     */
    public void setPersonaContacto(final String personaContacto) {
      this.personaContacto = personaContacto;
    }


    /**
     * Sets the acepta centro.
     *
     * @param aceptaCentro the new acepta centro
     */
    public void setAceptaCentro(final Long aceptaCentro) {
      this.aceptaCentro = aceptaCentro;
    }


    /**
     * Sets the fecha hora.
     *
     * @param fechaHora the new fecha hora
     */
    public void setFechaHora(final Date fechaHora) {
      this.fechaHora = fechaHora;
    }


    /**
     * Sets the observaciones.
     *
     * @param observaciones the new observaciones
     */
    public void setObservaciones(final String observaciones) {
      this.observaciones = observaciones;
    }


    /**
     * Sets the pk tipo llamada.
     *
     * @param pkTipoLlamada the new pk tipo llamada
     */
    public void setPkTipoLlamada(final Long pkTipoLlamada) {
      this.pkTipoLlamada = pkTipoLlamada;
    }


    /**
     * Sets the pk docu.
     *
     * @param pkDocu the new pk docu
     */
    public void setPkDocu(final Long pkDocu) {
      this.pkDocu = pkDocu;
    }
  }


