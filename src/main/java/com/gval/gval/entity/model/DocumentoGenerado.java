package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

import jakarta.persistence.*;


/**
 * The persistent class for the SDM_DOCUGENE database table.
 *
 */
@Entity
@Table(name = "SDM_DOCUGENE")
public class DocumentoGenerado implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 7498114792557100946L;

  /** The pk documento. */
  @Id
  @Column(name = "PK_DOCU", unique = true, nullable = false)
  private Long pkDocumento;

  /** The observaciones. */
  @Column(name = "DOOBSERVAC", length = 250)
  private String observaciones;

  /** The documento aportado. */
  @Column(name = "PK_DOCU2", precision = 38)
  private Long documentoAportado;


  /** The documento. */
  // bi-directional one-to-one association to Documento
  @OneToOne
  @JoinColumn(name = "PK_DOCU", nullable = false, insertable = false,
      updatable = false, referencedColumnName = "pkDocumento")
  @MapsId
  private Documento documento;


  /** The usuario. */
  // bi-directional many-to-one association to SdxUsuario
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA", nullable = false)
  private Usuario usuario;

  /** The direccion. */
  // bi-directional many-to-one association to SdmDireccio
  @ManyToOne
  @JoinColumn(name = "PK_DIRECCIO")
  private DireccionAda direccion;

  /** The firma documento. */
  // bi-directional many-to-one association to SdmFirmdocu
  @ManyToOne
  @JoinColumn(name = "PK_FIRMDOCU")
  private FirmaDocumento firmaDocumento;


  /**
   * Instantiates a new documento generado.
   */
  public DocumentoGenerado() {
    // Constructor vacío
  }

  /**
   * Gets the pk documento.
   *
   * @return the pkDocumento
   */
  public Long getPkDocumento() {
    return pkDocumento;
  }

  /**
   * Sets the pk documento.
   *
   * @param pkDocumento the pkDocumento to set
   */
  public void setPkDocumento(final Long pkDocumento) {
    this.pkDocumento = pkDocumento;
  }

  /**
   * Gets the observaciones.
   *
   * @return the observaciones
   */
  public String getObservaciones() {
    return observaciones;
  }

  /**
   * Sets the observaciones.
   *
   * @param observaciones the observaciones to set
   */
  public void setObservaciones(final String observaciones) {
    this.observaciones = observaciones;
  }

  /**
   * Gets the documento aportado.
   *
   * @return the documentoAportado
   */
  public Long getDocumentoAportado() {
    return documentoAportado;
  }

  /**
   * Sets the documento aportado.
   *
   * @param documentoAportado the documentoAportado to set
   */
  public void setDocumentoAportado(final Long documentoAportado) {
    this.documentoAportado = documentoAportado;
  }


  /**
   * Gets the documento.
   *
   * @return the documento
   */
  public Documento getDocumento() {
    return documento;
  }

  /**
   * Sets the documento.
   *
   * @param documento the documento to set
   */
  public void setDocumento(final Documento documento) {
    this.documento = documento;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the usuario to set
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the direccion.
   *
   * @return the direccion
   */
  public DireccionAda getDireccion() {
    return direccion;
  }

  /**
   * Sets the direccion.
   *
   * @param direccion the new direccion
   */
  public void setDireccion(final DireccionAda direccion) {
    this.direccion = direccion;
  }

  /**
   * Gets the firma documento.
   *
   * @return the firma documento
   */
  public FirmaDocumento getFirmaDocumento() {
    return firmaDocumento;
  }

  /**
   * Sets the firma documento.
   *
   * @param firmaDocumento the new firma documento
   */
  public void setFirmaDocumento(final FirmaDocumento firmaDocumento) {
    this.firmaDocumento = firmaDocumento;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
