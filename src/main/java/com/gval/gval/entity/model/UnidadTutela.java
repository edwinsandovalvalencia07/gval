package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;

import jakarta.persistence.*;

/**
 * The persistent class for the SDM_UNIDADTUTELA database table.
 *
 */
@Entity
@Table(name = "SDM_UNIDADTUTELA")
@GenericGenerator(name = "SDM_UNIDADTUTELA_PKUNIDADTUTELA_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_UNIDADTUTELA"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})

public final class UnidadTutela implements Serializable {


  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -1752040604743408877L;

  /** The pk unidad tutela. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_UNIDADTUTELA_PKUNIDADTUTELA_GENERATOR")
  @Column(name = "PK_UNIDADTUTELA", unique = true, nullable = false)
  private Long pkUnidadTutela;

  /** The descripcion. */
  @Column(name = "DESCRIPCION", length = 100)
  private String descripcion;


  /** The provincia. */
  // bi-directional many-to-one association to SdxProvinci
  @ManyToOne
  @JoinColumn(name = "PK_PROVINCI", nullable = false)
  private Provincia provincia;


  /**
   * Gets the pk unidad tutela.
   *
   * @return the pkUnidadTutela
   */
  public Long getPkUnidadTutela() {
    return pkUnidadTutela;
  }


  /**
   * Sets the pk unidad tutela.
   *
   * @param pkUnidadTutela the pkUnidadTutela to set
   */
  public void setPkUnidadTutela(final Long pkUnidadTutela) {
    this.pkUnidadTutela = pkUnidadTutela;
  }


  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }


  /**
   * Sets the descripcion.
   *
   * @param descripcion the descripcion to set
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }


  /**
   * Gets the provincia.
   *
   * @return the provincia
   */
  public Provincia getProvincia() {
    return provincia;
  }


  /**
   * Sets the provincia.
   *
   * @param provincia the provincia to set
   */
  public void setProvincia(final Provincia provincia) {
    this.provincia = provincia;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
