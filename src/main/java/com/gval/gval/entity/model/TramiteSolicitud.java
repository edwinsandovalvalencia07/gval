package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The Class TramiteSolicitud.
 */

@Entity
@Table(name = "sdm_tramite_solicitud")
public class TramiteSolicitud implements Serializable {
  /** The pk tramite solicitud. */
  @Id
  @Column(name = "PK_TRAMITE_SOLICITUD", unique = true, nullable = false,
      insertable = false, updatable = false)
  private Long pkTramiteSolicitud;

  /** The trfechcrea. */
  @Column(name = "TRFECHCREA", insertable = false, updatable = false)
  private Date trfechcrea;

  /** The trfechregi. */
  @Column(name = "TRFECHCREA", nullable = false, insertable = false,
      updatable = false)
  private Date trfechregi;

  /** The trapellidos. */
  @Column(name = "TRAPELLIDOS")
  private String trapellidos;

  /** The trnombre. */
  @Column(name = "TRNOMBRE")
  private String trnombre;

  /** The trnudocide. */
  @Column(name = "TRNUDOCIDE")
  private String trnudocide;

  /** The trtelefono. */
  @Column(name = "TRTELEFONO")
  private String trtelefono;

  /** The trexpediente. */
  @Column(name = "TREXPEDIENTE")
  private String trexpediente;

  /** The trinfor. */
  @Column(name = "TRINFOR", nullable = false)
  private int trinfor;

  /** The trretro. */
  @Column(name = "TRRETRO", nullable = false)
  private int trretro;

  /** The trrespo. */
  @Column(name = "TRRESPO", nullable = false)
  private int trrespo;

  /** The trpagos. */
  @Column(name = "TRPAGOS", nullable = false)
  private int trpagos;

  /** The trotros. */
  @Column(name = "TROTROS", nullable = false)
  private int trotros;

  /** The trotros desc. */
  @Column(name = "TROTROS_DESC")
  private String trotrosDesc;

  /** The trfechallamada. */
  @Column(name = "TRFECHLLAMADA")
  private Date trfechallamada;

  /** The trobserv. */
  @Column(name = "TROBSERV")
  private String trobserv;

  public Long getPkTramiteSolicitud() {
    return pkTramiteSolicitud;
  }

  public void setPkTramiteSolicitud(Long pkTramiteSolicitud) {
    this.pkTramiteSolicitud = pkTramiteSolicitud;
  }

  public Date getTrfechcrea() {
    return UtilidadesCommons.cloneDate(trfechcrea);
  }

  public void setTrfechcrea(Date trfechcrea) {
    this.trfechcrea = UtilidadesCommons.cloneDate(trfechcrea);
  }

  public Date getTrfechregi() {
    return UtilidadesCommons.cloneDate(trfechregi);
  }

  public void setTrfechregi(Date trfechregi) {
    this.trfechregi = UtilidadesCommons.cloneDate(trfechregi);
  }

  public String getTrapellidos() {
    return trapellidos;
  }

  public void setTrapellidos(String trapellidos) {
    this.trapellidos = trapellidos;
  }

  public String getTrnombre() {
    return trnombre;
  }

  public void setTrnombre(String trnombre) {
    this.trnombre = trnombre;
  }

  public String getTrnudocide() {
    return trnudocide;
  }

  public void setTrnudocide(String trnudocide) {
    this.trnudocide = trnudocide;
  }

  public String getTrtelefono() {
    return trtelefono;
  }

  public void setTrtelefono(String trtelefono) {
    this.trtelefono = trtelefono;
  }

  public String getTrexpediente() {
    return trexpediente;
  }

  public void setTrexpediente(String trexpediente) {
    this.trexpediente = trexpediente;
  }

  public int getTrinfor() {
    return trinfor;
  }

  public void setTrinfor(int trinfor) {
    this.trinfor = trinfor;
  }

  public int getTrretro() {
    return trretro;
  }

  public void setTrretro(int trretro) {
    this.trretro = trretro;
  }

  public int getTrrespo() {
    return trrespo;
  }

  public void setTrrespo(int trrespo) {
    this.trrespo = trrespo;
  }

  public int getTrpagos() {
    return trpagos;
  }

  public void setTrpagos(int trpagos) {
    this.trpagos = trpagos;
  }

  public int getTrotros() {
    return trotros;
  }

  public void setTrotros(int trotros) {
    this.trotros = trotros;
  }

  public String getTrotrosDesc() {
    return trotrosDesc;
  }

  public void setTrotrosDesc(String trotrosDesc) {
    this.trotrosDesc = trotrosDesc;
  }

  public Date getTrfechallamada() {
    return UtilidadesCommons.cloneDate(trfechallamada);
  }

  public void setTrfechallamada(Date trfechallamada) {
    this.trfechallamada = UtilidadesCommons.cloneDate(trfechallamada);
  }

  public String getTrobserv() {
    return trobserv;
  }

  public void setTrobserv(String trobserv) {
    this.trobserv = trobserv;
  }
}
