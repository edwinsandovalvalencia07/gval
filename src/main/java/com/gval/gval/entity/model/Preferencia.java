package com.gval.gval.entity.model;

import com.gval.gval.common.ConstantesCommons;
import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import jakarta.persistence.*;

/**
 * The Class Preferencia.
 */
@Entity
@Table(name = "SDM_PREFSOL")
@GenericGenerator(name = "SDM_PREFSOL_PKPREFSOL_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {@Parameter(name = "sequence_name", value = "SEC_SDM_PREFSOL"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class Preferencia implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -7895544865409293155L;

  /** The pk prefsol. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_PREFSOL_PKPREFSOL_GENERATOR")
  @Column(name = "PK_PREFSOL", unique = true, nullable = false)
  private Long pkPrefsol;

  /** The solicitud. */
  // bi-directional many-to-one association to SdmSolicit
  @ManyToOne
  @JoinColumn(name = "PK_SOLICIT", nullable = false)
  private Solicitud solicitud;

  /** The tipo preferencia solicitud. */
  // bi-directional many-to-one association to SdxTipoPrefsol
  @ManyToOne
  @JoinColumn(name = "PK_TIPO_PREFSOL")
  private TipoPreferencia tipoPreferencia;

  /** The documento. */
  // bi-directional one-to-one association to SdmDocu
  @OneToOne
  @JoinColumn(name = "PK_DOCU")
  private Documento documento;

  /** The fecha peticion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "PRFECHA")
  private Date fechaPeticion;

  /** The estado. */
  @Column(name = "PRESTADO")
  private Boolean estado;

  /** The motivo. */
  @Column(name = "PRMOTIVO", length = 500)
  private String motivo;

  /** The app origen. */
  @Column(name = "APP_ORIGEN")
  private String appOrigen;

  /** The preferencias catalogo servicio. */
  @OneToMany(mappedBy = "preferencia")
  private List<PreferenciaCatalogoServicio> preferenciasCatalogoServicio;

  /**
   * Instantiates a new preferencia.
   */
  public Preferencia() {
    // Constructor
  }

  /**
   * On pre persist.
   */
  @PrePersist
  public void onPrePersist() {
    appOrigen = ConstantesCommons.APLICACION_ADA3;
  }

  /**
   * Gets the pk prefsol.
   *
   * @return the pk prefsol
   */
  public Long getPkPrefsol() {
    return pkPrefsol;
  }

  /**
   * Sets the pk prefsol.
   *
   * @param pkPrefsol the new pk prefsol
   */
  public void setPkPrefsol(final Long pkPrefsol) {
    this.pkPrefsol = pkPrefsol;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public Solicitud getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the new solicitud
   */
  public void setSolicitud(final Solicitud solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the tipo preferencia solicitud.
   *
   * @return the tipo preferencia solicitud
   */
  public TipoPreferencia getTipoPreferencia() {
    return tipoPreferencia;
  }

  /**
   * Sets the tipo preferencia solicitud.
   *
   * @param tipoPreferencia the new tipo preferencia solicitud
   */
  public void setTipoPreferencia(final TipoPreferencia tipoPreferencia) {
    this.tipoPreferencia = tipoPreferencia;
  }

  /**
   * Gets the documento.
   *
   * @return the documento
   */
  public Documento getDocumento() {
    return documento;
  }

  /**
   * Sets the documento.
   *
   * @param documento the new documento
   */
  public void setDocumento(final Documento documento) {
    this.documento = documento;
  }

  /**
   * Gets the fecha peticion.
   *
   * @return the fecha peticion
   */
  public Date getFechaPeticion() {
    return (fechaPeticion == null) ? null : (Date) fechaPeticion.clone();
  }

  /**
   * Sets the fecha peticion.
   *
   * @param fechaPeticion the new fecha peticion
   */
  public void setFechaPeticion(final Date fechaPeticion) {
    this.fechaPeticion =
        (fechaPeticion == null) ? null : (Date) fechaPeticion.clone();
  }

  /**
   * Gets the estado.
   *
   * @return the estado
   */
  public Boolean getEstado() {
    return estado;
  }

  /**
   * Sets the estado.
   *
   * @param estado the new estado
   */
  public void setEstado(final Boolean estado) {
    this.estado = estado;
  }

  /**
   * Gets the motivo.
   *
   * @return the motivo
   */
  public String getMotivo() {
    return motivo;
  }

  /**
   * Sets the motivo.
   *
   * @param motivo the new motivo
   */
  public void setMotivo(final String motivo) {
    this.motivo = motivo;
  }

  /**
   * Gets the preferencias catalogo servicio.
   *
   * @return the preferencias catalogo servicio
   */
  public List<PreferenciaCatalogoServicio> getPreferenciasCatalogoServicio() {
    return UtilidadesCommons.collectorsToList(preferenciasCatalogoServicio);
  }

  /**
   * Sets the preferencias catalogo servicio.
   *
   * @param preferenciasCatalogoServicio the new preferencias catalogo servicio
   */
  public void setPreferenciasCatalogoServicio(
      final List<PreferenciaCatalogoServicio> preferenciasCatalogoServicio) {
    this.preferenciasCatalogoServicio =
        UtilidadesCommons.collectorsToList(preferenciasCatalogoServicio);
  }

  /**
   * Gets the app origen.
   *
   * @return the app origen
   */
  public String getAppOrigen() {
    return appOrigen;
  }

  /**
   * Sets the app origen.
   *
   * @param appOrigen the new app origen
   */
  public void setAppOrigen(final String appOrigen) {
    this.appOrigen = appOrigen;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
