package com.gval.gval.entity.model;

import java.io.Serializable;

import jakarta.persistence.*;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "SDX_DIAGNOST_CIE10")
@GenericGenerator(name = "SDX_DIAGNOST_CIE10_PKDIAGNOSTCIE10_GENERATOR", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
		@Parameter(name = "sequence_name", value = "SEC_SDX_DIAGNOST_CIE10"),
		@Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1") })
public class DiagnosticoCie10 implements Serializable {

	private static final long serialVersionUID = 5195248811861635450L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SDX_DIAGNOST_CIE10_PKDIAGNOSTCIE10_GENERATOR")
	@Column(name = "PK_DIAGNOST_CIE10", unique = true, nullable = false)
	private Long pkDiagnosticCie10;

	@Column(name = "COD_ENFERMEDAD", length = 6)
	private String codEnfermedad;

	@Column(name = "DESC_ENFERMEDAD", length = 250)
	private String descEnfermedad;

	@Column(name = "COD_BLOQUE", length = 10)
	private String codBloque;

	@Column(name = "DESC_BLOQUE", length = 250)
	private String descBloque;

	@Column(name = "COD_CAPITULO", nullable = false)
	private Long codCapitulo;

	@Column(name = "DESC_CAPITULO", length = 250)
	private String descCapitulo;

	@Column(name = "COD_VAR_ENFERMEDAD", length = 6)
	private String codVarEnfermedad;

	@Column(name = "DESC_VAR_ENFERMEDAD", length = 250)
	private String descVarEnfermedad;

	@Column(name = "ACTIVO", nullable = false)
	private Long activo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PK_DIAGNOST")
	private Diagnostico diagnostico;

	public DiagnosticoCie10() {
		super();
	}

	public Long getPkDiagnosticCie10() {
		return pkDiagnosticCie10;
	}

	public void setPkDiagnosticCie10(Long pkDiagnosticCie10) {
		this.pkDiagnosticCie10 = pkDiagnosticCie10;
	}

	public String getCodEnfermedad() {
		return codEnfermedad;
	}

	public void setCodEnfermedad(String codEnfermedad) {
		this.codEnfermedad = codEnfermedad;
	}

	public String getDescEnfermedad() {
		return descEnfermedad;
	}

	public void setDescEnfermedad(String descEnfermedad) {
		this.descEnfermedad = descEnfermedad;
	}

	public String getCodBloque() {
		return codBloque;
	}

	public void setCodBloque(String codBloque) {
		this.codBloque = codBloque;
	}

	public String getDescBloque() {
		return descBloque;
	}

	public void setDescBloque(String descBloque) {
		this.descBloque = descBloque;
	}

	public Long getCodCapitulo() {
		return codCapitulo;
	}

	public void setCodCapitulo(Long codCapitulo) {
		this.codCapitulo = codCapitulo;
	}

	public String getDescCapitulo() {
		return descCapitulo;
	}

	public void setDescCapitulo(String descCapitulo) {
		this.descCapitulo = descCapitulo;
	}

	public String getCodVarEnfermedad() {
		return codVarEnfermedad;
	}

	public void setCodVarEnfermedad(String codVarEnfermedad) {
		this.codVarEnfermedad = codVarEnfermedad;
	}

	public String getDescVarEnfermedad() {
		return descVarEnfermedad;
	}

	public void setDescVarEnfermedad(String descVarEnfermedad) {
		this.descVarEnfermedad = descVarEnfermedad;
	}

	public Long getActivo() {
		return activo;
	}

	public void setActivo(Long activo) {
		this.activo = activo;
	}

	public Diagnostico getDiagnost() {
		return diagnostico;
	}

	public void setDiagnost(Diagnostico diagnostico) {
		this.diagnostico = diagnostico;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

}
