package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;

/**
 * The persistent class for the SDX_TIPOSOLI database table.
 *
 */
@Entity
@Table(name = "SDX_TIPOSOLI")
@GenericGenerator(name = "SDM_TIPOSOLI_PKTIPOSOLI_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDX_TIPOSOLI"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class TipoSolicitud implements Serializable {


  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -3741421689062726274L;

  /** The pk tipo solicitud. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_TIPOSOLI_PKTIPOSOLI_GENERATOR")
  @Column(name = "PK_TIPOSOLI", nullable = false)
  private Long pkTipoSolicitud;

  /**
   * "Codigo de tipo de solicitud. Valores fijos: VA - Inicial de valoracion HO
   * - Inicial de homologacion RE - Revision".
   */
  @Column(name = "TICODIGO", nullable = false, length = 2)
  private String codigo;

  /** Fecha en que se da de alta el registro. */
  @Temporal(TemporalType.DATE)
  @Column(name = "TIFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** Nombre del Tipo de Solicitud. */
  @Column(name = "TINOMBRE", nullable = false, length = 50)
  private String nombre;

  /**
   * Indica un borrado logico si no esta activo, en caso contrario esta vigente.
   */
  @Column(name = "TISACTIVO", nullable = false, length = 1)
  private Boolean activo;

  /**
   * Instantiates a new tipo solicitud.
   */
  public TipoSolicitud() {
    // Constructor
  }

  /**
   * Gets the pk tipo solicitud.
   *
   * @return the pk tipo solicitud
   */
  public Long getPkTipoSolicitud() {
    return pkTipoSolicitud;
  }

  /**
   * Sets the pk tipo solicitud.
   *
   * @param pkTipoSolicitud the new pk tipo solicitud
   */
  public void setPkTipoSolicitud(final Long pkTipoSolicitud) {
    this.pkTipoSolicitud = pkTipoSolicitud;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return fechaCreacion;
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
