package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;

import jakarta.persistence.*;



/**
 * The persistent class for the SDM_PREFEREN database table.
 *
 */
@Entity
@Table(name = "SDM_PREFEREN")
@GenericGenerator(name = "SDM_PREFEREN_PKPREFEREN_GENERATOR",
strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
parameters = {
    @Parameter(name = "sequence_name", value = "SEC_SDM_PREFEREN"),
    @Parameter(name = "initial_value", value = "1"),
    @Parameter(name = "increment_size", value = "1")})
public class PreferenciaInformeSocial implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -8002147513795636385L;

  /** The pk preferencia. */
  @Id
  @Column(name = "PK_PREFEREN", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
  generator = "SDM_PREFEREN_PKPREFEREN_GENERATOR")
  private Long pkPreferencia;

  /** The codigo primer centro. */
  @Column(name = "PRCOPRCENTRO", length = 6)
  private String codigoPrimerCentro;

  /** The codigo segundo centro. */
  @Column(name = "PRCOSECENTRO", length = 6)
  private String codigoSegundoCentro;

  /** The codigo tercer centro. */
  @Column(name = "PRCOTECENTRO", length = 6)
  private String codigoTercerCentro;

  /** The intensidad. */
  @Column(name = "PRINTENSID", length = 1)
  private String intensidad;

  /** The nombre primer centro. */
  @Column(name = "PRNOPRCENTRO", length = 100)
  private String nombrePrimerCentro;

  /** The nombre segundo centro. */
  @Column(name = "PRNOSECENTRO", length = 100)
  private String nombreSegundoCentro;

  /** The nombre tercer centro. */
  @Column(name = "PRNOTECENTRO", length = 100)
  private String nombreTercerCentro;

  /** The orden profesional. */
  @Column(name = "PRORDENPRO")
  private Long ordenProfesional;

  /** The orden solicitud. */
  @Column(name = "PRORDENSOL")
  private Long ordenSolicitud;

  /** The tipo preferencia. */
  @Column(name = "PRPREFVAL")
  private Long tipoPreferencia;

  /** The tipo. */
  @Column(name = "PRTIPO", nullable = false)
  private boolean nuevaPreferencia;

  /** The tipo modalidad. */
  @Column(name = "PRTIPOMODA", length = 1)
  private String tipoModalidad;

  /** The informe social. */
  // bi-directional many-to-one association to SdmInfosoci
  @OneToOne
  @JoinColumn(name = "PK_INFOSOCI", nullable = false,
  referencedColumnName = "PK_INFOSOCI")
  private InformeSocial informeSocial;

  /** The preferencia catalogo servicio. */
  // bi-directional many-to-one association to SdmPrefCataserv
  @ManyToOne
  @JoinColumn(name = "PK_PREF_CATASERV")
  private PreferenciaCatalogoServicio preferenciaCatalogoServicio;

  /** The catalogo servicio. */
  // bi-directional many-to-one association to SdxCataserv
  @ManyToOne
  @JoinColumn(name = "PK_CATASERV", nullable = false)
  private CatalogoServicio catalogoServicio;

  /** The catalogo servicio. */
  // bi-directional many-to-one association to SdxCataserv
  @ManyToOne
  @JoinColumn(name = "PK_CATASERV_ADECUADO")
  private CatalogoServicio recursoPrescripcion;

  /** The version infosoci. */
  @Column(name = "PRMODALIDAD")
  private Long modalidadTeleasistencia;

  /** The motivo movil. */
  @Column(name = "PRMOTIVOMOVIL")
  private String motivoMovil;

  /** The motivo no idoneidad. */
  @Column(name = "PRMOTIVONOIDONEIDAD", length = 4000)
  private String motivoNoIdoneidad;

  /** The recurso idoneo. */
  @Column(name = "PRRECURSOIDONEO")
  private Boolean recursoIdoneo;

  // columnas nuevo formato

  /** The no idoneo informado. */
  @Column(name = "PRNOIDONEOINFO")
  private Boolean noIdoneoInformado;

  /** The catalogo explicado. */
  @Column(name = "PRCATAEXPLICADO")
  private Boolean catalogoExplicado;

  @Column(name = "PRCONTINUARPREST")
  private Boolean continuarPrestacion;

  /** The solicita internamiento. */
  @Column(name = "PRSOLICITANTERNAM")
  private Boolean solicitaInternamiento;

  /** The informado fiscalia. */
  @Column(name = "PRINFORMADOFISCAL")
  private Boolean informadoFiscalia;

  /** The informado fiscalia. */
  @Column(name = "PRINGVOLUNTARIO")
  private Boolean ingresoVoluntario;

  /** The recurso adecuado valoracion. */
  @Column(name = "PRRECURSOADECUADOVAL", length = 250)
  private String recursoAdecuadoValoracion;

  /** The aumento horas. */
  @Column(name = "PRAUMENTOHORAS")
  private Boolean aumentoHoras;

  /** The tipo centro preferencia. */
  @Column(name = "PRTIPOCENTRO")
  private Long tipoCentroPreferencia;

  // IT de Seguimiento PIA AP
  /** The incumple proyecto AP. */
  @Column(name = "PRINCUMPLEPROYAP")
  private Boolean incumpleProyectoAP;

  /** The incumple requisitos AP. */
  @Column(name = "PRINCUMPLEREQAP")
  private Boolean incumpleRequisitosAP;

  /** The incumple persona dep. */
  @Column(name = "PRINCUMPLEPERSONADEP")
  private Boolean incumplePersonaDep;

  /** The cambio situacion persona. */
  @Column(name = "PRCAMBSITUPERSONADEP")
  private Boolean cambioSituacionPersona;

  /** The motivo inclumple PVI. */
  @Column(name = "PRMOTIVOINCUMPLEPVI")
  private Boolean otrosIncumplePVI;

  /** The otros motivos incumple PVI. */
  @Column(name = "PROTROSINCUMPLEPVI", length = 250)
  private String otrosMotivosIncumplePVI;

  /** The cambio permite PVI. */
  @Column(name = "PRCAMBIOPERMITEPVI")
  private Boolean cambioPermitePVI;

  /**
   * Instantiates a new preferencia informe social.
   */
  public PreferenciaInformeSocial() {
    // Constructor vacío
  }

  /**
   * Gets the pk preferencia.
   *
   * @return the pkPreferencia
   */
  public Long getPkPreferencia() {
    return pkPreferencia;
  }

  /**
   * Sets the pk preferencia.
   *
   * @param pkPreferencia the pkPreferencia to set
   */
  public void setPkPreferencia(final Long pkPreferencia) {
    this.pkPreferencia = pkPreferencia;
  }

  /**
   * Gets the codigo primer centro.
   *
   * @return the codigoPrimerCentro
   */
  public String getCodigoPrimerCentro() {
    return codigoPrimerCentro;
  }

  /**
   * Sets the codigo primer centro.
   *
   * @param codigoPreferenciaCentro the new codigo primer centro
   */
  public void setCodigoPrimerCentro(final String codigoPreferenciaCentro) {
    this.codigoPrimerCentro = codigoPreferenciaCentro;
  }

  /**
   * Gets the codigo segundo centro.
   *
   * @return the codigoSegundoCentro
   */
  public String getCodigoSegundoCentro() {
    return codigoSegundoCentro;
  }

  /**
   * Sets the codigo segundo centro.
   *
   * @param codigoSedeCentro the new codigo segundo centro
   */
  public void setCodigoSegundoCentro(final String codigoSedeCentro) {
    this.codigoSegundoCentro = codigoSedeCentro;
  }

  /**
   * Gets the codigo tercer centro.
   *
   * @return the codigoTercerCentro
   */
  public String getCodigoTercerCentro() {
    return codigoTercerCentro;
  }

  /**
   * Sets the codigo tercer centro.
   *
   * @param codigoTerrenoCentro the new codigo tercer centro
   */
  public void setCodigoTercerCentro(final String codigoTerrenoCentro) {
    this.codigoTercerCentro = codigoTerrenoCentro;
  }

  /**
   * Gets the intensidad.
   *
   * @return the intensidad
   */
  public String getIntensidad() {
    return intensidad;
  }

  /**
   * Sets the intensidad.
   *
   * @param intensidad the intensidad to set
   */
  public void setIntensidad(final String intensidad) {
    this.intensidad = intensidad;
  }

  /**
   * Gets the nombre primer centro.
   *
   * @return the nombrePrimerCentro
   */
  public String getNombrePrimerCentro() {
    return nombrePrimerCentro;
  }

  /**
   * Sets the nombre primer centro.
   *
   * @param nombrePreferenciaCentro the new nombre primer centro
   */
  public void setNombrePrimerCentro(final String nombrePreferenciaCentro) {
    this.nombrePrimerCentro = nombrePreferenciaCentro;
  }

  /**
   * Gets the nombre segundo centro.
   *
   * @return the nombreSegundoCentro
   */
  public String getNombreSegundoCentro() {
    return nombreSegundoCentro;
  }

  /**
   * Sets the nombre segundo centro.
   *
   * @param nombreSedeCentro the new nombre segundo centro
   */
  public void setNombreSegundoCentro(final String nombreSedeCentro) {
    this.nombreSegundoCentro = nombreSedeCentro;
  }

  /**
   * Gets the nombre tercer centro.
   *
   * @return the nombreTercerCentro
   */
  public String getNombreTercerCentro() {
    return nombreTercerCentro;
  }

  /**
   * Sets the nombre tercer centro.
   *
   * @param nombreTerrenoCentro the new nombre tercer centro
   */
  public void setNombreTercerCentro(final String nombreTerrenoCentro) {
    this.nombreTercerCentro = nombreTerrenoCentro;
  }

  /**
   * Gets the orden profesional.
   *
   * @return the ordenProfesional
   */
  public Long getOrdenProfesional() {
    return ordenProfesional;
  }

  /**
   * Sets the orden profesional.
   *
   * @param ordenProfesional the ordenProfesional to set
   */
  public void setOrdenProfesional(final Long ordenProfesional) {
    this.ordenProfesional = ordenProfesional;
  }

  /**
   * Gets the orden solicitud.
   *
   * @return the ordenSolicitud
   */
  public Long getOrdenSolicitud() {
    return ordenSolicitud;
  }

  /**
   * Sets the orden solicitud.
   *
   * @param ordenSolicitud the ordenSolicitud to set
   */
  public void setOrdenSolicitud(final Long ordenSolicitud) {
    this.ordenSolicitud = ordenSolicitud;
  }

  /**
   * Gets the tipo preferencia.
   *
   * @return the tipoPreferencia
   */
  public Long getTipoPreferencia() {
    return tipoPreferencia;
  }

  /**
   * Sets the tipo preferencia.
   *
   * @param preferenciaValoracion the new tipo preferencia
   */
  public void setTipoPreferencia(final Long preferenciaValoracion) {
    this.tipoPreferencia = preferenciaValoracion;
  }

  /**
   * Gets the tipo modalidad.
   *
   * @return the tipoModalidad
   */
  public String getTipoModalidad() {
    return tipoModalidad;
  }

  /**
   * Sets the tipo modalidad.
   *
   * @param tipoModalidad the tipoModalidad to set
   */
  public void setTipoModalidad(final String tipoModalidad) {
    this.tipoModalidad = tipoModalidad;
  }

  /**
   * Gets the informe social.
   *
   * @return the informeSocial
   */
  public InformeSocial getInformeSocial() {
    return informeSocial;
  }

  /**
   * Sets the informe social.
   *
   * @param informeSocial the informeSocial to set
   */
  public void setInformeSocial(final InformeSocial informeSocial) {
    this.informeSocial = informeSocial;
  }

  /**
   * Gets the preferencia catalogo servicio.
   *
   * @return the preferenciaCatalogoServicio
   */
  public PreferenciaCatalogoServicio getPreferenciaCatalogoServicio() {
    return preferenciaCatalogoServicio;
  }

  /**
   * Sets the preferencia catalogo servicio.
   *
   * @param preferenciaCatalogoServicio the preferenciaCatalogoServicio to set
   */
  public void setPreferenciaCatalogoServicio(
      final PreferenciaCatalogoServicio preferenciaCatalogoServicio) {
    this.preferenciaCatalogoServicio = preferenciaCatalogoServicio;
  }

  /**
   * Gets the catalogo servicio.
   *
   * @return the catalogoServicio
   */
  public CatalogoServicio getCatalogoServicio() {
    return catalogoServicio;
  }

  /**
   * Sets the catalogo servicio.
   *
   * @param catalogoServicio the catalogoServicio to set
   */
  public void setCatalogoServicio(final CatalogoServicio catalogoServicio) {
    this.catalogoServicio = catalogoServicio;
  }

  /**
   * Gets the modalidad teleasistencia.
   *
   * @return the modalidad teleasistencia
   */
  public Long getModalidadTeleasistencia() {
    return modalidadTeleasistencia;
  }

  /**
   * Sets the modalidad teleasistencia.
   *
   * @param modalidadTeleasistencia the new modalidad teleasistencia
   */
  public void setModalidadTeleasistencia(final Long modalidadTeleasistencia) {
    this.modalidadTeleasistencia = modalidadTeleasistencia;
  }

  /**
   * Gets the motivo movil.
   *
   * @return the motivo movil
   */
  public String getMotivoMovil() {
    return motivoMovil;
  }

  /**
   * Sets the motivo movil.
   *
   * @param motivoMovil the new motivo movil
   */
  public void setMotivoMovil(final String motivoMovil) {
    this.motivoMovil = motivoMovil;
  }

  /**
   * Gets the motivo no idoneidad.
   *
   * @return the motivo no idoneidad
   */
  public String getMotivoNoIdoneidad() {
    return motivoNoIdoneidad;
  }

  /**
   * Sets the motivo no idoneidad.
   *
   * @param motivoNoIdoneidad the new motivo no idoneidad
   */
  public void setMotivoNoIdoneidad(final String motivoNoIdoneidad) {
    this.motivoNoIdoneidad = motivoNoIdoneidad;
  }

  /**
   * Gets the recurso idoneo.
   *
   * @return the recurso idoneo
   */
  public Boolean getRecursoIdoneo() {
    return recursoIdoneo;
  }

  /**
   * Sets the recurso idoneo.
   *
   * @param recursoIdoneo the new recurso idoneo
   */
  public void setRecursoIdoneo(final Boolean recursoIdoneo) {
    this.recursoIdoneo = recursoIdoneo;
  }

  /**
   * Gets the es nueva preferencia.
   *
   * @return the es nueva preferencia
   */
  public boolean isNuevaPreferencia() {
    return nuevaPreferencia;
  }

  /**
   * Sets the es nueva preferencia.
   *
   * @param nuevaPreferencia the new nueva preferencia
   */
  public void setNuevaPreferencia(final boolean nuevaPreferencia) {
    this.nuevaPreferencia = nuevaPreferencia;
  }

  /**
   * Gets the no idoneo informado.
   *
   * @return the no idoneo informado
   */
  public Boolean getNoIdoneoInformado() {
    return noIdoneoInformado;
  }

  /**
   * Sets the no idoneo informado.
   *
   * @param noIdoneoInformado the new no idoneo informado
   */
  public void setNoIdoneoInformado(final Boolean noIdoneoInformado) {
    this.noIdoneoInformado = noIdoneoInformado;
  }

  /**
   * Gets the catalogo explicado.
   *
   * @return the catalogo explicado
   */
  public Boolean getCatalogoExplicado() {
    return catalogoExplicado;
  }

  /**
   * Sets the catalogo explicado.
   *
   * @param catalogoExplicado the new catalogo explicado
   */
  public void setCatalogoExplicado(final Boolean catalogoExplicado) {
    this.catalogoExplicado = catalogoExplicado;
  }

  /**
   * Gets the solicita internamiento.
   *
   * @return the solicita internamiento
   */
  public Boolean getSolicitaInternamiento() {
    return solicitaInternamiento;
  }

  /**
   * Sets the solicita internamiento.
   *
   * @param solicitaInternamiento the new solicita internamiento
   */
  public void setSolicitaInternamiento(final Boolean solicitaInternamiento) {
    this.solicitaInternamiento = solicitaInternamiento;
  }

  /**
   * Gets the informado fiscalia.
   *
   * @return the informado fiscalia
   */
  public Boolean getInformadoFiscalia() {
    return informadoFiscalia;
  }

  /**
   * Sets the informado fiscalia.
   *
   * @param informadoFiscalia the new informado fiscalia
   */
  public void setInformadoFiscalia(final Boolean informadoFiscalia) {
    this.informadoFiscalia = informadoFiscalia;
  }

  /**
   * Gets the recurso adecuado valoracion.
   *
   * @return the recurso adecuado valoracion
   */
  public String getRecursoAdecuadoValoracion() {
    return recursoAdecuadoValoracion;
  }

  /**
   * Sets the recurso adecuado valoracion.
   *
   * @param recursoAdecuadoValoracion the new recurso adecuado valoracion
   */
  public void setRecursoAdecuadoValoracion(
      final String recursoAdecuadoValoracion) {
    this.recursoAdecuadoValoracion = recursoAdecuadoValoracion;
  }

  /**
   * Gets the aumento horas.
   *
   * @return the aumento horas
   */
  public Boolean getAumentoHoras() {
    return aumentoHoras;
  }

  /**
   * Sets the aumento horas.
   *
   * @param aumentoHoras the new aumento horas
   */
  public void setAumentoHoras(final Boolean aumentoHoras) {
    this.aumentoHoras = aumentoHoras;
  }

  /**
   * Gets the tipo centro preferencia.
   *
   * @return the tipo centro preferencia
   */
  public Long getTipoCentroPreferencia() {
    return tipoCentroPreferencia;
  }

  /**
   * Sets the tipo centro preferencia.
   *
   * @param tipoCentroPreferencia the new tipo centro preferencia
   */
  public void setTipoCentroPreferencia(final Long tipoCentroPreferencia) {
    this.tipoCentroPreferencia = tipoCentroPreferencia;
  }



  /**
   * Gets the otros incumple PVI.
   *
   * @return the otros incumple PVI
   */
  public Boolean getOtrosIncumplePVI() {
    return otrosIncumplePVI;
  }

  /**
   * Sets the otros incumple PVI.
   *
   * @param otrosIncumplePVI the new otros incumple PVI
   */
  public void setOtrosIncumplePVI(final Boolean otrosIncumplePVI) {
    this.otrosIncumplePVI = otrosIncumplePVI;
  }

  /**
   * Gets the otros motivos incumple PVI.
   *
   * @return the otros motivos incumple PVI
   */
  public String getOtrosMotivosIncumplePVI() {
    return otrosMotivosIncumplePVI;
  }

  /**
   * Sets the otros motivos incumple PVI.
   *
   * @param otrosMotivosIncumplePVI the new otros motivos incumple PVI
   */
  public void setOtrosMotivosIncumplePVI(final String otrosMotivosIncumplePVI) {
    this.otrosMotivosIncumplePVI = otrosMotivosIncumplePVI;
  }

  /**
   * Gets the cambio permite PVI.
   *
   * @return the cambio permite PVI
   */
  public Boolean getCambioPermitePVI() {
    return cambioPermitePVI;
  }

  /**
   * Sets the cambio permite PVI.
   *
   * @param cambioPermitePVI the new cambio permite PVI
   */
  public void setCambioPermitePVI(final Boolean cambioPermitePVI) {
    this.cambioPermitePVI = cambioPermitePVI;
  }

  /**
   * Gets the incumple proyecto AP.
   *
   * @return the incumple proyecto AP
   */
  public Boolean getIncumpleProyectoAP() {
    return incumpleProyectoAP;
  }

  /**
   * Sets the incumple proyecto AP.
   *
   * @param incumpleProyectoAP the new incumple proyecto AP
   */
  public void setIncumpleProyectoAP(final Boolean incumpleProyectoAP) {
    this.incumpleProyectoAP = incumpleProyectoAP;
  }

  /**
   * Gets the incumple requisitos AP.
   *
   * @return the incumple requisitos AP
   */
  public Boolean getIncumpleRequisitosAP() {
    return incumpleRequisitosAP;
  }

  /**
   * Sets the incumple requisitos AP.
   *
   * @param incumpleRequisitosAP the new incumple requisitos AP
   */
  public void setIncumpleRequisitosAP(final Boolean incumpleRequisitosAP) {
    this.incumpleRequisitosAP = incumpleRequisitosAP;
  }

  /**
   * Gets the incumple persona dep.
   *
   * @return the incumple persona dep
   */
  public Boolean getIncumplePersonaDep() {
    return incumplePersonaDep;
  }

  /**
   * Sets the incumple persona dep.
   *
   * @param incumplePersonaDep the new incumple persona dep
   */
  public void setIncumplePersonaDep(final Boolean incumplePersonaDep) {
    this.incumplePersonaDep = incumplePersonaDep;
  }

  /**
   * Gets the cambio situacion persona.
   *
   * @return the cambio situacion persona
   */
  public Boolean getCambioSituacionPersona() {
    return cambioSituacionPersona;
  }

  /**
   * Sets the cambio situacion persona.
   *
   * @param cambioSituacionPersona the new cambio situacion persona
   */
  public void setCambioSituacionPersona(final Boolean cambioSituacionPersona) {
    this.cambioSituacionPersona = cambioSituacionPersona;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

  public CatalogoServicio getRecursoPrescripcion() {
    return recursoPrescripcion;
  }

  public void setRecursoPrescripcion(final CatalogoServicio recursoPrescripcion) {
    this.recursoPrescripcion = recursoPrescripcion;
  }

  public Boolean getIngresoVoluntario() {
    return ingresoVoluntario;
  }

  public void setIngresoVoluntario(final Boolean ingresoVoluntario) {
    this.ingresoVoluntario = ingresoVoluntario;
  }

  public Boolean getContinuarPrestacion() {
    return continuarPrestacion;
  }

  public void setContinuarPrestacion(Boolean continuarPrestacion) {
    this.continuarPrestacion = continuarPrestacion;
  }

}
