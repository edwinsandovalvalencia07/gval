package com.gval.gval.entity.model;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.gval.gval.common.UtilidadesCommons;

// TODO: Auto-generated Javadoc
/**
 * The persistent class for the SDM_ESTUDIO database table.
 *
 */
@Entity
@Table(name = "SDM_ESTUDIO")
@GenericGenerator(name = "SDM_ESTUDIO_PKESTUDIO_GENERATOR", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
		@Parameter(name = "sequence_name", value = "SEC_SDM_ESTUDIO"), @Parameter(name = "initial_value", value = "1"),
		@Parameter(name = "increment_size", value = "1") })
public class Estudio implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4983218728438266610L;

	/** The pk estudio. */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SDM_ESTUDIO_PKESTUDIO_GENERATOR")
	@Column(name = "PK_ESTUDIO", unique = true, nullable = false)
	private Long pkEstudio;

	/** The activo. */
	@Column(name = "ESACTIVO", nullable = false, precision = 38)
	private Boolean activo;

	/** The estado. */
	@Column(name = "ESESTADO", nullable = false, length = 1)
	private String estado;

	/** The fecha fin. */
	@Temporal(TemporalType.DATE)
	@Column(name = "ESFECHAFIN")
	private Date fechaFin;

	/** The fecha inicio. */
	@Temporal(TemporalType.DATE)
	@Column(name = "ESFECHAINI")
	private Date fechaInicio;

	/** The fecha aprobacion. */
	@Temporal(TemporalType.DATE)
	@Column(name = "ESFECHAPRO")
	private Date fechaAprobacion;

	/** The fecha creacion. */
	@Temporal(TemporalType.DATE)
	@Column(name = "ESFECHCREA", nullable = false)
	private Date fechaCreacion;

	/** The motivo informe salud fisico. */
	@Column(name = "ESMOINSAFI", length = 4000)
	private String motivoInformeSaludFisico;

	/** The motivo informe salud mental. */
	@Column(name = "ESMOINSAME", length = 4000)
	private String motivoInformeSaludMental;

	/** The motivo informe salud. */
	@Column(name = "ESMOTIINSA", length = 4000)
	private String motivoInformeSalud;

	/** The motivo informe social. */
	@Column(name = "ESMOTIINSO", length = 4000)
	private String motivoInformeSocial;

	/** The motivo valoracion. */
	@Column(name = "ESMOTIVALO", length = 4000)
	private String motivoValoracion;

	/** The observaciones. */
	@Column(name = "ESOBSERVAC", length = 4000)
	private String observaciones;

	/** The otro tipo estudio. */
	@Column(name = "ESOTROTIPO", length = 250)
	private String otroTipoEstudio;

	/** The repeticion informe salud fisico. */
	@Column(name = "ESREINSAFI", precision = 38)
	private Boolean repeticionInformeSaludFisico;

	/** The repeticion informe salud mental. */
	@Column(name = "ESREINSAME", precision = 38)
	private Boolean repeticionInformeSaludMental;

	/** The repeticion informe salud. */
	@Column(name = "ESREPEINSA", precision = 38)
	private Boolean repeticionInformeSalud;

	/** The repeticion informe social. */
	@Column(name = "ESREPEINSO", precision = 38)
	private Boolean repeticionInformeSocial;

	/** The repeticion informe valoracion. */
	@Column(name = "ESREPEVALO", precision = 38)
	private Boolean repeticionInformeValoracion;

	/** The resultado. */
	@Column(name = "ESRESULTAD", precision = 38)
	private Boolean resultado;

	/** The tipo. */
	@Column(name = "ESTIPO", nullable = false, length = 1)
	private String tipo;

	/** The tipo tipoDictamen. */
	@Column(name = "ESTIPODICT", length = 1)
	private String tipoDictamen;

	/** The urgente. */
	@Column(name = "ESURGENTE", precision = 38)
	private Boolean urgente;

	/** The migrado sidep. */
	@Column(name = "MIGRADO_SIDEP", length = 1)
	private String migradoSidep;

	/** The documento urgencia. */
	@ManyToOne
	@JoinColumn(name = "PK_DOCU_URGENCIA")
	private DocumentoAportado documentoUrgencia;

	/** The dictamen. */
	@ManyToOne
	@JoinColumn(name = "PK_DICTAMEN", unique = true, nullable = false)
	private Dictamen dictamen;

	/** The documento. */
	// bi-directional many-to-one association to SdmDocu
	@ManyToOne
	@JoinColumn(name = "PK_DOCU")
	private Documento documento;

	/** The solicitud. */
	// bi-directional many-to-one association to SdmSolicit
	@ManyToOne
	@JoinColumn(name = "PK_SOLICIT", nullable = false)
	private Solicitud solicitud;

	/** The usuario 1. */
	// bi-directional many-to-one association to SdxUsuario
	@ManyToOne
	@JoinColumn(name = "PK_PERSONA2", nullable = false)
	private Usuario usuarioCreador;

	/** The usuario 2. */
	// bi-directional many-to-one association to SdxUsuario
	@ManyToOne
	@JoinColumn(name = "PK_PERSONA")
	private Usuario usuarioCierre;

	/** The sdm acta. */
	@ManyToOne
	@JoinColumn(name = "PK_ACTA")
	private Acta sdmActa;

	/** The sdm actacorrectiva. */
	@ManyToOne
	@JoinColumn(name = "PK_ACTACORR")
	private Acta sdmActacorrectiva;

	/**
	 * Instantiates a new estudio.
	 */
	public Estudio() {
		// Empty constructor
	}

	/**
	 * Gets the pk estudio.
	 *
	 * @return the pk estudio
	 */
	public long getPkEstudio() {
		return pkEstudio;
	}

	/**
	 * Sets the pk estudio.
	 *
	 * @param pkEstudio the new pk estudio
	 */
	public void setPkEstudio(final long pkEstudio) {
		this.pkEstudio = pkEstudio;
	}

	/**
	 * Gets the activo.
	 *
	 * @return the activo
	 */
	public Boolean getActivo() {
		return activo;
	}

	/**
	 * Sets the activo.
	 *
	 * @param activo the new activo
	 */
	public void setActivo(final Boolean activo) {
		this.activo = activo;
	}

	/**
	 * Gets the estado.
	 *
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * Sets the estado.
	 *
	 * @param estado the new estado
	 */
	public void setEstado(final String estado) {
		this.estado = estado;
	}

	/**
	 * Gets the fecha fin.
	 *
	 * @return the fecha fin
	 */
	public Date getFechaFin() {
		return UtilidadesCommons.cloneDate(fechaFin);
	}

	/**
	 * Sets the fecha fin.
	 *
	 * @param fechaFin the new fecha fin
	 */
	public void setFechaFin(final Date fechaFin) {
		this.fechaFin = UtilidadesCommons.cloneDate(fechaFin);
	}

	/**
	 * Gets the fecha inicio.
	 *
	 * @return the fecha inicio
	 */
	public Date getFechaInicio() {
		return UtilidadesCommons.cloneDate(fechaInicio);
	}

	/**
	 * Sets the fecha inicio.
	 *
	 * @param fechaInicio the new fecha inicio
	 */
	public void setFechaInicio(final Date fechaInicio) {
		this.fechaInicio = UtilidadesCommons.cloneDate(fechaInicio);
	}

	/**
	 * Gets the fecha aprobacion.
	 *
	 * @return the fecha aprobacion
	 */
	public Date getFechaAprobacion() {
		return UtilidadesCommons.cloneDate(fechaAprobacion);
	}

	/**
	 * Sets the fecha aprobacion.
	 *
	 * @param fechaAprobacion the new fecha aprobacion
	 */
	public void setFechaAprobacion(final Date fechaAprobacion) {
		this.fechaAprobacion = UtilidadesCommons.cloneDate(fechaAprobacion);
	}

	/**
	 * Gets the fecha creacion.
	 *
	 * @return the fecha creacion
	 */
	public Date getFechaCreacion() {
		return UtilidadesCommons.cloneDate(fechaCreacion);
	}

	/**
	 * Sets the fecha creacion.
	 *
	 * @param fechaCreacion the new fecha creacion
	 */
	public void setFechaCreacion(final Date fechaCreacion) {
		this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
	}

	/**
	 * Gets the motivo informe salud fisico.
	 *
	 * @return the motivo informe salud fisico
	 */
	public String getMotivoInformeSaludFisico() {
		return motivoInformeSaludFisico;
	}

	/**
	 * Sets the motivo informe salud fisico.
	 *
	 * @param motivoInformeSaludFisico the new motivo informe salud fisico
	 */
	public void setMotivoInformeSaludFisico(final String motivoInformeSaludFisico) {
		this.motivoInformeSaludFisico = motivoInformeSaludFisico;
	}

	/**
	 * Gets the motivo informe salud mental.
	 *
	 * @return the motivo informe salud mental
	 */
	public String getMotivoInformeSaludMental() {
		return motivoInformeSaludMental;
	}

	/**
	 * Sets the motivo informe salud mental.
	 *
	 * @param motivoInformeSaludMental the new motivo informe salud mental
	 */
	public void setMotivoInformeSaludMental(final String motivoInformeSaludMental) {
		this.motivoInformeSaludMental = motivoInformeSaludMental;
	}

	/**
	 * Gets the motivo informe salud.
	 *
	 * @return the motivo informe salud
	 */
	public String getMotivoInformeSalud() {
		return motivoInformeSalud;
	}

	/**
	 * Sets the motivo informe salud.
	 *
	 * @param motivoInformeSalud the new motivo informe salud
	 */
	public void setMotivoInformeSalud(final String motivoInformeSalud) {
		this.motivoInformeSalud = motivoInformeSalud;
	}

	/**
	 * Gets the motivo informe social.
	 *
	 * @return the motivo informe social
	 */
	public String getMotivoInformeSocial() {
		return motivoInformeSocial;
	}

	/**
	 * Sets the motivo informe social.
	 *
	 * @param motivoInformeSocial the new motivo informe social
	 */
	public void setMotivoInformeSocial(final String motivoInformeSocial) {
		this.motivoInformeSocial = motivoInformeSocial;
	}

	/**
	 * Gets the motivo valoracion.
	 *
	 * @return the motivo valoracion
	 */
	public String getMotivoValoracion() {
		return motivoValoracion;
	}

	/**
	 * Sets the motivo valoracion.
	 *
	 * @param motivoValoracion the new motivo valoracion
	 */
	public void setMotivoValoracion(final String motivoValoracion) {
		this.motivoValoracion = motivoValoracion;
	}

	/**
	 * Gets the observaciones.
	 *
	 * @return the observaciones
	 */
	public String getObservaciones() {
		return observaciones;
	}

	/**
	 * Sets the observaciones.
	 *
	 * @param observaciones the new observaciones
	 */
	public void setObservaciones(final String observaciones) {
		this.observaciones = observaciones;
	}

	/**
	 * Gets the otro tipo estudio.
	 *
	 * @return the otro tipo estudio
	 */
	public String getOtroTipoEstudio() {
		return otroTipoEstudio;
	}

	/**
	 * Sets the otro tipo estudio.
	 *
	 * @param otroTipoEstudio the new otro tipo estudio
	 */
	public void setOtroTipoEstudio(final String otroTipoEstudio) {
		this.otroTipoEstudio = otroTipoEstudio;
	}

	/**
	 * Gets the repeticion informe salud fisico.
	 *
	 * @return the repeticion informe salud fisico
	 */
	public Boolean getRepeticionInformeSaludFisico() {
		return repeticionInformeSaludFisico;
	}

	/**
	 * Sets the repeticion informe salud fisico.
	 *
	 * @param repeticionInformeSaludFisico the new repeticion informe salud fisico
	 */
	public void setRepeticionInformeSaludFisico(final Boolean repeticionInformeSaludFisico) {
		this.repeticionInformeSaludFisico = repeticionInformeSaludFisico;
	}

	/**
	 * Gets the repeticion informe salud mental.
	 *
	 * @return the repeticion informe salud mental
	 */
	public Boolean getRepeticionInformeSaludMental() {
		return repeticionInformeSaludMental;
	}

	/**
	 * Sets the repeticion informe salud mental.
	 *
	 * @param repeticionInformeSaludMental the new repeticion informe salud mental
	 */
	public void setRepeticionInformeSaludMental(final Boolean repeticionInformeSaludMental) {
		this.repeticionInformeSaludMental = repeticionInformeSaludMental;
	}

	/**
	 * Gets the repeticion informe salud.
	 *
	 * @return the repeticion informe salud
	 */
	public Boolean getRepeticionInformeSalud() {
		return repeticionInformeSalud;
	}

	/**
	 * Sets the repeticion informe salud.
	 *
	 * @param repeticionInformeSalud the new repeticion informe salud
	 */
	public void setRepeticionInformeSalud(final Boolean repeticionInformeSalud) {
		this.repeticionInformeSalud = repeticionInformeSalud;
	}

	/**
	 * Gets the repeticion informe social.
	 *
	 * @return the repeticion informe social
	 */
	public Boolean getRepeticionInformeSocial() {
		return repeticionInformeSocial;
	}

	/**
	 * Sets the repeticion informe social.
	 *
	 * @param repeticionInformeSocial the new repeticion informe social
	 */
	public void setRepeticionInformeSocial(final Boolean repeticionInformeSocial) {
		this.repeticionInformeSocial = repeticionInformeSocial;
	}

	/**
	 * Gets the repeticion informe valoracion.
	 *
	 * @return the repeticion informe valoracion
	 */
	public Boolean getRepeticionInformeValoracion() {
		return repeticionInformeValoracion;
	}

	/**
	 * Sets the repeticion informe valoracion.
	 *
	 * @param repeticionInformeValoracion the new repeticion informe valoracion
	 */
	public void setRepeticionInformeValoracion(final Boolean repeticionInformeValoracion) {
		this.repeticionInformeValoracion = repeticionInformeValoracion;
	}

	/**
	 * Gets the resultado.
	 *
	 * @return the resultado
	 */
	public Boolean getResultado() {
		return resultado;
	}

	/**
	 * Sets the resultado.
	 *
	 * @param resultado the new resultado
	 */
	public void setResultado(final Boolean resultado) {
		this.resultado = resultado;
	}

	/**
	 * Gets the tipo.
	 *
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Sets the tipo.
	 *
	 * @param tipo the new tipo
	 */
	public void setTipo(final String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Gets the tipo dictamen.
	 *
	 * @return the tipo dictamen
	 */
	public String getTipoDictamen() {
		return tipoDictamen;
	}

	/**
	 * Sets the tipo dictamen.
	 *
	 * @param tipoDictamen the new tipo dictamen
	 */
	public void setTipoDictamen(final String tipoDictamen) {
		this.tipoDictamen = tipoDictamen;
	}

	/**
	 * Gets the urgente.
	 *
	 * @return the urgente
	 */
	public Boolean getUrgente() {
		return urgente;
	}

	/**
	 * Sets the urgente.
	 *
	 * @param urgente the new urgente
	 */
	public void setUrgente(final Boolean urgente) {
		this.urgente = urgente;
	}

	/**
	 * Gets the migrado sidep.
	 *
	 * @return the migrado sidep
	 */
	public String getMigradoSidep() {
		return migradoSidep;
	}

	/**
	 * Sets the migrado sidep.
	 *
	 * @param migradoSidep the new migrado sidep
	 */
	public void setMigradoSidep(final String migradoSidep) {
		this.migradoSidep = migradoSidep;
	}

	/**
	 * Gets the documento urgencia.
	 *
	 * @return the documento urgencia
	 */
	public DocumentoAportado getDocumentoUrgencia() {
		return documentoUrgencia;
	}

	/**
	 * Sets the documento urgencia.
	 *
	 * @param documentoUrgencia the new documento urgencia
	 */
	public void setDocumentoUrgencia(final DocumentoAportado documentoUrgencia) {
		this.documentoUrgencia = documentoUrgencia;
	}

  /**
   * Gets the dictamen.
   *
   * @return the dictamen
   */
  public Dictamen getDictamen() {
    return dictamen;
  }

  /**
   * Sets the dictamen.
   *
   * @param dictamen the dictamen to set
   */
  public void setDictamen(final Dictamen dictamen) {
    this.dictamen = dictamen;
  }

	/**
	 * Gets the documento.
	 *
	 * @return the documento
	 */
	public Documento getDocumento() {
		return documento;
	}



	/**
	 * Sets the documento.
	 *
	 * @param documento the new documento
	 */
	public void setDocumento(final Documento documento) {
		this.documento = documento;
	}

	/**
	 * Gets the solicitud.
	 *
	 * @return the solicitud
	 */
	public Solicitud getSolicitud() {
		return solicitud;
	}

	/**
	 * Sets the solicitud.
	 *
	 * @param solicitud the new solicitud
	 */
	public void setSolicitud(final Solicitud solicitud) {
		this.solicitud = solicitud;
	}

	/**
	 * Gets the usuario 1.
	 *
	 * @return the usuario 1
	 */
	public Usuario getUsuarioCreador() {
		return usuarioCreador;
	}

	/**
	 * Sets the usuario 1.
	 *
	 * @param usuarioCreador the new usuario 1
	 */
	public void setUsuarioCreador(final Usuario usuarioCreador) {
		this.usuarioCreador = usuarioCreador;
	}

	/**
	 * Gets the usuario 2.
	 *
	 * @return the usuario 2
	 */
	public Usuario getUsuarioCierre() {
		return usuarioCierre;
	}

	/**
	 * Sets the usuario 2.
	 *
	 * @param usuarioCierre the new usuario 2
	 */
	public void setUsuarioCierre(final Usuario usuarioCierre) {
		this.usuarioCierre = usuarioCierre;
	}

	/**
	 * Gets the sdm acta.
	 *
	 * @return the sdm acta
	 */
	public Acta getSdmActa() {
		return sdmActa;
	}

	/**
	 * Sets the sdm acta.
	 *
	 * @param sdmActa the new sdm acta
	 */
	public void setSdmActa(final Acta sdmActa) {
		this.sdmActa = sdmActa;
	}

	/**
	 * Gets the sdm actacorrectiva.
	 *
	 * @return the sdm actacorrectiva
	 */
	public Acta getSdmActacorrectiva() {
		return sdmActacorrectiva;
	}

	/**
	 * Sets the sdm actacorrectiva.
	 *
	 * @param sdmActacorrectiva the new sdm actacorrectiva
	 */
	public void setSdmActacorrectiva(final Acta sdmActacorrectiva) {
		this.sdmActacorrectiva = sdmActacorrectiva;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

}
