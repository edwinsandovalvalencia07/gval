package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;

/**
 * The Class ComentarioIncidencia.
 */
@Entity
@Table(name = "SDM_COMNINCI")
@GenericGenerator(name = "SDM_COMNINCI_PKCOMNINCI_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_COMNINCI"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class ComentarioIncidencia implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -4033153019427428115L;

  /** The pk comninci. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_COMNINCI_PKCOMNINCI_GENERATOR")
  @Column(name = "PK_COMNINCI", unique = true, nullable = false)
  private Long pkComninci;

  /** The activo. */
  @Column(name = "COACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /** The comentario. */
  @Column(name = "COCOMENTAR", nullable = false, length = 4000)
  private String comentario;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "COFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The incidencia. */
  // bi-directional many-to-one association to SdmIncidenc
  @ManyToOne
  @JoinColumn(name = "PK_INCIDENC", nullable = false)
  private Incidencia incidencia;

  /** The usuario. */
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA", nullable = false)
  private Usuario usuario;

  /**
   * Instantiates a new comentario incidencia.
   */
  public ComentarioIncidencia() {
    // Constructor
  }

  /**
   * Gets the pk comninci.
   *
   * @return the pk comninci
   */
  public Long getPkComninci() {
    return pkComninci;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Gets the comentario.
   *
   * @return the comentario
   */
  public String getComentario() {
    return comentario;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the incidencia.
   *
   * @return the incidencia
   */
  public Incidencia getIncidencia() {
    return incidencia;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }

  /**
   * Sets the pk comninci.
   *
   * @param pkComninci the new pk comninci
   */
  public void setPkComninci(final Long pkComninci) {
    this.pkComninci = pkComninci;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Sets the comentario.
   *
   * @param comentario the new comentario
   */
  public void setComentario(final String comentario) {
    this.comentario = comentario;
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the incidencia.
   *
   * @param incidencia the new incidencia
   */
  public void setIncidencia(final Incidencia incidencia) {
    this.incidencia = incidencia;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
