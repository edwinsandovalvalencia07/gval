package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;
import jakarta.persistence.*;


/**
 * The persistent class for the SDX_TIPOVIA database table.
 *
 */
@Entity
@Table(name = "SDX_TIPOVIA")
public class TipoVia implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -1364588086159996019L;

  /** The pk tipo via. */
  @Id
  @Column(name = "PK_TIPOVIA", unique = true, nullable = false)
  private Long pkTipoVia;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "TIFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The nombre corto. */
  @Column(name = "TINOMBCORT", nullable = false, length = 10)
  private String nombreCorto;

  /** The nombre. */
  @Column(name = "TINOMBRE", nullable = false, length = 30)
  private String nombre;

  /** The activo. */
  @Column(name = "TIVACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /** The imserso. */
  @Column(name = "TOIMSERSO", length = 2)
  private String imserso;

  /**
   * Instantiates a new tipo via.
   */
  public TipoVia() {
    // Empty constructor
  }

  /**
   * Instantiates a new tipo via.
   *
   * @param pkTipoVia the pk tipo via
   * @param fechaCreacion the fecha creacion
   * @param nombreCorto the nombre corto
   * @param nombre the nombre
   * @param activo the activo
   * @param imserso the imserso
   */
  public TipoVia(final Long pkTipoVia, final Date fechaCreacion,
      final String nombreCorto, final String nombre, final Boolean activo,
      final String imserso) {
    super();
    this.pkTipoVia = pkTipoVia;
    this.fechaCreacion = fechaCreacion;
    this.nombreCorto = nombreCorto;
    this.nombre = nombre;
    this.activo = activo;
    this.imserso = imserso;
  }

  /**
   * Gets the pk tipo via.
   *
   * @return the pk tipo via
   */
  public Long getPkTipoVia() {
    return pkTipoVia;
  }

  /**
   * Sets the pk tipo via.
   *
   * @param pkTipoVia the new pk tipo via
   */
  public void setPkTipoVia(final Long pkTipoVia) {
    this.pkTipoVia = pkTipoVia;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return fechaCreacion;
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

  /**
   * Gets the nombre corto.
   *
   * @return the nombre corto
   */
  public String getNombreCorto() {
    return nombreCorto;
  }

  /**
   * Sets the nombre corto.
   *
   * @param nombreCorto the new nombre corto
   */
  public void setNombreCorto(final String nombreCorto) {
    this.nombreCorto = nombreCorto;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the imserso.
   *
   * @return the imserso
   */
  public String getImserso() {
    return imserso;
  }

  /**
   * Sets the imserso.
   *
   * @param imserso the new imserso
   */
  public void setImserso(final String imserso) {
    this.imserso = imserso;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
