package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

import jakarta.persistence.*;


/**
 * The persistent class for the SDX_COMARCA database table.
 *
 */
@Entity
@Table(name = "SDX_COMARCA")
public class Comarca implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 3393966079178286208L;


  /** The pk comarca. */
  @Id
  @Column(name = "PK_COMARCA", unique = true, nullable = false, precision = 2)
  private Long pkComarca;

  /** The codigo area. */
  @Column(name = "COD_AREA", precision = 2)
  private Long codigoArea;

  /** The nombre. */
  @Column(name = "NOM_COMA", length = 30)
  private String nombre;

  /** The provincia. */
  // bi-directional many-to-one association to SdxProvinci
  @ManyToOne
  @JoinColumn(name = "PK_PROVINCI")
  private Provincia provincia;

  /**
   * Instantiates a new comarca.
   */
  public Comarca() {
    // Empty constructor
  }

  /**
   * Instantiates a new comarca.
   *
   * @param pkComarca the pk comarca
   * @param codigoArea the codigo area
   * @param nombre the nombre
   * @param provincia the provincia
   */
  public Comarca(final Long pkComarca, final Long codigoArea,
      final String nombre, final Provincia provincia) {
    super();
    this.pkComarca = pkComarca;
    this.codigoArea = codigoArea;
    this.nombre = nombre;
    this.provincia = provincia;
  }

  /**
   * Gets the pk comarca.
   *
   * @return the pk comarca
   */
  public Long getPkComarca() {
    return pkComarca;
  }

  /**
   * Sets the pk comarca.
   *
   * @param pkComarca the new pk comarca
   */
  public void setPkComarca(final Long pkComarca) {
    this.pkComarca = pkComarca;
  }

  /**
   * Gets the codigo area.
   *
   * @return the codigo area
   */
  public Long getCodigoArea() {
    return codigoArea;
  }

  /**
   * Sets the codigo area.
   *
   * @param codigoArea the new codigo area
   */
  public void setCodigoArea(final Long codigoArea) {
    this.codigoArea = codigoArea;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the provincia.
   *
   * @return the provincia
   */
  public Provincia getProvincia() {
    return provincia;
  }

  /**
   * Sets the provincia.
   *
   * @param provincia the new provincia
   */
  public void setProvincia(final Provincia provincia) {
    this.provincia = provincia;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
