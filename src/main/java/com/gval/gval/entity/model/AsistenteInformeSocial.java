package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The Class AsistenteInformeSocial.
 */
@Entity
@Table(name = "SDM_ASISPERS_INFOSOCI")
@GenericGenerator(name = "SDM_ASISPERS_INFOSOCI_PKASISPERS_INFOSOCI_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_ASISPERS_INFOSOCI"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class AsistenteInformeSocial implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk asispers infosoci. */
  @Id
  @Column(name = "PK_ASISPERS_INFOSOCI", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_ASISPERS_INFOSOCI_PKASISPERS_INFOSOCI_GENERATOR")
  private Long pkAsispersInfosoci;

  /** The activo. */
  @Column(name = "APACTIVO", nullable = false)
  private Boolean activo;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "APFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The asistente. */
  // bi-directional many-to-one association to SdmAsispers
  @ManyToOne
  @JoinColumn(name = "PK_ASISPERS", nullable = false)
  private AsistentePersonal asistente;

  /** The informe social. */
  // bi-directional many-to-one association to SdmInfosoci
  @ManyToOne
  @JoinColumn(name = "PK_INFOSOCI", nullable = false)
  private InformeSocial informeSocial;

  /** The origen. */
  @Column(name = "APORIGEN")
  private String origen;

  /** The cumple requisitos. */
  @Column(name = "APCUMPLEREQ")
  private Boolean cumpleRequisitos;

  /** The cumple PVI. */
  @Column(name = "APCUMPLEPVI")
  private Boolean cumplePVI;

  /**
   * Instantiates a new asistente informe social.
   */
  public AsistenteInformeSocial() {
    // Constructor vacío
  }

  /**
   * Gets the pk asistente infosoci.
   *
   * @return the pk asistente infosoci
   */
  public Long getPkAsispersInfosoci() {
    return pkAsispersInfosoci;
  }

  /**
   * Sets the pk asistente infosoci.
   *
   * @param pkAsispersInfosoci the new pk asistente infosoci
   */
  public void setPkAsispersInfosoci(final Long pkAsispersInfosoci) {
    this.pkAsispersInfosoci = pkAsispersInfosoci;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the asistente.
   *
   * @return the asistente
   */
  public AsistentePersonal getAsistente() {
    return asistente;
  }

  /**
   * Sets the asistente.
   *
   * @param asistente the new asistente
   */
  public void setAsistente(final AsistentePersonal asistente) {
    this.asistente = asistente;
  }

  /**
   * Gets the informe social.
   *
   * @return the informe social
   */
  public InformeSocial getInformeSocial() {
    return informeSocial;
  }

  /**
   * Sets the informe social.
   *
   * @param informeSocial the new informe social
   */
  public void setInformeSocial(final InformeSocial informeSocial) {
    this.informeSocial = informeSocial;
  }

  /**
   * Gets the origen.
   *
   * @return the origen
   */
  public String getOrigen() {
    return origen;
  }

  /**
   * Sets the origen.
   *
   * @param origen the new origen
   */
  public void setOrigen(final String origen) {
    this.origen = origen;
  }

  /**
   * Gets the cumple requisitos.
   *
   * @return the cumple requisitos
   */
  public Boolean getCumpleRequisitos() {
    return cumpleRequisitos;
  }

  /**
   * Sets the cumple requisitos.
   *
   * @param cumpleRequisitos the new cumple requisitos
   */
  public void setCumpleRequisitos(final Boolean cumpleRequisitos) {
    this.cumpleRequisitos = cumpleRequisitos;
  }

  /**
   * Gets the cumple PVI.
   *
   * @return the cumple PVI
   */
  public Boolean getCumplePVI() {
    return cumplePVI;
  }

  /**
   * Sets the cumple PVI.
   *
   * @param cumplePVI the new cumple PVI
   */
  public void setCumplePVI(final Boolean cumplePVI) {
    this.cumplePVI = cumplePVI;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }



}
