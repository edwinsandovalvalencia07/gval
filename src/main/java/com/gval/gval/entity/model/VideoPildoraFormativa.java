package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;

/**
 * The Class VideoPildoraFormativa.
 */
@Entity
@Table(name = "TADA_PILDORAS_FORMATIVAS")
@GenericGenerator(
    name = "TADA_PILDORAS_FORMATIVAS_ID_PILDORA_FORMATIVA_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name",
            value = "SEC_TADA_PILDORAS_FORMATIVAS"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class VideoPildoraFormativa implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The id pildora formativa. */
  @Id
  @Column(name = "ID_PILDORA_FORMATIVA", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "TADA_PILDORAS_FORMATIVAS_ID_PILDORA_FORMATIVA_GENERATOR")
  private Long idPildoraFormativa;


  /** The captuilo. */
  @Column(name = "PF_CAPITULO", nullable = false)
  private Long capitulo;

  /** The nombre. */
  @Column(name = "PF_TITULO_LABEL", nullable = false)
  private String tituloLabel;

  /** The url. */
  @Column(name = "PF_URL", nullable = false)
  private String url;

  /** The activo. */
  @Column(name = "PF_ACTIVO", nullable = false)
  private boolean activo;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "PF_FECHA_CREACION", nullable = false)
  private Date fechaCreacion;

  /** The aplicacion. */
  @Column(name = "PF_APLICACION")
  private String aplicacion;

  /** The acces role. */
  @Column(name = "PF_ACCES_ROLE")
  private String accessRole;

  /**
   * Instantiates a new video pildora formativa.
   */
  public VideoPildoraFormativa() {
    super();
  }

  /**
   * Gets the id pildora formativa.
   *
   * @return the id pildora formativa
   */
  public Long getIdPildoraFormativa() {
    return idPildoraFormativa;
  }

  /**
   * Sets the id pildora formativa.
   *
   * @param idPildoraFormativa the new id pildora formativa
   */
  public void setIdPildoraFormativa(final Long idPildoraFormativa) {
    this.idPildoraFormativa = idPildoraFormativa;
  }


  /**
   * Gets the url.
   *
   * @return the url
   */
  public String getUrl() {
    return url;
  }

  /**
   * Sets the url.
   *
   * @param url the new url
   */
  public void setUrl(final String url) {
    this.url = url;
  }

  /**
   * Checks if is activo.
   *
   * @return true, if is activo
   */
  public boolean isActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the capitulo.
   *
   * @return the capitulo
   */
  public Long getCapitulo() {
    return capitulo;
  }

  /**
   * Sets the capitulo.
   *
   * @param capitulo the new capitulo
   */
  public void setCapitulo(final Long capitulo) {
    this.capitulo = capitulo;
  }

  /**
   * Gets the titulo label.
   *
   * @return the titulo label
   */
  public String getTituloLabel() {
    return tituloLabel;
  }

  /**
   * Sets the titulo label.
   *
   * @param tituloLabel the new titulo label
   */
  public void setTituloLabel(final String tituloLabel) {
    this.tituloLabel = tituloLabel;
  }


  /**
   * Gets the aplicacion.
   *
   * @return the aplicacion
   */
  public String getAplicacion() {
    return aplicacion;
  }


  /**
   * Sets the aplicacion.
   *
   * @param aplicacion the new aplicacion
   */
  public void setAplicacion(final String aplicacion) {
    this.aplicacion = aplicacion;
  }


  /**
   * Gets the access role.
   *
   * @return the access role
   */
  public String getAccessRole() {
    return accessRole;
  }


  /**
   * Sets the access role.
   *
   * @param accessRole the new access role
   */
  public void setAccessRole(final String accessRole) {
    this.accessRole = accessRole;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
