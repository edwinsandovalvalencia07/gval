package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The persistent class for the SDM_DOCUPEDIDOAPOR database table.
 *
 */
@Entity
@Table(name = "SDM_DOCUPEDIDOAPOR")
public class DocumentoPeticionAportado implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -8878702283848524792L;

  /** The pk documento peticion aportado. */
  @Id
  @Column(name = "PK_DOCUPEDIDOAPOR", unique = true, nullable = false)
  private Long pkDocumentoPeticionAportado;

  /** The activo. */
  @Column(name = "DOACTIVO")
  private Boolean activo;

  /** The fecha entrega. */
  @Temporal(TemporalType.DATE)
  @Column(name = "DOENTREGAD")
  private Date fechaEntrega;

  /** The documento. */
  // bi-directional many-to-one association to SdmDocu
  @ManyToOne
  @JoinColumn(name = "PK_DOCU")
  private Documento documento;

  /** The documento peticion. */
  // bi-directional many-to-one association to SdmDocupeticion
  @ManyToOne
  @JoinColumn(name = "PK_DOCUPETICION", nullable = false)
  private DocumentoPeticion documentoPeticion;

  /** The usuario. */
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA")
  private Usuario usuario;

  /** The tipo documento. */
  // bi-directional many-to-one association to SdxTipodocu
  @ManyToOne
  @JoinColumn(name = "PK_TIPODOCU", nullable = false)
  private TipoDocumento tipoDocumento;

  /**
   * Instantiates a new documento peticion aportado.
   */
  public DocumentoPeticionAportado() {
    super();
  }

  /**
   * Gets the pk documento peticion aportado.
   *
   * @return the pk documento peticion aportado
   */
  public Long getPkDocumentoPeticionAportado() {
    return pkDocumentoPeticionAportado;
  }

  /**
   * Sets the pk documento peticion aportado.
   *
   * @param pkDocumentoPeticionAportado the new pk documento peticion aportado
   */
  public void setPkDocumentoPeticionAportado(
      final Long pkDocumentoPeticionAportado) {
    this.pkDocumentoPeticionAportado = pkDocumentoPeticionAportado;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha entrega.
   *
   * @return the fecha entrega
   */
  public Date getFechaEntrega() {
    return fechaEntrega;
  }

  /**
   * Sets the fecha entrega.
   *
   * @param fechaEntrega the new fecha entrega
   */
  public void setFechaEntrega(final Date fechaEntrega) {
    this.fechaEntrega = fechaEntrega;
  }

  /**
   * Gets the documento.
   *
   * @return the documento
   */
  public Documento getDocumento() {
    return documento;
  }

  /**
   * Sets the documento.
   *
   * @param documento the new documento
   */
  public void setDocumento(final Documento documento) {
    this.documento = documento;
  }

  /**
   * Gets the documento peticion.
   *
   * @return the documento peticion
   */
  public DocumentoPeticion getDocumentoPeticion() {
    return documentoPeticion;
  }

  /**
   * Sets the documento peticion.
   *
   * @param documentoPeticion the new documento peticion
   */
  public void setDocumentoPeticion(final DocumentoPeticion documentoPeticion) {
    this.documentoPeticion = documentoPeticion;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the tipo documento.
   *
   * @return the tipo documento
   */
  public TipoDocumento getTipoDocumento() {
    return tipoDocumento;
  }

  /**
   * Sets the tipo documento.
   *
   * @param tipoDocumento the new tipo documento
   */
  public void setTipoDocumento(final TipoDocumento tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
