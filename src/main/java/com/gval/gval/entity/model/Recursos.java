package com.gval.gval.entity.model;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;
import jakarta.persistence.*;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the SDM_RECURSOS database table.
 *
 */
@Entity
@Table(name = "SDM_RECURSOS")
@GenericGenerator(name = "SDM_RECURSOS_PKRECURSOS_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_RECURSOS"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class Recursos implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 6223054694417681418L;

  /** The pk recursos. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_RECURSOS_PKRECURSOS_GENERATOR")
  @Column(name = "PK_RECURSOS", unique = true, nullable = false)
  private Long pkRecursos;

  /** The estimado. */
  @Column(name = "ESTIMADO", precision = 38)
  private Boolean estimado;

  /** The estimado jur. */
  @Column(name = "ESTIMADO_JUR", precision = 38)
  private Boolean estimadoJur;

  /** The reactivo. */
  @Column(name = "REACTIVO", nullable = false, precision = 38)
  private Boolean activo;

  /** The recargado. */
  @Column(name = "RECARGADO", precision = 1)
  private Boolean cargado;

  /** The recausadeses. */
  @Column(name = "RECAUSADESES", length = 1)
  private String causaDesestimacion;

  /** The recausainad. */
  @Column(name = "RECAUSAINAD", length = 1)
  private String causaInad;

  /** The redesc. */
  @Column(name = "REDESC", length = 1000)
  private String descripcion;

  /** The refechfinr. */
  @Temporal(TemporalType.DATE)
  @Column(name = "REFECHFINR")
  private Date fechaFinRetro;

  /** The refechretr. */
  @Temporal(TemporalType.DATE)
  @Column(name = "REFECHRETR")
  private Date fechaIniRetro;

  /** The remotidese. */
  @Column(name = "REMOTIDESE", length = 4000)
  private String motivoDesestima;

  /** The remotijur. */
  @Column(name = "REMOTIJUR", length = 4000)
  private String MotivoJuridico;

  /** The remotivo. */
  @Column(name = "REMOTIVO", length = 500)
  private String motivo;

  /** The renombre. */
  @Column(name = "RENOMBRE", length = 50)
  private String nombre;

  /** The renudocide. */
  @Column(name = "RENUDOCIDE", length = 50)
  private String identPersonaRecu;

  /** The reprimapel. */
  @Column(name = "REPRIMAPEL", length = 50)
  private String primerApe;

  /** The reseacepta. */
  @Column(name = "RESEACEPTA", precision = 38)
  private Boolean aceptaRecurso;

  /** The reseguapel. */
  @Column(name = "RESEGUAPEL", length = 50)
  private String segundoApe;

  /** The retipopia. */
  @Column(name = "RETIPOPIA", length = 3)
  private String tipoRecursoPia;

  /** The documento aportado. */
  // bi-directional many-to-one association to SdmDocuapor
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_DOCU")
  private DocumentoAportado documento;

  /** The estudio. */
  // bi-directional many-to-one association to SdmEstudio
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_ESTUDIO")
  private Estudio estudio;

  /** The resolucion. */
  // bi-directional many-to-one association to SdmResoluci
  /*
   * TO-DO Mapear tabla
   */

  /** The resolucion. */
  /*
   * @ManyToOne(fetch = FetchType.LAZY)
   *
   * @JoinColumn(name = "PK_RESOLUCI") TO-DO Mapear
   */

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_RESOLUCI")
  private Resolucion resolucion;

  /** The resolucion recurso. */
  // bi-directional many-to-one association to SdmResoluci
  /*
   * TO-DO Mapear tabla
   *
   * @ManyToOne(fetch = FetchType.LAZY)
   *
   * @JoinColumn(name = "RERESOLUCI")
   */
  @Column(name = "RERESOLUCI")
  private Long resolucionRecurso;

  /** The estado. */
  // bi-directional many-to-one association to SdxEstadorecurso
  /*
   * @ManyToOne(fetch = FetchType.LAZY)
   *
   * @JoinColumn(name = "PK_ESTADORECURSO", nullable = false) private Long
   * estado;
   */

  /** The tipo identificador. */
  // bi-directional many-to-one association to SdxTipoiden
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_TIPOIDEN")
  private TipoIdentificador identificador;

  /** The direccion. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_DIRECCIO")
  private DireccionAda direccion;

  /**
   * Instantiates a new cargo.
   */
  public Recursos() {
    // Empty constructor
  }

  /**
   * Gets the pk recursos.
   *
   * @return the pk recursos
   */
  public Long getPkRecursos() {
    return this.pkRecursos;
  }

  /**
   * Sets the pk recursos.
   *
   * @param pkRecursos the new pk recursos
   */
  public void setPkRecursos(final Long pkRecursos) {
    this.pkRecursos = pkRecursos;
  }

  /**
   * Gets the estimado.
   *
   * @return the estimado
   */
  public Boolean getEstimado() {
    return this.estimado;
  }

  /**
   * Sets the estimado.
   *
   * @param estimado the new estimado
   */
  public void setEstimado(final Boolean estimado) {
    this.estimado = estimado;
  }

  /**
   * Gets the estimado jur.
   *
   * @return the estimado jur
   */
  public Boolean getEstimadoJur() {
    return this.estimadoJur;
  }

  /**
   * Sets the estimado jur.
   *
   * @param estimadoJur the new estimado jur
   */
  public void setEstimadoJur(final Boolean estimadoJur) {
    this.estimadoJur = estimadoJur;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return this.activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the cargado.
   *
   * @return the cargado
   */
  public Boolean getCargado() {
    return this.cargado;
  }

  /**
   * Sets the cargado.
   *
   * @param cargado the new cargado
   */
  public void setCargado(final Boolean cargado) {
    this.cargado = cargado;
  }

  /**
   * Gets the causa desestimacion.
   *
   * @return the causa desestimacion
   */
  public String getCausaDesestimacion() {
    return this.causaDesestimacion;
  }

  /**
   * Sets the causa desestimacion.
   *
   * @param causaDesestimacion the new causa desestimacion
   */
  public void setCausaDesestimacion(final String causaDesestimacion) {
    this.causaDesestimacion = causaDesestimacion;
  }

  /**
   * Gets the causa inad.
   *
   * @return the causa inad
   */
  public String getCausaInad() {
    return this.causaInad;
  }

  /**
   * Sets the causa inad.
   *
   * @param causaInad the new causa inad
   */
  public void setCausaInad(final String causaInad) {
    this.causaInad = causaInad;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return this.descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the fecha fin retro.
   *
   * @return the fecha fin retro
   */
  public Date getFechaFinRetro() {
    return this.fechaFinRetro;
  }

  /**
   * Sets the fecha fin retro.
   *
   * @param fechaFinRetro the new fecha fin retro
   */
  public void setFechaFinRetro(final Date fechaFinRetro) {
    this.fechaFinRetro = fechaFinRetro;
  }

  /**
   * Gets the fecha ini retro.
   *
   * @return the fecha ini retro
   */
  public Date getFechaIniRetro() {
    return this.fechaIniRetro;
  }

  /**
   * Sets the fecha ini retro.
   *
   * @param fechaIniRetro the new fecha ini retro
   */
  public void setFechaIniRetro(final Date fechaIniRetro) {
    this.fechaIniRetro = fechaIniRetro;
  }

  /**
   * Gets the motivo desestima.
   *
   * @return the motivo desestima
   */
  public String getMotivoDesestima() {
    return this.motivoDesestima;
  }

  /**
   * Sets the motivo desestima.
   *
   * @param motivoDesestima the new motivo desestima
   */
  public void setMotivoDesestima(final String motivoDesestima) {
    this.motivoDesestima = motivoDesestima;
  }

  /**
   * Gets the motivo juridico.
   *
   * @return the motivo juridico
   */
  public String getMotivoJuridico() {
    return this.MotivoJuridico;
  }

  /**
   * Sets the motivo juridico.
   *
   * @param motivoJuridico the new motivo juridico
   */
  public void setMotivoJuridico(final String motivoJuridico) {
    this.MotivoJuridico = motivoJuridico;
  }

  /**
   * Gets the motivo.
   *
   * @return the motivo
   */
  public String getMotivo() {
    return this.motivo;
  }

  /**
   * Sets the motivo.
   *
   * @param motivo the new motivo
   */
  public void setMotivo(final String motivo) {
    this.motivo = motivo;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return this.nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the ident persona recu.
   *
   * @return the ident persona recu
   */
  public String getIdentPersonaRecu() {
    return this.identPersonaRecu;
  }

  /**
   * Sets the ident persona recu.
   *
   * @param identPersonaRecu the new ident persona recu
   */
  public void setIdentPersonaRecu(final String identPersonaRecu) {
    this.identPersonaRecu = identPersonaRecu;
  }

  /**
   * Gets the primer ape.
   *
   * @return the primer ape
   */
  public String getPrimerApe() {
    return this.primerApe;
  }

  /**
   * Sets the primer ape.
   *
   * @param primerApe the new primer ape
   */
  public void setPrimerApe(final String primerApe) {
    this.primerApe = primerApe;
  }

  /**
   * Gets the acepta recurso.
   *
   * @return the acepta recurso
   */
  public Boolean getAceptaRecurso() {
    return this.aceptaRecurso;
  }

  /**
   * Sets the acepta recurso.
   *
   * @param aceptaRecurso the new acepta recurso
   */
  public void setAceptaRecurso(final Boolean aceptaRecurso) {
    this.aceptaRecurso = aceptaRecurso;
  }

  /**
   * Gets the segundo ape.
   *
   * @return the segundo ape
   */
  public String getSegundoApe() {
    return this.segundoApe;
  }

  /**
   * Sets the segundo ape.
   *
   * @param segundoApe the new segundo ape
   */
  public void setSegundoApe(final String segundoApe) {
    this.segundoApe = segundoApe;
  }

  /**
   * Gets the tipo recurso pia.
   *
   * @return the tipo recurso pia
   */
  public String getTipoRecursoPia() {
    return this.tipoRecursoPia;
  }

  /**
   * Sets the tipo recurso pia.
   *
   * @param tipoRecursoPia the new tipo recurso pia
   */
  public void setTipoRecursoPia(final String tipoRecursoPia) {
    this.tipoRecursoPia = tipoRecursoPia;
  }

  /**
   * Gets the documento.
   *
   * @return the documento
   */
  public DocumentoAportado getDocumento() {
    return this.documento;
  }

  /**
   * Sets the documento.
   *
   * @param documento the new documento
   */
  public void setDocumento(final DocumentoAportado documento) {
    this.documento = documento;
  }

  /**
   * Gets the estudio.
   *
   * @return the estudio
   */
  public Estudio getEstudio() {
    return this.estudio;
  }

  /**
   * Sets the estudio.
   *
   * @param estudio the new estudio
   */
  public void setEstudio(final Estudio estudio) {
    this.estudio = estudio;
  }

  /**
   * Gets the resolucion.
   *
   * @return the resolucion
   */


  /**
   * Gets the resolucion recurso.
   *
   * @return the resolucion recurso
   */
  public Long getResolucionRecurso() {
    return this.resolucionRecurso;
  }

  /**
   * Gets the resolucion.
   *
   * @return the resolucion
   */
  public Resolucion getResolucion() {
    return resolucion;
  }

  /**
   * Sets the resolucion.
   *
   * @param resolucion the new resolucion
   */
  public void setResolucion(final Resolucion resolucion) {
    this.resolucion = resolucion;
  }

  /**
   * Sets the resolucion recurso.
   *
   * @param resolucionRecurso the new resolucion recurso
   */
  public void setResolucionRecurso(final Long resolucionRecurso) {
    this.resolucionRecurso = resolucionRecurso;
  }

  /**
   * Gets the identificador.
   *
   * @return the identificador
   */
  public TipoIdentificador getIdentificador() {
    return this.identificador;
  }

  /**
   * Sets the identificador.
   *
   * @param identificador the new identificador
   */
  public void setIdentificador(final TipoIdentificador identificador) {
    this.identificador = identificador;
  }

  /**
   * Gets the direccion.
   *
   * @return the direccion
   */
  public DireccionAda getDireccion() {
    return this.direccion;
  }

  /**
   * Sets the direccion.
   *
   * @param direccion the new direccion
   */
  public void setDireccion(final DireccionAda direccion) {
    this.direccion = direccion;
  }
}
