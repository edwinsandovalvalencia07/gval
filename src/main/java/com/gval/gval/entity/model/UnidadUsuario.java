package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The persistent class for the SDM_UNIUSU database table.
 *
 */
@Entity
@Table(name = "SDM_UNIUSU")
public class UnidadUsuario implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -7113037052349247419L;

  /** The pk unidad usuario. */
  @Id
  @Column(name = "PK_UNIUSU", unique = true, nullable = false)
  private Long pkUnidadUsuario;

  /** The activo. */
  @Column(name = "UNACTIVO", nullable = false, precision = 38)
  private boolean activo;

  /** The fecha hasta. */
  @Temporal(TemporalType.DATE)
  @Column(name = "UNFECHASTA")
  private Date fechaHasta;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "UNFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The fecha desde. */
  @Temporal(TemporalType.DATE)
  @Column(name = "UNFECHDESD", nullable = false)
  private Date fechaDesde;

  /** The unidad. */
  // bi-directional many-to-one association to SdxUnidad
  @ManyToOne
  @JoinColumn(name = "PK_UNIDAD", nullable = false)
  private Unidad unidad;

  /** The usuario. */
  // bi-directional many-to-one association to SdxUsuario
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA", nullable = false)
  private Usuario usuario;

  /**
   * Instantiates a new unidad usuario.
   */
  public UnidadUsuario() {
    // Empty constructor
  }

  /**
   * Gets the pk unidad usuario.
   *
   * @return the pk unidad usuario
   */
  public Long getPkUnidadUsuario() {
    return pkUnidadUsuario;
  }

  /**
   * Sets the pk unidad usuario.
   *
   * @param pkUnidadUsuario the new pk unidad usuario
   */
  public void setPkUnidadUsuario(final Long pkUnidadUsuario) {
    this.pkUnidadUsuario = pkUnidadUsuario;
  }

  /**
   * Checks if is activo.
   *
   * @return true, if is activo
   */
  public boolean isActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha hasta.
   *
   * @return the fecha hasta
   */
  public Date getFechaHasta() {
    return UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Sets the fecha hasta.
   *
   * @param fechaHasta the new fecha hasta
   */
  public void setFechaHasta(final Date fechaHasta) {
    this.fechaHasta = UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha desde.
   *
   * @return the fecha desde
   */
  public Date getFechaDesde() {
    return UtilidadesCommons.cloneDate(fechaDesde);
  }

  /**
   * Sets the fecha desde.
   *
   * @param fechaDesde the new fecha desde
   */
  public void setFechaDesde(final Date fechaDesde) {
    this.fechaDesde = UtilidadesCommons.cloneDate(fechaDesde);
  }

  /**
   * Gets the unidad.
   *
   * @return the unidad
   */
  public Unidad getUnidad() {
    return unidad;
  }

  /**
   * Sets the unidad.
   *
   * @param unidad the new unidad
   */
  public void setUnidad(final Unidad unidad) {
    this.unidad = unidad;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
