package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The Class CentroServiciosSociales.
 */
@Entity
@Table(name = "SDX_CENTRO_SERV_SOCIALES")
public class CentroServiciosSociales implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -695184489149208287L;

  /** The pk centro social. */
  @Id
  @Column(name = "PK_CENTRO_SERV_SOCIALES", unique = true, nullable = false)
  private Long pkCentroSocial;

  /** The ce codigo. */
  @Column(name = "CECODIGO", nullable = false, length = 3)
  private String codigo;

  /** The ce nombre. */
  @Column(name = "CENOMBRE", nullable = false, length = 100)
  private String nombre;

  /** The zona cobertura. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_UNIDAD")
  private Unidad unidad;

  /** The activo. */
  @Column(name = "CEACTIVO", nullable = false)
  private Boolean activo;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "CEFECHCREA")
  private Date fechaCreacion;

  /**
   * Instantiates a new centro servicios sociales.
   */
  public CentroServiciosSociales() {
    // Empty constructor
  }

  /**
   * Gets the pk centro social.
   *
   * @return the pk centro social
   */
  public Long getPkCentroSocial() {
    return this.pkCentroSocial;
  }

  /**
   * Sets the pk centro social.
   *
   * @param pkCentroSocial the new pk centro social
   */
  public void setPkCentroSocial(final Long pkCentroSocial) {
    this.pkCentroSocial = pkCentroSocial;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return this.codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return this.nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }


  /**
   * Gets the unidad.
   *
   * @return the unidad
   */
  public Unidad getUnidad() {
    return unidad;
  }

  /**
   * Sets the unidad.
   *
   * @param unidad the new unidad
   */
  public void setUnidad(final Unidad unidad) {
    this.unidad = unidad;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return this.activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(this.fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }
}
