package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;

import jakarta.persistence.*;


/**
 * The persistent class for the SDM_GRUPOVAL_USU database table.
 *
 */
@Entity
@Table(name = "SDM_GRUPOVAL_USU")
@GenericGenerator(name = "SDM_GRUPOVAL_PKGRUPOVAL_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_GRUPOVAL_USU"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class GrupoValoracionUsuario implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk grupoval usu. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_GRUPOVAL_PKGRUPOVAL_GENERATOR")
  @Column(name = "PK_GRUPOVAL_USU", unique = true, nullable = false)
  private long pkGrupovalUsu;

  /** The activo. */
  @Column(name = "GUACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /** The es valorador. */
  @Column(name = "GUVALORADOR", nullable = false, precision = 1)
  private Boolean esValorador;

  /** The es trabajador social. */
  @Column(name = "GUTSOCIAL", nullable = false, precision = 1)
  private Boolean esTrabajadorSocial;

  /** The grupo valoracion. */
  @ManyToOne
  @JoinColumn(name = "PK_GRUPOVAL", nullable = false)
  private GrupoValoracion grupoValoracion;

  /** The usuario. */
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA", nullable = false)
  private Usuario usuario;

  /**
   * Instantiates a new grupo valoracion usuario.
   */
  public GrupoValoracionUsuario() {
    super();
  }

  /**
   * Gets the pk grupoval usu.
   *
   * @return the pkGrupovalUsu
   */
  public long getPkGrupovalUsu() {
    return pkGrupovalUsu;
  }

  /**
   * Sets the pk grupoval usu.
   *
   * @param pkGrupovalUsu the pkGrupovalUsu to set
   */
  public void setPkGrupovalUsu(final long pkGrupovalUsu) {
    this.pkGrupovalUsu = pkGrupovalUsu;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the es valorador.
   *
   * @return the esValorador
   */
  public Boolean getEsValorador() {
    return esValorador;
  }

  /**
   * Sets the es valorador.
   *
   * @param esValorador the esValorador to set
   */
  public void setEsValorador(final Boolean esValorador) {
    this.esValorador = esValorador;
  }

  /**
   * Gets the es trabajador social.
   *
   * @return the esTrabajadorSocial
   */
  public Boolean getEsTrabajadorSocial() {
    return esTrabajadorSocial;
  }

  /**
   * Sets the es trabajador social.
   *
   * @param esTrabajadorSocial the esTrabajadorSocial to set
   */
  public void setEsTrabajadorSocial(final Boolean esTrabajadorSocial) {
    this.esTrabajadorSocial = esTrabajadorSocial;
  }

  /**
   * Gets the grupo valoracion.
   *
   * @return the grupoValoracion
   */
  public GrupoValoracion getGrupoValoracion() {
    return grupoValoracion;
  }

  /**
   * Sets the grupo valoracion.
   *
   * @param grupoValoracion the grupoValoracion to set
   */
  public void setGrupoValoracion(final GrupoValoracion grupoValoracion) {
    this.grupoValoracion = grupoValoracion;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the usuario to set
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
