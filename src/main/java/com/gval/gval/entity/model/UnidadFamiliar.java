package com.gval.gval.entity.model;

import com.gval.gval.common.ConstantesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The persistent class for the SDM_UNIDFAMI database table.
 *
 */
@Entity
@Table(name = "SDM_UNIDFAMI")
@GenericGenerator(name = "SDM_UNIDFAMI_PKUNIDFAMI_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_UNIDFAMI"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class UnidadFamiliar implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 6394258805228329291L;

  /** The pk unidfami. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_UNIDFAMI_PKUNIDFAMI_GENERATOR")
  @Column(name = "PK_UNIDFAMI", unique = true, nullable = false)
  private Long pkUnidfami;

  /** The activo. */
  @Column(name = "UNACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /** The excluido. */
  @Column(name = "UNEXCLUIDO", nullable = false, precision = 1)
  private Boolean excluido;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "UNFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The ingresado. */
  @Column(name = "UNINGRESADO", nullable = false, precision = 1)
  private Boolean ingresado;

  /** The ingreso. */
  @Column(name = "UNINGRESO", nullable = false, precision = 1)
  private Boolean ingreso;

  /** The app origen. */
  @Column(name = "APP_ORIGEN")
  private String appOrigen;

  /** The persona. */
  // bi-directional many-to-one association to SdmPersona
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA", nullable = false)
  private Persona persona;

  /** The solicitud. */
  // bi-directional many-to-one association to SdmSolicit
  @ManyToOne
  @JoinColumn(name = "PK_SOLICIT", nullable = false)
  private Solicitud solicitud;

  /** The tipo familiar. */
  // bi-directional many-to-one association to SdxTipofami
  @ManyToOne
  @JoinColumn(name = "PK_TIPOFAMI")
  private TipoFamiliar tipoFamiliar;

  /** The discapacidad mayor 33. */
  @Column(name = "UNDISCAPACIDAD33")
  private Integer discapacidadMayor33;

  /**
   * Instantiates a new unidad familiar.
   */
  public UnidadFamiliar() {
    // Empty constructor
  }

  /**
   * On pre persist.
   */
  @PrePersist
  public void onPrePersist() {
    appOrigen = ConstantesCommons.APLICACION_ADA3;
  }

  /**
   * Gets the pk unidfami.
   *
   * @return the pk unidfami
   */
  public Long getPkUnidfami() {
    return pkUnidfami;
  }

  /**
   * Sets the pk unidfami.
   *
   * @param pkUnidfami the new pk unidfami
   */
  public void setPkUnidfami(final Long pkUnidfami) {
    this.pkUnidfami = pkUnidfami;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the excluido.
   *
   * @return the excluido
   */
  public Boolean getExcluido() {
    return excluido;
  }

  /**
   * Sets the excluido.
   *
   * @param excluido the new excluido
   */
  public void setExcluido(final Boolean excluido) {
    this.excluido = excluido;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return fechaCreacion;
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

  /**
   * Gets the ingresado.
   *
   * @return the ingresado
   */
  public Boolean getIngresado() {
    return ingresado;
  }

  /**
   * Sets the ingresado.
   *
   * @param ingresado the new ingresado
   */
  public void setIngresado(final Boolean ingresado) {
    this.ingresado = ingresado;
  }

  /**
   * Gets the ingreso.
   *
   * @return the ingreso
   */
  public Boolean getIngreso() {
    return ingreso;
  }

  /**
   * Sets the ingreso.
   *
   * @param ingreso the new ingreso
   */
  public void setIngreso(final Boolean ingreso) {
    this.ingreso = ingreso;
  }

  /**
   * Gets the persona.
   *
   * @return the persona
   */
  public Persona getPersona() {
    return persona;
  }

  /**
   * Sets the persona.
   *
   * @param persona the new persona
   */
  public void setPersona(final Persona persona) {
    this.persona = persona;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public Solicitud getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the new solicitud
   */
  public void setSolicitud(final Solicitud solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the tipo familiar.
   *
   * @return the tipo familiar
   */
  public TipoFamiliar getTipoFamiliar() {
    return tipoFamiliar;
  }

  /**
   * Sets the tipo familiar.
   *
   * @param tipoFamiliar the new tipo familiar
   */
  public void setTipoFamiliar(final TipoFamiliar tipoFamiliar) {
    this.tipoFamiliar = tipoFamiliar;
  }

  /**
   * Gets the app origen.
   *
   * @return the app origen
   */
  public String getAppOrigen() {
    return appOrigen;
  }

  /**
   * Sets the app origen.
   *
   * @param appOrigen the new app origen
   */
  public void setAppOrigen(final String appOrigen) {
    this.appOrigen = appOrigen;
  }

  /**
   * Gets the discapacidad mayor 33.
   *
   * @return the discapacidad mayor 33
   */
  public Integer getDiscapacidadMayor33() {
    return discapacidadMayor33;
  }

  /**
   * Sets the discapacidad mayor 33.
   *
   * @param discapacidadMayor33 the new discapacidad mayor 33
   */
  public void setDiscapacidadMayor33(final Integer discapacidadMayor33) {
    this.discapacidadMayor33 = discapacidadMayor33;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
