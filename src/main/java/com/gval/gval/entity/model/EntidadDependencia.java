package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

import jakarta.persistence.*;

/**
 * The Class EntidadDependencia.
 */
@Entity
@Table(name = "SDM_ENTIDEPE")
public class EntidadDependencia implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1222453321749597633L;

  /** The pk entidepe. */
  @Id
  @Column(name = "PK_ENTIDEPE", unique = true, nullable = false)
  private Long pkEntidepe;

  /** The contacto. */
  @Column(name = "ENCONTACTO", length = 250)
  private String contacto;

  /** The departamento. */
  @Column(name = "ENDEPARTAM", length = 250)
  private String departamento;

  /** The nombre entidad. */
  @Column(name = "ENENTIDAD", length = 250)
  private String nombreEntidad;

  /** The servicio. */
  @Column(name = "ENSERVICIO", length = 250)
  private String servicio;

  /** The direccion. */
  // bi-directional many-to-one association to SdmDireccio
  @ManyToOne
  @JoinColumn(name = "PK_DIRECCIO", nullable = false)
  private DireccionAda direccion;

  /** The comunidad autonoma. */
  // bi-directional many-to-one association to SdxComauto
  @ManyToOne
  @JoinColumn(name = "PK_COMAUTO", nullable = false)
  private ComunidadAutonoma comunidadAutonoma;

  /**
   * Instantiates a new entidad dependencia.
   */
  public EntidadDependencia() {
    // Constructor
  }

  /**
   * Gets the pk entidepe.
   *
   * @return the pk entidepe
   */
  public Long getPkEntidepe() {
    return pkEntidepe;
  }

  /**
   * Sets the pk entidepe.
   *
   * @param pkEntidepe the new pk entidepe
   */
  public void setPkEntidepe(final Long pkEntidepe) {
    this.pkEntidepe = pkEntidepe;
  }

  /**
   * Gets the direccion.
   *
   * @return the direccion
   */
  public DireccionAda getDireccion() {
    return direccion;
  }

  /**
   * Sets the direccion.
   *
   * @param direccion the new direccion
   */
  public void setDireccion(final DireccionAda direccion) {
    this.direccion = direccion;
  }

  /**
   * Gets the comunidad autonoma.
   *
   * @return the comunidad autonoma
   */
  public ComunidadAutonoma getComunidadAutonoma() {
    return comunidadAutonoma;
  }

  /**
   * Sets the comunidad autonoma.
   *
   * @param comunidadAutonoma the new comunidad autonoma
   */
  public void setComunidadAutonoma(final ComunidadAutonoma comunidadAutonoma) {
    this.comunidadAutonoma = comunidadAutonoma;
  }

  /**
   * Gets the contacto.
   *
   * @return the contacto
   */
  public String getContacto() {
    return contacto;
  }

  /**
   * Sets the contacto.
   *
   * @param contacto the new contacto
   */
  public void setContacto(final String contacto) {
    this.contacto = contacto;
  }

  /**
   * Gets the departamento.
   *
   * @return the departamento
   */
  public String getDepartamento() {
    return departamento;
  }

  /**
   * Sets the departamento.
   *
   * @param departamento the new departamento
   */
  public void setDepartamento(final String departamento) {
    this.departamento = departamento;
  }

  /**
   * Gets the nombre entidad.
   *
   * @return the nombre entidad
   */
  public String getNombreEntidad() {
    return nombreEntidad;
  }

  /**
   * Sets the nombre entidad.
   *
   * @param nombreEntidad the new nombre entidad
   */
  public void setNombreEntidad(final String nombreEntidad) {
    this.nombreEntidad = nombreEntidad;
  }

  /**
   * Gets the servicio.
   *
   * @return the servicio
   */
  public String getServicio() {
    return servicio;
  }

  /**
   * Sets the servicio.
   *
   * @param servicio the new servicio
   */
  public void setServicio(final String servicio) {
    this.servicio = servicio;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
