package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The persistent class for the SDM_DATRESI database table.
 *
 */
@Entity
@Table(name = "SDM_DATRESI")
@GenericGenerator(name = "SDM_DATRESI_PKDATRESI_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {@Parameter(name = "sequence_name", value = "SEC_SDM_DATRESI"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class PeriodoResidencia implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -8360625037573576957L;

  /** The pk periodo residencia. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_DATRESI_PKDATRESI_GENERATOR")
  @Column(name = "PK_DATRESI", unique = true, nullable = false)
  private Long pkPeriodoResidencia;

  /** The fecha fin. */
  @Temporal(TemporalType.DATE)
  @Column(name = "DAFECFIN", nullable = false)
  private Date fechaFin;

  /** The fecha inicio. */
  @Temporal(TemporalType.DATE)
  @Column(name = "DAFECINI", nullable = false)
  private Date fechaInicio;

  /** The solicitud. */
  // bi-directional many-to-one association to SdmSolicit
  @ManyToOne
  @JoinColumn(name = "PK_SOLICIT")
  private Solicitud solicitud;

  /** The poblacion. */
  // bi-directional many-to-one association to SdxPoblac
  @ManyToOne
  @JoinColumn(name = "PK_POBLAC")
  private Poblacion poblacion;

  /** The provincia. */
  // bi-directional many-to-one association to SdxProvinci
  @ManyToOne
  @JoinColumn(name = "PK_PROVINCI")
  private Provincia provincia;

  /**
   * Instantiates a new periodo residencia.
   */
  public PeriodoResidencia() {
    // Empty constructor
  }

  /**
   * Gets the pk periodo residencia.
   *
   * @return the pkPeriodoResidencia
   */
  public Long getPkPeriodoResidencia() {
    return pkPeriodoResidencia;
  }

  /**
   * Sets the pk periodo residencia.
   *
   * @param pkPeriodoResidencia the pkPeriodoResidencia to set
   */
  public void setPkPeriodoResidencia(final Long pkPeriodoResidencia) {
    this.pkPeriodoResidencia = pkPeriodoResidencia;
  }

  /**
   * Gets the fecha fin.
   *
   * @return the fechaFin
   */
  public Date getFechaFin() {
    return UtilidadesCommons.cloneDate(fechaFin);
  }

  /**
   * Sets the fecha fin.
   *
   * @param fechaFin the fechaFin to set
   */
  public void setFechaFin(final Date fechaFin) {
    this.fechaFin = UtilidadesCommons.cloneDate(fechaFin);
  }

  /**
   * Gets the fecha inicio.
   *
   * @return the fechaInicio
   */
  public Date getFechaInicio() {
    return UtilidadesCommons.cloneDate(fechaInicio);
  }

  /**
   * Sets the fecha inicio.
   *
   * @param fechaInicio the fechaInicio to set
   */
  public void setFechaInicio(final Date fechaInicio) {
    this.fechaInicio = UtilidadesCommons.cloneDate(fechaInicio);
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public Solicitud getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the solicitud to set
   */
  public void setSolicitud(final Solicitud solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the poblacion.
   *
   * @return the poblacion
   */
  public Poblacion getPoblacion() {
    return poblacion;
  }

  /**
   * Sets the poblacion.
   *
   * @param poblacion the poblacion to set
   */
  public void setPoblacion(final Poblacion poblacion) {
    this.poblacion = poblacion;
  }

  /**
   * Gets the provincia.
   *
   * @return the provincia
   */
  public Provincia getProvincia() {
    return provincia;
  }

  /**
   * Sets the provincia.
   *
   * @param provincia the provincia to set
   */
  public void setProvincia(final Provincia provincia) {
    this.provincia = provincia;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
