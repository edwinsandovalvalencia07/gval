package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.Where;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import jakarta.persistence.*;


/**
 * The persistent class for the SDX_USUARIO database table.
 *
 */
@Entity
@Table(name = "SDX_USUARIO")
public class Usuario implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 7155538173725313819L;

  /** The pk persona. */
  @Id
  @Column(name = "PK_PERSONA", unique = true, nullable = false)
  private Long pkPersona;

  /** The estiloapp. */
  @Column(name = "ESTILOAPP")
  private Long estiloapp;

  /** The provincia. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PKPROVINCIA")
  private Provincia provincia;

  /** The activo. */
  @Column(name = "USACTIVO", nullable = false)
  private Boolean activo;

  /** The administrador. */
  @Column(name = "USADMINIST", nullable = false)
  private Boolean administrador;

  /** The uscbs 1. */
  @Column(name = "USCBS1", nullable = false)
  private Boolean uscbs1;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "USFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The firma. */
  @Column(name = "USFIRMA")
  private Boolean firma;

  /** The nombre. */
  @Column(name = "USNOMBRE", nullable = false, length = 15)
  private String nombre;

  /** The provincias. */
  @Column(name = "USPROVINCIAS", length = 100)
  private String provincias;

  /** The sector. */
  @Column(name = "USSECTOR", length = 1)
  private String sector;


  /** The ultima conexion. */
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "USULTCONEXION")
  private Date ultimaConexion;

  /** The persona. */
  // bi-directional one-to-one association to Persona
  @OneToOne
  @JoinColumn(name = "PK_PERSONA", referencedColumnName = "id")
  @MapsId
  private Persona persona;

  /** The perfiles. */
  // bi-directional many-to-many association to Perfil
  @OneToMany(mappedBy = "usuario")
  @Where(clause = " PEACTIVO = 1 ")
  private List<Perfil> perfiles;

  /**
   * Gets the pk persona.
   *
   * @return the pk persona
   */
  public Long getPkPersona() {
    return pkPersona;
  }

  /**
   * Sets the pk persona.
   *
   * @param pkPersona the new pk persona
   */
  public void setPkPersona(final Long pkPersona) {
    this.pkPersona = pkPersona;
  }

  /**
   * Gets the estiloapp.
   *
   * @return the estiloapp
   */
  public Long getEstiloapp() {
    return estiloapp;
  }

  /**
   * Sets the estiloapp.
   *
   * @param estiloapp the new estiloapp
   */
  public void setEstiloapp(final Long estiloapp) {
    this.estiloapp = estiloapp;
  }

  /**
   * Gets the provincia.
   *
   * @return the provincia
   */
  public Provincia getProvincia() {
    return provincia;
  }

  /**
   * Sets the provincia.
   *
   * @param provincia the new provincia
   */
  public void setProvincia(final Provincia provincia) {
    this.provincia = provincia;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the administrador.
   *
   * @return the administrador
   */
  public Boolean getAdministrador() {
    return administrador;
  }

  /**
   * Sets the administrador.
   *
   * @param administrador the new administrador
   */
  public void setAdministrador(final Boolean administrador) {
    this.administrador = administrador;
  }

  /**
   * Gets the uscbs 1.
   *
   * @return the uscbs 1
   */
  public Boolean getUscbs1() {
    return uscbs1;
  }

  /**
   * Sets the uscbs 1.
   *
   * @param uscbs1 the new uscbs 1
   */
  public void setUscbs1(final Boolean uscbs1) {
    this.uscbs1 = uscbs1;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return fechaCreacion;
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

  /**
   * Gets the firma.
   *
   * @return the firma
   */
  public Boolean getFirma() {
    return firma;
  }

  /**
   * Sets the firma.
   *
   * @param firma the new firma
   */
  public void setFirma(final Boolean firma) {
    this.firma = firma;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Gets the provincias.
   *
   * @return the provincias
   */
  public String getProvincias() {
    return provincias;
  }

  /**
   * Sets the provincias.
   *
   * @param provincias the new provincias
   */
  public void setProvincias(final String provincias) {
    this.provincias = provincias;
  }

  /**
   * Gets the sector.
   *
   * @return the sector
   */
  public String getSector() {
    return sector;
  }

  /**
   * Sets the sector.
   *
   * @param sector the new sector
   */
  public void setSector(final String sector) {
    this.sector = sector;
  }

  /**
   * Gets the persona.
   *
   * @return the persona
   */
  public Persona getPersona() {
    return persona;
  }

  /**
   * Sets the persona.
   *
   * @param persona the new persona
   */
  public void setPersona(final Persona persona) {
    this.persona = persona;
  }

  /**
   * Gets the perfiles.
   *
   * @return the perfiles
   */
  public List<Perfil> getPerfiles() {
    return perfiles;
  }

  /**
   * Sets the perfiles.
   *
   * @param perfiles the new perfiles
   */
  public void setPerfiles(final List<Perfil> perfiles) {
    this.perfiles = perfiles;
  }

  /**
   * Gets the ultima conexion.
   *
   * @return the ultima conexion
   */
  public Date getUltimaConexion() {
    return UtilidadesCommons.cloneDate(ultimaConexion);
  }

  /**
   * Sets the ultima conexion.
   *
   * @param ultimaConexion the new ultima conexion
   */
  public void setUltimaConexion(final Date ultimaConexion) {
    this.ultimaConexion = UtilidadesCommons.cloneDate(ultimaConexion);
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
