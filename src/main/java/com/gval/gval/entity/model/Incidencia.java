package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;

/**
 * The Class Incidencia.
 */
@Entity
@Table(name = "SDM_INCIDENC")
@GenericGenerator(name = "SDM_INCIDENC_PKINCIDENC_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_INCIDENC"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class Incidencia implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1192920994961973622L;

  /** The pk incidenc. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_INCIDENC_PKINCIDENC_GENERATOR")
  @Column(name = "PK_INCIDENC", unique = true, nullable = false)
  private Long pkIncidenc;

  /** The activo. */
  @Column(name = "INACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /** The descripcion. */
  @Column(name = "INDESC", length = 500)
  private String descripcion;

  /** The estado. */
  @Column(name = "INESTADO", nullable = false, length = 1)
  private String estado;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "INFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The pk docu. */
  @Column(name = "PK_DOCU")
  private Long pkDocu;

  /** The solicitud. */
  // bi-directional many-to-one association to SdmSolicit
  @ManyToOne
  @JoinColumn(name = "PK_SOLICIT", nullable = false)
  private Solicitud solicitud;

  /** The tipo incidencia. */
  // bi-directional many-to-one association to SdxTipoinci
  @ManyToOne
  @JoinColumn(name = "PK_TIPOINCI", nullable = false)
  private TipoIncidencia tipoIncidencia;

  /** The usuario. */
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA", nullable = false)
  private Usuario usuario;


  /**
   * Instantiates a new incidencia.
   */
  public Incidencia() {
    // Constructor
  }

  /**
   * Gets the pk incidenc.
   *
   * @return the pk incidenc
   */
  public Long getPkIncidenc() {
    return pkIncidenc;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Gets the estado.
   *
   * @return the estado
   */
  public String getEstado() {
    return estado;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the pk docu.
   *
   * @return the pk docu
   */
  public Long getPkDocu() {
    return pkDocu;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public Solicitud getSolicitud() {
    return solicitud;
  }

  /**
   * Gets the tipo incidencia.
   *
   * @return the tipo incidencia
   */
  public TipoIncidencia getTipoIncidencia() {
    return tipoIncidencia;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }

  /**
   * Sets the pk incidenc.
   *
   * @param pkIncidenc the new pk incidenc
   */
  public void setPkIncidenc(final Long pkIncidenc) {
    this.pkIncidenc = pkIncidenc;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Sets the estado.
   *
   * @param estado the new estado
   */
  public void setEstado(final String estado) {
    this.estado = estado;
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the pk docu.
   *
   * @param pkDocu the new pk docu
   */
  public void setPkDocu(final Long pkDocu) {
    this.pkDocu = pkDocu;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the new solicitud
   */
  public void setSolicitud(final Solicitud solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Sets the tipo incidencia.
   *
   * @param tipoIncidencia the new tipo incidencia
   */
  public void setTipoIncidencia(final TipoIncidencia tipoIncidencia) {
    this.tipoIncidencia = tipoIncidencia;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the new usuario
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
