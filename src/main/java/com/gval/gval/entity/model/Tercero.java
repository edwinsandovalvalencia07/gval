package com.gval.gval.entity.model;

import com.gval.gval.common.ConstantesCommons;
import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;


import jakarta.persistence.*;



/**
 * The persistent class for the SDM_TERCEROS database table.
 *
 */
@Entity
@Table(name = "SDM_TERCEROS")
@GenericGenerator(name = "SDM_TERCEROS_PKTERCEROS_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_TERCEROS"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public class Tercero implements Serializable {

  /** serial version UID. */
  private static final long serialVersionUID = -419685973125819025L;

  /** The pk terceros. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_TERCEROS_PKTERCEROS_GENERATOR")
  @Column(name = "PK_TERCEROS", unique = true, nullable = false)
  private Long pkTerceros;

  /** The pk lote. */

  // bi-directional many-to-one association to sdmlote
  @ManyToOne
  @JoinColumn(name = "PK_LOTE")
  private LoteTerceros loteTerceros;

  /** The cuenta iban. */
  @Column(name = "TECCCIBAN", length = 34)
  private String cuentaIban;

  /** The tecodcoiban. */
  @Column(name = "TECODCOIBAN", length = 2)
  private String codigoControl;

  /** The codigo control iban. */
  @Column(name = "TECODIBANC", length = 4)
  private String codigoBanco;

  /** The codigo sucursal. */
  @Column(name = "TECODISUCU", length = 4)
  private String codigoSucursal;

  /** The pais iban. */
  @Column(name = "TECOPAIBAN", length = 2)
  private String paisIban;

  /** The digito control. */
  @Column(name = "TEDIGICONT", length = 2)
  private String digitoControl;

  /** The banco original. */
  @Column(name = "TEDOCBANCOORIGINAL", precision = 1)
  private Boolean declaraResponsabilidad;

  /** The fecha alta. */
  @Temporal(TemporalType.DATE)
  @Column(name = "TEFECHALTA", nullable = false)
  private Date fechaAlta;

  /** The fecha validacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "TEFECHAVALI", nullable = false)
  private Date fechaValidacion;

  /** The fecha baja. */
  @Temporal(TemporalType.DATE)
  @Column(name = "TEFECHBAJA", nullable = false)
  private Date fechaBaja;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "TEFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The heredero. */
  @Column(name = "TEHEREDERO", nullable = false, precision = 1)
  private Boolean heredero;

  /** The nombre. */
  @Column(name = "TENOMBRE", length = 50)
  private String nombre;

  /** The nif. */
  @Column(name = "TENUMDOCIDE", length = 50)
  private String nif;

  /** The numero cuenta. */
  @Column(name = "TENUMECUEN", length = 10)
  private String numeroCuenta;

  /** The origen. */
  @Column(name = "TEORIGEN", nullable = false, length = 4)
  private String origen;

  /** The primer apellido. */
  @Column(name = "TEPRIMAPEL", length = 50)
  private String primerApellido;

  /** The activo. */
  @Column(name = "TERACTIVO", nullable = false, precision = 38)
  private Boolean activo;

  /** The segundo apellido. */
  @Column(name = "TESEGUAPEL", length = 50)
  private String segundoApellido;

  /** The tipo. */
  @Column(name = "TETIPO", precision = 38)
  private Long tipoPersona;

  /** The tipo tercero. */
  @Column(name = "TETIPO_TERCERO", nullable = false, length = 1)
  private String tipoTercero;

  /** The validado. */
  @Column(name = "TEVALIDADO", nullable = false, precision = 38)
  private Boolean validado;

  /** The version. */
  @Column(name = "TEVERSION", length = 2)
  private String version;

  /** The version iban. */
  @Column(name = "TEVERSIONIBAN", length = 2)
  private String versionIban;

  /** The app origen. */
  @Column(name = "APP_ORIGEN")
  private String appOrigen;

  /** The persona. */
  // bi-directional many-to-one association to SdmPersona
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA", nullable = false)
  private Persona persona;

  /** The expediente. */
  // bi-directional many-to-one association to SdmExpedien
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PK_EXPEDIEN", nullable = false)
  private Expediente expediente;

  /** The tipo identificador. */
  @Column(name = "PK_TIPOIDEN")
  private Long tipoIdentificador;

  /** The id api. */
  @Column(name = "CTA_ID")
  private Long idApi;

  /** The inddup api. */
  @Column(name = "CTA_INDDUP")
  private Long inddupApi;

  /** The pendiente activar. */
  @Column(name = "TEPENDIENTE_ACTIVAR", precision = 1)
  private Boolean pendienteActivar;


  /** The app modifica. */
  @Column(name = "APP_MODIFICA")
  private String appModifica;


  /** The fecha modificacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "TEFECHMODIFI")
  private Date fechaModificacion;

  /**
   * On pre persist.
   */
  @PrePersist
  public void onPrePersist() {
    fechaAlta = new Date();
    fechaCreacion = new Date();
    fechaModificacion = new Date();
    origen = ConstantesCommons.APLICACION_ADA3;
    appOrigen = ConstantesCommons.APLICACION_ADA3;
    appModifica = ConstantesCommons.APLICACION_ADA3;
  }

  /**
   * On pre update.
   */
  @PreUpdate
  public void onPreUpdate() {
    fechaModificacion = new Date();
    appModifica = ConstantesCommons.APLICACION_ADA3;
  }


  /**
   * Gets the lote terceros.
   *
   * @return the loteTerceros
   */
  public LoteTerceros getLoteTerceros() {
    return loteTerceros;
  }

  /**
   * Sets the lote terceros.
   *
   * @param loteTerceros the loteTerceros to set
   */
  public void setLoteTerceros(final LoteTerceros loteTerceros) {
    this.loteTerceros = loteTerceros;
  }

  /**
   * Instantiates a new tercero.
   */
  public Tercero() {
    // Constructor Tercero
  }

  /**
   * Gets the pk terceros.
   *
   * @return the pk terceros
   */
  public Long getPkTerceros() {
    return pkTerceros;
  }


  /**
   * Sets the pk terceros.
   *
   * @param pkTerceros the new pk terceros
   */
  public void setPkTerceros(final Long pkTerceros) {
    this.pkTerceros = pkTerceros;
  }



  /**
   * Gets the tipo identificador.
   *
   * @return the tipo identificador
   */
  public Long getTipoIdentificador() {
    return tipoIdentificador;
  }


  /**
   * Sets the tipo identificador.
   *
   * @param tipoIdentificador the new tipo identificador
   */
  public void setTipoIdentificador(final Long tipoIdentificador) {
    this.tipoIdentificador = tipoIdentificador;
  }


  /**
   * Gets the cuenta iban.
   *
   * @return the cuenta iban
   */
  public String getCuentaIban() {
    return cuentaIban;
  }


  /**
   * Sets the cuenta iban.
   *
   * @param cuentaIban the new cuenta iban
   */
  public void setCuentaIban(final String cuentaIban) {
    this.cuentaIban = cuentaIban;
  }


  /**
   * Gets the tecodcoiban.
   *
   * @return the tecodcoiban
   */
  public String getCodigoControl() {
    return codigoControl;
  }


  /**
   * Sets the tecodcoiban.
   *
   * @param codigoControl the new tecodcoiban
   */
  public void setCodigoControl(final String codigoControl) {
    this.codigoControl = codigoControl;
  }


  /**
   * Gets the codigo control iban.
   *
   * @return the codigo control iban
   */
  public String getCodigoBanco() {
    return codigoBanco;
  }


  /**
   * Sets the codigo control iban.
   *
   * @param codigoBanco the new codigo control iban
   */
  public void setCodigoBanco(final String codigoBanco) {
    this.codigoBanco = codigoBanco;
  }


  /**
   * Gets the codigo sucursal.
   *
   * @return the codigo sucursal
   */
  public String getCodigoSucursal() {
    return codigoSucursal;
  }


  /**
   * Sets the codigo sucursal.
   *
   * @param codigoSucursal the new codigo sucursal
   */
  public void setCodigoSucursal(final String codigoSucursal) {
    this.codigoSucursal = codigoSucursal;
  }


  /**
   * Gets the pais iban.
   *
   * @return the pais iban
   */
  public String getPaisIban() {
    return paisIban;
  }


  /**
   * Sets the pais iban.
   *
   * @param paisIban the new pais iban
   */
  public void setPaisIban(final String paisIban) {
    this.paisIban = paisIban;
  }


  /**
   * Gets the digito control.
   *
   * @return the digito control
   */
  public String getDigitoControl() {
    return digitoControl;
  }


  /**
   * Sets the digito control.
   *
   * @param digitoControl the new digito control
   */
  public void setDigitoControl(final String digitoControl) {
    this.digitoControl = digitoControl;
  }


  /**
   * Gets the banco original.
   *
   * @return the banco original
   */
  public Boolean getDeclaraResponsabilidad() {
    return declaraResponsabilidad;
  }


  /**
   * Sets the banco original.
   *
   * @param declaraResponsabilidad the new declara responsabilidad
   */
  public void setDeclaraResponsabilidad(final Boolean declaraResponsabilidad) {
    this.declaraResponsabilidad = declaraResponsabilidad;
  }


  /**
   * Gets the fecha alta.
   *
   * @return the fecha alta
   */
  public Date getFechaAlta() {
    return UtilidadesCommons.cloneDate(fechaAlta);
  }


  /**
   * Sets the fecha alta.
   *
   * @param fechaAlta the new fecha alta
   */
  public void setFechaAlta(final Date fechaAlta) {
    this.fechaAlta = UtilidadesCommons.cloneDate(fechaAlta);
  }


  /**
   * Gets the fecha validacion.
   *
   * @return the fecha validacion
   */
  public Date getFechaValidacion() {
    return UtilidadesCommons.cloneDate(fechaValidacion);
  }


  /**
   * Sets the fecha validacion.
   *
   * @param fechaValidacion the new fecha validacion
   */
  public void setFechaValidacion(final Date fechaValidacion) {
    this.fechaValidacion = UtilidadesCommons.cloneDate(fechaValidacion);
  }


  /**
   * Gets the fecha baja.
   *
   * @return the fecha baja
   */
  public Date getFechaBaja() {
    return UtilidadesCommons.cloneDate(fechaBaja);
  }


  /**
   * Sets the fecha baja.
   *
   * @param fechaBaja the new fecha baja
   */
  public void setFechaBaja(final Date fechaBaja) {
    this.fechaBaja = UtilidadesCommons.cloneDate(fechaBaja);
  }


  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }


  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }


  /**
   * Gets the heredero.
   *
   * @return the heredero
   */
  public Boolean getHeredero() {
    return heredero;
  }


  /**
   * Sets the heredero.
   *
   * @param heredero the new heredero
   */
  public void setHeredero(final Boolean heredero) {
    this.heredero = heredero;
  }


  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }


  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }


  /**
   * Gets the nif.
   *
   * @return the nif
   */
  public String getNif() {
    return nif;
  }


  /**
   * Sets the nif.
   *
   * @param nif the new nif
   */
  public void setNif(final String nif) {
    this.nif = nif;
  }


  /**
   * Gets the numero cuenta.
   *
   * @return the numero cuenta
   */
  public String getNumeroCuenta() {
    return numeroCuenta;
  }


  /**
   * Sets the numero cuenta.
   *
   * @param numeroCuenta the new numero cuenta
   */
  public void setNumeroCuenta(final String numeroCuenta) {
    this.numeroCuenta = numeroCuenta;
  }


  /**
   * Gets the origen.
   *
   * @return the origen
   */
  public String getOrigen() {
    return origen;
  }


  /**
   * Sets the origen.
   *
   * @param origen the new origen
   */
  public void setOrigen(final String origen) {
    this.origen = origen;
  }


  /**
   * Gets the primer apellido.
   *
   * @return the primer apellido
   */
  public String getPrimerApellido() {
    return primerApellido;
  }


  /**
   * Sets the primer apellido.
   *
   * @param primerApellido the new primer apellido
   */
  public void setPrimerApellido(final String primerApellido) {
    this.primerApellido = primerApellido;
  }


  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }


  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }


  /**
   * Gets the segundo apellido.
   *
   * @return the segundo apellido
   */
  public String getSegundoApellido() {
    return segundoApellido;
  }


  /**
   * Sets the segundo apellido.
   *
   * @param segundoApellido the new segundo apellido
   */
  public void setSegundoApellido(final String segundoApellido) {
    this.segundoApellido = segundoApellido;
  }

  /**
   * Gets the tipo persona.
   *
   * @return the tipo persona
   */
  public Long getTipoPersona() {
    return tipoPersona;
  }

  /**
   * Sets the tipo persona.
   *
   * @param tipoPersona the new tipo persona
   */
  public void setTipoPersona(final Long tipoPersona) {
    this.tipoPersona = tipoPersona;
  }


  /**
   * Gets the tipo tercero.
   *
   * @return the tipo tercero
   */
  public String getTipoTercero() {
    return tipoTercero;
  }


  /**
   * Sets the tipo tercero.
   *
   * @param tipoTercero the new tipo tercero
   */
  public void setTipoTercero(final String tipoTercero) {
    this.tipoTercero = tipoTercero;
  }


  /**
   * Gets the validado.
   *
   * @return the validado
   */
  public Boolean getValidado() {
    return validado;
  }


  /**
   * Sets the validado.
   *
   * @param validado the new validado
   */
  public void setValidado(final Boolean validado) {
    this.validado = validado;
  }


  /**
   * Gets the version.
   *
   * @return the version
   */
  public String getVersion() {
    return version;
  }


  /**
   * Sets the version.
   *
   * @param version the new version
   */
  public void setVersion(final String version) {
    this.version = version;
  }


  /**
   * Gets the version iban.
   *
   * @return the version iban
   */
  public String getVersionIban() {
    return versionIban;
  }


  /**
   * Sets the version iban.
   *
   * @param versionIban the new version iban
   */
  public void setVersionIban(final String versionIban) {
    this.versionIban = versionIban;
  }


  /**
   * Gets the persona.
   *
   * @return the persona
   */
  public Persona getPersona() {
    return persona;
  }


  /**
   * Sets the persona.
   *
   * @param persona the new persona
   */
  public void setPersona(final Persona persona) {
    this.persona = persona;
  }


  /**
   * Gets the expediente.
   *
   * @return the expediente
   */
  public Expediente getExpediente() {
    return expediente;
  }


  /**
   * Sets the expediente.
   *
   * @param expediente the new expediente
   */
  public void setExpediente(final Expediente expediente) {
    this.expediente = expediente;
  }


  /**
   * Gets the id api.
   *
   * @return the id api
   */
  public Long getIdApi() {
    return idApi;
  }


  /**
   * Sets the id api.
   *
   * @param idApi the new id api
   */
  public void setIdApi(final Long idApi) {
    this.idApi = idApi;
  }


  /**
   * Gets the inddup api.
   *
   * @return the inddup api
   */
  public Long getInddupApi() {
    return inddupApi;
  }


  /**
   * Sets the inddup api.
   *
   * @param inddupApi the new inddup api
   */
  public void setInddupApi(final Long inddupApi) {
    this.inddupApi = inddupApi;
  }


  /**
   * Gets the pendiente activar.
   *
   * @return the pendiente activar
   */
  public Boolean getPendienteActivar() {
    return pendienteActivar;
  }


  /**
   * Sets the pendiente activar.
   *
   * @param pendienteActivar the new pendiente activar
   */
  public void setPendienteActivar(final Boolean pendienteActivar) {
    this.pendienteActivar = pendienteActivar;
  }

  /**
   * Gets the app origen.
   *
   * @return the app origen
   */
  public String getAppOrigen() {
    return appOrigen;
  }

  /**
   * Sets the app origen.
   *
   * @param appOrigen the new app origen
   */
  public void setAppOrigen(final String appOrigen) {
    this.appOrigen = appOrigen;
  }

  /**
   * Gets the fecha modificacion.
   *
   * @return the fecha modificacion
   */
  public Date getFechaModificacion() {
    return UtilidadesCommons.cloneDate(fechaModificacion);
  }

  /**
   * Sets the fecha modificacion.
   *
   * @param fechaModificacion the new fecha modificacion
   */
  public void setFechaModificacion(final Date fechaModificacion) {
    this.fechaModificacion = UtilidadesCommons.cloneDate(fechaModificacion);
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
