package com.gval.gval.entity.model;

import java.io.Serializable;

import jakarta.persistence.*;

@Entity
@Table(name = "SDX_CAT_FORMACION", schema = "ADA")
public class CatalogoFormacion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PK_CAT_FORMACION", unique = true, nullable = false)
	private Long pkCatFormacion;

	@Column(name = "ACTIVO", nullable = false)
	private Boolean activo;

	@Column(name = "ESPECIFICO", nullable = false)
	private Boolean especifico;

	@Column(name = "NOMBRE", length = 200)
	private String nombre;

	@Column(name = "IMPARTIDOPOR", length = 200)
	private String impartidopor;

	@Column(name = "CODCAT_FORMACION", length = 10)
	private String codigo;

	@Column(name = "DURACION")
	private Long duracion;

	public Long getPkCatFormacion() {
		return pkCatFormacion;
	}

	public void setPkCatFormacion(Long pkCatFormacion) {
		this.pkCatFormacion = pkCatFormacion;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Boolean getEspecifico() {
		return especifico;
	}

	public void setEspecifico(Boolean especifico) {
		this.especifico = especifico;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getImpartidopor() {
		return impartidopor;
	}

	public void setImpartidopor(String impartidopor) {
		this.impartidopor = impartidopor;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Long getDuracion() {
		return duracion;
	}

	public void setDuracion(Long duracion) {
		this.duracion = duracion;
	}

}
