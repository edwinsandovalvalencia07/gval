package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;
import jakarta.persistence.*;

/**
 * The Class TipoIncidencia.
 */
@Entity
@Table(name = "SDX_TIPOINCI")
public class TipoIncidencia implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -5656937214161904319L;

  /** The pk tipoinci. */
  @Id
  @Column(name = "PK_TIPOINCI", unique = true, nullable = false)
  private Long pkTipoinci;

  /** The pk tipodocu. */
  @Column(name = "PK_TIPODOCU")
  private Long pkTipodocu;

  /** The autogenerado. */
  @Column(name = "TIAUTOGEN", nullable = false, precision = 1)
  private Boolean autogenerado;

  /** The bloquea. */
  @Column(name = "TIBLOQUEA", precision = 1)
  private Boolean bloquea;

  /** The borrable. */
  @Column(name = "TIBORRABLE", precision = 1)
  private Boolean borrable;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "TIFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The activo. */
  @Column(name = "TIIACTIVO", nullable = false, precision = 1)
  private Boolean activo;

  /** The nombre. */
  @Column(name = "TINOMBRE", nullable = false, length = 150)
  private String nombre;


  /**
   * Instantiates a new tipo incidencia.
   */
  public TipoIncidencia() {
    // Constructor
  }

  /**
   * Gets the pk tipoinci.
   *
   * @return the pk tipoinci
   */
  public Long getPkTipoinci() {
    return pkTipoinci;
  }

  /**
   * Gets the pk tipodocu.
   *
   * @return the pk tipodocu
   */
  public Long getPkTipodocu() {
    return pkTipodocu;
  }

  /**
   * Gets the autogenerado.
   *
   * @return the autogenerado
   */
  public Boolean getAutogenerado() {
    return autogenerado;
  }

  /**
   * Gets the bloquea.
   *
   * @return the bloquea
   */
  public Boolean getBloquea() {
    return bloquea;
  }

  /**
   * Gets the borrable.
   *
   * @return the borrable
   */
  public Boolean getBorrable() {
    return borrable;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return fechaCreacion;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Sets the pk tipoinci.
   *
   * @param pkTipoinci the new pk tipoinci
   */
  public void setPkTipoinci(final Long pkTipoinci) {
    this.pkTipoinci = pkTipoinci;
  }

  /**
   * Sets the pk tipodocu.
   *
   * @param pkTipodocu the new pk tipodocu
   */
  public void setPkTipodocu(final Long pkTipodocu) {
    this.pkTipodocu = pkTipodocu;
  }

  /**
   * Sets the autogenerado.
   *
   * @param autogenerado the new autogenerado
   */
  public void setAutogenerado(final Boolean autogenerado) {
    this.autogenerado = autogenerado;
  }

  /**
   * Sets the bloquea.
   *
   * @param bloquea the new bloquea
   */
  public void setBloquea(final Boolean bloquea) {
    this.bloquea = bloquea;
  }

  /**
   * Sets the borrable.
   *
   * @param borrable the new borrable
   */
  public void setBorrable(final Boolean borrable) {
    this.borrable = borrable;
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
