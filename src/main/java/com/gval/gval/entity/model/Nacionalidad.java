package com.gval.gval.entity.model;

import java.util.Date;

import jakarta.persistence.*;

/**
 * The type Nacionalidad.
 */
@Entity
@Table(name = "SDX_NACIONA", schema = "ADA")
public class Nacionalidad {

  /**
   * The Pk nacionalidad.
   */
  @Id
  @Column(name = "PK_NACIONA")
  private Long pkNacionalidad;

  /**
   * The Nombre nacionalidad.
   */
  @Column(name = "NANOMBRE")
  private String nombreNacionalidad;

  /**
   * The Cod imserso.
   */
  @Column(name = "NAIMSERSO")
  private String codImserso;

  /**
   * The Activo.
   */
  @Column(name = "NAACTIVO")
  private Long activo;

  /**
   * The Fech creacion.
   */
  @Temporal(TemporalType.DATE)
  @Column(name = "NAFECHCREA")
  private Date fechCreacion;

  /**
   * Instantiates a new Nacionalidad.
   */
  public Nacionalidad() {}

  /**
   * Gets pk nacionalidad.
   *
   * @return the pk nacionalidad
   */
  public Long getPkNacionalidad() {
    return pkNacionalidad;
  }

  /**
   * Sets pk nacionalidad.
   *
   * @param pkNacionalidad the pk nacionalidad
   */
  public void setPkNacionalidad(Long pkNacionalidad) {
    this.pkNacionalidad = pkNacionalidad;
  }

  /**
   * Gets nombre nacionalidad.
   *
   * @return the nombre nacionalidad
   */
  public String getNombreNacionalidad() {
    return nombreNacionalidad;
  }

  /**
   * Sets nombre nacionalidad.
   *
   * @param nombreNacionalidad the nombre nacionalidad
   */
  public void setNombreNacionalidad(String nombreNacionalidad) {
    this.nombreNacionalidad = nombreNacionalidad;
  }

  /**
   * Gets cod imserso.
   *
   * @return the cod imserso
   */
  public String getCodImserso() {
    return codImserso;
  }

  /**
   * Sets cod imserso.
   *
   * @param codImserso the cod imserso
   */
  public void setCodImserso(String codImserso) {
    this.codImserso = codImserso;
  }

  /**
   * Gets activo.
   *
   * @return the activo
   */
  public Long getActivo() {
    return activo;
  }

  /**
   * Sets activo.
   *
   * @param activo the activo
   */
  public void setActivo(Long activo) {
    this.activo = activo;
  }

  /**
   * Gets fech creacion.
   *
   * @return the fech creacion
   */
  public Date getFechCreacion() {
    return fechCreacion;
  }

  /**
   * Sets fech creacion.
   *
   * @param fechCreacion the fech creacion
   */
  public void setFechCreacion(Date fechCreacion) {
    this.fechCreacion = fechCreacion;
  }
}
