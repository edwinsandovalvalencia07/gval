package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

import jakarta.persistence.*;

/**
 * The Class TipoPreferencia.
 */
@Entity
@Table(name = "SDX_TIPO_PREFSOL")
public class TipoPreferencia implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 8320799740160348981L;

  /** The pk tipo prefsol. */
  @Id
  @Column(name = "pkTipoPrefsol", unique = true, nullable = false)
  private Long pkTipoPrefsol;

  /** The descripcion. */
  @Column(name = "TPDESC")
  private String descripcion;

  /**
   * Instantiates a new tipo preferencia.
   */
  public TipoPreferencia() {
    // Constructor
  }

  /**
   * Gets the pk tipo prefsol.
   *
   * @return the pk tipo prefsol
   */
  public Long getPkTipoPrefsol() {
    return pkTipoPrefsol;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the pk tipo prefsol.
   *
   * @param pkTipoPrefsol the new pk tipo prefsol
   */
  public void setPkTipoPrefsol(final Long pkTipoPrefsol) {
    this.pkTipoPrefsol = pkTipoPrefsol;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
