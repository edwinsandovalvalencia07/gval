package com.gval.gval.entity.model;

import java.io.Serializable;

import jakarta.persistence.*;


/**
 * The persistent class for the SDX_MOTIVOSFINALIZACION database table.
 * 
 */
@Entity
@Table(name = "SDX_MOTIVOSFINALIZACION")
public class MotivoFinalizacion implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk motivo finalizacion. */
  @Id
  @Column(name = "PK_MOTIVOSFINALIZACION", unique = true, nullable = false)
  private Long pkMotivoFinalizacion;

  /** The codigo. */
  @Column(name = "MOCODIGO", nullable = false, length = 4)
  private String codigo;

  /** The motivo. */
  @Column(name = "MOMOTIVO", length = 2000)
  private String motivo;


  /**
   * Instantiates a new motivo finalizacion.
   */
  public MotivoFinalizacion() {
    // Empty constructor
  }

  /**
   * Gets the pk motivo finalizacion.
   *
   * @return the pk motivo finalizacion
   */
  public Long getPkMotivoFinalizacion() {
    return pkMotivoFinalizacion;
  }

  /**
   * Sets the pk motivo finalizacion.
   *
   * @param pkMotivoFinalizacion the new pk motivo finalizacion
   */
  public void setPkMotivoFinalizacion(final Long pkMotivoFinalizacion) {
    this.pkMotivoFinalizacion = pkMotivoFinalizacion;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the motivo.
   *
   * @return the motivo
   */
  public String getMotivo() {
    return motivo;
  }

  /**
   * Sets the motivo.
   *
   * @param motivo the new motivo
   */
  public void setMotivo(final String motivo) {
    this.motivo = motivo;
  }

}
