package com.gval.gval.entity.model;

import com.gval.gval.common.ConstantesCommons;
import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;


import jakarta.persistence.*;


/**
 * The persistent class for the SDM_REPRESEN database table.
 *
 */
@Entity
@Table(name = "SDM_REPRESEN")
@GenericGenerator(name = "SDM_REPRESEN_PKREPRESEN_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_REPRESEN"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
public final class Representante implements Serializable {


  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 8579768429386926154L;

  /** The pk represen. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_REPRESEN_PKREPRESEN_GENERATOR")
  @Column(name = "PK_REPRESEN", unique = true, nullable = false)
  private Long pkRepresen;

  /** The activo. */
  @Column(name = "REACTIVO", nullable = false)
  private Boolean activo;

  /** The fecha representacion legal. */
  @Temporal(TemporalType.DATE)
  @Column(name = "REFECHARL")
  private Date fechaRepresentacionLegal;

  /** The fecha sentencia judicial. */
  @Temporal(TemporalType.DATE)
  @Column(name = "REFECHASENTENCIA")
  private Date fechaSentenciaJudicial;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "REFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The firma solicitud. */
  @Column(name = "REFIRMASOLI", precision = 1)
  private Boolean firmaSolicitud;

  /** The municipio tutelada. */
  @Column(name = "REMUNICIPIOTUTELADA", length = 50)
  private String municipioTutelada;

  /** The numero tutelada. */
  @Column(name = "RENUMTUTELADA", length = 3)
  private String numeroTutelada;


  /** The tipo representacion legal. */
  @Column(name = "RETIPO", length = 1)
  private String tipoRepresentacionLegal;

  /** The ultimo. */
  @Column(name = "REULTIMO")
  private Boolean ultimo;

  /** The app origen. */
  @Column(name = "APP_ORIGEN")
  private String appOrigen;

  /** The direccion ada. */
  // bi-directional many-to-one association to SdmDireccio
  @ManyToOne
  @JoinColumn(name = "PK_DIRECCIO")
  private DireccionAda direccionAda;

  /** The persona. */
  // bi-directional many-to-one association to SdmPersona
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA", nullable = false)
  private Persona persona;

  /** The solicitud. */
  // bi-directional many-to-one association to SdmSolicit
  @ManyToOne
  @JoinColumn(name = "PK_SOLICIT", nullable = false)
  private Solicitud solicitud;

  /** The unidad tutela. */
  // bi-directional many-to-one association to SdmUnidadtutela
  @ManyToOne
  @JoinColumn(name = "REUNIDADTUTELA")
  private UnidadTutela unidadTutela;

  /** The estado civil. */
  // bi-directional many-to-one association to SdxEstacivi
  @ManyToOne
  @JoinColumn(name = "PK_ESTACIVI")
  private EstadoCivil estadoCivil;

  /**
   * On pre persist.
   */
  @PrePersist
  public void onPrePersist() {
    appOrigen = ConstantesCommons.APLICACION_ADA3;
  }

  /**
   * Gets the pk represen.
   *
   * @return the pkRepresen
   */
  public Long getPkRepresen() {
    return pkRepresen;
  }

  /**
   * Sets the pk represen.
   *
   * @param pkRepresen the pkRepresen to set
   */
  public void setPkRepresen(final Long pkRepresen) {
    this.pkRepresen = pkRepresen;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha representacion legal.
   *
   * @return the fechaRepresentacionLegal
   */
  public Date getFechaRepresentacionLegal() {
    return UtilidadesCommons.cloneDate(fechaRepresentacionLegal);
  }

  /**
   * Sets the fecha representacion legal.
   *
   * @param fechaRepresentacionLegal the fechaRepresentacionLegal to set
   */
  public void setFechaRepresentacionLegal(final Date fechaRepresentacionLegal) {
    this.fechaRepresentacionLegal =
        UtilidadesCommons.cloneDate(fechaRepresentacionLegal);
  }

  /**
   * Gets the fecha sentencia judicial.
   *
   * @return the fechaSentenciaJudicial
   */
  public Date getFechaSentenciaJudicial() {
    return UtilidadesCommons.cloneDate(fechaSentenciaJudicial);
  }

  /**
   * Sets the fecha sentencia judicial.
   *
   * @param fechaSentenciaJudicial the fechaSentenciaJudicial to set
   */
  public void setFechaSentenciaJudicial(final Date fechaSentenciaJudicial) {
    this.fechaSentenciaJudicial =
        UtilidadesCommons.cloneDate(fechaSentenciaJudicial);
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the firma solicitud.
   *
   * @return the firmaSolicitud
   */
  public Boolean getFirmaSolicitud() {
    return firmaSolicitud;
  }

  /**
   * Sets the firma solicitud.
   *
   * @param firmaSolicitud the firmaSolicitud to set
   */
  public void setFirmaSolicitud(final Boolean firmaSolicitud) {
    this.firmaSolicitud = firmaSolicitud;
  }

  /**
   * Gets the municipio tutelada.
   *
   * @return the municipioTutelada
   */
  public String getMunicipioTutelada() {
    return municipioTutelada;
  }

  /**
   * Sets the municipio tutelada.
   *
   * @param municipioTutelada the municipioTutelada to set
   */
  public void setMunicipioTutelada(final String municipioTutelada) {
    this.municipioTutelada = municipioTutelada;
  }

  /**
   * Gets the numero tutelada.
   *
   * @return the numeroTutelada
   */
  public String getNumeroTutelada() {
    return numeroTutelada;
  }

  /**
   * Sets the numero tutelada.
   *
   * @param numeroTutelada the numeroTutelada to set
   */
  public void setNumeroTutelada(final String numeroTutelada) {
    this.numeroTutelada = numeroTutelada;
  }

  /**
   * Gets the tipo representacion legal.
   *
   * @return the tipoRepresentacionLegal
   */
  public String getTipoRepresentacionLegal() {
    return tipoRepresentacionLegal;
  }

  /**
   * Sets the tipo representacion legal.
   *
   * @param tipoRepresentacionLegal the tipoRepresentacionLegal to set
   */
  public void setTipoRepresentacionLegal(final String tipoRepresentacionLegal) {
    this.tipoRepresentacionLegal = tipoRepresentacionLegal;
  }

  /**
   * Gets the ultimo.
   *
   * @return the ultimo
   */
  public Boolean getUltimo() {
    return ultimo;
  }

  /**
   * Sets the ultimo.
   *
   * @param ultimo the new ultimo
   */
  public void setUltimo(final Boolean ultimo) {
    this.ultimo = ultimo;
  }

  /**
   * Gets the direccion ada.
   *
   * @return the direccionAda
   */
  public DireccionAda getDireccionAda() {
    return direccionAda;
  }

  /**
   * Sets the direccion ada.
   *
   * @param direccionAda the direccionAda to set
   */
  public void setDireccionAda(final DireccionAda direccionAda) {
    this.direccionAda = direccionAda;
  }

  /**
   * Gets the persona.
   *
   * @return the persona
   */
  public Persona getPersona() {
    return persona;
  }

  /**
   * Sets the persona.
   *
   * @param persona the persona to set
   */
  public void setPersona(final Persona persona) {
    this.persona = persona;
  }

  /**
   * Gets the solicitud.
   *
   * @return the solicitud
   */
  public Solicitud getSolicitud() {
    return solicitud;
  }

  /**
   * Sets the solicitud.
   *
   * @param solicitud the solicitud to set
   */
  public void setSolicitud(final Solicitud solicitud) {
    this.solicitud = solicitud;
  }

  /**
   * Gets the unidad tutela.
   *
   * @return the unidadTutela
   */
  public UnidadTutela getUnidadTutela() {
    return unidadTutela;
  }

  /**
   * Sets the unidad tutela.
   *
   * @param unidadTutela the unidadTutela to set
   */
  public void setUnidadTutela(final UnidadTutela unidadTutela) {
    this.unidadTutela = unidadTutela;
  }

  /**
   * Gets the estado civil.
   *
   * @return the estadoCivil
   */
  public EstadoCivil getEstadoCivil() {
    return estadoCivil;
  }

  /**
   * Sets the estado civil.
   *
   * @param estadoCivil the estadoCivil to set
   */
  public void setEstadoCivil(final EstadoCivil estadoCivil) {
    this.estadoCivil = estadoCivil;
  }

  /**
   * Gets the app origen.
   *
   * @return the app origen
   */
  public String getAppOrigen() {
    return appOrigen;
  }

  /**
   * Sets the app origen.
   *
   * @param appOrigen the new app origen
   */
  public void setAppOrigen(final String appOrigen) {
    this.appOrigen = appOrigen;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
