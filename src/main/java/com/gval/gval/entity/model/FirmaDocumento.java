package com.gval.gval.entity.model;

import com.gval.gval.common.UtilidadesCommons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;


/**
 * The persistent class for the SDM_FIRMDOCU database table.
 *
 */
@Entity
@Table(name = "SDM_FIRMDOCU")
public class FirmaDocumento implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The pk firma documento. */
  @Id
  @Column(name = "PK_FIRMDOCU", unique = true, nullable = false)
  private Long pkFirmaDocumento;

  /** The activo. */
  @Column(name = "FIACTIVO", nullable = false)
  private Boolean activo;

  /** The fecha hasta. */
  @Temporal(TemporalType.DATE)
  @Column(name = "FIFECHASTA")
  private Date fechaHasta;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "FIFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The fecha desde. */
  @Temporal(TemporalType.DATE)
  @Column(name = "FIFECHDESD", nullable = false)
  private Date fechaDesde;

  /** The suplente. */
  @Column(name = "FISUPLENTE", nullable = false)
  private Boolean suplente;

  /** The cargo. */
  // bi-directional many-to-one association to SdxCargo
  @ManyToOne
  @JoinColumn(name = "PK_CARGO", nullable = false)
  private Cargo cargo;

  /** The tipo documento. */
  // bi-directional many-to-one association to SdxTipodocu
  @ManyToOne
  @JoinColumn(name = "PK_TIPODOCU", nullable = false)
  private TipoDocumento tipoDocumento;

  /**
   * Instantiates a new firma documento.
   */
  public FirmaDocumento() {
    // Empty constructor
  }

  /**
   * Gets the pk firma documento.
   *
   * @return the pk firma documento
   */
  public Long getPkFirmaDocumento() {
    return pkFirmaDocumento;
  }

  /**
   * Sets the pk firma documento.
   *
   * @param pkFirmaDocumento the new pk firma documento
   */
  public void setPkFirmaDocumento(final Long pkFirmaDocumento) {
    this.pkFirmaDocumento = pkFirmaDocumento;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the fecha hasta.
   *
   * @return the fecha hasta
   */
  public Date getFechaHasta() {
    return UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Sets the fecha hasta.
   *
   * @param fechaHasta the new fecha hasta
   */
  public void setFechaHasta(final Date fechaHasta) {
    this.fechaHasta = UtilidadesCommons.cloneDate(fechaHasta);
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fecha creacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the new fecha creacion
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha desde.
   *
   * @return the fecha desde
   */
  public Date getFechaDesde() {
    return UtilidadesCommons.cloneDate(fechaDesde);
  }

  /**
   * Sets the fecha desde.
   *
   * @param fechaDesde the new fecha desde
   */
  public void setFechaDesde(final Date fechaDesde) {
    this.fechaDesde = UtilidadesCommons.cloneDate(fechaDesde);
  }

  /**
   * Gets the suplente.
   *
   * @return the suplente
   */
  public Boolean getSuplente() {
    return suplente;
  }

  /**
   * Sets the suplente.
   *
   * @param suplente the new suplente
   */
  public void setSuplente(final Boolean suplente) {
    this.suplente = suplente;
  }

  /**
   * Gets the cargo.
   *
   * @return the cargo
   */
  public Cargo getCargo() {
    return cargo;
  }

  /**
   * Sets the cargo.
   *
   * @param cargo the new cargo
   */
  public void setCargo(final Cargo cargo) {
    this.cargo = cargo;
  }

  /**
   * Gets the tipo documento.
   *
   * @return the tipo documento
   */
  public TipoDocumento getTipoDocumento() {
    return tipoDocumento;
  }

  /**
   * Sets the tipo documento.
   *
   * @param tipoDocumento the new tipo documento
   */
  public void setTipoDocumento(final TipoDocumento tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
