package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.*;

/**
 * The Class CBase.
 */
@Entity
@Table(name = "SDV_CBASE")
public class CBase implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 2133519558488064267L;

  /** The id. */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", updatable = false, nullable = false)
  private Long id;

  /** The maquina. */
  @Column(name = "EXP_MAQUINA", nullable = false, length = 1)
  private String maquina;

  /** The numero reset. */
  @Column(name = "EXP_NUMERO_RESET", nullable = false, precision = 10)
  private Long numeroReset;

  /** The provincia. */
  @Column(name = "SOL_PROVINCIA", length = 2)
  private String provincia;

  /** The identificador. */
  @Column(name = "SOL_DNI_SOLICITANTE", length = 15)
  private String identificador;

  /** The primer apellido. */
  @Column(name = "SOL_PRIMER_APELLIDO", length = 40)
  private String primerApellido;

  /** The segundo apellido. */
  @Column(name = "SOL_SEGUNDO_APELLIDO", length = 40)
  private String segundoApellido;

  /** The nombre. */
  @Column(name = "SOL_NOMBRE", nullable = false, length = 40)
  private String nombre;

  /** The grado discapacidad. */
  @Column(name = "VAL_GRADO", precision = 3)
  private Long gradoDiscapacidad;

  /** The puntos ATP. */
  @Column(name = "VAL_BAREMO_TERCERA", precision = 4)
  private Long puntosATP;

  /** The fecha nacimiento. */
  @Temporal(TemporalType.DATE)
  @Column(name = "SOL_FECHA_NACIMIENTO")
  private Date fechaNacimiento;

  /** The sexo. */
  @Column(name = "SOL_SEXO", length = 1)
  private String sexo;

  /** The fecha valoracion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "VAL_FECHA_VALORACION")
  private Date fechaValoracion;

  /** The fecha proxima revision. */
  @Temporal(TemporalType.DATE)
  @Column(name = "VAL_FECHA_PROX_REV")
  private Date fechaProximaRevision;

  /** The existe PDF. */
  @Column(name = "EXP_EXISTE_PDF_BVD", length = 1)
  private String existePDF;

  /** The resultado baremacion. */
  @Column(name = "RESULTADOBARTERCPERSCBASE", length = 2)
  private String resultadoBaremacion;

  /** The fecha junta. */
  @Temporal(TemporalType.DATE)
  @Column(name = "FECHAJUNTACBASE")
  private Date fechaJunta;

  /** The estado. */
  @Column(name = "ESTADOCBASE", length = 15)
  private String estado;

  /**
   * Instantiates a new c base.
   */
  public CBase() {
    // Constructor
  }

  /**
   * Gets the maquina.
   *
   * @return the maquina
   */
  public String getMaquina() {
    return maquina;
  }

  /**
   * Gets the numero reset.
   *
   * @return the numero reset
   */
  public Long getNumeroReset() {
    return numeroReset;
  }

  /**
   * Gets the provincia.
   *
   * @return the provincia
   */
  public String getProvincia() {
    return provincia;
  }

  /**
   * Gets the identificador.
   *
   * @return the identificador
   */
  public String getIdentificador() {
    return identificador;
  }

  /**
   * Gets the primer apellido.
   *
   * @return the primer apellido
   */
  public String getPrimerApellido() {
    return primerApellido;
  }

  /**
   * Gets the segundo apellido.
   *
   * @return the segundo apellido
   */
  public String getSegundoApellido() {
    return segundoApellido;
  }

  /**
   * Gets the nombre.
   *
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * Gets the grado discapacidad.
   *
   * @return the grado discapacidad
   */
  public Long getGradoDiscapacidad() {
    return gradoDiscapacidad;
  }

  /**
   * Gets the puntos ATP.
   *
   * @return the puntos ATP
   */
  public Long getPuntosATP() {
    return puntosATP;
  }

  /**
   * Gets the fecha nacimiento.
   *
   * @return the fecha nacimiento
   */
  public Date getFechaNacimiento() {
    return fechaNacimiento;
  }

  /**
   * Gets the sexo.
   *
   * @return the sexo
   */
  public String getSexo() {
    return sexo;
  }

  /**
   * Gets the fecha valoracion.
   *
   * @return the fecha valoracion
   */
  public Date getFechaValoracion() {
    return fechaValoracion;
  }

  /**
   * Gets the fecha proxima revision.
   *
   * @return the fecha proxima revision
   */
  public Date getFechaProximaRevision() {
    return fechaProximaRevision;
  }

  /**
   * Gets the existe PDF.
   *
   * @return the existe PDF
   */
  public String getExistePDF() {
    return existePDF;
  }

  /**
   * Gets the fecha junta.
   *
   * @return the fecha junta
   */
  public Date getFechaJunta() {
    return fechaJunta;
  }

  /**
   * Gets the estado.
   *
   * @return the estado
   */
  public String getEstado() {
    return estado;
  }

  /**
   * Sets the maquina.
   *
   * @param maquina the new maquina
   */
  public void setMaquina(final String maquina) {
    this.maquina = maquina;
  }

  /**
   * Sets the numero reset.
   *
   * @param numeroReset the new numero reset
   */
  public void setNumeroReset(final Long numeroReset) {
    this.numeroReset = numeroReset;
  }

  /**
   * Sets the provincia.
   *
   * @param provincia the new provincia
   */
  public void setProvincia(final String provincia) {
    this.provincia = provincia;
  }

  /**
   * Sets the identificador.
   *
   * @param identificador the new identificador
   */
  public void setIdentificador(final String identificador) {
    this.identificador = identificador;
  }

  /**
   * Sets the primer apellido.
   *
   * @param primerApellido the new primer apellido
   */
  public void setPrimerApellido(final String primerApellido) {
    this.primerApellido = primerApellido;
  }

  /**
   * Sets the segundo apellido.
   *
   * @param segundoApellido the new segundo apellido
   */
  public void setSegundoApellido(final String segundoApellido) {
    this.segundoApellido = segundoApellido;
  }

  /**
   * Sets the nombre.
   *
   * @param nombre the new nombre
   */
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }

  /**
   * Sets the grado discapacidad.
   *
   * @param gradoDiscapacidad the new grado discapacidad
   */
  public void setGradoDiscapacidad(final Long gradoDiscapacidad) {
    this.gradoDiscapacidad = gradoDiscapacidad;
  }

  /**
   * Sets the puntos ATP.
   *
   * @param puntosATP the new puntos ATP
   */
  public void setPuntosATP(final Long puntosATP) {
    this.puntosATP = puntosATP;
  }

  /**
   * Sets the fecha nacimiento.
   *
   * @param fechaNacimiento the new fecha nacimiento
   */
  public void setFechaNacimiento(final Date fechaNacimiento) {
    this.fechaNacimiento = fechaNacimiento;
  }

  /**
   * Sets the sexo.
   *
   * @param sexo the new sexo
   */
  public void setSexo(final String sexo) {
    this.sexo = sexo;
  }

  /**
   * Sets the fecha valoracion.
   *
   * @param fechaValoracion the new fecha valoracion
   */
  public void setFechaValoracion(final Date fechaValoracion) {
    this.fechaValoracion = fechaValoracion;
  }

  /**
   * Sets the fecha proxima revision.
   *
   * @param fechaProximaRevision the new fecha proxima revision
   */
  public void setFechaProximaRevision(final Date fechaProximaRevision) {
    this.fechaProximaRevision = fechaProximaRevision;
  }

  /**
   * Sets the existe PDF.
   *
   * @param existePDF the new existe PDF
   */
  public void setExistePDF(final String existePDF) {
    this.existePDF = existePDF;
  }

  /**
   * Sets the fecha junta.
   *
   * @param fechaJunta the new fecha junta
   */
  public void setFechaJunta(final Date fechaJunta) {
    this.fechaJunta = fechaJunta;
  }

  /**
   * Sets the estado.
   *
   * @param estado the new estado
   */
  public void setEstado(final String estado) {
    this.estado = estado;
  }

  /**
   * Gets the id.
   *
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(final Long id) {
    this.id = id;
  }

  /**
   * Gets the resultado baremacion.
   *
   * @return the resultado baremacion
   */
  public String getResultadoBaremacion() {
    return resultadoBaremacion;
  }

  /**
   * Sets the resultado baremacion.
   *
   * @param resultadoBaremacion the new resultado baremacion
   */
  public void setResultadoBaremacion(final String resultadoBaremacion) {
    this.resultadoBaremacion = resultadoBaremacion;
  }



  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
