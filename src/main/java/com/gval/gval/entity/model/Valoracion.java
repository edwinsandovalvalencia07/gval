package com.gval.gval.entity.model;

import com.gval.gval.common.ConstantesCommons;
import com.gval.gval.common.UtilidadesCommons;
import jakarta.persistence.*;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.Date;




/**
 * The persistent class for the SDM_VALORACI database table.
 *
 */
@Entity
@Table(name = "SDM_VALORACI")
@NamedStoredProcedureQuery(name = "valoraciones.eliminar",
    procedureName = "ELIMINAR_VALORACION",
    parameters = {@StoredProcedureParameter(mode = ParameterMode.IN,
        name = "P_PK_VALORACI", type = Long.class)})
@GenericGenerator(name = "SDM_VALORACI_PK_VALORACI_GENERATOR",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
        @Parameter(name = "sequence_name", value = "SEC_SDM_VALORACI"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")})
//
public final class Valoracion implements Serializable {


  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -2541618386283794637L;

  /** The pk valoracion. */
  @Id
  @Column(name = "PK_VALORACI", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE,
      generator = "SDM_VALORACI_PK_VALORACI_GENERATOR")
  private Long pkValoracion;

  /** The migrado sidep. */
  @Column(name = "MIGRADO_SIDEP", length = 1)
  private String migradoSidep;

  /** The documento valoracion. */
  @Column(name = "PK_DOCU2")
  private Long documentoValoracion;

  /** The pk grupoval. */
  @Column(name = "PK_GRUPOVAL")
  private Long pkGrupoValoracion;

  /** The activo. */
  @Column(name = "VAACTIVO", nullable = false)
  private Boolean activo;

  /** The cerrado. */
  @Column(name = "VACERRADO", nullable = false)
  private Boolean cerrado;

  /** The fecha creacion. */
  @Temporal(TemporalType.DATE)
  @Column(name = "VAFECHCREA", nullable = false)
  private Date fechaCreacion;

  /** The fecha real. */
  @Temporal(TemporalType.DATE)
  @Column(name = "VAFECHREAL")
  private Date fechaReal;

  /** The grado. */
  @Column(name = "VAGRADO", length = 1)
  private String grado;

  /** The nivel. */
  @Column(name = "VANIVEL", length = 1)
  private String nivel;

  /** The tipo valoracion. */
  @Column(name = "VATIPOVALO", length = 3)
  private String tipoValoracion;

  /** The subtipo valoracion. */
  @Column(name = "VASUBTIPOVALO", length = 2)
  private String subtipoValoracion;

  /** The version. */
  @Column(name = "VERSION", length = 50)
  private String version;

  /** The app origen. */
  @Column(name = "APP_ORIGEN")
  private String appOrigen;

  /** The asignar solicitud. */
  // bi-directional many-to-one association to SdmAsigsoli
  @ManyToOne
  @JoinColumn(name = "PK_ASIGSOLI", nullable = false)
  private AsignarSolicitud asignarSolicitud;

  /** The documento generado. */
  // bi-directional many-to-one association to SdmDocugene
  @ManyToOne
  @JoinColumn(name = "PK_DOCU")
  private DocumentoGenerado documentoGenerado;

  /** The usuario. */
  // bi-directional many-to-one association to SdxUsuario
  @ManyToOne
  @JoinColumn(name = "PK_PERSONA", nullable = false)
  private Usuario usuario;



  /**
   * Instantiates a new valoracion.
   */
  public Valoracion() {
    // Constructor vacío
  }

  /**
   * On pre persist.
   */
  @PrePersist
  public void onPrePersist() {
    appOrigen = ConstantesCommons.APLICACION_ADA3;
  }

  /**
   * On pre update.
   */
  @PreUpdate
  public void onPreUpdate() {
    appOrigen = ConstantesCommons.APLICACION_ADA3;
  }

  /**
   * Gets the pk valoracion.
   *
   * @return the pkValoracion
   */
  public Long getPkValoracion() {
    return pkValoracion;
  }

  /**
   * Sets the pk valoracion.
   *
   * @param pkValoracion the pkValoracion to set
   */
  public void setPkValoracion(final Long pkValoracion) {
    this.pkValoracion = pkValoracion;
  }

  /**
   * Gets the migrado sidep.
   *
   * @return the migradoSidep
   */
  public String getMigradoSidep() {
    return migradoSidep;
  }

  /**
   * Sets the migrado sidep.
   *
   * @param migradoSidep the migradoSidep to set
   */
  public void setMigradoSidep(final String migradoSidep) {
    this.migradoSidep = migradoSidep;
  }

  /**
   * Gets the documento valoracion.
   *
   * @return the documentoValoracion
   */
  public Long getDocumentoValoracion() {
    return documentoValoracion;
  }

  /**
   * Sets the documento valoracion.
   *
   * @param documentoValoracion the documentoValoracion to set
   */
  public void setDocumentoValoracion(final Long documentoValoracion) {
    this.documentoValoracion = documentoValoracion;
  }

  /**
   * Gets the pk grupo valoracion.
   *
   * @return the pkGrupoValoracion
   */
  public Long getPkGrupoValoracion() {
    return pkGrupoValoracion;
  }

  /**
   * Sets the pk grupo valoracion.
   *
   * @param pkGrupoValoracion the pkGrupoValoracion to set
   */
  public void setPkGrupoValoracion(final Long pkGrupoValoracion) {
    this.pkGrupoValoracion = pkGrupoValoracion;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Boolean getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the activo to set
   */
  public void setActivo(final Boolean activo) {
    this.activo = activo;
  }

  /**
   * Gets the cerrado.
   *
   * @return the cerrado
   */
  public Boolean getCerrado() {
    return cerrado;
  }

  /**
   * Sets the cerrado.
   *
   * @param cerrado the cerrado to set
   */
  public void setCerrado(final Boolean cerrado) {
    this.cerrado = cerrado;
  }

  /**
   * Gets the fecha creacion.
   *
   * @return the fechaCreacion
   */
  public Date getFechaCreacion() {
    return UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Sets the fecha creacion.
   *
   * @param fechaCreacion the fechaCreacion to set
   */
  public void setFechaCreacion(final Date fechaCreacion) {
    this.fechaCreacion = UtilidadesCommons.cloneDate(fechaCreacion);
  }

  /**
   * Gets the fecha real.
   *
   * @return the fechaReal
   */
  public Date getFechaReal() {
    return UtilidadesCommons.cloneDate(fechaReal);
  }

  /**
   * Sets the fecha real.
   *
   * @param fechaReal the fechaReal to set
   */
  public void setFechaReal(final Date fechaReal) {
    this.fechaReal = UtilidadesCommons.cloneDate(fechaReal);
  }

  /**
   * Gets the grado.
   *
   * @return the grado
   */
  public String getGrado() {
    return grado;
  }

  /**
   * Sets the grado.
   *
   * @param grado the grado to set
   */
  public void setGrado(final String grado) {
    this.grado = grado;
  }

  /**
   * Gets the nivel.
   *
   * @return the nivel
   */
  public String getNivel() {
    return nivel;
  }

  /**
   * Sets the nivel.
   *
   * @param nivel the nivel to set
   */
  public void setNivel(final String nivel) {
    this.nivel = nivel;
  }

  /**
   * Gets the tipo valoracion.
   *
   * @return the tipoValoracion
   */
  public String getTipoValoracion() {
    return tipoValoracion;
  }

  /**
   * Sets the tipo valoracion.
   *
   * @param tipoValoracion the tipoValoracion to set
   */
  public void setTipoValoracion(final String tipoValoracion) {
    this.tipoValoracion = tipoValoracion;
  }

  /**
   * Gets the version.
   *
   * @return the version
   */
  public String getVersion() {
    return version;
  }

  /**
   * Sets the version.
   *
   * @param version the version to set
   */
  public void setVersion(final String version) {
    this.version = version;
  }

  /**
   * Gets the asignar solicitud.
   *
   * @return the asignarSolicitud
   */
  public AsignarSolicitud getAsignarSolicitud() {
    return asignarSolicitud;
  }

  /**
   * Sets the asignar solicitud.
   *
   * @param asignarSolicitud the asignarSolicitud to set
   */
  public void setAsignarSolicitud(final AsignarSolicitud asignarSolicitud) {
    this.asignarSolicitud = asignarSolicitud;
  }

  /**
   * Gets the documento generado.
   *
   * @return the documentoGenerado
   */
  public DocumentoGenerado getDocumentoGenerado() {
    return documentoGenerado;
  }

  /**
   * Sets the documento generado.
   *
   * @param documentoGenerado the documentoGenerado to set
   */
  public void setDocumentoGenerado(final DocumentoGenerado documentoGenerado) {
    this.documentoGenerado = documentoGenerado;
  }

  /**
   * Gets the usuario.
   *
   * @return the usuario
   */
  public Usuario getUsuario() {
    return usuario;
  }

  /**
   * Sets the usuario.
   *
   * @param usuario the usuario to set
   */
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }

  /**
   * Gets the subtipo valoracion.
   *
   * @return the subtipo valoracion
   */
  public String getSubtipoValoracion() {
    return subtipoValoracion;
  }

  /**
   * Sets the subtipo valoracion.
   *
   * @param subtipoValoracion the new subtipo valoracion
   */
  public void setSubtipoValoracion(final String subtipoValoracion) {
    this.subtipoValoracion = subtipoValoracion;
  }

  /**
   * Gets the app origen.
   *
   * @return the app origen
   */
  public String getAppOrigen() {
    return appOrigen;
  }

  /**
   * Sets the app origen.
   *
   * @param appOrigen the new app origen
   */
  public void setAppOrigen(final String appOrigen) {
    this.appOrigen = appOrigen;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
