package com.gval.gval.entity.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

import jakarta.persistence.*;


/**
 * The persistent class for the SDM_CONF database table.
 *
 */
@Entity
@Table(name = "SDM_CONF")
public class Configuracion implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 7682557629115495153L;

  /** The pk configuracion. */
  @Id
  @Column(name = "PK_CONF", unique = true, nullable = false)
  private Long pkConfiguracion;

  /** The codigo Padre. */
  @Column(name = "COCODIGO_PADRE", nullable = false, length = 50)
  private String codigoPadre;

  /** The codigo. */
  @Column(name = "COCODIGO", nullable = false, length = 50)
  private String codigo;

  /** The descripcion. */
  @Column(name = "CODESC", nullable = false, length = 250)
  private String descripcion;

  /** The valor caracter. */
  @Column(name = "COVALOCARAR", length = 250)
  private String valorCaracter;

  /** The valor numerico. */
  @Column(name = "COVALONUME")
  private Long valorNumerico;

  /**  The Label. */
  @Column(name = "LABEL", nullable = false, length = 50)
  private String label;

  /**
   * Instantiates a new configuracion.
   */
  public Configuracion() {
    // Empty constructor
  }

  /**
   * Gets the pk configuracion.
   *
   * @return the pk configuracion
   */
  public Long getPkConfiguracion() {
    return pkConfiguracion;
  }

  /**
   * Sets the pk configuracion.
   *
   * @param pkConfiguracion the new pk configuracion
   */
  public void setPkConfiguracion(final Long pkConfiguracion) {
    this.pkConfiguracion = pkConfiguracion;
  }

  /**
   * Gets the codigo.
   *
   * @return the codigo
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the codigo.
   *
   * @param codigo the new codigo
   */
  public void setCodigo(final String codigo) {
    this.codigo = codigo;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the valor caracter.
   *
   * @return the valor caracter
   */
  public String getValorCaracter() {
    return valorCaracter;
  }

  /**
   * Sets the valor caracter.
   *
   * @param valorCaracter the new valor caracter
   */
  public void setValorCaracter(final String valorCaracter) {
    this.valorCaracter = valorCaracter;
  }

  /**
   * Gets the valor numerico.
   *
   * @return the valor numerico
   */
  public Long getValorNumerico() {
    return valorNumerico;
  }

  /**
   * Sets the valor numerico.
   *
   * @param valorNumerico the new valor numerico
   */
  public void setValorNumerico(final Long valorNumerico) {
    this.valorNumerico = valorNumerico;
  }

  /**
   * Gets the codigo padre.
   *
   * @return the codigoPadre
   */
  public String getCodigoPadre() {
    return codigoPadre;
  }

  /**
   * Sets the codigo padre.
   *
   * @param codigoPadre the codigoPadre to set
   */
  public void setCodigoPadre(final String codigoPadre) {
    this.codigoPadre = codigoPadre;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * Sets the label.
   *
   * @param label the label to set
   */
  public void setLabel(final String label) {
    this.label = label;
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

}
