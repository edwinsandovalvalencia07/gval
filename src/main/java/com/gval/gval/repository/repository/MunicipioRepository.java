package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Municipio;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * The Interface MunicipioRepository.
 */
@Repository
public interface MunicipioRepository extends JpaRepository<Municipio, Long>,
    ExtendedQuerydslPredicateExecutor<Municipio> {

  /**
   * Find by codigo municipio and codigo provincia.
   *
   * @param codigoMunicipio the codigo municipio
   * @param codigoProvincia the codigo provincia
   * @return the optional
   */
  Optional<Municipio> findByCodigoMunicipioAndCodigoProvincia(
      Long codigoMunicipio, Long codigoProvincia);

}
