package com.gval.gval.repository.repository;


import com.gval.gval.dto.dto.HistoricoEstadosSituacionesDTO;
import com.gval.gval.entity.model.Incidencia;
import com.gval.gval.entity.model.Solicitud;
import com.gval.gval.entity.model.Usuario;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;


/**
 * The Interface IncidenciaRepository.
 */
@Repository
public interface IncidenciaRepository extends JpaRepository<Incidencia, Long>,
    ExtendedQuerydslPredicateExecutor<Incidencia> {

  /**
   * Find by solicitud and activo true and tipo incidencia autogenerado false
   * order by tipo incidencia.
   *
   * @param solicitud the solicitud
   * @return the list
   */
  List<Incidencia> findBySolicitudAndActivoTrueAndTipoIncidenciaAutogeneradoFalseOrderByTipoIncidencia(
      Solicitud solicitud);


  /**
   * Find by solicitud and activo true and descripcion order by pk incidenc.
   *
   * @param solicitud the solicitud
   * @param descripcion the descripcion
   * @return the list
   */
  List<Incidencia> findBySolicitudAndActivoTrueAndDescripcionOrderByPkIncidenc(
      Solicitud solicitud, String descripcion);

  /**
   * Find by pk docu and estado not and activo true.
   *
   * @param pkDocu the pk docu
   * @param estado the estado
   * @return the list
   */
  List<Incidencia> findByPkDocuAndEstadoNotAndActivoTrue(Long pkDocu,
      String estado);

  /**
   * Find by solicitud and estado not and activo true.
   *
   * @param entity the entity
   * @param string the string
   * @return the incidencia
   */
  Incidencia findBySolicitudAndEstadoNotAndActivoTrue(Solicitud entity,
      String string);

  /**
   * Find by solicitud and estado not and tipo incidencia bloquea true and
   * activo true.
   *
   * @param pkSolicitud the pk solicitud
   * @return the list
   */
  @Query("select I from Incidencia I JOIN I.solicitud S JOIN I.tipoIncidencia TI "
      + "WHERE S.pkSolicitud = :pkSolicitud "
      + "AND I.estado <> 'C' AND TI.bloquea = true "
      + "AND I.activo = true AND TI.activo = true "
      + "ORDER BY I.pkIncidenc DESC")
  List<Incidencia> findBySolicitudAndEstadoNotAndTipoIncidenciaBloqueaTrueAndActivoTrue(
      @Param("pkSolicitud") Long pkSolicitud);

  /**
   * Find historico estados situaciones from solicitud.
   *
   * @param solicitud the solicitud
   * @return the list
   */
  @Query("select new es.gva.dependencia.ada.dto.HistoricoEstadosSituacionesDTO(inci.fechaCreacion, ti.nombre, inci.descripcion) "
      + " from Incidencia inci inner join inci.tipoIncidencia ti "
      + " where inci.activo = 1 and inci.solicitud = :solicitud "
      + " order by inci.fechaCreacion ASC ")
  List<HistoricoEstadosSituacionesDTO> findHistoricoEstadosSituacionesFromSolicitud(
      @Param("solicitud") Solicitud solicitud);


  /**
   * Find by solicitud and usuario and tipo incidencia pk tipoinci.
   *
   * @param solicitud the solicitud
   * @param usuario the usuario
   * @param pkTipoinci the pk tipoinci
   * @param sort the sort
   * @return the list
   */
  Optional<List<Incidencia>> findBySolicitudAndUsuarioAndTipoIncidenciaPkTipoinci(
      Solicitud solicitud, Usuario usuario, Long pkTipoinci, Sort sort);

  /**
   * Obtener incidencias bloqueo solicitud por usuario fecha.
   *
   * @param pkSolicitud the pk solicitud
   * @param pkTipoinci the pk tipoinci
   * @param pkUsuario the pk usuario
   * @param diaAnterior the dia anterior
   * @param diaPosterior the dia posterior
   * @return the list
   */
  @Query("SELECT I from Incidencia I JOIN I.solicitud S JOIN I.tipoIncidencia TI JOIN I.usuario U "
      + "WHERE S.pkSolicitud = :pkSolicitud "
      + "AND TI.pkTipoinci = :pkTipoinci AND I.estado <> 'C' "
      + "AND U.pkPersona = :pkUsuario "
      + "AND I.fechaCreacion BETWEEN :diaAnterior AND :diaPosterior "
      + "AND I.activo = true ")
  List<Incidencia> obtenerIncidenciasBloqueoSolicitudPorUsuarioFecha(
      @Param("pkSolicitud") Long pkSolicitud,
      @Param("pkTipoinci") Long pkTipoinci, @Param("pkUsuario") Long pkUsuario,
      @Param("diaAnterior") Date diaAnterior,
      @Param("diaPosterior") Date diaPosterior);

  /**
   * Obtener incidencias bloqueo solicitud por usuario.
   *
   * @param pkSolicitud the pk solicitud
   * @param pkTipoinci the pk tipoinci
   * @param pkUsuario the pk usuario
   * @return the list
   */
  @Query("select I from Incidencia I JOIN I.solicitud S JOIN I.tipoIncidencia TI JOIN I.usuario U "
      + "WHERE S.pkSolicitud = :pkSolicitud "
      + "AND TI.pkTipoinci = :pkTipoinci AND I.estado <> 'C' "
      + "AND U.pkPersona = :pkUsuario " + "AND I.activo = true ")
  List<Incidencia> obtenerIncidenciasBloqueoSolicitudPorUsuario(
      @Param("pkSolicitud") Long pkSolicitud,
      @Param("pkTipoinci") Long pkTipoinci, @Param("pkUsuario") Long pkUsuario);

  /**
   * Obtener incidencias bloqueo solicitud.
   *
   * @param pkSolicitud the pk solicitud
   * @param pkTipoinci the pk tipoinci
   * @return the list
   */
  @Query("select I from Incidencia I JOIN I.solicitud S JOIN I.tipoIncidencia TI "
      + "WHERE S.pkSolicitud = :pkSolicitud "
      + "AND TI.pkTipoinci = :pkTipoinci AND I.estado <> 'C' "
      + "AND I.activo = true ")
  List<Incidencia> obtenerIncidenciasBloqueoSolicitud(
      @Param("pkSolicitud") Long pkSolicitud,
      @Param("pkTipoinci") Long pkTipoinci);

  /**
   * Obtener incidencia no validacion por discrepancia.
   *
   * @param pkDocu the pk docu
   * @param pkTipoinci the pk tipoinci
   * @return the optional
   */
  @Query("Select I from Incidencia I JOIN I.tipoIncidencia TI "
      + "WHERE TI.pkTipoinci = :pkTipoinci AND I.pkDocu = :pkDocu "
      + "AND (I.estado = 'I' OR I.estado = 'N') AND I.activo = true")
  Incidencia obtenerIncidenciaNoValidacionPorDiscrepancia(
      @Param("pkDocu") Long pkDocu, @Param("pkTipoinci") Long pkTipoinci);

  /**
   * Buscar incidencia no validacion por discrepancia fecha de creacion.
   *
   * @param pkDocu the pk docu
   * @return the optional
   */
  @Query("SELECT I from Incidencia I JOIN I.tipoIncidencia TI "
      + "WHERE I.pkDocu = :pkDocu " + "AND I.estado = 'C' "
      + "ORDER BY I.pkIncidenc DESC")
  List<Incidencia> buscarIncidenciasNoValidacionPorDiscrepanciaCerradas(
      @Param("pkDocu") Long pkDocu);

  /**
   * Bobtener incidencias bloqueo solicitud cerradas.
   *
   * @param pkSolicitud the pk solicitud
   * @return the list
   */
  @Query("select I from Incidencia I JOIN I.solicitud S JOIN I.tipoIncidencia TI "
      + "WHERE S.pkSolicitud = :pkSolicitud "
      + "AND TI.pkTipoinci = 364 AND I.estado = 'C' " + "AND I.activo = false ")
  List<Incidencia> obtenerIncidenciasBloqueoSolicitudCerradas(
      @Param("pkSolicitud") Long pkSolicitud);


  /**
   * Obtener incidencia documento.
   *
   * @param pkDocu the pk docu
   * @return the incidencia
   */
  @Query("Select I from Incidencia I JOIN I.tipoIncidencia TI "
      + "WHERE I.pkDocu = :pkDocu " + "AND I.activo = true")
  List<Incidencia> obtenerIncidenciaDocumento(@Param("pkDocu") Long pkDocu);
}
