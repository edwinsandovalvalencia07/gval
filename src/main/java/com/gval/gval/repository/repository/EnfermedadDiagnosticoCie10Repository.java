package com.gval.gval.repository.repository;

import com.gval.gval.entity.vistas.model.EnfermedadDiagnosticoCie10;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The Class EnfermedadDiagnosticoCie10Repository.
 */
@Repository
public interface EnfermedadDiagnosticoCie10Repository
    extends JpaRepository<EnfermedadDiagnosticoCie10, Long> {

  /**
   * Obtener enfermedades diagnosticos.
   *
   * @param codigoCapitulo the codigo capitulo
   * @return the list
   */
  @Query(
      value = " SELECT * FROM SDV_LD_ENFERMEDADES WHERE COD_CAPITULO = :cod_capitulo ",
      nativeQuery = true)
  List<EnfermedadDiagnosticoCie10> obtenerEnfermedadesDiagnosticos(
      @Param("cod_capitulo") String codigoCapitulo);


}
