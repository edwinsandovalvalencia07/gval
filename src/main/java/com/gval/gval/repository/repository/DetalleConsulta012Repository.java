package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.DetalleConsulta012;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The Interface DetalleConsulta012Repository.
 */
@Repository
public interface DetalleConsulta012Repository
    extends JpaRepository<DetalleConsulta012, Long>,
    ExtendedQuerydslPredicateExecutor<DetalleConsulta012> {
}
