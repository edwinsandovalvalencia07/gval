package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Provincia;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * The Interface ProvinciaRepository.
 */
@Repository
public interface ProvinciaRepository extends JpaRepository<Provincia, Long>,
    ExtendedQuerydslPredicateExecutor<Provincia> {

  /**
   * Find by imserso.
   *
   * @param codigoProvincia the codigo provincia
   * @return the optional
   */
  Optional<Provincia> findByImserso(String codigoProvincia);

  /**
   * Find by comunidad autonoma pk comauto.
   *
   * @param pkComauto the pk comauto
   * @return the list
   */
  List<Provincia> findByComunidadAutonomaPkComauto(Long pkComauto);



  /**
   * Find by imserso in.
   *
   * @param listaCodigoProvincia the lista codigo provincia
   * @return the list
   */
  List<Provincia> findByImsersoIn(List<String> listaCodigoProvincia);

}
