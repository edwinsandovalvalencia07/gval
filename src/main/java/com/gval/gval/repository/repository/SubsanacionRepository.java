package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.DocumentoGenerado;
import com.gval.gval.entity.model.Solicitud;
import com.gval.gval.entity.model.Subsanacion;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * The Interface SubsanacionRepository.
 */
@Repository
public interface SubsanacionRepository extends JpaRepository<Subsanacion, Long>,
    ExtendedQuerydslPredicateExecutor<Subsanacion> {


  /**
   * Find by solicitud and activo true and fecha completado is null.
   *
   * @param solicitud the solicitud
   * @return the list
   */
  List<Subsanacion> findBySolicitudAndActivoTrueAndFechaCompletadoIsNull(
      Solicitud solicitud);

  /**
   * Find by documento generado.
   *
   * @param documentoGenerado the documento generado
   * @return the subsanacion
   */
  Subsanacion findByDocumentoGenerado(DocumentoGenerado documentoGenerado);

  /**
   * Find by solicitud pk solicitud and activo true fecha completado is null.
   *
   * @param pkSolicitud the pk solicitud
   * @return the list
   */
  List<Subsanacion> findBySolicitudPkSolicitudAndActivoTrueAndFechaCompletadoIsNull(
      Long pkSolicitud);

  /**
   * Find by solicitud expediente pk expedien and activo true and fecha
   * completado is null.
   *
   * @param pkExpedien the pk expedien
   * @param sort the sort
   * @return the list
   */
  List<Subsanacion> findBySolicitudExpedientePkExpedienAndActivoTrueAndFechaCompletadoIsNullAndSolicitudActivoTrue(
      Long pkExpedien, Sort sort);

  /**
   * Find by solicitud pk solicitud and activo true.
   *
   * @param pkSolicitud the pk solicitud
   * @return the optional
   */
  Optional<List<Subsanacion>> findBySolicitudPkSolicitudAndActivoTrue(
      Long pkSolicitud);

  /**
   * Comprobar bloqueo documento.
   *
   * @param pkSolicitud the pk solicitud
   * @param pkTipodocu the pk tipodocu
   * @return the string
   */
  @Query(value = "select ES_DOCUMENTO_BLOQUEANTE(:pkSolicitud, :pkTipodocu)"
      + " from dual ", nativeQuery = true)
  String comprobarBloqueoDocumento(@Param("pkSolicitud") Long pkSolicitud,
      @Param("pkTipodocu") Long pkTipodocu);

  /**
   * Find by documento A subsanar.
   *
   * @param pkDocumento the pk documento
   * @return the subsanacion
   */
  List<Subsanacion> findByDocumentoASubsanarPkDocumentoAndActivoTrueOrderByPkSubsanacionDesc(
      Long pkDocumento);

  /**
   * Find by documento A subsanar pk documento and activo false order by pk
   * subsanacion desc.
   *
   * @param pkDocumento the pk documento
   * @return the list
   */
  List<Subsanacion> findByDocumentoASubsanarPkDocumentoAndActivoFalseOrderByPkSubsanacionDesc(
      Long pkDocumento);
}
