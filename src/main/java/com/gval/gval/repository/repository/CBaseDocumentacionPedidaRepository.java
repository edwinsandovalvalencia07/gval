package com.gval.gval.repository.repository;


import com.gval.gval.entity.CBaseDocumentacionPedida;
import com.gval.gval.entity.CBaseDocumentacionPedidaPk;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The Interface CBaseDocumentacionPedidaRepository.
 */
@Repository
public interface CBaseDocumentacionPedidaRepository
    extends JpaRepository<CBaseDocumentacionPedida, CBaseDocumentacionPedidaPk>,
        ExtendedQuerydslPredicateExecutor<CBaseDocumentacionPedida> {

  /**
   * Gets the sdv basedocumentacionpedida de persona.
   *
   * @param documentoPersona the documento persona
   * @return the sdv basedocumentacionpedida de persona
   */
  @Query(
      value = "SELECT * FROM SDV_CBASE_DOCPEDIDA WHERE SOL_DNI_SOLICITANTE = :identificador",
      nativeQuery = true)
  List<CBaseDocumentacionPedida> findCBaseDocumentacionPedidaPorIdentificador(
      @Param("identificador") String documentoPersona);
}
