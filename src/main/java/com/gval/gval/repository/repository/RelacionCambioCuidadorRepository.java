package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.RelacionCambioCuidador;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * The Interface RelacionCambioCuidadorRepository.
 */
@Repository
public interface RelacionCambioCuidadorRepository
    extends JpaRepository<RelacionCambioCuidador, Long>,
    ExtendedQuerydslPredicateExecutor<RelacionCambioCuidador> {



}
