package com.gval.gval.repository.repository.impl;


import java.math.BigDecimal;
import java.util.List;

import com.gval.gval.repository.repository.custom.SolicitudRepositoryCustom;
import jakarta.persistence.*;


/**
 * The Class SolicitudRepositoryImpl.
 */
public class SolicitudRepositoryImpl implements SolicitudRepositoryCustom {

  /** The Constant ESCENARIO_CALCULO. */
  private static final String ESCENARIO_CALCULO = "ESCENARIO_CALCULO";

  /** The Constant OBTENER_DOCUMENTOS_REQUERIBLES. */
  private static final String OBTENER_DOCUMENTOS_REQUERIBLES =
      "OBTENER_DOCUMENTOS_REQUERIBLES";

  /** The entity manager. */
  @PersistenceContext
  EntityManager entityManager;

  /**
   * Obtener escenario calculado.
   *
   * @param pkSolicitud the pk solicitud
   * @param revision the revision
   * @return the list
   */
  @Override
  public List<BigDecimal[]> obtenerEscenarioCalculado(final Long pkSolicitud,
      final Boolean revision) {
    final int pkSolicitParam = 1;
    final int soRevisionParam = 2;
    final int cDocusOutParam = 3;

    final StoredProcedureQuery spq =
        entityManager.createStoredProcedureQuery(ESCENARIO_CALCULO);
    spq.registerStoredProcedureParameter(pkSolicitParam, Long.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(soRevisionParam, Boolean.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(cDocusOutParam, void.class,
        ParameterMode.REF_CURSOR);
    spq.setParameter(pkSolicitParam, pkSolicitud);
    spq.setParameter(soRevisionParam, revision);

    spq.execute();
    return spq.getResultList();
  }

  /**
   * Obtener documentos requeribles.
   *
   * @param pkSolicitud the pk solicitud
   * @param codigoPerfil the codigo perfil
   * @return the list
   */
  @Override
  public List<Object[]> obtenerDocumentosRequeribles(final Long pkSolicitud,
      final String codigoPerfil) {
    final int pkSolicitParam = 1;
    final int perfilParam = 2;
    final int cDocusOutParam = 3;

    final StoredProcedureQuery spq = entityManager
        .createStoredProcedureQuery(OBTENER_DOCUMENTOS_REQUERIBLES);
    spq.registerStoredProcedureParameter(pkSolicitParam, Long.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(perfilParam, String.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(cDocusOutParam, void.class,
        ParameterMode.REF_CURSOR);
    spq.setParameter(pkSolicitParam, pkSolicitud);
    spq.setParameter(perfilParam, codigoPerfil);

    spq.execute();
    return spq.getResultList();
  }
}
