package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.EstadoCivil;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The Interface EstadoCivilRepository.
 */
@Repository
public interface EstadoCivilRepository extends JpaRepository<EstadoCivil, Long>,
    ExtendedQuerydslPredicateExecutor<EstadoCivil> {

  
}
