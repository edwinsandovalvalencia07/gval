package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.VideoPildoraFormativa;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repositorio de consultas para Cuidador.
 */
@Repository
public interface VideoPildoraFormativaRepository
    extends JpaRepository<VideoPildoraFormativa, Long> {


  /**
   * Find all by activo true order by capitulo asc.
   *
   * @param orden the orden
   * @return the list
   */
  List<VideoPildoraFormativa> findAllByActivoTrue(Sort orden);

  /**
   * Find all by activo true and aplicacion order by capitulo asc.
   *
   * @param codigoApp the codigo app
   * @param orden the orden
   * @return the video pildora formativa
   */
  List<VideoPildoraFormativa> findAllByActivoTrueAndAplicacion(String codigoApp,
      Sort orden);

}
