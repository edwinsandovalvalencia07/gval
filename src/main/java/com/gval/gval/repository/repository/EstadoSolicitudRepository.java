package com.gval.gval.repository.repository;

import com.gval.gval.dto.dto.HistoricoEstadosDTO;
import com.gval.gval.entity.model.EstadoSolicitud;
import com.gval.gval.entity.model.Solicitud;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The Interface EstadoSolicitudRepository.
 */
@Repository
public interface EstadoSolicitudRepository
    extends JpaRepository<EstadoSolicitud, Long>,
    ExtendedQuerydslPredicateExecutor<EstadoSolicitud> {


  /**
   * Recupera un estado menor al proceso que se le pasa.
   *
   * @param pkSolicitud pk de la solicitud a comprobar
   * @param value the value
   * @return devuelve el estado de la solicitud activo menor que el proceso que
   *         se le pasa
   */
  EstadoSolicitud findBySolicitudPkSolicitudEqualsAndEstadoProcesoLessThanAndActivoTrue(
      Long pkSolicitud, Long value);

  /**
   * Find by solicitud pk solicitud equals and activo true.
   *
   * @param pkSolicitud the pk solicitud
   * @return the estado solicitud
   */
  EstadoSolicitud findBySolicitudPkSolicitudEqualsAndActivoTrue(
      Long pkSolicitud);

  /**
   * Find historico estados from solicitud.
   *
   * @param solicitud the solicitud
   * @return the list
   */
  @Query("select new es.gva.dependencia.ada.dto.HistoricoEstadosDTO(est.nombre, se.descripcion, es.fechaDesde, es.fechaHasta) "
      + " from EstadoSolicitud es "
      + " inner join es.estado est on est.activo = 1 "
      + " left join es.subestado se on se.activo = 1 "
      + " where es.solicitud = :solicitud order by es.pkEstsol ASC ")
  List<HistoricoEstadosDTO> findHistoricoEstadosFromSolicitud(
      @Param("solicitud") Solicitud solicitud);
}
