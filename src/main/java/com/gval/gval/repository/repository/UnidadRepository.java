package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Unidad;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * The Interface UnidadRepository.
 */
@Repository
public interface UnidadRepository extends JpaRepository<Unidad, Long>,
    ExtendedQuerydslPredicateExecutor<Unidad> {

  
  /**
   * Find by poblacion.
   *
   * @param codigoProvincia the codigo provincia
   * @param codigoMunicipio the codigo municipio
   * @param codigoPoblacion the codigo poblacion
   * @return the optional
   */
  @Query(value = "SELECT uni.* FROM sdx_poblac po "
      + "    INNER JOIN sdx_municipi mu ON po.pk_municipi = mu.pk_municipi AND mu.muactivo = 1 "
      + "    INNER JOIN sdx_unidad uni ON uni.pk_zonacobe = mu.pk_zonacobe AND uni.unactivo = 1 "
      + "  WHERE po.poactivo = 1 AND po.cod_prov = :codigoProvincia "
      + "    AND po.cod_muni = :codigoMunicipio "
      + "    AND po.cod_poblac = :codigoPoblacion", nativeQuery = true)
  Optional<Unidad> findByPoblacion(
      @Param("codigoProvincia") String codigoProvincia,
      @Param("codigoMunicipio") String codigoMunicipio,
      @Param("codigoPoblacion") String codigoPoblacion);

}
