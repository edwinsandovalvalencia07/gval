package com.gval.gval.repository.repository;



import com.gval.gval.entity.model.AsignarSolicitud;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;


/**
 * The Interface AsignarSolicitudRepository.
 */
@Repository
public interface AsignarSolicitudRepository
    extends JpaRepository<AsignarSolicitud, Long>,
        ExtendedQuerydslPredicateExecutor<AsignarSolicitud> {

  /**
   * Obtener citacion asignada por solicitud Y usuario.
   *
   * @param pkSolicit the pk solicit
   * @param pkPersona the pk persona
   * @return the list
   */
  @Query(value = " select asig" + " from AsignarSolicitud asig"
      + " left join asig.valoracion val"
      + " where asig.activo = 1 and asig.fechaCita is not null"
      + " and val is null and asig.usuario.pkPersona = :pkPersona"
      + " AND asig.solicitud.pkSolicitud = :pkSolicit")
  List<AsignarSolicitud> obtenerCitacionesAsignadasSinValoracion(
      @Param("pkSolicit") Long pkSolicit, @Param("pkPersona") Long pkPersona);

  /**
   * Obtener citacion asignada por expediente Y usuario.
   *
   * @param pkPersona the pk persona
   * @param pkExpedien the pk expedien
   * @return the list
   */
  @Query(value = " SELECT asig.*" + " FROM sdm_asigsoli asig"
      + " INNER JOIN sdm_solicit so ON so.pk_solicit = asig.pk_solicit"
      + " INNER JOIN sdm_valoraci va ON va.pk_asigsoli = asig.pk_asigsoli"
      + " WHERE asig.asactivo = 1 AND asig.asfechcita IS NOT NULL"
      + " AND va.pk_valoraci IS NULL AND pk_persona2 = :pkPersona"
      + " AND so.pk_solicit = :pkExpedien ", nativeQuery = true)
  List<AsignarSolicitud> obtenerCitacionAsignadaPorExpedienteYUsuario(
      @Param("pkPersona") Long pkPersona, @Param("pkExpedien") Long pkExpedien);


  /**
   * Find by solicitud pk solicitud and fecha aplazamiento date after.
   *
   * @param pkSolicit the pk solicit
   * @param fecha the fecha
   * @return the optional
   */
  Optional<AsignarSolicitud> findBySolicitudPkSolicitudAndFechaAplazamientoAfter(
      Long pkSolicit, Date fecha);


  /**
   * Find by solicitud pk solicitud.
   *
   * @param pKSolicit the k solicit
   * @return the asignar solicitud
   */
  List<AsignarSolicitud> findBySolicitudPkSolicitud(Long pKSolicit);

}
