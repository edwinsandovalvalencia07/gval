package com.gval.gval.repository.repository;

import com.gval.gval.entity.vistas.model.DisponiblesListado;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * The Interface DocDisponiblesListadoRepository.
 */
@Repository

public interface DocumentosDisponiblesListadoRepository extends JpaRepository<DisponiblesListado, Long> {


	/**
	 * Obtener doc disponibles.
	 *
	 * @param resoluciones the resoluciones
	 * @return the list
	 */

	@Query(value = " SELECT distinct pk_tipodocu,CONCAT(TINOMBRE, concat(concat(' (',count(TINOMBRE)), ' documentos).')) as TINOMBRE,"
			+ "          LISTAGG(pk_resoluci, ',' ) WITHIN GROUP (ORDER BY pk_resoluci) AS PK_Resoluci from ("
			+ "              select a.pk_tipodocu, a.PK_RESOLUCI, a.TINOMBRE from SDV_LD_DOC_DISPONIBLES_S a where  a.pk_resoluci in :resoluciones "
			+ "            union all"
			+ "              SELECT b.pk_tipodocu, b.PK_RESOLUCI, b.TINOMBRE from SDV_LD_DOC_DISPONIBLES2_S b where  b.pk_resoluci in  :resoluciones "
			+ "              union all"
			+ "                 SELECT c.pk_tipodocu, c.PK_RESOLUCI, c.TINOMBRE from SDV_LD_DOC_DISPONIBLES3_S c where  c.pk_resoluci in :resoluciones "
			+ "             union all"
			+ "              SELECT d.pk_tipodocu, d.PK_RESOLUCI, d.TINOMBRE from SDV_LD_DOC_DISPONIBLES4_S d where  d.pk_resoluci in :resoluciones "
			+ "              union all"
			+ "            SELECT e.pk_tipodocu, e.PK_RESOLUCI, e.TINOMBRE from SDV_LD_DOC_DISPONIBLES5_S e where e.pk_resoluci in :resoluciones) group by PK_TIPODOCU,TINOMBRE",
        nativeQuery = true)

		List<DisponiblesListado> obtenerDocDisponibles(@Param("resoluciones") Set<Integer> resoluciones);



}
