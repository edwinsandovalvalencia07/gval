package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Documento;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

/**
 * Repositorio de consultas de Documento.
 *
 * @author Indra
 */
@Repository
public interface DocumentoRepository extends JpaRepository<Documento, Long>,
    ExtendedQuerydslPredicateExecutor<Documento>, DocumentoRepositoryCustom {

  /**
   * Obtener documentos por solicitud en estado.
   *
   * @param pkSolicitud the pk solicitud
   * @param codigoEstadoDocumento the codigo estado documento
   * @return the list
   */
  @Query(" select d " + " from Documento d , HistoricoEstadoDocumento as hed "
      + " inner join d.solicitud as s "
      + " inner join hed.estadoDocumento as ed "
      + " where hed.documento = d.pkDocumento and hed.activo = true and d.activo = true "
      + " and ed.codigo = :codigoEstadoDocumento "
      + " and s.pkSolicitud = :pkSolicitud ")
  List<Documento> obtenerDocumentosPorSolicitudEnEstado(
      @Param("pkSolicitud") Long pkSolicitud,
      @Param("codigoEstadoDocumento") String codigoEstadoDocumento);

  /**
   * Obtener documentos no validados por solicitud.
   *
   * @param pkSolicitud the pk solicitud
   * @return the list
   */
  @Query(value = " SELECT d.* " + " from SDM_ESTDOCU ed "
      + " INNER JOIN SDM_DOCU d ON ed.PK_DOCU = d.PK_DOCU AND ed.ESACTIVO = 1 AND d.DOCACTIVO = 1 "
      + " INNER JOIN SDX_ESTADO_DOCU xed ON ed.PK_ESTADO_DOCU = xed.PK_ESTADO_DOCU AND xed.ESACTIVO = 1 "
      + " WHERE xed.ESCODIGO = 'NVA' AND d.PK_SOLICIT = :pkSolicitud "
      + " AND ed.PK_DOCU NOT IN " + " (SELECT i.PK_DOCU FROM SDM_INCIDENC i "
      + " WHERE i.PK_DOCU = ed.PK_DOCU AND i.INESTADO != 'C' AND i.INACTIVO = 1) "
      + " AND ed.PK_DOCU NOT IN (SELECT s.PK_DOCU2 FROM SDM_SUBSANAC S "
      + " WHERE PK_DOCU2 is not null and s.suactivo= 1) ", nativeQuery = true)
  List<Documento> obtenerDocumentosNoValidadosPorSubsanacion(
      @Param("pkSolicitud") Long pkSolicitud);


  /**
   * Find ultimo documento solicitud by nombre documento anterior fecha acuse.
   *
   * @param pkSolicit the pk solicit
   * @param codigo the codigo *
   * @param fechaAcuse the fecha acuse
   * @return the documento generado
   */
  @Query(value = "SELECT * FROM (select d.* from sdm_docu d "
      + "inner join sdm_solicit s on s.pk_solicit = d.pk_solicit "
      + "inner join sdx_tipodocu td on td.pk_tipodocu = d.pk_tipodocu "
      + "where s.pk_solicit = :pkSolicit and td.ticodigo = :codigo and d.dofechregi <= :fechaAcuse order by d.dofechregi desc) where rownum = 1",
      nativeQuery = true)
  Optional<Documento> findUltimoDocumentoSolicitudByNombreDocumentoAnteriorFechaAcuse(
      @Param("pkSolicit") Long pkSolicit, @Param("codigo") String codigo,
      @Param("fechaAcuse") Date fechaAcuse);

  /**
   * Find by solicitud pk solicitud and activo true and tipo documento codigo.
   *
   * @param pkSolicitud the pk solicitud
   * @param codigo the codigo
   * @return the list
   */
  List<Documento> findBySolicitudPkSolicitudAndActivoTrueAndTipoDocumentoCodigo(
      Long pkSolicitud, String codigo);

}
