package com.gval.gval.repository.repository;

//import com.gval.gval.entity.vistas.model.CapituloDiagnosticoCie10;

import com.gval.gval.entity.vistas.model.CapituloDiagnosticoCie10;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The Interface DiagnosticoCIE10Repository.
 */
@Repository
public interface CapituloDiagnosticoCie10Repository
    extends JpaRepository<CapituloDiagnosticoCie10, Long> {

  /**
   * Obtener diagnosticos.
   *
   * @return the list
   */
  @Query(value = " SELECT * FROM SDV_LD_CAPITULOS ", nativeQuery = true)
  List<CapituloDiagnosticoCie10> obtenerCapitulosDiagnosticos();

}
