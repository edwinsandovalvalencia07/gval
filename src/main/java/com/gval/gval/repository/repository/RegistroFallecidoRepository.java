package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.RegistroFallecido;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * The Interface RegistroFallecidoRepository.
 */
@Repository
public interface RegistroFallecidoRepository
    extends JpaRepository<RegistroFallecido, Long>,
    ExtendedQuerydslPredicateExecutor<RegistroFallecido> {

}
