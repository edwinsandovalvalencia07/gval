package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Persona;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;


/**
 * The Interface PersonaRepository.
 */
@Repository
public interface PersonaRepository extends JpaRepository<Persona, Long>,
    ExtendedQuerydslPredicateExecutor<Persona> {

  /**
   * Find by pk persona.
   *
   * @param pkPersona the pk persona
   * @return the optional
   */
  Optional<Persona> findByPkPersona(Long pkPersona);

  /**
   * Find by id api and ind dup api and nif.
   *
   * @param idApi the id api
   * @param indDupApi the ind dup api
   * @param nif the nif
   * @return the optional
   */
  Optional<Persona> findByIdApiAndIndDupApiAndNif(Long idApi, Long indDupApi,
      String nif);

  /**
   * Find by nif.
   *
   * @param nif the nif
   * @return the optional
   */
  Optional<Persona> findByNif(String nif);

  /**
   * Find all by nif and activo true.
   *
   * @param nif the nif
   * @return the list
   */
  List<Persona> findAllByNifAndActivoTrue(String nif);


  /**
   * Lanza procedimiento de cruce de datos con Registro Civil, Sanidad, INE y
   * SIINTEGRA
   *
   * @param pkExpediente the pk expediente
   * @return the date
   */
  @Query(value = "SELECT CRUCE_EXITUS_SOLICITANTE(:pkExpediente) FROM DUAL",
      nativeQuery = true)
  Date cruceExitusSolicitante(@Param("pkExpediente") Long pkExpediente);


  /**
   * Consulta si la persona asociada al expediente tiene datos en SDM_EXITUS
   *
   * @param pkExpediente the pk expediente
   * @return the long
   */
  @Query(
      value = "SELECT COUNT(*) FROM SDM_EXPEDIEN exp INNER JOIN SDM_PERSONA per "
          + " ON exp.PK_PERSONA = per.PK_PERSONA "
          + " INNER JOIN SDM_EXITUS exi "
          + " ON per.PENUDOCIDE = exi.IDENTIFICACION "
          + " WHERE exp.PK_EXPEDIEN = :pkExpediente",
      nativeQuery = true)
  Long tieneCruceExitus(@Param("pkExpediente") Long pkExpediente);
}
