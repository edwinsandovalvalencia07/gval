package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Subestado;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The Interface SubestadoRepository.
 */
@Repository
public interface SubestadoRepository extends JpaRepository<Subestado, Long>,
    ExtendedQuerydslPredicateExecutor<Subestado> {

  /**
   * Find by codigo.
   *
   * @param codigo the codigo
   * @return the estado DTO
   */
  Subestado findByCodigoAndActivoTrue(String codigo);

  /**
   * Find by codigo.
   *
   * @param codigo the codigo
   * @return the estado DTO
   */
  Subestado findByPkSubestadoAndActivoTrue(Long pkSubestado);

}
