package com.gval.gval.repository.repository;

import com.gval.gval.dto.dto.RecursosDTO;
import com.gval.gval.entity.model.Recursos;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Optional;



// TODO: Auto-generated Javadoc
/**
 * The Interface RecursoRepository.
 */
@Repository
public interface RecursosRepository extends JpaRepository<Recursos, Long>,
    ExtendedQuerydslPredicateExecutor<Recursos> {


  /**
   * Find by pk recurso and activo true.
   *
   * @param pkRecursos the pk recursos
   * @return the optional
   */
  Optional<Recursos> findByPkRecursosAndActivoTrue(Long pkRecursos);

  /**
   * Obtener recursos grado Y nivel.
   *
   * @param pkSolicit the pk solicit
   * @return the int
   */
  @Query(value = "SELECT COUNT(*) FROM sdm_recursos recu "
		  + "INNER JOIN sdm_resoluci reso ON reso.pk_resoluci = recu.pk_resoluci "
		  + "INNER JOIN sdm_solicit soli ON soli.pk_solicit = reso.pk_solicit "
		  + "WHERE reso.retipo IN ('GYN', 'GNT', 'DNT') "
		  + "AND recu.reactivo = 1 AND reso.pk_solicit = :pkSolicit "
		  + "AND recu.pk_estadorecurso = 2 ",
	  nativeQuery = true)
  int obtenerRecursosGradoYNivel(@Param("pkSolicit") Long pkSolicit);


  /**
   * Obtener unico recurso por solicitud.
   *
   * @param pkEstudio the pk estudio
   * @return the recursos
   */
  @Query(
      value = "select re.* from sdm_recursos re "
          + "inner join sdm_estudio es on es.pk_estudio = re.pk_estudio "
          + "where es.pk_estudio = :pkEstudio AND ROWNUM = 1 ",
      nativeQuery = true)
  Recursos obtenerUnicoRecursoPorSolicitud(
      @Param("pkEstudio") Long pkEstudio);

  /**
   * Obtener fecha registro recurso.
   *
   * @param pkRecurso the pk recurso
   * @return the date
   */
  @Query(value = "SELECT DOFECHREGI FROM SDM_RECURSOS RE, SDM_DOCU D WHERE  RE.PK_DOCU= D.PK_DOCU AND RE.PK_RECURSOS= :pkRecurso", nativeQuery = true)
  Date obtenerFechaRegistroRecurso(@Param("pkRecurso") String pkRecurso);
  
  @Query(value = "SELECT * FROM ("
      + "SELECT sr.* "
      + "FROM sdm_recursos sr "
      + "JOIN sdm_resoluci r ON sr.pk_resoluci = r.pk_resoluci "
      + "WHERE r.pk_resoluci = :pkResoluci "
      + "ORDER BY sr.pk_recursos DESC "
      + ")"
      + "WHERE ROWNUM = 1 ", nativeQuery = true)
  Recursos obtenerRecursoDeResolucion(@Param("pkResoluci") Long pkResoluci);

  /**
   * Obtener recurso por resolucion.
   *
   * @param pkResoluci the pk resoluci
   * @return the recursos DTO
   */
  @Query(value = "SELECT * FROM ( SELECT r.* FROM SDM_RECURSOS r WHERE r.pk_resoluci = :pkResoluci)  WHERE rownum = 1", nativeQuery = true)
  RecursosDTO obtenerRecursoPorResolucion(@Param("pkResoluci") Long pkResoluci);

}
