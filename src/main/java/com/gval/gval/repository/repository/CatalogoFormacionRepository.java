package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.CatalogoFormacion;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;



/**
 * The Interface MotivoFinalizacionSolicitudRepository.
 */
@Repository
public interface CatalogoFormacionRepository extends JpaRepository<CatalogoFormacion, Long>,
ExtendedQuerydslPredicateExecutor<CatalogoFormacion> {

  /**
   * Find by codigo.
   *
   * @param codigo the codigo
   * @return the optional
   */
  Optional<CatalogoFormacion> findByCodigo(String codigo);

  /**
   * Find lista activos.
   *
   * @return the list
   */
  @Override
  List<CatalogoFormacion> findAll();

  @Query("SELECT cat FROM CatalogoFormacion cat WHERE cat.especifico = 0 ")
  List<CatalogoFormacion> findCatFormacionNoEspecifico();

}
