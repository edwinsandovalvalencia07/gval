package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Dictamen;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Interface DictamenRepository.
 */
@Repository
public interface DictamenRepository extends JpaRepository<Dictamen, Long>,
    ExtendedQuerydslPredicateExecutor<Dictamen> {

  /**
   * Find ultimo dictamen by solicitud.
   *
   * @param pkSolicitud the pk solicitud
   * @return the dictamen
   */
  @Query(value = "SELECT * from sdm_dictamen di "
      + "INNER JOIN sdm_estudio es on es.pk_dictamen = di.pk_dictamen "
      + "WHERE es.pk_solicit = :pkSolicitud AND es.esactivo = 1 "
      + "ORDER BY es.esfechcrea DESC, es.pk_estudio DESC", nativeQuery = true)
  List<Dictamen> findUltimoDictamenBySolicitud(
      @Param("pkSolicitud") final Long pkSolicitud);

	/**
	 * Existe informe social.
	 *
	 * @param pkSolicit the pk solicit
	 * @return the int
	 */
	@Query(value = "select count(*) from SDM_INFOSOCI where PK_SOLICIT = :pkSolicit", nativeQuery = true)
	int existeInformeSocialEnSolicitud(@Param("pkSolicit") Long pkSolicit);

	/**
	 * Existe docu tipo infentorno.
	 *
	 * @param pkSolicit the pk solicit
	 * @return the int
	 */
    @Query(value = "SELECT COUNT(*) " + "FROM SDM_SOLICIT "
        + "INNER JOIN SDM_DOCU "
        + "ON SDM_SOLICIT.PK_SOLICIT = SDM_DOCU.PK_SOLICIT "
        + "INNER JOIN SDX_TIPODOCU "
        + "ON SDX_TIPODOCU.PK_TIPODOCU  = SDM_DOCU.PK_TIPODOCU "
        + "WHERE SDM_SOLICIT.PK_SOLICIT = :pkSolicit "
        + "AND SDX_TIPODOCU.TICODIGO = 'INFENTORNO'", nativeQuery = true)
	int existeDocuTipoInfEntorno(@Param("pkSolicit") Long pkSolicit);


    /**
     * Obtener ultimo dictamen expediente.
     *
     * @param pkExpedien the pk expedien
     * @return the dictamen
     */
    @Query(value = "  SELECT * FROM "
        + "    (SELECT sd.* "
        + "    FROM Sdm_Dictamen sd JOIN sdm_Estudio e ON sd.pk_dictamen = e.pk_dictamen "
        + "    INNER JOIN sdm_Solicit s ON e.pk_solicit = s.pk_solicit  "
        + "    INNER JOIN sdm_Expedien ex ON s.pk_expedien = ex.pk_Expedien "
        + "    WHERE ex.pk_expedien = :pkExpedien "
        + "    AND sd.digrado IS NOT NULL AND sd.dinivel IS NOT NULL "
        + "    ORDER BY e.esfechcrea DESC "
        + "    )"
        + "  WHERE rownum <= 1", nativeQuery = true)
    Dictamen obtenerUltimoDictamenExpediente(@Param("pkExpedien") Long pkExpedien);
}
