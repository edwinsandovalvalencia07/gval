package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.ZonaCobertura;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * The Interface ZonaCoberturaRepository.
 */
@Repository
public interface ZonaCoberturaRepository
    extends JpaRepository<ZonaCobertura, Long>,
    ExtendedQuerydslPredicateExecutor<ZonaCobertura> {

}
