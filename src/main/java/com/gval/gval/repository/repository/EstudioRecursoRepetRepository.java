package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.EstudioRecursoRepet;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;



/**
 * The Interface RecursoRepository.
 */
@Repository
public interface EstudioRecursoRepetRepository
    extends JpaRepository<EstudioRecursoRepet, Long>,
    ExtendedQuerydslPredicateExecutor<EstudioRecursoRepet> {


  /**
   * Find by pk recurso and activo true.
   *
   * @param pkEstudio the pk estudio
   * @return the optional
   */
  Optional<EstudioRecursoRepet> findByPkEstudioAndActivoTrue(Long pkEstudio);

}
