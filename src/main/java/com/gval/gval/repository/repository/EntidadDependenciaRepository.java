package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.EntidadDependencia;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The Interface EntidadDependenciaRepository.
 */
@Repository
public interface EntidadDependenciaRepository
    extends JpaRepository<EntidadDependencia, Long>,
    ExtendedQuerydslPredicateExecutor<EntidadDependencia> {

  /**
   * Find by comunidad autonoma pk comauto.
   *
   * @param pkComAuto the pk com auto
   * @return the list
   */
  List<EntidadDependencia> findByComunidadAutonomaPkComauto(Long pkComAuto);

}
