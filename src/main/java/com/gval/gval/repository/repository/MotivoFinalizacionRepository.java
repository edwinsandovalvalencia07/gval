package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.MotivoFinalizacion;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * The Interface MotivoFinalizacionRepository.
 */
@Repository
public interface MotivoFinalizacionRepository
    extends JpaRepository<MotivoFinalizacion, Long>,
    ExtendedQuerydslPredicateExecutor<MotivoFinalizacion> {

  /**
   * Find by codigo.
   *
   * @param codigo the codigo
   * @return the optional
   */
  Optional<MotivoFinalizacion> findByCodigo(String codigo);
}
