package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.PropuestaPia;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The Interface PropuestaPiaRepository.
 */
@Repository
public interface PropuestaPiaRepository
    extends JpaRepository<PropuestaPia, Long>,
    ExtendedQuerydslPredicateExecutor<PropuestaPia> {


}