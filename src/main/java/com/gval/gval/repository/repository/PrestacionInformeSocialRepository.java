package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.InformeSocial;
import com.gval.gval.entity.model.PrestacionInformeSocial;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * The Interface PrestacionInformeSocialRepository.
 */
@Repository
public interface PrestacionInformeSocialRepository
    extends JpaRepository<PrestacionInformeSocial, Long>,
    ExtendedQuerydslPredicateExecutor<PrestacionInformeSocial> {

  /**
   * Find by informe social.
   *
   * @param informeSocial the informe social
   * @return the list
   */
  List<PrestacionInformeSocial> findByInformeSocial(
      InformeSocial informeSocial);

}
