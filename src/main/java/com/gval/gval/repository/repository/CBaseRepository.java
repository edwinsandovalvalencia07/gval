package com.gval.gval.repository.repository;

//import es.gva.dependencia.ada.dto.CBaseDTO;
import com.gval.gval.dto.dto.CBaseDTO;
import com.gval.gval.entity.model.CBase;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * The Interface CBaseRepository.
 */
@Repository
public interface CBaseRepository extends JpaRepository<CBase, Long>,
    ExtendedQuerydslPredicateExecutor<CBase> {

  /**
   * Find C base por identificador.
   *
   * @param identificador the identificador
   * @return the c base DTO
   */
  @Query("select new es.gva.dependencia.ada.dto.CBaseDTO(c.identificador, c.nombre, c.primerApellido, c.segundoApellido, c.fechaNacimiento, c.existePDF, c.puntosATP, c.gradoDiscapacidad, c.fechaJunta, c.fechaProximaRevision, c.resultadoBaremacion, c.fechaValoracion, c.provincia, c.estado) "
      + " from  CBase c " + " where c.identificador = :identificador")
  CBaseDTO findCBasePorIdentificador(
      @Param("identificador") String identificador);
}
