package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Persona;
import com.gval.gval.entity.model.Solicitud;
import com.gval.gval.entity.model.TipoFamiliar;
import com.gval.gval.entity.model.UnidadFamiliar;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The Interface UnidadFamiliarRepository.
 */
@Repository
public interface UnidadFamiliarRepository
    extends JpaRepository<UnidadFamiliar, Long>,
    ExtendedQuerydslPredicateExecutor<UnidadFamiliar> {

  /**
   * Find by solicitud and activo true.
   *
   * @param solicitud the solicitud
   * @return the list
   */
  List<UnidadFamiliar> findBySolicitudAndActivoTrue(Solicitud solicitud);


  /**
   * Find by solicitud and persona and tipo familiar and activo false.
   *
   * @param solicitud the solicitud
   * @param persona the persona
   * @param tipoFamiliar the tipo familiar
   * @return the unidad familiar
   */
  UnidadFamiliar findBySolicitudAndPersonaAndTipoFamiliarAndActivoFalse(
      Solicitud solicitud, Persona persona, TipoFamiliar tipoFamiliar);

}
