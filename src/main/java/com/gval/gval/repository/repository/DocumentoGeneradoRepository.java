package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.DocumentoGenerado;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The Interface DocumentoGeneradoRepository.
 */
@Repository
public interface DocumentoGeneradoRepository
    extends JpaRepository<DocumentoGenerado, Long>,
    ExtendedQuerydslPredicateExecutor<DocumentoGenerado> {


  /**
   * Find by documento solicitud activo true and documento solicitud expediente
   * pk expedien and documento activo.
   *
   * @param pkExpedien the pk expedien
   * @param activo the activo
   * @param orden the orden
   * @return the list
   */
  List<DocumentoGenerado> findByDocumentoSolicitudActivoTrueAndDocumentoSolicitudExpedientePkExpedienAndDocumentoActivo(
      Long pkExpedien, Boolean activo, Sort orden);

  /**
   * Find by documento solicitud pk solicitud.
   *
   * @param pkSolicitud the pk solicitud
   * @param activo the activo
   * @param orden the orden
   * @return the list
   */
  List<DocumentoGenerado> findByDocumentoSolicitudPkSolicitudAndDocumentoActivo(
      Long pkSolicitud, Boolean activo, Sort orden);

  /**
   * Find by pk docu.
   *
   * @param pkDocu the pk docu
   * @return the documento generado
   */
  DocumentoGenerado findByPkDocumento(Long pkDocu);

  /**
   * Obtener documentos acuses no generados.
   *
   * @return the list
   */
  @Query(value = "SELECT  dg" + "  FROM  DocumentoGenerado dg"
      + " INNER JOIN dg.documento d" + " INNER JOIN d.tipoDocumento td"
      + " WHERE  d.activo = 1 AND d.fichero IS NULL AND d.fechaRegistro IS NOT NULL AND td.tipo = 'G' AND td.identificadorSforms IS NOT NULL"
      + " AND  d.claveConsulta IS NOT NULL")
  List<DocumentoGenerado> obtenerDocumentosAcusesNoGenerados();

  /**
   * Obtener documentos Informe Calculo No generados.
   *
   * @param codigosTipoDocu the codigos tipo docu
   * @return the list
   */
  @Query(value = "SELECT  dg" + "  FROM  DocumentoGenerado dg"
      + " INNER JOIN dg.documento d" + " INNER JOIN d.tipoDocumento td"
      + " WHERE  d.activo = 1 AND d.fichero IS NULL AND td.tipo = 'G' AND td.identificadorSforms IS NOT NULL"
      + " AND  d.uuid IS NULL AND ROWNUM <= 5000 AND td.codigo IN (:p_tiCodigos)")
  List<DocumentoGenerado> obtenerDocumentosSinAcusesNoGenerados(
      @Param("p_tiCodigos") List<String> codigosTipoDocu);
}
