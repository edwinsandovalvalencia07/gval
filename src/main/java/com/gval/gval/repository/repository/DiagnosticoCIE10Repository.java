package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.DiagnosticoCie10;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The Interface DiagnosticoCIE10Repository.
 */
@Repository
public interface DiagnosticoCIE10Repository extends JpaRepository<DiagnosticoCie10, Long> {

}
