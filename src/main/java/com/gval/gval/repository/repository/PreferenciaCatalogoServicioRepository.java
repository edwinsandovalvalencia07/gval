package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.PreferenciaCatalogoServicio;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * The Interface PreferenciaCatalogoServicioRepository.
 */
public interface PreferenciaCatalogoServicioRepository
    extends JpaRepository<PreferenciaCatalogoServicio, Long>,
    ExtendedQuerydslPredicateExecutor<PreferenciaCatalogoServicio> {

  /**
   * Find by preferencia pk prefsol.
   *
   * @param pkPrefsol the pk prefsol
   * @return the list
   */
  List<PreferenciaCatalogoServicio> findByPreferenciaPkPrefsol(Long pkPrefsol);

}
