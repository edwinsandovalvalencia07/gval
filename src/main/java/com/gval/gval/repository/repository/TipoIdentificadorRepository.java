package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.TipoIdentificador;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * The Interface TipoIdentificadorRepository.
 */
@Repository
public interface TipoIdentificadorRepository
    extends JpaRepository<TipoIdentificador, Long>,
    ExtendedQuerydslPredicateExecutor<TipoIdentificador> {

  /**
   * Find by codigo imserso.
   *
   * @param codigoImserso the codigo imserso
   * @return the optional
   */
  Optional<TipoIdentificador> findByCodigoImserso(String codigoImserso);

  /**
   * Find by nombre.
   *
   * @param nombre the nombre
   * @return the optional
   */
  Optional<TipoIdentificador> findByNombre(String nombre);

}
