package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.VariableEconomica;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The Interface VariableEconomicaRepository.
 */
@Repository
public interface VariableEconomicaRepository
    extends JpaRepository<VariableEconomica, Long>,
    ExtendedQuerydslPredicateExecutor<VariableEconomica> {

  /**
   * Find by codigo.
   *
   * @param codigo the codigo
   * @return the list
   */
  public VariableEconomica findByCodigoAndEjercicioCerradoFalse(String codigo);

}
