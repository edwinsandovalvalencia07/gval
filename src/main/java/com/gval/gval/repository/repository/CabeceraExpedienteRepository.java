package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.CabeceraExpediente;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * The Interface ExpedienteRepository.
 */
@Repository
public interface CabeceraExpedienteRepository
    extends JpaRepository<CabeceraExpediente, Long>,
    ExtendedQuerydslPredicateExecutor<CabeceraExpediente> {


}
