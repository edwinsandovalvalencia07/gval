package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.CodigoPostal;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * The Interface CodigoPostalRepository.
 */
@Repository
public interface CodigoPostalRepository
    extends JpaRepository<CodigoPostal, Long>,
    ExtendedQuerydslPredicateExecutor<CodigoPostal> {

  /**
   * Find all.
   *
   * @return the list
   */
  @Override
  List<CodigoPostal> findAll();

  /**
   * Find by codigo postal.
   *
   * @param codigoPostal the codigo postal
   * @return the optional
   */
  Optional<CodigoPostal> findByCodigoPostal(String codigoPostal);

}
