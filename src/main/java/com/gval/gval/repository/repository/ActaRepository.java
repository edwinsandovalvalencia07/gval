package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Acta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * The Interface ActaRepository.
 */
@Repository
public interface ActaRepository extends JpaRepository<Acta, Long> {


}
