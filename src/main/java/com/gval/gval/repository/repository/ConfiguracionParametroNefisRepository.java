package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.ConfiguracionParametroNefis;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The Interface ConfiguracionParametroNefisRepository.
 */
@Repository
public interface ConfiguracionParametroNefisRepository
    extends JpaRepository<ConfiguracionParametroNefis, Long> {

  /**
   * Find by id param.
   *
   * @param id the id
   * @return the configuracion parametro nefis
   */
  ConfiguracionParametroNefis findByIdParam(Long id);

}
