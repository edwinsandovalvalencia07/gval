package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Comarca;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * The Interface ComarcaRepository.
 */
@Repository
public interface ComarcaRepository extends JpaRepository<Comarca, Long>,
    ExtendedQuerydslPredicateExecutor<Comarca> {

}
