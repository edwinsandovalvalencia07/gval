package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Resolucion;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;
import com.gval.gval.entity.vistas.model.ResolucionListado;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The Interface ResolucionRepository.
 */
@Repository
public interface ResolucionListadoRepository
    extends JpaRepository<ResolucionListado, Long>,
    ExtendedQuerydslPredicateExecutor<ResolucionListado>,
    ResolucionListadoRepositoryCustom {


  /**
   * Find by pk expedien.
   *
   * @param pkExpedien the pk expedien
   * @param orden the orden
   * @return the list
   */
  List<ResolucionListado> findByPkExpedien(Long pkExpedien, Sort orden);


  /**
   * Find by pk solicitud.
   *
   * @param pkSolicitud the pk solicitud
   * @param orden the orden
   * @return the list
   */
  List<ResolucionListado> findByPkSolicitud(Long pkSolicitud, Sort orden);


  /**
   * Save.
   *
   * @param entity the entity
   * @return the resolucion
   */
  Resolucion save(Resolucion entity);
}
