package com.gval.gval.repository.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gval.gval.entity.model.Nacionalidad;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

@Repository
public interface NacionalidadRepository
    extends JpaRepository<Nacionalidad, Long>,
    ExtendedQuerydslPredicateExecutor<Nacionalidad> {

  Nacionalidad findByPkNacionalidad(Long pkNacionalidad);

}
