package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.MotivoFinalizacion;
import com.gval.gval.entity.model.MotivoFinalizacionSolicitud;
import com.gval.gval.entity.model.Solicitud;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * The Interface MotivoFinalizacionSolicitudRepository.
 */
@Repository
public interface MotivoFinalizacionSolicitudRepository
    extends JpaRepository<MotivoFinalizacionSolicitud, Long>,
    ExtendedQuerydslPredicateExecutor<MotivoFinalizacionSolicitud> {

  /**
   * Find by solicitud and motivo finalizacion and activo true.
   *
   * @param solicitud the solicitud
   * @param motivoFinalizacion the motivo finalizacion
   * @return the list
   */
  List<MotivoFinalizacionSolicitud> findBySolicitudAndMotivoFinalizacionAndActivoTrue(
      Solicitud solicitud, MotivoFinalizacion motivoFinalizacion);

}
