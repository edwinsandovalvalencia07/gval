package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.TipoVia;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * The Interface TipoViaRepository.
 */
@Repository
public interface TipoViaRepository extends JpaRepository<TipoVia, Long>,
    ExtendedQuerydslPredicateExecutor<TipoVia> {

  /**
   * Find by pk tipo via and activo true.
   *
   * @param pkTipoVia the pk tipo via
   * @return the optional
   */
  Optional<TipoVia> findByPkTipoViaAndActivoTrue(Long pkTipoVia);

  /**
   * Find by nombre corto and activo true.
   *
   * @param nombreCorto the nombre corto
   * @return the optional
   */
  Optional<TipoVia> findByNombreCortoAndActivoTrue(String nombreCorto);

  /**
   * Find by imserso and activo true.
   *
   * @param codigoImserso the codigo imserso
   * @return the optional
   */
  Optional<TipoVia> findByImsersoAndActivoTrue(String codigoImserso);

  /**
   * Find all.
   *
   * @return the list
   */
  @Override
  List<TipoVia> findAll();

}
