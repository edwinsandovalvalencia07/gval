package com.gval.gval.repository.repository.impl;

import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.repository.repository.custom.DocumentoRepositoryCustom;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;

import java.util.Date;
import java.util.List;
import java.util.Optional;



/**
 * The Class DocumentoRepositoryImpl.
 */
public class DocumentoRepositoryImpl implements DocumentoRepositoryCustom {

  /** The entity manager. */
  @PersistenceContext
  EntityManager entityManager;


  /**
   * Obtener documentos sincronizacion.
   *
   * @param fechaDesde the fecha desde
   * @param numeroDocumentos the numero documentos
   * @return the optional
   */
  @Override
  public Optional<List<Long>> obtenerDocumentosSincronizacion(
      final Date fechaDesde, final int numeroDocumentos) {
    if (fechaDesde == null || numeroDocumentos == 0) {
      return Optional.empty();
    }
    final Query query = entityManager.createQuery(" select d.pkDocumento "
        + " from Documento d " + " where (d.uuid is null or d.fichero is null) "
        + " and not (d.uuid is null and d.fichero is null) "
        + " and d.fechaCreacion >= :fechaDesde "
        + " and d.errorSincronizacion = false "
        + " and d.activo = true order by d.fechaCreacion", Long.class);
    query.setParameter("fechaDesde", fechaDesde);
    final List<Long> listaPkDocumento = UtilidadesCommons.castList(Long.class,
        query.setMaxResults(numeroDocumentos).getResultList());
    return Optional.ofNullable(listaPkDocumento);
  }
}
