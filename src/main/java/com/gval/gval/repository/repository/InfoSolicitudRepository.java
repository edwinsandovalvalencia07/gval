package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.InfoSolicitud;
import com.gval.gval.entity.model.Solicitud;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * The Interface InfoSolicitudRepository.
 */
@Repository
public interface InfoSolicitudRepository
    extends JpaRepository<InfoSolicitud, Long>,
    ExtendedQuerydslPredicateExecutor<InfoSolicitud> {

  /**
   * Find by solicitud.
   *
   * @param solicitud the solicitud
   * @return the optional
   */
  Optional<InfoSolicitud> findBySolicitud(Solicitud solicitud);

}
