package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Estado;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The Interface EstadoRepository.
 */
@Repository
public interface EstadoRepository extends JpaRepository<Estado, Long>,
    ExtendedQuerydslPredicateExecutor<Estado> {

  /**
   * Find by codigo.
   *
   * @param codigo the codigo
   * @return the estado DTO
   */
  Estado findByCodigoAndActivoTrue(String codigo);

  /**
   * Find by codigo.
   *
   * @param codigo the codigo
   * @return the estado DTO
   */
  Estado findByPkEstadoAndActivoTrue(Long pkEstado);

}
