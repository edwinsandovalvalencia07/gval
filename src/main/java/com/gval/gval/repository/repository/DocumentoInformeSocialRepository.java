package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Documento;
import com.gval.gval.entity.model.DocumentoInformeSocial;
import com.gval.gval.entity.model.InformeSocial;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * The Interface DocumentoInformeSocialRepository.
 */
@Repository
public interface DocumentoInformeSocialRepository
    extends JpaRepository<DocumentoInformeSocial, Long>,
    ExtendedQuerydslPredicateExecutor<DocumentoInformeSocial> {


  /**
   * Find by documento and informe social activo true.
   *
   * @param documento the documento
   * @return the list
   */
  List<DocumentoInformeSocial> findByDocumentoAndInformeSocialActivoTrue(
      Documento documento);

  /**
   * Find by informe social.
   *
   * @param informeSocial the informe social
   * @return the list
   */
  List<DocumentoInformeSocial> findByInformeSocial(InformeSocial informeSocial);


  /**
   * Buscar documento informe sin cambio tipologia.
   *
   * @param pkdocu the pkdocu
   * @return the optional
   */
  @Query(value = "select diso.* from sdm_docu do "
      + " inner join sdm_docu_infosoci diso on do.pk_docu = diso.pk_docu "
      + " inner join sdm_infosoci iso on diso.pk_infosoci = iso.pk_infosoci and iso.isactivo = 0 and trunc(iso.infechcrea) = trunc(do.dofechcrea) "
      + " where do.pk_docu =:pkdocu", nativeQuery = true)
  Optional<DocumentoInformeSocial> buscarDocumentoInformeSinCambioTipologia(
      @Param("pkdocu") Long pkdocu);

}
