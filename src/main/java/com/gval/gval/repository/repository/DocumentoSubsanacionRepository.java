package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.DocumentoSubsanacion;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * The Interface DocumentoSubsanacionRepository.
 */
@Repository
public interface DocumentoSubsanacionRepository
    extends JpaRepository<DocumentoSubsanacion, Long>,
    ExtendedQuerydslPredicateExecutor<DocumentoSubsanacion> {

  /**
   * Find by subsanacion pk subsanacion.
   *
   * @param pkSubsanacion the pk subsanacion
   * @return the list
   */
  List<DocumentoSubsanacion> findBySubsanacionPkSubsanacion(Long pkSubsanacion);

  /**
   * Find by subsanacion pk subsanacion and persona pk persona.
   *
   * @param pkSubsanacion the pk subsanacion
   * @param pkPersona the pk persona
   * @return the optional
   */
  Optional<List<DocumentoSubsanacion>> findBySubsanacionPkSubsanacionAndPersonaPkPersona(
      Long pkSubsanacion, Long pkPersona);

  /**
   * Find by expediente.
   *
   * @param pkExpedien the pk expedien
   * @return the list
   */
  @Query(value = "select dsub.* from sdm_docusubs dsub"
      + " inner join sdm_subsanac sub on dsub.pk_subsanac = sub.pk_subsanac and sub.suactivo = 1"
      + " inner join sdm_solicit s on sub.pk_solicit = s.pk_solicit"
      + " inner join sdx_tipodocu td on td.pk_tipodocu = dsub.pk_tipodocu and td.tiautorizaacceso = 1"
      + " where dsub.dosactivo = 1 and s.pk_expedien = :pkExpedien",
      nativeQuery = true)
  List<DocumentoSubsanacion> findDocumentosSubsanacionAutorizacionByExpediente(
      @Param("pkExpedien") Long pkExpedien);
}
