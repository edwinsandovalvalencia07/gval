package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.TipoFamiliar;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * The Interface TipoFamiliarRepository.
 */
@Repository
public interface TipoFamiliarRepository
    extends JpaRepository<TipoFamiliar, Long>,
    ExtendedQuerydslPredicateExecutor<TipoFamiliar> {

  /**
   * Find by pk tipofami and activo true.
   *
   * @param activo the activo
   * @return the optional
   */
  Optional<TipoFamiliar> findByPkTipofamiAndActivoTrue(Long activo);

  /**
   * Find all.
   *
   * @return the list
   */
  @Override
  List<TipoFamiliar> findAll();

  /**
   * Find byrelacion familiar and activo true.
   *
   * @param relacionFamiliar the relacion familiar
   * @return the optional
   */
  List<TipoFamiliar> findByRelacionFamiliarAndActivoTrue(Long relacionFamiliar);

  /**
   * Find by activo true.
   *
   * @return the list
   */
  List<TipoFamiliar> findByActivoTrue();
}
