package com.gval.gval.repository.repository.impl;


import com.gval.gval.repository.repository.custom.NefisRepositoryCustom;
import jakarta.persistence.*;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The Class NefisRepositoryImpl.
 */
@Repository
public class NefisRepositoryImpl implements NefisRepositoryCustom {

  /** The Constant GENERAR_DOCUMENTO_CONTABLE. */
  private static final String GENERAR_DOCUMENTO_CONTABLE =
      "GENERAR_DOCUMENTO_CONTABLE";

  /** The Constant ANULAR_PETICION_NEFIS. */
  private static final String ANULAR_PETICION_NEFIS = "ANULAR_PETICION_NEFIS";
  /** The entity manager. */
  @PersistenceContext
  EntityManager entityManager;

  /**
   * Generar documento contable.
   *
   * @param tipoDocumento the tipo documento
   * @param subTipoDocumento the sub tipo documento
   * @param tipoComplementario the tipo complementario
   * @param sector the sector
   * @param tipoPrestacion the tipo prestacion
   * @param provincia the provincia
   * @param ejercicio the ejercicio
   * @param fechaEfecto the fecha efecto
   * @param importe the importe
   * @param usuario the usuario
   * @return the string
   */
  @Override
  public String generarDocumentoContable(final String tipoDocumento,
      final String subTipoDocumento, final String tipoComplementario,
      final String sector, final String tipoPrestacion, final String provincia,
      final Long ejercicio, final Date fechaEfecto, final BigDecimal importe,
      final Long usuario) {
    final int tipoDocumentoParam = 1;
    final int subTipoDocumentoParam = 2;
    final int tipoComplementarioParam = 3;
    final int sectorParam = 4;
    final int tipoPrestacionParam = 5;
    final int provinciaParam = 6;
    final int ejercicioParam = 7;
    final int fechaEfectoParam = 8;
    final int importeParam = 9;
    final int usuarioParam = 10;
    final int resultadoParam = 11;

    // TODO cambiar GENERAR_DOCUMENTO_CONTABLE_CON_SECTOR POR
    // GENERAR_DOCUMENTO_CONTABLE si queremos que funcione sin sector
    final StoredProcedureQuery spq =
        entityManager.createStoredProcedureQuery(GENERAR_DOCUMENTO_CONTABLE);
    spq.registerStoredProcedureParameter(tipoDocumentoParam, String.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(subTipoDocumentoParam, String.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(tipoComplementarioParam, String.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(sectorParam, String.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(tipoPrestacionParam, String.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(provinciaParam, String.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(ejercicioParam, Long.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(fechaEfectoParam, Date.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(importeParam, BigDecimal.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(usuarioParam, Long.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(resultadoParam, String.class,
        ParameterMode.OUT);

    spq.setParameter(tipoDocumentoParam, tipoDocumento);
    spq.setParameter(subTipoDocumentoParam, subTipoDocumento);
    spq.setParameter(tipoComplementarioParam, tipoComplementario);
    spq.setParameter(sectorParam, sector);
    spq.setParameter(tipoPrestacionParam, tipoPrestacion);
    spq.setParameter(provinciaParam, provincia);
    spq.setParameter(ejercicioParam, ejercicio);
    spq.setParameter(fechaEfectoParam, fechaEfecto);
    spq.setParameter(importeParam, importe);
    spq.setParameter(usuarioParam, usuario);

    spq.execute();

    return (String) spq.getOutputParameterValue(resultadoParam);
  }

  /**
   * Anular peticion nefis.
   *
   * @param idDoc the id doc
   * @return the long
   */
  @Override
  public Long anularPeticionNefis(final Long idDoc) {
    final int idDocumentoParam = 1;
    final int resultadoParam = 2;

    final StoredProcedureQuery spq =
        entityManager.createStoredProcedureQuery(ANULAR_PETICION_NEFIS);
    spq.registerStoredProcedureParameter(idDocumentoParam, Long.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(resultadoParam, Long.class,
        ParameterMode.OUT);

    spq.setParameter(idDocumentoParam, idDoc);

    spq.execute();

    return (Long) spq.getOutputParameterValue(resultadoParam);
  }

}
