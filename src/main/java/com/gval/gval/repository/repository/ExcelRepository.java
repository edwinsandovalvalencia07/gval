package com.gval.gval.repository.repository;


import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gval.gval.entity.vistas.model.GenerarExcel;

// TODO: Auto-generated Javadoc
/**
 * The Interface ExcelRepository.
 */
public interface ExcelRepository extends JpaRepository <GenerarExcel,Integer>{

  /**
   * Obtener resoluciones seleccionadas.
   *
   * @param resoluciones the resoluciones
   * @return the list
   */
//  @Query(" SELECT ex from GenerarExcel ex where ex.pkResoluci IN :resoluciones and ex.pkTipoDocu in :pkTipoDocu")
//	List<GenerarExcel> obtenerResolucionesSeleccionadas(@Param("resoluciones") Set<Long>  resoluciones,@Param("pkTipoDocu") List<Long> valoresPkTipoDoc);

//	@Query(" SELECT ex from GenerarExcel ex where ex.pkResoluci IN :resoluciones and ex.rowNumber=:rowNum")
//	List<GenerarExcel> obtenerResolucionesSeleccionadas(@Param("resoluciones") Set<Long> resoluciones,
//			@Param("rowNum") Integer rowNum);

	@Query(" SELECT ex from GenerarExcel ex where ex.pkResoluci IN :resoluciones")
	List<GenerarExcel> obtenerResolucionesSeleccionadas(@Param("resoluciones") Set<Long> resoluciones);

}
