package com.gval.gval.repository.repository.impl;

import com.gval.gval.repository.repository.custom.DocumentoDescargaRepositoryCustom;
import jakarta.persistence.EntityManager;
import jakarta.persistence.ParameterMode;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.StoredProcedureQuery;

import java.util.List;


/**
 * The Class SolicitudRepositoryImpl.
 */
public class DocumentoDescargaRepositoryImpl implements DocumentoDescargaRepositoryCustom {

	/** The Constant OBTENER_DOCUMENTOS_REQUERIBLES. */
	private static final String DESCARGAR_DOCUMENTOS_V2 = "DESCARGAR_DOCUMENTOS_V2";

	/** The entity manager. */
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public List<Object[]> obtenerDocumentoPDF(String codResolu, String codTipoDocu, String orden) {
	    final int codResoluParam = 1;
	    final int codTipoDocuParam = 2;
	    final int ordenParam = 3;
	    final int cDocusOutParam = 4;
	    
		final StoredProcedureQuery spq = entityManager.createStoredProcedureQuery(DESCARGAR_DOCUMENTOS_V2);
		spq.registerStoredProcedureParameter(codResoluParam, String.class, ParameterMode.IN);
		spq.registerStoredProcedureParameter(codTipoDocuParam, String.class, ParameterMode.IN);
		spq.registerStoredProcedureParameter(ordenParam, String.class, ParameterMode.IN);
		spq.registerStoredProcedureParameter(cDocusOutParam, void.class, ParameterMode.REF_CURSOR);

		spq.setParameter(codResoluParam, codResolu);
		spq.setParameter(codTipoDocuParam, codTipoDocu);
		spq.setParameter(ordenParam, orden);

		spq.execute();

		// Obtener los resultados como una lista de arrays de objetos
		return spq.getResultList();
	}

}
