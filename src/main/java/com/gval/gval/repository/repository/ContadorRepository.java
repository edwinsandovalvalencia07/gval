package com.gval.gval.repository.repository;


import com.gval.gval.entity.model.Contador;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;



/**
 * The interface Contador repository.
 */
@Repository
public interface ContadorRepository extends JpaRepository<Contador, Long>,
    ExtendedQuerydslPredicateExecutor<Contador> {

  /**
   * Obtener contador sad.
   *
   * @param pZonaCobertura the zona cobertura
   * @param pClaveEjercicio the clave ejercicio
   * @return the long
   */
  @Query(
      value = "SELECT PCK_CL_DIST_SCRIPTS.fn_obtener_contador_sad (:pZonaCobertura, :pClaveEjercicio) FROM DUAL",
      nativeQuery = true)
  Long obtenerContadorSad(@Param("pZonaCobertura") Long pZonaCobertura,
      @Param("pClaveEjercicio") Long pClaveEjercicio);


  /**
   * Find by fk zona cobertura and clave anyo.
   *
   * @param fkZonaCobertura the fk zona cobertura
   * @param anyo the anyo
   * @return the contador
   */
  Contador findByFkZonaCoberturaAndEjercicioAnyo(Long fkZonaCobertura,
      Integer anyo);



}
