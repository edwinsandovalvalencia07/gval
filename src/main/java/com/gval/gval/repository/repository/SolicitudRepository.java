package com.gval.gval.repository.repository;

import java.util.List;
import java.util.Optional;

import com.gval.gval.dto.dto.SolicitudListadoDTO;
import com.gval.gval.repository.repository.custom.SolicitudRepositoryCustom;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gval.gval.entity.model.Expediente;
import com.gval.gval.entity.model.Solicitud;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;



// TODO: Auto-generated Javadoc
/**
 * The Interface SolicitudRepository.
 */
@Repository
public interface SolicitudRepository extends JpaRepository<Solicitud, Long>,
    ExtendedQuerydslPredicateExecutor<Solicitud>, SolicitudRepositoryCustom {

  /**
   * Find by expediente order by pk solicitud desc.
   *
   * @param expediente the expediente
   * @param orden the orden
   * @return the optional
   */
  List<Solicitud> findByExpediente(Expediente expediente, Sort orden);

  /**
   * Find by expediente and activo true and expediente activo true.
   *
   * @param expediente the expediente
   * @param sort the sort
   * @return the list
   */
  List<Solicitud> findByExpedienteAndActivoTrueAndExpedienteActivoTrue(
      Expediente expediente, Sort sort);

  /**
   * Find solicitud listado from expediente.
   *
   * @param expediente the expediente
   * @return the list
   */
  @Query("select new es.gva.dependencia.ada.dto.SolicitudListadoDTO(s.pkSolicitud, s.codigo, ts.nombre as tipoSolicitud, s.fechaRegistro,"
      + "CASE WHEN sub.descripcion is not null THEN CONCAT(esol.estado.nombre ,' ', sub.descripcion) ELSE esol.estado.nombre END as estadoSolicitud, "
      + "s.bloqueada, s.grado, CASE WHEN s.tieneNivel = true THEN s.nivel ELSE '' END as nivelPantalla, s.fechaRevisable, s.traslado, s.trasladoSaliente, s.revision, "
      + "s.motivoFinalizada) " + " from Solicitud s, EstadoSolicitud esol"
      + "   left join s.tipoSolicitud as ts"
      + "   left join esol.subestado as sub "
      + " where s.pkSolicitud = esol.solicitud" + " and esol.activo = true"
      + " and s.expediente = :expediente" + " and s.activo = true "
      + " order by s.pkSolicitud ASC")
  List<SolicitudListadoDTO> findSolicitudListadoFromExpediente(
      @Param("expediente") Expediente expediente);

  /**
   * Find solicitud resuelta GYN.
   *
   * @param pkExpedien the pk expedien
   * @return the optional
   */
  // TODO: Revisar cuando se mapee sdm_resoluci
  @Query(value = "SELECT COUNT(*) FROM sdm_solicit s "
      + "INNER JOIN sdm_resoluci re ON s.pk_solicit = re.pk_solicit AND retipo in('GYN','GNT','DNT') AND reactivo = 1 AND reestado = 'N' AND rerevocada = 0 "
      + "WHERE  s.soactivo = 1 AND s.pk_expedien = :pkExpedien AND rownum = 1",
      nativeQuery = true)
  Integer existeSolicitudResueltaGYN(@Param("pkExpedien") Long pkExpedien);

  /**
   * Existe solicitud resuelta grado notificada.
   *
   * @param pkSolicit the pk solicit
   * @return the integer
   */
  @Query(value = "SELECT COUNT(*) FROM sdm_resoluci re "
      + "WHERE retipo in('GYN','GNT','DNT') AND revalido = 1 AND reestado = 'N' AND re.pk_solicit = :pkSolicit",
      nativeQuery = true)
  Integer existeSolicitudResueltaGradoNotificada(
      @Param("pkSolicit") Long pkSolicit);


  /**
   * Existe documento requerido.
   *
   * @param pkSolicit the pk solicit
   * @return the integer
   */
  @Query(value = "SELECT COUNT(*) FROM sdm_solicit s "
      + "INNER JOIN sdm_docu d ON s.pk_solicit = d.pk_solicit AND d.docactivo = 1 "
      + "INNER JOIN sdm_docupeticion dp ON d.pk_docu = dp.pk_docu AND dp.doactivo = 1 "
      + "WHERE s.soactivo = 1 AND s.pk_solicit = :pkSolicit AND dp.dofechcomp IS NULL",
      nativeQuery = true)
  Integer existeDocumentoRequerido(@Param("pkSolicit") Long pkSolicit);


  /**
   * Existe solicitud preferencias cuidador tipo.
   *
   * @param pkSolicit the pk solicit
   * @param codigo the codigo
   * @return the integer
   */
  @Query(value = "SELECT COUNT(*) FROM sdm_solicit s "
      + "INNER JOIN (SELECT sol.pk_expedien, pref.* FROM sdm_prefsol pref "
      + "INNER JOIN sdm_solicit sol ON sol.pk_solicit = pref.pk_solicit AND sol.soactivo = 1 "
      + "INNER JOIN sdm_pref_cataserv pcat ON pcat.pk_prefsol = pref.pk_prefsol "
      + "INNER JOIN sdx_cataserv cat ON cat.pk_cataserv =  pcat.pk_cataserv AND cat.cacodigo=:codigo "
      + "INNER JOIN sdm_cnp cnp ON cnp.pk_pref_cataserv =  pcat.pk_pref_cataserv AND cnp.cnactivo = 1 "
      + ") prefer_exp ON prefer_exp.pk_expedien = s.pk_expedien "
      + "INNER JOIN  sdm_estsol est  ON est.pk_solicit =  s.pk_solicit  AND est.esactivo = 1 "
      + "INNER JOIN sdx_estado ext  ON est.pk_estado = ext.pk_estado "
      + "WHERE s.soactivo = 1 AND ext.esproceso <> 10 AND ext.pk_estado>=3 AND ((ext.escodigo in ('CO') AND s.sobloquead=0) OR (ext.escodigo != 'CO' )) AND s.pk_solicit = :pkSolicit ",
      nativeQuery = true)
  Integer existeSolicitudPreferenciasCuidadorTipo(
      @Param("pkSolicit") Long pkSolicit, @Param("codigo") String codigo);


  /**
   * Existe expediente activo sidep.
   *
   * @param nif the nif
   * @return the long
   */
  @Query(value = "SELECT EXISTE_EXPEDIENTE_SIDEP(:nif) FROM DUAL",
      nativeQuery = true)
  Long existeExpedienteActivoSidep(@Param("nif") String nif);

  /**
   * Obtener ultima solicitud inicial.
   *
   * @param pkExpedien the pk expedien
   * @return the long
   */
  @Query(value = "SELECT OBTENERULTIMASOLICITUDINICIAL(:pkExpedien) FROM DUAL",
      nativeQuery = true)
  Long obtenerUltimaSolicitudInicial(@Param("pkExpedien") Long pkExpedien);


  /**
   * Existe solicitud imserso.
   *
   * @param pkSolicit the pk solicit
   * @return the boolean
   */
  @Query(value = " select EXISTE_SOLICITUD_IMSERSO(:pkSolicit) from dual ",
      nativeQuery = true)
  String existeSolicitudImserso(@Param("pkSolicit") Long pkSolicit);


  /**
   * Existe pia para informe social seguimiento.
   *
   * @param pkSolicit the pk solicit
   * @param codigo the codigo
   * @return the string
   */
  @Query(
      value = " select existe_pia_en_solicitud(:pkSolicit, :codigo) from dual ",
      nativeQuery = true)
  String existePiaParaInformeSocialSeguimiento(
      @Param("pkSolicit") Long pkSolicit, @Param("codigo") String codigo);

  /**
   * Existe pia servicio en solicitud.
   *
   * @param pkSolicit the pk solicit
   * @param codigo the codigo
   * @return the string
   */
  @Query(
      value = " select existe_pia_servicio_en_solicitud(:pkSolicit, :codigo) from dual ",
      nativeQuery = true)
  String existePiaServicioEnSolicitud(@Param("pkSolicit") Long pkSolicit,
      @Param("codigo") String codigo);

  /**
   * Find by codigo and activo true.
   *
   * @param codigo the codigo
   * @return the solicitud
   */
  Optional<Solicitud> findByCodigoAndActivoTrue(String codigo);

  /**
   * Existe subsanacion abierta.
   *
   * @param pkSolicit the pk solicit
   * @return the integer
   */
  @Query(value = "SELECT COUNT(*) FROM sdm_solicit s "
  		+ "INNER JOIN sdm_subsanac subsa ON s.pk_solicit = subsa.pk_solicit "
  		+ "WHERE s.pk_solicit = :pkSolicit AND "
  		+ "subsa.suactivo = 1 AND "
  		+ "subsa.sufechcomp IS NULL "
  		+ "ORDER BY subsa.pk_solicit DESC",
      nativeQuery = true)
  Integer existeSubsanacionAbierta(@Param("pkSolicit") Long pkSolicit);

	/**
	 * Obtener solicitud anterior by expe soli.
	 *
	 * @param pkExpedien the pk expedien
	 * @param pkSolicit the pk solicit
	 * @return the solicitud
	 */
	@Query(value = "SELECT ssaa.* FROM (SELECT s.* FROM sdm_solicit s "
			+ "INNER JOIN sdm_expedien ex ON s.pk_expedien = ex.pk_expedien WHERE ex.pk_expedien = :pkExpedien "
			+ "AND s.soactivo = 1 AND s.pk_solicit < :pkSolicit "
			+ "ORDER BY s.pk_solicit DESC) ssaa WHERE rownum = 1", nativeQuery = true)
	Solicitud obtenerSolicitudAnteriorByExpeSoli(@Param("pkExpedien") Long pkExpedien,
			@Param("pkSolicit") Long pkSolicit);

	/**
	 * Obtener solicitud anterior soli.
	 *
	 * @param pkSolicit the pk solicit
	 * @return the solicitud
	 */
	@Query(value = "SELECT ssaa.* FROM (SELECT * FROM sdm_solicit s WHERE s.soactivo = 1 "
			+ "AND s.pk_solicit < :pkSolicit "
			+ "ORDER BY s.pk_solicit DESC) ssaa WHERE rownum = 1", nativeQuery = true)
	Solicitud obtenerSolicitudAnteriorSoli(@Param("pkSolicit") Long pkSolicit);

    /**
     * Recalcular grado Y nivel.
     *
     * @param numeroSolicitud the numero solicitud
     */
    @Procedure(name = "RECALCULAR_GRADO_Y_NIVEL")
    void recalcularGradoYNivel(@Param("codsolicitud") String numeroSolicitud);

    /**
     * Obtener solicitud estado tipo.
     *
     * @param pkSolicitud the pk solicitud
     * @param tipo the tipo
     * @return the solicitud DTO
     */
    @Query(value = "select *from SDM_SOLICIT soli " + " inner join SDM_ESTUDIO estu on soli.PK_SOLICIT=estu.PK_SOLICIT "
        + " where soli.PK_SOLICIT= :pkSolicitud and  estu.ESESTADO ='P' and estu.ESTIPO = :tipo", nativeQuery = true)
    Solicitud obtenerSolicitudEstadoTipo(@Param("pkSolicitud") Long pkSolicitud, @Param("tipo") String tipo);
}
