package com.gval.gval.repository.repository.custom;


import com.gval.gval.dto.dto.*;

/**
 * The Interface ResolucionListadoRepositoryCustom.
 */
public interface ResolucionListadoRepositoryCustom {

  /**
   * Guardar resolucion pia notificada.
   *
   * @param expediente the expediente
   * @param documento the documento
   * @param resolucion the resolucion
   * @param usuarioDTO the usuario DTO
   * @return the string
   */
  Long guardarResolucionPiaNotificada(ExpedienteDTO expediente,
                                      DocumentoGeneradoDTO documento, ResolucionListadoDTO resolucion,
                                      UsuarioDTO usuarioDTO);

  /**
   * Marcha atras resolucion notificada.
   *
   * @param pkResoluci the pk resoluci
   * @param pkSolicit the pk solicit
   * @param pkUsuario the pk usuario
   * @param esPia the es pia
   * @param enviadoNsisaad the enviado nsisaad
   * @param firmadoLote the firmado lote
   * @param documento the documento
   * @return the string
   */
  void marchaAtrasResolucionNotificada(Long pkResoluci, Long pkSolicit,
      Long pkUsuario, Boolean esPia, Boolean enviadoNsisaad,
      Boolean firmadoLote, DocumentoDTO documento);

  /**
   * Revocar resolucion.
   *
   * @param codigoSolicitud the codigo solicitud
   * @param pkUsuario the pk usuario
   * @param resolucionDTO the resolucion DTO
   * @param pkResolucionARevocar the pk resolucion A revocar
   */
  void revocarResolucion(String codigoSolicitud, Long pkUsuario,
      ResolucionDTO resolucionDTO, Long pkResolucionARevocar);

}
