package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.TramiteSolicitud;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * The Interface TramiteSolicitudRepository.
 */
@Repository
public interface TramiteSolicitudRepository
    extends JpaRepository<TramiteSolicitud, Long>,
    ExtendedQuerydslPredicateExecutor<TramiteSolicitud> {
  /**
   * Repositorio tramite solicitud
   */
}
