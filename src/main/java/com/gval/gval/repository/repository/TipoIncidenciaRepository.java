package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.TipoIncidencia;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The Interface TipoIncidenciaRepository.
 */
@Repository
public interface TipoIncidenciaRepository
    extends JpaRepository<TipoIncidencia, Long>,
    ExtendedQuerydslPredicateExecutor<TipoIncidencia> {


  /**
   * Find by autogenerado false and bloquea false and activo true and pk tipodocu is null order by nombre.
   *
   * @return the list
   */
  List<TipoIncidencia> findByAutogeneradoFalseAndBloqueaFalseAndActivoTrueAndPkTipodocuIsNullOrderByNombre();

}
