package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.MotivoGeneracionInformeSocial;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * The Interface MotivoGeneracionInformeSocialRepository.
 */
@Repository
public interface MotivoGeneracionInformeSocialRepository
    extends JpaRepository<MotivoGeneracionInformeSocial, Long>,
    ExtendedQuerydslPredicateExecutor<MotivoGeneracionInformeSocial> {


}
