package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.MotivoGeneracion;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * The Interface MotivoGeneracionRepository.
 */
@Repository
public interface MotivoGeneracionRepository
    extends JpaRepository<MotivoGeneracion, Long>,
    ExtendedQuerydslPredicateExecutor<MotivoGeneracion> {

  /**
   * Find by codigo.
   *
   * @param codigo the codigo
   * @return the optional
   */
  Optional<MotivoGeneracion> findByCodigo(String codigo);


}
