package com.gval.gval.repository.repository;



import com.gval.gval.entity.model.AsistentePersonal;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


/**
 * Repositorio de consultas para AsistentePersonal.
 */
public interface AsistenteRepository
extends JpaRepository<AsistentePersonal, Long>,
        ExtendedQuerydslPredicateExecutor<AsistentePersonal> {

  /**
   * Find by pk asistente personal and activo true.
   *
   * @param pkAsispers the pk asistente personal
   * @return the optional
   */
  Optional<AsistentePersonal> findByPkAsispers(Long pkAsispers);

  /**
   * Find asistentes personales verificador por pk expediente.
   *
   * @param pkExpediente the pk expediente
   * @return the list
   */
  // TODO: implementar @Query nativa correspondiente
  @Query(value = " select DISTINCT(ap.pk_asispers) " + "from sdm_solicit s "
      + " inner join sdm_prefsol pf on s.pk_solicit = pf.pk_solicit "
      + " inner join sdm_pref_cataserv pc on pc.pk_prefsol = pf.pk_prefsol "
      + " inner join sdm_asispers ap on ap.pk_pref_cataserv = pc.pk_pref_cataserv "
      + " inner join sdm_persona p on ap.pk_persona = p.pk_persona "
      + " where ap.asfechfin is null and pf.prestado = 0 and ap.asactivo = 1 and s.pk_expedien = :pkExpediente ",
      nativeQuery = true)
  List<Long> findAsistentesVerificadorPorPkExpediente(
      @Param("pkExpediente") Long pkExpediente);

  /**
   * Find by persona nif and activo true.
   *
   * @param nif the nif
   * @return the optional
   */
  List<AsistentePersonal> findByPersonaNifAndActivoTrue(String nif);


  /**
   * Find by preferencia catalogo servicio pk pref cataserv and activo true.
   *
   * @param pkPreferenciaCatalogoServicio the pk preferencia catalogo servicio
   * @return the list
   */
  List<AsistentePersonal> findByPreferenciaCatalogoServicioPkPrefCataservAndActivoTrue(
      Long pkPreferenciaCatalogoServicio);

  /**
   * Find asistentes por pk pia.
   *
   * @param pkPia the pk pia
   * @return the list
   */
  @Query(value = " SELECT DISTINCT(ap.PK_ASISPERS) FROM sdm_asispers ap "
      + "INNER JOIN sdm_proppia ppia on ppia.pk_proppia = ap.pk_proppia "
      + "INNER JOIN SDM_PIA sp ON sp.PK_PIA = ppia.PK_PIA "
      + "WHERE sp.PK_PIA = :pkPia and ap.ASFECHFIN is null ", nativeQuery = true)
  List<Long> findAsistentesPorPkPia(@Param("pkPia") Long pkPia);

}
