package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.EstadoTramiteSolicitud;
import com.gval.gval.entity.model.TramiteSolicitud;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * The Interface EstadoTramiteSolicitudRepository.
 */
@Repository
public interface EstadoTramiteSolicitudRepository
    extends JpaRepository<EstadoTramiteSolicitud, Long>,
    ExtendedQuerydslPredicateExecutor<EstadoTramiteSolicitud> {

  /**
   * Obtener estado tramite.
   *
   * @param tramiteSolicitud the tramite solicitud
   * @param activo the activo
   * @return the list
   */
  Optional<EstadoTramiteSolicitud> findByTramiteSolicitudAndActivo(
      TramiteSolicitud tramiteSolicitud, int activo);
}

