package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Expediente;
import com.gval.gval.entity.model.Persona;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;


/**
 * The Interface ExpedienteRepository.
 */
@Repository
public interface ExpedienteRepository extends JpaRepository<Expediente, Long>,
    ExtendedQuerydslPredicateExecutor<Expediente> {

  /**
   * Ejecuta la secuencia y obtiene el valor siguiente.
   *
   * @return the num exp sequence
   */
  @Query(value = "SELECT SEC_SDM_NUMEXPEDIEN.NEXTVAL FROM dual",
      nativeQuery = true)
  Long getNumExpSequence();

  /**
   * Find by solicitante and activo true.
   *
   * @param solicitante the solicitante
   * @return the optional
   */
  Optional<Expediente> findBySolicitanteAndActivoTrue(Persona solicitante);

  /**
   * Habilitar pestanya informes sociales.
   *
   * @param expediente the expediente
   * @return true, if successful
   */
  @Query("select case when (count(s.pkSolicitud) > 0) then true else false end"
      + " from Solicitud s, EstadoSolicitud esol"
      + " where s.pkSolicitud = esol.solicitud"
      + " and s.activo = 1 and esol.activo = 1"
      + " and (esol.estado > 3 or (esol.estado = 3 and NOT (s.bloqueada = 1 and s.pendienteDocumentacion = 1)))"
      + " and s.expediente = :expediente")
  boolean habilitarPestanyaInformesSociales(
      @Param("expediente") Expediente expediente);

  /**
   * Habilitar pestanya valoraciones.
   *
   * @param expediente the expediente
   * @return true, if successful
   */
  // @formatter:off
  @Query("select case when (( select count(asig.pkAsigsoli) "
      + "           from Solicitud s, AsignarSolicitud asig"
      + "           where s.expediente = e.pkExpedien"
      + "           and s.pkSolicitud = asig.solicitud and s.activo = 1"
      + "           and asig.activo = 1 and asig.fechaCita IS NOT NULL) > 0) then true"
      + "   when (( select count(asig.pkAsigsoli)"
      + "           from Solicitud s, AsignarSolicitud asig, Valoracion va"
      + "           where s.expediente = e.pkExpedien"
      + "           and s.pkSolicitud = asig.solicitud and s.activo = 1"
      + "           and va.asignarSolicitud = asig.pkAsigsoli"
      + "           and va.activo = 1) > 0) then true else false end"
      + " from Expediente e where e = :expediente")
  // @formatter:on
  boolean habilitarPestanyaValoraciones(
      @Param("expediente") Expediente expediente);

  /**
   * Obtener propuesta pia posterior.
   *
   * @param pkSolicit the pk solicit
   * @param fechaResolucion the fecha resolucion
   * @return the list
   */
  @Query(value = "select nvl(cat.cacodigo, ca.cacodigo) from sdm_proppia p "
      + "    inner join sdm_presprpi pr on pr.pk_proppia = p.pk_proppia"
      + "    inner join sdx_cataserv ca on ca.pk_cataserv = pr.pk_cataserv"
      + "    left join sdm_pia pi on pi.pk_pia = p.pk_pia"
      + "    left join sdx_cataserv cat on pi.pk_cataserv = cat.pk_cataserv"
      + "    where p.pk_solicit = :pkSolicit and p.prfechcrea > :fechaResolucion",
      nativeQuery = true)
  List<String> obtenerPropuestaPiaPosterior(@Param("pkSolicit") Long pkSolicit,
      @Param("fechaResolucion") Date fechaResolucion);

  /**
   * Find by solicitante nif and activo true.
   *
   * @param nif the nif
   * @return the optional
   */
  List<Expediente> findBySolicitanteNifAndActivoTrue(String nif);


  /**
   * Habilitar pestanya pias.
   *
   * @param pkExpedien the pk expedien
   * @return true, if successful
   */
  // @formatter:off
  @Query(
      value = "select count(pia.pk_pia)" + " from sdm_pia pia "
          + " where pia.pk_expedien = :pkExpedien "
          + " and pia.piactivo = 1 and pia.pk_resoluci2 IS NOT NULL",
      nativeQuery = true)
  // @formatter:on
  Integer habilitarPestanyaPias(@Param("pkExpedien") Long pkExpedien);


  /**
   * Expediente no activo.
   *
   * @param pkPersona the pk persona
   * @return true, if error
   */
  // @formatter:off
  @Query(
      value = "select count(exped.pk_expedien) from sdm_expedien exped where exped.exactivo = 0 and exped.pk_persona = :pkPersona ",
      nativeQuery = true)
  // @formatter:on
  Integer tieneExpedienteInactivo(@Param("pkPersona") Long pkPersona);


  /**
   * Existe expediente resuelta grado notificada.
   *
   * @param pkExpedien the pk expedien
   * @return the integer
   */
  @Query(value = "SELECT COUNT(*) FROM sdm_resoluci re "
      + "inner join sdm_solicit s on re.pk_solicit=s.pk_solicit "
      + "inner join sdm_expedien ex on s.pk_expedien=ex.pk_expedien "
      + "WHERE retipo in('GYN','GNT','DNT') AND revalido = 1 AND reestado = 'N' AND ex.pk_expedien = :pkExpedien",
      nativeQuery = true)
  Integer existeResolucionGyNporExpediente(
      @Param("pkExpedien") Long pkExpedien);

  /**
   * Obtener ultimo grado expediente.
   *
   * @param pkExpedien the pk expedien
   * @return the string
   */
  @Query(value = "SELECT ULTIMO_GRADO_EXPEDIENTE(:pkExpedien) FROM dual",
      nativeQuery = true)
  String obtenerUltimoGradoExpediente(@Param("pkExpedien") Long pkExpedien);
}
