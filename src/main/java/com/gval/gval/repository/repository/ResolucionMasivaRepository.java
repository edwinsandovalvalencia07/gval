package com.gval.gval.repository.repository;

import com.gval.gval.entity.vistas.model.ResolucionListado;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * The Interface ResolucionMasivaRepository.
 */
@Repository("resolucionMasivaRepository")
public interface ResolucionMasivaRepository
    extends JpaRepository<ResolucionListado, Long> {

  /**
   * Gets the total cars by model entiy.
   *
   * @param idProcedimiento the id procedimiento
   * @param pkSolicit the pk solicit
   * @param pkResoluci the pk resoluci
   * @param pkPia the pk pia
   * @param pkDocu the pk docu
   * @param pkPersona the pk persona
   * @return the total cars by model entiy
   */
  @Procedure(procedureName = "MARCHA_ATRAS_RESO_MASIVA",
      outputParameterName = "P_RESULTADO")
  String marchaAtrasResoMasiva(
      @Param("P_ID_PROCEDIMIENTO") Long idProcedimiento,
      @Param("P_PK_SOLICIT") Long pkSolicit,
      @Param("P_PK_RESOLUCI") Long pkResoluci, @Param("P_PK_PIA") Long pkPia,
      @Param("P_PK_DOCU") Long pkDocu, @Param("P_PK_PERSONA") Long pkPersona);

  /**
   * Prueba generar reso.
   *
   * @param idProcedimiento the id procedimiento
   * @param pkSolicit the pk solicit
   * @param pkResoluci the pk resoluci
   * @param pkPersona the pk persona
   * @param fechaReso the fecha reso
   * @param fechaEfecto the fecha efecto
   * @param observaciones the observaciones
   * @param esNuevoLote the es nuevo lote
   * @param numCargaMasiva the num carga masiva
   * @return the string
   */
  @Procedure(procedureName = "GENERAR_RESO_MASIVA",
      outputParameterName = "p_resultado")
  String generarResoMasiva(@Param("p_id_procedimiento") Long idProcedimiento,
      @Param("p_pk_solicit") Long pkSolicit,
      @Param("p_pk_resoluci") Long pkResoluci,
      @Param("p_pk_persona") Long pkPersona,
      @Param("p_refechreso") Date fechaReso,
      @Param("p_refechvigo") Date fechaEfecto,
      @Param("p_observaciones") String observaciones,
      @Param("p_es_nuevo_lote") Long esNuevoLote,
      @Param("p_num_carga_masiva") Long numCargaMasiva);

  /**
   * Refrescar vistas materializadas.
   *
   * @param idProcedimiento the id procedimiento
   */
  @Procedure(procedureName = "REFRESCAR_VISTAS_MAT")
  void refrescarVistasMaterializadas(
      @Param("p_id_procedimiento") Long idProcedimiento);


}
