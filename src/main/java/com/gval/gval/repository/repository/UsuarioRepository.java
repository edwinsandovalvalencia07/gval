package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Usuario;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * The Interface UsuarioRepository.
 */
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long>,
    ExtendedQuerydslPredicateExecutor<Usuario> {

  /**
   * Find by nombre and activo true.
   *
   * @param nif the nif
   * @return the optional
   */
  Optional<Usuario> findByNombreIgnoreCaseAndActivoTrue(String nif);

}
