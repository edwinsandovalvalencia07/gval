package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Retropia;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

// TODO: Auto-generated Javadoc
/**
 * The Interface RetropiaRepository.
 */
@Repository
public interface RetropiaRepository extends JpaRepository<Retropia, Long> {

  /**
   * Encontrar retropia por pk resolucion.
   *
   * @param pkResoluci the pk resoluci
   * @return the optional
   */
  @Query(value = "SELECT * FROM  ( SELECT rp.* FROM SDM_RETROPIA rp WHERE rp.PK_RESOLUCI = :pkResoluci )  WHERE rownum = 1", nativeQuery = true)
  Optional<Retropia> encontrarRetropiaPorPkResolucion(@Param("pkResoluci") Long pkResoluci);

}
