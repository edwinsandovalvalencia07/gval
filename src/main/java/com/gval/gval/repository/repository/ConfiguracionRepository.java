package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Configuracion;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * The Interface ConfiguracionRepository.
 */
@Repository
public interface ConfiguracionRepository
    extends JpaRepository<Configuracion, Long>,
    ExtendedQuerydslPredicateExecutor<Configuracion> {

  /**
   * Find by codigo.
   *
   * @param codigo the codigo
   * @return the configuracion
   */
  Configuracion findByCodigo(final String codigo);
  
  
  /**
   * Find by codigo padre and codigo.
   *
   * @param codigoPadre the codigo padre
   * @param codigo the codigo
   * @return the optional
   */
  Optional<Configuracion> findByCodigoPadreAndCodigo(final String codigoPadre, final String codigo);

  /**
   * Find by pk configuracion.
   *
   * @param pkConfiguracion the pk configuracion
   * @return the configuracion
   */
  Configuracion findByPkConfiguracion(Long pkConfiguracion);

}
