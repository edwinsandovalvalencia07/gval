package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.ComunidadAutonoma;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * The Interface ComunidadAutonomaRepository.
 */
@Repository
public interface ComunidadAutonomaRepository
    extends JpaRepository<ComunidadAutonoma, Long>,
    ExtendedQuerydslPredicateExecutor<ComunidadAutonoma> {

}
