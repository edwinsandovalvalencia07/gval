package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Resolucion;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

// TODO: Auto-generated Javadoc
/**
 * The Interface ResolucionRepository.
 */
@Repository
public interface ResolucionRepository extends JpaRepository<Resolucion, Long>,
    ExtendedQuerydslPredicateExecutor<Resolucion> {

  /**
   * Find by tipo.
   *
   * @param tipo the tipo
   * @return the optional
   */
  List<Resolucion> findByTipo(String tipo);

  /**
   * Find by pkresoluci.
   *
   * @param pkResoluci the pk resoluci
   * @return the optional
   */
  Resolucion findByPkResoluci(Long pkResoluci);

  /**
   * Find by pk solicit and valido true.
   *
   * @param pkSolicit the pk solicit
   * @param sort the sort
   * @return the optional
   */
  List<Resolucion> findBySolicitudPkSolicitudAndValidoTrue(Long pkSolicit,
      Sort sort);

  /**
   * Obtiene la resolucion por tipo que no este revocada.
   *
   * @param pkSolicit the pk solicit
   * @param tipo the tipo
   * @return the resolucion
   */
  Resolucion findBySolicitudPkSolicitudAndTipoAndRevocadaFalse(Long pkSolicit,
      String tipo);

  /**
   * Find by pk solicit and caducada true.
   *
   * @param pkSolicit the pk solicit
   * @return the resolucion
   */
  Resolucion findBySolicitudPkSolicitudAndCaducadaTrue(Long pkSolicit);

  /**
   * Obtener resolucion pia por recurso.
   *
   * @param pkSolicit the pk solicit
   * @param pkRecurso the pk recurso
   * @return the list
   */
  @Query(value = " select r.* " + "from sdm_resoluci r "
      + "inner join sdm_pia p on r.pk_resoluci = p.pk_resoluci2 "
      + "inner join sdm_solicit s on s.pk_solicit = p.pk_solicit "
      + "where s.pk_solicit = :pkSolicit and p.pk_cataserv = :pkCataserv "
      + "order by r.pk_resoluci DESC", nativeQuery = true)
  List<Resolucion> obtenerResolucionPiaPorRecurso(
      @Param("pkSolicit") Long pkSolicit, @Param("pkCataserv") Long pkRecurso);

  /**
   * Obtener informacion PIA caducada.
   *
   * @param pkSolicit the pk solicit
   * @return the list
   */
  @Query(value = " select r.refechreso, ca.cacodigo " + "from sdm_resoluci r "
      + "inner join sdm_pia p on r.pk_resoluci = p.pk_resoluci2 and p.picaducada = 1 "
      + "inner join sdm_solicit s on s.pk_solicit = p.pk_solicit "
      + "inner join sdx_cataserv ca on ca.pk_cataserv = p.pk_cataserv "
      + "where s.pk_solicit = :pkSolicit", nativeQuery = true)
  Optional<Object> obtenerInformacionPIACaducada(
      @Param("pkSolicit") Long pkSolicit);

  /**
   * Obtener resolucion pia by pk solicit segun cataserv.
   *
   * @param pkExpedien the pk expedien
   * @param codCataserv the cod cataserv
   * @return the list
   */
  @Query(value = "select r.* from sdm_resoluci r "
      + "    inner join sdm_solicit s on s.pk_solicit = r.pk_solicit "
      + "    inner join sdm_pia p on p.pk_resoluci2 = r.pk_resoluci and p.pifechbaja is null"
      + "    inner join sdx_cataserv cata on p.pk_cataserv = cata.pk_cataserv"
      + "    where s.pk_expedien = :pkExpedien and cata.cacodigo = :codCataserv"
      + "    order by r.pk_resoluci desc", nativeQuery = true)
  List<Resolucion> obtenerResolucionPiaByPkExpedienteSegunCataserv(
      @Param("pkExpedien") Long pkExpedien,
      @Param("codCataserv") String codCataserv);

  /**
   * Obtener resolucion revocacion PI ao caducidad.
   *
   * @param pkExpedien the pk expedien
   * @return the resolucion
   */
  @Query(
      value = "select * from sdm_resoluci r "
          + "inner join sdm_solicit s on s.pk_solicit = r.pk_solicit "
          + "where s.pk_expedien = :pkExpedien and r.RETIPO in ('RCP','RAC')",
      nativeQuery = true)
  Resolucion obtenerResolucionRevocacionPIAoCaducidad(
      @Param("pkExpedien") Long pkExpedien);

  /**
   * Marcar resolucion como enviada.
   *
   * @param pkresoluci the pkresoluci
   * @param pksolicit the pksolicit
   * @return the resolucion
   */
  @Modifying
  @Query(value = "UPDATE nsisaad.envios_imserso "
      + "SET res_pk = :pkresoluci, pk_resoluci = :pkresoluci "
      + "WHERE pk_tipoenvio=8 and activo = 1 "
      + "and response_error = 'N' and aplicacion=1 "
      + "and pk_solicit = :pksolicit", nativeQuery = true)
  void marcarResolucionComoEnviada(@Param("pkresoluci") Long pkresoluci,
      @Param("pksolicit") Long pksolicit);

  /**
   * Obtener ultima solicitud resuelta GYN.
   *
   * @param pkExpedien the pk expedien
   * @return the solicitud
   */
  @Query(
      value = "SELECT * FROM sdm_resoluci where pk_resoluci = (SELECT MAX(re.pk_resoluci) FROM sdm_solicit s"
          + " INNER JOIN sdm_resoluci re ON s.pk_solicit = re.pk_solicit AND retipo in('GYN','GNT','DNT') AND reactivo = 1 AND reestado = 'N' AND rerevocada = 0"
          + " WHERE  s.soactivo = 1 AND s.pk_expedien = :pkExpedien)",
      nativeQuery = true)
  Resolucion obtenerUltimaResolucionResueltaGYN(
      @Param("pkExpedien") Long pkExpedien);

  /**
   * Find by solicitud activo true and solicitud expediente pk expedien and tipo and valido true.
   *
   * @param pkExpedien the pk expedien
   * @param tipo the tipo
   * @param sort the sort
   * @return the list
   */
  List<Resolucion> findBySolicitudActivoTrueAndSolicitudExpedientePkExpedienAndTipoAndValidoTrue(
      Long pkExpedien, String tipo, Sort sort);

  /**
   * Comprobar ultima resolucion pia prestaciones.
   *
   * @param pkExpedien the pk expedien
   * @return the string
   */
  @Query(value = "select CASE WHEN count(*) > 0 THEN 'S' ELSE 'N' END "
      + "from sdm_resoluci res "
      + "inner join sdm_solicit sol on res.pk_solicit=sol.pk_solicit and sol.soactivo=1 "
      + "inner join sdm_pia pia on res.pk_resoluci=pia.pk_resoluci2 and pia.pivalido=1 and pia.piultimo = 1 "
      + "where reactivo=1 and res.retipo='PIA' and pia.pitipo='P' and sol.pk_expedien = :pkExpedien",
      nativeQuery = true)
  String existeUltimaResolucionPiaPrestaciones(
      @Param("pkExpedien") Long pkExpedien);

  /**
   * Obtener ultima resolucion con grado Y nivel.
   *
   * @param pkExpedien the pk expedien
   * @param pkSolicit the pk solicit
   * @return the resolucion
   */
  @Query(
      value = "SELECT * "
          + "  FROM "
          + "    (SELECT sr.* FROM Sdm_Resoluci sr  "
          + "    JOIN sdm_Solicit s ON sr.PK_SOLICIT = s.PK_SOLICIT  "
          + "    JOIN sdm_Expedien e ON s.PK_EXPEDIEN   = e.PK_EXPEDIEN  "
          + "    WHERE s.PK_SOLICIT = :pkSolicit AND sr.reactivo = 1 " + "    AND e.pk_expedien  = :pkExpedien AND sr.regrado IS NOT NULL "
          + "    AND sr.renivel  IS NOT NULL ORDER BY sr.PK_RESOLUCI DESC "
          + "    ) "
          + "  WHERE rownum <= 1",
      nativeQuery = true)
  Resolucion obtenerUltimaResolucionConGradoYNivel(
      @Param("pkExpedien") Long pkExpedien, @Param("pkSolicit") Long pkSolicit);

  /**
   * Obtener resolucion anterior con grado Y nivel.
   *
   * @param pkExpedien the pk expedien
   * @param pkSolicit the pk solicit
   * @param pkResoluci the pk resoluci
   * @return the resolucion
   */
  @Query(value = "SELECT * " + "  FROM " + "    (SELECT sr.* FROM Sdm_Resoluci sr  " + "    JOIN sdm_Solicit s ON sr.PK_SOLICIT = s.PK_SOLICIT  "
      + "    JOIN sdm_Expedien e ON s.PK_EXPEDIEN   = e.PK_EXPEDIEN  " + "    WHERE  sr.reactivo = 1 " + "    AND e.pk_expedien  = :pkExpedien AND sr.regrado IS NOT NULL "
      + "    AND sr.renivel IS NOT NULL and sr.PK_RESOLUCI < :pkResoluci and s.PK_SOLICIT < :pkSolicit ORDER BY sr.PK_RESOLUCI DESC " + "    ) " + "  WHERE rownum <= 1", nativeQuery = true)
  Resolucion obtenerResolucionAnteriorConGradoYNivel(@Param("pkExpedien") Long pkExpedien, @Param("pkSolicit") Long pkSolicit, @Param("pkResoluci") Long pkResoluci);
  /**
   * Obtener resolucion revocacion PI ao caducidad.
   *
   * @param pkExpedien the pk expedien
   * @param grado the grado
   * @return the resolucion
   */
  @Query(value = "SELECT ES_VIGOR_POR_FECHAREGISTRO(:fecha, :grado) FROM DUAL ", nativeQuery = true)
  List<String> esVigorPorFechaRegistro(@Param("fecha") Date pkExpedien, @Param("grado") String grado);

  /**
   * Obtener resolucion revocacion PI ao caducidad.
   *
   * @param fecha the fecha
   * @param grado the grado
   * @param nivel the nivel
   * @return the resolucion
   */
  @Query(value = "SELECT ES_VIGOR_POR_FECHAREGISTRO(:fecha, :grado, :nivel) FROM DUAL ", nativeQuery = true)
  List<String> esVigorPorFechaRegistroAnterior(@Param("fecha") Date fecha, @Param("grado") String grado, @Param("nivel") String nivel);

  /**
   * Obtener resolucion correccion por id.
   *
   * @param pkResoluci3 the pk resoluci 3
   * @return the resolucion
   */
  @Query(value = "SELECT * FROM sdm_resoluci WHERE pk_resoluci3 = :pkResoluci3 ", nativeQuery = true)
  Optional<Resolucion> obtenerResolucionCorreccionPorId(@Param("pkResoluci3") Long pkResoluci3);

}
