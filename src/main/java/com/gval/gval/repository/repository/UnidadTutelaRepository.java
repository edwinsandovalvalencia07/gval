package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.UnidadTutela;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The Interface UnidadTutelaRepository.
 */
@Repository
public interface UnidadTutelaRepository extends JpaRepository<UnidadTutela, Long>,
    ExtendedQuerydslPredicateExecutor<UnidadTutela> {

  
}
