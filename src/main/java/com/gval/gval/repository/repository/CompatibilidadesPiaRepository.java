package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.CatalogoServicio;
import com.gval.gval.entity.model.CompatibilidadesPia;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * The Interface CompatibilidadesPiaRepository.
 */
@Repository
public interface CompatibilidadesPiaRepository
    extends JpaRepository<CompatibilidadesPia, Long>,
    ExtendedQuerydslPredicateExecutor<CompatibilidadesPia> {

  /**
   * Find compatibilidades pia servicio 1 and servicio 2.
   *
   * @param servicio1 the servicio 1
   * @param servicio2 the servicio 2
   * @return the optional
   */
  Optional<CompatibilidadesPia> findByServicio1AndServicio2AndActivoTrue(
      CatalogoServicio servicio1, CatalogoServicio servicio2);

}
