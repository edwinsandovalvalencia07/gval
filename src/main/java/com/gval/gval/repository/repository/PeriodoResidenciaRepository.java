package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.PeriodoResidencia;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * The Interface PeriodoResidenciaRepository.
 */
@Repository
public interface PeriodoResidenciaRepository
    extends JpaRepository<PeriodoResidencia, Long>,
    ExtendedQuerydslPredicateExecutor<PeriodoResidencia> {

  /**
   * Find by solicitud expediente pk expedien.
   *
   * @param pkPeriodoResidencia the pk periodo residencia
   * @return the list
   */
  List<PeriodoResidencia> findBySolicitudExpedientePkExpedien(
      Long pkPeriodoResidencia);

  /**
   * Find all.
   *
   * @return the list
   */
  @Override
  List<PeriodoResidencia> findAll();

}
