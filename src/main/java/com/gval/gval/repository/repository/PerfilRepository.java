package com.gval.gval.repository.repository;

import java.util.List;

import com.gval.gval.dto.dto.UsuarioSimpleDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gval.gval.entity.model.Perfil;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

// TODO: Auto-generated Javadoc
/**
 * The Interface PerfilRepository.
 */
public interface PerfilRepository extends JpaRepository<Perfil, Long>,
    ExtendedQuerydslPredicateExecutor<Perfil> {

  /**
   * Find listado valoradores.
   *
   * @return the list
   */
  @Query("SELECT new es.gva.dependencia.ada.dto.UsuarioSimpleDTO(PER.pkPersona, PER.nif, PER.nombre, PER.primerApellido, PER.segundoApellido) "
      + " FROM Usuario USU, Persona PER, GrupoValoracionUsuario GVU, GrupoValoracion GV "
      + " WHERE USU.persona = PER.pkPersona AND USU.pkPersona = GVU.usuario AND GVU.grupoValoracion = GV.pkGrupoValoracion "
      + " AND USU.activo = 1 AND GVU.activo = 1 AND GVU.esValorador = 1 ")
  List<UsuarioSimpleDTO> findListadoValoradores();

  /**
   * Find listado valoradores by grupo.
   *
   * @param grupoValoracion the grupo valoracion
   * @return the list
   */
  @Query("SELECT new es.gva.dependencia.ada.dto.UsuarioSimpleDTO(PER.pkPersona, USU.nombre, PER.nombre, PER.primerApellido, PER.segundoApellido) "
      + " FROM Usuario USU, Persona PER, GrupoValoracionUsuario GVU, GrupoValoracion GV "
      + " WHERE USU.persona = PER.pkPersona AND USU.pkPersona = GVU.usuario AND GVU.grupoValoracion = GV.pkGrupoValoracion "
      + " AND USU.activo = 1 AND GVU.activo = 1 AND GVU.esValorador = 1 "
      + " AND GV.pkGrupoValoracion = :grupoValoracion")
  List<UsuarioSimpleDTO> findListadoValoradoresByGrupo(
      @Param("grupoValoracion") Long grupoValoracion);

  /**
   * Find listado valoradores by grupo and unidad.
   *
   * @param grupoValoracion the grupo valoracion
   * @param unidad the unidad
   * @return the list
   */
  @Query("SELECT new es.gva.dependencia.ada.dto.UsuarioSimpleDTO(PER.pkPersona, USU.nombre, PER.nombre, PER.primerApellido, PER.segundoApellido) "
      + " FROM Usuario USU, UnidadUsuario UU, Unidad UN, Persona PER, GrupoValoracionUsuario GVU, GrupoValoracion GV"
      + " WHERE UU.usuario = USU.pkPersona AND UU.unidad = UN.pkUnidad AND USU.persona = PER.pkPersona"
      + " AND USU.pkPersona = GVU.usuario AND GVU.grupoValoracion = GV.pkGrupoValoracion"
      + " AND UU.activo = 1 AND USU.activo = 1 AND GVU.activo = 1 AND GVU.esValorador = 1 "
      + " AND GV.pkGrupoValoracion = :grupoValoracion AND UN.pkUnidad = :unidad")
  List<UsuarioSimpleDTO> findListadoValoradoresByGrupoAndUnidad(
      @Param("grupoValoracion") Long grupoValoracion,
      @Param("unidad") Long unidad);

  /**
   * Find perfil apertura.
   *
   * @param pkPersona the pk persona
   * @return the long
   */
  @Query(value = "select perf.tiperfil_apertura as prefil_apertura "
      + " from (select perf.pk_persona, tp.tiperfil_apertura, "
      + " ROW_NUMBER() OVER (PARTITION BY perf.pk_persona ORDER BY tp.tiprioridad desc) orden "
      + " from sdm_perfil_clau perf "
      + " inner join sdx_tipoperf_clau tp on tp.PK_TIPOPERF = perf.pk_tipoperf and tp.TIPACTIVO = 1 AND tp.tiperfil_apertura > 0 "
      + " where perf.PEACTIVO = 1) perf "
      + " where perf.orden = 1 and perf.pk_persona = :pkPersona ",
      nativeQuery = true)
  Long findPerfilApertura(@Param("pkPersona") Long pkPersona);

  /**
   * Obtener perfiles activos por usuario.
   *
   * @param pkPersona the pk persona
   * @return the list
   */
  @Query("select perf from Perfil perf where perf.activo = 1 and perf.usuario.pkPersona = :pkPersona ")
  List<Perfil> obtenerPerfilesActivosPorUsuario(@Param("pkPersona") Long pkPersona);


  /**
   * Usuario tiene cod permiso.
   *
   * @param pkPersona the pk persona
   * @param codPermiso the cod permiso
   * @return the int
   */

  @Query(value = "SELECT COUNT(p.pk_permiso) FROM sdm_perfil perf "
   + "  INNER JOIN sdx_tipoperf tp ON perf.pk_tipoperf = tp.pk_tipoperf "
      + "  INNER JOIN sdm_permperf pp ON pp.pk_tipoperf = tp.pk_tipoperf "
      + "  INNER JOIN sdm_permiso p  ON pp.pk_permiso = p.pk_permiso "
      + "  INNER JOIN sdx_usuario usu ON perf.pk_persona  = usu.pk_persona "
      + "  WHERE perf.peactivo = 1 AND tp.tipactivo = 1 AND p.peactivo = 1 AND usu.usactivo = 1 AND usu.pk_persona = :pkPersona AND p.pecodigo = :codPermiso", nativeQuery = true)
  int usuarioTieneCodPermiso(@Param("pkPersona") Long pkPersona, @Param("codPermiso") String codPermiso);

}
