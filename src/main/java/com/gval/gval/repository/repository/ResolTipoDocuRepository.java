package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.ResolTipoDocu;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * The Class ResolTipoDocuRepository.
 */
public interface ResolTipoDocuRepository
    extends JpaRepository<ResolTipoDocu, Long>,
    ExtendedQuerydslPredicateExecutor<ResolTipoDocu> {

  /**
   * Find by tipo.
   *
   * @param pkTipodocu the pk tipodocu
   * @return the optional
   */
  Optional<ResolTipoDocu> findByPkTipodocu(Long pkTipodocu);

  /**
   * Find by pkresoluci.
   *
   * @param pkResoluci the pk resoluci
   * @return the optional
   */
  List<ResolTipoDocu> findByPkResoluci(Long pkResoluci);
}
