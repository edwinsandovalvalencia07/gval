package com.gval.gval.repository.repository;


import com.gval.gval.entity.model.CategoriaDocSubsanacion;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * The Interface CategoriaDocSubsanacionRepository.
 */
@Repository
public interface CategoriaDocSubsanacionRepository
    extends JpaRepository<CategoriaDocSubsanacion, Long>,
    ExtendedQuerydslPredicateExecutor<CategoriaDocSubsanacion> {


  /**
   * Find by categoria.
   *
   * @param categoria the categoria
   * @return the optional
   */
  List<CategoriaDocSubsanacion> findByCategoria(String categoria);


  /**
   * Find by categoria and activo true.
   *
   * @param categoria the categoria
   * @return the categoria doc subsanacion
   */
  List<CategoriaDocSubsanacion> findByCategoriaAndActivoTrue(String categoria);


  /**
   * Find by activo true.
   *
   * @return the list
   */
  List<CategoriaDocSubsanacion> findByActivoTrue();


  /**
   * Find by tipo documento pk tipo documento and relativo true and activo true.
   *
   * @param pkTipoDocumento the pk tipo documento
   * @return the list
   */
  List<CategoriaDocSubsanacion> findByTipoDocumentoPkTipoDocumentoAndRelativoTrueAndActivoTrue(
      Long pkTipoDocumento);


}
