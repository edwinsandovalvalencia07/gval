package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.CatalogoServicio;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * The Interface CatalogoServicioRepository.
 */
@Repository
public interface CatalogoServicioRepository
    extends JpaRepository<CatalogoServicio, Long>,
    ExtendedQuerydslPredicateExecutor<CatalogoServicio> {

  /**
   * Find by codigo.
   *
   * @param codigo the codigo
   * @return the optional
   */
  Optional<CatalogoServicio> findByCodigo(String codigo);

  /**
   * Find by codigo and activo true.
   *
   * @param codigo the codigo
   * @return the catalogo servicio
   */
  CatalogoServicio findByCodigoAndActivoTrue(String codigo);

  /**
   * Obtener recursos PIA por informe social.
   *
   * @param pkInformeSocial the pk informe social
   * @return the list
   */
  @Query(value = " SELECT ca.* " + "FROM sdx_cataserv ca "
      + "INNER JOIN sdm_pia pi ON pi.pk_cataserv = ca.pk_cataserv "
      + "INNER JOIN sdm_infosoci inf ON pi.pk_pia = inf.pk_pia "
      + "WHERE inf.pk_infosoci = :pkInfosoci ", nativeQuery = true)
  CatalogoServicio obtenerRecursosPIAPorInformeSocial(
      @Param("pkInfosoci") Long pkInformeSocial);

  /**
   * Obtener foto pias vigentes.
   *
   * @param pkExpedien the pk expedien
   * @return the list
   */
  @Query(value = "SELECT ca.* FROM SDV_PIAS_VIGENTES pv "
      + " INNER JOIN sdx_cataserv ca ON ca.cacodigo = pv.recurso "
      + " WHERE pv.pk_expedien = :pkExpedien ", nativeQuery = true)
  List<CatalogoServicio> obtenerFotoPiasVigentes(
      @Param("pkExpedien") Long pkExpedien);

}
