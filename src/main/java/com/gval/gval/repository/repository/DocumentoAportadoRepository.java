package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.DocumentoAportado;
import com.gval.gval.entity.model.Expediente;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repositorio de consultas para Documento Aportado.
 *
 * @author Indra
 */
@Repository
public interface DocumentoAportadoRepository
    extends JpaRepository<DocumentoAportado, Long>,
    ExtendedQuerydslPredicateExecutor<DocumentoAportado> {

  /**
   * Consulta los documentos aportados por expediente y tipo de documento.
   *
   * @param expediente Expediente a tener en cuenta
   * @param tipoDocumento Tipo de documento a tener en cuenta
   * @return the list
   */
  @Query("SELECT DA FROM DocumentoAportado DA JOIN DA.documento D WHERE D.tipoDocumento.codigo = :tipoDocumento AND D.expediente = :expediente AND D.activo = true ORDER BY DA.pkDocumento DESC")
  List<DocumentoAportado> consultarDocumentosAportados(
      @Param("expediente") Expediente expediente,
      @Param("tipoDocumento") String tipoDocumento);


  /**
   * Find by documento solicitud activo true and documento solicitud expediente
   * pk expedien and documento activo.
   *
   * @param pkExpediente the pk expediente
   * @param activo the activo
   * @param orden the orden
   * @return the list
   */
  List<DocumentoAportado> findByDocumentoSolicitudActivoTrueAndDocumentoSolicitudExpedientePkExpedienAndDocumentoActivo(
      Long pkExpediente, Boolean activo, Sort orden);



  /**
   * Find by documento solicitud pk solicitud.
   *
   * @param pkSolicitud the pk solicitud
   * @param activo the activo
   * @param orden the orden
   * @return the list
   */
  List<DocumentoAportado> findByDocumentoSolicitudPkSolicitudAndDocumentoActivo(
      Long pkSolicitud, Boolean activo, Sort orden);


  /**
   * Acciones tras paso A comprobada de preferencias.
   *
   * @param pkSolicitud the pk solicitud
   * @param pkPersona the pk persona
   * @param pkDocumento the pk documento
   */
  @Procedure(name = "documentoaportado.accionescomprobadapreferencias",
      procedureName = "ACC_TRAS_PREF_COMPROB_SADSADTA")
  void accionesTrasPasoAComprobadaDePreferencias(
      @Param("p_pkSolicit") Long pkSolicitud,
      @Param("p_pkUsuario") Long pkPersona,
      @Param("p_pkDocu") Long pkDocumento);

}
