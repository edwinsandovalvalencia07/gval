package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Representante;
import com.gval.gval.entity.model.Solicitud;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * The Interface RepresentanteRepository.
 */
@Repository
public interface RepresentanteRepository
    extends JpaRepository<Representante, Long>,
    ExtendedQuerydslPredicateExecutor<Representante> {

  /**
   * Find by solicitud and activo true.
   *
   * @param solicitud the solicitud
   * @return the representante
   */
  Representante findBySolicitudAndActivoTrue(final Solicitud solicitud);

  /**
   * Find by solicitud expediente pk expedien and activo true and ultimo true.
   *
   * @param pkExpedien the pk expedien
   * @return the representante
   */
  Representante findBySolicitudExpedientePkExpedienAndActivoTrueAndUltimoTrue(
      Long pkExpedien);

}
