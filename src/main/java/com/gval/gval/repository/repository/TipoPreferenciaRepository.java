package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.TipoPreferencia;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * The Interface PreferenciaSolicitudRepository.
 */
@Repository
public interface TipoPreferenciaRepository
    extends JpaRepository<TipoPreferencia, Long>,
    ExtendedQuerydslPredicateExecutor<TipoPreferencia> {

  /**
   * Find by pk tipo prefsol.
   *
   * @param id the id
   * @return the object
   */
  Optional<TipoPreferencia> findByPkTipoPrefsol(Long id);

}
