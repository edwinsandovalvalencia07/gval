package com.gval.gval.repository.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;


/**
 * The Interface DocumentoRepositoryCustom.
 */
public interface DocumentoRepositoryCustom {


  /**
   * Obtener documentos sincronizacion.
   *
   * @param fechaDesde the fecha desde
   * @param numeroDocumentos the numero documentos
   * @return the optional
   */
  Optional<List<Long>> obtenerDocumentosSincronizacion(Date fechaDesde,
      int numeroDocumentos);
}
