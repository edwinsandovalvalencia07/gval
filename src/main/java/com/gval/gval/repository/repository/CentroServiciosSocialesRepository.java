package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.CentroServiciosSociales;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The Interface CentroServiciosSocialesRepository.
 */
@Repository
public interface CentroServiciosSocialesRepository
    extends JpaRepository<CentroServiciosSociales, Long> {

  /**
   * Find by activo true and unidades pk unidad.
   *
   * @param unidad the unidad
   * @param by the by
   * @return the centro servicios sociales
   */
  List<CentroServiciosSociales> findByActivoTrueAndUnidadPkUnidad(Long unidad,
      Sort by);
}
