package com.gval.gval.repository.repository.custom;

import java.util.List;

/**
 * The Interface SolicitudRepositoryCustom.
 */
public interface DocumentoDescargaRepositoryCustom {

  List<Object[]> obtenerDocumentoPDF(String codResolu, String codTipoDocu, String orden);
}
