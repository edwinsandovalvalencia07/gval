package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Cuidador;
import com.gval.gval.entity.model.CuidadorInformeSocial;
import com.gval.gval.entity.model.InformeSocial;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * The Interface CuidadorInformeSocialRepository.
 */
@Repository
public interface CuidadorInformeSocialRepository
    extends JpaRepository<CuidadorInformeSocial, Long>,
    ExtendedQuerydslPredicateExecutor<CuidadorInformeSocial> {

  /**
   * Find by activo true and cuidador and informe social.
   *
   * @param cuidador the cuidador
   * @param informeSocial the informe social
   * @return the list
   */
  List<CuidadorInformeSocial> findByCuidadorAndInformeSocialAndActivoTrue(
      Cuidador cuidador, InformeSocial informeSocial);


  /**
   * Find by informe social and activo true.
   *
   * @param informeSocial the informe social
   * @return the list
   */
  List<CuidadorInformeSocial> findByInformeSocialAndActivoTrue(
      InformeSocial informeSocial);

  /**
   * Find by informe social and origen and activo true.
   *
   * @param informeSocial the informe social
   * @param origen the origen
   * @return the list
   */
  List<CuidadorInformeSocial> findByInformeSocialAndOrigenAndActivoTrue(
      InformeSocial informeSocial, String origen);

  /**
   * Find by cuidador pk cuidador.
   *
   * @param pkCuidador the pk cuidador
   * @return the optional
   */
  Optional<CuidadorInformeSocial> findByCuidadorPkCuidador(Long pkCuidador);

}
