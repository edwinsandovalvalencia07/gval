package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.CambioVersion;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * The Interface CambioVersionRepository.
 */
@Repository
public interface CambioVersionRepository
    extends JpaRepository<CambioVersion, Long>,
    ExtendedQuerydslPredicateExecutor<CambioVersion> {


}
