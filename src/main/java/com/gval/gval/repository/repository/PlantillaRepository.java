package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Plantilla;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * The Interface PlantillaRepository.
 */
@Repository
public interface PlantillaRepository extends JpaRepository<Plantilla, Long>,
    ExtendedQuerydslPredicateExecutor<Plantilla> {

  /**
   * Find by tipo documento pk tipo documento and activo true.
   *
   * @param pkTipoDocumento the pk tipo documento
   * @return the optional
   */
  Optional<Plantilla> findByTipoDocumentoPkTipoDocumentoAndActivoTrue(
      Long pkTipoDocumento);

}
