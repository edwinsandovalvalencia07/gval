package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Expediente;
import com.gval.gval.entity.model.Tercero;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * The Interface TerceroRepository.
 */
@Repository
public interface TerceroRepository extends JpaRepository<Tercero, Long>,
    ExtendedQuerydslPredicateExecutor<Tercero> {

  /**
   * Find by persona pk persona and activo true.
   *
   * @param pkPersona the pk persona
   * @return the list
   */
  List<Tercero> findByPersonaPkPersonaAndActivoTrue(Long pkPersona);

  /**
   * Find by persona pk persona and pk expediente and activo true.
   *
   * @param pkPersona the pk persona
   * @param pkExpedien the pk expedien
   * @return the list
   */
  List<Tercero> findByPersonaPkPersonaAndExpedientePkExpedienAndActivoTrue(
      Long pkPersona, Long pkExpedien);

  /**
   * Find by pk terceros and activo true.
   *
   * @param pkTerceros the pk terceros
   * @return the optional
   */
  Optional<Tercero> findByPkTercerosAndActivoTrue(Long pkTerceros);

  /**
   * Find by expediente.
   *
   * @param expediente the expediente
   * @return the list
   */
  List<Tercero> findByExpedienteOrderByActivoDescPkTercerosDesc(
      Expediente expediente);


}
