package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.VersionAda;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The Interface VersionAdaRepository.
 */
@Repository
public interface VersionAdaRepository extends JpaRepository<VersionAda, Long>,
    ExtendedQuerydslPredicateExecutor<VersionAda> {



  /**
   * Find by activo true and cambios version activo true.
   *
   * @param by the by
   * @return the list
   */
  List<VersionAda> findByActivoTrue(Sort by);
}
