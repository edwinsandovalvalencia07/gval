package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.DetalleTramiteSolicitud;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * The Interface DetalleTramiteSolicitudRepository.
 */
@Repository
public interface DetalleTramiteSolicitudRepository
    extends JpaRepository<DetalleTramiteSolicitud, Long>,
    ExtendedQuerydslPredicateExecutor<DetalleTramiteSolicitud> {

  /**
   * Find by pk tramite solicitud.
   *
   * @param aLong the a long
   * @return the optional
   */
  Optional<DetalleTramiteSolicitud> findByPkTramiteSolicitud(Long aLong);
}
