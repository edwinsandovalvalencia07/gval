package com.gval.gval.repository.repository;

import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;
import com.gval.gval.entity.vistas.model.PiaResueltoListado;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The Interface PiaResueltoListadoRepository.
 */
@Repository
public interface PiaResueltoListadoRepository
    extends JpaRepository<PiaResueltoListado, Long>,
    ExtendedQuerydslPredicateExecutor<PiaResueltoListado> {

  /**
   * Find by pk expedien.
   *
   * @param pkExpedien the pk expedien
   * @param orden the orden
   * @return the list
   */
  List<PiaResueltoListado> findByPkExpedien(Long pkExpedien, Sort orden);

}
