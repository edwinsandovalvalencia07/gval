package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.SituacionLaboral;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * The Interface SituacionLaboralRepository.
 */
@Repository
public interface SituacionLaboralRepository
    extends JpaRepository<SituacionLaboral, Long>,
    ExtendedQuerydslPredicateExecutor<SituacionLaboral> {

}
