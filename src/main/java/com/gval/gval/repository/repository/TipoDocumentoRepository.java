package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.TipoDocumento;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

// TODO: Auto-generated Javadoc
/**
 * Repositorio de consultas para Tipo de Documento.
 *
 * @author Indra
 */
@Repository
public interface TipoDocumentoRepository
    extends JpaRepository<TipoDocumento, Long>,
    ExtendedQuerydslPredicateExecutor<TipoDocumento> {


  /**
   * Find by codigo.
   *
   * @param codigo the codigo
   * @return the optional
   */
  Optional<TipoDocumento> findByCodigo(String codigo);

  /**
   * Find by codigo and activo true.
   *
   * @param codigo the codigo
   * @return the optional
   */
  Optional<TipoDocumento> findByCodigoAndActivoTrue(String codigo);
  // @formatter:off
  /**
   * Find tipo documentos.
   *
   * Cipi (:esEntidadLocal = false)
   *
   * Entidad local no finalizado (:esEntidadLocal = true and
   * :solicitudFinalizada = false and td.aportaAyuntamiento = true)
   *
   * Entidad local finalizado (:esEntidadLocal = true and :solicitudFinalizada =
   * true and td.aportaAyuntamiento = true and td.aportaAyuntamientoFinalizado =
   * true)
   *
   * @param esEntidadLocal the es entidad local
   * @param solicitudFinalizada the solicitud finalizada
   * @param filtroTiposDocumentos the filtro tipos documentos
   * @return the list
   */
  @Query(" select td from TipoDocumento td "
      + " where td.activo = true and tipo = 'A' " + "  and ("
      + "  (:esEntidadLocal = false) or"
      + "  (:esEntidadLocal = true and :solicitudFinalizada = false and td.aportaAyuntamiento = true) or"
      + "  (:esEntidadLocal = true and :solicitudFinalizada = true  and td.aportaAyuntamiento = true and td.aportaAyuntamientoFinalizado = true))"
      + "  and td.codigo not in :filtroTiposDocumentos"
      + " order by td.nombre asc")
  // @formatter:on
  List<TipoDocumento> findTipoDocumentosAportables(
      @Param("esEntidadLocal") boolean esEntidadLocal,
      @Param("solicitudFinalizada") boolean solicitudFinalizada,
      @Param("filtroTiposDocumentos") List<String> filtroTiposDocumentos);

  /**
   * Find by codigo in.
   *
   * @param codigos the codigos
   * @param sort the sort
   * @return the list
   */
  List<TipoDocumento> findByCodigoIn(List<String> codigos, Sort sort);

  /**
   * Find by nombre.
   *
   * @param nombre the nombre
   * @return the optional
   */
  Optional<TipoDocumento> findByNombre(String nombre);

  /**
   * Obtener tipodocu resolucion.
   *
   * @param pkResoluci the pk resoluci
   * @return the long
   */
  @Query(value = "SELECT OBTENER_TIPODOCU_RESOLUCION(:pkResoluci) FROM DUAL",
      nativeQuery = true)
  Long obtenerTipodocuResolucion(@Param("pkResoluci") Long pkResoluci);

  /**
   * Doc no aportables.
   *
   * @return the list
   */
  @Query(value = "select * from sdx_tipodocu where TIBLOQAPORTA = 1",
      nativeQuery = true)
  List<TipoDocumento> docNoAportables();

  /**
   * Obtener tipo docu activo por codigo.
   *
   * @param codTipoDocu the cod tipo docu
   * @return the tipo documento
   */
  @Query(value = "select * from sdx_tipodocu where tidactivo = 1 and ticodigo = :codTipoDocu ", nativeQuery = true)
  TipoDocumento obtenerTipoDocuActivoPorCodigo(@Param("codTipoDocu") String codTipoDocu);

}
