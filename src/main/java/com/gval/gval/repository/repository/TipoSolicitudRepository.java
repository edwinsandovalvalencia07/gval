package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.TipoSolicitud;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The Interface TipoSolicitudRepository.
 */
@Repository
public interface TipoSolicitudRepository
    extends JpaRepository<TipoSolicitud, Long>,
    ExtendedQuerydslPredicateExecutor<TipoSolicitud> {

  /**
   * Find by codigo and activo true.
   *
   * @param codigo the codigo
   * @return the tipo solicitud
   */
  TipoSolicitud findByCodigoAndActivoTrue(String codigo);
}
