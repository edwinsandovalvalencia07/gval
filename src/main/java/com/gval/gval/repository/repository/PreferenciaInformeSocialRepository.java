package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Expediente;
import com.gval.gval.entity.model.InformeSocial;
import com.gval.gval.entity.model.PreferenciaInformeSocial;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


/**
 * The Interface PreferenciaInformeSocialRepository.
 */
public interface PreferenciaInformeSocialRepository
    extends JpaRepository<PreferenciaInformeSocial, Long>,
    ExtendedQuerydslPredicateExecutor<PreferenciaInformeSocial> {

  /**
   * Delete by informe social.
   *
   * @param informeSocial the informe social
   */
  void deleteByInformeSocial(InformeSocial informeSocial);


  /**
   * Find by preferencia catalogo servicio pk pref cataserv.
   *
   * @param pkPrefCataserv the pk pref cataserv
   * @return the list
   */
  List<PreferenciaInformeSocial> findByPreferenciaCatalogoServicioPkPrefCataserv(
      Long pkPrefCataserv);

  /**
   * Habilitar pestanya informes sociales.
   *
   * @param expediente the expediente
   * @return true, if successful
   */
  @Query("select case when (count(s.pkSolicitud) > 0) then true else false end"
      + " from Solicitud s, EstadoSolicitud esol"
      + " where s.pkSolicitud = esol.solicitud"
      + " and s.activo = 1 and esol.activo = 1"
      + " and (esol.estado > 3 or (esol.estado = 3 and NOT (s.bloqueada = 1 and s.pendienteDocumentacion = 1)))"
      + " and s.expediente = :expediente")
  boolean habilitarPestanyaInformesSociales(
      @Param("expediente") Expediente expediente);


  /**
   * Obtener preferencias por info social.
   *
   * @param pkInfosoci the pk infosoci
   * @return the list
   */
  @Query(value = "SELECT PREFEREN.* FROM SDM_PREFEREN PREFEREN "
      + "       WHERE preferen.pk_infosoci = :pkInfosoci"
      + "       AND preferen.prprefval = 2"
      + "       AND preferen.pk_cataserv = 2", nativeQuery = true)
  List<PreferenciaInformeSocial> obtenerPreferenciasPorInfoSocial(
      @Param("pkInfosoci") Long pkInfosoci);

}
