package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.EstadoDocumento;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The Interface EstadoDocumentoRepository.
 */
@Repository
public interface EstadoDocumentoRepository
    extends JpaRepository<EstadoDocumento, Long>,
    ExtendedQuerydslPredicateExecutor<EstadoDocumento> {

  /**
   * Find by codigo.
   *
   * @param codigo the codigo
   * @return the estadoDocumento DTO
   */
  EstadoDocumento findByCodigoAndActivoTrue(String codigo);

  /**
   * Find by codigo.
   *
   * @param pkEstadoDocumento the pk estado documento
   * @return the estadoDocumento DTO
   */
  EstadoDocumento findByPkEstadoDocumento(Long pkEstadoDocumento);

}
