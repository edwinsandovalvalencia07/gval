package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Ejercicio;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Interface EjercicioRepository.
 */
@Repository
public interface EjercicioRepository extends JpaRepository<Ejercicio, Long>,
    ExtendedQuerydslPredicateExecutor<Ejercicio> {

  /**
   * Tiene capacidad economica.
   *
   * @param solicitante the solicitante
   * @return the boolean
   */
  @Query("SELECT CASE WHEN count(*) > 0 THEN true ELSE false END "
      + "FROM CapacidadEconomica ca "
      + "WHERE ca.tipoCapacidad = 'S' AND ca.persona.pkPersona = :pk_persona "
      + "AND ca.ejercicio.cerrado = 0 AND ca.validadoUnidadFamiliar = 1 ")
  Boolean tieneCapacidadEconomica(@Param("pk_persona") final Long solicitante);


  /**
   * Tiene documentacion circuito verificacion.
   *
   * @param pkSolicitud the pk solicitud
   * @return the boolean
   */
  @Query("SELECT CASE WHEN count(*) > 0 THEN true ELSE false END "
      + "FROM  HistoricoEstadoDocumento ed "
      + "WHERE ed.documento.activo = 1 AND ed.documento.solicitud.pkSolicitud = :pk_solicitud "
			+ " AND ed.activo = 1 AND ed.estadoDocumento.pkEstadoDocumento != 3 AND ed.estadoDocumento.pkEstadoDocumento != 21 ")
  Boolean tieneDocumentacionCircuitoVerificacion(
      @Param("pk_solicitud") final Long pkSolicitud);

  /**
   * Calcular total horas.
   *
   * @param grado the grado
   * @param nivel the nivel
   * @return the big decimal
   */
  @Query("SELECT va.valor FROM VariableEconomica va "
      + "  WHERE va.codigo = 'HSADG' || :grado || 'N' || :nivel and va.ejercicio.cerrado = 0")
  BigDecimal calcularTotalHoras(@Param("grado") String grado,
      @Param("nivel") String nivel);

  /**
   * Calcular total horas excepcionales.
   *
   * @param grado the grado
   * @return the big decimal
   */
  @Query("SELECT va.valor FROM VariableEconomica va "
      + "  WHERE va.codigo = 'HSADG' || :grado || 'EX' and va.ejercicio.cerrado = 0")
  BigDecimal calcularTotalHorasExcepcionales(@Param("grado") String grado);

  /**
   * Obtener porcentaje horas cuidados.
   *
   * @return the big decimal
   */
  @Query("SELECT va.valor FROM VariableEconomica va "
      + "WHERE va.codigo = 'PORCHSAD' AND va.ejercicio.cerrado = 0")
  BigDecimal obtenerPorcentajeHorasCuidados();

  /**
   * Find by cerrado false.
   *
   * @return the ejercicio DTO
   */
  Ejercicio findByCerradoFalse();


  /**
   * Obtener ejercicios anterior A vigente.
   *
   * @param numEjercicios the num ejercicios
   * @return the list
   */
  @Query(
      value = "SELECT * FROM sdx_ejercici where ejanyo >= (extract(year from sysdate) - :numEjercicios) and ejcerrado = 1",
      nativeQuery = true)
  List<Ejercicio> obtenerEjerciciosAnteriorAVigente(
      @Param("numEjercicios") Long numEjercicios);


}
