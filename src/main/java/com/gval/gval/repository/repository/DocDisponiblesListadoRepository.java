package com.gval.gval.repository.repository;

import com.gval.gval.entity.vistas.model.DisponiblesListado;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * The Interface DocDisponiblesListadoRepository.
 */
@Repository
public interface DocDisponiblesListadoRepository extends JpaRepository<DisponiblesListado,Long> {
	
	/**
	 * Obtener doc disponibles.
	 *
	 * @param resoluciones the resoluciones
	 * @return the list
	 */
	@Query(value = " SELECT distinct pk_tipodocu,pk_resoluci,  TINOMBRE ||'. (' ||count(*) over ( partition by pk_tipodocu)||' documentos)' tinombre from ("
			+ "  select * from SDV_DOC_DISPONIBLES a where a.pk_resoluci IN :resoluciones"
			+ "  union all "
			+ "  select * from SDV_DOC_DISPONIBLES2 b where b.pk_resoluci IN :resoluciones"
			+ "  UNION ALL "
			+ "  select * from SDV_DOC_DISPONIBLES3 c where c.pk_resoluci IN :resoluciones"
			+ "  UNION ALL "
			+ "  select * from SDV_DOC_DISPONIBLES4 d where d.pk_resoluci IN :resoluciones"
			+ "  UNION ALL "
			+ "  select * from SDV_DOC_DISPONIBLES5 e where e.pk_resoluci IN :resoluciones )", nativeQuery = true)	
	List<DisponiblesListado> obtenerDocDisponibles(@Param("resoluciones") List<Integer>  resoluciones);
	
}
