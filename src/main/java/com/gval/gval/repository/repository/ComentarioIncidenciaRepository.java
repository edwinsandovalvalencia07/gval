package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.ComentarioIncidencia;
import com.gval.gval.entity.model.Incidencia;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The Interface ComentarioIncidenciaRepository.
 */
@Repository
public interface ComentarioIncidenciaRepository
    extends JpaRepository<ComentarioIncidencia, Long>,
    ExtendedQuerydslPredicateExecutor<ComentarioIncidencia> {

  /**
   * Find by incidencia and activo true.
   *
   * @param incidencia the incidencia
   * @return the list
   */
  List<ComentarioIncidencia> findByIncidenciaAndActivoTrue(
      Incidencia incidencia);

}
