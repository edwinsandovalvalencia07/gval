package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Pia;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Interface PiaRepository.
 */
public interface PiaRepository extends JpaRepository<Pia, Long> {

  /**
   * Obtener ultimo pia serviciode resolucion gy N.
   *
   * @param pkResoluci the pk resoluci
   * @param fechareso the fechareso
   * @return true, if successful
   */
  @Query(value = "select * from (select pia.* from SDM_PIA pia " + " inner join  SDM_RESOLUCI re on pia.PK_RESOLUCI =re.PK_RESOLUCI "
      + " WHERE re.PK_RESOLUCI= :pkResoluci and  pia.PIACTIVO=1 and pia.PIFECHRES= :fechareso and "
      + " ((((pia.PITIPO='TA') or (pia.PITIPO='AD')) or ((pia.PITIPO='CD') or (pia.PITIPO='CN'))) or (pia.PITIPO='AR')) order by pia.PK_PIA desc) where rownum = 1", nativeQuery = true)
  boolean obtenerUltimoPiaServiciodeResolucionGyN(@Param("pkResoluci") Long pkResoluci, @Param("fechareso") Date fechareso);

  /**
   * Obtener prestacion pia recurrida.
   *
   * @param pkResoluci2 the pk resoluci 2
   * @return the pia
   */
  @Query(value = "SELECT * "
      + "FROM sdm_pia "
      + "WHERE PK_RESOLUCI2 = :pkResoluci2 ", nativeQuery = true)
  Pia obtenerPrestacionPiaRecurrida(@Param("pkResoluci2") Long pkResoluci2);

  /**
   * Obtenerobtener pia de resolucion de correcion pia.
   *
   * @param pkResoluci2 the pk resoluci 2
   * @return the pia
   */
  @Query(value = "SELECT * "
      + "FROM sdm_pia "
      + "WHERE PK_RESOLUCI2 = :pkResoluci2 ", nativeQuery = true)
  Pia obtenerobtenerPiaDeResolucionDeCorrecionPia(@Param("pkResoluci2") Long pkResoluci2);
}
