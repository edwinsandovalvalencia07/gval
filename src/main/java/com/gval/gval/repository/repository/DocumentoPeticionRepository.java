package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.DocumentoPeticion;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The Interface DocumentoPeticionRepository.
 */
@Repository
public interface DocumentoPeticionRepository
    extends JpaRepository<DocumentoPeticion, Long>,
    ExtendedQuerydslPredicateExecutor<DocumentoPeticion> {

}
