package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.AsistenteInformeSocial;
import com.gval.gval.entity.model.AsistentePersonal;
import com.gval.gval.entity.model.InformeSocial;
//import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


/**
 * The Interface AsistenteInformeSocialRepository.
 */
public interface AsistenteInformeSocialRepository
    extends JpaRepository<AsistenteInformeSocial, Long>,
        ExtendedQuerydslPredicateExecutor<AsistenteInformeSocial> {

  /**
   * Find by activo true and asistente and informe social.
   *
   * @param asistente the asistente
   * @param informeSocial the informe social
   * @return the list
   */
  List<AsistenteInformeSocial> findByAsistenteAndInformeSocialAndActivoTrue(
      AsistentePersonal asistente, InformeSocial informeSocial);


  /**
   * Find by informe social and activo true.
   *
   * @param informeSocial the informe social
   * @return the list
   */
  List<AsistenteInformeSocial> findByInformeSocialAndActivoTrue(
      InformeSocial informeSocial);

  /**
   * Find by informe social and origen and activo true.
   *
   * @param informeSocial the informe social
   * @param origen the origen
   * @return the list
   */
  List<AsistenteInformeSocial> findByInformeSocialAndOrigenAndActivoTrue(
      InformeSocial informeSocial, String origen);

  /**
   * Find by asistente pk asispers.
   *
   * @param pkAsispers the pk asispers
   * @return the optional
   */
  Optional<AsistenteInformeSocial> findByAsistentePkAsispers(Long pkAsispers);
}
