package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.DireccionAda;
import com.gval.gval.entity.model.Persona;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * The Interface DireccionRepository.
 */
@Repository
public interface DireccionRepository extends JpaRepository<DireccionAda, Long>,
    ExtendedQuerydslPredicateExecutor<DireccionAda> {

  /**
   * Find bypk direccion and activo true.
   *
   * @param pkDireccion the pk direccion
   * @return the optional
   */
  Optional<DireccionAda> findBypkDireccionAndActivoTrue(Long pkDireccion);


  /**
   * Find by persona ada pk persona and habitual anterior true and activo true.
   *
   * @param pkPersona the pk persona
   * @return the optional
   */
  Optional<DireccionAda> findByPersonaAdaPkPersonaAndHabitualAnteriorTrueAndActivoTrue(
      Long pkPersona);

  /**
   * Find by persona ada pk persona and habitual true and activo true.
   *
   * @param pkPersona the pk persona
   * @return the optional
   */
  Optional<DireccionAda> findByPersonaAdaPkPersonaAndHabitualTrueAndActivoTrue(
      Long pkPersona);

  /**
   * Find by persona ada pk persona and esOtraCCAA true and activo true.
   *
   * @param pkPersona the pk persona
   * @return the optional
   */
  List<DireccionAda> findByPersonaAdaPkPersonaAndEsOtraCCAATrueAndActivoTrueOrderByPkDireccionDesc(
      Long pkPersona);

  /**
   * Find by persona ada pk persona and notificacion true and activo true.
   *
   * @param pkPersona the pk persona
   * @return the optional
   */
  Optional<DireccionAda> findByPersonaAdaPkPersonaAndNotificacionTrueAndActivoTrue(
      Long pkPersona);

  /**
   * Find by persona ada.
   *
   * @param persona the persona
   * @return the list
   */
  List<DireccionAda> findByPersonaAda(Persona persona);

  /**
   * Find by persona ada pk persona and activo true.
   *
   * @param pkPersona the pk persona
   * @param sort the sort
   * @return the list
   */
  List<DireccionAda> findByPersonaAdaPkPersonaAndActivoTrue(Long pkPersona,
      Sort sort);

  /**
   * Find all.
   *
   * @return the list
   */
  @Override
  List<DireccionAda> findAll();


  /**
   * Find first by persona ada pk persona and activo true order by pk direccion
   * desc.
   *
   * @param pkPersona the pk persona
   * @return the optional
   */
  Optional<DireccionAda> findFirstByPersonaAdaPkPersonaAndActivoTrueOrderByPkDireccionDesc(
      Long pkPersona);

}
