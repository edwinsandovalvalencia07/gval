package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Documento;
import com.gval.gval.entity.model.Expediente;
import com.gval.gval.entity.model.Preferencia;
import com.gval.gval.entity.model.Solicitud;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * The Interface PreferenciaRepository.
 */
@Repository
public interface PreferenciaRepository extends JpaRepository<Preferencia, Long>,
    ExtendedQuerydslPredicateExecutor<Preferencia> {

  /**
   * Find by solicitud in.
   *
   * @param solicitud the solicitud
   * @param sort the sort
   * @return the list
   */
  List<Preferencia> findBySolicitudIn(List<Solicitud> solicitud, Sort sort);

  /**
   * Find by solicitud in.
   *
   * @param solicitud the solicitud
   * @param sort the sort
   * @return the list
   */
  List<Preferencia> findBySolicitudIn(Solicitud solicitud, Sort sort);

  /**
   * Find by solicitud and estado false.
   *
   * @param solicitud the solicitud
   * @return the list
   */
  List<Preferencia> findBySolicitudAndEstadoFalse(Solicitud solicitud);

  /**
   * Find by documento.
   *
   * @param documento the documento
   * @return the preferencia
   */
  Preferencia findByDocumento(Documento documento);

  /**
   * Find first by solicitud and estado false and documento is null order by pk
   * preferencia desc.
   *
   * @param entity the entity
   * @return the optional
   */
  Optional<Preferencia> findFirstBySolicitudAndEstadoFalseAndDocumentoIsNullOrderByPkPrefsolDesc(
      Solicitud entity);

  /**
   * Obtener preferencias abiertas expediente.
   *
   * @param pkExpediente the pk expediente
   * @return the long
   */
  @Query(
      value = " select OBTENER_PREFERENCIAS_ABIERTAS(:pkExpediente) from dual ",
      nativeQuery = true)
  Long obtenerPreferenciasAbiertasExpediente(
      @Param("pkExpediente") Long pkExpediente);

  /**
   * Obtener ultima preferencia por catalogo servicio.
   *
   * @param expediente the expediente
   * @param codigoCataServ the codigo cata serv
   * @return the list
   */
  @Query(" select ps "
      + " from Preferencia ps, PreferenciaCatalogoServicio as pc "
      + " inner join ps.solicitud as s "
      + " inner join pc.catalogoServicio as c "
      + " where ps.pkPrefsol = pc.preferencia and c.codigo = :codigoCataServ and s.expediente = :expediente  "
      + " order by ps.estado asc, ps.fechaPeticion desc NULLS LAST")
  List<Preferencia> obtenerPreferenciasPorCatalogoServicio(
      @Param("expediente") Expediente expediente,
      @Param("codigoCataServ") String codigoCataServ);

  /**
   * Find first by solicitud in.
   *
   * @param solicitud the solicitud
   * @param sort the sort
   * @return the optional
   */
  Optional<Preferencia> findFirstBySolicitudIn(Solicitud solicitud, Sort sort);

  /**
   * Find by solicitud expediente pk expedien and estado false.
   *
   * @param pkExpedien the pk expedien
   * @return the list
   */
  List<Preferencia> findBySolicitudExpedientePkExpedienAndEstadoFalse(
      Long pkExpedien);

  /**
   * Tiene preferencias cerradas para abrir.
   *
   * @param pkExpediente the pk expediente
   * @return the long
   */
  @Query(value = "select OBTENER_PREF_CERR_PARA_ABR(:pkExpediente) from dual ",
      nativeQuery = true)
  Long tienePreferenciasCerradasParaAbrir(
      @Param("pkExpediente") Long pkExpediente);

  /**
   * Tiene preferencias duplicadas.
   *
   * @param pkExpedien the pk expedien
   * @param pkPrefsol the pk prefsol
   * @return the string
   */
  @Query(
      value = "select es_preferencia_duplicada(:pkExpedien, :pkPrefsol) from dual ",
      nativeQuery = true)
  String tienePreferenciasDuplicadas(@Param("pkExpedien") Long pkExpedien,
      @Param("pkPrefsol") Long pkPrefsol);
}
