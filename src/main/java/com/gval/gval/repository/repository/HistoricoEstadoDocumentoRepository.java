package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.HistoricoEstadoDocumento;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * The Interface HistoricoEstadoDocumentoRepository.
 */
@Repository
public interface HistoricoEstadoDocumentoRepository
    extends JpaRepository<HistoricoEstadoDocumento, Long>,
    ExtendedQuerydslPredicateExecutor<HistoricoEstadoDocumento> {

  /**
   * Find by documento pk documento equals and activo true.
   *
   * @param pkDocumento the pk documento
   * @return the historico estado documento
   */
  Optional<HistoricoEstadoDocumento> findByDocumentoPkDocumentoEqualsAndActivoTrue(
      Long pkDocumento);

  /**
   * Find first by documento pk documento equals.
   *
   * @param pkDocumento the pk documento
   * @param by the by
   * @return the optional
   */
  Optional<HistoricoEstadoDocumento> findFirstByDocumentoPkDocumentoEquals(
      Long pkDocumento, Sort by);

  /**
   * Find fisrt by documento pk documento equals and activo false.
   *
   * @param pkDocumento the pk documento
   * @param by the by
   * @return the optional
   */
  Optional<HistoricoEstadoDocumento> findFirstByDocumentoPkDocumentoEqualsAndActivoFalse(
      Long pkDocumento, Sort by);

}
