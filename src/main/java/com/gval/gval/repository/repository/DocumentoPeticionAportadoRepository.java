package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.DocumentoPeticionAportado;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * The Interface DocumentoPeticionAportadoRepository.
 */
@Repository
public interface DocumentoPeticionAportadoRepository
    extends JpaRepository<DocumentoPeticionAportado, Long>,
    ExtendedQuerydslPredicateExecutor<DocumentoPeticionAportado> {

}
