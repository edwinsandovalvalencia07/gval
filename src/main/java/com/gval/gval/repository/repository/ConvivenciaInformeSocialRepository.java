package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.ConvivenciaInformeSocial;
import com.gval.gval.entity.model.InformeSocial;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * The Interface ConvivenciaInformeSocialRepository.
 */
@Repository
public interface ConvivenciaInformeSocialRepository
    extends JpaRepository<ConvivenciaInformeSocial, Long>,
    ExtendedQuerydslPredicateExecutor<ConvivenciaInformeSocial> {

    /**
     * Find by informe social.
     *
     * @param informeSocial the informe social
     * @return the list
     */
    List<ConvivenciaInformeSocial> findByInformeSocial(InformeSocial informeSocial);
    
}
