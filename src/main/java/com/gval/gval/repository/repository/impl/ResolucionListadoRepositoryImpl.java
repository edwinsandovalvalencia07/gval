package com.gval.gval.repository.repository.impl;



import com.gval.gval.common.UtilidadesCommons;
import com.gval.gval.dto.dto.*;
import com.gval.gval.repository.repository.custom.ResolucionListadoRepositoryCustom;
import jakarta.persistence.EntityManager;
import jakarta.persistence.ParameterMode;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.StoredProcedureQuery;
import org.apache.commons.lang3.BooleanUtils;

import java.util.Date;



/**
 * The Class ResolucionListadoRepositoryImpl.
 */
public class ResolucionListadoRepositoryImpl
    implements ResolucionListadoRepositoryCustom {


  /** The Constant RESUELTA_PIA_NOTIFICADA. */
  private static final String RESUELTA_PIA_NOTIFICADA =
      "RESUELTA_PIA_NOTIFICADA";

  /** The Constant MARCHA_ATRAS. */
  private static final String MARCHA_ATRAS = "MARCHA_ATRAS";

  /** The Constant REVOCAR_RECURSO_PIA. */
  private static final String REVOCAR_RECURSO_PIA = "REVOCAR_RECURSO_PIA";

  /** The entity manager. */
  @PersistenceContext
  EntityManager entityManager;


  /**
   * Guardar resolucion pia notificada.
   *
   * @param expediente the expediente
   * @param documentoGen the documento gen
   * @param resolucion the resolucion
   * @param usuario the usuario
   * @return the string
   */
  @Override
  public Long guardarResolucionPiaNotificada(final ExpedienteDTO expediente,
                                             final DocumentoGeneradoDTO documentoGen,
                                             final ResolucionListadoDTO resolucion, final UsuarioDTO usuario) {
    final int pkExpedienParam = 1;
    final int pkPersonaParam = 2;
    final int fechaNotiParam = 3;
    final int fechaFirmaParam = 4;
    final int observacionesParam = 5;
    final int fechaRegistroParam = 6;
    final int descRegistroParam = 7;
    final int esPiaParam = 8;
    final int nombreDocuParam = 9;
    final int rutaDocuParam = 10;
    final int numRegParam = 11;
    final int uuIdParam = 12;
    final int pkSolicitParam = 13;
    final int pkDocuParam = 14;
    final int pkResoluciParam = 15;
    final int pkPiaParam = 16;
    final int pkTipoDocuParam = 17;
    final int pkTipodocuCentroParam = 18;


    final StoredProcedureQuery spq =
        entityManager.createStoredProcedureQuery(RESUELTA_PIA_NOTIFICADA);
    spq.registerStoredProcedureParameter(pkExpedienParam, Long.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(pkPersonaParam, Long.class,
        ParameterMode.IN);

    spq.registerStoredProcedureParameter(fechaNotiParam, Date.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(fechaFirmaParam, Date.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(observacionesParam, String.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(fechaRegistroParam, Date.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(descRegistroParam, String.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(esPiaParam, Boolean.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(nombreDocuParam, String.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(rutaDocuParam, String.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(numRegParam, String.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(uuIdParam, String.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(pkSolicitParam, Long.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(pkDocuParam, Long.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(pkResoluciParam, Long.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(pkPiaParam, Long.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(pkTipoDocuParam, Long.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(pkTipodocuCentroParam, Long.class,
        ParameterMode.OUT);


    spq.setParameter(pkExpedienParam, resolucion.getPkExpedien());
    spq.setParameter(pkPersonaParam, usuario.getPkPersona());

    spq.setParameter(fechaNotiParam, resolucion.getFechaNotificacion());
    spq.setParameter(fechaFirmaParam, resolucion.getFechaFirma());
    spq.setParameter(observacionesParam,
        UtilidadesCommons.nvlDefect(documentoGen.getObservaciones()));
    spq.setParameter(fechaRegistroParam,
        documentoGen.getDocumento().getFechaRegistro());
    spq.setParameter(descRegistroParam, UtilidadesCommons
        .nvlDefect(documentoGen.getDocumento().getDescripcionRegistro()));
    spq.setParameter(esPiaParam,
        BooleanUtils.isTrue(resolucion.getPkPia() != null));
    spq.setParameter(nombreDocuParam, documentoGen.getDocumento().getNombre());
    spq.setParameter(rutaDocuParam,
        new String(documentoGen.getDocumento().getFichero()));
    spq.setParameter(numRegParam,
        documentoGen.getDocumento().getNumeroRegistro());
    spq.setParameter(uuIdParam,
        UtilidadesCommons.nvlDefect(documentoGen.getDocumento().getUuid()));
    spq.setParameter(pkSolicitParam, resolucion.getPkSolicitud());
    spq.setParameter(pkDocuParam,
        resolucion.getPkDocu() != null ? resolucion.getPkDocu() : 0L);
    spq.setParameter(pkResoluciParam, resolucion.getPkResolucion());
    spq.setParameter(pkPiaParam,
        resolucion.getPkPia() != null ? resolucion.getPkPia() : 0L);
    spq.setParameter(pkTipoDocuParam,
        documentoGen.getDocumento().getTipoDocumento().getPkTipoDocumento());

    spq.execute();

    return (Long) spq.getOutputParameterValue(pkTipodocuCentroParam);

  }

  /**
   * Marcha atras resolucion notificada.
   *
   * @param pkResoluci the pk resoluci
   * @param pkSolicit the pk solicit
   * @param pkPersona the pk persona
   * @param esPia the es pia
   * @param enviadoNsisaad the enviado nsisaad
   * @param firmadoLote the firmado lote
   * @param documento the documento
   * @return the string
   */
  @Override
  public void marchaAtrasResolucionNotificada(final Long pkResoluci,
      final Long pkSolicit, final Long pkPersona, final Boolean esPia,
      final Boolean enviadoNsisaad, final Boolean firmadoLote,
      final DocumentoDTO documento) {

    final int pkResoluciParam = 1;
    final int pkSolicitParam = 2;
    final int pkPersonaParam = 3;
    final int pkDocuParam = 4;
    final int esPiaParam = 5;
    final int enviadoNsisaadParam = 6;
    final int firmadoLoteParam = 7;

    final StoredProcedureQuery spq =
        entityManager.createStoredProcedureQuery(MARCHA_ATRAS);
    spq.registerStoredProcedureParameter(pkResoluciParam, Long.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(pkSolicitParam, Long.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(pkPersonaParam, Long.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(pkDocuParam, Long.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(esPiaParam, Boolean.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(enviadoNsisaadParam, Boolean.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(firmadoLoteParam, Boolean.class,
        ParameterMode.IN);

    spq.setParameter(pkResoluciParam, pkResoluci);
    spq.setParameter(pkSolicitParam, pkSolicit);
    spq.setParameter(pkPersonaParam, pkPersona);
    spq.setParameter(pkDocuParam, documento.getPkDocumento());
    spq.setParameter(esPiaParam, esPia);
    spq.setParameter(enviadoNsisaadParam, enviadoNsisaad);
    spq.setParameter(firmadoLoteParam, firmadoLote);

    spq.execute();
  }

  /**
   * Revocar resolucion.
   *
   * @param codigoSolicitud the codigo solicitud
   * @param pkUsuario the pk usuario
   * @param resolucionDTO the resolucion DTO
   * @param pkResolucionARevocar the pk resolucion A revocar
   */
  @Override
  public void revocarResolucion(final String codigoSolicitud,
      final Long pkUsuario, final ResolucionDTO resolucionDTO,
      final Long pkResolucionARevocar) {

    final int codigoSolicitudParam = 1;
    final int pkusuarioParam = 2;
    final int pkResolucionARevocarParam = 3;
    final int fechaResolucionParam = 4;
    final int fechaEfectoParam = 5;
    final int tipoResoParam = 6;

    final StoredProcedureQuery spq =
        entityManager.createStoredProcedureQuery(REVOCAR_RECURSO_PIA);
    spq.registerStoredProcedureParameter(codigoSolicitudParam, String.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(pkusuarioParam, Long.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(pkResolucionARevocarParam, Long.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(fechaResolucionParam, Date.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(fechaEfectoParam, Date.class,
        ParameterMode.IN);
    spq.registerStoredProcedureParameter(tipoResoParam, String.class,
        ParameterMode.IN);

    spq.setParameter(codigoSolicitudParam, codigoSolicitud);
    spq.setParameter(pkusuarioParam, pkUsuario);
    spq.setParameter(pkResolucionARevocarParam, pkResolucionARevocar);
    spq.setParameter(fechaResolucionParam, resolucionDTO.getFechaResolucion());
    spq.setParameter(fechaEfectoParam, resolucionDTO.getFechaEfecto());
    spq.setParameter(tipoResoParam, resolucionDTO.getTipo());

    spq.execute();
  }

}
