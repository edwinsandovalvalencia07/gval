package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.TipoResolucion;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repositorio de consultas para Tipo de Resolucion.
 *
 * @author Indra
 */
@Repository
public interface TipoResolucionRepository
    extends JpaRepository<TipoResolucion, Long>,
    ExtendedQuerydslPredicateExecutor<TipoResolucion> {

  /**
   * Obtiene los tipos de resoluciones
   *
   * que pueden ser creados en ADA3.
   *
   * @return the long
   */
  @Query("select tr from TipoResolucion tr "
      + " where tr.activo = 1 and tr.manual = 1  and tr.esADA = 1 "
      + " order by tr.descripcion asc")
  List<TipoResolucion> obtenerTipoResolucionesManuales();

  /**
   * Obtener resoluciones.
   *
   * @return the list
   */
  @Query("select tr from TipoResolucion tr "
          + "WHERE tr.activo = 1 order by tr.descripcion asc ")
  List<TipoResolucion> obtenerResoluciones();

  /**
   * Obtener documentos disponibles.
   *
   * @param pkTipoResolucis the pk tipo resolucis
   * @return the list
   */
  @Query(
      value = "SELECT do.PK_TIPODOCU as pkTipoResolucion, td.tinombre as descripcion, td.*"
          + "    FROM ADA.sdm_resodocu rd "
          + "    INNER JOIN ADA.sdm_docu do ON do.pk_docu = rd.pk_docu AND do.Docactivo = 1 AND do.dofichero IS NOT NULL "
          + "    INNER JOIN ADA.Sdm_Solicit so ON so.Pk_Solicit = do.Pk_Solicit AND so.soactivo = 1 "
          + "    INNER JOIN ADA.sdm_expedien ex ON ex.pk_expedien = so.pk_expedien AND ex.exactivo = 1 "
          + "    INNER JOIN ADA.sdm_persona per ON per.pk_persona = ex.pk_persona AND ex.EXACTIVO = 1 "
          + "    INNER JOIN sdx_tipodocu td ON td.pk_tipodocu = do.pk_tipodocu "
          + "    WHERE rd.pk_resoluci IN (:pkTipoReso)",
      nativeQuery = true)
  List<TipoResolucion> obtenerDocumentosDisponibles(
      @Param("pkTipoReso") int pkTipoResolucis);

  /**
   * Obtiene una lista de TipoResolucion filtrada según el estado de actividad y el tipo.
   *
   * @param filtro - Si es 0, la consulta ignora la condición sobre TIMANUAL. - Si es 1, la consulta incluye solo las filas donde TIMANUAL = 1. La consulta siempre incluye filas donde TIACTIVO = 1 y excluye filas donde PK_TIPORESO es 'DSG' o 'DSP'.
   *
   * @return Lista de TipoResolucion ordenada por PK_TIPORESO ascendente.
   */
  @Query(value = "SELECT * FROM SDM_TIPORESO "
      + "WHERE TIACTIVO = 1 "
      + "AND NOT (PK_TIPORESO = 'DSG' OR PK_TIPORESO = 'DSP') "
      + "AND (:filtro = 0 OR (TIMANUAL = 1 AND :filtro = 1)) "
      + "ORDER BY PK_TIPORESO ASC ", nativeQuery = true)
  List<TipoResolucion> obtenerTiporesoFiltro(@Param("filtro") int filtro);

}
