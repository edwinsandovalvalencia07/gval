package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.PiaLlamadas;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The Interface PiaLlamadasRepository.
 */
@Repository
public interface PiaLlamadasRepository
    extends JpaRepository<PiaLlamadas, Long>,
    ExtendedQuerydslPredicateExecutor<PiaLlamadas> {

  /**
   * Find by pk expedien.
   *
   * @param pkpia the pkpia
   * @return the list
   */
  List<PiaLlamadas> findByPkPia(Long pkpia);

  /**
   * Find by pk pia llamada.
   *
   * @param pkPiaLlamada the pk pia llamada
   * @return the list
   */
  List<PiaLlamadas> findByPkPiaLlamada(Long pkPiaLlamada);

}
