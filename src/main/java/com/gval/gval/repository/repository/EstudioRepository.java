package com.gval.gval.repository.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gval.gval.entity.model.Estudio;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;


// TODO: Auto-generated Javadoc
/**
 * The Interface EstudioRepository.
 */
@Repository
public interface EstudioRepository extends JpaRepository<Estudio, Long>,
    ExtendedQuerydslPredicateExecutor<Estudio> {

  /**
   * Find by solicitud pk solicitud and tipo and estado activo true.
   *
   * @param pkSolicit the pk solicit
   * @param tipo the tipo
   * @param estado the estado
   * @return the list
   */
  List<Estudio> findBySolicitudPkSolicitudAndTipoAndEstadoAndActivoTrue(
      Long pkSolicit, String tipo, String estado);


  /**
   * Find by solicitud pk solicitud and tipo in and estado and activo true.
   *
   * @param pkSolicit the pk solicit
   * @param tipo the tipo
   * @param estado the estado
   * @return the optional
   */
  Optional<Estudio> findBySolicitudPkSolicitudAndTipoInAndEstadoAndActivoTrue(
      Long pkSolicit, List<String> tipo, String estado);

  /**
   * Find by documento urgencia pk documento and tipo and estado activo true.
   *
   * @param pkDocumento the pk documento
   * @param tipo the tipo
   * @param estado the estado
   * @return the optional
   */
  Optional<Estudio> findByDocumentoUrgenciaPkDocumentoAndTipoAndEstadoAndActivoTrue(
      Long pkDocumento, String tipo, String estado);


  /**
   * Find ultimo estudio con repeticion.
   *
   * @param pkSolicitud the pk solicitud
   * @return the estudio DTO
   */
  @Query(value = "SELECT * FROM (SELECT se.* FROM sdm_estudio se "
      + "INNER JOIN sdm_solicit ss ON ss.pk_solicit = se.pk_solicit AND ss.soactivo = 1 "
      + "WHERE se.esactivo = 1 AND se.esestado = 'C' "
      + "AND se.pk_acta IS NOT NULL "
      + "AND (se.esrepeinsa = 1 OR se.esreinsafi = 1 OR se.esreinsame = 1 OR se.esrepevalo = 1 OR se.esrepeinso = 1) "
      + "AND se.pk_solicit = :pkSolicitud "
      + "ORDER BY esfechapro DESC) where rownum = 1", nativeQuery = true)
  Estudio findUltimoEstudioConRepeticion(
      @Param("pkSolicitud") Long pkSolicitud);


  /**
   * Find by solicitud pk solicitud.
   *
   * @param pkSolicit the pk solicit
   * @return the list
   */
  List<Estudio> findBySolicitudPkSolicitud(Long pkSolicit);


  /**
   * Find estudio solicitud pk solicitud.
   *
   * @param pkSolicit the pk solicit
   * @return the list
   */
  @Query(value = "select * from SDM_ESTUDIO estu " + " where estu.esactivo = 1 and estu.PK_SOLICIT= :pkSolicit " + " order by estu.PK_ESTUDIO ASC ", nativeQuery = true)
  List<Estudio> findEstudioSolicitudPkSolicitud(@Param("pkSolicit") Long pkSolicit);

  /**
   * Find by expediente numero expediente.
   *
   * @param numeroExpediente the numero expediente
   * @return the list
   */
  @Query(value = "SELECT * FROM SDM_ESTUDIO ES "
      + "INNER JOIN SDM_SOLICIT SO ON SO.PK_SOLICIT = ES.PK_SOLICIT "
      + "INNER JOIN SDM_EXPEDIEN EX ON EX.PK_EXPEDIEN = SO.PK_EXPEDIEN "
      + "WHERE EX.EXNUMEXP = :exnumexp", nativeQuery = true)
  List<Estudio> findByExpedienteNumeroExpediente(
      @Param("exnumexp") String numeroExpediente);

  /**
   * Find by solicitud codigo solicitud.
   *
   * @param codigoSolicitud the codigo solicitud
   * @return the list
   */
  @Query(value = "SELECT * FROM SDM_ESTUDIO ES "
      + "INNER JOIN SDM_SOLICIT SO ON SO.PK_SOLICIT = ES.PK_SOLICIT "
      + "INNER JOIN SDM_EXPEDIEN EX ON EX.PK_EXPEDIEN = SO.PK_EXPEDIEN "
      + "WHERE SO.SOCODIGO = :socodigo AND ES.ESACTIVO = 1", nativeQuery = true)
  List<Estudio> findBySolicitudCodigoSolicitud(
      @Param("socodigo") String codigoSolicitud);

  /**
   * Find by pk estudio.
   *
   * @param pkEstudio the pk estudio
   * @return the estudio
   */
  Estudio findByPkEstudio(Long pkEstudio);

  /**
   * Es menor 3 anyos.
   *
   * @param codigoSolicitud the codigo solicitud
   * @return true, if successful
   */
  @Query(value = "SELECT IS_MENOR3ANYOS(:socodigo) FROM DUAL", nativeQuery = true)
  int esMenor3Anyos(@Param("socodigo") String codigoSolicitud);

  /**
   * Existe acta en fecha.
   *
   * @param acFecha the ac fecha
   * @return the int
   */
  @Query(value = "SELECT COUNT(rownum) FROM sdm_acta where acfecha = :acFecha and acactivo = 1", nativeQuery = true)
  int existeActaEnFecha(@Param("acFecha") Date acFecha);


  /**
   * Obtener estudio posterior.
   *
   * @param pkEstudio the pk estudio
   * @param pkSolici the pk solici
   * @param tipo the tipo
   * @return the list
   */
  @Query("SELECT e FROM Estudio e WHERE e.solicitud.pkSolicitud = :pkSolicitud AND e.tipo = :tipo AND e.pkEstudio <> :pkEstudio ORDER BY e.pkEstudio ASC ")
  List<Estudio> obtenerEstudioPosterior(@Param("pkEstudio") Long pkEstudio, @Param("pkSolicitud") Long pkSolici, @Param("tipo") String tipo);



  /**
   * Obtener estudios A aprobar.
   *
   * @param fecha the fecha
   * @return the list
   */
  @Query(value = "select estu.* from SDM_ESTUDIO estu " + "    inner join SDM_SOLICIT soli on estu.PK_SOLICIT=soli.PK_SOLICIT " + "    inner join SDM_EXPEDIEN exp on soli.PK_EXPEDIEN=exp.PK_EXPEDIEN "
      + "    inner join SDM_PERSONA per on exp.PK_PERSONA=per.PK_PERSONA "
      + "    where estu.ESESTADO='C' and estu.PK_ACTA is null and soli.SOACTIVO=1 and (exp.EXTRASSALE=0 or exp.EXTRASSALE is null) and "
      + "    per.PEACTIVO=1 and estu.ESACTIVO=1 and estu.ESFECHAPRO = :fecha ", nativeQuery = true)
  List<Estudio> obtenerEstudiosAAprobar(@Param("fecha") Date fecha);

  /**
   * Tipo modificacion grado.
   *
   * @param pkResolucion the pk resolucion
   * @return the string
   */
  @Query(value = "select RES_GN_TIPO_MODIFICACION_GRADO(:pkResolucion) from dual", nativeQuery = true)
  List<String> tipoModificacionGrado(@Param("pkResolucion") Long pkResolucion);


}
