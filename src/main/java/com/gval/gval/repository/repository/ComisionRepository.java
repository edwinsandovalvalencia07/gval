package com.gval.gval.repository.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gval.gval.entity.model.Comision;

/**
 * The Interface ComisionRepository.
 */
@Repository
public interface ComisionRepository extends JpaRepository<Comision, Long> {

}
