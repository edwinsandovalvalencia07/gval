package com.gval.gval.repository.repository.custom;

import java.math.BigDecimal;
import java.util.Date;

/**
 * The Interface NefisRepository.
 */
public interface NefisRepositoryCustom {

  /**
   * Generar documento contable.
   *
   * @param tipoDocumento the tipo documento
   * @param subTipoDocumento the sub tipo documento
   * @param tipoComplementario the tipo complementario
   * @param sector the sector
   * @param tipoPrestacon the tipo prestacon
   * @param provincia the provincia
   * @param ejercicio the ejercicio
   * @param fechaEfecto the fecha efecto
   * @param importe the importe
   * @param usuario the usuario
   * @return the string
   */
  String generarDocumentoContable(final String tipoDocumento,
      final String subTipoDocumento, final String tipoComplementario,
      final String sector, final String tipoPrestacon, final String provincia,
      final Long ejercicio, final Date fechaEfecto, final BigDecimal importe,
      final Long usuario);

  /**
   * Anular peticion nefis.
   *
   * @param idDoc the id doc
   * @return the long
   */
  Long anularPeticionNefis(Long idDoc);

}
