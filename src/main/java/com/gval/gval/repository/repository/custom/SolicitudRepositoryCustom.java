package com.gval.gval.repository.repository.custom;

import java.math.BigDecimal;
import java.util.List;

/**
 * The Interface SolicitudRepositoryCustom.
 */
public interface SolicitudRepositoryCustom {

  /**
   * Obtener escenario calculado.
   *
   * @param pkSolicitud the pk solicitud
   * @param revision the revision
   * @return the list
   */
  List<BigDecimal[]> obtenerEscenarioCalculado(final Long pkSolicitud,
      final Boolean revision);

  /**
   * Obtener documentos requeribles.
   *
   * @param pkSolicitud the pk solicitud
   * @param codigoPerfil the codigo perfil
   * @return the list
   */
  List<Object[]> obtenerDocumentosRequeribles(Long pkSolicitud,
      String codigoPerfil);
}
