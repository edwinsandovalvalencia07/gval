package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.InformeSocial;
import com.gval.gval.entity.model.Solicitud;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * The Interface InformeSocialRepository.
 */
@Repository
public interface InformeSocialRepository
    extends JpaRepository<InformeSocial, Long>,
    ExtendedQuerydslPredicateExecutor<InformeSocial> {

  /**
   * Find by solicitud and activo true and es ultimo informe true.
   *
   * @param solicitud the solicitud
   * @return the informe social
   */
  InformeSocial findBySolicitudAndActivoTrueAndEsUltimoInformeTrue(
      Solicitud solicitud);

  /**
   * Find by solicitud and es ultimo informe true.
   *
   * @param solicitud the solicitud
   * @return the list
   */
  List<InformeSocial> findBySolicitudAndEsUltimoInformeTrue(
      Solicitud solicitud);

  /**
   * Find by solicitud and tipo informe social and activo true and es ultimo
   * informe true.
   *
   * @param solicitud the solicitud
   * @param tipoInformeSocial the tipo informe social
   * @return the informe social
   */
  InformeSocial findBySolicitudAndTipoInformeSocialAndActivoTrueAndEsUltimoInformeTrue(
      Solicitud solicitud, Long tipoInformeSocial);


  /**
   * Find by solicitud and activo true.
   *
   * @param solicitud the solicitud
   * @return the list
   */
  List<InformeSocial> findBySolicitudAndActivoTrue(Solicitud solicitud);

  /**
   * Find by solicitud and activo true order by id.
   *
   * @param solicitud the solicitud
   * @return the list
   */
  List<InformeSocial> findBySolicitudAndActivoTrueOrderByPkInformeSocial(
      Solicitud solicitud);


  /**
   * Find by solicitud expediente pk expedien and activo true.
   *
   * @param pkExpedien the pk expedien
   * @param orden the orden
   * @return the list
   */
  List<InformeSocial> findBySolicitudExpedientePkExpedienAndSolicitudActivoTrueAndActivoTrue(
      Long pkExpedien, Sort orden);

  /**
   * Find by solicitud pk solicitud and activo true.
   *
   * @param pkSolicitud the pk solicitud
   * @return the list
   */
  List<InformeSocial> findBySolicitudPkSolicitudAndActivoTrue(Long pkSolicitud);

  /**
   * Find by solicitud pk solicitud and activo true and estado peticion in.
   *
   * @param pkSolicitud the pk solicitud
   * @param estados the estados
   * @return the list
   */
  List<InformeSocial> findBySolicitudPkSolicitudAndActivoTrueAndEstadoPeticionIn(
      Long pkSolicitud, List<String> estados);

  /**
   * Existe pia para informe social seguimiento.
   *
   * @param pkSolicit the pk solicit
   * @return the string
   */
  @Query(
      value = " select EXISTE_PIA_PARA_IS_SEGUIMIENTO(:pkSolicit) from dual ",
      nativeQuery = true)
  String existePiaParaInformeSocialSeguimiento(
      @Param("pkSolicit") Long pkSolicit);

  /**
   * Obtener pk pia para informe social seguimiento.
   *
   * @param pkSolicit the pk solicit
   * @return the long
   */
  @Query(value = " select OBTENER_PK_PIA_IS_SEGUIMIENTO(:pkSolicit) from dual ",
      nativeQuery = true)
  Long obtenerPkPiaParaInformeSocialSeguimiento(
      @Param("pkSolicit") Long pkSolicit);

  /**
   * Obtener informes sociales abiertos por nueva prerefencia A borrar.
   *
   * @param pkdocu the pkdocu
   * @return the optional
   */
  @Query(value = "SELECT INFOSOCI.* FROM SDM_INFOSOCI INFOSOCI "
      + " INNER JOIN SDM_DOCU_INFOSOCI DOCINF ON INFOSOCI.PK_INFOSOCI = DOCINF.PK_INFOSOCI "
      + " INNER JOIN SDM_DOCU DOCU ON DOCU.PK_DOCU =DOCINF.PK_DOCU AND DOCU.DOCACTIVO=1 "
      + " WHERE INFOSOCI.INESTADO<>'C' AND INFOSOCI.ISACTIVO=1 "
      + " AND NOT EXISTS (SELECT 1 FROM SDM_MOTGENERACION_IS MOTIVO "
      + "   WHERE MOTIVO.PK_INFOSOCI = INFOSOCI.PK_INFOSOCI AND MOTIVO.PK_MOTGENERACION <> 5) "
      + " AND NOT EXISTS (SELECT 1 FROM SDM_DOCU_INFOSOCI DOCINF2 "
      + "     INNER JOIN SDM_DOCU DO2 ON DO2.PK_DOCU = DOCINF2.PK_DOCU AND DO2.DOCACTIVO = 1 "
      + "   WHERE DOCINF2.PK_INFOSOCI = INFOSOCI.PK_INFOSOCI AND DOCINF2.PK_DOCU <> DOCINF.PK_DOCU) "
      + " AND DOCU.PK_DOCU=:pkdocu", nativeQuery = true)
  Optional<InformeSocial> obtenerInformeSocialAbiertosPorNuevaPrerefenciaABorrar(
      @Param("pkdocu") Long pkdocu);

  /**
   * Obtener ultimo informe sociales.
   *
   * @param pkSolicitud the pkSolicitud
   * @param motivos the motivos
   * @return the optional
   */
  @Query(
      value = "SELECT ifs.* FROM(SELECT infosoci.pk_infosoci,       ROW_NUMBER() OVER (PARTITION BY s.pk_expedien ORDER BY infosoci.pk_infosoci DESC) rn"
          + "  FROM sdm_infosoci infosoci"
          + "       inner join SDM_MOTGENERACION_IS MOTIVO on MOTIVO.PK_INFOSOCI = INFOSOCI.PK_INFOSOCI"
          + "       inner join sdm_solicit s ON s.pk_solicit = infosoci.pk_solicit WHERE infosoci.isactivo = 1 and infosoci.inestado = 'C' and motivo.pk_motgeneracion in :motivos and s.pk_expedien = (select pk_expedien from sdm_solicit where pk_solicit = :pkSolicitud)) ult_infosoci"
          + "       inner join sdm_infosoci ifs on ifs.pk_infosoci = ult_infosoci.pk_infosoci"
          + "       WHERE rn = 1",
      nativeQuery = true)
  Optional<InformeSocial> obtenerUltimoInformeSocialPorMotivo(
      @Param("pkSolicitud") Long pkSolicitud,
      @Param("motivos") List<Long> motivos);

}
