package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Cuidador;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Repositorio de consultas para Cuidador.
 */
@Repository
public interface CuidadorRepository extends JpaRepository<Cuidador, Long>,
    ExtendedQuerydslPredicateExecutor<Cuidador> {

  /**
   * Find by pk cuidador and activo true.
   *
   * @param pkCuidador the pk cuidador
   * @return the optional
   */
  Optional<Cuidador> findByPkCuidador(Long pkCuidador);


  /**
   * Find by persona nif and activo true.
   *
   * @param nif the nif
   * @return the optional
   */
  List<Cuidador> findByPersonaNifAndActivoTrue(String nif);

  /**
   * Find by preferencia catalogo servicio pk pref cataserv.
   *
   * @param pkPrefCataserv the pk pref cataserv
   * @return the list
   */
  List<Cuidador> findByPreferenciaCatalogoServicioPkPrefCataserv(
      Long pkPrefCataserv);

  /**
   * Find cuidadores por pk expediente.
   *
   * @param pkExpediente the pk expediente
   * @return the list
   */
  @Query(value = " select DISTINCT(cnp.pk_cnp) " + "from sdm_solicit s "
      + " inner join sdm_prefsol pf on s.pk_solicit = pf.pk_solicit "
      + " inner join sdm_pref_cataserv pc on pc.pk_prefsol = pf.pk_prefsol "
      + " inner join sdm_cnp cnp on cnp.pk_pref_cataserv = pc.pk_pref_cataserv "
      + " inner join sdm_persona p on cnp.pk_persona = p.pk_persona "
      + " where cnp.cnfechbaja is null and cnp.cnactivo = 1 and s.pk_expedien = :pkExpediente ",
      nativeQuery = true)
  List<Long> findCuidadoresPorPkExpediente(
      @Param("pkExpediente") Long pkExpediente);


  /**
   * Find cuidadores verificador por pk expediente.
   *
   * @param pkExpediente the pk expediente
   * @return the list
   */
  @Query(value = " select DISTINCT(cnp.pk_cnp) " + "from sdm_solicit s "
      + " inner join sdm_prefsol pf on s.pk_solicit = pf.pk_solicit "
      + " inner join sdm_pref_cataserv pc on pc.pk_prefsol = pf.pk_prefsol "
      + " inner join sdm_cnp cnp on cnp.pk_pref_cataserv = pc.pk_pref_cataserv "
      + " inner join sdm_persona p on cnp.pk_persona = p.pk_persona "
      + " where cnp.cnfechbaja is null and pf.prestado = 0 and cnp.cnactivo = 1 and s.pk_expedien = :pkExpediente ",
      nativeQuery = true)
  List<Long> findCuidadoresVerificadorPorPkExpediente(
      @Param("pkExpediente") Long pkExpediente);

  /**
   * Find nif cuidador activo.
   *
   * @param nif the nif
   * @return the string
   */
  @Query(value = " select DISTINCT(p.penudocide)  " + " from sdm_solicit s  "
      + " inner join sdm_prefsol pf on s.pk_solicit = pf.pk_solicit  "
      + " inner join sdm_pref_cataserv pc on pc.pk_prefsol = pf.pk_prefsol  "
      + " inner join sdm_cnp cnp on cnp.pk_pref_cataserv = pc.pk_pref_cataserv  "
      + " inner join sdm_persona p on cnp.pk_persona = p.pk_persona  "
      + " where cnp.cnfechbaja is null and cnp.cnactivo = 1 "
      + " and p.penudocide = :nif ", nativeQuery = true)
  String findNifCuidadorActivo(@Param("nif") String nif);


  /**
   * Find by preferencia catalogo servicio pk pref cataserv and activo true.
   *
   * @param pkPreferenciaCatalogoServicio the pk preferencia catalogo servicio
   * @return the list
   */
  List<Cuidador> findByPreferenciaCatalogoServicioPkPrefCataservAndActivoTrue(
      Long pkPreferenciaCatalogoServicio);

  /**
   * Obtener listado cuidadores por informe social.
   *
   * @param pkInformeSocial the pk informe social
   * @return the list
   */
  @Query(value = " SELECT DISTINCT(cnp.pk_cnp) FROM sdm_cnp cnp "
      + "INNER JOIN sdm_infosoci inf ON inf.pk_pia = cnp.pk_pia "
      + "WHERE cnp.cnfechefecto is not null and cnp.cnfechbaja is null "
      + "AND inf.pk_infosoci = :pkInfosoci ", nativeQuery = true)
  List<Long> findCuidadoresPorPkInformeSocial(
      @Param("pkInfosoci") Long pkInformeSocial);

  /**
   * Find cuidadores por pk pia.
   *
   * @param pkPia the pk pia
   * @return the list
   */
  @Query(value = " SELECT DISTINCT(cnp.pk_cnp) FROM sdm_cnp cnp "
      + " INNER JOIN sdm_pia pia on pia.pk_pia = cnp.pk_pia "
      + " WHERE pia.pk_pia =  :pkPia ", nativeQuery = true)
  List<Long> findCuidadoresPorPkPia(@Param("pkPia") Long pkPia);

  /**
   * Find cuidadores por pk expediente en IST independientemente de si tiene
   * fecha de baja.
   *
   * @param pkExpediente the pk expediente
   * @return the list
   */
  @Query(value = "select pk_cnp from ("
      + " select cnp.pk_cnp, ROW_NUMBER() OVER(PARTITION BY cnp.pk_persona ORDER BY cnp.pk_cnp DESC) rn"
      + " from sdm_prefsol pf"
      + " INNER JOIN sdm_pref_cataserv pc ON pf.pk_prefsol = pc.pk_prefsol"
      + " INNER JOIN sdm_cnp cnp ON cnp.pk_pref_cataserv = pc.pk_pref_cataserv AND cnp.cnactivo = 1"
      + " INNER JOIN sdm_solicit s on s.pk_solicit = pf.pk_solicit AND s.soactivo = 1"
      + " where s.pk_expedien = :pkExpediente) " + " where rn = 1",
      nativeQuery = true)
  List<Long> findCuidadoresPorPkExpedienteEnTodasPreferencias(
      @Param("pkExpediente") Long pkExpediente);

}
