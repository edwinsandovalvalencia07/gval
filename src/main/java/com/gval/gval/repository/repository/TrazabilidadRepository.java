package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.Trazabilidad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;

/**
 * repository para guarda en la tabla de trazabilidad.
 */
@Repository
public interface TrazabilidadRepository extends JpaRepository<Trazabilidad, Timestamp> {

}
