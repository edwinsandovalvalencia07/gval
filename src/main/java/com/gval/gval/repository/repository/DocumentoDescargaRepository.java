package com.gval.gval.repository.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gval.gval.entity.vistas.model.DocumentoDescarga;

/**
 * The Interface DocDisponiblesListadoRepository.
 */
@Repository
public interface DocumentoDescargaRepository extends JpaRepository<DocumentoDescarga, Long> {

  /**
   * Obtener codigos documentos para descarga.
   *
   * @param codResolu the codResolu
   * @param codTipoDocu the codTipoDocu
   * @param codTipoDocu the orderByu
   * @return the list
   */

  @Query(value = "{call DESCARGAR_DOCUMENTOS_V2(:P_COD_RESOLUCI,:P_COD_TIPODOCU,:P_ORDEN)}", nativeQuery = true)
  List<DocumentoDescarga> obtenerCodigosDocumentosParaDescarga2(
			@Param("P_COD_RESOLUCI") String codResolu,
			@Param("P_COD_TIPODOCU") String codTipoDocu,
			@Param("P_ORDEN") String orderBy
			);

}
