package com.gval.gval.repository.repository;

import com.gval.gval.dto.dto.ZonaCoberturaConsulta012DTO;
import com.gval.gval.entity.model.Poblacion;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * The Interface PoblacionRepository.
 */
@Repository
public interface PoblacionRepository extends JpaRepository<Poblacion, Long>,
    ExtendedQuerydslPredicateExecutor<Poblacion> {

  /**
   * Find by municipio provincia pk provincia and activo true.
   *
   * @param pkProvincia the pk provincia
   * @param orden the orden
   * @return the list
   */
  List<Poblacion> findByMunicipioProvinciaPkProvinciaAndActivoTrue(
      Long pkProvincia, Sort orden);

  /**
   * Find by codigo provincia and codigo municipio and codigo poblacion.
   *
   * @param codigoProvincia the codigo provincia
   * @param codigoMunicipio the codigo municipio
   * @param codigoPoblacion the codigo poblacion
   * @return the optional
   */
  Optional<Poblacion> findByCodigoProvinciaAndCodigoMunicipioAndCodigoPoblacion(
      String codigoProvincia, String codigoMunicipio, String codigoPoblacion);

  /**
   * Find poblacion zona cobertura 012.
   *
   * @return the list
   */
  @Query("select new es.gva.dependencia.ada.dto.ZonaCoberturaConsulta012DTO(concat(pobl.nombre,' (',prov.nombre,')'), zonaCobe.nombre, direc.telefonoFijo) "
      + " from Poblacion pobl,Municipio muni, ZonaCobertura zonaCobe, Unidad uni, DireccionAda direc, Provincia prov"
      + " where pobl.municipio.pkMunicipio = muni.pkMunicipio "
      + " and zonaCobe.pkZonaCobertura = muni.zonaCobertura.pkZonaCobertura"
      + " and uni.zonaCobertura.pkZonaCobertura = zonaCobe.pkZonaCobertura"
      + " and uni.direccion.pkDireccion = direc.pkDireccion"
      + " and prov.pkProvincia = muni.provincia.pkProvincia"
      + " and muni.activo=1" + " and zonaCobe.activo=1" + " and uni.activo=1"
      + " and pobl.activo=1" + " and muni.codigoProvincia IN('03','46','12')"
      + " order by pobl.nombre ASC ")
  List<ZonaCoberturaConsulta012DTO> findPoblacionZonaCobertura012();
}
