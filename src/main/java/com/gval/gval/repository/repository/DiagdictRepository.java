package com.gval.gval.repository.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gval.gval.entity.model.Diagdict;

// TODO: Auto-generated Javadoc
/**
 * The Interface DiagdictRepository.
 */
@Repository
public interface DiagdictRepository extends JpaRepository<Diagdict, Long> {


  /**
   * Delete by dictamen pk dictamen.
   *
   * @param pkDictamen the pk dictamen
   */
  void deleteByDictamenPkDictamen(Long pkDictamen);
}
