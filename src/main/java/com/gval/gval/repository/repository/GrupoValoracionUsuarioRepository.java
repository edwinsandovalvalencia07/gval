package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.GrupoValoracionUsuario;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * The Interface PerfilRepository.
 */
public interface GrupoValoracionUsuarioRepository
    extends JpaRepository<GrupoValoracionUsuario, Long>,
    ExtendedQuerydslPredicateExecutor<GrupoValoracionUsuario> {

  
  /**
   * Find by activo true and usuario pk persona.
   *
   * @param pkPersona the pk persona
   * @return the optional
   */
  Optional<GrupoValoracionUsuario> findByActivoTrueAndUsuarioPkPersona(Long pkPersona);

}
