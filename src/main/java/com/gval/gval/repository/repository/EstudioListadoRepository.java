package com.gval.gval.repository.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gval.gval.entity.vistas.model.EstudioListado;

@Repository
public interface EstudioListadoRepository extends JpaRepository<EstudioListado, Long>{
	
	 List<EstudioListado> findByPkSolicit(Long pkSolicit, Sort orden);
}
