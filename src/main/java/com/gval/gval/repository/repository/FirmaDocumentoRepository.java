package com.gval.gval.repository.repository;

import com.gval.gval.entity.model.FirmaDocumento;
import com.gval.gval.entity.model.TipoDocumento;
import com.gval.gval.interfaceJar.ExtendedQuerydslPredicateExecutor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repositorio de consultas de FirmaDocumento.
 *
 * @author Indra
 */
@Repository
public interface FirmaDocumentoRepository
    extends JpaRepository<FirmaDocumento, Long>,
    ExtendedQuerydslPredicateExecutor<FirmaDocumento> {

  /**
   * Find by tipo documento and activo true.
   *
   * @param tipoDocumento the tipo documento
   * @return the firma documento
   */
  FirmaDocumento findByTipoDocumentoAndActivoTrue(TipoDocumento tipoDocumento);

  /**
   * Find by tipo documento and activo true and suplente false.
   *
   * @param entity the entity
   * @return the firma documento
   */
  FirmaDocumento findByTipoDocumentoAndActivoTrueAndSuplenteFalse(
      TipoDocumento entity);
}
